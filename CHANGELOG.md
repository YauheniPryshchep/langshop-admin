# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v1.1.0](https://gitlab.com/devit.team/shopify/administration/app/compare/v1.0.1...v1.1.0) (2021-04-14)

### Features
* Admin: Affiliate program to admin
* Admin: Add Confirmation window when start campaign
* Admin: Add opportunity change default sender in campaign

## [v1.0.1](https://gitlab.com/devit.team/shopify/administration/app/-/tags/v1.0.1) (2021-02-17)

### Features
* Admin: New authorization provider
* Admin: Coupons
* Admin: Customer Emails

### Fixes
* Admin: Campaign Filters
