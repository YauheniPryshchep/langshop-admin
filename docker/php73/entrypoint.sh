#!/usr/bin/env sh
adduser -S -D -h /home/user -u $CURRENT_USER_ID $FPM_USER $FPM_USER
sed -i -r "/user=/s/([0-9]+)/$FPM_USER/1" /etc/supervisor/conf.d/lumen-worker.conf
FPM_CONFIG_PATH="${FPM_CONFIG_PATH:-/usr/local/etc/php-fpm.conf}";

FPM_PORT="${FPM_PORT:-9000}";
FPM_USER="${FPM_USER:-root}";
FPM_GROUP="${FPM_GROUP:-root}";

sed -i "s#%FPM_PORT%#${FPM_PORT}#g" "$FPM_CONFIG_PATH";
sed -i "s#%FPM_USER%#${FPM_USER}#g" "$FPM_CONFIG_PATH";
sed -i "s#%FPM_GROUP%#${FPM_GROUP}#g" "$FPM_CONFIG_PATH";
if [ ! -d /home/${CURRENT_USER} ]; then
	mkdir /home/${CURRENT_USER} 
	chown ${CURRENT_USER}:${CURRENT_USER} /home/${CURRENT_USER}
fi
exec "$@";