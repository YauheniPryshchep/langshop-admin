-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Апр 02 2018 г., 13:28
-- Версия сервера: 5.5.59-0ubuntu0.14.04.1
-- Версия PHP: 5.5.9-1ubuntu4.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `asa_dev`
--

-- --------------------------------------------------------

--
-- Структура таблицы `email_templates`
--

CREATE TABLE IF NOT EXISTS `email_templates` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `data` mediumtext NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`),
  KEY `type` (`type`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Дамп данных таблицы `email_templates`
--

INSERT INTO `email_templates` (`id`, `slug`, `name`, `type`, `data`) VALUES
(1, 'historyComeBackMessage', 'History Come Back Message', 'history', '<table width="60%" height="373" class="c1064 c403 c399 c400" style="box-sizing: border-box; height: 373px; padding: 5px 5px 5px 5px; width: 60%; border-collapse: collapse; margin: 60px 261.5px 10px 261.5px;"><tbody id="i4fb" style="box-sizing: border-box;"><tr id="irei" style="box-sizing: border-box;"><td id="idvd" valign="top" bgcolor="#ffffff" style="box-sizing: border-box; font-size: 20px; font-weight: 300; vertical-align: top; margin: 0; padding: 0; background-color: #ffffff; color: #58317a; border: 1px solid #e9e9e9; border-radius: 3px 3px 3px 3px;"><table width="100%" height="69" bgcolor="#1d235f" id="iox7" class="c1292" style="box-sizing: border-box; height: 69px; margin: 0 auto 10px auto; padding: 5px 5px 5px 5px; width: 100%; background-color: #1d235f; border-collapse: collapse;"><tbody id="i2dm" style="box-sizing: border-box;"><tr id="i7wp" style="box-sizing: border-box;"><td id="ipbi4" width="11%" valign="top" style="box-sizing: border-box; font-size: 12px; font-weight: 300; vertical-align: top; color: rgb(111, 119, 125); margin: 0; padding: 0; width: 11%;"><img src="https://devit-general-media.s3.amazonaws.com/bSwuGIFIezgmJtla6hD8LHXULGDjpfpqQpOSom0w.png" width="108" height="95" id="iev03" class="c1411" style="box-sizing: border-box; color: black; width: 108px; height: 95px;"></td><td id="isc2r" width="70%" valign="top" style="box-sizing: border-box; font-size: 12px; font-weight: 300; vertical-align: top; color: rgb(111, 119, 125); margin: 0; padding: 0; width: 70%;"><div id="i4jmp" class="c1479" style="box-sizing: border-box; padding: 10px; font-size: 37px; text-align: center; color: #f8f8f8; text-shadow: 1px 1px 1px black;">{{title}}</div></td></tr></tbody></table><div id="i28uy" class="c1636" style="box-sizing: border-box; padding: 10px; font-size: 14px;">{{message}}</div></td></tr></tbody></table>'),
(2, 'iMobileAddBuildRequest', 'iMobile Add Build Request', 'iMobile', '<table width="100%" height="150" class="c1363 c406" style="box-sizing: border-box; height: 150px; margin: 0 auto 10px auto; padding: 5px 5px 5px 5px; width: 100%; border-collapse: collapse;"><tbody id="i125" style="box-sizing: border-box;"><tr id="ii2j" style="box-sizing: border-box;"><td id="iqmg4" valign="top" bgcolor="#f6f6f6" style="box-sizing: border-box; font-size: 12px; font-weight: 300; vertical-align: top; color: rgb(111, 119, 125); margin: 0; padding: 0; background-color: #f6f6f6;"><table width="560" height="494" bgcolor="#FFF" class="c1066 c401 c402 c406" id="iegl" style="box-sizing: border-box; height: 494px; padding: 20px 20px 20px 20px; border-collapse: collapse; width: 560px; background-color: #FFF; border-radius: 5px 5px 5px 5px; margin: 40px auto 40px auto; border: 0 solid rgba(255,255,255,0);"><tbody id="i037" style="box-sizing: border-box;"><tr id="iula" style="box-sizing: border-box;"><td id="iajw" valign="top" width="560" style="box-sizing: border-box; font-size: 12px; font-weight: 300; vertical-align: top; color: rgb(111, 119, 125); margin: 0; padding: 0; width: 560px;"><img src="https://devit-general-media.s3.amazonaws.com/LSCbkkTB8X8QGHveWAxnsjaYaTfOaHsZfelDDO4A.png" id="i6s3" class="c1292" style="box-sizing: border-box; color: black;"><table width="100%" height="100%" id="iffa" class="c1753" style="box-sizing: border-box; height: auto; margin: 0 auto 1px auto; padding: 5px 5px 5px 5px; width: 100%; border-collapse: collapse;"><tbody id="i3k4" style="box-sizing: border-box;"><tr id="iw24" style="box-sizing: border-box;"><td id="iz4z4" valign="top" style="box-sizing: border-box; font-size: 12px; font-weight: 300; vertical-align: top; color: rgb(111, 119, 125); margin: 0; padding: 20px 20px 20px 20px;"><div id="izhrp" class="c1989" style="box-sizing: border-box; padding: 10px; font-size: 14px; color: #222; font-family: Helvetica, serif;">Hello!</div><span class="c1799" style="box-sizing: border-box; color: rgb(34, 34, 34); font-family: Helvetica, serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 300; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial; float: none; display: inline;"> </span><div class="c1989" style="box-sizing: border-box; padding: 10px; font-size: 14px; color: #222; font-family: Helvetica, serif;"><div style="box-sizing: border-box;">This letter is to inform you that <b style="box-sizing: border-box;">your request was added to the queue</b>. The process of app generation will take some time. After generation we’ll send you email with link on your application file. </div></div><div class="c1989" style="box-sizing: border-box; padding: 10px; font-size: 14px; color: #222; font-family: Helvetica, serif;">Please, do not reply this message.</div><div class="c1989" style="box-sizing: border-box; padding: 10px; font-size: 14px; color: #222; font-family: Helvetica, serif;"><div style="box-sizing: border-box;">Best regards,</div><div style="box-sizing: border-box;"><br style="box-sizing: border-box;"></div><div style="box-sizing: border-box;">iMobile Team</div></div></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table>'),
(3, 'iMobileAdminStageMessage', 'iMobile Admin Stage Message', 'iMobile', '<table width="100%" height="150" bgcolor="#f6f6f6" class="c1558 c406" style="box-sizing: border-box; height: 150px; margin: 0 auto 10px auto; padding: 5px 5px 5px 5px; width: 100%; border-collapse: collapse; background-color: #f6f6f6;"><tbody id="i237" style="box-sizing: border-box;"><tr id="iw8q" style="box-sizing: border-box;"><td id="i8jnj" valign="top" style="box-sizing: border-box; font-size: 12px; font-weight: 300; vertical-align: top; color: rgb(111, 119, 125); margin: 0; padding: 40px 0 80px 0;"><table width="560" height="494" class="c1066 c401 c402 c406" style="box-sizing: border-box; height: 494px; padding: 20px 20px 20px 20px; border-collapse: collapse; width: 560px; border-radius: 5px 5px 5px 5px; margin: 40px auto 40px auto; background-color: #fff;" bgcolor="#fff"><tbody id="i037" style="box-sizing: border-box;"><tr id="iula" style="box-sizing: border-box;"><td id="iajw" valign="top" width="560" style="box-sizing: border-box; font-size: 12px; font-weight: 300; vertical-align: top; color: rgb(111, 119, 125); margin: 0; padding: 0; width: 560px;"><img src="https://devit-general-media.s3.amazonaws.com/LSCbkkTB8X8QGHveWAxnsjaYaTfOaHsZfelDDO4A.png" id="i6s3" class="c1292" style="box-sizing: border-box; color: black;"><table width="100%" height="100%" id="iffa" class="c1753" style="box-sizing: border-box; height: 100%; margin: 0 auto 1px auto; padding: 5px 5px 5px 5px; width: 100%; border-collapse: collapse;"><tbody id="i3k4" style="box-sizing: border-box;"><tr id="iw24" style="box-sizing: border-box;"><td id="iz4z4" valign="top" style="box-sizing: border-box; font-size: 12px; font-weight: 300; vertical-align: top; color: rgb(111, 119, 125); margin: 0; padding: 20px 20px 20px 20px;"><div id="ijyyg" class="c1989" style="box-sizing: border-box; padding: 10px; font-size: 14px; color: #222; font-family: Helvetica, serif; margin: 3px 0 0 0;"><b id="i0lwa" style="box-sizing: border-box;">Store Data</b></div><div id="imtwl" class="c1989" style="box-sizing: border-box; padding: 10px; font-size: 14px; color: #222; font-family: Helvetica, serif; margin: 3px 0 0 0;"><b id="ijvsg" style="box-sizing: border-box;">Name:</b> {{name}}</div><div id="iibot" class="c1989" style="box-sizing: border-box; padding: 10px; font-size: 14px; color: #222; font-family: Helvetica, serif; margin: 3px 0 0 0;"><b id="isvzg" style="box-sizing: border-box;">Email:</b> {{email}}</div><div id="igl85" class="c1989" style="box-sizing: border-box; padding: 10px; font-size: 14px; color: #222; font-family: Helvetica, serif; margin: 3px 0 0 0;"><b id="ikpof" style="box-sizing: border-box;">Build Data</b></div><div id="iswz6" class="c1989" style="box-sizing: border-box; padding: 10px; font-size: 14px; color: #222; font-family: Helvetica, serif; margin: 3px 0 0 0;"><b id="icvlq" style="box-sizing: border-box;">ID:</b> {{id}}</div><div id="io7mp" class="c1989" style="box-sizing: border-box; padding: 10px; font-size: 14px; color: #222; font-family: Helvetica, serif; margin: 3px 0 0 0;"><b id="irdtm" style="box-sizing: border-box;">Stage:</b> {{stage}}</div><div id="i7gjk" class="c1989" style="box-sizing: border-box; padding: 10px; font-size: 14px; color: #222; font-family: Helvetica, serif; margin: 3px 0 0 0;"><b id="iya8m" style="box-sizing: border-box;">Type:</b> {{type}}</div><div id="iugs7" class="c1989" style="box-sizing: border-box; padding: 10px; font-size: 14px; color: #222; font-family: Helvetica, serif; margin: 3px 0 0 0;"><b id="izh9l" data-highlightable="1" class="c1478" style="box-sizing: border-box;">Added:</b> {{addedTimestamp}}</div><div id="i1ptn" class="c1989" style="box-sizing: border-box; padding: 10px; font-size: 14px; color: #222; font-family: Helvetica, serif; margin: 3px 0 0 0;"><b id="i2i86" data-highlightable="1" class="c1528" style="box-sizing: border-box;">Finished:</b> {{finishedTimestamp}}</div><div id="i02vk" class="c1989" style="box-sizing: border-box; padding: 10px; font-size: 14px; color: #222; font-family: Helvetica, serif; margin: 3px 0 0 0;"><b id="ib8x4" style="box-sizing: border-box;">Url:</b> {{url}}</div></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table>'),
(4, 'iMobileBuildSuccess', 'iMobile Build Success Message', 'iMobile', '<table width="100%" height="150" class="c1363 c406" style="box-sizing: border-box; height: 150px; margin: 0 auto 10px auto; padding: 5px 5px 5px 5px; width: 100%; border-collapse: collapse;"><tbody id="i125" style="box-sizing: border-box;"><tr id="ii2j" style="box-sizing: border-box;"><td id="iqmg4" valign="top" bgcolor="#f6f6f6" style="box-sizing: border-box; font-size: 12px; font-weight: 300; vertical-align: top; color: rgb(111, 119, 125); margin: 0; padding: 0; background-color: #f6f6f6;"><table width="560" height="327" bgcolor="#FFF" class="c1066 c401 c402 c406" id="isna" style="box-sizing: border-box; height: 327px; padding: 20px 20px 20px 20px; border-collapse: collapse; width: 560px; background-color: #FFF; border-radius: 5px 5px 5px 5px; margin: 40px auto 40px auto; border: 0 solid rgba(255,255,255,0);"><tbody id="i037" style="box-sizing: border-box;"><tr id="iula" style="box-sizing: border-box;"><td id="iajw" valign="top" width="100%" style="box-sizing: border-box; font-size: 12px; font-weight: 300; vertical-align: top; color: rgb(111, 119, 125); margin: 0; padding: 0; width: 100%;"><img src="https://devit-general-media.s3.amazonaws.com/LSCbkkTB8X8QGHveWAxnsjaYaTfOaHsZfelDDO4A.png" id="i6s3" class="c1292" style="box-sizing: border-box; color: black;"><table width="100%" height="100%" id="iffa" class="c1753" style="box-sizing: border-box; height: auto; margin: 0 auto 1px auto; padding: 5px 5px 5px 5px; width: 100%; border-collapse: collapse;"><tbody id="i3k4" style="box-sizing: border-box;"><tr id="iw24" style="box-sizing: border-box;"><td id="iz4z4" valign="top" style="box-sizing: border-box; font-size: 12px; font-weight: 300; vertical-align: top; color: rgb(111, 119, 125); margin: 0; padding: 20px 20px 20px 20px; max-width: auto; min-height: auto; height: auto;"><div class="c1417" style="box-sizing: border-box; padding: 10px; font-size: 14px; font-family: Helvetica, serif; color: #222;">Hello!</div><div class="c1417" style="box-sizing: border-box; padding: 10px; font-size: 14px; font-family: Helvetica, serif; color: #222;">This letter is to inform you that <b style="box-sizing: border-box;">your build request was completed successfully</b>.<div style="box-sizing: border-box;">Download your application you can by clicking on the link below.</div></div><table width="100%" height="150" class="c1903" style="box-sizing: border-box; width: 100%; margin-top: 10px; margin-bottom: 10px; height: auto;"><tbody id="id8lk" style="box-sizing: border-box;"><tr id="i8smc" style="box-sizing: border-box;"><td id="i10jl" valign="top" style="box-sizing: border-box; font-size: 12px; font-weight: 300; vertical-align: top; color: rgb(111, 119, 125); margin: 0; padding: 10px 0 0 10px; text-decoration: underline;"><a target="_blank" href="{{url}}" class="button" style="box-sizing: border-box; padding: 0 10px 0 10px; margin: 0 0 0 0; border: 14px solid #348eda; vertical-align: bottom; line-height: 39px; background-color: #348eda; border-collapse: separate; color: #fff; font-size: 14px; font-family: Helvetica, serif; border-radius: 5px 5px 5px 5px; text-decoration: none;">Download App</a></td></tr></tbody></table><div class="c2262" style="box-sizing: border-box; padding: 10px; font-size: 14px; font-family: Helvetica, serif; color: #222;">If the above link doesn''t work, yo can find it on <a href="{{storeUrl}}" target="_blank" class="link" style="box-sizing: border-box;">iMobile app page in your store</a>.</div><div class="c2262" style="box-sizing: border-box; padding: 10px; font-size: 14px; font-family: Helvetica, serif; color: #222;"><div style="box-sizing: border-box;">Best regards,</div><div style="box-sizing: border-box;"><br style="box-sizing: border-box;"></div><div style="box-sizing: border-box;">iMobile Team</div></div></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table>'),
(5, 'iMobilePlainMessage', 'iMobile Plain Message', 'iMobile', '<table width="100%" height="150" class="c1363 c406" style="box-sizing: border-box; height: 150px; margin: 0 auto 10px auto; padding: 5px 5px 5px 5px; width: 100%; border-collapse: collapse;"><tbody id="i125" style="box-sizing: border-box;"><tr id="ii2j" style="box-sizing: border-box;"><td id="iqmg4" valign="top" bgcolor="#f6f6f6" style="box-sizing: border-box; font-size: 12px; font-weight: 300; vertical-align: top; color: rgb(111, 119, 125); margin: 0; padding: 0; background-color: #f6f6f6;"><table width="560" height="327" bgcolor="#FFF" class="c1066 c401 c402 c406" id="ih1q" style="box-sizing: border-box; height: 327px; padding: 20px 20px 20px 20px; border-collapse: collapse; width: 560px; background-color: #FFF; border-radius: 5px 5px 5px 5px; margin: 40px auto 40px auto; border: 0 solid rgba(255,255,255,0);"><tbody id="i037" style="box-sizing: border-box;"><tr id="iula" style="box-sizing: border-box;"><td id="iajw" valign="top" width="560" style="box-sizing: border-box; font-size: 12px; font-weight: 300; vertical-align: top; color: rgb(111, 119, 125); margin: 0; padding: 0; width: 560px;"><img src="https://devit-general-media.s3.amazonaws.com/LSCbkkTB8X8QGHveWAxnsjaYaTfOaHsZfelDDO4A.png" id="i6s3" class="c1292" style="box-sizing: border-box; color: black;"><table width="100%" height="100%" id="iffa" class="c1753" style="box-sizing: border-box; height: auto; margin: 0 auto 1px auto; padding: 5px 5px 5px 5px; width: 100%; border-collapse: collapse;"><tbody id="i3k4" style="box-sizing: border-box;"><tr id="iw24" style="box-sizing: border-box;"><td id="iz4z4" valign="top" class="c496" style="box-sizing: border-box; font-size: 12px; font-weight: 300; vertical-align: top; color: rgb(111, 119, 125); margin: 0; padding: 20px 20px 20px 20px; max-width: auto; min-height: auto; height: auto;"><div class="c1631" style="box-sizing: border-box; padding: 10px; font-size: 14px; color: #222;">{{message}}</div></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table>');

-- --------------------------------------------------------

--
-- Структура таблицы `email_templates_variables`
--

CREATE TABLE IF NOT EXISTS `email_templates_variables` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `email_templates_id` int(11) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `label` varchar(255) NOT NULL,
  `type` enum('dynamic','static') NOT NULL DEFAULT 'static',
  `required` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `system` tinyint(1) NOT NULL DEFAULT '1',
  `value` text,
  PRIMARY KEY (`id`),
  KEY `email_templates_id` (`email_templates_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=18 ;

--
-- Дамп данных таблицы `email_templates_variables`
--

INSERT INTO `email_templates_variables` (`id`, `email_templates_id`, `name`, `label`, `type`, `required`, `system`, `value`) VALUES
(1, 1, 'title', 'title', 'dynamic', 1, 1, NULL),
(2, 1, 'message', 'message', 'dynamic', 1, 0, NULL),
(3, 2, 'title', 'title', 'dynamic', 0, 1, NULL),
(4, 3, 'title', 'title', 'dynamic', 0, 1, NULL),
(5, 3, 'stage', 'stage', 'dynamic', 0, 1, NULL),
(6, 3, 'name', 'name', 'dynamic', 0, 1, NULL),
(7, 3, 'email', 'email', 'dynamic', 0, 1, NULL),
(8, 3, 'id', 'id', 'dynamic', 0, 1, NULL),
(9, 3, 'type', 'type', 'dynamic', 0, 1, NULL),
(10, 3, 'addedTimestamp', 'addedTimestamp', 'dynamic', 0, 1, NULL),
(11, 3, 'finishedTimestamp', 'finishedTimestamp', 'dynamic', 0, 1, NULL),
(12, 3, 'url', 'url', 'dynamic', 0, 1, NULL),
(13, 4, 'title', 'title', 'dynamic', 0, 1, NULL),
(14, 4, 'url', 'url', 'dynamic', 1, 1, NULL),
(15, 5, 'title', 'title', 'dynamic', 0, 1, NULL),
(16, 5, 'message', 'message', 'dynamic', 1, 1, NULL),
(17, 4, 'storeUrl', 'Link for iMobile release page', 'dynamic', 0, 1, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2017_12_04_111445_create_users_table', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Дамп данных таблицы `roles`
--

INSERT INTO `roles` (`id`, `name`, `description`) VALUES
(1, 'sadmin', 'Super Administrator. God of this lands.'),
(2, 'nobody', 'No name stranger'),
(4, 'admin', 'System administrator'),
(5, '123', 'test role');

-- --------------------------------------------------------

--
-- Структура таблицы `roles_scopes`
--

CREATE TABLE IF NOT EXISTS `roles_scopes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `roles_id` int(11) unsigned NOT NULL,
  `scopes_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roleId` (`roles_id`,`scopes_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=57 ;

--
-- Дамп данных таблицы `roles_scopes`
--

INSERT INTO `roles_scopes` (`id`, `roles_id`, `scopes_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 1, 4),
(5, 1, 5),
(6, 1, 6),
(23, 1, 7),
(24, 1, 8),
(25, 1, 9),
(26, 1, 10),
(27, 1, 11),
(31, 1, 12),
(32, 1, 13),
(47, 1, 14),
(48, 1, 15),
(39, 1, 16),
(40, 1, 17),
(41, 1, 18),
(42, 1, 19),
(49, 1, 20),
(50, 1, 21),
(51, 1, 22),
(52, 1, 23),
(15, 4, 1),
(16, 4, 2),
(17, 4, 3),
(18, 4, 4),
(19, 4, 5),
(20, 4, 6),
(21, 4, 7),
(22, 4, 8),
(28, 4, 9),
(29, 4, 10),
(30, 4, 11),
(35, 4, 12),
(36, 4, 13),
(37, 4, 14),
(38, 4, 15),
(43, 4, 16),
(44, 4, 17),
(45, 4, 18),
(46, 4, 19),
(53, 4, 20),
(54, 4, 21),
(55, 4, 22),
(56, 4, 23),
(12, 5, 1),
(13, 5, 2),
(14, 5, 3);

-- --------------------------------------------------------

--
-- Структура таблицы `scopes`
--

CREATE TABLE IF NOT EXISTS `scopes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `scopes_types_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `scopes_types_id` (`scopes_types_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=24 ;

--
-- Дамп данных таблицы `scopes`
--

INSERT INTO `scopes` (`id`, `name`, `description`, `scopes_types_id`) VALUES
(1, 'imobile-buildrequests-show', 'iMobile: view build requests list', 2),
(2, 'imobile-buildrequests-update', 'iMobile: edit build request', 2),
(3, 'main-users-show', 'Main: users show', 1),
(4, 'main-users-update', 'Main: users edit', 1),
(5, 'main-roles-show', 'Main: roles show', 1),
(6, 'main-roles-update', 'Main: roles update', 1),
(7, 'imobile-stores-demo-show', 'iMobile: Show demo stores', 2),
(8, 'imobile-stores-demo-update', 'iMobile: Edit demo stores', 2),
(9, 'imobile-storeshistory-show', 'iMobile: Show stores history', 2),
(10, 'imobile-storeshistory-update', 'iMobile: Update stores history', 2),
(11, 'main-email-templates-manage', 'Main: Manage email templates', 3),
(12, 'langshop-stores-demo-show', 'LangShop: Show demo stores', 4),
(13, 'langshop-stores-demo-update', 'LangShop: Edit demo stores', 4),
(14, 'langshop-storeshistory-show', 'LangShop: Show stores history', 4),
(15, 'langshop-storeshistory-update', 'LangShop: Update stores history', 4),
(16, 'buildify-stores-demo-show', 'Buildify: Show demo stores', 5),
(17, 'buildify-stores-demo-update', 'Buildify: Update demo stores', 5),
(18, 'buildify-storeshistory-show', 'Buildify: Show stores history', 5),
(19, 'buildify-storeshistory-update', 'Buildify: Update stores history', 5),
(20, 'imobile-stores-discounts-show', 'iMobile: Show discount stores', 2),
(21, 'imobile-stores-discounts-update', 'iMobile: Edit discount stores', 2),
(22, 'imobile-stores-trials-show', 'iMobile: Show trial stores', 2),
(23, 'imobile-stores-trials-update', 'iMobile: Edit trial stores', 2);

-- --------------------------------------------------------

--
-- Структура таблицы `scopes_types`
--

CREATE TABLE IF NOT EXISTS `scopes_types` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Дамп данных таблицы `scopes_types`
--

INSERT INTO `scopes_types` (`id`, `name`) VALUES
(1, 'Users'),
(2, 'iMobile'),
(3, 'Settings'),
(4, 'LangShop'),
(5, 'Buildify');

-- --------------------------------------------------------

--
-- Структура таблицы `stores_history`
--

CREATE TABLE IF NOT EXISTS `stores_history` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `type` enum('installed','uninstalled','undefined') NOT NULL,
  `store` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `reason` text,
  `response` text,
  `processed` tinyint(1) NOT NULL DEFAULT '0',
  `response_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `app` (`app`,`type`,`store`,`email`,`created_at`,`updated_at`,`processed`),
  KEY `response_at` (`response_at`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=31 ;

--
-- Дамп данных таблицы `stores_history`
--

INSERT INTO `stores_history` (`id`, `app`, `type`, `store`, `email`, `created_at`, `updated_at`, `reason`, `response`, `processed`, `response_at`) VALUES
(1, 'imobile', 'installed', 'undefined', 'undefined', '2018-03-19 12:38:22', '2018-03-19 12:38:22', NULL, NULL, 0, NULL),
(2, 'imobile', 'installed', 'undefined', 'undefined', '2018-03-19 12:38:25', '2018-03-19 12:38:25', NULL, NULL, 0, NULL),
(3, 'imobile', 'installed', 'undefined', 'undefined', '2018-03-19 12:38:26', '2018-03-19 12:38:26', NULL, NULL, 0, NULL),
(4, 'imobile', 'installed', 'undefined', 'undefined', '2018-03-19 12:38:27', '2018-03-19 12:38:27', NULL, NULL, 0, NULL),
(5, 'imobile', 'installed', 'undefined', 'undefined', '2018-03-19 12:38:27', '2018-03-19 12:38:27', NULL, NULL, 0, NULL),
(6, 'imobile', 'installed', 'undefined', 'undefined', '2018-03-19 12:38:27', '2018-03-19 12:38:27', NULL, NULL, 0, NULL),
(7, 'imobile', 'installed', 'undefined', 'undefined', '2018-03-19 12:38:28', '2018-03-19 12:38:28', NULL, NULL, 0, NULL),
(8, 'imobile', 'installed', 'undefined', 'undefined', '2018-03-19 12:38:28', '2018-03-19 12:38:28', NULL, NULL, 0, NULL),
(9, 'imobile', 'installed', 'undefined', 'undefined', '2018-03-19 12:38:29', '2018-03-19 12:38:29', NULL, NULL, 0, NULL),
(10, 'imobile', 'installed', 'undefined', 'undefined', '2018-03-19 12:38:29', '2018-03-19 12:38:29', NULL, NULL, 0, NULL),
(11, 'imobile', 'uninstalled', 'undefined', 'undefined', '2018-03-19 12:39:09', '2018-03-19 12:39:09', NULL, NULL, 0, NULL),
(12, 'imobile', 'uninstalled', 'undefined', 'undefined', '2018-03-19 12:39:10', '2018-03-19 12:39:10', NULL, NULL, 0, NULL),
(13, 'imobile', 'uninstalled', 'undefined', 'undefined', '2018-03-19 12:39:11', '2018-03-19 12:39:11', NULL, NULL, 0, NULL),
(14, 'imobile', 'uninstalled', 'undefined', 'undefined', '2018-03-19 12:39:12', '2018-03-19 12:39:12', NULL, NULL, 0, NULL),
(15, 'imobile', 'installed', 'undefined', 'undefined', '2018-03-19 12:43:45', '2018-03-19 12:43:45', NULL, NULL, 0, NULL),
(16, 'imobile', 'installed', 'test-imobile-ttitan-1.myshopify.com', 'rodion.bezruk@devit-team.com', '2018-03-19 12:56:44', '2018-03-19 12:56:44', NULL, NULL, 0, NULL),
(17, 'imobile', 'uninstalled', 'test-imobile-ttitan-1.myshopify.com', 'rodion.bezruk@devit-team.com', '2018-03-19 13:00:50', '2018-03-21 11:12:43', NULL, NULL, 1, '2018-03-21 13:12:43'),
(18, 'imobile', 'uninstalled', 'test-imobile-ttitan-1.myshopify.com', 'rodion.bezruk@devit-team.com', '2018-03-19 13:06:06', '2018-03-21 11:11:11', 'test 123', 'test 123', 0, NULL),
(19, 'imobile', 'uninstalled', 'undefined', 'rodion.bezruk@devit-team.com', '2018-03-21 07:54:09', '2018-03-21 07:54:09', NULL, NULL, 0, NULL),
(20, 'imobile', 'uninstalled', 'langshopcut.myshopify.com', 'rodion.bezruk@devit-team.com', '2018-03-21 08:02:12', '2018-03-21 08:02:12', NULL, NULL, 0, NULL),
(21, 'imobile', 'uninstalled', 'langshopcut.myshopify.com', 'rodion.bezruk@devit-team.com', '2018-03-21 08:02:20', '2018-03-21 08:02:20', NULL, NULL, 0, NULL),
(22, 'imobile', 'uninstalled', 'langshopcut.myshopify.com1', 'rodion.bezruk@devit-team.com', '2018-03-21 08:02:28', '2018-03-21 08:02:28', NULL, NULL, 0, NULL),
(23, 'imobile', 'uninstalled', 'langshopcut.myshopify.com1', 'rodion.bezruk@devit-team.com', '2018-03-21 08:02:29', '2018-03-21 08:02:29', NULL, NULL, 0, NULL),
(24, 'imobile', 'uninstalled', 'langshopcut.myshopify.com1', 'rodion.bezruk@devit-team.com', '2018-03-21 08:02:33', '2018-03-21 08:02:33', NULL, NULL, 0, NULL),
(25, 'imobile', 'installed', 'langshopcut.myshopify.com1', 'rodion.bezruk@devit-team.com', '2018-03-21 08:02:38', '2018-03-21 08:02:38', NULL, NULL, 0, NULL),
(26, 'imobile', 'installed', 'langshopcut.myshopify.com1', 'rodion.bezruk@devit-team.com', '2018-03-21 08:02:39', '2018-03-21 08:02:39', NULL, NULL, 0, NULL),
(27, 'imobile', 'installed', 'langshopcut.myshopify.com1', 'rodion.bezruk@devit-team.com', '2018-03-21 08:02:40', '2018-03-21 09:49:26', 'tgvughi', NULL, 0, NULL),
(28, 'langshop', 'uninstalled', 'wishlist-8.myshopify.com', 'yan.developer.it@gmail.com', '2018-03-26 05:25:17', '2018-03-28 05:25:17', NULL, NULL, 0, NULL),
(30, 'imobile', 'installed', 'test-imobile-ttitan-2.myshopify.com', 'rodion.bezruk@devit-team.com', '2018-03-30 10:46:43', '2018-03-30 10:46:43', NULL, NULL, 0, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Base Admin', 'admin@devit-team.com', '$2y$10$xc/KBWIVAy/SsYlEbI7pTeG2uvoSfxK1byiO5bN6rd6/xIpBfRKI2', '2017-12-04 14:39:34', '2017-12-04 14:39:34', NULL),
(2, 'Taras Prokofiev', 'taras.developer.it@gmail.com', '$2y$10$5xAq1rwqrNUHehGDEEnOeuXWk.RkbOBsY/.V3lwyzE5YH.EuZzAMK', '2018-03-17 18:21:19', '2018-03-17 19:08:26', NULL),
(3, 'Rodion Bezruk', 'rodion.bezruk@devit-team.com', '$2y$10$NzdPQtx4MDjy2MJUd54BIelMXIwLrzfT7OiLVElg8Zpi1jpw93reK', '2018-03-18 09:51:32', '2018-03-18 09:51:32', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `users_roles`
--

CREATE TABLE IF NOT EXISTS `users_roles` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `users_id` int(10) unsigned NOT NULL,
  `roles_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `userId` (`users_id`,`roles_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `users_roles`
--

INSERT INTO `users_roles` (`id`, `users_id`, `roles_id`) VALUES
(1, 1, 1),
(3, 2, 4),
(4, 3, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `webhook_log`
--

CREATE TABLE IF NOT EXISTS `webhook_log` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `store` varchar(255) NOT NULL,
  `data` longtext,
  `type` varchar(255) NOT NULL,
  `app` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `store` (`store`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=33 ;

--
-- Дамп данных таблицы `webhook_log`
--

INSERT INTO `webhook_log` (`id`, `store`, `data`, `type`, `app`, `created_at`, `updated_at`) VALUES
(1, 'undefined', '{"filter":"","sort":{"direction":""},"page":0,"limit":25}', 'app/installed', 'imobile', '2018-03-19 12:38:23', '2018-03-19 12:38:23'),
(2, 'undefined', '{"filter":"","sort":{"direction":""},"page":0,"limit":25}', 'app/installed', 'imobile', '2018-03-19 12:38:25', '2018-03-19 12:38:25'),
(3, 'undefined', '{"filter":"","sort":{"direction":""},"page":0,"limit":25}', 'app/installed', 'imobile', '2018-03-19 12:38:26', '2018-03-19 12:38:26'),
(4, 'undefined', '{"filter":"","sort":{"direction":""},"page":0,"limit":25}', 'app/installed', 'imobile', '2018-03-19 12:38:27', '2018-03-19 12:38:27'),
(5, 'undefined', '{"filter":"","sort":{"direction":""},"page":0,"limit":25}', 'app/installed', 'imobile', '2018-03-19 12:38:27', '2018-03-19 12:38:27'),
(6, 'undefined', '{"filter":"","sort":{"direction":""},"page":0,"limit":25}', 'app/installed', 'imobile', '2018-03-19 12:38:27', '2018-03-19 12:38:27'),
(7, 'undefined', '{"filter":"","sort":{"direction":""},"page":0,"limit":25}', 'app/installed', 'imobile', '2018-03-19 12:38:28', '2018-03-19 12:38:28'),
(8, 'undefined', '{"filter":"","sort":{"direction":""},"page":0,"limit":25}', 'app/installed', 'imobile', '2018-03-19 12:38:28', '2018-03-19 12:38:28'),
(9, 'undefined', '{"filter":"","sort":{"direction":""},"page":0,"limit":25}', 'app/installed', 'imobile', '2018-03-19 12:38:29', '2018-03-19 12:38:29'),
(10, 'undefined', '{"filter":"","sort":{"direction":""},"page":0,"limit":25}', 'app/installed', 'imobile', '2018-03-19 12:38:29', '2018-03-19 12:38:29'),
(11, 'undefined', '{"filter":"","sort":{"direction":""},"page":0,"limit":25}', 'app/uninstalled', 'imobile', '2018-03-19 12:39:09', '2018-03-19 12:39:09'),
(12, 'undefined', '{"filter":"","sort":{"direction":""},"page":0,"limit":25}', 'app/uninstalled', 'imobile', '2018-03-19 12:39:10', '2018-03-19 12:39:10'),
(13, 'undefined', '{"filter":"","sort":{"direction":""},"page":0,"limit":25}', 'app/uninstalled', 'imobile', '2018-03-19 12:39:11', '2018-03-19 12:39:11'),
(14, 'undefined', '{"filter":"","sort":{"direction":""},"page":0,"limit":25}', 'app/uninstalled', 'imobile', '2018-03-19 12:39:12', '2018-03-19 12:39:12'),
(15, 'undefined', '{"filter":"","sort":{"direction":""},"page":0,"limit":25}', 'app/installed', 'imobile', '2018-03-19 12:43:45', '2018-03-19 12:43:45'),
(16, 'test-imobile-ttitan-1.myshopify.com', '', 'app/installed', 'imobile', '2018-03-19 12:56:44', '2018-03-19 12:56:44'),
(17, 'test-imobile-ttitan-1.myshopify.com', '{"store":"test-imobile-ttitan-1.myshopify.com","email":"rodion.bezruk@devit-team.com"}', 'app/uninstalled', 'imobile', '2018-03-19 13:00:50', '2018-03-19 13:00:50'),
(18, 'test-imobile-ttitan-1.myshopify.com', '{"store":"test-imobile-ttitan-1.myshopify.com","email":"rodion.bezruk@devit-team.com"}', 'app/installed', 'imobile', '2018-03-19 13:06:06', '2018-03-19 13:06:06'),
(19, 'undefined', '{"name":"langshopcut.myshopify.com","email":"rodion.bezruk@devit-team.com"}', 'app/uninstalled', 'imobile', '2018-03-21 07:54:09', '2018-03-21 07:54:09'),
(20, 'langshopcut.myshopify.com', '{"store":"langshopcut.myshopify.com","email":"rodion.bezruk@devit-team.com"}', 'app/uninstalled', 'imobile', '2018-03-21 08:02:12', '2018-03-21 08:02:12'),
(21, 'langshopcut.myshopify.com', '{"store":"langshopcut.myshopify.com","email":"rodion.bezruk@devit-team.com"}', 'app/uninstalled', 'imobile', '2018-03-21 08:02:20', '2018-03-21 08:02:20'),
(22, 'langshopcut.myshopify.com1', '{"store":"langshopcut.myshopify.com1","email":"rodion.bezruk@devit-team.com"}', 'app/uninstalled', 'imobile', '2018-03-21 08:02:28', '2018-03-21 08:02:28'),
(23, 'langshopcut.myshopify.com1', '{"store":"langshopcut.myshopify.com1","email":"rodion.bezruk@devit-team.com"}', 'app/uninstalled', 'imobile', '2018-03-21 08:02:29', '2018-03-21 08:02:29'),
(24, 'langshopcut.myshopify.com1', '{"store":"langshopcut.myshopify.com1","email":"rodion.bezruk@devit-team.com"}', 'app/uninstalled', 'imobile', '2018-03-21 08:02:33', '2018-03-21 08:02:33'),
(25, 'langshopcut.myshopify.com1', '{"store":"langshopcut.myshopify.com1","email":"rodion.bezruk@devit-team.com"}', 'app/installed', 'imobile', '2018-03-21 08:02:38', '2018-03-21 08:02:38'),
(26, 'langshopcut.myshopify.com1', '{"store":"langshopcut.myshopify.com1","email":"rodion.bezruk@devit-team.com"}', 'app/installed', 'imobile', '2018-03-21 08:02:39', '2018-03-21 08:02:39'),
(27, 'langshopcut.myshopify.com1', '{"store":"langshopcut.myshopify.com1","email":"rodion.bezruk@devit-team.com"}', 'app/installed', 'imobile', '2018-03-21 08:02:40', '2018-03-21 08:02:40'),
(28, 'test-imobile-ttitan-1.myshopify.com', '{"store":"test-imobile-ttitan-1.myshopify.com","email":"rodion.bezruk@devit-team.com"}', 'app/installed', 'imobile', '2018-03-21 10:02:25', '2018-03-21 10:02:25'),
(29, 'test-imobile-ttitan-1.myshopify.com', '{"store":"test-imobile-ttitan-1.myshopify.com","email":"rodion.bezruk@devit-team.com"}', 'app/installed', 'imobile', '2018-03-21 10:06:40', '2018-03-21 10:06:40'),
(30, 'wishlist-8.myshopify.com', '{"store":"wishlist-8.myshopify.com","email":"yan.developer.it@gmail.com"}', 'app/uninstalled', 'langshop', '2018-03-28 05:25:17', '2018-03-28 05:25:17'),
(31, 'wishlist-8.myshopify.com', '{"store":"wishlist-8.myshopify.com","email":"yan.developer.it@gmail.com"}', 'app/installed', 'langshop', '2018-03-28 05:25:34', '2018-03-28 05:25:34'),
(32, 'test-imobile-ttitan-2.myshopify.com', '{"store":"test-imobile-ttitan-2.myshopify.com","email":"rodion.bezruk@devit-team.com"}', 'app/installed', 'imobile', '2018-03-30 10:46:43', '2018-03-30 10:46:43');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
