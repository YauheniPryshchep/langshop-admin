<?php

class Event1
{
    private $data;
    private $conditions = [];

    public function __construct(array $data)
    {
        $this->data = $data;
    }

    public function addCondition(Condition1 $condition) {
        $this->conditions[] = $condition;
    }

    public function checkConditions() {
        foreach ($this->conditions as $condition) {
            if($condition->isPassed($this->data)) {
                $condition->onPassed($this->data);
            } else {
                $condition->onFailed($this->data);
            }
        }
    }
}

class Event2
{

}

class EventFactory
{
    public static function make(string $slug)
    {
        return new Event1();
    }
}

class Condition1
{
    public function __construct(array $data)
    {
        // validate

    }

    public function addAction(Action1 $action) {
        $this->
    }
}

class Condition2
{
    public function __construct(array $data)
    {
        throw new Error(); // if invalid data
    }
}

class ConditionFactory
{
    public static function make(string $slug, array $data)
    {
        return new Condition2($data);
    }
}

class Action1
{
    public function __construct(array $data)
    {
        // validate
    }
}

class Action2
{
    public function __construct(array $data)
    {
        // validate
    }
}

class ActionFactory
{
    public static function make(string $slug)
    {
        return new Action2();
    }
}

