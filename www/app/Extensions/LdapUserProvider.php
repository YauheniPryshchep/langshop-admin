<?php

namespace App\Extensions;

use Adldap\Laravel\Facades\Adldap;
use Adldap\Laravel\Facades\Resolver;
use Adldap\Models\User as LdapUser;
use Adldap\Query\Builder;
use App\Models\User;
use Illuminate\Auth\EloquentUserProvider;
use Illuminate\Contracts\Auth\Authenticatable as UserContract;
use Illuminate\Support\Str;

class LdapUserProvider extends EloquentUserProvider
{
    /**
     * Retrieve a user by the given credentials.
     *
     * @param array $credentials
     * @return UserContract|null
     */
    public function retrieveByCredentials(array $credentials)
    {
        if (empty($credentials) ||
            (count($credentials) === 1 &&
                array_key_exists('password', $credentials))) {

            return null;
        }

        /**
         * @var Builder $query
         */
        $query = Adldap::search()->users();

        foreach ($credentials as $key => $value) {

            if (Str::contains($key, 'password')) {
                continue;
            }

            $query->orWhere('uid', '=', $value)
                ->orWhere('mail', '=', $value);
        }

        /**
         * @var LdapUser $ldapUser
         */
        $ldapUser = $query->select(['cn', 'uid', 'mail', 'jpegphoto', 'memberOf'])->first();
        if (!$ldapUser) {
            return null;
        }

        return $this->toModel($ldapUser);
    }

    /**
     * Validate a user against the given credentials.
     *
     * @param UserContract $user
     * @param array $credentials
     * @return bool
     */
    public function validateCredentials(UserContract $user, array $credentials)
    {
        if (empty($credentials['password'])) {
            return false;
        }

        return Resolver::authenticate($user->ldap, ['password' => $credentials['password']]);
    }

    /**
     * @param LdapUser $ldapUser
     * @return User
     */
    private function toModel(LdapUser $ldapUser)
    {
        $query = $this->createModel()->newQuery();
        $email = $ldapUser->getEmail();
        $name  = $ldapUser->getCommonName();
        $photo = $ldapUser->getJpegPhoto();

        /**
         * @var User $model
         */
        $model = $query->where('email', $email)->first();
        if (!$model) {
            $model        = $this->createModel();
            $model->email = $email;
            $model->name  = $name;
            $model->photo = $photo;
            $model->save();

            $model->roles()->sync([2]);
        } elseif(empty($model->photo) && !empty($photo)) {
            $model->photo = $photo;
            $model->save();
        }

        $model->setLdapUser($ldapUser);

        return $model;
    }
}