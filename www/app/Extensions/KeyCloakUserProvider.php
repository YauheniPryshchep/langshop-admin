<?php

namespace App\Extensions;

use App\Models\User;
use App\Services\KeyCloakService;
use Exception;
use Illuminate\Auth\EloquentUserProvider;
use Illuminate\Contracts\Auth\Authenticatable as UserContract;

class KeyCloakUserProvider extends EloquentUserProvider
{
    /**
     * Retrieve a user by the given credentials.
     *
     * @param array $credentials
     * @return UserContract|null
     * @throws Exception
     */
    public function retrieveByCredentials(array $credentials)
    {
        $service = new KeyCloakService();
        if (empty($credentials) ||
            (count($credentials) === 1 &&
                !array_key_exists('code', $credentials))) {

            return null;
        }

        $token = $service->getAccessToken($credentials['code']);
        $userInfo = $service->getUserProfile($token);

        if (!count($userInfo)) {
            return null;
        }

        return $this->toModel($userInfo);
    }

    /**
     * Validate a user against the given credentials.
     *
     * @param UserContract $user
     * @param array $credentials
     * @return bool
     */
    public function validateCredentials(UserContract $user, array $credentials)
    {
        return true;
    }

    /**
     * @param $userInfo
     * @return User
     */
    private function toModel(array $userInfo)
    {
        $newModel = $this->createModel();

        $query = ($newModel instanceof User)
            ? $newModel->withTrashed()
            : $newModel->newQuery();

        $email = $userInfo['email'];
        $name = $userInfo['name'];
        $photo = $userInfo['picture'] ?? "";

        /**
         * @var User $model
         */
        $model = $query
            ->where('email', $email)
            ->first();

        if (!$model) {
            $model = $this->createModel();
            $model->email = $email;
            $model->name = $name;
            $model->photo = $photo;
            $model->save();

            $model->roles()->sync([2]);
        }   else if(!empty($photo)) {
            $model->photo = $photo;
            $model->save();
        }

        if(!empty($model->deleted_at))
        {
            $model->deleted_at = null;
            $model->save();
        }

        return $model;
    }
}