<?php

namespace App\Accessors;

use JsonSerializable;
use Carbon\Carbon;

class MailGunTemplate implements JsonSerializable
{

    /**
     *
     * @var string 
     */
    protected $name;

    /**
     *
     * @var string 
     */
    protected $description;

    /**
     *
     * @var string 
     */
    protected $createdAt;

    /**
     *
     * @var string 
     */
    protected $id;

    public function __construct(array $template)
    {
        $this->name = $template['name'];
        $this->description = $template['description'];
        $this->setCreatedAt($template['createdAt']);
        $this->id = $template['id'];
    }

    /**
     * Date format converter
     * @param string $createdAt
     */
    public function setCreatedAt(string $createdAt)
    {
        $this->createdAt = Carbon::parse($createdAt)->format("Y-m-d H:i:s");
    }

    /**
     * 
     * @return JsonResponse
     */
    public function jsonSerialize()
    {
        return [
            'name' => $this->name,
            'description' => $this->description,
            'createdAt' => $this->createdAt,
            'id' => $this->id
        ];
    }

}
