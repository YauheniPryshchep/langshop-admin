<?php

namespace App\Helpers;

class Arrays
{
    /**
     * @param string|null $json
     * @return array
     */
    public static function fromJson(?string $json): array
    {
        return \GuzzleHttp\json_decode($json, true);
    }

    /**
     * @param array $a
     * @param array $b
     * @return bool
     */
    public static function equal(array $a, array $b): bool
    {
        return (
            count($a) == count($b)
            && array_diff($a, $b) === array_diff($b, $a)
        );
    }

    /**
     * @param array $a
     * @param array $b
     * @return array
     */
    public static function diff(array $a, array $b): array
    {
        return array_values(array_diff($a, $b));
    }

    /**
     * @param array $a
     * @param array $b
     * @return array
     */
    public static function intersect(array $a, array $b): array
    {
        return array_values(array_intersect($a, $b));
    }

    /**
     * @param array $a
     * @return array
     */
    public static function unique(array $a): array
    {
        return array_values(array_unique($a));
    }
}