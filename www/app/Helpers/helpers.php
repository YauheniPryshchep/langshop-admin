<?php
if (!function_exists('config_path')) {
    /**
     * Get the configuration path.
     *
     * @param string $path
     * @return string
     */
    function config_path($path = '')
    {
        return app()->basePath() . '/config' . ($path ? '/' . $path : $path);
    }
}

if (!function_exists('email_view_path')) {
    /**
     * Get the configuration path.
     *
     * @param string $path
     * @return string
     */
    function email_view_path($path = '')
    {
        return app()->basePath() . '/resources/views/mailTemplates' . ($path ? '/' . ltrim($path, '/') : $path);
    }
}
