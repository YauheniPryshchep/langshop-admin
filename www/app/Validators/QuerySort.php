<?php

namespace App\Validators;

use Illuminate\Validation\Rule;

class QuerySort extends AbstractValidator
{
    /**
     * @OA\Schema(
     *      schema="QuerySort",
     *      type="object",
     *      @OA\Property(property="key", type="string"),
     *      @OA\Property(property="direction", type="string", enum={"ASC","DESC"})
     * )
     */

    const SORT_DIR_ASC  = 'ASC';
    const SORT_DIR_DESC = 'DESC';

    /**
     * @var string|null
     */
    private $sortKey;

    /**
     * @var string|null
     */
    private $sortDir;

    /**
     * @var array
     */
    private $rules;

    /**
     * QuerySort constructor.
     * @param array $data
     * @param array $sortKeys
     */
    public function __construct(array $data, array $sortKeys)
    {
        $this->rules = [
            'sort.key'       => [
                'string',
                Rule::in($sortKeys)
            ],
            'sort.direction' => [
                'string',
                Rule::in([
                    self::SORT_DIR_DESC,
                    self::SORT_DIR_ASC
                ])
            ]
        ];

        parent::__construct($data);

        $data = $this->validate();

        if (empty($data['sort'])) {
            return;
        }

        $this->sortKey = $data['sort']['key'] ?? null;
        $this->sortDir = $data['sort']['direction'] ?? null;
    }

    /**
     * @return array
     */
    protected function rules(): array
    {
        return $this->rules;
    }

    /**
     * @return null|string
     */
    public function getSortKey(): ?string
    {
        return $this->sortKey;
    }

    /**
     * @return null|string
     */
    public function getSortDir(): ?string
    {
        return $this->sortDir;
    }
}