<?php

namespace App\Validators;


use App\Models\Campaign;
use App\Validations\MailGunTemplate;
use Illuminate\Validation\Rule;

class CampaignUpdateItem extends AbstractValidator
{

    /**
     * @var array
     */
    private $input;

    /**
     * @var array
     */
    private $rules;

    /**
     * @var array
     */
    private $params;

    /**
     * CampaignItem constructor.
     * @param array $params
     * @param array $rules
     */

    const LESS = 'less';
    const GREATER = 'greater';
    const EQUAL_OR_LESS = 'equal_or_less';
    const EQUAL_OR_GREATER = 'equal_or_greater';
    const EQUAL = 'equal';
    const CONTAINS = 'contains';
    const IN = 'in';

    public function __construct(array $params, array $rules = [])
    {
        $this->params = $params;
        $this->rules = [
            'title'                                  => 'string|max:255',
            'description'                            => 'string|max:255',
            'subject'                                => 'string|max:255',
            'status'                                 => 'integer',
            'from'                                   => 'nullable|email|max:255',
            'sender'                                 => 'nullable|string|max:255',
            'reply_to'                               => 'nullable|email|max:255',
            'type'                                   => [
                'string',
                Rule::in([
                    Campaign::TYPE_EMAIL
                ])
            ],
            'template'                               => [
                'string',
                MailGunTemplate::RULE
            ],
            'author_id'                              => 'integer',
            'start_at'                               => 'nullable|date_format:Y-m-d H:i:s',
            "recipients_filter"                      => 'nullable',
            'recipients_filter.filters'              => 'array',
            'recipients_filter.limit'                => 'integer|nullable',
            'recipients_filter.filters.*.key'        => [
                'required_with:recipients_filter.filters',
                'string'
            ],
            'recipients_filter.filters.*.value'      => [
                'nullable'
            ],
            'recipients_filter.filters.*.comparator' => [
                'string'
            ]
        ];

        parent::__construct($params);
        $this->input = $this->validate();
    }

    /**
     * @return array
     */
    protected function rules(): array
    {
        return $this->rules;
    }

    /**
     * @return array
     */
    public function input(): array
    {
        return $this->input;
    }
}
