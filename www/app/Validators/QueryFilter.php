<?php

namespace App\Validators;

use App\Providers\ValidationServiceProvider;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use phpDocumentor\Reflection\Types\Integer;

class QueryFilter extends AbstractValidator
{
    /**
     * @OA\Schema(
     *      schema="QueryFilter",
     *      type="object",
     *      @OA\Property(property="key", type="string"),
     *      @OA\Property(property="value", type="string"),
     *      @OA\Property(property="comparator", type="string", enum={"less", "greater", "equal_or_less", "equal_or_greater", "equal", "contains", "in"})
     * )
     */

    const LESS = 'less';
    const GREATER = 'greater';
    const EQUAL_OR_LESS = 'equal_or_less';
    const EQUAL_OR_GREATER = 'equal_or_greater';
    const EQUAL = 'equal';
    const CONTAINS = 'contains';
    const IN = 'in';

    const EXCLUDED = [
        'sourceId'
    ];

    const PARSED_TO_INT = [
        'sourceId'
    ];

    /**
     * @var array
     */
    private $params = [];

    /**
     * @var array
     */
    private $rules = [];
    /**
     * @var array
     */
    private $input = [];

    /**
     * QueryFilter constructor.
     * @param array $data
     * @param array $rules
     */
    public function __construct(array $data = [], array $rules = [])
    {
        if (empty($data['filters'])) {
            return;
        }

        $this->rules = array_merge([
            'filters'              => 'array|filled',
            'filters.*.key'        => [
                'required_with:filters',
                'string',
                Rule::in(array_keys($rules))
            ],
            'filters.*.value'      => [
                'nullable'
            ],
            'filters.*.comparator' => [
                'string',
                Rule::in([
                    self::LESS,
                    self::GREATER,
                    self::EQUAL_OR_GREATER,
                    self::EQUAL_OR_LESS,
                    self::EQUAL,
                    self::CONTAINS,
                    self::IN
                ])
            ]
        ]);

        parent::__construct($data);

        $data = $this->validate();
        $this->input = $this->validate();

        $data['filters'] = array_map(function ($filter) {
            if (!array_key_exists('value', $filter)) {
                $filter['value'] = null;
            }

            return $filter;

        }, $data['filters']);

        $this->validateFiltersValues(array_combine(array_column($data['filters'], 'key'), array_column($data['filters'], 'value')), $rules);

        $this->create($data['filters']);
    }

    /**
     * @param array $data
     * @param array $rules
     */
    protected function validateFiltersValues(array $data, array $rules)
    {
        $this->getValidator($data, $rules)->validate();
    }

    /**
     * @return array
     */
    public function getInput(): array
    {
        return $this->input;
    }

    /**
     * @return array
     */
    protected function rules(): array
    {
        return $this->rules;
    }

    /**
     * @param array $filters
     */
    public function create(array $filters): void
    {
        foreach ($filters as $filter) {
            $this->set($filter['key'], $filter['value'], !empty($filter['comparator']) ? $filter['comparator'] : self::EQUAL);
        }
    }

    /**
     * @param string $key
     * @param mixed $value
     * @param string $comparator
     */
    public function set(string $key, $value, string $comparator = self::EQUAL): void
    {
        $this->params[] = [
            'key'        => $key,
            'value'      => $value,
            'comparator' => $comparator
        ];
    }

    /**
     * @return array
     */
    public function getQueryArray(): array
    {
        $params = [];

        foreach ($this->params as $param) {
            $params[$param['key']] = $param['value'];
        }

        return $params;
    }

    /**
     * @param Builder $query
     * @param array $relationships
     * @return Builder
     */
    public function createDatabaseQuery(Builder $query, array $relationships = []): Builder
    {
        if (empty($this->params)) {
            return $query;
        }

        foreach ($this->params as $param) {
            $parts = explode('.', $param['key']);

            switch (count($parts)) {

                case 2 and $parts[0] === 'has':
                    $query->has($parts[1]);
                    break;

                case 2:
                    $param['key'] = implode(".", [
                            $relationships[$parts[0]] ?? $parts[0],
                            $parts[1]]
                    );

                    $query->whereHas($parts[0], function (Builder $q) use ($param) {
                        $this->setFilter($q, $param);
                    });

                    break;

                case 1:
                    $this->setFilter($query, $param);

                    break;

            }
        }

        return $query;
    }

    /**
     * @param Builder $query
     * @param array $param
     */
    private function setFilter(Builder $query, array $param)
    {
        $param['key'] = !in_array($param['key'], self::EXCLUDED)
            ? Str::snake($param['key'])
            : $param['key'];
        $param['value'] = in_array($param['key'],self::PARSED_TO_INT)
            ? intval($param['value'])
            : $param['value'];

        switch ($param['comparator']) {
            case self::CONTAINS:

                $query->where($param['key'], 'like', '%' . $param['value'] . '%');

                break;

            case self::EQUAL:
                $query->where($param['key'], '=', $param['value']);

                break;

            case self::EQUAL_OR_LESS:

                $query->where($param['key'], '<=', $param['value']);

                break;

            case self::EQUAL_OR_GREATER:

                $query->where($param['key'], '>=', $param['value']);

                break;

            case self::LESS:

                $query->where($param['key'], '<', $param['value']);

                break;

            case self::GREATER:

                $query->where($param['key'], '>', $param['value']);

                break;

            case self::IN:

                $query->whereIn($param['key'], explode(',', $param['value']));

                break;

        }
    }

    /**
     * @return string
     */
    public function getQueryString(): string
    {
        if (empty($this->params)) {
            return '';
        }

        $query = [];

        foreach ($this->params as $param) {
            switch ($param['comparator']) {
                case self::CONTAINS:

                    $query[] = "({$param['key']}:{$param['value']}*)";

                    break;

                case self::EQUAL:

                    $query[] = "({$param['key']}:'{$param['value']}')";

                    break;

                case self::EQUAL_OR_LESS:

                    $query[] = "({$param['key']}:<={$param['value']})";

                    break;

                case self::EQUAL_OR_GREATER:

                    $query[] = "({$param['key']}:>={$param['value']})";

                    break;

                case self::LESS:

                    $query[] = "({$param['key']}:<{$param['value']})";

                    break;

                case self::GREATER:

                    $query[] = "({$param['key']}:>{$param['value']})";

                    break;

            }
        }

        return implode(' AND ', $query);
    }
}
