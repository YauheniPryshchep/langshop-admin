<?php

namespace App\Validators;

use App\Models\Vocabulary;
use Illuminate\Validation\Rule;

class VocabularyUpdateItem extends AbstractValidator
{

    /**
     * @var array
     */
    private $input;

    /**
     * @var array
     */
    private $rules;

    /**
     * @var array
     */
    private $params;

    /**
     * VocabularyUpdateItem constructor.
     * @param array $params
     * @param array $rules
     */
    public function __construct(array $params, array $rules = [])
    {
        $this->params = $params;
        $this->rules = [
            'original'   => 'string',
            'translated' => 'string',
            'from'       => 'string',
            'to'         => 'string',
            'source'     => [
                'string',
                Rule::in([
                    Vocabulary::SOURCE_AGENCY,
                    Vocabulary::SOURCE_MACHINE,
                    Vocabulary::SOURCE_HUMAN,
                ])
            ],
            'sourceId'   => 'integer',
        ];

        parent::__construct($params);
        $this->input = $this->validate();
    }

    /**
     * @return array
     */
    protected function rules(): array
    {
        return $this->rules;
    }

    /**
     * @return array
     */
    public function input(): array
    {
        return $this->input;
    }
}
