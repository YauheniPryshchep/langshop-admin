<?php

namespace App\Validators;

use App\Models\Coupon;
use App\Models\Plan;
use Illuminate\Validation\Rule;

class CouponsUpdateItem extends AbstractValidator
{

    /**
     * @var array
     */
    private $input;

    /**
     * @var array
     */
    private $rules;

    /**
     * @var array
     */
    private $params;

    /**
     * CouponsUpdateItem constructor.
     * @param array $params
     * @param array $rules
     */
    public function __construct(array $params, array $rules = [])
    {
        $this->params = $params;
        $this->rules = [
            'name'           => 'string',
            'discount_type'  => [
                'integer',
                Rule::in([
                    Coupon::TYPE_FIXED,
                    Coupon::TYPE_PERCENT
                ])
            ],
            'discount_value' => 'integer',
            'usage_limit'    => 'integer',
            'reusable'       => 'boolean',
            'plans'          => [
                'nullable',
            ],
            'status'         => [
                'integer',
                Rule::in([
                    Coupon::STATUS_DRAFT,
                    Coupon::STATUS_ACTIVE,
                    Coupon::STATUS_BLOCKED
                ])
            ],
            'start_at'       => 'nullable|date_format:Y-m-d H:i:s',
            'end_at'         => 'nullable|date_format:Y-m-d H:i:s'
        ];

        parent::__construct($params);
        $this->input = $this->validate();
    }

    /**
     * @return array
     */
    protected function rules(): array
    {
        return $this->rules;
    }

    /**
     * @return array
     */
    public function input(): array
    {
        return $this->input;
    }
}
