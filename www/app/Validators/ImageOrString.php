<?php

namespace App\Validators;

use Illuminate\Http\UploadedFile;
use Illuminate\Validation\Validator;

class ImageOrString
{
    const IMAGE_MAX_SIZE_BYTES     = 5000000; // 5 mb

    const IMAGE_MIME_TYPES = [
        'image/jpeg',
        'image/png',
        'image/gif'
    ];

    /**
     * @param Validator $validator
     * @param string $message
     * @return bool
     */
    private function fail(Validator $validator, string $message)
    {
        $validator->setCustomMessages([
            "image_or_string" => $message
        ]);

        return false;
    }

    /**
     * @param $attribute
     * @param $value
     * @param $parameters
     * @param Validator $validator
     * @return bool
     */
    public function validate($attribute, $value, $parameters, Validator $validator)
    {
        if (empty($value)) {
            return true;
        }

        if (is_string($value)) {
            return true;
        }

        if ($value instanceof UploadedFile) {
            if ($value->getSize() > self::IMAGE_MAX_SIZE_BYTES) {
                return $this->fail($validator, $attribute . ' to large. Must be less then 1MB.');
            }

            if (!$mimeType = $value->getMimeType()) {
                return $this->fail($validator, $attribute . ' file have unknown mime type.');
            }

            if (!in_array($mimeType, self::IMAGE_MIME_TYPES)) {
                return $this->fail($validator, $attribute . ' file mime type is invalid. Allowed ' . implode(", ", self::IMAGE_MIME_TYPES) . '.');
            }

            return true;
        }

        return $this->fail($validator, $attribute . ' must be a string or file.');
    }
}