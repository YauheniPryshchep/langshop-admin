<?php

namespace App\Validators;


use App\Models\StoreEvents;
use Illuminate\Validation\Rule;

class StoreEventsValidators extends AbstractValidator
{

    /**
     * @var array
     */
    private $input;

    /**
     * @var array
     */
    private $rules;

    /**
     * @var array
     */
    private $params;

    /**
     * CampaignItem constructor.
     * @param array $params
     * @param array $rules
     */

    public function __construct(array $params, array $rules = [])
    {
        $this->params = $params;
        $this->rules = [
            'domain'     => 'required|string|max:255',
            'type'       => [
                'required',
                'string',
                Rule::in([
                    StoreEvents::STORE_ADD_TO_CAMPAIGN,
                    StoreEvents::UNSUBSCRIBED_FROM_MAILING,
                    StoreEvents::ONE_TIME_PAYMENT_CHARGE,
                    StoreEvents::ASSIGN_REFERRAL,
                    StoreEvents::CANCEL_REFERRAL,
                    StoreEvents::DECLINED_REFERRAL,
                ])
            ],
            'event_data' => [
                'nullable',
                'array',
            ],
            'timestamp' => "required|numeric"
        ];

        parent::__construct($params);
        $this->input = $this->validate();
    }

    /**
     * @return array
     */
    protected function rules(): array
    {
        return $this->rules;
    }

    /**
     * @return array
     */
    public function input(): array
    {
        return $this->input;
    }
}
