<?php

namespace App\Validators;


use App\Models\Campaign;
use App\Services\Filters\Resolver;
use App\Services\RecipientsFilterService;
use App\Validations\MailGunTemplate;
use Illuminate\Validation\Rule;

class ActionValidator extends AbstractValidator
{

    /**
     * @var array
     */
    private $input;

    /**
     * @var array
     */
    private $rules;

    /**
     * @var array
     */
    private $params;


    public function __construct(array $params, array $rules = [])
    {
        $this->params = $params;
        $this->rules = $rules;

        parent::__construct($params);
        $this->input = $this->validate();
    }

    /**
     * @return array
     */
    protected function rules(): array
    {
        return $this->rules;
    }

    /**
     * @return array
     */
    public function input(): array
    {
        return $this->input;
    }
}
