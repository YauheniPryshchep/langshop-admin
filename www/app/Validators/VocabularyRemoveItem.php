<?php

namespace App\Validators;

use App\Models\Vocabulary;
use Illuminate\Validation\Rule;

class VocabularyRemoveItem extends AbstractValidator
{

    /**
     * @var array
     */
    private $input;

    /**
     * @var array
     */
    private $rules;

    /**
     * @var array
     */
    private $params;


    /**
     * VocabularyCreateItem constructor.
     * @param array $params
     * @param array $rules
     */
    public function __construct(array $params, array $rules = [])
    {
        $this->params = $params;
        $this->rules = [
            'from'       => 'string|required',
            'to'         => 'string|required',

        ];

        parent::__construct($params);
        $this->input = $this->validate();
    }

    /**
     * @return array
     */
    protected function rules(): array
    {
        return $this->rules;
    }

    /**
     * @return array
     */
    public function input(): array
    {
        return $this->input;
    }
}
