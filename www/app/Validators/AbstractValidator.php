<?php
namespace App\Validators;

use Illuminate\Support\Facades\Validator;

abstract class AbstractValidator
{
    /**
     * @var Validator
     */
    protected $validator;

    /**
     * @var array
     */
    private $rules;

    /**
     * AbstractValidator constructor.
     * @param array $data

     */

    public function __construct(array $data)
    {
        $this->rules = $this->rules();
        $this->validator = $this->getValidator($data);
    }

    /**
     * @return array
     */
    abstract protected function rules(): array;

    /**
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator|Validator
     */
    protected function getValidator(array $data)
    {
        return Validator::make($data, $this->rules);
    }

    /**
     * @return array
     */
    protected function validate(): array
    {
        return $this->validator->validate();
    }
}