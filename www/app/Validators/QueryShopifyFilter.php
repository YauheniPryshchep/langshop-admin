<?php

namespace App\Validators;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;

class  QueryShopifyFilter extends AbstractValidator
{
    /**
     * @var array
     */
    private $rules = [];

    /**
     * @var array
     */
    private $input;

    /**
     * QueryFilter constructor.
     * @param array $data
     * @param array $rules
     */
    public function __construct(array $data = [], array $rules = [])
    {
        $this->rules =$rules;

        parent::__construct($data);

        $this->input = $this->validate();
    }

    /**
     * @return array
     */
    public function getInput(): array
    {
        return $this->input;
    }

    /**
     * @return array
     */
    protected function rules(): array
    {
        return $this->rules;
    }

    /**
     * @return string
     */
    public function getQueryString(): string
    {
        if (empty($this->input)) {
            return '';
        }

        $query = [];

        foreach ($this->input as $key => $param) {
            $query[] = "({$key}:{$param})";
        }

        return implode(' AND ', $query);
    }
}
