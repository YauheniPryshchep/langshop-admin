<?php

namespace App\Validators;

use App\Models\Coupon;
use App\Models\Plan;
use Illuminate\Validation\Rule;

class CouponsCreateItem extends AbstractValidator
{

    /**
     * @var array
     */
    private $input;

    /**
     * @var array
     */
    private $rules;

    /**
     * @var array
     */
    private $params;

    /**
     * CouponsUpdateItem constructor.
     * @param array $params
     * @param array $rules
     */
    public function __construct(array $params, array $rules = [])
    {
        $this->params = $params;
        $this->rules = [
            'name'           => 'required|string',
            'discount_type'  => [
                'required',
                'integer',
                Rule::in([
                    Coupon::TYPE_FIXED,
                    Coupon::TYPE_PERCENT
                ])
            ],
            'discount_value' => 'required|integer',
            'usage_limit'    => 'required|integer',
            'reusable'       => 'required|boolean',
            'plans'          => [
                'nullable',
            ],
            'status'         => [
                'required',
                'integer',
                Rule::in([
                    Coupon::STATUS_DRAFT,
                    Coupon::STATUS_ACTIVE,
                    Coupon::STATUS_BLOCKED
                ])
            ],
            'start_at'       => 'date_format:Y-m-d H:i:s',
            'end_at'         => 'nullable|date_format:Y-m-d H:i:s'
        ];

        parent::__construct($params);
        $this->input = $this->validate();
    }

    /**
     * @return array
     */
    protected function rules(): array
    {
        return $this->rules;
    }

    /**
     * @return array
     */
    public function input(): array
    {
        return $this->input;
    }
}
