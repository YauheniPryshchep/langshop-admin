<?php

namespace App\Validators;

use Illuminate\Validation\ValidationException;

class QueryCursor extends AbstractValidator
{
    /**
     * @var array
     */
    private $rules = [
        'limit'  => 'integer|min:1|max:250|filled',
        'after'  => 'string|filled',
        'before' => 'string|filled',
    ];

    /**
     * @OA\Parameter(
     *      parameter="query_cursor_after",
     *      name="after",
     *      description="Show items after cursor",
     *      @OA\Schema(
     *          type="string"
     *      ),
     *      in="query"
     * )
     *
     * @var string|null
     */
    private $after;

    /**
     * @OA\Parameter(
     *      parameter="query_cursor_before",
     *      name="before",
     *      description="Show items before cursor",
     *      @OA\Schema(
     *          type="string"
     *      ),
     *      in="query"
     * )
     *
     * @var string|null
     */
    private $before;

    /**
     * @OA\Parameter(
     *      parameter="query_cursor_limit",
     *      name="limit",
     *      description="Items per page",
     *      @OA\Schema(
     *          type="integer",
     *          minimum="1",
     *          maximum="250"
     *      ),
     *      in="query"
     * )
     *
     * @var int
     */
    private $limit = 50;

    /**
     * QueryCursor constructor.
     * @param array $data
     * @throws ValidationException
     */
    public function __construct(array $data = [])
    {
        parent::__construct($data);

        $this->create($this->validate());
    }

    /**
     * @return array
     */
    protected function rules(): array
    {
        return $this->rules;
    }

    /**
     * @param array $data
     */
    public function create(array $data): void
    {
        if (isset($data['limit'])) {
            $this->limit = $data['limit'];
        }

        if (isset($data['after'])) {
            $this->after = $data['after'];
        } elseif (isset($data['before'])) {
            $this->before = $data['before'];
        }
    }

    /**
     * @return int
     */
    public function getLimit(): int
    {
        return $this->limit;
    }

    /**
     * @return null|string
     */
    public function getAfter(): ?string
    {
        return $this->after;
    }

    /**
     * @return null|string
     */
    public function getBefore(): ?string
    {
        return $this->before;
    }
}
