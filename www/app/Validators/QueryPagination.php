<?php

namespace App\Validators;

class QueryPagination extends AbstractValidator
{
    /**
     * @var array
     */
    private $rules = [
        'limit' => 'integer|min:1|max:1000|filled',
        'page'  => 'integer|min:1|filled',
    ];

    /**
     * @OA\Parameter(
     *      parameter="query_pagination_limit",
     *      name="limit",
     *      description="Items per page",
     *      @OA\Schema(
     *          type="integer",
     *          minimum="1"
     *      ),
     *      in="query"
     * )
     *
     * @var int
     */
    private $limit = 50;

    /**
     * @OA\Parameter(
     *      parameter="query_pagination_page",
     *      name="page",
     *      description="Page to show",
     *      @OA\Schema(
     *          type="integer",
     *          minimum="1"
     *      ),
     *      in="query"
     * )
     * @var int
     */
    private $page = 1;

    /**
     * QueryPagination constructor.
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        parent::__construct($data);

        $this->create($this->validate());
    }

    /**
     * @return array
     */
    protected function rules(): array
    {
        return $this->rules;
    }

    /**
     * @param array $data
     */
    public function create(array $data)
    {
        foreach (array_keys($this->rules) as $key) {
            if (!isset($data[$key])) {
                continue;
            }

            $this->$key = $data[$key];
        }
    }

    /**
     * @return int
     */
    public function getLimit(): int
    {
        return $this->limit;
    }

    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }
}