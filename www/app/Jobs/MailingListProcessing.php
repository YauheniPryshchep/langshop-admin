<?php

namespace App\Jobs;

use App\Services\MailingListProcessingService;
use App\Services\MailGunService;
use App\Models\CampaignDomain;
use App\Exceptions\CustomException;
use Exception;
use DB;
use App\Models\Campaign;

class MailingListProcessing extends Job
{

    /**
     *
     * @var integer 
     */
    public $campaignId;

    /**
     * @var string 
     */
    public $langshop;

    /**
     * @var string 
     */
    public $dashboard;

    /**
     * @var string 
     */
    public $credentials;

    /**
     * @var string 
     */
    public $url;

    /**
     * MailingListProcessing constructor.
     * @param int $campaignId
     */
    public function __construct(int $campaignId, string $credentials)
    {
        $this->campaignId = $campaignId;
        $this->credentials = $credentials;
        $this->url = 'lists';
        $this->langshop = config('database.connections.langshop.database');
        $this->dashboard = config('database.connections.dashboard.database');
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $campaign = Campaign::find($this->campaignId);
        $campaign->update(['status' => Campaign::STATUS_IN_PROGRESS]);
        $this->connections = config('mailgun.' . $this->credentials);
        $address = 'campaign-' . $this->campaignId . '@' . $this->connections['domain'];

        $mailingService = new MailingListProcessingService($this->credentials, $address);

        try {
            $mailingService->createList();
        } catch (Exception $e) {
            
        }
        $domains = CampaignDomain::where('campaign_id', $this->campaignId)->get('domain');

        $domainsEmails = DB::select('SELECT merged.emails, merged.domains FROM ('
                        . 'SELECT store_emails.value as emails, stores.name as domains FROM ' . $this->langshop . '.stores as stores '
                        . 'JOIN ' . $this->langshop . '.store_emails as store_emails '
                        . 'ON stores.id=store_emails.store_id UNION SELECT leads_emails.email, leads.shopify_domain '
                        . 'FROM ' . $this->dashboard . '.leads_emails as leads_emails '
                        . 'JOIN ' . $this->dashboard . '.leads as leads ON leads_emails.lead_id=leads.id) as merged WHERE merged.domains '
                        . 'IN ("' . implode('","', $domains->pluck("domain")->toArray()) . '")');

        $members = array_column($domainsEmails, 'emails');
        $mailingService->addMembers($members);

//        $emailSender = new MailGunService($this->credentials);
//        $emailSender->sendMail($address, $campaign->subject, $campaign->description, $campaign->templade_id);
        $campaign->update(['status' => Campaign::STATUS_DONE]);

        return;
    }

}
