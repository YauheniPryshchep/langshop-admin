<?php

namespace App\Jobs;

use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Database\Query\Builder;
use Illuminate\Database\Query\Grammars\MySqlGrammar;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ViewDnsIpReverse extends Job
{
    /**
     * @var int
     */
    private $page;

    /**
     * @var int
     */
    private $index;

    /**
     * @var string[]
     */
    private $range = [
        '23.227.38.32',
        '23.227.38.64',
        '23.227.38.65',
    ];

    /**
     * ViewDnsIpReverse constructor.
     * @param int $index
     * @param int $page
     */
    public function __construct(int $index, int $page)
    {
        $this->index = $index;
        $this->page = $page;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws GuzzleException
     */
    public function handle()
    {
        $client = new Client();

        if (!isset($this->range[$this->index])) {
            $this->updateJobStatus();
            return;
        }

        if ($this->attempts() > 2) {
            $this->updateJobStatus();
            return;
        }

        if (!$apiKey = $this->getApiKey()) {
            $this->updateJobStatus();
            return;
        }

        $response = $client->request(
            'GET',
            "https://api.viewdns.info/reverseip/",
            [
                'query' => [
                    'host'   => $this->range[$this->index],
                    'output' => 'json',
                    'apikey' => $apiKey,
                    'page'   => $this->page
                ]
            ]
        );


        $this->saveKeyUsage($apiKey);

        $data = json_decode((string)$response->getBody(), true);
        if (empty($data['response']['domain_count'])) {
            $this->updateJobStatus();
            return;
        }

        $pages = ceil($data['response']['domain_count'] / 10000);
        $this->saveDomains($data['response']['domains']);

        if ($this->page < $pages) {
            static::dispatch($this->index, $this->page + 1);
        } else {
            static::dispatch($this->index + 1);
        }
    }

    /**
     * @return string|null
     */
    private function getApiKey(): ?string
    {
        $apiKey = DB::table('viewdns_api_keys')
            ->select('key')
            ->where('requests', '>', 0)
            ->first();

        if (!$apiKey) {
            return null;
        }

        return $apiKey->key;
    }

    private function saveKeyUsage(string $key)
    {
        DB::table('viewdns_api_keys')
            ->where('key', $key)
            ->update([
                'requests' => DB::raw('requests - 1')
            ]);
    }

    /**
     * @param array $domains
     */
    private function saveDomains(array $domains)
    {
        if (empty($domains)) {
            return;
        }

        $insert = array_reduce($domains, function ($carry, $domain) {
            if (is_numeric(str_replace(".", "", $domain['name']))) {
                return $carry;
            }

            $carry[] = [
                'public_domain' => $domain['name']
            ];

            return $carry;
        }, []);

        if (empty($insert)) {
            return;
        }

        $queryGrammar = new class extends MySqlGrammar {
            /**
             * Compile an insert statement into SQL.
             *
             * @param Builder $query
             * @param array $values
             * @return string
             */
            public function compileInsert(Builder $query, array $values)
            {
                // Essentially we will force every insert to be treated as a batch insert which
                // simply makes creating the SQL easier for us since we can utilize the same
                // basic routine regardless of an amount of records given to us to insert.
                $table = $this->wrapTable($query->from);

                if (!is_array(reset($values))) {
                    $values = [$values];
                }

                $columns = $this->columnize(array_keys(reset($values)));

                // We need to build a list of parameter place-holders of values that are bound
                // to the query. Each insert should have the exact same amount of parameter
                // bindings so we will loop through the record and parameterize them all.
                $parameters = collect($values)->map(function ($record) {
                    return '(' . $this->parameterize($record) . ')';
                })->implode(', ');

                return "insert ignore into {$table} ({$columns}) values {$parameters}";
            }
        };

        DB::connection()->setQueryGrammar($queryGrammar);

        try {
            DB::table('leads')->insert($insert);
        } catch (Exception $e) {

        }
    }

    /**
     *
     */
    private function updateJobStatus()
    {
        DB::table('jobs_logger')
            ->where("job_type", self::class)
            ->update([
                "job_data" => json_encode([
                    "status" => 0
                ])
            ]);
    }

    /**
     * @param int $index
     * @param int $page
     */
    public static function dispatch(int $index = 0, int $page = 1)
    {
        $delay = config('worker.leads_search_execute_delay');

        dispatch(
            (new static($index, $page))
                ->onConnection('database')
                ->delay($delay)
        );
    }
}
