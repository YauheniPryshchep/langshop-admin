<?php

namespace App\Jobs;

use App\Models\Campaign;
use App\Models\CampaignProcess;
use App\Services\Filters\Resolver;
use Illuminate\Support\Facades\Log;

class CollectCampaignEmails extends Job implements CampaignJobInterface
{
    const MAX_RECIPIENTS_PER_REQUEST = 500;

    /**
     * @var Campaign
     */
    private $campaign;


    /**
     * CollectCampaignStores constructor.
     * @param Campaign $campaign
     */
    public function __construct(Campaign $campaign)
    {
        $this->campaign = $campaign;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        /**
         * @var $process CampaignProcess
         */
        $process = $this->createProcess([
            "campaign_id" => $this->campaign->id,
            "processor"   => Campaign::STATUS_COLLECT_EMAILS,
            "status"      => CampaignProcess::STATUS_DISPATCHING
        ]);

        $collectStoresProcess = CampaignProcess::query()
            ->where('campaign_id', $this->campaign->id)
            ->where('processor', Campaign::STATUS_COLLECT_STORES)
            ->first()
            ->toArray();

        $resolver = new Resolver([]);

        $emails = $this->getEmails($collectStoresProcess["result"], $resolver);

        $this->updateProcess($process, [
            "result" => json_encode($emails),
            "status" => CampaignProcess::STATUS_PROCESSED
        ]);

        $this->updateCampaignStatus();
    }

    /**
     * @param $domains
     * @param Resolver $resolver
     * @return array|mixed
     */
    private function getEmails($domains, Resolver $resolver)
    {
        $emails = [];

        if ($domains && count($domains) < self::MAX_RECIPIENTS_PER_REQUEST) {
            $result = $resolver->resolveEmails($domains);
            $emails = $result['items'];
        } else {
            $chunked = array_chunk($domains, self::MAX_RECIPIENTS_PER_REQUEST);
            for ($i = 0; $i < count($chunked); $i++) {
                $result = $resolver->resolveEmails($chunked[$i]);
                $emails = array_merge($emails, $result['items']);
            }
        }

        return $emails;
    }

    /**
     * @param Campaign $campaign
     */
    public static function dispatch(Campaign $campaign)
    {
        dispatch(
            (new static($campaign))
                ->onConnection('database')
        );
    }

    public function updateCampaignStatus()
    {
        $this->campaign->update([
            "status" => Campaign::STATUS_WAITING_TO_PREPARE_EMAIL_VALIDATES
        ]);
    }

    /**
     * @param CampaignProcess $process
     * @param array $attributes
     */
    public function updateProcess(CampaignProcess $process, array $attributes)
    {
        CampaignProcess::query()
            ->where("id", $process->id)
            ->update($attributes);
    }

    public function createProcess(array $attributes)
    {
        return CampaignProcess::query()
            ->create($attributes);
    }
}
