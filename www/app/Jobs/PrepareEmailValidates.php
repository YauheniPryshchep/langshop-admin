<?php

namespace App\Jobs;

use App\Models\Campaign;
use App\Models\CampaignProcess;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class PrepareEmailValidates extends Job implements CampaignJobInterface
{
    /**
     * @var Campaign
     */
    private $campaign;

    /**
     * CollectCampaignStores constructor.
     * @param Campaign $campaign
     */
    public function __construct(Campaign $campaign)
    {
        $this->campaign = $campaign;
    }


    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $collectEmailsProcess = CampaignProcess::query()
            ->where('campaign_id', $this->campaign->id)
            ->where('processor', Campaign::STATUS_COLLECT_EMAILS)
            ->first()
            ->toArray();

        $emails = $this->getUniqueEmails(
            $this->emailsToLowerCase(array_column($collectEmailsProcess['result'], "email"))
        );

        $chunks = $this->getChunckedEmails($emails);

        $this->createProcesses($chunks);

        $this->updateCampaignStatus();
    }

    /**
     * @param Campaign $campaign
     */
    public static function dispatch(Campaign $campaign)
    {
        dispatch(
            (new static($campaign))
                ->onConnection('database')
        );
    }

    public function updateCampaignStatus()
    {
        $this->campaign->update([
            "status" => Campaign::STATUS_VALIDATE_EMAILS
        ]);
    }

    /**
     * @param CampaignProcess $process
     * @param array $attributes
     */
    public function updateProcess(CampaignProcess $process, array $attributes)
    {
        CampaignProcess::query()
            ->where("id", $process->id)
            ->update($attributes);
    }

    /**
     * @param array $attributes
     * @return Builder|Model
     */
    public function createProcess(array $attributes)
    {
        return CampaignProcess::query()
            ->create($attributes);
    }

    /**
     * @param array $emails
     * @return void
     */
    public function createProcesses(array $emails)
    {

        foreach ($emails as $email) {
            /**
             * @var CampaignProcess $process
             */
            $process = $this->createProcess([
                "campaign_id" => $this->campaign->id,
                "processor"   => Campaign::STATUS_VALIDATE_EMAILS,
                "status"      => CampaignProcess::STATUS_PENDING,
                "data"        => $email[0]
            ]);

            ValidateCampaignEmails::dispatch($this->campaign, $process);
        }

    }

    /**
     * @param array $emails
     * @return array
     */
    private function emailsToLowerCase(array $emails)
    {
        return array_map(function ($email) {
            return strtolower($email);
        }, $emails);
    }

    /**
     * @param array $emails
     * @return array
     */
    private function getUniqueEmails(array $emails)
    {
        return array_unique($emails);
    }

    /**
     * @param array $emails
     * @param int $size
     * @return array
     */
    private function getChunckedEmails(array $emails, int $size = 1)
    {
        return array_chunk($emails, $size);
    }
}
