<?php

namespace App\Jobs;

use App\Models\Campaign;
use App\Models\CampaignDomain;
use App\Models\CampaignProcess;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class CollectCampaignStores extends Job implements CampaignJobInterface
{
    /**
     * @var Campaign
     */
    private $campaign;


    /**
     * CollectCampaignStores constructor.
     * @param Campaign $campaign
     */
    public function __construct(Campaign $campaign)
    {
        $this->campaign = $campaign;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        /**
         * @var $process CampaignProcess
         */
        $process = $this->createProcess([
            "campaign_id" => $this->campaign->id,
            "processor"   => Campaign::STATUS_COLLECT_STORES,
            "status"      => CampaignProcess::STATUS_DISPATCHING
        ]);

        $recipients = $this->getDomains();

        $this->updateProcess($process, [
            "result" => json_encode($recipients),
            "status" => CampaignProcess::STATUS_PROCESSED
        ]);

        $this->updateCampaignStatus();
    }

    /**
     * @return array
     */
    private function getDomains()
    {
        $rows = CampaignDomain::query()
            ->where("campaign_id", $this->campaign->id)
            ->get();

        $domains = [];

        foreach ($rows as $row) {
            $domains[] = $row->domain;
        }

        return $domains;
    }

    /**
     * @param Campaign $campaign
     */
    public static function dispatch(Campaign $campaign)
    {
        dispatch(
            (new static($campaign))
                ->onConnection('database')
        );
    }

    public function updateCampaignStatus()
    {
        $this->campaign->update([
            "status" => Campaign::STATUS_WAITING_TO_COLLECT_EMAILS
        ]);
    }

    /**
     * @param CampaignProcess $process
     * @param array $attributes
     */
    public function updateProcess(CampaignProcess $process, array $attributes)
    {
        CampaignProcess::query()
            ->where("id", $process->id)
            ->update($attributes);
    }

    /**
     * @param array $attributes
     * @return Builder|Model
     */
    public function createProcess(array $attributes)
    {
        return CampaignProcess::query()
            ->create($attributes);
    }
}
