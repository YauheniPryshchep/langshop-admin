<?php

namespace App\Jobs;

use App\Models\CampaignProcess;

interface CampaignJobInterface
{
    public function updateCampaignStatus();

    public function updateProcess(CampaignProcess $process, array $attributes);

    public function createProcess(array $attributes);
}