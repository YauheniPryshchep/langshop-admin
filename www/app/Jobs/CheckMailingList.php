<?php

namespace App\Jobs;

use App\Models\Campaign;
use App\Models\CampaignProcess;
use App\Services\MailingListService;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\Log;
use Exception;

class CheckMailingList extends Job implements CampaignJobInterface
{
    /**
     * @var Campaign
     */
    private $campaign;

    public function __construct(Campaign $campaign)
    {
        $this->campaign = $campaign;
    }

    /**
     * The job failed to process.
     *
     * @param Exception $exception
     * @return void
     */
    public function failed(Exception $exception)
    {
        $this->returnCampaignStatus();
    }

    /**
     * Execute the job.
     * @return void
     * @throws GuzzleException
     */
    public function handle()
    {
        /**
         * @var $process CampaignProcess
         */
        $process = $this->createProcess([
            "campaign_id" => $this->campaign->id,
            "processor"   => Campaign::STATUS_CHECK_MAILLIST,
            "status"      => CampaignProcess::STATUS_DISPATCHING
        ]);

        $recipientData = CampaignProcess::query()
            ->where('campaign_id', $this->campaign->id)
            ->where('processor', Campaign::STATUS_UPLOAD_MEMBERS)
            ->first()
            ->toArray();

        $mailList = CampaignProcess::query()
            ->where('campaign_id', $this->campaign->id)
            ->where('processor', Campaign::STATUS_CREATE_MAILLIST)
            ->first()
            ->toArray();

        if (!$this->isReadyToSend($recipientData['result']['emails'], $mailList['result']['address'])) {
            $this->returnCampaignStatus();
            return;
        }

        $this->updateProcess($process, [
            "result" => true,
            "status" => CampaignProcess::STATUS_PROCESSED
        ]);

        $this->updateCampaignStatus();
    }

    /**
     * @param array $members
     * @param string $address
     * @return bool
     * @throws GuzzleException
     */
    private function isReadyToSend(array $members, string $address): bool
    {
        $count = $this->getMembersCount($address);
        return $count === count($members);
    }

    /**
     * @param string $address
     * @return int
     * @throws GuzzleException
     */
    private function getMembersCount(string $address): int
    {
        $mailGunListService = new MailingListService('default');
        $items = $mailGunListService->getMaillistUploadedEvent($address);

        return $this->count($items);
    }

    /**
     * @param array $items
     * @return int|mixed
     */
    private function count(array $items)
    {
        $count = 0;

        foreach ($items as $item) {
            $count += $item["upserted-count"];
            $count += $item["failed-count"];
        }

        return $count;
    }

    /**
     * @param Campaign $campaign
     */
    public static function dispatch(Campaign $campaign)
    {
        dispatch(
            (new static($campaign))
                ->onConnection('database')
        );
    }

    public function updateCampaignStatus()
    {
        $this->campaign->update([
            "status" => Campaign::STATUS_WAITING_TO_SENDING_EMAIL
        ]);
    }

    public function returnCampaignStatus()
    {
        $this->campaign->update([
            "status" => Campaign::STATUS_WAITING_TO_CHECK_MAILLIST
        ]);
    }

    /**
     * @param CampaignProcess $process
     * @param array $attributes
     */
    public function updateProcess(CampaignProcess $process, array $attributes)
    {
        CampaignProcess::query()
            ->where("id", $process->id)
            ->update($attributes);
    }

    /**
     * @param array $attributes
     * @return Builder|Model
     */
    public function createProcess(array $attributes)
    {
        return CampaignProcess::query()
            ->firstOrCreate($attributes, $attributes);
    }

}
