<?php

namespace App\Jobs;

use App\Models\Campaign;
use App\Models\CampaignProcess;
use App\Objects\MailingListObject;
use App\Services\MailingListService;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;

class CreateCampaignMaillist extends Job implements CampaignJobInterface
{
    /**
     * @var Campaign
     */
    private $campaign;


    /**
     * CreateCampaignMaillist constructor.
     * @param Campaign $campaign
     */
    public function __construct(Campaign $campaign)
    {
        $this->campaign = $campaign;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws GuzzleException
     */
    public function handle()
    {
        /**
         * @var $process CampaignProcess
         */
        $process = $this->createProcess([
            "campaign_id" => $this->campaign->id,
            "processor"   => Campaign::STATUS_CREATE_MAILLIST,
            "status"      => CampaignProcess::STATUS_DISPATCHING
        ]);
        $mailList = $this->createMailingList();

        $this->updateProcess($process, [
            "result" => json_encode(["address" => $mailList->getAddress()]),
            "status" => CampaignProcess::STATUS_PROCESSED
        ]);

        $this->updateCampaignStatus();
    }

    /**
     * @return MailingListObject
     * @throws GuzzleException
     */
    private function createMailingList()
    {
        $mailGunListService = new MailingListService('default');
        $mailgunDomain = config('mailgun.default.domain');
        return $mailGunListService->storeMailingList([
            "address"     => 'campaign-' . $this->campaign->id . '@' . $mailgunDomain,
            "name"        => $this->campaign->title,
            "description" => $this->campaign->description
        ]);
    }

    /**
     * @param Campaign $campaign
     */
    public static function dispatch(Campaign $campaign)
    {
        dispatch(
            (new static($campaign))
                ->onConnection('database')
        );
    }

    public function updateCampaignStatus()
    {
        $this->campaign->update([
            "status" => Campaign::STATUS_WAITING_TO_UPLOAD_MEMBERS
        ]);
    }

    /**
     * @param CampaignProcess $process
     * @param array $attributes
     */
    public function updateProcess(CampaignProcess $process, array $attributes)
    {
        CampaignProcess::query()
            ->where("id", $process->id)
            ->update($attributes);
    }

    /**
     * @param array $attributes
     * @return Builder|Model
     */
    public function createProcess(array $attributes)
    {
        return CampaignProcess::query()
            ->firstOrCreate($attributes, $attributes);
    }
}
