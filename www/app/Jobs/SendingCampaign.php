<?php

namespace App\Jobs;

use App\Models\Campaign;
use App\Models\CampaignDomain;
use App\Models\CampaignProcess;
use App\Models\StoreEvents;
use App\Services\MailGunService;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\Log;

class SendingCampaign extends Job implements CampaignJobInterface
{

    const MAX_MEMBER_PER_REQUEST = 250;
    /**
     * @var Campaign
     */
    private $campaign;

    public function __construct(Campaign $campaign)
    {
        $this->campaign = $campaign;
    }

    /**
     * Execute the job.
     * @return void
     * @throws GuzzleException
     */
    public function handle()
    {
        $processSending = $this->getProcess();

        if (!empty($processSending)) {
            $this->updateCampaignStatus();
            return;
        }

        /**
         * @var $process CampaignProcess
         */
        $process = $this->createProcess([
            "campaign_id" => $this->campaign->id,
            "processor"   => Campaign::STATUS_SENDING_EMAIL,
            "status"      => CampaignProcess::STATUS_DISPATCHING
        ]);

        $recipientData = CampaignProcess::query()
            ->where('campaign_id', $this->campaign->id)
            ->where('processor', Campaign::STATUS_UPLOAD_MEMBERS)
            ->first()
            ->toArray();

        $mailList = CampaignProcess::query()
            ->where('campaign_id', $this->campaign->id)
            ->where('processor', Campaign::STATUS_CREATE_MAILLIST)
            ->first()
            ->toArray();


        $this->sendEmail($this->campaign, $mailList['result']['address']);

        $this->addDomainsToCampaignEvent($recipientData['result']['domains']);

        $this->updateProcess($process, [
            "result" => true,
            "status" => CampaignProcess::STATUS_PROCESSED
        ]);

        $this->updateCampaignStatus();
    }

    /**
     * @param Campaign $campaign
     * @param string $address
     * @throws GuzzleException
     */
    private function sendEmail(Campaign $campaign, string $address)
    {
        $options = [];

        if ($campaign->sender && $campaign->from && $campaign->reply_to) {
            $options['from'] = "{$campaign->sender} <{$campaign->from}>";
            $options['reply_to'] = $campaign->reply_to;
        }

        $mailgunService = new MailGunService('default', $options);

        $mailgunService->send([
            'to'                    => $address,
            'subject'               => $campaign->subject,
            'template'              => $campaign->template ? $campaign->template : 'default',
            'o:tag'                 => $address,
            'h:X-Mailgun-Variables' => json_encode([
                'subject'           => $campaign->subject,
                'message'           => $campaign->description,
                "domain"            => "%recipient.domain%",
                "store_name"        => "%recipient.name%",
                "email"             => "%recipient.email%",
                "iana_timezone"     => "%recipient.iana_timezone%",
                "plan_display_name" => "%recipient.plan_display_name%",
                "primary_locale"    => "%recipient.primary_locale%",
                "currency"          => "%recipient.currency%",
                "customer_email"    => "%recipient.customer_email%",
                "password_enabled"  => "%recipient.password_enabled%",
                "utm_source"        => "partners",
                "utm_medium"        => $campaign->type ?? "email",
                "utm_campaign"      => $campaign->id
            ])
        ]);
    }

    /**
     * @param Campaign $campaign
     */
    public static function dispatch(Campaign $campaign)
    {
        dispatch(
            (new static($campaign))
                ->onConnection('database')
        );
    }

    public function updateCampaignStatus()
    {
        $this->campaign->update([
            "status" => Campaign::STATUS_WAITING_TO_COMPLETE
        ]);
    }

    /**
     * @param CampaignProcess $process
     * @param array $attributes
     */
    public function updateProcess(CampaignProcess $process, array $attributes)
    {
        CampaignProcess::query()
            ->where("id", $process->id)
            ->update($attributes);
    }

    /**
     * @param array $attributes
     * @return Builder|Model
     */
    public function createProcess(array $attributes)
    {
        return CampaignProcess::query()
            ->firstOrCreate($attributes, $attributes);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Builder|Model|object|null
     */
    public function getProcess()
    {
        return CampaignProcess::query()
            ->where("campaign_id", $this->campaign->id)
            ->where("processor", Campaign::STATUS_SENDING_EMAIL)
            ->first();
    }

    /**
     * @param array $domains
     */
    private function addDomainsToCampaignEvent(array $domains)
    {
        foreach (array_chunk($domains, self::MAX_MEMBER_PER_REQUEST) as $chunk) {
            $rows = [];
            for ($i = 0; $i < count($chunk); $i++) {
                $rows[] = [
                    "domain"     => $chunk[$i],
                    'type'       => StoreEvents::STORE_ADD_TO_CAMPAIGN,
                    "event_data" => json_encode(["campaign_id" => $this->campaign->id]),
                    'timestamp'  => time()
                ];
            }

            StoreEvents::query()->insert($rows);
        }
    }
}
