<?php

namespace App\Jobs;

use App\Models\Campaign;
use App\Models\CampaignProcess;
use App\Services\EmailValidateService;
use Exception;

class ValidateCampaignEmails extends Job
{
    /**
     * @var Campaign
     */
    private $campaign;

    /**
     * @var CampaignProcess
     */
    private $process;

    /**
     * @var EmailValidateService
     */
    private $emailValidateService;

    /**
     * CollectCampaignStores constructor.
     * @param Campaign $campaign
     * @param CampaignProcess $process
     */
    public function __construct(Campaign $campaign, CampaignProcess $process)
    {
        $this->campaign = $campaign;
        $this->process = $process;
    }

    /**
     * The job failed to process.
     *
     * @param Exception $exception
     * @return void
     */
    public function failed(Exception $exception)
    {
        $this->updateProcess([
            "status" => CampaignProcess::STATUS_FAILED
        ]);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->emailValidateService = new EmailValidateService();

        $result = $this->emailValidateService->validate($this->process->data);

        if ($this->emailValidateService->isValidEmail($result)) {
            $this->updateProcess([
                "status" => CampaignProcess::STATUS_PROCESSED
            ]);
        } else {
            $this->updateProcess([
                "status" => CampaignProcess::STATUS_FAILED
            ]);
        }
    }

    /**
     * @param Campaign $campaign
     * @param CampaignProcess $process
     */
    public static function dispatch(Campaign $campaign, CampaignProcess $process)
    {
        dispatch(
            (new static($campaign, $process))
                ->onConnection('database')
        );
    }

    /**
     * @param array $attributes
     */
    public function updateProcess(array $attributes)
    {
        $this->process
            ->update($attributes);
    }
}
