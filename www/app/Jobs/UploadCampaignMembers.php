<?php

namespace App\Jobs;

use App\Models\Campaign;
use App\Models\CampaignProcess;
use App\Models\Store;
use App\Objects\MailingListObject;
use App\Services\MailingListProcessingService;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\Log;


class UploadCampaignMembers extends Job implements CampaignJobInterface
{

    CONST MAX_MEMBER_PER_REQUEST = 500;

    /**
     * @var Campaign
     */
    private $campaign;


    /**
     * UploadCampaignMembers constructor.
     * @param Campaign $campaign
     */
    public function __construct(Campaign $campaign)
    {
        $this->campaign = $campaign;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $recipientEmails = CampaignProcess::query()
            ->where('campaign_id', $this->campaign->id)
            ->where('processor', Campaign::STATUS_COLLECT_EMAILS)
            ->first()
            ->toArray();

        $mailList = CampaignProcess::query()
            ->where('campaign_id', $this->campaign->id)
            ->where('processor', Campaign::STATUS_CREATE_MAILLIST)
            ->first()
            ->toArray();

        /**
         * @var $process CampaignProcess
         */
        $process = $this->createProcess([
            "campaign_id" => $this->campaign->id,
            "processor"   => Campaign::STATUS_UPLOAD_MEMBERS,
            "status"      => CampaignProcess::STATUS_DISPATCHING
        ]);

        $recipientData = $this->getMembers($recipientEmails);

        $this->addMembers($recipientData['emails'], $mailList);

        $this->updateProcess($process, [
            "result" => json_encode($recipientData),
            "status" => CampaignProcess::STATUS_PROCESSED
        ]);

        $this->updateCampaignStatus();
    }

    /**
     * @param Campaign $campaign
     */
    public static function dispatch(Campaign $campaign)
    {
        dispatch(
            (new static($campaign))
                ->onConnection('database')
        );
    }

    public function updateCampaignStatus()
    {
        $this->campaign->update([
            "status" => Campaign::STATUS_WAITING_TO_CHECK_MAILLIST
        ]);
    }

    /**
     * @param CampaignProcess $process
     * @param array $attributes
     */
    public function updateProcess(CampaignProcess $process, array $attributes)
    {
        CampaignProcess::query()
            ->where("id", $process->id)
            ->update($attributes);
    }

    /**
     * @param array $attributes
     * @return Builder|Model
     */
    public function createProcess(array $attributes)
    {
        return CampaignProcess::query()
            ->firstOrCreate($attributes, $attributes);
    }

    /**
     * @return array|false
     */
    private function getValidateEmail()
    {
        $data = [];

        $processes = $this->getValidateEmailProcess();

        foreach ($processes as $process) {
            $data[] = $process->data;
        }

        return $data;
    }

    /**
     * @return Builder|Model|object|null
     */
    private function getValidateEmailProcess()
    {
        return CampaignProcess::query()
            ->where('campaign_id', $this->campaign->id)
            ->where('processor', Campaign::STATUS_VALIDATE_EMAILS)
            ->where('status', CampaignProcess::STATUS_PROCESSED)
            ->get();
    }

    /**
     * @param array $recipientEmails
     * @return array
     */
    private function getMembers(array $recipientEmails)
    {
        $emails = [];
        $domains = [];
        $data = [];

        $validEmails = $this->getValidateEmail();

        foreach ($recipientEmails['result'] as $recipient) {

            $email = $recipient['email'];
            $domain = $recipient['domain'];

            if (!in_array($domain, $domains)) {
                $domains[] = $domain;
            }

            if (!in_array(strtolower($email), $validEmails)) {
                continue;
            }

            if (!in_array(strtolower($email), $emails)) {
                $vars = $this->getVars($domain);
                $emails[] = $email;
                $data[] = [
                    "address" => $email,
                    "vars"    => [
                        "domain"            => $domain,
                        "name"              => $vars["name"] ?? null,
                        "email"             => $vars["email"] ?? $email,
                        "iana_timezone"     => $vars["iana_timezone"] ?? null,
                        "plan_display_name" => $vars["plan_display_name"] ?? null,
                        "primary_locale"    => $vars["primary_locale"] ?? null,
                        "currency"          => $vars["currency"] ?? null,
                        "customer_email"    => $vars["customer_email"] ?? null,
                        "password_enabled"  => $vars["password_enabled"] ?? null
                    ]
                ];
            }
        }

        return [
            "emails"  => $data,
            "domains" => $domains
        ];
    }

    /**
     * @param string $domain
     * @return array
     */
    private function getVars(string $domain)
    {
        $vars = [];

        $storeData = Store::query()->where("name", $domain)->with("storeData")->first();

        if ($storeData) {
            foreach ($storeData->storeData as $data) {
                $vars[$data["key"]] = $data["value"];
            }
        }
        return $vars;
    }

    /**
     * @param array $members
     * @param array $mailList
     */
    private function addMembers(array $members, array $mailList)
    {
        $mailingProcessingService = new MailingListProcessingService('default', $mailList['result']['address']);
        if (count($members) > self::MAX_MEMBER_PER_REQUEST) {
            $chunked = array_chunk($members, self::MAX_MEMBER_PER_REQUEST);
            for ($i = 0; $i < count($chunked); $i++) {
                $mailingProcessingService->addMembers($chunked[$i]);
            }
        } else {
            $mailingProcessingService->addMembers($members);
        }
    }
}
