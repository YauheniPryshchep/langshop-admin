<?php

namespace App\Validations;

use App\Services\MailGunTemplateService;
use Illuminate\Validation\Validator;

class MailGunTemplate
{
    const RULE = 'mailgun_template';

    /**
     * @param Validator $validator
     * @param string $message
     * @return bool
     */
    private function fail(Validator $validator, string $message)
    {
        $validator->setCustomMessages([
            self::RULE => $message
        ]);

        return false;
    }

    /**
     * @param $attribute
     * @param $value
     * @param $parameters
     * @param Validator $validator
     * @return bool
     */
    public function validate($attribute, $value, $parameters, Validator $validator)
    {
        $mailGunTemplateService = new MailGunTemplateService('default');

        try {
            $mailGunTemplateService->getTemplate($value);
            return true;
        }catch (\Exception $exception){
            return $this->fail($validator, __('validation.exists', ['attribute' => $attribute]));

        }
    }
}