<?php

namespace App\Requests;

use Illuminate\Support\Facades\Validator;

class CampaignDomainsRequest
{

    /**
     * @var array
     */
    protected $params = [];

    public function __construct(array $params)
    {
        $this->params = $params;
    }

    /**
     * Create campaign domains validation
     * @return array
     */
    public function create()
    {
        $validator = Validator::make($this->params, [
                    'domains' => 'array',
                    'campaign_id' => 'integer'
        ]);
        return $validator->validate();
    }

}
