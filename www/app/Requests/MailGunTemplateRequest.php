<?php

namespace App\Requests;

use Illuminate\Support\Facades\Validator;

class MailGunTemplateRequest
{

    protected $params;

    public function __construct(array $params)
    {
        $this->params = $params;
    }

    /**
     * Campaign template create validation
     * @return array
     */
    public function create()
    {
        $validator = Validator::make($this->params, [
                    'name' => 'required|string|max:255',
                    'description' => 'required|string|max:255',
                    'template' => 'string',
                    'tag' => 'string|max:255',
                    'comment' => 'string|max:255'
        ]);
        return $validator->validate();
    }

    /**
     * Campaign template update validation
     * @return array
     */
    public function update()
    {
        $validator = Validator::make($this->params, [
                    'description' => 'required|string|max:255',
        ]);
        return $validator->validate();
    }

}
