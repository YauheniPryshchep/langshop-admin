<?php

namespace App\Requests;

use Illuminate\Support\Facades\Validator;

class CampaignRequest
{

    protected $params;

    public function __construct(array $params)
    {
        $this->params = $params;
    }

    /**
     * Campaign create validation
     * @return array
     */
    public function create()
    {
        $validator = Validator::make($this->params, [
                    'title' => 'required|string|max:255',
                    'description' => 'required|string|max:255',
                    'subject' => 'required|string|max:255',
                    'template_id' => 'required|string',
                    'author_id' => 'required|integer',
                    'start_at' => 'nullable|date_format:Y-m-d H:i:s'
        ]);

        return $validator->validate();
    }

    /**
     * Campaign update validation
     * @return array
     */
    public function update()
    {
        $validator = Validator::make($this->params, [
                    'title' => 'string|max:255',
                    'description' => 'string|max:255',
                    'subject' => 'string|max:255',
                    'template_id' => 'string',
                    'author_id' => 'integer',
                    'status' => 'integer',
                    'start_at' => 'nullable|date_format:Y-m-d H:i:s']);
        return $validator->validate();
    }

    /**
     * @return array
     */
    protected function rules(): array
    {
        return $this->rules;
    }

    /**
     * @return array
     */
    public function input(): array
    {
        return $this->input;
    }
}
