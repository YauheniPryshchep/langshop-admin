<?php

namespace App\Requests;

use Illuminate\Support\Facades\Validator;

class RecipientsFilterRequest
{

    /**
     * @var array
     */
    protected $params = [];

    public function __construct(array $params)
    {
        $this->params = $params;
    }

    /**
     * Create recipients filter validation
     * @return array
     */
    public function create()
    {
        $validator = Validator::make($this->params, [
            'filters'                    => 'array',
            'filters.*.comparator'       => 'string',
            'filters.*.key'              => 'string',
            'filters.*.value.start_date' => 'date',
            'filters.*.value.end_date'   => 'date',
            'campaign_id'                => 'integer'
        ]);
        return $validator->validate();
    }

}
