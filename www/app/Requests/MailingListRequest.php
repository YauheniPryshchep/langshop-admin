<?php

namespace App\Requests;

use Illuminate\Support\Facades\Validator;

class MailingListRequest
{

    protected $params;

    public function __construct(array $params)
    {
        $this->params = $params;
    }

    /**
     * Mailing list create validation
     * @return array
     */
    public function create()
    {
        $validator = Validator::make($this->params, [
                    'address' => 'required|string|max:255',
                    'name' => 'required|string|max:255',
                    'description' => 'string',
                    'access_level' => 'string|max:255',
                    'reply_preference' => 'string|max:255'
        ]);
        return $validator->validate();
    }

}
