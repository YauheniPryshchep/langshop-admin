<?php

namespace App\Requests;

use Illuminate\Support\Facades\Validator;

class MailGunEmailRequest
{

    protected $params;

    public function __construct(array $params)
    {
        $this->params = $params;
    }

    /**
     * Send email validation
     * @return array
     */
    public function create()
    {
        $validator = Validator::make($this->params, [
                    'to' => 'required|string|max:255',
                    'subject' => 'required|string|max:255',
                    'text' => 'required|string',
        ]);
        return $validator->validate();
    }

}
