<?php

namespace App\Services;

use App\Models\StoreEvents;
use App\Validators\StoreEventsValidators;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class StoreEventSystemService
{
    /**
     * @param array $attributes
     * @return array
     */
    public function store(array $attributes)
    {
        return $attributes;
    }
}
