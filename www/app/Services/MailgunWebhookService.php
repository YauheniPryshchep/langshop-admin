<?php

namespace App\Services;

use App\Exceptions\Http\BadRequestError;
use App\Models\Lead;
use App\Models\LeadEmail;
use App\Models\Store;
use App\Models\StoreData;
use Exception;
use Illuminate\Support\Facades\Log;


class MailgunWebhookService
{

    /**
     * @param $recipient
     * @return array
     */
    public function getDomains($recipient)
    {

        $langShopDomain = $this->getLangshopDomain($recipient);

        if ($langShopDomain) {
            return [ $langShopDomain->name ];
        }

        $leads = $this->getLeadsDomain($recipient);
        $domains = [];

        foreach ($leads as $lead) {
            if (!in_array($lead->public_domain, $domains)) {
                $domains[] = $lead->public_domain;
            }
            continue;
        }

        return $domains;

    }

    /**
     * @param $recipient
     * @return Store|null
     */
    private function getLangshopDomain($recipient)
    {
        $storeDate = StoreData::query()
            ->where('value', '=', $recipient)
            ->where('key', '=', 'email')
            ->first();

        if (!$storeDate) {
            return null;
        }

        /**
         * @var $store Store
         */
        $store = Store::query()
            ->where('id', '=', $storeDate->store_id)
            ->first();

        return $store;
    }

    /**
     * @param $recipient
     * @return Lead|null
     */
    private function getLeadsDomain($recipient)
    {
        $storeDate = LeadEmail::query()
            ->where('email', '=', $recipient)
            ->get();

        if (!$storeDate) {
            return null;
        }

        $ids = [];

        foreach ($storeDate as $store) {
            $ids[] = $store->lead_id;
        }

        /**
         * @var $leads Lead
         */
        $leads = Lead::query()
            ->whereIn('id', $ids)
            ->get();

        return $leads;
    }

}
