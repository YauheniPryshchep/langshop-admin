<?php

namespace App\Services;

use App\Exceptions\Http\InternalError;
use App\Models\Partners\Payout;
use App\Models\Partners\User;
use Exception;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Facades\Log;
use App\Requests\MailGunEmailRequest;

class MailGunService extends MailGunClient
{

    /**
     * @var string
     */
    private $credentials;

    /**
     * @var string
     */
    protected $url;

    /**
     * @var array
     */
    public $connections = [];

    /**
     * MailGunService constructor.
     * @param string $credentials
     * @param array $options
     */
    public function __construct(string $credentials, array $options = [])
    {
        $this->credentials = $credentials;
        $this->connections = config('mailgun.' . $this->credentials);
        $this->url = $this->connections['domain'] . '/messages';

        if (!empty($options)) {
            $this->connections['from'] = $options['from'];
            $this->connections['reply-to'] = $options['reply_to'];
        }
    }

    /**
     * Sending message with template via MailGun
     * @param string $to
     * @param string $subject
     * @param string $message
     * @param string $template
     * @return string
     * @throws GuzzleException
     */
    public function sendMail($to, $subject, $message, $template = null)
    {
        $params = [
            'to'      => $to,
            'subject' => $subject,
            'text'    => $message,
        ];
        $validator = new MailGunEmailRequest($params);
        $attributes = $validator->create();
        if (!is_array($attributes)) {
            return $attributes;
        }
        if (isset($template)) {
            return $this->sendTemplate($to, $subject, $message, $template);
        }

        return $this->sendMessage($to, $subject, $message);
    }

    /**
     * Sending message with template via MailGun
     * @param string $to
     * @param string $subject
     * @param string $message
     * @param string $template
     * @return string
     * @throws GuzzleException
     */
    private function sendTemplate($to, $subject, $message, $template = null)
    {
        $data = [
            'to'                    => $to,
            'subject'               => $subject,
            'template'              => $template ? $template : 'default',
            'h:X-Mailgun-Variables' => json_encode([
                'subject' => $subject,
                'message' => $message
            ])
        ];

        return $this->send($data);
    }

    public function build(string $path, array $attributes)
    {
        return view($path)->with($attributes)->render();
    }

    /**
     * @param User $user
     * @param Payout $payout
     * @return string
     * @throws GuzzleException
     */
    public function sendPayoutAccepted(User $user, Payout $payout)
    {
        $html = $this->build('emails.paid-payout', [
            'userName'    => $user->getUserName() ?? "There",
            'profileLink' => config('partners.url'),
            'payoutLink'  => config('partners.url') . '/payouts/' . $payout->id,
            'paypal'      => $user->getSettingsValue('paypal_account')
        ]);

        $data = [
            'to'                    => $user->email,
            'subject'               => "Withdraw Request Accepted",
            'html'                  => $html,
            'h:X-Mailgun-Variables' => json_encode([
                'subject' => "Withdraw Request Accepted",
            ])
        ];

        return $this->send($data);
    }

    /**
     * @param User $user
     * @param Payout $payout
     * @return string
     * @throws GuzzleException
     */
    public function sendPayoutDeclined(User $user, Payout $payout)
    {
        $html = $this->build('emails.declined-payout', [
            'userName'    => $user->getUserName() ?? "There",
            'profileLink' => config('partners.url'),
            'payoutLink'  => config('partners.url') . '/payouts/' . $payout->id,
            'paypal'      => $user->getSettingsValue('paypal_account')
        ]);

        $data = [
            'to'                    => $user->email,
            'subject'               => "Withdraw Request Declined",
            'html'                  => $html,
            'h:X-Mailgun-Variables' => json_encode([
                'subject' => "Withdraw Request Accepted",
            ])
        ];


        return $this->send($data);
    }

    /**
     * Sending message via MailGun
     * @param string $to
     * @param string $subject
     * @param string $message
     * @return string
     * @throws GuzzleException
     */
    private function sendMessage($to, $subject, $message)
    {
        $data = [
            'from'    => $this->connections['from'],
            'to'      => $to,
            'subject' => $subject,
            'html'    => $message,
            'text'    => strip_tags(nl2br($message))
        ];

        if (!empty($this->connections['reply-to'])) {
            $data['h:Reply-To'] = $this->connections['reply-to'];
        }


        return $this->send($data);
    }

    /**
     * Sending email via MailGun
     * @param array $data
     * @return string
     */
    public function send($data)
    {
        $data = array_replace(['from' => $this->connections['from']], $data);

        if (!empty($this->connections['reply-to'])) {
            $data['h:Reply-To'] = $this->connections['reply-to'];
        }

        try {
            $this->request($this->credentials, 'POST', $this->url, ['form_params' => $data], true);
            return trans('email.successfully_sent');
        } catch (BadResponseException $e) {
            $response = json_decode($e->getResponse()->getBody()->getContents());

            throw new InternalError(
                400,
                $response->message ?? "Bad Request",
                $response->errors ?? [],
                $response->code ?? 400
            );
        } catch (GuzzleException $e) {

            return $e->getMessage();
        }
    }

}
