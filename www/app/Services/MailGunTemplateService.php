<?php

namespace App\Services;

use App\Exceptions\Http\ServiceUnavailable;
use App\Requests\MailGunTemplateRequest;
use App\Accessors\MailGunTemplate;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Http\Response;

class MailGunTemplateService extends MailGunClient
{

    /**
     * @var string
     */
    private $credentials;

    /**
     * @var string
     */
    protected $url;

    /**
     * @var array
     */
    private $connections = [];

    /**
     * MailGunTemplateService constructor.
     */
    public function __construct(string $credentials)
    {
        $this->credentials = $credentials;
        $this->connections = config('mailgun.' . $this->credentials);
        $this->url = $this->connections['domain'] . '/templates';
    }

    /**
     * Get campaign templates list from MailGun API
     * @return array
     * @throws GuzzleException
     */
    public function getTemplates()
    {
        $response = $this->request($this->credentials, 'GET', $this->url, ['query' => ['limit' => 100]]);
        $template = json_decode($response->getBody(), true);
        $items = array_map(function (array $item) {
            return new MailGunTemplate($item);
        }, $template['items']);

        return $items;
    }

    /**
     * Get campaign template from MailGun API
     * @param string $name
     * @param string $active
     * @return MailGunTemplate
     * @throws GuzzleException
     */
    public function getTemplate(string $name, $active = '')
    {
        $response = $this->request($this->credentials, 'GET', $this->url . '/' . $name, ['query' => ['active' => $active]]);
        $template = json_decode($response->getBody(), true);
        return new MailGunTemplate($template['template']);
    }

    /**
     * Store campaign template to MailGun API
     * @param array $params
     * @return MailGunTemplate|array
     * @throws GuzzleException
     */
    public function storeTemplate(array $params)
    {
        $validator = new MailGunTemplateRequest($params);
        $attributes = $validator->create();
        if (!is_array($attributes)) {
            return $attributes;
        }
        $response = $this->request($this->credentials, 'POST', $this->url, ['form_params' => $params]);
        $template = json_decode($response->getBody(), true);
        return new MailGunTemplate($template['template']);
    }

    /**
     * Update campaign template in MailGun API
     * @param string $name
     * @param array $params
     * @return array
     * @throws GuzzleException
     */
    public function updateTemplate(string $name, array $params)
    {
        $validator = new MailGunTemplateRequest($params);
        $attributes = $validator->update();
        if (!is_array($attributes)) {
            return $attributes;
        }
        $response = $this->request($this->credentials, 'PUT', $this->url . '/' . $name, ['form_params' => $params]);
        return json_decode($response->getBody(), true);
    }

    /**
     * Delete campaign template from MailGun API
     * @param string $name
     * @return array
     * @throws GuzzleException
     */
    public function deleteTemplate(string $name)
    {
        $response = $this->request($this->credentials, 'DELETE', $this->url . '/' . $name);
        return json_decode($response->getBody(), true);
    }

}
