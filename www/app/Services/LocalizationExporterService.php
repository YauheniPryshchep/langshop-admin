<?php

namespace App\Services\App;

use Illuminate\Support\Collection;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use RecursiveRegexIterator;
use RegexIterator;

class LocalizationExporterService
{
    /**
     * @var array
     */
    protected $strings = [];

    /**
     * @var string
     */
    protected $phpRegex = '/^.+\.php$/i';

    /**
     * @var string
     */
    protected $jsonRegex = '/^.+\.json$/i';

    /**
     * @var string
     */
    protected $excludePath = DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR;

    /**
     * @var string
     */
    protected $packageSeparator = '.';

    /**
     * Method to return generate array with contents of parsed language files.
     *
     * @return static
     */
    public function export()
    {
        foreach ([resource_path('lang')] as $dir) {

            // Collect language files and build array with translations
            $files = $this->findLanguageFiles($dir);

            // Parse translations and create final array
            array_walk($files['lang'], [$this, 'parseLangFiles'], $dir);
            array_walk($files['vendor'], [$this, 'parseVendorFiles'], $dir);
            array_walk($files['json'], [$this, 'parseJsonFiles'], $dir);
        }

        return $this;
    }

    /**
     * Find available language files and parse them to array.
     *
     * @param string $path
     *
     * @return array
     */
    protected function findLanguageFiles($path)
    {
        // Loop through directories
        $dirIterator = new RecursiveDirectoryIterator($path, RecursiveDirectoryIterator::SKIP_DOTS);
        $recIterator = new RecursiveIteratorIterator($dirIterator);

        // Fetch only php files - skip others
        $phpFiles  = array_values(
            array_map('current',
                iterator_to_array(
                    new RegexIterator($recIterator, $this->phpRegex, RecursiveRegexIterator::GET_MATCH)
                )
            )
        );
        $jsonFiles = array_values(
            array_map('current',
                iterator_to_array(
                    new RegexIterator($recIterator, $this->jsonRegex, RecursiveRegexIterator::GET_MATCH)
                )
            )
        );

        $files = array_merge($phpFiles, $jsonFiles);

        // Sort array by filepath
        sort($files);

        // Remove full path from items
        array_walk($files, function (&$item) use ($path) {
            $item = str_replace($path, '', $item);
        });

        // Fetch non-vendor files from filtered php files
        $nonVendorFiles = array_filter($files, function ($file) {
            return strpos($file, $this->excludePath) === false && strpos($file, '.json') === false;
        });

        // Fetch vendor files from filtered php files
        $vendorFiles = array_filter(array_diff($files, $nonVendorFiles), function ($file) {
            return strpos($file, 'json') === false;
        });

        // Fetch .json files from filtered files
        $jsonFiles = array_filter($files, function ($file) {
            return strpos($file, '.json') !== false;
        });

        return [
            'lang'   => array_values($nonVendorFiles),
            'vendor' => array_values($vendorFiles),
            'json'   => array_values($jsonFiles),
        ];
    }

    /**
     * Method to return array for json serialization.
     *
     * @return array
     */
    public function jsonSerialize()
    {
        return $this->strings;
    }

    /**
     * Method to return array.
     *
     * @return array
     */
    public function toArray()
    {
        return $this->strings;
    }

    /**
     * Method to return i18n language object.
     *
     * @param string $language
     * @return array
     */
    public function toI18n(string $language)
    {
        $default = config('app.fallback_locale');
        $locales = $this->strings[$language] ?? [];
        if ($language === $default) {
            return $locales;
        }

        $fallback = $this->strings[$default] ?? [];
        if (empty($fallback)) {
            return $locales;
        }

        return array_replace_recursive($fallback, $locales);
    }

    /**
     * If you need special format of array that's recognised by some npm localization packages as Lang.js
     * https://github.com/rmariuzzo/Lang.js use this method.
     *
     * @param string $prefix
     *
     * @return array
     */
    public function toFlat($prefix = '.')
    {
        $results              = [];
        $default_locale       = config('app.fallback_locale');
        $default_json_strings = null;

        foreach ($this->strings as $lang => $strings) {
            if ($lang !== 'json') {
                foreach ($strings as $lang_array => $lang_messages) {
                    $key           = $lang . $prefix . $lang_array;
                    $results[$key] = $lang_messages;
                }
            } else {
                foreach ($strings as $json_lang => $json_strings) {
                    $key = $json_lang . $prefix . '__JSON__';

                    if (array_key_exists($key, $results)) {
                        $results[$key] = $json_strings;
                    } else {
                        $results[$key] = $json_strings;
                    }

                    // Pick only the first $json_strings
                    if (!$default_json_strings) {
                        $default_json_strings = $json_strings;
                    }
                }
            }
        }

        // Create a JSON key value pair for the default language
        $default_key = $default_locale . $prefix . '__JSON__';
        if (!array_key_exists($default_key, $results)) {
            $buffer = array_keys(
                $default_json_strings ? get_object_vars($default_json_strings) : []
            );

            $results[$default_key] = array_combine($buffer, $buffer);
        }

        return $results;
    }

    /**
     * Method to return array as collection.
     *
     * @return Collection
     */
    public function toCollection()
    {
        return collect($this->strings);
    }

    /**
     * Method to parse language files.
     * @param string $file
     * @param string $key
     * @param string $dir
     */
    protected function parseLangFiles(string $file, string $key, string $dir)
    {
        // Base package name without file ending
        $packageName = basename($file, '.php');

        // Get package, language and file contents from language file
        // /<language_code>/(<package/)<filename>.php
        $language     = explode(DIRECTORY_SEPARATOR, $file)[1];
        $fileContents = require $dir . DIRECTORY_SEPARATOR . $file;

        // Check if language already exists in array
        if (array_key_exists($language, $this->strings)) {
            if (array_key_exists($packageName, $this->strings[$language])) {
                $this->strings[$language][$packageName] =
                    array_replace_recursive((array)$this->strings[$language][$packageName], (array)
                    $fileContents);
            } else {
                $this->strings[$language][$packageName] = $fileContents;
            }
        } else {
            $this->strings[$language] = [
                $packageName => $fileContents,
            ];
        }
    }

    /**
     * Method to parse language files from vendor folder.
     *
     * @param string $file
     * @param string $key
     * @param string $dir
     */
    protected function parseVendorFiles(string $file, string $key, string $dir)
    {
        // Base package name without file ending
        $packageName = basename($file, '.php');

        // Get package, language and file contents from language file
        // /vendor/<package>/<language_code>/<filename>.php
        $package      = explode(DIRECTORY_SEPARATOR, $file)[2];
        $language     = explode(DIRECTORY_SEPARATOR, $file)[3];
        $fileContents = require $dir . DIRECTORY_SEPARATOR . $file;

        // Check if language already exists in array
        if (array_key_exists($language, $this->strings)) {
            // Check if package already exists in language
            if (array_key_exists($package, $this->strings[$language])) {
                if (array_key_exists($packageName, $this->strings[$language][$package])) {
                    $this->strings[$language][$package][$packageName] =
                        array_replace_recursive((array)$this->strings[$language][$package][$packageName],
                            (array)
                            $fileContents);
                } else {
                    $this->strings[$language][$package][$packageName] = $fileContents;
                }
            } else {
                $this->strings[$language][$package] = [$packageName => $fileContents];
            }
        } else {
            $this->strings[$language] = [
                $package => [
                    $packageName => $fileContents,
                ],
            ];
        }
    }

    /**
     * Method to parse language files from vendor folder.
     *
     * @param string $file
     * @param string $key
     * @param string $dir
     */
    protected function parseJsonFiles(string $file, string $key, string $dir)
    {
        // Base package name without file ending
        $language = basename($file, '.json');

        // Get package, language and file contents from language file
        // /<language_code>/(<package/)<filename>.php
        $fileContents = json_decode(file_get_contents($dir . $file));

        // Check if language already exists in array
        if (array_key_exists('json', $this->strings)) {
            if (array_key_exists($language, $this->strings['json'])) {
                $this->strings['json'][$language] =
                    array_replace_recursive((array)$this->strings['json'][$language], (array)$fileContents);
            } else {
                $this->strings['json'][$language] = $fileContents;
            }
        } else {
            $this->strings['json'] = [
                $language => $fileContents,
            ];
        }
    }
}