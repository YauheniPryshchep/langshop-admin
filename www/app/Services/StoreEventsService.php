<?php

namespace App\Services;

use Illuminate\Support\Facades\DB;

class StoreEventsService
{
    /**
     * @var string
     */
    protected $page;

    /**
     * @var string
     */
    protected $limit;

    /**
     * @var string
     */
    protected $langshop;

    /**
     * @var string
     */
    protected $dashboard;

    public function __construct(array $params, string $domain)
    {
        $this->langshop = config('database.connections.langshop.database');
        $this->dashboard = config('database.connections.dashboard.database');
        $this->page = $params['page'] ?? 1;
        $this->limit = $params['limit'] ?? 50;
        $this->domain = $domain ?? '';
    }

    /**
     * @return string
     */
    private function getBaseQuery(): string
    {
        return 'SELECT DISTINCT (merged.id), merged.type, merged.event_data, merged.timestamp, merged.domain '
            . ' FROM (SELECT id, type,  event_data, timestamp, domain FROM ' . $this->dashboard . '.stores_events '
            . ' WHERE domain = "' . $this->domain . '"'
            . ' UNION '
            . ' SELECT sh.id, sh.type, sh.payload, sh.timestamp, s.name as domain '
            . 'FROM ' . $this->langshop . '.store_history sh '
            . 'JOIN ' . $this->langshop . '.stores s ON s.id = sh.store_id'
            . ' WHERE s.name = "' . $this->domain . '" AND sh.source = "internal") AS merged ';
    }

    /**
     * @return string
     */
    private function getBaseCountQuery(): string
    {
        return 'SELECT DISTINCT COUNT((merged.id)) as events_count, merged.type, merged.event_data, merged.timestamp, merged.domain '
            . ' FROM (SELECT id, type,  event_data, timestamp, domain FROM ' . $this->dashboard . '.stores_events '
            . ' WHERE domain = "' . $this->domain . '"'
            . ' UNION '
            . ' SELECT sh.id, sh.type, sh.payload, sh.timestamp, s.name as domain '
            . 'FROM ' . $this->langshop . '.store_history sh '
            . 'JOIN ' . $this->langshop . '.stores s ON s.id = sh.store_id'
            . ' WHERE s.name = "' . $this->domain . '" AND sh.source = "internal") AS merged ';
    }

    /**
     * @return array
     */
    public function getEvents()
    {
        $sql = $this->getBaseQuery();
        $counter = $this->getBaseCountQuery();

        $sql .= " ORDER BY merged.timestamp DESC";

        if (isset($this->limit)) {
            $sql .= ' LIMIT ' . ($this->page - 1) * $this->limit . ', ' . $this->limit;
        }

        $count = array_column(DB::select($counter), 'events_count');

        $total = ceil(reset($count) / $this->limit);

        return [
            "items"      => array_map(function ($item) {
                if (!is_null($item->event_data)) {
                    $item->event_data = json_decode($item->event_data);
                }
                return $item;
            }, DB::select($sql)),
            "page"       => $this->page,
            "totalPages" => $total
        ];
    }


}
