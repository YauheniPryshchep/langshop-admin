<?php

namespace App\Services;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;

class MailGunClient
{

    /**
     * Guzzlehttp requests to destination source
     *
     * @param string $credentials
     * @param string $method
     * @param string $path
     * @param array $options
     * @param bool $is_messages
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function request(string $credentials, string $method, string $path, array $options = [], bool $is_messages = false)
    {
        $config = config('mailgun.' . $credentials);

        if($is_messages){
            $config['secret'] = $config['sending_key'];
        }

        $client = new Client([
            'base_uri' => 'https://' . $config['endpoint'] . '/v3/',
            'auth' => ['api', $config['secret']]
        ]);

        return $client->request($method, $path, $options);
    }

}
