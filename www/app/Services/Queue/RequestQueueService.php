<?php

namespace App\Services\Queue;

use Illuminate\Support\Facades\Cache;
use Psr\SimpleCache\InvalidArgumentException;

class RequestQueueService
{
    /**
     * @var Cache
     */
    private $cacheDriver;

    /**
     * @var string[]
     */
    private $availableQueues;

    /**
     * RequestQueueService constructor.
     * @param string[] $availableQueues
     */
    public function __construct(array $availableQueues)
    {
        if (empty($availableQueues)) {
            throw new \InvalidArgumentException("Missing queue keys");
        }

        $this->cacheDriver = Cache::store('redis');
        $this->availableQueues = $availableQueues;
    }

    /**
     * @return string
     */
    public function getLessUsageQueue(): string
    {
        $lessUsageQueue = null;
        $minQueueSize = null;

        foreach ($this->availableQueues as $i => $queue) {
            $requests = $this->getQueue($queue);
            $queueSize = count($requests);

            if ($queueSize === 0) {
                return $queue;
            }

            if ($minQueueSize && $minQueueSize < $queueSize) {
                continue;
            }

            $lessUsageQueue = $queue;
            $minQueueSize = $queueSize;
        }

        return $lessUsageQueue;
    }

    /**
     * @param string $queue
     * @param string $id
     */
    public function queue(string $queue, string $id): void
    {
        $requests = $this->getQueue($queue);

        $requests[] = $id;

        $this->updateQueue($queue, $requests);
    }

    /**
     * @param string $queue
     * @param string $id
     */
    public function deQueue(string $queue, string $id): void
    {
        $requests = $this->getQueue($queue);
        $index = array_search($id, $requests);
        if ($index === false) {
            return;
        }

        array_splice($requests, $index, 1);

        $this->updateQueue($queue, $requests);
    }

    /**
     * @param string $queue
     * @param string $id
     * @return int|null
     */
    public function getPosition(string $queue, string $id): ?int
    {
        $requests = $this->getQueue($queue);
        $index = array_search($id, $requests);

        return $index !== false ? $index : null;
    }

    /**
     * @param string $queue
     * @return array
     */
    private function getQueue(string $queue): array
    {
        try {
            $requests = $this->cacheDriver->get($queue);
        } catch (InvalidArgumentException $e) {
            return [];
        }

        return is_array($requests) ? $requests : [];
    }

    /**
     * @param string $queue
     * @param array $requests
     */
    private function updateQueue(string $queue, array $requests): void
    {
        $this->cacheDriver->put($queue, $requests, 3600);
    }
}
