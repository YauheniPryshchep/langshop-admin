<?php

namespace App\Services;

use App\Models\Store;
use App\Models\Partners\User;

use Illuminate\Database\Eloquent\Builder;


class StoreReferralService
{

    /**
     * @var array
     */
    private $filterValidation = [
        'tab'         => 'string',
        'search_term' => 'string',
    ];
    /**
     * @var Builder
     */
    private $baseQuery;

    public function __construct()
    {
        $this->baseQuery = Store::query()
            ->with("referrals");
    }

    /**
     * @param User $user
     * @return Store
     */
    public function getAllStores(User $user)
    {

        if (empty($user->settings['referral_link'])) {
            return [];
        }
        /**
         * @var Store $stores
         */
        $stores = Store::query()
            ->whereHas('referrals',
                function (Builder $query) use ($user) {
                    $query->where('ref', $user->settings['referral_link']);
                })
            ->with(
                [
                    "referrals" => function ($query) use ($user) {
                        $query->where('ref', $user->settings['referral_link'])
                            ->orderBy('created_at', 'asc');
                    }
                ]
            )
            ->get();

        return $stores;
    }
}
