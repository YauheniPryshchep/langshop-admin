<?php

namespace App\Services\Analytics;

use App\Models\Plan;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;


class AnalyticsService
{
    /**
     * @var AnalyticSqlService
     */
    private $analyticsSqlService;

    public function __construct()
    {
        $this->analyticsSqlService = new AnalyticSqlService();
    }

    /**
     * @return array|false
     */
    public function plansQuantities()
    {
        return ["now" => $this->getStoresCountByPlans(Carbon::now())];
    }

    /**
     * @param Carbon $date
     * @return array|false
     */
    public function plansQuantitiesDaysAgo(Carbon $date)
    {
        return ["sevenDaysAgo" => $this->getStoresCountByPlans($date)];
    }

    /**
     * @param Carbon $date
     * @return array
     */
    private function getStoresCountByPlans(Carbon $date)
    {
        $plans = [
            Plan::WITHOUT_PLAN_SLUG    => 0,
            Plan::PLAN_FREE_SLUG       => 0,
            Plan::PLAN_STANDARD_SLUG   => 0,
            Plan::PLAN_ADVANCED_SLUG   => 0,
            Plan::PLAN_ENTERPRISE_SLUG => 0,
        ];

        $sql = $this->analyticsSqlService->getPlanQuantitiesSql($date);

        $quantities = DB::select($sql);

        foreach ($quantities as $quantity) {
            $plans[Plan::getPlanSlug($quantity->plan)] = $quantity->count;
        }

        return $plans;
    }
}
