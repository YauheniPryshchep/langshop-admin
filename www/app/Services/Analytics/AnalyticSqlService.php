<?php

namespace App\Services\Analytics;

use Carbon\Carbon;

class AnalyticSqlService
{

    /**
     * @var string
     */
    private $connection;

    /**
     * AnalyticSqlService constructor.
     * @param string $connection
     */
    public function __construct(string $connection = 'langshop')
    {
        $this->connection = config('database.connections.' . $connection . '.database');
    }

    /**
     * @param Carbon $date
     * @return string
     */
    public function getPlanQuantitiesSql(Carbon $date)
    {
        return implode(" ", [
            $this->planQuantitiesBaseQuery(),
            $this->storeDataShopifyPlanJoin(),
            $this->storeSubscriptionWithChargesJoin($date),
            $this->storeSubscriptionInstallUninstallJoin($date),
            $this->storeSubscriptionCloseOpenLeftJoin($date),
            $this->planQuantitiesBaseWhereClause($date)
        ]);

    }

    /**
     * @return string
     */
    private function planQuantitiesBaseQuery()
    {
        return 'SELECT IFNULL(ss.plan_id, 0) as plan, COUNT(*) as count FROM ' . $this->connection . '.stores s';
    }

    /**
     * @return string
     */
    private function storeDataShopifyPlanJoin()
    {
        return "JOIN " . $this->connection . ".store_data sd ON sd.store_id = s.id AND sd.key = 'plan_display_name'";
    }

    /**
     * @param Carbon $date
     * @return string
     */
    private function storeSubscriptionWithChargesJoin(Carbon $date)
    {

        return 'LEFT JOIN ' . $this->connection . '.stores_subscriptions ss ON ss.id = (
	                SELECT tmp.id
                    FROM ' . $this->connection . '.stores_subscriptions tmp
                    WHERE 
		                tmp.store_id = s.id
                        AND tmp.created_at <= "' . $date . '"
                        AND (
			                (tmp.status = 2)
			                OR (tmp.status = 4 AND tmp.updated_at > "' . $date . '")
                        )
                    ORDER BY tmp.created_at DESC
                    LIMIT 1
                )';
    }

    /**
     * @param Carbon $date
     * @return string
     */
    private function storeSubscriptionInstallUninstallJoin(Carbon $date)
    {
        return 'JOIN ' . $this->connection . '.store_history shi ON shi.id = (
                    SELECT tmp.id
                    FROM ' . $this->connection . '.store_history tmp
                    WHERE 
		                tmp.store_id = s.id 
                        AND tmp.type IN ("relationship-installed", "relationship-uninstalled")
                        AND tmp.timestamp <= ' . $date->timestamp . '
                    ORDER BY tmp.timestamp DESC, id DESC
                    LIMIT 1
                )
                LEFT JOIN '. $this->connection . '.store_charges sc ON sc.charge_id = ss.charge_id';
    }

    /**
     * @param Carbon $date
     * @return string
     */
    private function storeSubscriptionCloseOpenLeftJoin(Carbon $date)
    {
        return 'LEFT JOIN ' . $this->connection . '.store_history sha ON sha.id = (
                    SELECT tmp.id
                    FROM ' . $this->connection . '.store_history tmp
                    WHERE 
		                tmp.store_id = s.id 
                        AND tmp.type IN ("relationship-reactivated", "relationship-deactivated")
                        AND tmp.timestamp <= ' . $date->timestamp . '
                    ORDER BY tmp.timestamp DESC, id DESC
                    LIMIT 1
                )';
    }

    /**
     * @param Carbon $date
     * @return string
     */
    private function planQuantitiesBaseWhereClause(Carbon $date)
    {
        return "WHERE 
	                shi.type = 'relationship-installed'
                    AND (
		                sha.type IS NULL
                        OR sha.type = 'relationship-reactivated'
	                )
                AND sd.value NOT IN ('affiliate', 'partner_test', 'staff', 'plus_partner_sandbox')
                AND (
		                sc.id IS NULL
                    OR (
			            sc.test = 0
                        AND (
				            sc.trial_ends_on IS NULL
                            OR sc.trial_ends_on <= '" . $date . "'
			            )
                    )
                )
                GROUP BY ss.plan_id";
    }
}