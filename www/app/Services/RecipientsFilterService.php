<?php

namespace App\Services;

use App\Requests\RecipientsFilterRequest;
use App\Models\CampaignRecipientsFilter;
use App\Exceptions\CustomException;

class RecipientsFilterService
{

    /**
     * Create campaign recipients filter
     * @param array $params
     * @return collection
     */
    public function storeFilter(array $params)
    {
        $validator = new RecipientsFilterRequest($params);
        $attributes = $validator->create();
        if (!is_array($attributes)) {
            return $attributes;
        }
        $filter = CampaignRecipientsFilter::create($attributes);
        return $filter;
    }

    /**
     * Get campaign recipients filter
     * @param int $id
     * @return collection
     * @throws CustomException
     */
    public function getFilter(int $id)
    {
        if (!$filter = CampaignRecipientsFilter::find($id)) {
            throw new CustomException(trans('errors.campaigns.404'), 404);
        }
        return $filter;
    }

    /**
     * Update campaign recipients filter
     * @param int $id
     * @param array $params
     * @return collection
     * @throws CustomException
     */
    public function updateFilter(int $id, array $params)
    {
        $validator = new RecipientsFilterRequest($params);
        $attributes = $validator->create();
        if (!is_array($attributes)) {
            return $attributes;
        }

        if (!$filter = CampaignRecipientsFilter::find($id)) {
            throw new CustomException(trans('errors.campaigns.404'), 404);
        }

        $filter->update($attributes);
        return $filter;
    }

    /**
     * Delete campaign recipients filter
     * @param int $id
     * @return void
     * @throws CustomException
     */
    public function deleteFilter(int $id)
    {
        if (!$filter = CampaignRecipientsFilter::find($id)) {
            throw new CustomException(trans('errors.campaigns.404'), 404);
        }

        $filter->delete();
        return;
    }

}
