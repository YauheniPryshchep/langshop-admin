<?php

namespace App\Services;

use App\Models\CampaignUnsubscribeList;
use App\Validators\StoreSubscribeValidators;
use Exception;

class StoreSubscribeService
{
    /**
     * @param array $params
     * @param string $domain
     * @return CampaignUnsubscribeList
     * @throws Exception
     */
    public function update(array $params, string $domain)
    {
        $query = new StoreSubscribeValidators($params);
        /**
         * @var CampaignUnsubscribeList $subscribe
         */
        $subscribe = CampaignUnsubscribeList::query()->where('domain', $domain)->first();

        $subscribe->delete();

        return $subscribe;
    }
}
