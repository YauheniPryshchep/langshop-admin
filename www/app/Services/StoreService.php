<?php

namespace App\Services;

use App\Models\Store;
use App\Traits\FilterTrait;
use App\Utilities\ActiveStoresFilter;
use App\Utilities\ClosedStoresFilter;
use App\Utilities\IsDemoFilter;
use App\Utilities\IsPartnerTestFilter;
use App\Utilities\IsTrialFilter;
use App\Utilities\LangShopPlanFilter;
use App\Utilities\LangShopVersionFilter;
use App\Utilities\ShopifyPlanFilter;
use App\Utilities\StoreNameFilter;
use App\Utilities\StoreSortFilter;
use App\Utilities\UsageCouponFilter;
use App\Utilities\UsageDiscountFilter;
use App\Validators\QueryPagination;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class StoreService
{
    use FilterTrait;
    /**
     * @var array
     */
    private $relatedFilters = [
        'search_term'   => StoreNameFilter::class,
        'active_status' => ActiveStoresFilter::class,
        'closed_status' => ClosedStoresFilter::class,
        'app_version'   => LangShopVersionFilter::class,
        'langshop_plan' => LangShopPlanFilter::class,
        'shopify_plan'  => ShopifyPlanFilter::class,
        'trial'         => IsTrialFilter::class,
        'demo'          => IsDemoFilter::class,
        'coupon'        => UsageCouponFilter::class,
        'discount'      => UsageDiscountFilter::class,
        'partner_test'  => IsPartnerTestFilter::class,
        'sort'          => StoreSortFilter::class,
    ];

    /**
     * @var array
     */
    private $filterValidation = [
        'tab'         => 'string',
        'search_term' => 'string',
    ];
    /**
     * @var Builder
     */
    private $baseQuery;

    public function __construct()
    {
        $this->baseQuery = Store::query()
            ->with('lastEvent')
            ->with('users')
            ->whereNotNull('token')
            ->where('token', "!=", "");
    }

    /**
     * @param array $attributes
     * @return array
     */
    public function stores(array $attributes)
    {
        $this->setFilterValidation(array_merge($this->filterValidation, [
            "store_status"  => "string",
            "sort"          => "string",
            "active_status" => "string",
            "closed_status" => "string",
            "langshop_plan" => "string",
            "shopify_plan"  => "string",
            "demo"          => "string",
            "partner_test"  => "string",
            "trial"         => "string",
            "coupon"        => "string",
            "discount"      => "string",
            "app_version"   => [
                "string",
                Rule::in([
                    "v1",
                    "v2"
                ])
            ]
        ]));
        return $this->getCollection($attributes);
    }

    /**
     * @param array $filterValidation
     */
    public function setFilterValidation(array $filterValidation): void
    {
        $this->filterValidation = $filterValidation;
    }

    /**
     * @param Builder $baseQuery
     */
    public function setBaseQuery(Builder $baseQuery): void
    {
        $this->baseQuery = $baseQuery;
    }

    /**
     * @param array $attributes
     * @return array
     */
    private function getCollection(array $attributes)
    {
        $pagination = new QueryPagination($attributes);

        $validator = Validator::make($attributes, $this->filterValidation);
        $filters = $validator->validate();

        $query = $this->baseQuery;

        $query = $this->filterBy($filters, $query, $this->relatedFilters);

        $count = $query->count();

        /**
         * @var LengthAwarePaginator $paginator
         */
        $paginator = $query->paginate(
            $pagination->getLimit(),
            ['*'],
            'page',
            $pagination->getPage()
        );

        $totalPages = ceil($count / $pagination->getLimit());

        return [
            "items" => $paginator->getCollection(),
            "count" => $count,
            "page"  => $pagination->getPage(),
            "pages" => $totalPages
        ];
    }

}
