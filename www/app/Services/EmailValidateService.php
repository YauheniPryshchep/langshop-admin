<?php

namespace App\Services;

use App\Exceptions\Http\HttpError;
use App\Exceptions\Http\InternalError;
use App\Services\Guzzle\HttpClient;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;
use Psr\Http\Message\ResponseInterface;

class EmailValidateService
{
    const MAX_REQUEST_RETRIES_ATTEMPTS = 3;
    const SAFE_STATUS = "safe";
    const UNKNOWN_STATUS = "unknown";
    const INVALID_STATUS = "invalid";
    const RISKY_STATUS = "risky";
    /**
     * @var array
     */
    private $emails;
    /**
     * @var HttpClient
     */
    private $httpClient;

    /**
     * @var int
     */
    private $emailsPerRequest;

    /**
     * @var string
     */
    private $apiKey;

    /**
     * EmailValidateService constructor.
     */
    public function __construct()
    {
        $this->emailsPerRequest = config('email-validator.batchLimit');
        $this->apiKey = config('email-validator.apiKey');

        $this->httpClient = new HttpClient([
            'max_retry_attempts' => self::MAX_REQUEST_RETRIES_ATTEMPTS,
            "timeout"            => 60,
            'headers'            => [
                'Accept' => 'application/json',
                'apiKey' => $this->apiKey,
            ]
        ]);

    }

    /**
     * @param string $email
     * @return array
     */
    public function validate(string $email)
    {
        $results = [];

        $response = $this->request(
            'POST',
            "https://email.devit-team.com/api/v1/email",
            [
                'json' => [
                    "email" => $email
                ],
            ]
        );

        $response = json_decode($response->getBody(), true);

        $results = array_merge($results, Arr::get($response, 'data', []));

        return $results;
    }

    /**
     * @param array $email
     * @return bool
     */
    public function isValidEmail(array $email): bool
    {
        $email = json_decode(json_encode($email), true);
        return $this->isValid($email);
    }

    /**
     * @param string $method
     * @param string $url
     * @param array $options
     * @return ResponseInterface
     * @throws HttpError
     */
    public function request(string $method, string $url, array $options = []): ResponseInterface
    {
        try {
            $response = $this->httpClient->request($method, $url, $options);
        } catch (BadResponseException $e) {
            $response = $e->getResponse();
        } catch (GuzzleException $e) {
            throw new InternalError(
                4001,
                "Email Verification api error",
                $e->getTrace()
            );
        } catch (\Throwable $exception) {
            ob_start();
            var_dump('Exception');
            var_dump($exception->getMessage());
            Log::info(ob_get_clean());
        }

        return $response;
    }

    /**
     * @param array $email
     * @return string
     */
    private function getReachableStatus(array $email): string
    {
        return $email['is_reachable'];
    }

    /**
     * @param array $email
     * @return bool
     */
    private function isValid(array $email): bool
    {
        $is_reachable = $this->getReachableStatus($email);

        if ($is_reachable === self::SAFE_STATUS) {
            return true;
        }

        if (in_array($is_reachable, [self::INVALID_STATUS])) {
            return false;
        }

        return $this->isSyntaxValid($email)
            && $this->isAccept($email)
            && $this->isDeliverable($email);
    }

    /**
     * @param array $email
     * @return bool
     */
    private function isSyntaxValid(array $email): bool
    {
        return Arr::get($email, 'syntax.is_valid_syntax', false);
    }

    /**
     * @param array $email
     * @return bool
     */
    private function isDeliverable(array $email): bool
    {
        if ($this->isSmtpError($email)) {
            return true;
        }

        return Arr::get($email, 'smtp.is_deliverable', false);
    }

    /**
     * @param array $email
     * @return bool
     */
    private function isSmtpError(array $email): bool
    {
        return Arr::get($email, 'smtp.error', false);
    }

    /**
     * @param array $email
     * @return bool
     */
    private function isAccept(array $email): bool
    {
        return Arr::get($email, 'mx.accepts_mail', false);
    }

}