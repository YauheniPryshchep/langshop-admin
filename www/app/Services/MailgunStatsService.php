<?php

namespace App\Services;

use App\Exceptions\Http\BadRequestError;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Log;

class MailgunStatsService extends MailGunClient
{

    /**
     * @var string
     */
    private $credentials;

    /**
     * @var string
     */
    protected $url;

    /**
     * @var array
     */
    private $connections = [];

    /**
     * MailGunService constructor.
     */
    public function __construct(string $credentials)
    {
        $this->credentials = $credentials;
        $this->connections = config('mailgun.' . $this->credentials);
        $this->url = $this->connections['domain'] . '/tags';
    }

    /**
     * @param string $tag
     * @param $started_at
     * @return array
     */
    public function getTagStats(string $tag, $started_at)
    {

        $started_at =  Carbon::parse($started_at)->getTimestamp();

          try {
            $response = $this->request($this->credentials, 'GET', $this->url . '/' . $tag . '/stats?event=delivered&event=failed&resolution=month&start=' . $started_at);
            return json_decode($response->getBody(), true);
        } catch (Exception $e) {
            throw new BadRequestError();
        }
    }
}
