<?php

namespace App\Services\AppsLocales;

use App\Exceptions\CustomException;
use App\Models\User;
use CupOfTea\GitWrapper\Facades\Git;
use Exception;
use GitWrapper\GitWorkingCopy;
use GitWrapper\GitWrapper;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class LocalesService
{
    /**
     * @var array
     */
    private $requiredFields = [
        'auth',
        'key_path',
        'path',
        'repository',
        'branch',
        'folder'
    ];

    /**
     * @var GitWrapper
     */
    private $gitWrapper;

    /**
     * @var GitWorkingCopy
     */
    private $git;

    /**
     * @var array
     */
    private $config;

    /**
     * @var Filesystem
     */
    private $storage;

    /**
     * @var PhpLocalesManager
     */
    private $manager;

    /**
     * @var string
     */
    private $defaultLocale;

    /**
     * LocalesService constructor.
     * @throws CustomException
     */
    public function __construct()
    {
        $this->validate();
        $this->init();
    }

    /**
     * Validate to make sure, all fields is filled and exists
     * @throws CustomException
     */
    private function validate()
    {
        $this->config = array_merge(
            config('git.connections.langshop'),
            config('localization.git')
        );

        // Make sure all fields is filled in config
        foreach ($this->requiredFields as $field):
            if (isset($this->config[$field]) == false || $this->config[$field] == NULL || $this->config[$field] == ''):
                throw new CustomException('Error: Missing configuration to work with GIT repository of langshop', 500);
            endif;
        endforeach;
    }

    /**
     * Initialize GitWrapper Facade
     * @throws CustomException
     */
    private function init()
    {
        $this->storage = Storage::disk('git');
        $this->manager = new PhpLocalesManager($this->storage);
        $this->defaultLocale = config('localization.default_locale');

        try {
            // https://github.com/CupOfTea696/Laravel-GitWrapper
            $this->gitWrapper = Git::connection('langshop');
            $this->gitWrapper->setTimeout(600);
            $this->gitWrapper->setPrivateKey($this->config['key_path']);

            $this->git = $this->gitWrapper->workingCopy($this->config['path']);
            if ($this->git->isCloned() == false) {
                // Clone repository
                $this->git = $this->gitWrapper->init($this->config['path']);
                $this->git->remote('add', 'origin', $this->config['repository']);

                // I want to clone only Specific folder instead of whole project
                $this->git->config('core.sparseCheckout', 'true');
                $this->storage->put('langshop/.git/info/sparse-checkout', $this->config['folder']);
            }

            // Reset
//            $this->git->clean('-d', '-f');
//            $this->git->reset(['hard' => true]);

            // Rebase branch
            $this->git->pull('origin', $this->config['branch'], '--depth=1', '--rebase');

            // Switch to branch
            $this->git->checkout($this->config['branch']);
        } catch (Exception $e) {
            throw new CustomException($e->getMessage(), 500);
        }
    }

    /**
     * Push all changes to remote repository
     * @param string $locale
     * @param string $file
     */
    private function push(string $locale, string $file)
    {
        $this->auth();

        $language = locale_get_display_language($locale);

        $this->git->add('.');
        $this->git->commit("(text) Updated {$language} {$file} localization");
        $this->git->push('origin', $this->config['branch']);
    }

    /**
     * Authorize for git user
     */
    private function auth()
    {
        /**
         * Let's get know who did changes in locales. Who is Fucked up ?))
         *
         * @var User $user
         */
        $user = Auth::user();

        $this->git->config('user.name', $user->name);
        $this->git->config('user.email', $user->email);
    }

    /**
     * @return array
     * @throws FileNotFoundException
     * @description Return List of Locales , based on filter and pagination query
     */
    public function getLocales()
    {
        $languages = $this->storage->directories('langshop/' . $this->config['folder']);
        $filesSources = $this->getFilesSources();

        $response = [];
        foreach ($languages as &$language) {
            $code = basename($language);

            $response[] = [
                'iso'      => $code,
                'title'    => locale_get_display_language($code),
                'progress' => $this->getFilesProgress($code, $filesSources)
            ];
        }

        return $response;
    }

    /**
     * @param string $file
     * @return int
     * @description Return total count of existing languages
     */
    public function getTotalLocales()
    {
        return 0;
    }

    /**
     * @param string $locale
     * @return array
     * @throws FileNotFoundException
     * @description Return List of Locales , based on filter and pagination query
     */
    public function getLocaleFiles(string $locale)
    {
        $files = $this->getFiles('langshop/' . $this->config['folder'] . '/' . $this->defaultLocale);

        $response = [];
        foreach ($files as &$file) {
            $name = basename($file);

            $response[] = [
                'name'     => basename($file),
                'title'    => str_replace(['.php', '_'], ['', ' '], ucfirst(basename($file))),
                'progress' => $this->getFileProgress($locale, $name)
            ];
        }

        return $response;
    }

    /**
     * @return int
     * @description Return total count of existing languages
     */
    public function getTotalLocaleFiles()
    {
        try {
            $files = $this->getFiles('langshop/' . $this->config['folder'] . '/' . $this->defaultLocale);
            return count($files);
        } catch (Exception $exception) {
            return 0;
        }

    }

    /**
     * @param string $locale
     * @param string $file
     * @return array|mixed
     * @throws FileNotFoundException
     */
    public function getFile(string $locale, string $file)
    {
        $items = [];

        $source = $this->manager->get('langshop/' . $this->config['folder'] . '/' . $this->defaultLocale . '/' . $file);

        if ($locale !== $this->defaultLocale) {
            try {
                $target = $this->manager->get('langshop/' . $this->config['folder'] . '/' . $locale . '/' . $file);
            } catch (FileNotFoundException $e) {
                $target = [];
            }

            foreach ($source as $key => $value) {
                $items[] = [
                    'label'  => $this->getItemLabel($key),
                    'key'    => $key,
                    'source' => $value,
                    'target' => $target[$key] ?? '',
                ];
            }
        } else {
            foreach ($source as $key => $value) {
                $items[] = [
                    'label'  => $this->getItemLabel($key),
                    'key'    => $key,
                    'source' => null,
                    'target' => $value,
                ];
            }
        }

        return $items;
    }

    /**
     * @param string $locale
     * @param string $file
     * @param array $input
     * @return array|mixed
     * @throws CustomException
     * @throws FileNotFoundException
     */
    public function updateFile(string $locale, string $file, array $input)
    {
        $locales = [];
        $keys = array_flip(array_column($input, 'key'));

        $items = array_reduce(
            $this->getFile($locale, $file),
            function ($items, $item) use (&$locales, $input, $keys) {
                if (isset($keys[$item['key']])) {
                    $item['target'] = $input[$keys[$item['key']]]['target'];
                }

                $locales[$item['key']] = $item['target'];

                $items[] = $item;

                return $items;
            },
            []
        );

        $this->manager->update(
            $this->manager->template('langshop/' . $this->config['folder'] . '/' . $this->defaultLocale . '/' . $file),
            'langshop/' . $this->config['folder'] . '/' . $locale . '/' . $file,
            $locales
        );

        $this->push($locale, $file);

        return $items;
    }

    /**
     * @param string $directory
     * @return array
     */
    private function getFiles(string $directory): array
    {
        return array_reduce(
            $this->storage->files($directory),
            function ($files, $file) {
                $name = basename($file);

                if ($name === '.gitkeep') {
                    return $files;
                }

                $files[] = $file;

                return $files;
            },
            []
        );
    }

    /**
     * @param string $key
     * @return string
     */
    private function getItemLabel(string $key): string
    {
        $parts = array_map(
            'ucfirst',
            explode(PhpLocalesManager::KEY_SEPARATOR, $key));

        return join(' :: ', $parts);
    }

    /**
     * @param string $locale
     * @param array $filesSources
     * @return float
     */
    private function getFilesProgress(string $locale, array $filesSources): float
    {
        if ($locale === $this->defaultLocale) {
            return 100;
        }

        $total = 0;
        $translated = 0;

        foreach ($filesSources as $file => $locales) {
            $total += count(array_keys($locales));

            try {
                $targets = $this->manager->get('langshop/' . $this->config['folder'] . '/' . $locale . '/' . $file);
            } catch (FileNotFoundException $e) {
                continue;
            }

            foreach ($locales as $key => $value) {
                if (empty($targets[$key])) {
                    continue;
                }

                $translated++;
            }
        }

        return round($translated / $total * 100, 2);
    }

    /**
     * @param string $locale
     * @param string $file
     * @return float
     * @throws FileNotFoundException
     */
    private function getFileProgress(string $locale, string $file): float
    {
        if ($locale === $this->defaultLocale) {
            return 100;
        }

        $items = $this->getFile($locale, $file);
        $total = count($items);
        $translated = 0;

        foreach ($this->getFile($locale, $file) as $item) {
            if (empty($item['target'])) {
                continue;
            }

            $translated++;
        }

        return round($translated / $total * 100, 2);
    }

    /**
     * @return array
     * @throws FileNotFoundException
     */
    private function getFilesSources(): array
    {
        $filesSources = [];

        $files = $this->getFiles('langshop/' . $this->config['folder'] . '/' . $this->defaultLocale);
        foreach ($files as $file) {
            $name = basename($file);
            $filesSources[$name] = $this->manager->get('langshop/' . $this->config['folder'] . '/' . $this->defaultLocale . '/' . $name);
        }

        return $filesSources;
    }
}