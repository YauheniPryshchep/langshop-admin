<?php

namespace App\Services\AppsLocales;

use App\Exceptions\CustomException;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Contracts\Filesystem\Filesystem;
use RecursiveArrayIterator;
use RecursiveIteratorIterator;
use Throwable;

class PhpLocalesManager
{
    const KEY_SEPARATOR = '//';
    const LINE_OFFSET   = '    ';

    /**
     * @var Filesystem
     */
    private $storage;

    /**
     * PhpLocalesManager constructor.
     * @param Filesystem $storage
     */
    public function __construct(Filesystem $storage)
    {
        $this->storage = $storage;
    }

    /**
     * @param string $path
     * @return array
     * @throws FileNotFoundException
     */
    public function get(string $path): array
    {
        $content = $this->storage->get($path);

        return $this->parseLocales($content);
    }

    /**
     * @param string $template
     * @param string $path
     * @param array $locales
     * @throws CustomException
     */
    public function update(string $template, string $path, array $locales)
    {
        $items = [];

        foreach ($locales as $key => $value) {
            $this->setItem($items, $key, $value);
        }

        $content = sprintf(
            $template,
            $this->printArray(
                $items,
                function ($key) {
                    if (preg_match("/^[$][\w]+$/", $key)) {
                        return $key;
                    }

                    if (preg_match("/^[A-Z][\w]+[:]{2}[A-Z0-9_]+$/", $key)) {
                        return $key;
                    }

                    return "\"{$key}\"";
                }
            )
        );

        if (!$this->isEqualLocales($locales, $this->parseLocales($content))) {
            throw new CustomException('Creating php array for current locales failed', 500);
        }

        $this->storage->put($path, $content);
    }

    /**
     * @param string $path
     * @return string
     * @throws FileNotFoundException
     */
    public function template(string $path): string
    {
        $content = $this->storage->get($path);

        if (!preg_match('/return[\s]*\[/', $content, $matches, PREG_OFFSET_CAPTURE)) {
            return '<?php' . PHP_EOL
                . 'return %s';
        }

        return substr($content, 0, $matches[0][1])
            . 'return %s';
    }

    /**
     * @param string $content
     * @return array
     */
    private function parseLocales(string $content): array
    {
        $locales = [];

        // Catch if goes wrong something..
        try {
            $items = eval($this->escapeStatements($content));
        } catch (Throwable $t) {
            $items = [];
        }

        $arrayIterator     = new RecursiveArrayIterator($items);
        $recursiveIterator = new RecursiveIteratorIterator($arrayIterator);

        foreach ($recursiveIterator as $value) {
            $path = [];

            foreach (range(0, $recursiveIterator->getDepth()) as $depth) {
                $path[] = $recursiveIterator->getSubIterator($depth)->key();
            }

            $locales[join(self::KEY_SEPARATOR, $path)] = $value;
        }

        return $locales;
    }

    /**
     * @param string $content
     * @return string
     */
    private function escapeStatements(string $content): string
    {
        $content = str_replace('<?php', '', $content);
        $content = preg_replace('/^use[\s]/', '//', $content);
        $content = preg_replace('/([\w:_]+?)[\s]*(?==>[\s]*["\'[])/', "\"\$1\"", $content);

        return $content;
    }

    /**
     * @param array $items
     * @param string $key
     * @param string $value
     * @return array|mixed
     */
    private function setItem(array &$items, string $key, string $value)
    {
        $parts = explode(self::KEY_SEPARATOR, $key);

        while (count($parts) > 1) {
            $key = array_shift($parts);

            if (!isset($items[$key]) || !is_array($items[$key])) {
                $items[$key] = [];
            }

            $items = &$items[$key];
        }

        $items[array_shift($parts)] = $value;

        return $items;
    }

    /**
     * @param array $items
     * @param callable|null $printArrayKey
     * @param callable|null $printLocaleValue
     * @param int $depth
     * @return string
     */
    private function printArray(array $items, callable $printArrayKey = null, callable $printLocaleValue = null, int $depth = 0): string
    {
        $offset = '';
        for ($i = 0; $i < $depth; $i++) {
            $offset .= self::LINE_OFFSET;
        }

        $content = "[" . PHP_EOL;

        if ($this->isSimpleArray($items)) {
            $keys  = array_keys($items);
            $count = count($keys);

            for ($i = 0; $i < $count; $i++) {
                $item    = $items[$keys[$i]];
                $content .= $offset . self::LINE_OFFSET;

                if (is_array($item)) {
                    $content .= $this->printArray(
                        $item,
                        $printArrayKey,
                        $printLocaleValue,
                        $depth + 1
                    );

                    continue;
                }

                $content .= $printLocaleValue
                    ? call_user_func($printLocaleValue, $item)
                    : $this->escapeLocaleValue($item);

                $content .= "," . PHP_EOL;
            }
        } else {
            foreach ($items as $key => $item) {
                $content .= $offset . self::LINE_OFFSET;

                $content .= $printArrayKey
                    ? call_user_func($printArrayKey, $key)
                    : "\"{$key}\"";

                $content .= " => ";

                if (is_array($item)) {
                    $content .= $this->printArray(
                        $item,
                        $printArrayKey,
                        $printLocaleValue,
                        $depth + 1
                    );

                    continue;
                }

                $content .= $printLocaleValue
                    ? call_user_func($printLocaleValue, $item)
                    : $this->escapeLocaleValue($item);

                $content .= "," . PHP_EOL;
            }
        }

        $content .= "{$offset}]";

        if ($depth > 0) {
            $content .= ',' . PHP_EOL;
        } else {
            $content .= ';';
        }

        return $content;
    }

    /**
     * @param array $items
     * @return bool
     */
    private function isSimpleArray(array $items): bool
    {
        $keys = array_keys($items);
        for ($i = 0; $i < count($keys); $i++) {
            if ((string)$keys[$i] === (string)$i) {
                continue;
            }

            return false;
        }

        return true;
    }

    /**
     * @param array $first
     * @param array $second
     * @return bool
     */
    private function isEqualLocales(array $first, array $second): bool
    {
        $firstKeys  = array_keys($first);
        $secondKeys = array_keys($second);

        if (count($firstKeys) !== count($secondKeys)) {
            return false;
        }

        foreach ($firstKeys as $i => $key) {
            if (!array_key_exists($i, $secondKeys)) {
                return false;
            }

            if ($secondKeys[$i] !== $key) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param string $value
     * @return string
     */
    private function escapeLocaleValue(string $value): string
    {
        $value = str_replace('\\', '\\\\', $value);
        $value = str_replace('"', '\"', $value);

        return "\"" . $value . "\"";
    }
}