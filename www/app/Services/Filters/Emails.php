<?php

namespace App\Services\Filters;

use App\Services\Filters\FilterInterface;
use App\Services\Filters\JoinsInterface;

class Emails implements FilterInterface, JoinsInterface
{
    /**
     * @param string $comparator
     * @param mixed $value
     * @param string $type
     * @return string
     */
    public function make(string $comparator, $value, string $type): string
    {
        $column = $type === Resolver::PARTNERS_QUERY_TYPE
            ? 'leads_emails.email '
            : 'store_data_emails.key = "email" AND store_data_emails.value ';

        switch ($comparator) {
            case 'is':
                $sql = $column . '  IN ("' . implode('","', $value) . '")';
                break;
            case 'is_not':
                $sql = $column . ' NOT IN ("' . implode('","', $value) . '")';
                break;
            default:
                $sql = 1;
        }
        return $sql;
    }

    /**
     * @param string $comparator
     * @param $value
     * @return array
     */
    public function typesQuery(string $comparator, $value): array
    {
        return Resolver::BOTH_QUERY_TYPE;
    }

    /**
     * @param string $langshop
     * @param string $dashboard
     * @param string $type
     * @return array
     */
    public function makeJoins(string $langshop, string $dashboard, string $type): array
    {
        if ($type === Resolver::LANGSHOP_QUERY_TYPE) {
            return ["store_data_emails" => 'LEFT JOIN ' . $langshop . '.store_data AS store_data_emails '
                . 'ON ' . $type . '.id = store_data_emails.store_id '];
        }

        return ["leads_emails" => 'LEFT JOIN ' . $dashboard . '.leads_emails AS leads_emails '
            . 'ON ' . $type . '.shopify_domain = leads_emails.domain '];

    }
}
