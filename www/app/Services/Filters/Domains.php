<?php

namespace App\Services\Filters;

use App\Services\Filters\FilterInterface;

class Domains implements FilterInterface
{
    /**
     * @param string $comparator
     * @param mixed $value
     * @param string $type
     * @return string
     */
    public function make(string $comparator, $value, string $type): string
    {
        $column = $type === Resolver::PARTNERS_QUERY_TYPE ? $type.'.shopify_domain' : $type. '.name';

        switch ($comparator) {
            case 'is':
                $sql = $column.'  IN ("' . implode('","', $value) .'")';
                break;
            case 'is_not':
                $sql = $column.' NOT IN ("' . implode('","', $value) .'")';
                break;
            default:
                $sql = 1;
        }
        return $sql;
    }

    public function typesQuery(string $comparator, $value): array
    {

        return Resolver::BOTH_QUERY_TYPE;
    }
}
