<?php

namespace App\Services\Filters;

use App\Services\Filters\FilterInterface;
use App\Services\Filters\JoinsInterface;
use Illuminate\Support\Facades\Log;

class InCampaign implements FilterInterface
{
    /**
     * @var string
     */
    protected $dashboard;

    /**
     * InCampaign constructor.
     */
    public function __construct()
    {
        $this->dashboard = config('database.connections.dashboard.database');

    }

    /**
     * @param string $comparator
     * @param mixed $value
     * @param string $type
     * @return string
     */
    public function make(string $comparator, $value, string $type): string
    {
        if (!is_array($value)) {
            return 1;
        }
        $column = $type === Resolver::PARTNERS_QUERY_TYPE ? $type . '.shopify_domain' : $type . '.name';

        switch ($comparator) {
            case 'in':
                if ($value != 0) {
                    $sql = ' ' . $column . ' IN (
                        SELECT licd.domain FROM ' . $this->dashboard . '.campaign_domains as licd WHERE licd.campaign_id IN ("' . implode('","', $value) . '")
		            )';
                } else {
                    $sql = ' ' . $column . ' NOT IN (
                        SELECT licd.domain FROM ' . $this->dashboard . '.campaign_domains as licd WHERE licd.campaign_id IS NOT NULL 
		            )';
                }
                break;
            case 'in_not':
                if ($value != 0) {
                    $sql = ' ' . $column . ' NOT IN (
                        SELECT licd.domain FROM ' . $this->dashboard . '.campaign_domains as licd WHERE licd.campaign_id IN ("' . implode('","', $value) . '")
		            )';
                } else {
                    $sql = ' ' . $column . ' NOT IN (
                        SELECT licd.domain FROM ' . $this->dashboard . '.campaign_domains as licd WHERE licd.campaign_id IS NULL 
		            )';
                }
                break;
            default:
                $sql = 1;
        }
        return $sql;
    }

    /**
     * @param string $comparator
     * @param $value
     * @return array
     */
    public function typesQuery(string $comparator, $value): array
    {
        return [Resolver::LANGSHOP_QUERY_TYPE, Resolver::PARTNERS_QUERY_TYPE];
    }

}
