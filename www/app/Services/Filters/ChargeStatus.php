<?php

namespace App\Services\Filters;

use App\Services\Filters\FilterInterface;
use App\Services\Filters\JoinsInterface;

class ChargeStatus implements FilterInterface, JoinsInterface
{

    /**
     * @param string $comparator
     * @param mixed $value
     * @param string $type
     * @return string
     */
    public function make(string $comparator, $value, string $type): string
    {
        if (!in_array($value, ['accepted', 'declined', 'frozen'])) {
            return 1;
        }

        switch ($comparator) {
            case 'is':
                $sql = 'charge_status.status="' . $value . '" ';
                break;
            case 'is_not':
                $sql = 'charge_status.status!="' . $value . '" ';
                break;
            default:
                $sql = 1;
        }
        return $sql;
    }

    /**
     * @param string $langshop
     * @param string $dashboard
     * @param string $type
     * @return array
     */
    public function makeJoins(string $langshop, string $dashboard, string $type): array
    {
        return ["charge_status" => 'LEFT JOIN ' . $langshop . '.store_charges AS charge_status '
            . 'ON ' . $type . '.id = charge_status.id '];
    }


    /**
     * @param string $comparator
     * @param $value
     * @return array
     */
    public function typesQuery(string $comparator, $value): array
    {
        return [Resolver::LANGSHOP_QUERY_TYPE];
    }
}
