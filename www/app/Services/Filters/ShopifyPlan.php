<?php

namespace App\Services\Filters;

use App\Services\Filters\FilterInterface;
use App\Services\Filters\JoinsInterface;

class ShopifyPlan implements FilterInterface, JoinsInterface
{

    /**
     * @param string $comparator
     * @param mixed $value
     * @param string $type
     * @return string
     */
    public function make(string $comparator, $value, string $type): string
    {
        $plans = [
            'affiliate',
            'basic',
            'business',
            'cancelled',
            'custom',
            'dormant',
            'fraudulent',
            'frozen',
            'npo_full',
            'npo_lite',
            'open_learning',
            'partner_test',
            'paused',
            'plus_partner_sandbox',
            'professional',
            'shopify_plus',
            'staff',
            'staff_business',
            'starter',
            'trial',
            'unlimited'
        ];
        if (!in_array($value, $plans)) {
            return 1;
        }

        switch ($comparator) {
            case 'is':
                $sql = 'store_data_shopify_plan.key="plan_display_name" AND store_data_shopify_plan.value="' . $value . '" ';
                break;
            default:
                $sql = 1;
        }
        return $sql;
    }

    /**
     * @param string $langshop
     * @param string $dashboard
     * @param string $type
     * @return array
     */
    public function makeJoins(string $langshop, string $dashboard, string $type): array
    {
        return ["store_data_shopify_plan" => 'LEFT JOIN ' . $langshop . '.store_data AS store_data_shopify_plan '
            . 'ON ' . $type . '.id = store_data_shopify_plan.store_id '];
    }

    public function typesQuery(string $comparator, $value): array
    {
        return [Resolver::LANGSHOP_QUERY_TYPE];
    }

}
