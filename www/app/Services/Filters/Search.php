<?php

namespace App\Services\Filters;

use App\Services\Filters\FilterInterface;

class Search implements FilterInterface
{
    /**
     * @param string $comparator
     * @param mixed $value
     * @param string $type
     * @return string
     */
    public function make(string $comparator, $value, string $type): string
    {
        $column = $type === Resolver::PARTNERS_QUERY_TYPE ? $type.'.shopify_domain' : $type. '.name';

        switch ($comparator) {
            case 'like':
                $sql = $column.' like "%'.$value.'%"';
                break;
            default:
                $sql = 1;
        }
        return $sql;
    }

    /**
     * @param string $comparator
     * @param $value
     * @return array
     */
    public function typesQuery(string $comparator, $value): array
    {
        return Resolver::BOTH_QUERY_TYPE;
    }
}
