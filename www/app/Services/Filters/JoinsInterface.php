<?php

namespace App\Services\Filters;

interface JoinsInterface
{

    /**
     * @param string $langshop
     * @param string $dashboard
     * @param string $type
     * @return array
     */
    public function makeJoins(string $langshop, string $dashboard, string $type): array ;

}
