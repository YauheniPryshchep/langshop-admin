<?php

namespace App\Services\Filters;

use App\Services\Filters\FilterInterface;
use App\Services\Filters\JoinsInterface;

class TimeZone implements FilterInterface, JoinsInterface
{

    /**
     * @param string $comparator
     * @param mixed $value
     * @param string $type
     * @return string
     */
    public function make(string $comparator, $value, string $type): string
    {
        if (substr_count($value, '/') < 1) {
            return 1;
        }
        switch ($comparator) {
            case 'is':
                $sql = 'store_data_timezone.key="iana_timezone" AND store_data_timezone.value="' . $value . '" ';
                break;
            default:
                $sql = 1;
        }
        return $sql;
    }

    /**
     * @param string $langshop
     * @param string $dashboard
     * @param string $type
     * @return array
     */
    public function makeJoins(string $langshop, string $dashboard, string $type): array
    {
        return ["store_data_timezone" => 'LEFT JOIN ' . $langshop . '.store_data AS store_data_timezone '
            . 'ON ' . $type . '.id = store_data_timezone.store_id '];
    }

    /**
     * @param string $comparator
     * @param $value
     * @return array
     */
    public function typesQuery(string $comparator, $value): array
    {
        return [Resolver::LANGSHOP_QUERY_TYPE];
    }

}
