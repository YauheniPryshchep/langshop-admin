<?php

namespace App\Services\Filters;

use App\Services\Filters\FilterInterface;

class ActiveStatus implements FilterInterface
{
    /**
     * @param string $comparator
     * @param mixed $value
     * @param string $type
     * @return string
     */
    public function make(string $comparator, $value, string $type): string
    {

        switch ([$comparator, $value]) {
            case ['is', 'installed']:
                $sql = $type.'.active=1';
                break;
            case ['is', 'uninstalled']:
            case ['is_not', 'installed']:
                $sql = $type.'.active=0';
                break;
            case ['was_never', 'installed']:
                $sql = $type.'.active IS NULL';
                break;
            default:
                $sql = 1;
        }
        return $sql;
    }

    public function typesQuery(string $comparator, $value): array
    {
        if ($comparator === 'was_never') {
            return [Resolver::PARTNERS_QUERY_TYPE];
        }

        return [Resolver::LANGSHOP_QUERY_TYPE];
    }
}
