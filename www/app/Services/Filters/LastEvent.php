<?php

namespace App\Services\Filters;

use App\Services\Filters\FilterInterface;
use App\Services\Filters\JoinsInterface;
use Carbon\Carbon;

class LastEvent implements FilterInterface, JoinsInterface
{

    /**
     * @param string $comparator
     * @param mixed $value
     * @param string $type
     * @return string
     */
    public function make(string $comparator, $value, string $type): string
    {
        if (!$value['start'] || !$value['end']) {
            return 1;
        }

        $startTimestamp = Carbon::parse($value['start'])->timestamp;
        $endTimestamp = Carbon::parse($value['end'])->timestamp;

        if ($startTimestamp === $endTimestamp) {
            $endTimestamp = $endTimestamp + 23 * 59 * 59;
        }

        $sql = 'store_history.type="' . $comparator . '" AND store_history.timestamp>' .
            $startTimestamp . ' '
            . 'AND store_history.timestamp<' . $endTimestamp . ' ';
        return $sql;
    }

    /**
     * @param string $langshop
     * @param string $dashboard
     * @param string $type
     * @return array
     */
    public function makeJoins(string $langshop, string $dashboard, string $type): array
    {
        return ["store_history" => 'LEFT JOIN ' . $langshop . '.store_history AS store_history '
            . 'ON ' . $type . '.id = store_history.store_id '];
    }

    /**
     * @param string $comparator
     * @param $value
     * @return array
     */
    public function typesQuery(string $comparator, $value): array
    {
        return [Resolver::LANGSHOP_QUERY_TYPE];
    }
}
