<?php

namespace App\Services\Filters;

interface FilterInterface
{

    /**
     * @param string $comparator
     * @param mixed $value
     * @param string $type
     * @return string
     */
    public function make(string $comparator, $value, string $type): string;

    /**
     * @param string $comparator
     * @param $value
     * @return array
     */
    public function typesQuery(string $comparator, $value): array ;
}
