<?php

namespace App\Services\Filters;

use App\Requests\RecipientsFilterRequest;
use Illuminate\Support\Facades\DB;

class Resolver
{

    /**
     * @var array
     */
    protected $filters = [];

    /**
     * @var string
     */
    protected $page;

    /**
     * @var string
     */
    protected $limit;

    /**
     * @var string
     */
    protected $langshop;

    /**
     * @var string
     */
    protected $dashboard;

    /**
     * @var array
     */
    protected $queries;

    const LANGSHOP_QUERY_TYPE = 'langshop';
    const PARTNERS_QUERY_TYPE = 'partners';
    const BOTH_QUERY_TYPE = [self::LANGSHOP_QUERY_TYPE, self::PARTNERS_QUERY_TYPE];

    const FILTER_ACTIVE_STATUS = 'active_status';
    const FILTER_CHARGE_STATUS = 'charge_status';
    const FILTER_SHOPIFY_PLAN = 'shopify_plan';
    const FILTER_INSTALLATION_DATE = 'installation_date';
    const FILTER_TIMEZONE = 'timezone';
    const FILTER_LAST_EVENT = 'last_event';
    const FILTER_IN_CAMPAIGN = 'in_campaign';
    const FILTER_SUBSCRIBE = 'subscribe';
    const FILTER_DOMAINS = 'domains';
    const FILTER_EMAILS = 'emails';
    const FILTER_SEARCH = 'search';
    const FILTERS_CLASSES = [
        self::FILTER_ACTIVE_STATUS     => ActiveStatus::class,
        self::FILTER_CHARGE_STATUS     => ChargeStatus::class,
        self::FILTER_SHOPIFY_PLAN      => ShopifyPlan::class,
        self::FILTER_INSTALLATION_DATE => InstallationDate::class,
        self::FILTER_TIMEZONE          => TimeZone::class,
        self::FILTER_LAST_EVENT        => LastEvent::class,
        self::FILTER_IN_CAMPAIGN       => InCampaign::class,
        self::FILTER_SUBSCRIBE         => Subscribe::class,
        self::FILTER_DOMAINS           => Domains::class,
        self::FILTER_EMAILS            => Emails::class,
        self::FILTER_SEARCH            => Search::class,
    ];

    public function __construct(array $params)
    {

        $validator = new RecipientsFilterRequest($params);
        $attributes = $validator->create();
        if (!is_array($attributes)) {
            return $attributes;
        }
        $this->filters = $params['filters'] ?? [];
        $this->page = $params['page'] ?? 1;
        $this->limit = $params['limit'] ?? 50;

        $this->langshop = config('database.connections.langshop.database');
        $this->dashboard = config('database.connections.dashboard.database');

        $this->queries = [
            "langshop" => [],
            "partners" => []
        ];
    }

    /**
     * @return array
     */
    private function getBaseQuery(): array
    {
        return [
            "prefix" => 'SELECT DISTINCT (merged.name), merged.active, merged.id FROM (',
            "suffix" => ') AS merged '
        ];
    }

    /**
     * @return array
     */
    private function getCountQuery(): array
    {
        return [
            "prefix" => 'SELECT DISTINCT(COUNT(merged.name)) as recipients_count FROM (',
            "suffix" => ') AS merged '
        ];
    }

    /**
     * @return string
     */
    private function getBasePartnersQuery(): string
    {
        return 'SELECT DISTINCT (partners.shopify_domain) as name , partners.active, partners.id FROM '
            . $this->dashboard . '.leads partners';

    }

    /**
     * @return string
     */
    private function getBaseLangShopQuery(): string
    {
        return 'SELECT DISTINCT (langshop.name) , langshop.active, langshop.id FROM '
            . $this->langshop . '.stores langshop';

    }

    /**
     * @return string
     */
    private function getBaseLangShopCountQuery(): string
    {
        return 'SELECT COUNT( DISTINCT (langshop.name)) as recipients_count FROM '
            . $this->langshop . '.stores langshop';

    }

    /**
     * @return string
     */
    private function getBasePartnersCountQuery(): string
    {
        return 'SELECT COUNT( DISTINCT (partners.shopify_domain)) as recipients_count,  partners.active, partners.id FROM '
            . $this->dashboard . '.leads partners';
    }

    /**
     * @param string $limit
     */
    public function setLimit(string $limit): void
    {
        $this->limit = $limit;
    }

    /**
     * @param string $page
     */
    public function setPage(string $page): void
    {
        $this->page = $page;
    }

    /**
     * @param $domains
     * @return string
     */
    private function getEmailBaseQuery($domains): string
    {
        return "SELECT DISTINCT (merged.email), merged.store_id, merged.domain"
            . " FROM ("
            . "  SELECT sd.value as email, sd.store_id, s.name as domain FROM " . $this->langshop . ".store_data sd "
            . " JOIN " . $this->langshop . ".stores s ON s.id = sd.store_id "
            . " WHERE  sd.key = " . "'email'" . " AND s.name IN ('$domains')"
            . " UNION "
            . "SELECT le.email, le.id, le.domain FROM " . $this->dashboard . ".leads_emails le "
            . " WHERE le.domain IN ('$domains')"
            . ") AS merged";

    }

    /**
     * @param $langshop
     * @param $partners
     * @return array
     */
    private function getUsageTypes($langshop, $partners): array
    {
        if (!$langshop && !$partners) {
            return self::BOTH_QUERY_TYPE;
        }

        if ($langshop && $partners) {
            return [self::LANGSHOP_QUERY_TYPE];
        }

        if ($langshop) {
            return [self::PARTNERS_QUERY_TYPE];
        }

        return [self::LANGSHOP_QUERY_TYPE];

    }

    /**
     * @return array
     */
    public function resolve()
    {
        $isIgnoreLangshop = $this->isIgnoreTypeQuery(self::LANGSHOP_QUERY_TYPE);
        $isIgnorePartners = $this->isIgnoreTypeQuery(self::PARTNERS_QUERY_TYPE);
        $types = $this->getUsageTypes($isIgnoreLangshop, $isIgnorePartners);

        foreach ($this->filters as $filter) {
            $key = $filter['key'];
            $comparator = $filter['comparator'];
            $value = $filter['value'];

            if (!isset(self::FILTERS_CLASSES[$key])) {
                continue;
            }

            $filterClass = self::FILTERS_CLASSES[$key];

            /**
             * @var FilterClass $filterClass
             */
            $filterClass = new $filterClass();

            foreach ($types as $type) {
                if ($filterClass instanceof JoinsInterface) {
                    $this->makeJoins($filterClass, $type);
                }

                if ($filterClass instanceof FilterInterface) {
                    $this->make($filterClass, $type, $comparator, $value);
                }
            }
        }

        $queries = $this->getQueries();

        $sqlQuery = $queries['items'];
        $countQuery = $queries['count'];

        if (isset($this->limit)) {
            $sqlQuery .= ' LIMIT ' . ($this->page - 1) * $this->limit . ', ' . $this->limit;
        }

        $count = array_column(DB::select($countQuery), 'recipients_count');
        $total = ceil(reset($count) / $this->limit);

        return [
            "count"      => reset($count),
            "items"      => DB::select($sqlQuery),
            "page"       => $this->page,
            "totalPages" => $total
        ];
    }

    /**
     * @param $filterClass
     * @param string $type
     */
    private function makeJoins($filterClass, string $type)
    {
        $this->queries[$type]['joins'] = array_merge($this->queries[$type]['joins'] ?? [], $filterClass->makeJoins($this->langshop, $this->dashboard, $type));
    }

    /**
     * @param $filterClass
     * @param string $type
     * @param string $comparator
     * @param $value
     */
    private function make($filterClass, string $type, string $comparator, $value)
    {
        $this->queries[$type]['queries'][] = $filterClass->make($comparator, $value, $type);
    }

    /**
     * @return array
     */
    private function getQueries(): array
    {
        $queries = [];

        foreach ($this->queries as $key => $query) {

            if (!count($this->queries[$key])) {
                continue;
            }

            if (!empty($this->queries[$key]['queries'])) {
                $joins = array_values($this->queries[$key]['joins'] ?? []);
                $implodeFiltes = implode($joins, ' ')
                    . ' WHERE ' . implode(' AND ', $this->queries[$key]['queries']);

                if ($key === self::PARTNERS_QUERY_TYPE) {
                    $queries["items"][] = $this->getBasePartnersQuery() . ' ' . $implodeFiltes;
                    $queries["count"][] = $this->getBasePartnersCountQuery() . ' ' . $implodeFiltes;
                } else {
                    $queries["items"][] = $this->getBaseLangShopQuery() . ' ' . $implodeFiltes;
                    $queries["count"][] = $this->getBaseLangShopCountQuery() . ' ' . $implodeFiltes;
                }
            }
        }

        if (!count($queries)) {
            $queries["items"] = [$this->getBaseLangShopQuery(), $this->getBasePartnersQuery()];
            $queries["count"] = [$this->getBaseLangShopCountQuery(), $this->getBasePartnersCountQuery()];
        }

        return [
            "items" => count($queries['items']) > 1 ? $this->makeUnion(implode(" UNION ", $queries["items"])) : $queries["items"][0],
            "count" => count($queries['count']) > 1 ? $this->makeCountUnion(implode(" UNION ", $queries["items"])) : $queries["count"][0]
        ];
    }

    /**
     * @param string $queries
     * @return string
     */
    private function makeUnion(string $queries): string
    {
        $baseQuery = $this->getBaseQuery();
        return $baseQuery['prefix']
            . $queries
            . $baseQuery['suffix'];
    }

    /**
     * @param string $queries
     * @return string
     */
    private function makeCountUnion(string $queries): string
    {
        $baseQuery = $this->getCountQuery();
        return $baseQuery['prefix']
            . $queries
            . $baseQuery['suffix'];
    }

    /**
     * @param $type
     * @return bool
     */
    private function isIgnoreTypeQuery(string $type): bool
    {
        $ignore = false;

        foreach ($this->filters as $filter) {
            $key = $filter['key'];
            $comparator = $filter['comparator'];
            $value = $filter['value'];


            if (!isset(self::FILTERS_CLASSES[$key])) {
                continue;
            }


            $filterClass = self::FILTERS_CLASSES[$key];

            /**
             * @var FilterClass $filterClass
             */

            $filterClass = new $filterClass();

            $types = $filterClass->typesQuery($comparator, $value);
            if (!in_array($type, $types)) {
                $ignore = true;
                break;
            }
        }

        return $ignore;
    }

    /**
     * @param $domains
     * @return array
     */
    public function resolveEmails($domains)
    {

        $domainsImplode = implode("','", $domains);

        $sql = $this->getEmailBaseQuery($domainsImplode);

        return [
            "items" => DB::select($sql),
        ];
    }


}
