<?php

namespace App\Services\Filters;

use App\Services\Filters\FilterInterface;
use App\Services\Filters\JoinsInterface;
use Carbon\Carbon;

class InstallationDate implements FilterInterface, JoinsInterface
{

    /**
     * @param string $comparator
     * @param mixed $value
     * @param string $type
     * @return string
     */
    public function make(string $comparator, $value, string $type): string
    {
        if (!$value['start'] || !$value['end']) {
            return 1;
        }

        $start = Carbon::parse($value['start']);
        $end = Carbon::parse($value['end']);

        if ($start == $end) {
            $start = $start->setHour(0);
            $end = $end->setHour(23);
            $end = $end->setMinute(59);
        }
        switch ($comparator) {
            case 'installed_between':
                $sql = 'store_history.type="installed" AND timestamp>' .
                    $start->timestamp . ' '
                    . 'AND timestamp<' .$end->timestamp . ' ';
                break;
            case 'uninstalled_between':
                $sql = 'store_history.type="uninstalled" AND timestamp>' .
                    $start->timestamp . ' '
                    . 'AND timestamp<' . $end->timestamp . ' ';
                break;
            default:
                $sql = 1;
        }
        return $sql;
    }

    /**
     * @param string $langshop
     * @param string $dashboard
     * @param string $type
     * @return array
     */
    public function makeJoins(string $langshop, string $dashboard, string $type): array
    {
        return ["store_history" => 'LEFT JOIN ' . $langshop . '.store_history AS store_history '
            . 'ON ' . $type . '.id = store_history.store_id '];
    }

    /**
     * @param string $comparator
     * @param $value
     * @return array
     */
    public function typesQuery(string $comparator, $value): array
    {
        return [Resolver::LANGSHOP_QUERY_TYPE];
    }

}
