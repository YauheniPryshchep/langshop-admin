<?php

namespace App\Services\Filters;

use App\Services\Filters\FilterInterface;
use App\Services\Filters\JoinsInterface;

class Subscribe implements FilterInterface, JoinsInterface
{

    /**
     * @param string $comparator
     * @param mixed $value
     * @param string $type
     * @return string
     */
    public function make(string $comparator, $value, string $type): string
    {
        if (!in_array($value, ['subscribe', 'unsubscribe'])) {
            return 1;
        }

        switch ([$comparator, $value]) {
            case ['is', 'subscribe']:
                $sql = 'campaigns_unsubscribe_list.status IS NULL';
                break;
            case ['is', 'unsubscribe']:
                $sql = 'campaigns_unsubscribe_list.status=0';
                break;
            default:
                $sql = 1;
        }
        return $sql;
    }

    /**
     * @param string $langshop
     * @param string $dashboard
     * @param string $type
     * @return array
     */
    public function makeJoins(string $langshop, string $dashboard, string $type): array
    {
        $column = $type === Resolver::PARTNERS_QUERY_TYPE ? $type . '.shopify_domain' : $type . '.name';

        return ["campaigns_unsubscribe_list" => 'LEFT JOIN ' . $dashboard . '.campaigns_unsubscribe_list AS campaigns_unsubscribe_list '
            . 'ON ' . $column . ' = campaigns_unsubscribe_list.domain '];
    }

    /**
     * @param string $comparator
     * @param $value
     * @return array
     */
    public function typesQuery(string $comparator, $value): array
    {
        return [Resolver::PARTNERS_QUERY_TYPE, Resolver::LANGSHOP_QUERY_TYPE];
    }

}
