<?php

namespace App\Services;

use App\Requests\CampaignDomainsRequest;
use App\Models\CampaignDomain;
use App\Exceptions\CustomException;

class CampaignDomainsService
{

    /**
     * Create campaign domains list
     * @param array $params
     * @return array
     */
    public function storeCampaignDomains(array $params)
    {
        $validator = new CampaignDomainsRequest($params);
        $attributes = $validator->create();
        if (is_array($attributes)) {
            $campaignDomain['campaign_id'] = $attributes['campaign_id'];

            foreach ($attributes['domains'] as $domain) {
                $campaignDomain['domain'] = $domain;
                CampaignDomain::create($campaignDomain);
            }
        }

        return $attributes;
    }

    /**
     * Delete campaign domain
     * @param int $id
     * @return void
     * @throws CustomException
     */
    public function deleteCampaignDomain(int $id)
    {
        if (!$domain = CampaignDomain::find($id)) {
            throw new CustomException(trans('errors.campaigns.404'), 404);
        }

        $domain->delete();
        return;
    }

}
