<?php

namespace App\Services;

use Illuminate\Support\Facades\Log;

class MailingListProcessingService extends MailGunClient
{

    /**
     * @var string 
     */
    public $credentials;

    /**
     *
     * @var string 
     */
    public $address;

    public function __construct(string $credentials, string $address)
    {
        $this->credentials = $credentials;
        $this->address = $address;
        $this->url = 'lists';
    }

    public function createList()
    {
        $params = [
            'address' => $this->address
        ];
        return $this->request($this->credentials, 'POST', $this->url, ['form_params' => $params]);
    }

    public function addMembers(array $members)
    {

        return $this->request($this->credentials, 'POST',
                        $this->url . '/' . $this->address . '/members.json',
                        ['form_params' => ['members' => json_encode($members)]]);
    }

    public function deleteList()
    {
        return $this->request($this->credentials, 'DELETE', $this->url . '/' . $this->address);
    }

    public function events($params) {
        return $this->request($this->credentials, 'POST', 'frontend', ['form_params' => $params]);
    }

}
