<?php

namespace App\Services;

use App\Exceptions\Http\NotFoundError;
use App\Models\Campaign;
use App\Models\Customer;
use App\Traits\FilterTrait;
use App\Utilities\CustomerSearchFilter;
use App\Utilities\CustomerSortFilter;
use App\Validators\QueryPagination;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Validator;

class CustomersService
{
    use FilterTrait;
    /**
     * @var array
     */
    private $relatedFilters = [
        'search_term' => CustomerSearchFilter::class,
        'sort'        => CustomerSortFilter::class,
    ];

    /**
     * @var array
     */
    private $filterValidation = [
        'search_term' => 'string',
    ];
    /**
     * @var Builder
     */
    private $baseQuery;

    /**
     * CouponsService constructor.
     */
    public function __construct()
    {
        $this->baseQuery = Customer::query()
            ->with('stores');
    }

    /**
     * @param array $attributes
     * @return array
     */
    public function getItems(array $attributes)
    {
        $this->setFilterValidation(array_merge($this->filterValidation, [
            "sort" => "string",
        ]));

        return $this->getCollection($attributes);
    }

    /**
     * @param array $filterValidation
     */
    public function setFilterValidation(array $filterValidation): void
    {
        $this->filterValidation = $filterValidation;
    }

    /**
     * @param Builder $baseQuery
     */
    public function setBaseQuery(Builder $baseQuery): void
    {
        $this->baseQuery = $baseQuery;
    }

    /**
     * @param array $attributes
     * @return array
     */
    private function getCollection(array $attributes)
    {
        $pagination = new QueryPagination($attributes);


        $validator = Validator::make($attributes, $this->filterValidation);
        $filters = $validator->validate();

        $query = $this->baseQuery;

        $query = $this->filterBy($filters, $query, $this->relatedFilters);


        $count = $query->count();


        /**
         * @var LengthAwarePaginator $paginator
         */
        $paginator = $query->paginate(
            $pagination->getLimit(),
            ['*'],
            'page',
            $pagination->getPage()
        );

        $totalPages = ceil($count / $pagination->getLimit());

        return [
            "items" => $paginator->getCollection(),
            "count" => $count,
            "page"  => $pagination->getPage(),
            "pages" => $totalPages
        ];
    }


    /**
     * @param int $id
     * @return Customer
     */
    public function show(int $id)
    {
        /**
         * @var Customer $customer
         */
        $customer = Customer::query()
            ->where('id', $id)
            ->with('stores')
            ->first();

        $customer->campaigns = $this->getCustomerCampaigns($customer);

        if (!$customer) {
            throw new NotFoundError();
        }

        return $customer;
    }

    /**
     * @param Customer $customer
     * @return Collection
     */
    private function getCustomerCampaigns(Customer $customer): Collection
    {
        $stores = [];

        foreach ($customer->stores as $store) {
            $stores[] = $store->name;
        }

        /**
         * @var Campaign $campaigns
         */
        $campaigns = Campaign::query()
            ->whereHas('campaign_domains', function (Builder $query) use ($stores) {
                $query->whereIn('domain', $stores);
            })
            ->get();

        return $campaigns;
    }
}