<?php

namespace App\Services\Partners;

use App\Services\Guzzle\HttpClient;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Http\Message\ResponseInterface;

class PartnerApiService
{
    /**
     * @var string
     */
    private $partnerUrl;

    /**
     * @var string
     */
    private $partnerToken;

    /**
     * @var HttpClient
     */
    private $httpClient;

    public function __construct()
    {
        $this->partnerUrl = config('partners.url');
        $this->partnerToken = config('partners.secret_key');

        $this->httpClient = new HttpClient([
            'base_uri' => $this->partnerUrl,
            'headers'  => [
                'Api-key' => $this->partnerToken,
                'Accept'  => 'application/json'
            ]
        ]);
    }

    /**
     * @param int $id
     * @return ResponseInterface
     * @throws GuzzleException
     */
    public function balance(int $id)
    {
        $response = $this->httpClient->request("GET", '/system/admin/balance/' . $id);

        $result = json_decode((string)$response->getBody());

        return $result;
    }

    /**
     * @return ResponseInterface
     * @throws GuzzleException
     */
    public function totalBalance()
    {
        $response = $this->httpClient->request("GET", '/system/admin/balance');

        $result = json_decode((string)$response->getBody());

        return $result;
    }
}