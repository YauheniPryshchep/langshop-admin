<?php

namespace App\Services\Partners;

use App\Exceptions\Http\NotFoundError;
use App\Models\Partners\Payout;
use App\Models\Partners\User;
use App\Objects\SimplePaginationObject;
use App\Services\MailGunService;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class PayoutsService
{
    /**
     * @param string $status
     * @param SimplePaginationObject $pagination
     * @param bool $unassigned
     * @return array
     */
    public function getAll(string $status, SimplePaginationObject $pagination, bool $unassigned = false)
    {
        return $this->_getAll($status, $pagination, $unassigned);
    }

    /**
     * @param int $id
     * @return Payout|Builder|Model|object|null
     */
    public function getInfo(int $id)
    {
        return Payout::with(['payoutTransactions'])
            ->where('id', $id)
            ->with('user')
            ->first();
    }

    /**
     * @param int $id
     * @param string $status
     * @return Payout|Builder|Model|object|null
     * @throws GuzzleException
     */
    public function update(int $id, string $status)
    {
        $mailgunService = new MailGunService('default');
        if (!$payout = Payout::with(['payoutTransactions'])
            ->where('id', $id)
            ->with('user')
            ->first()) {
            throw new NotFoundError();
        }

        $user_id = $payout->user['id'];
        /**
         * @var $user User
         */
        $user = User::query()
            ->where('id', $user_id)
            ->with('userSettings')
            ->first();

        if ($status === Payout::PAID_STATUS) {
            $mailgunService->sendPayoutAccepted($user, $payout);
        } else {
            $mailgunService->sendPayoutDeclined($user, $payout);
        }

        $payout->update(["status" => $status]);
        return $payout;
    }


    /**
     * @param int $id
     * @param SimplePaginationObject $pagination
     * @param bool $unassigned
     * @return array
     */
    public function getUserPayouts(int $id, SimplePaginationObject $pagination, bool $unassigned = false)
    {
        return $this->_getUserPayouts($id, $pagination, $unassigned);
    }

    /**
     * @param User $user
     * @return int
     */
    private function _getUserPayoutsCount(User $user)
    {
        $query = Payout::query()
            ->where('user_id', $user->id)
            ->with('payoutTransactions');

        return $query->count();
    }

    /**
     * @param int $id
     * @param SimplePaginationObject $pagination
     * @param bool $unassigned
     * @return array
     */
    private function _getUserPayouts(int $id, SimplePaginationObject $pagination, bool $unassigned = false)
    {
        $limit = $pagination->limit;
        $offset = $pagination->limit * ($pagination->page - 1);

        /**
         * @var User $user
         */
        $user = User::query()
            ->where('id', $id)
            ->first();

        if (!$user) {
            throw new NotFoundError();
        }

        $count = $this->_getUserPayoutsCount($user);

        $pages = ceil($count / $pagination->limit);

        $query = Payout::query()
            ->where('user_id', $user->id)
            ->with('payoutTransactions');

        $query->offset($offset)
            ->limit($limit);

        return [
            "count"      => $count,
            "items"      => $query->get(),
            "page"       => $pagination->page,
            "totalPages" => $pages
        ];
    }

    /**
     * @return int
     */
    private function _getAllCount()
    {
        $query = Payout::query();

        return $query->count();
    }

    /**
     * @param string $status
     * @param SimplePaginationObject $pagination
     * @param bool $unassigned
     * @return array
     */
    private function _getAll(string $status, SimplePaginationObject $pagination, bool $unassigned = false)
    {
        $limit = $pagination->limit;
        $offset = $pagination->limit * ($pagination->page - 1);


        $count = $this->_getAllCount();

        $pages = ceil($count / $pagination->limit);

        $query = Payout::query()
            ->with('payoutTransactions');

        if ($status) {
            $query->where('status', $status);
        }

        $query->offset($offset)
            ->limit($limit);

        return [
            "count"      => $count,
            "items"      => $query->get(),
            "page"       => $pagination->page,
            "totalPages" => $pages
        ];
    }


}