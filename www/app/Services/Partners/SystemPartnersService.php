<?php

namespace App\Services\Partners;

use App\Exceptions\Http\BadRequestError;
use App\Models\PartnerToken;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class SystemPartnersService
{
    /**
     * @param $id
     * @return string
     */
    public function login($id)
    {
        $token = md5(Str::random(128) . time());
        PartnerToken::query()
            ->create([
                "token"     => $token,
                "user_id"   => $id,
                "timestamp" => time() + 3600,
                "locked"    => 0
            ]);

        return $token;
    }

    /**
     * @param array $attributes
     * @return array
     */
    public function verify(array $attributes)
    {
        $validator = Validator::make($attributes, [
            'token' => 'required|string'
        ]);
        $input = $validator->validate();

        $token = $input['token'];


        $partnerToken = PartnerToken::query()
            ->where('token', $token)
            ->first();

        if (empty($partnerToken)) {
            throw new BadRequestError();
        }

        if ($partnerToken->locked) {
            throw new BadRequestError(400, 'Token is locked for using');
        }

        if ($partnerToken->timestamp < time()) {
            throw new BadRequestError(400, 'Authorization token is expired');
        }


        $partnerToken->update([
            'locked' => true
        ]);

        return ["user_id" => $partnerToken->user_id];

    }
}