<?php

namespace App\Services\Partners;

use App\Exceptions\Http\NotFoundError;
use App\Models\Partners\User;
use App\Models\Store;
use App\Objects\SimplePaginationObject;
use App\Validators\PartnerCommissionValidator;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

class PartnersUserService
{
    /**
     * @param SimplePaginationObject $pagination
     * @param bool $unassigned
     * @return array
     */
    public function getAll(SimplePaginationObject $pagination, bool $unassigned = false)
    {
        return $this->_getAll($pagination, $unassigned);
    }

    /**
     * @param int $id
     * @param SimplePaginationObject $pagination
     * @param bool $unassigned
     * @return mixed
     */
    public function getStores(int $id, SimplePaginationObject $pagination, bool $unassigned = false)
    {
        return $this->_getStores($id, $pagination, $unassigned);
    }


    /**
     * @param int $id
     * @return User[]|Builder[]|Collection
     */
    public function getInfo(int $id)
    {
        return User::with(['userData', 'userSettings'])
            ->where('id', $id)
            ->get();
    }

    /**
     * @param User $user
     * @return int
     */
    private function _getAllStoresCount(User $user)
    {

        $query = Store::query()
            ->whereHas('referrals',
                function (Builder $query) use ($user) {
                    $query->where('ref', $user->settings['referral_link']);
                })
            ->with(
                [
                    "referrals" => function ($query) use ($user) {
                        $query->where('ref', $user->settings['referral_link'])
                            ->orderBy('created_at', 'asc');
                    }
                ]
            );

        return $query->count();
    }

    /**
     * @param int $id
     * @param SimplePaginationObject $pagination
     * @param bool $unassigned
     * @return array
     */
    private function _getStores(int $id, SimplePaginationObject $pagination, bool $unassigned = false)
    {
        $limit = $pagination->limit;
        $offset = $pagination->limit * ($pagination->page - 1);

        /**
         * @var User $user
         */
        $user = User::query()
            ->where('id', $id)
            ->with(['userSettings'])
            ->first();

        if (!$user) {
            throw new NotFoundError();
        }

        $count = $this->_getAllStoresCount($user);

        $pages = ceil($count / $pagination->limit);

        $query = Store::query()
            ->whereHas('referrals',
                function (Builder $query) use ($user) {
                    $query->where('ref', $user->settings['referral_link']);
                })
            ->with(
                [
                    "referrals" => function ($query) use ($user) {
                        $query->where('ref', $user->settings['referral_link'])
                            ->orderBy('created_at', 'asc');
                    }
                ]
            );

        $query->offset($offset)
            ->limit($limit);

        return [
            "count"      => $count,
            "items"      => $query->get(),
            "page"       => $pagination->page,
            "totalPages" => $pages
        ];
    }

    /**
     * @param SimplePaginationObject $pagination
     * @return int
     */
    private function _getAllCount(SimplePaginationObject $pagination)
    {
        $query = User::query();

        if (!empty($pagination->filter)) {
            $query->where(function (Builder $query) use ($pagination) {
                $query->where('email', 'like', '%' . $pagination->filter . '%');
            });
        }

        return $query->count();
    }

    /**
     * @param SimplePaginationObject $pagination
     * @param bool $unassigned
     * @return array
     */
    private function _getAll(SimplePaginationObject $pagination, bool $unassigned = false)
    {
        $limit = $pagination->limit;
        $offset = $pagination->limit * ($pagination->page - 1);
        $sortField = (!empty($pagination->sort->field)) ? $pagination->sort->field : 'created_at';
        $sortDirection = $pagination->sort->direction;
        $count = $this->_getAllCount($pagination);

        $pages = ceil($count / $pagination->limit);

        $query = User::query()->with(['userData', 'userSettings']);

        if (!empty($pagination->filter)) {
            $query->where(function (Builder $query) use ($pagination) {
                $query->where('email', 'like', '%' . $pagination->filter . '%');
            });
        }

        $query->orderBy($sortField, $sortDirection);

        $query->offset($offset)
            ->limit($limit);

        return [
            "count"      => $count,
            "items"      => $query->get(),
            "page"       => $pagination->page,
            "totalPages" => $pages
        ];
    }

    /**
     * @param int $id
     * @param array $attributes
     * @return User
     */
    public function setNewCommission(int $id, array $attributes)
    {
        $query = new PartnerCommissionValidator($attributes);

        /**
         * @var $user User
         */
        if (!$user = User::query()->where('id', $id)->first()) {
            throw new NotFoundError();
        }

        $input =$query->input();

        $user->userCommissions()->create([
            "commission" => $input['commission'],
            "created_at" => Carbon::parse($input['created_at'])
        ]);

        return $user;
    }


}