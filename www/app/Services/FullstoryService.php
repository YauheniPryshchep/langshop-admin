<?php

namespace App\Services;


use App\Exceptions\Http\BadRequestError;
use App\Exceptions\Http\HttpError;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\HttpFoundation\Response;

class FullstoryService
{

    /**
     * @var string
     */
    private $apiKey;

    /**
     * @var Client
     */
    private $httpClient;

    /**
     * FullstoryService constructor.
     */
    public function __construct()
    {
        $this->apiKey = config('fullstory.api_key');
        $this->httpClient = new Client();
    }

    public function getUserSessions(string $uid)
    {
        $params = [
            "query"   => [
                "uid" => $uid
            ],
            "headers" => [
                "Content-Type"  => "application/json",
                "Authorization" => "Basic " . $this->apiKey
            ]
        ];

        try {
            $sessions = $this->httpClient->request("GET", 'https://www.fullstory.com/api/v1/sessions', $params);
        } catch (BadResponseException $e) {
            throw new HttpError($e->getCode(), Response::HTTP_INTERNAL_SERVER_ERROR, $e->getMessage());
        }
      return json_decode($sessions->getBody(), true);
    }
}
