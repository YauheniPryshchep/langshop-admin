<?php

namespace App\Services\Shopify\Helpers;

class ValuesMutator
{
    /**
     * @var array
     */
    private $mutations = [];

    /**
     * ValuesMutator constructor.
     * @param array $mutations
     */
    public function __construct(array $mutations = [])
    {
        $this->mutations = $mutations;
    }

    /**
     * @param string $key
     * @param mixed $mutation
     * @return void
     */
    public function addMutation(string $key, $mutation): void
    {
        $this->mutations[$key] = $mutation;
    }

    /**
     * @param array $item
     * @return void
     */
    public function mutate(array &$item): void
    {
        foreach ($this->mutations as $key => $mutation) {
            if (!array_key_exists($key, $item)) {
                continue;
            }

            if (is_callable($mutation)) {
                $item[$key] = call_user_func($mutation, $item[$key]);
            }
        }
    }
}