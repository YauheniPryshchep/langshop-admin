<?php

namespace App\Services\Shopify\Helpers;

class PropertiesMutator
{
    /**
     * @var array
     */
    private $mutations = [];

    /**
     * PropertiesMutator constructor.
     * @param array $mutations
     */
    public function __construct(array $mutations = [])
    {
        $this->mutations = $mutations;
    }

    /**
     * @param string $key
     * @param string $property
     */
    public function addMutation(string $key, string $property): void
    {
        $this->mutations[$key] = $property;
    }

    /**
     * @param array $item
     * @return void
     */
    public function mutate(array &$item): void
    {
        foreach ($this->mutations as $key => $field) {
            if (!array_key_exists($key, $item)) {
                continue;
            }

            $item[$field] = $item[$key];
            unset($item[$key]);
        }
    }
}