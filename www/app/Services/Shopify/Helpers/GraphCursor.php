<?php

namespace App\Services\Shopify\Helpers;

class GraphCursor
{
    /**
     * @param int $id
     * @return string
     */
    public static function toCursor(int $id): string
    {
        return base64_encode(json_encode(['last_id' => $id, 'last_value' => (string)$id]));
    }

    /**
     * @param string $cursor
     * @return int|null
     */
    public static function toId(string $cursor): ?int
    {
        $cursorData = json_decode(base64_decode($cursor));

        return $cursorData->last_id ?? null;
    }
}