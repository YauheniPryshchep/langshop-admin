<?php

namespace App\Services\Shopify\Helpers;

class GID
{
    /**
     * @param int|string $id
     * @param string $prefix
     * @return string
     */
    public static function toGid($id, string $prefix): string
    {
        return $prefix . $id;
    }

    /**
     * @param string $gid
     * @param string $prefix
     * @return string|int
     */
    public static function toId(string $gid, string $prefix)
    {
        $id = str_replace($prefix, '', $gid);

        return is_numeric($id)
            ? (int)$id
            : $id;
    }
}
