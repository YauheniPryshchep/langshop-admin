<?php

namespace App\Services\Shopify\Helpers;

use App\Collections\AbstractCollection;
use JsonSerializable;

class CursorNavigator implements JsonSerializable
{
    /**
     * @var AbstractCollection
     */
    private $collection;

    /**
     * @var bool
     */
    private $hasNextPage = false;

    /**
     * @var ?string
     */
    private $nextItemsAfter = null;

    /**
     * @var bool
     */
    private $hasPreviousPage = false;

    /**
     * @var ?string
     */
    private $previousItemsBefore = null;

    /**
     * CursorNavigator constructor.
     * @param AbstractCollection $collection
     * @param array $pageInfo
     */
    public function __construct(AbstractCollection $collection, array $pageInfo)
    {
        $this->collection = $collection;
        if (!$collection->count()) {
            return;
        }

        $this->hasNextPage     = $pageInfo['hasNextPage'];
        $this->hasPreviousPage = $pageInfo['hasPreviousPage'];

        if ($this->hasNextPage) {
            if (!empty($pageInfo['nextItemsAfter'])) {
                $this->nextItemsAfter = $pageInfo['nextItemsAfter'];
            } else {
                $this->nextItemsAfter = $this->collection
                    ->offsetGet($this->collection->count() - 1)
                    ->cursor;
            }
        }

        if ($this->hasPreviousPage) {
            if (!empty($pageInfo['previousItemsBefore'])) {
                $this->previousItemsBefore = $pageInfo['previousItemsBefore'];
            } else {
                $this->previousItemsBefore = $this->collection
                    ->offsetGet(0)
                    ->cursor;
            }
        }
    }

    /**
     * @return AbstractCollection
     */
    public function getCollection(): AbstractCollection
    {
        return $this->collection;
    }

    /**
     * @return bool
     */
    public function isHasNextPage(): bool
    {
        return $this->hasNextPage;
    }

    /**
     * @return bool
     */
    public function isHasPreviousPage(): bool
    {
        return $this->hasPreviousPage;
    }

    /**
     * @return null|string
     */
    public function nextItemsAfter(): ?string
    {
        return $this->nextItemsAfter;
    }

    /**
     * @return null|string
     */
    public function previousItemsBefore(): ?string
    {
        return $this->previousItemsBefore;
    }

    /**
     * Convert the object into something JSON serializable.
     *
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            'items'               => $this->getCollection(),
            'nextItemsAfter'      => $this->nextItemsAfter(),
            'previousItemsBefore' => $this->previousItemsBefore()
        ];
    }
}