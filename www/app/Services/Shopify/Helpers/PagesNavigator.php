<?php

namespace App\Services\Shopify\Helpers;

use App\Collections\AbstractCollection;
use JsonSerializable;

class PagesNavigator implements JsonSerializable
{
    /**
     * @var AbstractCollection
     */
    private $collection;

    /**
     * @var int
     */
    private $totalPages = 1;

    /**
     * @var int
     */
    private $currentPage = 1;

    /**
     * @var int
     */
    private $totalItems = 0;

    /**
     * PagesNavigator constructor.
     * @param AbstractCollection $collection
     * @param int $totalItems
     * @param int $itemsPerPage
     * @param int $currentPage
     */
    public function __construct(
        AbstractCollection $collection,
        int $totalItems,
        int $itemsPerPage,
        int $currentPage
    ) {
        $this->collection  = $collection;
        $this->totalItems  = $totalItems;
        $this->currentPage = $currentPage;
        $this->totalPages  = ($totalItems > 0)
            ? ceil($totalItems / $itemsPerPage)
            : 1;
    }

    /**
     * @return AbstractCollection
     */
    public function getCollection(): AbstractCollection
    {
        return $this->collection;
    }

    /**
     * @return int
     */
    public function getTotalPages(): int
    {
        return $this->totalPages;
    }

    /**
     * @return int
     */
    public function getCurrentPage(): int
    {
        return $this->currentPage;
    }

    /**
     * @return int
     */
    public function getTotalItems(): int
    {
        return $this->totalItems;
    }

    /**
     * Convert the object into something JSON serializable.
     *
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            'items'       => $this->getCollection(),
            'totalPages'  => $this->getTotalPages(),
            'totalItems'  => $this->getTotalItems(),
            'currentPage' => $this->getCurrentPage(),
        ];
    }
}
