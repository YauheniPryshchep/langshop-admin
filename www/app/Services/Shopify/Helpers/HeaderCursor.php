<?php

namespace App\Services\Shopify\Helpers;

use function GuzzleHttp\Psr7\parse_query;

class HeaderCursor
{
    /**
     * @param array $headers
     * @return array
     */
    public static function parsePageInfo(array $headers): array
    {
        $cursor   = self::getCursor($headers['Link'][0] ?? null);
        $pageInfo = self::getPageInfo($cursor);

        return array_merge(
            $cursor,
            $pageInfo
        );
    }

    /**
     * @param string|null $link
     * @return array
     */
    protected static function getCursor(?string $link): array
    {
        $cursor = [
            'nextItemsAfter'      => null,
            'previousItemsBefore' => null
        ];

        if (empty($link)) {
            return $cursor;
        }

        foreach (preg_split("/,\s*</", $link) as $link) {
            if (!preg_match("/<?([^>]*)>(.*)/", $link, $matches)) {
                continue;
            }

            $url   = $matches[1];
            $query = parse_url($url, PHP_URL_QUERY);
            if (empty($query)) {
                continue;
            }

            $query = parse_query($query);
            if (empty($query['page_info'])) {
                continue;
            }

            $pageInfo = $query['page_info'];

            if (!preg_match("/rel=\"([\w]+)\"/", $matches[2], $matches)) {
                continue;
            }

            if ($matches[1] === 'next') {
                $cursor['nextItemsAfter'] = $pageInfo;
            } elseif ($matches[1] === 'previous') {
                $cursor['previousItemsBefore'] = $pageInfo;
            }
        }

        return $cursor;
    }

    /**
     * @param array $cursor
     * @return array
     */
    protected static function getPageInfo(array $cursor): array
    {
        return [
            'hasPreviousPage' => !empty($cursor['previousItemsBefore']),
            'hasNextPage'     => !empty($cursor['nextItemsAfter'])
        ];
    }
}
