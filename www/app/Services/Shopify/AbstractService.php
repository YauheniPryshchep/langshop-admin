<?php

namespace App\Services\Shopify;

use App\Validators\QueryCursor;
use App\Validators\QueryShopifyFilter;
use App\Validators\QuerySort;
use rdx\graphqlquery\Container;
use rdx\graphqlquery\Query;

abstract class AbstractService
{
    /**
     * @noinspection PhpDocMissingThrowsInspection
     * @noinspection PhpUnhandledExceptionInspection
     *
     * @return static
     */
    public static function make()
    {
        return app()->make(static::class);
    }

    /**
     * @param array $input
     * @param array $fields
     * @return array
     */
    protected function mapInputFields(array $input, array $fields)
    {
        foreach ($fields as $key => $field) {
            if (!isset($input[$field])) {
                continue;
            }

            $input[$key] = $input[$field];
            unset($input[$field]);
        }

        return $input;
    }

    /**
     * @param Container $container
     * @param QueryCursor $cursor
     * @return Container
     */
    protected function buildQueryCursor(Container $container, QueryCursor $cursor): Container
    {
        if ($after = $cursor->getAfter()) {
            $container->attribute('first', $cursor->getLimit());
            $container->attribute('after', $after);
        } elseif ($before = $cursor->getBefore()) {
            $container->attribute('last', $cursor->getLimit());
            $container->attribute('before', $before);
        } else {
            $container->attribute('first', $cursor->getLimit());
        }

        $container->field('pageInfo')
            ->fields('hasNextPage', 'hasPreviousPage');

        $edges = $container->field('edges');
        $edges->field('cursor');

        return $edges;
    }

    /**
     * @param Container $container
     * @param QueryShopifyFilter $filter
     * @param QuerySort|null $sort
     * @return Container
     */
    protected function buildQueryArgs(Container $container, QueryShopifyFilter $filter = null, QuerySort $sort = null): Container
    {
        if (!is_null($filter)) {
            $inputs = $filter->getInput();
            if (!empty($inputs)) {
                foreach ($inputs as $key => $input) {
                    $container->attribute($key, $input);
                }
            }
        }

        if (!is_null($sort)) {
            $sortKey = $sort->getSortKey();
            $reverse = $sort->getSortDir() === QuerySort::SORT_DIR_DESC ? true : false;
            if (!empty($sortKey)) {
                $container->attribute('sortKey', Query::enum($sortKey));
                $container->attribute('reverse', $reverse);
            }
        }

        return $container;
    }

    /**
     * @param array $nodes
     * @param array $fields
     * @return Query
     */
    protected function buildQueryNodes(array $nodes, array $fields): Query
    {
        $query = Query::query();

        foreach ($nodes as $i => $node) {
            $field = $query->field('node', "node{$i}")
                ->attribute('id', $node);

            $path = explode("/", $node);

            $field->fragment($path[count($path) - 2])
                ->fields(...$fields);
        }

        return $query;
    }
}
