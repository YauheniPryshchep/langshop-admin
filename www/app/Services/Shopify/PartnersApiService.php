<?php

namespace App\Services\Shopify;

use App\Exceptions\Http\HttpError;
use App\Exceptions\Http\InternalError;
use App\Exceptions\Http\TooManyRequestsError;
use App\Helpers\Arrays;
use App\Logging\LogsStackContainer;
use App\Services\Guzzle\HttpClient;
use App\Services\Queue\RequestQueueService;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Psr\Http\Message\ResponseInterface;
use Throwable;

class PartnersApiService
{
    const GRAPHQL_API_ROOT = '/api/unstable';

    const MAX_REQUEST_RETRIES_ATTERMPS = 3;

    CONST AVAILABLE_KEYS = [
        'key-1',
        'key-2'
    ];

    /**
     * The Guzzle client.
     *
     * @var HttpClient
     */
    protected $httpClient;

    /**
     * @var int
     */
    protected $partnerId;

    /**
     * @var string
     */
    protected $secretKey;

    /**
     * @var string[]
     */
    protected $scopes = [];

    /**
     * @var RequestQueueService;
     */
    protected $requestQueueService;

    /**
     * @return static
     */
    public static function make()
    {
        return app()->make(static::class);
    }

    /**
     * PartnersApiService constructor.
     */
    public function __construct()
    {
        $this->httpClient = new HttpClient([
            'max_retry_attempts' => self::MAX_REQUEST_RETRIES_ATTERMPS,
            'headers'            => [
                'Accept' => 'application/json',
                'Origin' => url('', [], true)
            ]
        ]);
        $this->partnerId = Config::get('shopify.partner_id');
        $this->secretKey = explode(",", Config::get('shopify.partner_secret'));
        $this->requestQueueService = new RequestQueueService($this->secretKey);
    }

    /**
     * @param string $method
     * @param string $url
     * @param array $options
     * @return ResponseInterface
     * @throws HttpError
     */
    public function request(string $method, string $url, array $options = []): ResponseInterface
    {
        try {
            $response = $this->httpClient->request($method, $url, $options);
        } catch (BadResponseException $e) {
            $response = $e->getResponse();
        } catch (GuzzleException $e) {
            throw new InternalError(
                4001,
                "Shopify api error",
                $e->getTrace()
            );
        }

        return $response;
    }

    /**
     * @param string $query
     * @param array $variables
     * @param string $root
     * @param int $retry
     * @return array
     * @throws HttpError
     */
    public function queue(string $query, array $variables = [], string $root = self::GRAPHQL_API_ROOT, int $retry = 0)
    {
        $requestId = Str::uuid();
        $queue = $this->requestQueueService->getLessUsageQueue();

        $this->requestQueueService->queue($queue, $requestId);
        $requestPosition = $this->requestQueueService->getPosition($queue, $requestId);
        $starttime = time();

        while (true) {
            if($requestPosition === 0){
                break;
            }

            if ((time() - $starttime) > 30) {
                throw new InternalError(
                    4001,
                    "Shopify api error"
                );
                break;
            }

            sleep(1);

            $requestPosition = $this->requestQueueService->getPosition($queue, $requestId);
        }

        $response = $this->graph($query, $queue, $variables, $root, $retry);

        $this->requestQueueService->deQueue($queue, $requestId);

        return $response;
    }


    /**
     * @param string $query
     * @param array $variables
     * @param string $root
     * @param int $retry
     * @param string $queue
     * @return array
     */
    public function graph(string $query, string $queue, array $variables = [], string $root = self::GRAPHQL_API_ROOT, int $retry = 0): array
    {
        $body = [
            'query' => $query
        ];

        if (!empty($variables)) {
            $body['variables'] = $variables;
        }

        $response = $this->request(
            'POST',
            "https://partners.shopify.com/{$this->partnerId}{$root}/graphql.json",
            [
                'headers' => [
                    'X-Shopify-Access-Token' => $queue,
                    'Content-Type'           => 'application/json',
                ],
                'body'    => json_encode($body),
            ]
        );

        $result = $this->jsonDecode($response->getBody());

        try {
            $this->handleGraphQlQueryErrors($response, $result);
        } catch (TooManyRequestsError $e) {
            if ($retry < self::MAX_REQUEST_RETRIES_ATTERMPS) {
                return $this->graph($query, $queue, $variables, $root, $retry + 1);
            }

            throw $e;
        }

        return $result['data'];
    }


    /**
     * @param ResponseInterface $response
     * @param array $result
     * @throws HttpError
     */
    protected function handleGraphQlQueryErrors(ResponseInterface $response, array $result)
    {
        if (!empty($result['errors'])) {
            LogsStackContainer::log("Received error from Shopify GraphQL API", null, [
                'response' => (string)$response->getBody()
            ]);

            if (
                count($result['errors']) === 1
                && !empty($result['errors'][0]['extensions']['code'])
            ) {
                switch ($result['errors'][0]['extensions']['code']) {
                    case "THROTTLED":
                        throw new TooManyRequestsError();
                }
            }

            throw new InternalError(
                "Shopify GraphQl error",
                array_map(function ($error) {
                    return $error['message'];
                }, $result['errors'])
            );
        }

        if (!isset($result['data'])) {
            LogsStackContainer::log("Invalid Shopify response format. Missing \"data\" property", null, [
                'response' => (string)$response->getBody()
            ]);

            throw new InternalError("Shopify format error");
        }
    }

    /**
     * @param string $json
     * @return array
     * @throws HttpError
     */
    protected function jsonDecode(string $json): array
    {
        if (empty($json)) {
            return [];
        }

        try {
            $result = Arrays::fromJson($json);
        } catch (Throwable $e) {
            LogsStackContainer::log('Can\'t decode Shopify response.', $e, [
                'response' => $json
            ]);

            throw new InternalError(
                "Shopify decode error",
                null,
                $e
            );
        }

        return $result;
    }
}
