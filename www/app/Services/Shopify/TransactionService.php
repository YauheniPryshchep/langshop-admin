<?php

namespace App\Services\Shopify;

use App\Collections\ItemsCollection;
use App\Exceptions\Http\NotFoundError;
use App\Factories\Shopify\TransactionFactory;
use App\Models\Partners\User;
use App\Models\PayoutTransaction;
use App\Models\ReferralStores;
use App\Models\Store;
use App\Services\PayoutService;
use App\Services\Shopify\Helpers\CursorNavigator;
use App\Services\StoreReferralService;
use App\Validators\QueryCursor;
use App\Validators\QueryShopifyFilter;
use Carbon\Carbon;
use Exception;
use Illuminate\Validation\ValidationException;
use rdx\graphqlquery\Container;
use rdx\graphqlquery\Query;

class TransactionService extends AbstractService
{
    /**
     * @var PartnersApiService
     */
    protected $partnersApiService;

    /**
     * @var StoreReferralService
     */
    protected $storesService;

    /**
     * @var TransactionFactory
     */
    protected $transactionFactory;

    /**
     * TransactionService constructor.
     */
    public function __construct()
    {
        $this->partnersApiService = new PartnersApiService();
        $this->transactionFactory = new TransactionFactory();
        $this->storesService = new StoreReferralService();

    }

    /**
     * @param QueryCursor $cursor
     * @param QueryShopifyFilter $filter
     * @return array
     * @throws ValidationException
     */
    public function getTransactions(QueryCursor $cursor, QueryShopifyFilter $filter): array
    {
        $transactions = [];

        do {
            $cursorNavigation = $this->getAllTransactions($cursor, $filter);

            $transactions = array_merge($transactions, $cursorNavigation->getCollection()->toArray());

            if (!$cursorNavigation->isHasNextPage()) {
                break;
            }
            $cursor = new QueryCursor([
                "after" => $cursorNavigation->nextItemsAfter()
            ]);

        } while ($cursorNavigation->isHasNextPage());

        return $transactions;
    }

    /**
     * @param User $user
     * @param Store $store
     * @param PayoutTransaction|null $lastStoreTransaction
     * @return array
     * @throws ValidationException
     */
    public function getStoreTransactions(User $user, Store $store, ?PayoutTransaction $lastStoreTransaction): array
    {
        $transactions = [];

        if (empty($store->referrals)) {
            return [];
        }

        foreach ($store->referrals as $referral) {

            if ($referral->status === ReferralStores::STATUS_DECLINED) {
                continue;
            }

            if (!$this->isActiveReferrals(
                $lastStoreTransaction,
                $referral->cancelled_at
            )) {
                continue;
            }

            $range = $this->getDateTransactionRange(
                $lastStoreTransaction,
                $referral->created_at,
                $referral->cancelled_at);

            $cursor = new QueryCursor();

            $filter = new QueryShopifyFilter(array_merge(['myshopifyDomain' => $store->name, 'appId' => 'gid://partners/App/' . config('partners.langshop_app_id')], $range), [
                'myshopifyDomain' => 'required|string',
                'createdAtMin'    => 'string',
                'createdAtMax'    => 'nullable|string',
                'appId'           => 'required',
            ]);

            $transactions = array_merge($transactions, $this->getTransactions($cursor, $filter));
        }
        rsort($transactions);

        $transactions = array_map(function ($transaction) use ($user) {
             $transaction['commission'] = $user->getTransactionCommission($transaction);
             return $transaction;
        }, $transactions);

        return $transactions;
    }

    /**
     * @param int $id
     * @return array|false
     * @throws ValidationException
     */
    public function getBalance(int $id)
    {
        /**
         * @var $user User
         */
        $user = User::query()
            ->where('id', $id)
            ->first();

        if (!$user) {
            throw new NotFoundError();
        }

        $payoutService = new PayoutService();

        $stores = $this->storesService->getAllStores($user);
        $transactions = [];

        foreach ($stores as $store) {
            $lastStoreTransaction = $payoutService->getLastStoreTransaction($user, $store);

            $transactions = array_merge($transactions, $this->getStoreTransactions($user, $store, $lastStoreTransaction));
        }

        $balance = array_reduce($transactions, function ($carry, $transaction) use ($user) {
            if ($this->isTransactionApplied($transaction)) {
                return $carry += $user->getTransactionCommission($transaction);
            } else {
                return $carry;
            }
        }, 0);

        return array_combine(['transactions', 'balance'], [$transactions, $balance]);
    }

    /**
     * @return array
     * @throws ValidationException
     */
    public function getAllBalance()
    {
        $users = User::query()->get();
        $balances = [];
        foreach ($users as $user) {
            $balances[] = array_merge(["user" => $user], $this->getBalance($user->id));
        }
        return $balances;
    }

    /**
     * @param QueryCursor $cursor
     * @param QueryShopifyFilter $filter
     * @return CursorNavigator
     */
    public function getAllTransactions(QueryCursor $cursor, QueryShopifyFilter $filter): CursorNavigator
    {
        $query = Query::query();

        $transactions = $this->buildQueryArgs($query->field('transactions'), $filter);

        $this->buildQueryItem(
            $this->buildQueryCursor($transactions, $cursor)
                ->field('node')
        );

        $data = $this->partnersApiService->queue($query->build());

        $itemsCollection = ItemsCollection::create($this->transactionFactory, $data['transactions']['edges']);

        return new CursorNavigator($itemsCollection, $data['transactions']['pageInfo']);
    }

    /**
     * @param Container $node
     */
    private function buildQueryItem(Container $node): void
    {
        $node->fields('id', 'createdAt');

        $this->buildQueryFragment($node, 'AppSubscriptionSale');

        $this->buildQueryFragment($node, 'AppSaleAdjustment');
    }

    /**
     * @param Container $node
     * @param string $fragmentName
     */
    private function buildQueryFragment(Container $node, string $fragmentName): void
    {
        $fragment = $node->fragment($fragmentName);

        $fragment->field('shopifyFee')
            ->fields(['amount', 'currencyCode']);

        $fragment->field('netAmount')
            ->fields(['amount', 'currencyCode']);

        $fragment->field('shop')
            ->fields('myshopifyDomain', 'name', 'id');
    }

    /**
     * @param PayoutTransaction|null $lastTransaction
     * @param Carbon|null $referralCancelledAt
     * @return bool
     */
    private function isActiveReferrals(?PayoutTransaction $lastTransaction, ?Carbon $referralCancelledAt): bool
    {
        if (!is_null($lastTransaction)
            && !is_null($referralCancelledAt)
            && Carbon::parse($lastTransaction->created_at)->greaterThan($referralCancelledAt)
        ) {
            return false;
        }

        return true;
    }

    /**
     * @param PayoutTransaction|null $lastTransaction
     * @param Carbon $referralCreatedAt
     * @param Carbon|null $referralCancelledAt
     * @return array
     */
    private function getDateTransactionRange(?PayoutTransaction $lastTransaction, Carbon $referralCreatedAt, ?Carbon $referralCancelledAt): array
    {
        $createdAtMin = null;
        $createdAtMax = null;
        $lastTransactionCreatedAt = $lastTransaction ? Carbon::parse($lastTransaction->created_at)->addDay() : null;

        if (!is_null($lastTransaction) && $lastTransactionCreatedAt->greaterThan($referralCreatedAt)) {
            $createdAtMin = $lastTransactionCreatedAt;
        } else {
            $createdAtMin = $referralCreatedAt;
        }

        if (!is_null($referralCancelledAt)) {
            $createdAtMax = $referralCancelledAt;
        }
        return array_combine(['createdAtMin', 'createdAtMax'], [$createdAtMin->toDateTimeLocalString(), $createdAtMax ? $createdAtMax->toDateTimeLocalString() : null]);
    }

    /**
     * @param array $transaction
     * @return bool
     * @throws Exception
     */
    private function isTransactionApplied(array $transaction)
    {
        if (Carbon::parse($transaction["createdAt"])->diffInDays(Carbon::now()) < 14) {
            return false;
        }

        if ($payout = PayoutTransaction::query()->where('transaction_id', $transaction['id'])->first()) {
            return false;
        }

        return true;
    }
}
