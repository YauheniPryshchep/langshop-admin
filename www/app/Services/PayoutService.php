<?php

namespace App\Services;

use App\Exceptions\Http\BadRequestError;
use App\Exceptions\Http\NotFoundError;
use App\Models\Payout;
use App\Models\PayoutTransaction;
use App\Models\Store;
use App\Models\Partners\User;
use App\Services\Shopify\TransactionService;
use App\Validators\QueryPagination;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use PDF;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Validation\ValidationException;

class PayoutService
{
    /**
     * @var Builder
     */
    private $baseQuery;

    /**
     * PayoutService constructor.
     */
    public function __construct()
    {
        $this->baseQuery = Payout::query();
    }

    /**
     * @param User $user
     * @return array
     * @throws ValidationException
     */
    public function payouts(User $user): array
    {
        return $this->getCollection($user);
    }

    /**
     * @param string $domain
     * @param User $user
     * @return array
     * @throws ValidationException
     */
    public function storePayouts(string $domain, User $user): array
    {
        $query = $this->baseQuery
            ->whereHas('payoutTransactions', function(Builder $query) use ($domain) { $query
                ->where('myshopifyDomain', $domain);}
            );

        $this->setBaseQuery($query);

        return $this->getCollection($user);
    }

    /**
     * @param int $id
     * @param User $user
     * @return Payout
     */
    public function show(int $id, User $user): Payout
    {
        /**
         * @var Payout $payout
         */
        $payout = $this->baseQuery
            ->where("id", $id)
            ->where("user_id", $user->id)
            ->with('payoutTransactions')
            ->first();

        if (empty($payout)) {
            throw new NotFoundError();
        }

        return $payout;
    }

    /**
     * @param User $user
     * @return array
     * @throws ValidationException
     */
    private function getCollection(User $user)
    {
        $items = [];
        $page = 1;
        do {
            $pagination = new QueryPagination(["page" => $page]);

            $query = $this->baseQuery
                ->where('user_id', $user->id)
                ->with('payoutTransactions');

            $count = $query->count();

            /**
             * @var LengthAwarePaginator $paginator
             */
            $paginator = $query->paginate(
                $pagination->getLimit(),
                ['*'],
                'page',
                $pagination->getPage()
            );

            $totalPages = ceil($count / $pagination->getLimit());

            $items = array_merge($items, $paginator->getCollection()->toArray());

            $page = $page + 1;

        } while ($page < $totalPages);

        return [
            "items" => $items
        ];
    }

    /**
     * @param User $user
     * @param Store $store
     * @return PayoutTransaction|null
     */
    public function getLastStoreTransaction(User $user, Store $store)
    {
        /**
         * @var Payout $payout
         */
        $payout = $this->baseQuery
            ->where('user_id', $user->id)
            ->orderBy('created_at', 'desc')
            ->first();

        if (!$payout) {
            return null;
        }

        /**
         * @var PayoutTransaction $transaction
         */
        $transaction = PayoutTransaction::query()
            ->where('payout_id', $payout->id)
            ->where("myshopifyDomain", $store->name)
            ->orderBy('created_at', 'desc')
            ->first();

        if (!$transaction) {
            return null;
        }

        return $transaction;
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function invoice(int $id)
    {

        /**
         * @var Payout $payout
         */
        $payout = $this->baseQuery
            ->where("id", $id)
            ->with('payoutTransactions')
            ->first()
            ->toArray();

        if (!$payout) {
            throw new BadRequestError();
        }

        $user = User::query()
            ->where("id", $payout['user_id'])
            ->with("userSettings")
            ->first()
            ->toArray();

        $transactions = $payout['payout_transactions'];
        $total = collect($payout['payout_transactions'])->sum('amount');
        $payout_id = $payout['id'];
        $date = Carbon::parse($payout["created_at"])->toFormattedDateString();
        $company_name = $user['settings']['company_name'] ?? "";
        $paypal = $user['settings']['paypal_account'] ?? "";
        $client_address = $user['settings']['client_address'] ?? "";
        $tax = $user['settings']['tax'] ?? "";


        $pdf = PDF::asd('invoices.payout',
            compact('transactions', 'total', 'payout_id', 'date', 'company_name', 'paypal', 'client_address', 'tax'));

        return $pdf->download('invoice.pdf');

    }

    /**
     * @param Builder $baseQuery
     */
    private function setBaseQuery(Builder $baseQuery): void
    {
        $this->baseQuery = $baseQuery;
    }
}
