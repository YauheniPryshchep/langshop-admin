<?php

namespace App\Services\Guzzle;

use App\Logging\LogsStackContainer;
use App\Services\Guzzle\Middlewares\RetryMiddleware;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\HandlerStack;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\HttpFoundation\Response;

class HttpClient extends Client implements ClientInterface
{
    /**
     * HttpClient constructor.
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        if (!array_key_exists('handler', $config)) {
            $config['handler'] = HandlerStack::create();
        }

        $config['handler']->push(RetryMiddleware::factory(
            [
                'retry_with_proxies' => true,
                'retry_on_status'    => [
                    Response::HTTP_TOO_MANY_REQUESTS,
                    Response::HTTP_SERVICE_UNAVAILABLE,
                    Response::HTTP_INTERNAL_SERVER_ERROR,
                ],
                'on_retry_callback'  => function (
                    int $retryCount,
                    float $delay,
                    RequestInterface &$request,
                    array &$options,
                    ResponseInterface $response = null
                ) {
                Log::info("Retrying http request: {$retryCount} attempt with {$delay}s delay.");
                    LogsStackContainer::log(
                        "Retrying http request: {$retryCount} attempt with {$delay}s delay."
                    );
                }
            ]
        ));

        $config = array_replace(
            Config::get('guzzle'),
            $config
        );

        parent::__construct($config);
    }

    /**
     * @param string $method
     * @param string $uri
     * @param array $options
     * @return ResponseInterface
     * @throws GuzzleException
     */
    public function request($method, $uri = '', array $options = []): ResponseInterface
    {
        LogsStackContainer::dump(
            "Send HTTP request",
            [
                'method'  => $method,
                'url'     => $uri,
                'options' => $options
            ]
        );

        try {
            $response = parent::request($method, $uri, $options);
        } catch (GuzzleException $e) {
            LogsStackContainer::log("HTTP request failed with exception", $e);
            Log::info("HTTP request failed with exception");

            throw $e;
        }

        return $response;
    }

    /**
     * Sends a PSR-7 request and returns a PSR-7 response.
     *
     * @param RequestInterface $request
     *
     * @return ResponseInterface
     *
     * @throws GuzzleException
     */
    public function sendRequest(RequestInterface $request): ResponseInterface
    {
        return $this->send($request);
    }
}
