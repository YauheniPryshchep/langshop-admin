<?php

namespace App\Services;

use App\Exceptions\Http\BadRequestError;
use App\Exceptions\Http\NotFoundError;
use App\Exceptions\Http\ResourceExistError;
use App\Models\Vocabulary;
use App\Validators\QueryFilter;
use App\Validators\QueryPagination;
use App\Validators\QuerySort;
use App\Validators\VocabularyCreateItem;
use App\Validators\VocabularyRemoveItem;
use App\Validators\VocabularyUpdateItem;
use Exception;
use Illuminate\Pagination\LengthAwarePaginator;
use MongoDB\Driver\Exception\BulkWriteException;

class VocabularyService
{
    /**
     * VocabularyService constructor.
     */
    public function __construct()
    {

    }

    /**
     * @param $attributes
     * @return array
     */
    public function getItems(array $attributes)
    {
        $pagination = new QueryPagination($attributes);

        $sort = new QuerySort($attributes, [
            '__id',
            'from'
        ]);

        $filter = new QueryFilter($attributes, [
            'from'       => 'filled',
            'to'         => 'filled',
            'source'     => 'filled',
            'sourceId'   => 'required_with:source',
            'original'   => 'filled',
            'translated' => 'filled',
        ]);

        $query = Vocabulary::query();

        if ($filter) {
            $query = $filter->createDatabaseQuery($query);
        }

        if ($sort) {
            $key = $sort->getSortKey() ?? 'updated_at';
            $direction = $sort->getSortDir() ?? 'DESC';

            if ($key === 'created_at') {
                $query->orderBy('priority', $direction === 'ASC' ? 'DESC' : 'ASC');
                $query->orderBy('id', $direction);
            }

            $query->orderBy($key, $direction);
        }

        /**
         * @var LengthAwarePaginator $paginator
         */
        $paginator = $query->paginate(
            $pagination->getLimit(),
            ['*'],
            'page',
            $pagination->getPage()
        );

        $count = $query->count();
        $totalPages = ceil($count / $pagination->getLimit());

        return [
            "items" => $paginator->getCollection(),
            "count" => $count,
            "page"  => $pagination->getPage(),
            "pages" => $totalPages
        ];
    }

    /**
     * @return array
     */
    public function getDirections()
    {
        $directions = [];
        $fromLanguages = Vocabulary::query()
            ->select('from')
            ->distinct()
            ->get()
            ->toArray();

        foreach ($fromLanguages as $from) {
            $direction['from'] = $from[0];
            $direction['to'] = Vocabulary::query()
                ->select('to')
                ->where('from', $from[0])
                ->groupBy('to')
                ->pluck('to');

            $directions[] = $direction;
        }

        return $directions;
    }

    /**
     * @param array $attributes
     * @param string $id
     * @return Vocabulary
     */
    public function update(array $attributes, string $id): Vocabulary
    {
        /**
         * @var Vocabulary $vocabulary
         */
        $vocabulary = Vocabulary::query()->find($id);

        if (!$vocabulary) {
            throw new NotFoundError();
        }

        $query = new VocabularyUpdateItem($attributes);
        $vocabulary->update($query->input());

        return $vocabulary;
    }

    /**
     * @param array $attributes
     * @return Vocabulary
     */
    public function store(array $attributes): Vocabulary
    {
        $query = new VocabularyCreateItem($attributes);
        $input = $query->input();
        $hash = $this->hash($input["original"], $input['from'], $input['to']);
        $_id = $this->id($hash, $input["source"], $input["sourceId"]);
        $count = Vocabulary::query()
            ->where('_id', $_id)
            ->count();

        if ($count > 0) {
            throw new ResourceExistError();
        }
        try {
            /**
             * @var Vocabulary $vocabulary
             */
            $vocabulary = Vocabulary::query()->create(
                array_merge($input, compact(
                    '_id',
                    'hash'
                ))
            );
            return $vocabulary;
        } catch (Exception $e) {
            throw new ResourceExistError();
        }
    }

    /**
     * @param string $id
     * @throws Exception
     */
    public function delete(string $id)
    {
        if (!$vocabulary = Vocabulary::query()->find($id)) {
            throw new NotFoundError();
        }

        $vocabulary->delete();
        return;
    }

    /**
     * @param array $attributes
     */
    public function deleteDirections(array $attributes)
    {
        $query = new VocabularyRemoveItem($attributes);

        /**
         * @var Vocabulary $vocabulary
         */

        $input = $query->input();
        Vocabulary::query()
            ->where('from', $input['from'])
            ->where("to", $input['to'])
            ->delete();

        return;
    }

    /**
     * @param string $hash
     * @param string $source
     * @param int $sourceId
     * @return string
     */
    protected function id(string $hash, string $source, int $sourceId): string
    {
        return md5("{$hash}::{$source}::{$sourceId}");
    }

    /**
     * @param string $string
     * @param string $from
     * @param string $to
     * @return string
     */
    protected function hash(string $string, string $from, string $to): string
    {
        $string = trim($string);
        return md5("{$string}::{$from}::{$to}");
    }
}