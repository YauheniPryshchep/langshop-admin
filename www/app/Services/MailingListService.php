<?php

namespace App\Services;

use App\Exceptions\Http\NotFoundError;
use App\Requests\MailingListRequest;
use App\Objects\MailingListObject;
use App\Exceptions\CustomException;
use Exception;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Facades\Log;

class MailingListService extends MailGunClient
{

    /**
     * @var string
     */
    private $credentials;

    /**
     * @var string
     */
    protected $url;

    /**
     * @var array
     */
    private $connections = [];

    /**
     * MailingListService constructor.
     * @param string $credentials
     */
    public function __construct(string $credentials)
    {
        $this->credentials = $credentials;
        $this->connections = config('mailgun.' . $this->credentials);
        $this->url = 'lists';
    }

    /**
     * Get mailing lists from MailGun API
     * @return array
     * @throws GuzzleException
     */
    public function getMailingLists()
    {
        $response = $this->request($this->credentials, 'GET', $this->url . '/pages', ['query' => ['limit' => 100]]);
        $mailingList = json_decode($response->getBody(), true);
        $items = array_map(function (array $item) {
            return new MailingListObject($item);
        }, $mailingList['items']);
        return $items;
    }

    /**
     * Store mailing list to MailGun API
     * @param array $params
     * @return MailingListObject
     * @throws GuzzleException
     */
    public function storeMailingList(array $params)
    {
        $validator = new MailingListRequest($params);
        $attributes = $validator->create();
        if (!is_array($attributes)) {
            return $attributes;
        }
        $response = $this->request($this->credentials, 'POST', $this->url, ['form_params' => $params]);
        $mailingList = json_decode($response->getBody(), true);
        return new MailingListObject($mailingList['list']);
    }

    /**
     * Store mailing list to MailGun API
     * @param string $address
     * @return MailingListObject
     * @throws GuzzleException
     */
    public function getMailingList(string $address)
    {
        try {
            $response = $this->request($this->credentials, 'GET', $this->url . '/' . $address);
        } catch (Exception $e) {
            return null;
        }
        $mailingList = json_decode($response->getBody(), true);
        return new MailingListObject($mailingList['list']);
    }

    /**
     * @param string $address
     * @return |null
     * @throws GuzzleException
     */
    public function getMaillistUploadedEvent(string $address)
    {
        try {
            $response = $this->request($this->credentials, 'GET', 'events?event=list_uploaded&list=' . $address);
        } catch (Exception $e) {
            return null;
        }
        $events = json_decode($response->getBody(), true);

        return $events['items'];
    }

    /**
     * Update mailing list at MailGun API
     * @param string $address
     * @param array $params
     * @return MailingListObject|array
     * @throws GuzzleException
     */
    public function updateMailingList(string $address, array $params)
    {
        $validator = new MailingListRequest($params);
        $attributes = $validator->create();
        if (!is_array($attributes)) {
            return $attributes;
        }
        try {
            $response = $this->request($this->credentials, 'PUT', $this->url . '/' . $address, ['form_params' => $params]);
        } catch (Exception $e) {
            throw new NotFoundError();
        }
        $mailingList = json_decode($response->getBody(), true);
        return new MailingListObject($mailingList['list']);
    }

    /**
     * Delete mailing list from MailGun API
     * @param string $address
     * @return array
     * @throws GuzzleException
     */
    public function deleteMailingList(string $address)
    {
        try {
            $response = $this->request($this->credentials, 'DELETE', $this->url . '/' . $address);
        } catch (Exception $e) {
            throw new NotFoundError();
        }
        return json_decode($response->getBody(), true);
    }

}
