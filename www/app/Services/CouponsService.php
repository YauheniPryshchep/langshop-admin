<?php

namespace App\Services;

use App\Exceptions\Http\NotFoundError;
use App\Models\Coupon;
use App\Models\StoreCoupon;
use App\Traits\FilterTrait;
use App\Utilities\CouponSearch;
use App\Utilities\CouponSort;
use App\Validators\CouponsCreateItem;
use App\Validators\CouponsUpdateItem;
use App\Validators\QueryPagination;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Validator;

class CouponsService
{
    use FilterTrait;
    /**
     * @var array
     */
    private $relatedFilters = [
        'search_term' => CouponSearch::class,
        'sort'        => CouponSort::class,
    ];

    /**
     * @var array
     */
    private $filterValidation = [
        'search_term' => 'string',
    ];
    /**
     * @var Builder
     */
    private $baseQuery;

    /**
     * CouponsService constructor.
     */
    public function __construct()
    {
        $this->baseQuery = Coupon::query();
    }

    /**
     * @param array $attributes
     * @return array
     */
    public function getItems(array $attributes)
    {
        $this->setFilterValidation(array_merge($this->filterValidation, [
            "sort" => "string",
        ]));

        return $this->getCollection($attributes);
    }

    /**
     * @param array $filterValidation
     */
    public function setFilterValidation(array $filterValidation): void
    {
        $this->filterValidation = $filterValidation;
    }

    /**
     * @param Builder $baseQuery
     */
    public function setBaseQuery(Builder $baseQuery): void
    {
        $this->baseQuery = $baseQuery;
    }

    /**
     * @param array $attributes
     * @return array
     */
    private function getCollection(array $attributes)
    {
        $pagination = new QueryPagination($attributes);

        $validator = Validator::make($attributes, $this->filterValidation);
        $filters = $validator->validate();

        $query = $this->baseQuery;

        $query = $this->filterBy($filters, $query, $this->relatedFilters);

        $count = $query->count();

        /**
         * @var LengthAwarePaginator $paginator
         */
        $paginator = $query->paginate(
            $pagination->getLimit(),
            ['*'],
            'page',
            $pagination->getPage()
        );

        $totalPages = ceil($count / $pagination->getLimit());

        return [
            "items" => $paginator->getCollection(),
            "count" => $count,
            "page"  => $pagination->getPage(),
            "pages" => $totalPages
        ];
    }

    /**
     * @param int $id
     * @param array $attributes
     * @return Coupon
     */
    public function update(int $id, array $attributes)
    {
        /**
         * @var Coupon $coupon
         */
        $coupon = Coupon::query()->find($id);

        if (!$coupon) {
            throw new NotFoundError();
        }

        $query = new CouponsUpdateItem($attributes);
        $input = $query->input();
        $coupon->update($query->input());

        return $coupon;
    }

    /**
     * @param array $attributes
     * @return Coupon
     */
    public function store(array $attributes)
    {
        $query = new CouponsCreateItem($attributes);
        /**
         * @var Coupon $coupon
         */
        $coupon = Coupon::query()->create(
            $query->input()
        );

        return $coupon;
    }

    /**
     * @param int $id
     * @return Coupon
     */
    public function show(int $id): Coupon
    {
        /**
         * @var Coupon $coupon
         */
        $coupon = Coupon::query()
            ->where('id', $id)
            ->first();

        if (!$coupon) {
            throw new NotFoundError();
        }

        return $coupon;
    }

    /**
     * @param int $id
     * @throws Exception
     */
    public function destroy(int $id)
    {
        $coupon = Coupon::query()
            ->where('id', $id)
            ->first();

        if (!$coupon) {
            throw new NotFoundError();
        }
        $coupon->delete();
    }

    /**
     * @param int $id
     * @return array
     */
    public function usage(int $id, array $attributes)
    {
        /**
         * @var Coupon $coupon
         */
        $coupon = Coupon::query()
            ->where('id', $id)
            ->first();

        if (!$coupon) {
            throw new NotFoundError();
        }

        $pagination = new QueryPagination($attributes);

        $query = StoreCoupon::query()
            ->where('coupon_id', $coupon->id)
            ->with("store");

        /**
         * @var LengthAwarePaginator $paginator
         */
        $paginator = $query->paginate(
            $pagination->getLimit(),
            ['*'],
            'page',
            $pagination->getPage()
        );

        $count = $query->count();
        $totalPages = ceil($count / $pagination->getLimit());

        return [
            "items" => $paginator->getCollection(),
            "count" => $count,
            "page"  => $pagination->getPage(),
            "pages" => $totalPages
        ];
    }
}