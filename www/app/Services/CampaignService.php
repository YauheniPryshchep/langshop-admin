<?php

namespace App\Services;

use App\Exceptions\Http\NotFoundError;
use App\Models\CampaignDomain;
use App\Models\CampaignRecipientsFilter;
use App\Objects\SimplePaginationObject;
use App\Models\Campaign;
use App\Services\Filters\Resolver;
use App\Validators\CampaignCreateItem;
use App\Validators\CampaignUpdateItem;
use Exception;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Database\Eloquent\Builder;

class CampaignService
{
    const MAX_RECIPIENTS_PER_REQUEST = 500;

    /**
     * @var string
     */
    private $domain;

    /**
     * @var string
     */
    private $connection;

    public function __construct()
    {
        $this->connection = 'default';
        $this->domain = config('mailgun.' . $this->connection . '.domain');
    }

    /**
     * Get campaigns list
     * @param $request
     * @return array
     */
    public function getCampaigns($request)
    {
        $pagination = new SimplePaginationObject();
        $pagination->fillFromRequest($request, [
            'created_at',
            'title',
            'started_at',
            'updated_at',
            'template_id',
            'author_id'
        ]);

        $count = Campaign::query()
            ->count();

        $pages = ceil($count / $pagination->limit);
        if ($pagination->page > $pages) {
            $pagination->page = $pages;
        }

        $query = Campaign::query()
            ->with('author');

        $query->orderBy($pagination->sort->field, $pagination->sort->direction);

        if (!empty($pagination->filter)) {
            $query->where(function (Builder $query) use ($pagination) {
                $query->where('title', 'like', '%' . $pagination->filter . '%')
                    ->orWhere('subject', 'like', '%' . $pagination->filter . '%');
            });
        }


        $items = $query
            ->limit($pagination->limit)
            ->offset($pagination->limit * ($pagination->page - 1))
            ->get();

        return [
            'count'      => $count,
            'items'      => $items,
            'page'       => $pagination->page,
            'totalPages' => $pages
        ];
    }

    /**
     * Get campaign
     * @param integer $id
     * @return Campaign
     */
    public function getCampaign(int $id): Campaign
    {
        /**
         * @var Campaign $campaign
         */

        $campaign = Campaign::query()->with('campaign_domains', 'recipients_filter')->find($id);

        if (is_null($campaign)) {
            throw new NotFoundError();
        }

        return $campaign;
    }


    /**
     * Store campaign
     * @param array $attributes
     * @return Campaign
     */
    public function storeCampaign(array $attributes): Campaign
    {
        $query = new CampaignCreateItem($attributes);

        /**
         * @var Campaign $campaign
         */
        $campaign = Campaign::query()->create($query->input());

        $filter = isset($attributes['recipients_filter'])
            ? [
                'filters' => $attributes['recipients_filter']['filters'] ?? [],
                'limit'   => $attributes['recipients_filter']['limit'] ?? null,
            ]
            : [];

        $campaign->recipients_filter()
            ->save(new CampaignRecipientsFilter($filter));

        $campaign->load('recipients_filter');

        return $campaign;
    }

    /**
     * Update campaign
     * @param array $attributes
     * @param int $id
     * @return NotFoundError|Campaign
     */
    public function updateCampaign(array $attributes, int $id): Campaign
    {
        /**
         * @var Campaign $campaign
         */
        $campaign = Campaign::query()->find($id);

        if (!$campaign) {
            throw new NotFoundError();
        }

        $query = new CampaignUpdateItem($attributes);

        $input = $query->input();

        $campaign->update($query->input());

        if ($input['status'] === Campaign::STATUS_READY_TO_START) {
            $this->addDomainsToCampaigns($campaign);
        }

        if (isset($attributes['recipients_filter'])) {
            $campaign->recipients_filter->update($attributes['recipients_filter']);
        }

        $campaign->load('recipients_filter');

        return $campaign;
    }

    /**
     * Delete campaign
     * @param integer $id
     * @return void
     * @throws GuzzleException
     */
    public function deleteCampaign(int $id)
    {
        if (!$campaign = Campaign::query()->find($id)) {
            throw new NotFoundError();
        }
        $mailGunListService = new MailingListService('default');
        $mailgunDomain = config('mailgun.default.domain');
        $address = 'campaign-' . $campaign->id . '@' . $mailgunDomain;

        $list = $mailGunListService->getMailingList($address);

        if ($list) {
            $mailGunListService->deleteMailingList($address);
        }

        $campaign->delete();
        return;
    }

    private function addDomainsToCampaigns(Campaign $campaign)
    {
        $resolver = new Resolver(
            [
                'filters' => $campaign->recipients_filter['filters']
            ]
        );

        $recipients = $this->getDomains($resolver, $campaign);

        $domains = array_column($recipients, 'name');

        $rows = [];
        for ($i = 0; $i < count($domains); $i++) {

            CampaignDomain::query()->insert([
                "campaign_id" => $campaign->id,
                "domain"      => $domains[$i]
            ]);
        }

    }

    /**
     * @param Resolver $resolver
     * @param Campaign $campaign
     * @return array
     */
    private function getDomains(Resolver $resolver, Campaign $campaign)
    {
        $recipients = [];
        $limit = $campaign->recipients_filter->limit;
        $page = 1;

        if ($limit && $limit < self::MAX_RECIPIENTS_PER_REQUEST) {
            $resolver->setLimit($limit);
            $result = $resolver->resolve();
            $recipients = $result['items'];
        } else if ($limit) {
            while (count($recipients) < $limit) {
                $resolver->setLimit(self::MAX_RECIPIENTS_PER_REQUEST);
                $resolver->setPage($page);
                $page++;
                $result = $resolver->resolve();
                $recipients = array_merge($recipients, $result['items']);
            }
        } else {
            $recipients = array_merge($recipients, $this->getAllUnLimitedDomains($page, $recipients, null, $resolver));
        }
        return array_slice($recipients, 0, $limit);
    }

    /**
     * @param $page
     * @param $recipients
     * @param $totalPages
     * @param Resolver $resolver
     * @return array
     */
    private function getAllUnLimitedDomains(int $page, array $recipients, $totalPages, Resolver $resolver)
    {
        if (!$totalPages) {
            $resolver->setLimit(self::MAX_RECIPIENTS_PER_REQUEST);
            $resolver->setPage($page);

            $result = $resolver->resolve();

            $recipients = array_merge($recipients, $result['items']);
            $this->getAllUnLimitedDomains($page + 1, $recipients, $result['totalPages'], $resolver);
        } else {
            while ($page < $totalPages) {
                $resolver->setLimit(self::MAX_RECIPIENTS_PER_REQUEST);
                $resolver->setPage($page);
                $page++;
                $result = $resolver->resolve();
                $recipients = array_merge($recipients, $result['items']);
            }
        }
        return $recipients;
    }
}
