<?php

namespace App\Entities\Shopify\Items;

use App\Collections\AbstractEntity;

/**
 * @OA\Schema(schema="TransactionItem")
 */
class TransactionItem extends AbstractEntity
{
    /**
     * @OA\Property(readOnly="true")
     *
     * @var int
     */
    public $id;

    /**
     * @OA\Property()
     *
     * @var string
     */
    public $createdAt;

    /**
     * @var array
     */
    public $netAmount;

    /**
     * @var array
     */
    public $shopifyFee;

    /**
     * @var array
     */
    public $shop;

    /**
     * @var string
     */
    public $cursor;

    /**
     * @param int $id
     * @return static
     */
    public function setId(int $id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @param string $cursor
     * @return static
     */
    public function setCursor(string $cursor)
    {
        $this->cursor = $cursor;
        return $this;
    }

    /**
     * @param array|null $netAmount
     */
    public function setNetAmount(?array $netAmount): void
    {
        $this->netAmount = $netAmount;
    }

    /**
     * @param array|null $shop
     */
    public function setShop(?array $shop): void
    {
        $this->shop = $shop;
    }

    /**
     * @param string|null $createdAt
     */
    public function setCreatedAt(?string $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return array
     */
    public function getShopifyFee(): array
    {
        return $this->shopifyFee;
    }

    /**
     * @param array|null $shopifyFee
     */
    public function setShopifyFee(?array $shopifyFee): void
    {
        $this->shopifyFee = $shopifyFee;
    }

}
