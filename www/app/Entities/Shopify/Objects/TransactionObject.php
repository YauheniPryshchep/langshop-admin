<?php

namespace App\Entities\Shopify\Objects;

use App\Entities\Shopify\Items\TransactionItem;
use App\Services\Shopify\Helpers\GID;

/**
 * @OA\Schema(schema="TransactionObject")
 */
class TransactionObject extends TransactionItem
{

    const GID_PREFIXES = [
        'gid://partners/AppSubscriptionSale/',
        'gid://partners/AppSaleAdjustment/'
    ];

    const GID_PREFIX =  'gid://partners/AppSubscriptionSale/';


    public function __construct(array $properties = [])
    {
        parent::__construct($properties);
    }

    /**
     * @return string
     */
    public function getGID(): string
    {
        return GID::toGid($this->id, self::GID_PREFIX);
    }

    /**
     * @return int
     */
    public function getResourceIdentifier()
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return string
     */
    public static function getGidPrefix(string $id): string
    {
        foreach (self::GID_PREFIXES as $key => $prefix){
            if(is_numeric(GID::toId($id, $prefix))){
                return $prefix;
            }
        }
    }
}
