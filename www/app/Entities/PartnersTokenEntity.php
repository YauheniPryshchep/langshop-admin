<?php

namespace App\Entities;

use InvalidArgumentException;

class PartnersTokenEntity
{
    /**
     * Request token, received after authorization
     *
     * @var string
     */
    private $requestToken;

    /**
     * Max requests bucket size
     *
     * @var int
     */
    private $maxRequestsBucket;

    /**
     * Time to restore 1 request in bucket (in sec).
     *
     * @var float
     */
    private $requestRestoreCycle;

    /**
     * Max concurrent requests for normal throttling
     * Higher concurrence - longer sleeps
     *
     * @var int
     */
    private $allowedConcurrence = 20;

    /**
     * Wait time (in sec) on empty bucket
     *
     * @var float
     */
    private $waitOnEmpty;

    /**
     * Coefficient to calculate requests throttling with quadratic function:
     * Y = Z * (X ^ 2),
     * where Z - coefficient,
     * Y - max seconds to wait per empty bucket,
     * X - max requests bucket
     *
     * @var float
     */
    private $sleepCoefficient;

    /**
     * PartnersTokenEntity constructor.
     */
    public function __construct()
    {
        $this->maxRequestsBucket = 40;
        $this->requestRestoreCycle = 0.5;
        $this->waitOnEmpty = $this->allowedConcurrence * $this->requestRestoreCycle;
        $this->sleepCoefficient = $this->waitOnEmpty / pow($this->maxRequestsBucket, 2);
    }

    /**
     * @param int $requestsLeft
     * @return int
     */
    public function getMicrosecondsToWait(int $requestsLeft): int
    {
        if ($requestsLeft >= $this->maxRequestsBucket) {
            return 0;
        }

        $requestsUsed = $this->maxRequestsBucket - $requestsLeft;

        /**
         * Calculate wait time Y with quadratic function,
         * where Z - calculated sleep coefficient, X - amount of used requests from bucket
         */
        $secondsToWait = $this->sleepCoefficient * pow($requestsUsed, 2);

        return (int)round($secondsToWait * 1000 * 1000);
    }

    /**
     * @return float
     */
    public function getRequestRestoreCycle(): float
    {
        return $this->requestRestoreCycle;
    }

    /**
     * @return int
     */
    public function getMaxRequestsBucket(): int
    {
        return $this->maxRequestsBucket;
    }

    /**
     * @return string
     */
    public function getRequestToken(): string
    {
        return $this->requestToken;
    }
}
