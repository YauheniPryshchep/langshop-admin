<?php

namespace App\Exceptions\Http;

use Symfony\Component\HttpFoundation\Response;
use Throwable;

class ResourceExistError extends HttpError
{
    /**
     * PaymentRequiredError constructor.
     * @param int $internalCode
     * @param string|null $message
     * @param Throwable|null $previous
     */
    public function __construct(
        int $internalCode = Response::HTTP_CONFLICT,
        string $message = null,
        Throwable $previous = null
    ) {
        parent::__construct(
            Response::HTTP_CONFLICT,
            $internalCode,
            $message,
            [],
            $previous
        );
    }
}
