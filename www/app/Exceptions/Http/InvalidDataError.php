<?php

namespace App\Exceptions\Http;

use Symfony\Component\HttpFoundation\Response;
use Throwable;

class InvalidDataError extends HttpError
{
    /**
     * InvalidDataError constructor.
     * @param int $internalCode
     * @param array $errors
     * @param string|null $message
     * @param Throwable|null $previous
     */
    public function __construct(
        int $internalCode = Response::HTTP_UNPROCESSABLE_ENTITY,
        array $errors = [],
        string $message = null,
        Throwable $previous = null
    ) {
        parent::__construct(
            Response::HTTP_UNPROCESSABLE_ENTITY,
            $internalCode,
            $message,
            array_map(
                function ($error) {
                    return is_array($error) ? $error[0] : $error;
                },
                array_values($errors)
            ),
            $previous
        );
    }
}
