<?php

namespace App\Exceptions\Http;

use Symfony\Component\HttpFoundation\Response;
use Throwable;

class InternalError extends HttpError
{
    /**
     * @var bool
     */
    protected $shouldReport = true;

    /**
     * InternalError constructor.
     * @param int $internalCode
     * @param string|null $message
     * @param array $errors
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct(
        int $internalCode = Response::HTTP_INTERNAL_SERVER_ERROR,
        string $message = null,
        array $errors = [],
        int $code = Response::HTTP_INTERNAL_SERVER_ERROR,
        Throwable $previous = null
    )
    {
        parent::__construct(
            $code,
            $internalCode,
            $message,
            $errors,
            $previous
        );
    }
}
