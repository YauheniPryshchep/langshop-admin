<?php

namespace App\Exceptions\Http;

use App\Exceptions\Contracts\ApplicationException;
use Symfony\Component\HttpFoundation\Response;
use Throwable;

class NotFoundError extends HttpError
{
    /**
     * NotFoundError constructor.
     * @param int $internalCode
     * @param string|null $message
     * @param Throwable|null $previous
     */
    public function __construct(
        int $internalCode = ApplicationException::CODE_MISSING_RESOURCE,
        string $message = null,
        Throwable $previous = null
    ) {
        parent::__construct(
            Response::HTTP_NOT_FOUND,
            $internalCode,
            $message,
            [],
            $previous
        );
    }
}
