<?php

namespace App\Exceptions\Http;

use Symfony\Component\HttpFoundation\Response;
use Throwable;

class BadRequestError extends HttpError
{
    /**
     * BadRequestError constructor.
     * @param int $internalCode
     * @param string|null $message
     * @param Throwable|null $previous
     */
    public function __construct(
        int $internalCode = Response::HTTP_BAD_REQUEST,
        string $message = null,
        Throwable $previous = null
    ) {
        parent::__construct(
            Response::HTTP_BAD_REQUEST,
            $internalCode,
            $message,
            [],
            $previous
        );
    }
}
