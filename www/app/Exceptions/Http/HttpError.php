<?php

namespace App\Exceptions\Http;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Throwable;

/**
 * @OA\Schema(
 *   schema="HttpError",
 *   type="object",
 *   @OA\Property(property="code", format="int64", type="integer"),
 *   @OA\Property(property="internal", format="int64", type="integer"),
 *   @OA\Property(property="message", type="string"),
 *   @OA\Property(property="errors", type="array", @OA\Items(type="string"))
 * )
 */
class HttpError extends HttpException
{
    /**
     * @var bool
     */
    protected $shouldReport = false;

    /**
     * @var int
     */
    protected $internal;

    /**
     * @var array
     */
    protected $errors = [];

    /**
     * HttpError constructor.
     * @param int $statusCode
     * @param int $internalCode
     * @param string $message
     * @param array $errors
     * @param Throwable|null $previous
     * @param array $headers
     */
    public function __construct(
        int $statusCode = Response::HTTP_INTERNAL_SERVER_ERROR,
        int $internalCode = Response::HTTP_INTERNAL_SERVER_ERROR,
        string $message = null,
        array $errors = [],
        Throwable $previous = null,
        array $headers = []
    ) {
        $message = $message ?? __("errors." . $internalCode);

        parent::__construct(
            $statusCode,
            $message,
            $previous,
            $headers
        );

        $this->internal = $internalCode;

        if (!empty($errors)) {
            $this->errors = $errors;
        }
    }

    /**
     * @param int $internal
     * @return static
     */
    public function setInternal(int $internal)
    {
        $this->internal = $internal;
        return $this;
    }

    /**
     * @return int
     */
    public function getInternal(): int
    {
        return $this->internal;
    }

    /**
     * @param array $errors
     * @return HttpError
     */
    public function setErrors(array $errors)
    {
        $this->errors = $errors;
        return $this;
    }

    /**
     * @param bool $shouldReport
     * @return static
     */
    public function setShouldReport(bool $shouldReport)
    {
        $this->shouldReport = $shouldReport;
        return $this;
    }

    /**
     * @return array
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    /**
     * @return bool
     */
    public function isShouldReport(): bool
    {
        return $this->shouldReport;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'code'     => $this->getStatusCode(),
            'errors'   => $this->getErrors(),
            'message'  => $this->getMessage(),
            'internal' => $this->getInternal(),
        ];
    }
}
