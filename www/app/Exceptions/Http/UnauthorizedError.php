<?php

namespace App\Exceptions\Http;

use Symfony\Component\HttpFoundation\Response;
use Throwable;

class UnauthorizedError extends HttpError
{
    /**
     * UnauthorizedError constructor.
     * @param int $internalCode
     * @param string|null $message
     * @param Throwable|null $previous
     */
    public function __construct(
        int $internalCode = Response::HTTP_UNAUTHORIZED,
        string $message = null,
        Throwable $previous = null
    ) {
        parent::__construct(
            Response::HTTP_UNAUTHORIZED,
            $internalCode,
            $message,
            [],
            $previous
        );
    }
}
