<?php

namespace App\Exceptions\Http;

use Symfony\Component\HttpFoundation\Response;
use Throwable;

class ServiceUnavailable extends HttpError
{
    /**
     * ServiceUnavailable constructor.
     * @param int $internalCode
     * @param string|null $message
     * @param Throwable|null $previous
     */
    public function __construct(
        int $internalCode = Response::HTTP_SERVICE_UNAVAILABLE,
        string $message = null,
        Throwable $previous = null
    ) {
        parent::__construct(
            Response::HTTP_SERVICE_UNAVAILABLE,
            $internalCode,
            $message,
            [],
            $previous
        );
    }
}
