<?php

namespace App\Exceptions\Http;

use Symfony\Component\HttpFoundation\Response;
use Throwable;

class PaymentRequiredError extends HttpError
{
    /**
     * PaymentRequiredError constructor.
     * @param int $internalCode
     * @param string|null $message
     * @param Throwable|null $previous
     */
    public function __construct(
        int $internalCode = Response::HTTP_PAYMENT_REQUIRED,
        string $message = null,
        Throwable $previous = null
    ) {
        parent::__construct(
            Response::HTTP_PAYMENT_REQUIRED,
            $internalCode,
            $message,
            [],
            $previous
        );
    }
}
