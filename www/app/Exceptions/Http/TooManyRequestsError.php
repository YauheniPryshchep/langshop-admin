<?php

namespace App\Exceptions\Http;

use Symfony\Component\HttpFoundation\Response;
use Throwable;

class TooManyRequestsError extends HttpError
{
    /**
     * ForbiddenError constructor.
     * @param int $internalCode
     * @param string|null $message
     * @param Throwable|null $previous
     */
    public function __construct(
        int $internalCode = Response::HTTP_TOO_MANY_REQUESTS,
        string $message = null,
        Throwable $previous = null
    ) {
        parent::__construct(
            Response::HTTP_TOO_MANY_REQUESTS,
            $internalCode,
            $message,
            [],
            $previous
        );
    }
}
