<?php

namespace App\Exceptions;

use App\Exceptions\Contracts\ApplicationException;
use App\Exceptions\Http\HttpError;
use App\Exceptions\Http\InternalError;
use App\Exceptions\Http\InvalidDataError;
use App\Exceptions\Http\NotFoundError;
use App\Exceptions\Http\ResourceExistError;
use App\Exceptions\Http\TooManyRequestsError;
use App\Logging\LogsStackContainer;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\App;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Exceptions\PostTooLargeException;
use Illuminate\Http\Exceptions\ThrottleRequestsException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Queue\MaxAttemptsExceededException;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
        HttpError::class,
        MaxAttemptsExceededException::class,
        ThrottleRequestsException::class,
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param Exception $exception
     * @return mixed|void
     * @throws Throwable
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * @param Exception $e
     * @return bool
     */
    public function shouldReport(Exception $e)
    {
        if ($e instanceof HttpError) {
            return $e->isShouldReport();
        }

        return parent::shouldReport($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param Request $request
     * @param Exception $e
     * @return JsonResponse
     * @throws Throwable
     */
    public function render($request, Exception $e)
    {
        if ($e instanceof HttpError) {
            return $this->error($request, $e);
        }

        if ($e instanceof ValidationException) {

            return $this->error(
                $request,
                new InvalidDataError(
                    ApplicationException::CODE_INCORRECT_REQUEST_DATA,
                    $e->errors(),
                    null,
                    $e
                )
            );
        }

        if ($e instanceof MethodNotAllowedHttpException
            || $e instanceof NotFoundHttpException) {
            return $this->error(
                $request,
                new NotFoundError(
                    ApplicationException::CODE_INVALID_ROUTE,
                    null,
                    $e
                )
            );
        }

        if($e instanceof ConflictHttpException){
            return $this->error(
                $request,
                new ResourceExistError()
            );
        }

        if ($e instanceof ModelNotFoundException) {
            return $this->error(
                $request,
                new NotFoundError()
            );
        }

        if ($e instanceof MaxAttemptsExceededException
            || $e instanceof ThrottleRequestsException) {
            return $this->error(
                $request,
                new TooManyRequestsError()
            );
        }

        if ($e instanceof PostTooLargeException) {
            return $this->error(
                $request,
                new InvalidDataError(
                    ApplicationException::CODE_REQUEST_DATA_TO_LARGE,
                    [],
                    null,
                    $e
                )
            );
        }

        if ($e instanceof HttpException) {
            $code = $e->getCode();
            if (!$code) {
                $code = 500;
            }

            return $this->error(
                $request,
                new HttpError(
                    $code,
                    $code,
                    $e->getMessage(),
                    [],
                    $e
                )
            );
        }

        LogsStackContainer::log("Received unhandled exception", $e);

        return $this->error(
            $request,
            new InternalError(
                ApplicationException::CODE_UNHANDLED_EXCEPTION,
                null,
                [],
                $e
            )
        );
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param Request $request
     * @param HttpError $e
     * @return JsonResponse
     */
    private function error(Request $request, HttpError $e)
    {
        $data = $e->toArray();

        if (!App::environment('production')) {
            $data['log'] = LogsStackContainer::toArray();
        }

        return response()->json(
            $data,
            $e->getStatusCode(),
            $e->getHeaders()
        );

    }
}
