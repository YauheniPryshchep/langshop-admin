<?php

namespace App\Exceptions\Contracts;

use Exception;
use ReflectionClass;
use ReflectionException;
use ReflectionProperty;

abstract class ApplicationException extends Exception
{
    const CODE_TOKEN_NOT_PROVIDED = 4010;
    const CODE_TOKEN_IS_EXPIRED = 4011;
    const CODE_TOKEN_IS_INVALID = 4012;
    const CODE_INVALID_CREDENTIALS = 4017;
    const CODE_INVALID_ROUTE = 4018;
    const CODE_MISSING_RESOURCE = 4040;
    const CODE_STORE_IS_FROZEN = 4020;

    const CODE_INCORRECT_REQUEST_DATA = 4220;

    const CODE_UNHANDLED_EXCEPTION = 5000;



    /**
     * Prepare the instance for serialization.
     *
     * @return array
     * @throws ReflectionException
     */
    public function __sleep()
    {
        $properties = (new ReflectionClass($this))->getProperties();

        foreach ($properties as $property) {
            $property->setValue($this, $this->getPropertyValue($property));
        }

        return array_values(array_filter(array_map(function (ReflectionProperty $p) {
            return $p->isStatic() ? null : $p->getName();
        }, $properties)));
    }

    /**
     * Restore the model after serialization.
     *
     * @return void
     * @throws ReflectionException
     */
    public function __wakeup()
    {
        foreach ((new ReflectionClass($this))->getProperties() as $property) {
            if ($property->isStatic()) {
                continue;
            }

            $property->setValue($this, $this->getPropertyValue($property));
        }
    }

    /**
     * Get the property value for the given property.
     *
     * @param ReflectionProperty $property
     * @return mixed
     */
    protected function getPropertyValue(ReflectionProperty $property)
    {
        $property->setAccessible(true);

        return $property->getValue($this);
    }
}
