<?php

namespace App\Factories\Shopify;

use App\Entities\Shopify\Items\TransactionItem;
use App\Entities\Shopify\Objects\TransactionObject;
use App\Factories\Interfaces\{ItemFactoryInterface,
    ObjectFactoryInterface};
use App\Services\Shopify\Helpers\GID;
use App\Services\Shopify\Helpers\ValuesMutator;

class TransactionFactory implements
    ObjectFactoryInterface,
    ItemFactoryInterface
{

    /**
     * @var ValuesMutator
     */
    private $valuesMutator;

    /**
     * CollectionFactory constructor.
     */
    public function __construct()
    {

        $this->valuesMutator = new ValuesMutator([
            'id'      => function (string $gid) {
                return GID::toId($gid, TransactionObject::getGidPrefix($gid));
            },
        ]);
    }

    /**
     * @param array $node
     * @return TransactionObject
     */
    public function object(array $node): TransactionObject
    {
        $this->valuesMutator->mutate($node);

        return new TransactionObject($node);
    }

    /**
     * @param array $edge
     * @return TransactionItem
     */
    public function item(array $edge): TransactionItem
    {
        $node = $edge['node'];

        $this->valuesMutator->mutate($node);

        return (new TransactionItem($node))
            ->setCursor($edge['cursor']);
    }

    /**
     * @return array
     */
    public function getKeysMap(): array
    {
        return [];
    }

    /**
     * @param int $resourceId
     * @return string
     */
    public function resourceGid(int $resourceId): string
    {
        return GID::toGid($resourceId, TransactionObject::GID_PREFIX);
    }

    /**
     * @param string $resourceGid
     * @return string|int
     */
    public function resourceId(string $resourceGid)
    {
        return GID::toId($resourceGid, TransactionObject::GID_PREFIX);
    }
}
