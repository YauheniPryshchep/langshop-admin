<?php

namespace App\Factories\Interfaces;

use App\Collections\AbstractEntity;

interface ItemFactoryInterface
{
    /**
     * @param array $edge
     * @return AbstractEntity
     */
    public function item(array $edge);
}
