<?php

namespace App\Factories\Interfaces;

use App\Collections\AbstractEntity;

interface ObjectFactoryInterface
{
    /**
     * @param array $node
     * @return AbstractEntity
     */
    public function object(array $node);
}
