<?php

namespace App\Console;

use App\Console\Commands\DispatchLeadsSearch;
use App\Console\Commands\HandleCompleteCampaigns;
use App\Console\Commands\HandleFlowTrigger;
use App\Console\Commands\HandleLeadsWorker;
use App\Console\Commands\HandleMailCampaigns;
use App\Console\Commands\HandleRemoveBrokenLeadsEmails;
use App\Console\Commands\HandleLocalesGit;
use Illuminate\Console\Scheduling\Schedule;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;
use Laravelista\LumenVendorPublish\VendorPublishCommand;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        HandleLeadsWorker::class,
        DispatchLeadsSearch::class,
        HandleLocalesGit::class,
        HandleMailCampaigns::class,
        HandleCompleteCampaigns::class,
        HandleRemoveBrokenLeadsEmails::class,
        VendorPublishCommand::class,
        HandleFlowTrigger::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command(HandleCompleteCampaigns::class)
            ->everyMinute();
    }
}
