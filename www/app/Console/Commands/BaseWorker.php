<?php

namespace App\Console\Commands;

use Exception;
use Illuminate\Console\Command;


abstract class BaseWorker extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = '';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    /**
     * Main worker execution time in seconds
     *
     * @var int
     */
    private $workExecutionTime = 600;

    /**
     * Cycle normal sleep timeout in seconds
     *
     * @var int
     */
    private $cycleSleep = 1;

    /**
     * Deep sleep in seconds timeout use if last worker cycle not have leads
     *
     * @var int
     */
    private $cycleDeepSleep = 30;

    /**
     * Worker start timestamp
     *
     * @var int
     */
    private $startTime = 0;

    public function __construct($signature, $description)
    {
        parent::__construct();

        $this->signature = $signature;
        $this->description = $description;
    }


    public function handle()
    {
        $this->startTime = microtime(true);

        if (!$this->setupExecutionTime()) {
            return;
        }

        $availableWorkTime = $this->workExecutionTime - 300;
        while ($availableWorkTime > (time() - $this->startTime)) {
            if (!$this->workCycle()) {
                sleep($this->cycleDeepSleep);
            } else {
                sleep($this->cycleSleep);
            }
        }

        fwrite(STDOUT, 'Mailing processing finished after ' . (time() - $this->startTime) . ' seconds');
    }

    /**
     * @return bool
     * @throws Exception
     */
    abstract function workCycle(): bool;


    /**
     * @return bool
     */
    private function setupExecutionTime(): bool
    {
        ini_set('max_execution_time', $this->workExecutionTime);

        if (ini_get('max_execution_time') < $this->workExecutionTime) {
            fwrite(STDERR, 'Mailing worker stopped. max_execution_time is to low');

            return false;
        }

        return true;
    }
}