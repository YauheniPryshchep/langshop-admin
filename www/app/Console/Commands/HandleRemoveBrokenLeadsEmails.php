<?php

namespace App\Console\Commands;

use App\Models\Lead;
use App\Models\LeadEmail;
use App\Models\LeadPhone;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use function GuzzleHttp\Psr7\mimetype_from_extension;

class HandleRemoveBrokenLeadsEmails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'remove:broken-emails';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make synchronization local changes of Locale files with Git repository...';

    /**
     * @return void
     * @throws Exception
     */
    public function handle()
    {


        $count = $this->getEmailsCount();

        $page = 0;
        $pages = ceil($count / 500);
        $brokenEmails = [];

        for ($i = 0; $i < $pages; $i++) {
            $emails = $this->getEmails($i, 500);

            foreach ($emails as $email) {
                $lead = Lead::query()->where('id', $email->lead_id)->first();
                $email->update([
                    "domain" => $lead->shopify_domain
                ]);
            }
        }
    }

    public function getEmails($page, $limit)
    {
        return LeadPhone::query()
            ->limit($limit)
            ->offset($limit * $page)
            ->get();
    }

    public function getEmailsCount()
    {
        return LeadPhone::query()->count();
    }
}