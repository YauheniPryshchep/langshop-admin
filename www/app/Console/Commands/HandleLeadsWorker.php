<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\TransferStats;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\UriInterface;
use stdClass;
use function GuzzleHttp\Psr7\mimetype_from_extension;

class HandleLeadsWorker extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'leads:worker';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Handle leads processing';

    /**
     * Main worker execution time in seconds
     *
     * @var int
     */
    private $workExecutionTime = 600;

    /**
     * Cycle normal sleep timeout in seconds
     *
     * @var int
     */
    private $cycleSleep = 1;

    /**
     * Deep sleep in seconds timeout use if last worker cycle not have leads
     *
     * @var int
     */
    private $cycleDeepSleep = 30;

    /**
     * Worker start timestamp
     *
     * @var int
     */
    private $startTime = 0;

    /**
     * @var ClientInterface
     */
    private $httpClient;

    /**
     * HandleLeadsWorker constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->httpClient = new Client();
    }

    /**
     * @return void
     * @throws Exception
     */
    public function handle()
    {
        fwrite(STDOUT, 'Leads worker started');

        $timestamp = Cache::get('timestamp_leads_worker');
        $interval = config('worker.leads_worker_execute_interval');

        if ($timestamp && time() < $timestamp + $interval) {
            fwrite(STDOUT, 'Leads worker sleep');
            sleep($this->workExecutionTime);
            return;
        }

        Cache::put('timestamp_leads_worker', time());

        $this->startTime = microtime(true);

        if (!$this->setupExecutionTime()) {
            return;
        }

        fwrite(STDOUT, 'Leads worker start execution');

        $availableWorkTime = $this->workExecutionTime - 300;
        while ($availableWorkTime > (time() - $this->startTime)) {
            if (!$this->workCycle()) {
                sleep($this->cycleDeepSleep);
            } else {
                sleep($this->cycleSleep);
            }
        }

        fwrite(STDOUT, 'Leads processing finished after ' . (time() - $this->startTime) . ' seconds');
    }

    /**
     * @return bool
     */
    private function setupExecutionTime(): bool
    {
        ini_set('max_execution_time', $this->workExecutionTime);

        if (ini_get('max_execution_time') < $this->workExecutionTime) {
            fwrite(STDERR, 'Leads worker stopped. max_execution_time is to low');

            return false;
        }

        return true;
    }

    /**
     * @return bool
     */
    private function workCycle(): bool
    {
        if (!$lead = $this->findLead()) {
            return false;
        }

        try {
            $pages = [
                "/",
                "/policies/terms-of-service",
                "/policies/privacy-policy"
            ];

            $emails = [];
            $phones = [];

            foreach ($pages as $page) {
                try {
                    $response = $this->httpClient->request(
                        "GET",
                        "https://{$lead->public_domain}{$page}",
                        [
                            'timeout'         => 20,
                            'connect_timeout' => 20,
                        ]
                    );
                } catch (GuzzleException $e) {
                    continue;
                }

                $content = $this->sanitizeContent($response);

                $emails = array_merge(
                    $emails,
                    $this->findContentEmails($content)
                );

                $phones = array_merge(
                    $phones,
                    $this->findContentPhones($content)
                );
            }

            $shopify_domain = $this->getShopifyDomain($lead->public_domain);

            $this->updateLeadInfo(
                $lead->id,
                [
                    'shopify_domain' => $shopify_domain
                ]
            );

            $this->updateLeadEmails($shopify_domain, $emails);
            $this->updateLeadPhones($shopify_domain, $phones);
            $this->updateLeadStatus($lead->id, true);
        } catch (Exception $e) {
            $this->updateLeadStatus($lead->id, false);
        }
        return true;
    }

    /**
     * @return stdClass|null
     */
    private function findLead(): ?stdClass
    {
        $unchecked = DB::table('leads')
            ->select([
                'id',
                'public_domain'
            ])
            ->where('checked', false)
            ->first();

        if ($unchecked) {
            return $unchecked;
        }

        return DB::table('leads')
            ->select([
                'id',
                'public_domain'
            ])
            ->where('checked', true)
            ->orderBy('updated_at', "asc")
            ->first();
    }

    /**
     * @param int $id
     * @param array $data
     */
    private function updateLeadInfo(int $id, array $data)
    {
        DB::table('leads')
            ->where('id', $id)
            ->update(
                array_merge(
                    $data,
                    ['checked' => true]
                )
            );
    }

    /**
     * @param int $id
     * @param bool $status
     */
    private function updateLeadStatus(int $id, bool $status)
    {

        DB::table('leads')
            ->where('id', $id)
            ->update(
                [
                    "status"     => $status,
                    "updated_at" => Carbon::now()
                ]
            );
    }


    /**
     * @param string $domain
     * @param array $emails
     */
    private function updateLeadEmails(string $domain, array $emails)
    {
        $emails = array_diff(
            array_values(
                array_unique($emails)
            ),

            DB::table('leads_emails')
                ->where('domain', $domain)
                ->pluck('email')
                ->toArray()
        );

        if (empty($emails)) {
            return;
        }

        DB::table('leads_emails')
            ->insert(
                array_map(
                    function ($email) use ($domain) {
                        return [
                            'domain' => $domain,
                            'email'  => $email
                        ];
                    },
                    $emails
                )
            );
    }

    /**
     * @param string $domain
     * @param array $phones
     */
    private function updateLeadPhones(string $domain, array $phones)
    {
        $phones = array_diff(
            array_values(
                array_unique($phones)
            ),
            DB::table('leads_phones')
                ->where('domain', $domain)
                ->pluck('phone')
                ->toArray()
        );

        if (empty($phones)) {
            return;
        }

        DB::table('leads_phones')
            ->insert(
                array_map(
                    function ($phone) use ($domain) {
                        return [
                            'domain' => $domain,
                            'phone'  => $phone
                        ];
                    },
                    $phones
                )
            );
    }

    /**
     * @param string $publicDomain
     * @return string
     * @throws Exception
     */
    private function getShopifyDomain(string $publicDomain): string
    {
        /**
         * @var UriInterface $url
         */
        $url = null;

        try {
            $this->httpClient->request(
                "GET",
                "https://{$publicDomain}/admin",
                [
                    "allow_redirects" => true,
                    'timeout'         => 20,
                    'connect_timeout' => 20,
                    "on_stats"        => function (TransferStats $stats) use (&$url) {
                        $url = $stats->getEffectiveUri();
                    }
                ]
            );
        } catch (GuzzleException $e) {
            throw new Exception("Can't handle response from {$publicDomain}");
        }

        if (!$url) {
            throw new Exception("Missing redirect url");
        }

        $shopifyDomain = $url->getHost();
        if (strpos($url, ".myshopify.com") === false) {
            throw new Exception("Invalid Shopify domain");
        }

        return $shopifyDomain;
    }

    /**
     * @param ResponseInterface $response
     * @return string
     */
    private function sanitizeContent(ResponseInterface $response): string
    {
        $patterns = [
            "/[<]script[^>]*[>](?:[\s\S]*?)[<][\/]script[>]/",
            "/[<]style[^>]*[>](?:[\s\S]*?)[<][\/]style[>]/",
        ];

        $content = (string)$response->getBody();

        foreach ($patterns as $pattern) {
            $content = preg_replace($pattern, "", $content);
        }

        return !is_null($content)
            ? $content
            : (string)$response->getBody();
    }

    /**
     * @param string $content
     * @return array
     */
    private function findContentEmails(string $content): array
    {
        if (!preg_match_all("/\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]{3,}\.[A-Za-z]{2,6}\b/", $content, $matches)) {
            return [];
        }

        return array_values(
            array_filter(
                $matches[0],
                function ($email) {

                    $email = strtolower($email);
                    $explode = explode('.', $email);
                    $domainZone = $explode[count($explode) - 1];

                    if (mimetype_from_extension($domainZone)) {
                        return false;
                    }

                    if (strpos($email, '@template') || strpos($email, '@layout') || strpos($email, '@2x.progressive')) {
                        return false;
                    }

                    $domain = explode('@', $email)[1] ?? "";
                    $host = explode('@', $email)[0] ?? "";

                    $excludedDomains = [
                        'ejemplo.com',
                        'exemplo.com',
                        'domain.com',
                        'example.com',
                        'yourwebsite.com',
                        'website.com',
                        'shopify.com',
                        'somewhere.com',
                        'exemple.com',
                        'courriel.com',
                        'bbb.ccc',
                        'celex.comuna',
                        'youremail.com',
                        'email.com',
                        'capitalbadges.co.uk.co.uk',
                        'yourdomain.com'
                    ];

                    if (in_array($domain, $excludedDomains)) {
                        return false;
                    }

                    $excludeHost = [
                        'ejemplo',
                        'exemplo',
                        'domain',
                        'example',
                        'yourwebsite',
                        'website',
                        'shopify',
                        'somewhere',
                        'exemple',
                        'courriel',
                        'youremail',
                        'yourdomain',
                        'email.example',
                        'emailexample'
                    ];

                    if (in_array(explode('.', $domain)[0], $excludeHost)
                        || $host !== "shopify" && in_array($host, $excludeHost)
                    ) {
                        return false;
                    }

                    return filter_var($email, FILTER_VALIDATE_EMAIL);
                }
            )
        );
    }

    /**
     * @param string $content
     * @return array
     */
    private function findContentPhones(string $content): array
    {
        if (!preg_match_all("/href=([\"'])tel:([^'\"]+?)\\1/", $content, $matches)) {
            return [];
        }

        return $matches[2];
    }
}