<?php

namespace App\Console\Commands;

use App\Jobs\CheckMailingList;
use App\Jobs\CollectCampaignEmails;
use App\Jobs\CollectCampaignStores;
use App\Jobs\CreateCampaignMaillist;
use App\Jobs\SendingCampaign;
use App\Jobs\TriggerFlowAction;
use App\Jobs\UploadCampaignMembers;
use App\Jobs\ValidateCampaignEmails;
use App\Models\Campaign;
use App\Models\Store;
use App\Models\TriggerEventLog;
use App\Models\WorkflowConditionGroup;
use App\Models\WorkflowLogs;
use App\Services\Workflows\ActionInterface;
use App\Services\Workflows\ConditionInterface;
use App\Services\Workflows\EventInterface;
use App\Services\Workflows\WorkflowService;
use Carbon\Carbon;
use Carbon\CarbonImmutable;
use Carbon\CarbonInterface;
use Exception;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;

class HandleFlowTrigger extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'flow:trigger';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Flow trigger';

    /**
     * Main worker execution time in seconds
     *
     * @var int
     */
    private $workExecutionTime = 600;

    /**
     * Cycle normal sleep timeout in seconds
     *
     * @var int
     */
    private $cycleSleep = 1;

    /**
     * Deep sleep in seconds timeout use if last worker cycle not have leads
     *
     * @var int
     */
    private $cycleDeepSleep = 30;

    /**
     * Worker start timestamp
     *
     * @var int
     */
    private $startTime = 0;


    public function handle()
    {
        fwrite(STDOUT, 'Flow worker started');

        $this->startTime = microtime(true);

        if (!$this->setupExecutionTime()) {
            return;
        }

        $availableWorkTime = $this->workExecutionTime - 300;
        while ($availableWorkTime > (time() - $this->startTime)) {
            if (!$this->workCycle()) {
                sleep($this->cycleDeepSleep);
            } else {
                sleep($this->cycleSleep);
            }
        }
    }

    /**
     * @return bool
     * @throws Exception
     */
    private function workCycle(): bool
    {

        ob_start();

        try {
            /**
             * @var TriggerEventLog $trigger
             */
            $trigger = $this->findTrigger();

            if (!$trigger) {
                return false;
            }


            $this->transferTriggerToStatus($trigger, ["status" => TriggerEventLog::STATUS_PROCESSING]);

            $workflowService = new WorkflowService();

            $event = $workflowService->make([
                "workflow_id"    => $trigger->workflow->id,
                "event"          => $trigger->event_slug,
                "conditions"     => $trigger->workflow->conditions->toArray(),
                "failed_actions" => $trigger->workflow->failed_actions->toArray()
            ]);

            $storeId = $trigger->event_data['storeId'];
            /**
             * @var Store $store
             */
            $store = Store::query()->where('id', $storeId)->first();

            $event->setStore($store);

            $conditionsGroups = $event->getConditionsGroups();

            $positions = $workflowService->getGroupsPositions();

            if (count($positions) === $this->getConditionsCount($trigger->id)) {

                $event->doFailedActions();
            }

            foreach ($conditionsGroups as $id => $group) {

                $condition = $this->getCondition($trigger->id, $positions[$id]);

                if (!empty($condition)
                    && in_array($condition->status, [WorkflowLogs::STATUS_SATISFIED, WorkflowLogs::STATUS_NOT_SATISFIED])) {

                    continue;
                }

                if ($this->isHasDelayed($group)) {

                    if (!$condition) {
                        $this->createDelayedCondition($trigger, $positions[$id], $group);
                        return true;
                    }

                    if ($event->isConditionsMatch($positions[$id])) {

                        $condition->update([
                            "status" => WorkflowLogs::STATUS_SATISFIED
                        ]);

                        $this->transferTriggerToStatus($trigger, ["status" => TriggerEventLog::STATUS_DONE]);

                        $event->doActions($positions[$id]);

                        return true;
                    }

                    $condition->update([
                        "status" => WorkflowLogs::STATUS_NOT_SATISFIED
                    ]);

                } else {

                    if ($event->isConditionsMatch($positions[$id])) {

                        $this->createCondition([
                            "trigger_id"         => $trigger->id,
                            "workflow_id"        => $trigger->workflow->id,
                            "condition_group_id" => $positions[$id],
                            "status"             => WorkflowLogs::STATUS_SATISFIED
                        ]);

                        $this->transferTriggerToStatus($trigger, ["status" => TriggerEventLog::STATUS_DONE]);

                        $event->doActions($positions[$id]);

                        return true;
                    }

                    $this->createCondition([
                        "trigger_id"         => $trigger->id,
                        "workflow_id"        => $trigger->workflow->id,
                        "condition_group_id" => $positions[$id],
                        "status"             => WorkflowLogs::STATUS_NOT_SATISFIED
                    ]);
                }
            }

            return true;

        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * @param TriggerEventLog $trigger
     * @param int $conditionGroupId
     * @param array $conditions
     * @return Builder|Model
     */
    private function createDelayedCondition(TriggerEventLog $trigger, int $conditionGroupId, array $conditions)
    {
        $delay = $this->getDelay($conditions);

        $condition = WorkflowLogs::query()
            ->create([
                'trigger_id'         => $trigger->id,
                'workflow_id'        => $trigger->workflow->id,
                'condition_group_id' => $conditionGroupId,
                'check_at'           => $delay
            ]);

        $this->transferTriggerToStatus($trigger, [
            "status"   => TriggerEventLog::STATUS_WAITING_DELAYED_CONDITION,
            "check_at" => $delay
        ]);
        return $condition;
    }

    /**
     * @param array $data
     */
    private function createCondition(array $data)
    {
        WorkflowLogs::query()
            ->create($data);
    }

    /**
     * @param int $triggerId
     * @param int $conditionGroupId
     * @return Builder|Model|object|null
     */
    private function getCondition(int $triggerId, int $conditionGroupId)
    {
        return WorkflowLogs::query()
            ->where('trigger_id', $triggerId)
            ->where('condition_group_id', $conditionGroupId)
            ->first();
    }

    /**
     * @param int $triggerId
     * @return int
     */
    private function getConditionsCount(int $triggerId)
    {
        return WorkflowLogs::query()
            ->where('trigger_id', $triggerId)
            ->where("status", [WorkflowLogs::STATUS_NOT_SATISFIED, WorkflowLogs::STATUS_SATISFIED])
            ->count();
    }

    /**
     * @param array $group
     * @return bool
     */
    private function isHasDelayed(array $group)
    {
        /**
         * @var ConditionInterface $conditionInstance
         */
        foreach ($group as $conditionInstance) {
            if ($conditionInstance->getSlug() === WorkflowConditionGroup::DELAY_CONDITION) {
                return true;
            }
            continue;
        }
        return false;
    }

    /**
     * @param array $conditions
     * @return CarbonInterface|null
     */
    private function getDelay(array $conditions)
    {
        /**
         * @var ConditionInterface $delayCondition
         */
        $delayCondition = Arr::first($conditions, function (ConditionInterface $condition) {
            return $condition->getSlug() === WorkflowConditionGroup::DELAY_CONDITION;
        });

        if (!$delayCondition) {
            return null;
        }
        $conditionData = $delayCondition->getData();
        /**
         * @var CarbonInterface $now
         */
        $now = CarbonImmutable::now();

        return $now->add($conditionData['value'], $conditionData['matcher']);
    }

    /**
     * @return bool
     */
    private function setupExecutionTime(): bool
    {
        ini_set('max_execution_time', $this->workExecutionTime);

        if (ini_get('max_execution_time') < $this->workExecutionTime) {
            fwrite(STDERR, 'Mailing worker stopped. max_execution_time is to low');

            return false;
        }

        return true;
    }

    /**
     * @param TriggerEventLog $trigger
     * @param array $data
     */
    private function transferTriggerToStatus(TriggerEventLog $trigger, array $data)
    {
        $trigger->update($data);
    }

    /**
     * @return TriggerEventLog|null
     */
    private function findTrigger()
    {
        /**
         * @var TriggerEventLog $trigger
         */
        $trigger = TriggerEventLog::query()
            ->whereIn('status', [
                TriggerEventLog::STATUS_WAITING_DELAYED_CONDITION,
                TriggerEventLog::STATUS_WAITING_IN_START,
            ])
            ->where(function ($query) {
                $query->where('check_at', '<=', Carbon::now())
                    ->orWhereNull('check_at');
            })
            ->first();

        return $trigger;
    }


}