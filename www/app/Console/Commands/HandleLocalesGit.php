<?php

namespace App\Console\Commands;

use Exception;
use Illuminate\Console\Command;

class HandleLocalesGit extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'locales:sync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make synchronization local changes of Locale files with Git repository...';

    /**
     * @return void
     * @throws Exception
     */
    public function handle()
    {
        // Possible values: [clone, pull , push, commit ]
        $command = $this->argument('command');
        if(!$command) return;

        switch ($command):
            case "clone":
                break;
            case "pull":
                break;
            case "push":
                break;
            case "commit":
                break;
        endswitch;
    }
}