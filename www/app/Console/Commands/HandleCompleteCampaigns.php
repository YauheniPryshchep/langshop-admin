<?php

namespace App\Console\Commands;

use App\Models\Campaign;
use App\Services\MailgunStatsService;
use App\Services\MailingListService;
use Carbon\Carbon;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Console\Command;


class HandleCompleteCampaigns extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'campaigns:complete';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Handle complete campaigns';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $campaigns = $this->findCampaigns();

        foreach ($campaigns as $campaign) {

            if (!$this->isCampaignsFinished($campaign)) {
                continue;
            }

            $campaign->update(
                [
                    "status"       => Campaign::STATUS_DONE,
                    "completed_at" => Carbon::now()
                ]);
        }
    }

    /**
     * @param Campaign $campaign
     * @return bool
     * @throws GuzzleException
     */
    private function isCampaignsFinished(Campaign $campaign)
    {
        $mailgunDomain = config('mailgun.default.domain');
        $tag = 'campaign-' . $campaign->id . '@' . $mailgunDomain;
        $mailgunStatsService = new MailgunStatsService('default');
        $mailGunListService = new MailingListService('default');
        $mailingList = $mailGunListService->getMailingList($tag);


        try {
            $stats = $mailgunStatsService->getTagStats($tag, $campaign->started_at);

            if (!count($stats)) {
                return false;
            }

            $countStats = $this->count($stats['stats']);


            if ($countStats < $mailingList->getMembersCount()) {
                return false;
            }

            return true;
        } catch (\Exception $exception) {
            return false;
        }

    }

    /**
     * @param $stats
     * @return int
     */
    private function count($stats)
    {
        $count = 0;

        foreach ($stats as $stat) {
            $count += $stat['delivered']['total'];
            $count += $stat['failed']['temporary']['total'];
            $count += $stat['failed']['permanent']['total'];
        }

        return $count;

    }

    /**
     * @return Campaign
     */
    private function findCampaigns()
    {
        /**
         * @var Campaign $campaigns
         */
        $campaigns = Campaign::query()
            ->where('status', Campaign::STATUS_WAITING_TO_COMPLETE)
            ->get();

        return $campaigns;
    }
}