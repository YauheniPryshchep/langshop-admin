<?php

namespace App\Console\Commands;

use App\Jobs\CheckMailingList;
use App\Jobs\CollectCampaignEmails;
use App\Jobs\CollectCampaignStores;
use App\Jobs\CreateCampaignMaillist;
use App\Jobs\PrepareEmailValidates;
use App\Jobs\SendingCampaign;
use App\Jobs\UploadCampaignMembers;
use App\Jobs\ValidateCampaignEmails;
use App\Models\Campaign;
use App\Models\CampaignProcess;
use Carbon\Carbon;
use Exception;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Log;

class HandleMailCampaigns extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'campaigns:mailing';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Mailing Campaigns';

    /**
     * Main worker execution time in seconds
     *
     * @var int
     */
    private $workExecutionTime = 600;

    /**
     * Cycle normal sleep timeout in seconds
     *
     * @var int
     */
    private $cycleSleep = 1;

    /**
     * Deep sleep in seconds timeout use if last worker cycle not have leads
     *
     * @var int
     */
    private $cycleDeepSleep = 30;

    /**
     * Worker start timestamp
     *
     * @var int
     */
    private $startTime = 0;


    public function handle()
    {
        fwrite(STDOUT, 'Mailing worker started');

        $this->startTime = microtime(true);

        if (!$this->setupExecutionTime()) {
            return;
        }

        $availableWorkTime = $this->workExecutionTime - 300;
        while ($availableWorkTime > (time() - $this->startTime)) {
            if (!$this->workCycle()) {
                sleep($this->cycleDeepSleep);
            } else {
                sleep($this->cycleSleep);
            }
        }
    }

    /**
     * @return bool
     */
    private function workCycle(): bool
    {
        try {

            $campaigns = $this->findCampaigns();

            if (!$campaigns) {
                return false;
            }
            /**
             * @var Campaign $campaign
             */
            foreach ($campaigns as $campaign) {
                $this->checkCampaign($campaign);
            }
            return true;

        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * @param Campaign $campaign
     */
    private function checkCampaign(Campaign $campaign)
    {
        switch ($campaign->status) {
            case Campaign::STATUS_READY_TO_START:
                $this->transferCampaignToStatus($campaign, Campaign::STATUS_COLLECT_STORES);
                CollectCampaignStores::dispatch($campaign);
                break;
            case Campaign::STATUS_WAITING_TO_COLLECT_EMAILS:
                $this->transferCampaignToStatus($campaign, Campaign::STATUS_COLLECT_EMAILS);
                CollectCampaignEmails::dispatch($campaign);
                break;
            case Campaign::STATUS_WAITING_TO_PREPARE_EMAIL_VALIDATES:
                $this->transferCampaignToStatus($campaign, Campaign::STATUS_PREPARE_EMAIL_VALIDATES);
                PrepareEmailValidates::dispatch($campaign);
                break;
            case Campaign::STATUS_VALIDATE_EMAILS:
                if ($this->isValidationEmailsFinish($campaign)) {
                    $this->transferCampaignToStatus($campaign, Campaign::STATUS_WAITING_TO_CREATE_MAILLIST);
                }
                break;
            case Campaign::STATUS_WAITING_TO_CREATE_MAILLIST:
                $this->transferCampaignToStatus($campaign, Campaign::STATUS_CREATE_MAILLIST);
                CreateCampaignMaillist::dispatch($campaign);
                break;
            case Campaign::STATUS_WAITING_TO_UPLOAD_MEMBERS:
                $this->transferCampaignToStatus($campaign, Campaign::STATUS_UPLOAD_MEMBERS);
                UploadCampaignMembers::dispatch($campaign);
                break;
            case Campaign::STATUS_WAITING_TO_CHECK_MAILLIST:
                $this->transferCampaignToStatus($campaign, Campaign::STATUS_CHECK_MAILLIST);
                CheckMailingList::dispatch($campaign);
                break;
            case Campaign::STATUS_WAITING_TO_SENDING_EMAIL:
                $this->transferCampaignToStatus($campaign, Campaign::STATUS_SENDING_EMAIL);
                SendingCampaign::dispatch($campaign);
                break;
            default:
                break;
        }

    }

    /**
     * @return bool
     */
    private function setupExecutionTime(): bool
    {
        ini_set('max_execution_time', $this->workExecutionTime);

        if (ini_get('max_execution_time') < $this->workExecutionTime) {
            fwrite(STDERR, 'Mailing worker stopped. max_execution_time is to low');

            return false;
        }

        return true;
    }

    /**
     * @param Campaign $campaign
     * @param int $status
     */
    private function transferCampaignToStatus(Campaign $campaign, int $status)
    {
        if ($status === Campaign::STATUS_COLLECT_STORES) {
            $campaign->update([
                "status"     => $status,
                "started_at" => Carbon::now()
            ]);
        } else {
            $campaign->update([
                "status" => $status
            ]);
        }
    }

    /**
     * @return Builder[]|Collection
     */
    private function findCampaigns()
    {

        return Campaign::query()
            ->whereIn('status', [
                Campaign::STATUS_READY_TO_START,
                Campaign::STATUS_WAITING_TO_COLLECT_EMAILS,
                Campaign::STATUS_WAITING_TO_VALIDATE_EMAILS,
                Campaign::STATUS_WAITING_TO_PREPARE_EMAIL_VALIDATES,
                Campaign::STATUS_VALIDATE_EMAILS,
                Campaign::STATUS_WAITING_TO_CREATE_MAILLIST,
                Campaign::STATUS_WAITING_TO_UPLOAD_MEMBERS,
                Campaign::STATUS_WAITING_TO_SENDING_EMAIL,
                Campaign::STATUS_WAITING_TO_CHECK_MAILLIST,
            ])
            ->where(function ($query) {
                $query->where('start_at', '<=', Carbon::now())
                    ->orWhereNull('start_at');
            })
            ->with('recipients_filter')
            ->get();
    }

    public function isValidationEmailsFinish(Campaign $campaign)
    {
        $count = CampaignProcess::query()
            ->where("processor", Campaign::STATUS_VALIDATE_EMAILS)
            ->where('campaign_id', $campaign->id)
            ->whereIn('status', [CampaignProcess::STATUS_PENDING, CampaignProcess::STATUS_DISPATCHING])
            ->count();

        return !$count;
    }

}