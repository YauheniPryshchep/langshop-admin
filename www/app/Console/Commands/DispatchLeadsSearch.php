<?php

namespace App\Console\Commands;

use App\Jobs\ViewDnsIpReverse;
use Exception;
use Illuminate\Support\Facades\DB;

class DispatchLeadsSearch extends BaseWorker
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'leads:search';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Dispatch jobs to search leads';

    /**
     * @return void
     * @throws Exception
     */

    public function __construct()
    {
        parent::__construct($this->signature, $this->description);
    }

    public function workCycle(): bool
    {
        if ($this->isJobRunning()) {
            return false;
        }
        $this->updateJobStatus();
        ViewDnsIpReverse::dispatch();
        return true;
    }

    private function updateJobStatus()
    {
        $job = DB::table('jobs_logger')
            ->where("job_type", ViewDnsIpReverse::class)
            ->first();

        if (!$job) {
            DB::table('jobs_logger')
                ->insert([
                    "job_type" => ViewDnsIpReverse::class,
                    "job_data" => json_encode([
                        "status" => 1
                    ])
                ]);
        } else {
            DB::table('jobs_logger')
                ->where("job_type", ViewDnsIpReverse::class)
                ->update([
                    "job_data" => json_encode([
                        "status" => 1
                    ])
                ]);
        }
    }

    private function isJobRunning(): bool
    {
        $job = DB::table('jobs_logger')
            ->where("job_type", ViewDnsIpReverse::class)
            ->first();

        if (!$job) {
            return false;
        }

        $job_data = json_decode($job->job_data);

        return $job_data->status ?? false;
    }


}