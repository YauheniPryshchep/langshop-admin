<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @OA\Schema(
 *      schema="WorkflowModel",
 *      type="object",
 *      @OA\Property(property="title", type="string"),
 *      @OA\Property(property="event_slug", type="string"),
 *      @OA\Property(property="status", type="integer"),
 *      @OA\Property(property="conditions", type="array", @OA\Items(ref="#/components/schemas/ConditionGroupModel")),
 *      @OA\Property(property="created_at", type="string", pattern="\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}", readOnly="true"),
 *      @OA\Property(property="updated_at", type="string", pattern="\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}", readOnly="true"),
 * )
 */
class Workflow extends Model
{
    const STATUS_DISABLED = 0;
    const STATUS_ACTIVE = 1;

    const EVENT_APPLICATION_INSTALLED = 'application-installed';
    const EVENT_APPLICATION_UNINSTALLED = 'application-uninstalled';
    const EVENT_ABILITIES_ACTIVATED = 'abilities-activated';
    const EVENT_SUBSCRIPTION_ACTIVATED = 'subscription-activated';
    const EVENT_SUBSCRIPTION_TRIAL_STARTED = 'subscription-trial-started';
    const EVENT_STORE_STARTS_PAYING = 'store-starts-paying';

    /**
     * @var string
     */
    protected $table = 'workflows';

    /**
     * @var array
     */
    protected $hidden = [
        'workflowConditions',
        'workflowFailedActions',
        'token',
        'scopes'
    ];

    /**
     * @var array
     */
    protected $appends = [
        'conditions',
        'failed_actions'
    ];



    protected $fillable = [
        'title',
        'event_slug',
        'status'
    ];

    /**
     * @return HasMany
     */
    public function workflowConditions()
    {
        return $this->hasMany(
            WorkflowConditionGroup::class,
            'workflow_id',
            'id'
        );
    }

    /**
     * @return array
     */
    public function getConditionsAttribute()
    {
        return $this->workflowConditions;
    }


    /**
     * @return Builder[]|Collection
     */
    public function workflowFailedActions()
    {
        return WorkflowAction::query()
            ->where('conditions_group_id', null)
            ->where("workflow_id", $this->id)
            ->get();
    }

    /**
     * @return array
     */
    public function getFailedActionsAttribute()
    {
        return WorkflowAction::query()
            ->where('conditions_group_id', null)
            ->where("workflow_id", $this->id)
            ->get();
    }
}
