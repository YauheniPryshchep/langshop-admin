<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @OA\Schema(
 *      schema="ConditionGroupModel",
 *      type="object",
 *      @OA\Property(property="successAction", ref="#/components/schemas/ActionModel" ),
 *      @OA\Property(property="failedAction", ref="#/components/schemas/ActionModel" ),
 *      @OA\Property(property="position", type="integer"),
 *      @OA\Property(property="conditions", type="array", @OA\Items(ref="#/components/schemas/ConditionsModel")),
 *      @OA\Property(property="created_at", type="string", pattern="\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}", readOnly="true"),
 *      @OA\Property(property="updated_at", type="string", pattern="\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}", readOnly="true"),
 * )
 */

/**
 * @OA\Schema(
 *      schema="ConditionsModel",
 *      type="object",
 *      @OA\Property(property="condition", type="string"),
 *      @OA\Property(property="matcher", type="string"),
 *      @OA\Property(property="value", type="string"),
 * )
 */
class WorkflowConditionGroup extends Model
{
    /**
     * @var string
     */
    protected $table = 'workflows_conditions_groups';

    const ACTIVE_STATUS_CONDITION = 'active_status';
    const COUPON_CONDITION = 'condition';
    const DEMO_CONDITION = 'demo';
    const DELAY_CONDITION = 'delay';
    const EVENT_COUNTER_CONDITION = 'event_counter';
    const LANGSHOP_PLAN_CONDITION = 'langshop_plan';
    const REFERRAL_PROGRAM_CONDITION = 'referral_program';
    const SHOPIFY_PLAN_CONDITION = 'shopify_plan';
    const STORE_CURRENCY_CONDITION = 'store_currency';
    const STORE_CUSTOMER_EMAIL_CONDITION = 'store_customer_email';
    const STORE_DOMAIN_CONDITION = 'store_domain';
    const STORE_EMAIL_CONDITION = 'store_email';
    const STORE_NAME_CONDITION = 'store_name';
    const STORE_PASSWORD_ENABLED_CONDITION = 'store_password_enabled';
    const STORE_PRIMARY_LOCALE_CONDITION = 'store_primary_locale';
    const STORE_TIMEZONE_CONDITION = 'store_timezone';
    const TEST_CHARGE_CONDITION = 'test_charge';


    const END_WITH_MATCHER = 'end_with';
    const GRATER_THEN_MATCHER = 'grater_then';
    const GRATER_THEN_OR_EQUAL_MATCHER = 'grater_then_or_equal';
    const IS_CONTAINS_MATCHER = 'is_contains';
    const IS_EQUAL_MATCHER = 'is_equal';
    const IS_FALSE_MATCHER = 'is_false';
    const IS_NOT_CONTAINS_MATCHER = 'is_not_contains';
    const IS_NOT_EQUAL_MATCHER = 'is_not_equal';
    const IS_TRUE_MATCHER = 'is_true';
    const LESS_THEN_MATCHER = 'less_then';
    const LESS_THEN_OR_EQUAL_MATCHER = 'less_then_or_equal';
    const NOT_END_WITH_MATCHER = 'not_end_with';
    const NOT_START_WITH_MATCHER = 'not_start_with';
    const START_WITH_MATCHER = 'start_with';


    /**
     * @var array
     */
    protected $hidden = [
        'workflow_id',
        "conditionActions"
    ];

    /**
     * @var array
     */
    protected $appends = [
        'actions',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'conditions' => 'array'
    ];

    /**
     * @var array
     */
    protected $fillable = [
        'workflow_id',
        'position',
        'conditions'
    ];

    /**
     * @return BelongsTo
     */
    public function workflow()
    {
        return $this->belongsTo(Workflow::class, 'workflow_id', 'id');
    }

    /**
     * @return HasMany
     */
    public function conditionActions()
    {
        return $this->hasMany(WorkflowAction::class, 'conditions_group_id', 'id');
    }

    /**
     * @return array
     */
    public function getFailedActionAttribute()
    {
        if (empty($this->conditionFailedAction)) {
            return null;
        }

        return $this->conditionFailedAction;
    }

    /**
     * @return array
     */
    public function getActionsAttribute()
    {
        return $this->conditionActions;
    }

}
