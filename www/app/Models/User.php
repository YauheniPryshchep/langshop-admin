<?php

namespace App\Models;

use Adldap\Laravel\Traits\HasLdapUser;
use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Lumen\Auth\Authorizable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Model implements AuthenticatableContract, AuthorizableContract, JWTSubject
{
    use Authenticatable, Authorizable, SoftDeletes, HasLdapUser;

    /**
     * @var array
     */
    protected $dates = [
        'deleted_at'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'photo'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'pivot'
    ];

    /**
     * @return BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany(
            Role::class,
            'users_roles',
            'users_id',
            'roles_id'
        );
    }

    /**
     * @return BelongsToMany
     */
    public function scopes()
    {
        return $this->roles()
            ->join('roles_scopes', 'roles_scopes.roles_id', '=', 'roles.id')
            ->join('scopes', 'scopes.id', '=', 'roles_scopes.scopes_id')
            ->select('scopes.*');
    }

    /**
     * @param $value
     */
    public function setPhotoAttribute($value)
    {
        $this->attributes['photo'] = $value;
    }

    /**
     * @param string $scope
     * @return bool
     */
    public function haveScope(string $scope)
    {
        return $this->scopes()
                ->where('scopes.name', $scope)
                ->count() > 0;
    }

    /**
     * @return bool
     */
    public function isSAdmin()
    {
        return $this->roles()
                ->where('name', '=', 'sadmin')
                ->count() > 0;
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}
