<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StoreDemo extends Model
{
    /**
     * @var string
     */
    protected $connection = 'langshop';

    /**
     * @var string
     */
    protected $table = 'stores_demo';

    /**
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    /**
     * @var string
     */
    protected $primaryKey = 'name';

    /**
     * @var string
     */
    protected $keyType = 'string';

    /**
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var bool
     */
    public $timestamps = false;
}
