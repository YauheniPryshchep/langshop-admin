<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @OA\Schema(
 *      schema="PayoutsModel",
 *      type="object",
 *      allOf={
 *          @OA\Schema(
 *              @OA\Property(property="id", type="number"),
 *              @OA\Property(property="status", type="number"),
 *              @OA\Property(property="paypal", type="string"),
 *              @OA\Property(property="created_at", type="string", pattern="\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}", nullable="true"),
 *              @OA\Property(property="updated_at", type="string", pattern="\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}", nullable="true")
 *          ),
 *          @OA\Schema(
 *              @OA\Property(property="transactions", type="array", @OA\Items(ref="#/components/schemas/PayoutTransactionModel")),
 *          )
 *      }
 * )
 */
class Payout extends Model
{

    const PENDING_STATUS = 1;
    const PAID_STATUS = 2;
    const DECLINED_STATUS = 3;
    const FROZEN_STATUS = 4;
    const ACCEPTED_STATUS = 5;

    /**
     * @var string
     */
    protected $connection = 'partners';

    /**
     * @var string
     */
    protected $table = 'payouts';

    protected $fillable = [
        'user_id',
        'paypal',
        'status'
    ];

    /**
     * @return HasMany
     */
    public function payoutTransactions()
    {
        return $this->hasMany(PayoutTransaction::class, 'payout_id', 'id');
    }



//    public function getDataAttribute()
//    {
//        $storeData = [];
//
//        $this->storeData->each(function (StoreData $data) use (&$storeData) {
//            $storeData[$data->key] = $data->value;
//        });
//
//        return $storeData;
//    }

    /**
     * @param int $options
     * @return string
     */
    public function toJson($options = 0)
    {
        return parent::toJson(JSON_FORCE_OBJECT);
    }


}
