<?php

namespace App\Models\Partners;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class UserCommission extends Model {

    /**
     * @var string
     */
    protected $table = "users_commissions";
    /**
     * @var string
     */
    protected $connection = "partners";

    /**
     * @var array
     */
    protected $hidden = [
        'user_id',
        'id'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        "commission",
        "created_at"
    ];

    /**
     * UserData constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = []) {
        parent::__construct($attributes);
    }

    /**
     * @return BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }



}
/**
 * @OA\Schema(
 *      schema="UserData",
 *      type="object",
 *      @OA\Property(property="first_name", type="string", maxLength=255),
 *      @OA\Property(property="last_name", type="string", maxLength=255),
 *      @OA\Property(property="company", type="string", maxLength=255),
 *      @OA\Property(property="position", type="string", maxLength=255),
 * )
 */




