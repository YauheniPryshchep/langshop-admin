<?php

namespace App\Models\Partners;

use App\Helpers\ImageHelper;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Arr;

class User extends Model
{
    const USERS_DIR_NAME = "user";

    /**
     * @var string
     */
    protected $table = "users";

    /**
     * @var string
     */
    protected $connection = 'partners';

    protected $hidden = [
        'password',
        'updated_at',
        'email_confirmed',
        'userData',
        'userSettings'
    ];

    /**
     * @var array
     */
    protected $with = [
        //'userData'
    ];

    /**
     * @var array
     */
    protected $appends = [
        'data',
        'settings',
        'isEmailConfirmed',
        'commissions'
    ];

    /**
     * @var array
     */
    protected $castDataAttributes;

    /**
     * User constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->castDataAttributes = [
            'avatar' => function ($value) {
                return $this->getAvatarThumbnail($value, ImageHelper::THUMBNAIL_LARGE_SIZE);
            }
        ];
    }

    protected static function boot()
    {
        parent::boot();
    }

    /**
     * @return mixed
     */
    public function isEmailConfirmed()
    {
        return $this->isEmailConfirmed;
    }

    /**
     * @param $email
     * @return string
     */
    public function getEmailAttribute($email)
    {
        return $email ?? "";
    }

    public function getIsEmailConfirmedAttribute()
    {
        return filter_var($this->email_confirmed, FILTER_VALIDATE_BOOLEAN);
    }

    /**
     * @return HasMany
     */
    public function userCommissions()
    {
        return $this->hasMany(UserCommission::class, 'user_id');
    }

    /**
     * @return HasMany
     */
    public function userData()
    {
        return $this->hasMany(UserData::class, "user_id", "id");
    }

    /**
     * @return HasMany
     */
    public function userSettings()
    {
        return $this->hasMany(UserSettings::class, "user_id", "id");
    }


    /**
     * @param string $key
     * @return string|null
     */
    public function getDataValue(string $key): ?string
    {
        $data = $this->userData->where('key', $key)->first();

        if (!$data) {
            return null;
        }

        return $data->value;
    }

    /**
     * @return string|null
     */
    public function getUserName(): ?string
    {
        return $this->getDataValue('first_name') . " " . $this->getDataValue("last_name");
    }

    /**
     * @return string|null
     */
    public function getAvatar(): ?string
    {
        $value = $this->getDataValue('avatar');
        return $this->getAvatarThumbnail($value, ImageHelper::THUMBNAIL_LARGE_SIZE);
    }

    /**
     * @param string $key
     * @return string|null
     */
    public function getSettingsValue(string $key): ?string
    {
        $data = $this->userSettings->where('key', $key)->first();

        if (!$data) {
            return null;
        }

        return $data->value;
    }

    /**
     * @return array|mixed
     */
    public function getCommissionsAttribute()
    {
        if (!$this->userCommissions) {
            return null;
        }

        return $this->userCommissions;
    }

    public function getActiveCommissionAttribute()
    {
        if (empty($this->commissions)) {
            return null;
        }

        $commissions = $this->commissions->filter(function ($commission) {
            return Carbon::parse($commission->created_at)->lessThan(Carbon::now());
        });

        return $commissions->pop();
    }

    /**
     * @param array $transaction
     * @return float|int
     */
    public function getTransactionCommission(array $transaction)
    {
        if (empty($this->commissions)) {
            return $transaction['netAmount']['amount'] * .3;
        }

        $commission = $this->commissions->first(
            function ($commission) use ($transaction) {
                return Carbon::parse($commission->created_at)->lessThan(Carbon::parse($transaction['createdAt']));
            });

        if (empty($commission)) {
            return $transaction['netAmount']['amount'] * .3;
        }

        return $transaction['netAmount']['amount'] * ($commission->commission / 100);
    }

    /**
     * @return array|null
     */
    public function getDataAttribute()
    {
        $data = [];

        if (!$this->userData) {
            return null;
        }

        $this->userData->each(function (UserData $userData) use (&$data) {
            $data[$userData->key] = $this->getCastedDataAttribute($userData->key, $userData->value);
        });

        return $data;
    }

    /**
     * @param $key
     * @param $value
     * @return mixed
     */
    private function getCastedDataAttribute($key, $value)
    {
        if (!$handler = Arr::get($this->castDataAttributes, $key)) {
            return $value;
        }
        if (is_callable($handler)) {
            return call_user_func($handler, $value);
        }

        return $value;
    }

    /**
     * @return array|null
     */
    public function getSettingsAttribute()
    {
        $settings = [];

        if (!$this->userSettings) {
            return null;
        }

        $this->userSettings->each(function (UserSettings $userSettings) use (&$settings) {
            $settings[$userSettings->key] = $userSettings->value;
        });

        return $settings;
    }

    /**
     * @param $value
     * @param int $size
     * @return mixed
     */
    public function getAvatarThumbnail($value, int $size)
    {
        if (empty($value)) {
            return $value;
        }

        return config('partners.partners_cdn_url') . "{$this->id}/" . ImageHelper::getThumbnailName($value, $size);
    }

}
