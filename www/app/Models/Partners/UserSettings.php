<?php

namespace App\Models\Partners;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class UserSettings extends Model {

    /**
     * @var string
     */
    protected $table = "users_settings";

    /**
     * @var string
     */
    protected $connection = 'partners';

    public $timestamps = false;

    protected $hidden = [
      'user_id',
      'id'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'key',
        'value'
    ];

    public function __construct(array $attributes = []) {
        parent::__construct($attributes);
    }

    /**
     * @return BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

}

