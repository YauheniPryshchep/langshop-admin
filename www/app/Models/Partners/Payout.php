<?php

namespace App\Models\Partners;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Payout extends Model
{
    const PENDING_STATUS = 1;
    const PAID_STATUS = 2;
    const DECLINED_STATUS = 3;
    const FROZEN_STATUS = 4;
    const ACCEPTED_STATUS = 5;
    /**
     * @var string
     */
    protected $table = 'payouts';

    /**
     * @var string
     */
    protected $connection = 'partners';

    protected $fillable = [
        'user_id',
        'paypal',
        'status'
    ];

    /**
     * @return HasMany
     */
    public function payoutTransactions()
    {
        return $this->hasMany(PayoutTransaction::class, 'payout_id', 'id');
    }

    public function user(){
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    /**
     * @param int $options
     * @return string
     */
    public function toJson($options = 0)
    {
        return parent::toJson(JSON_FORCE_OBJECT);
    }


}
