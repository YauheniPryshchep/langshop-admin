<?php

namespace App\Models\Partners;

use App\Models\Store;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;

class PayoutTransaction extends Model
{

    /**
     * @var string
     */
    protected $table = 'payout_transactions';

    /**
     * @var string
     */
    protected $connection = 'partners';

    /**
     * @var bool
     */
    public $timestamps = false;

    protected $fillable = [
        "myshopifyDomain",
        "amount",
        "created_at",
        "transaction_id"
    ];

    /**
     * @return HasOne
     */
    public function store()
    {
        return $this->hasOne(Store::class, 'name', 'myshopifyDomain');
    }

    /**
     * @return BelongsTo
     */
    public function payout()
    {
        return $this->belongsTo(Payout::class, 'payout_id');
    }

}
