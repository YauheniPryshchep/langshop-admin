<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * @OA\Schema(
 *      schema="PayoutTransactionModel",
 *      type="object",
 *      allOf={
 *          @OA\Schema(
 *              @OA\Property(property="id", type="number"),
 *              @OA\Property(property="amount", type="number"),
 *          ),
 *          @OA\Schema(
 *              @OA\Property(property="store", ref="#/components/schemas/StoreModel"),
 *          )
 *      }
 * )
 */
class PayoutTransaction extends Model
{
    /**
     * @var string
     */
    protected $connection = 'partners';

    /**
     * @var string
     */
    protected $table = 'payout_transactions';

    /**
     * @var bool
     */
    public $timestamps = false;

    protected $fillable = [
        "myshopifyDomain",
        "amount",
        "created_at",
        "transaction_id"
    ];

    /**
     * @return HasOne
     */
    public function store()
    {
        return $this->hasOne(Store::class, 'name', 'myshopifyDomain');
    }

    /**
     * @return BelongsTo
     */
    public function payout()
    {
        return $this->belongsTo(Payout::class, 'payout_id');
    }

}
