<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class NewsItem extends Model
{
    /**
     * @var string
     */
    protected $table = 'news';

    /**
     * @var array
     */
    protected $fillable = [
        'app',
        'type',
        'src',
        'video',
        'layout',
        'sticky',
        'position',
        'title',
        'description',
        'primaryLink',
        'primaryText',
        'secondaryLink',
        'secondaryText',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'sticky' => 'boolean',
        'position' => 'integer'
    ];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        self::creating(function (NewsItem $item) {
            $position = self::query()
                ->where('app', $item->app)
                ->count() + 1;

            if(empty($item->position)) {
                $item->position = $position;
            } elseif($item->position > $position) {
                $item->position = $position;
            }

            self::query()
                ->where('app', $item->app)
                ->where('position', '>=', $item->position)
                ->update([
                    'position' => DB::raw('position + 1')
                ]);
        });

        self::updating(function (NewsItem $item) {
            if (!$item->isDirty('position')) {
                return;
            }

            $count = self::query()
                    ->where('app', $item->app)
                    ->count();

            if($item->position > $count) {
                $item->position = $count;
            }

            $query = self::query()
                ->where('id', '!=', $item->id)
                ->where('app', $item->app);

            $original = $item->getOriginal();
            if ($item->position > $original['position']) {
                $query
                    ->where('position', '<=', $item->position)
                    ->where('position', '>', $original['position'])
                    ->update([
                        'position' => DB::raw('position - 1')
                    ]);
            } else {
                $query
                    ->where('position', '>=', $item->position)
                    ->where('position', '<', $original['position'])
                    ->update([
                        'position' => DB::raw('position + 1')
                    ]);
            }
        });

        self::deleted(function (NewsItem $item) {
            self::query()
                ->where('app', $item->app)
                ->where('position', '>', $item->position)
                ->update([
                    'position' => DB::raw('position - 1')
                ]);
        });
    }

    /**
     * @param $image
     */
    public function setSrcAttribute($image)
    {
        if ($image instanceof UploadedFile) {
            $fileName = Storage::disk('s3Assets')
                ->put('/', $image, 'public');

            $this->attributes['src'] = Storage::disk('s3Assets')->url($fileName);
        } else {
            $this->attributes['src'] = $image;
        }
    }
}
