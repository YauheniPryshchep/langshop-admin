<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;

class StoreSubscription extends Model
{
    const STATUS_PENDING   = 1;
    const STATUS_ACTIVE    = 2;
    const STATUS_DECLINED  = 3;
    const STATUS_CANCELLED = 4;

    protected $connection ='langshop';

    /**
     * @var string
     */
    protected $table = 'stores_subscriptions';

    /**
     * @var array
     */
    protected $hidden = [
        'store_id',
        'id',
        'charge_id',
        'plan'
    ];

    /**
     * @var array
     */
    protected $fillable = [
        'plan_id',
        'store_id',
        'charge_id',
        'status',
        'annual'
    ];

    /**
     * @var array
     */
    protected $casts = [
        'annual'     => 'boolean',
        'changeable' => 'boolean',
    ];

    /**
     * @return HasOne
     */
    public function plan(): HasOne
    {
        return $this->hasOne(Plan::class, 'id', 'plan_id');
    }

    /**
     * @return HasOne
     */
    public function charge(): HasOne
    {
        return $this->hasOne(StoreCharge::class, 'charge_id', 'charge_id');
    }

    /**
     * @return BelongsTo
     */
    public function store()
    {
        return $this->belongsTo(Store::class, 'store_id', 'id');
    }

    /**
     * @param int $status
     * @return bool
     */
    public function isStatus(int $status): bool
    {
        return $this->status === $status;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->isStatus(self::STATUS_ACTIVE);
    }

    /**
     * @return bool
     */
    public function isAnnual(): bool
    {
        return !!$this->annual;
    }

    /**
     * @return bool
     */
    public function isChangeable(): bool
    {
        return !!$this->changeable;
    }

    /**
     * @return bool
     */
    public function isFree(): bool
    {
        return $this->getPlan()->isFree();
    }

    /**
     * @return Plan
     */
    public function getPlan(): Plan
    {
        return $this->plan;
    }
}