<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 *      schema="CampaignModel",
 *      type="object",
 *      @OA\Property(property="title", type="string"),
 *      @OA\Property(property="description", type="string"),
 *      @OA\Property(property="subject", type="string"),
 *      @OA\Property(property="type", type="string", enum={"email"}),
 *      @OA\Property(property="template", type="string"),
 *      @OA\Property(property="author_id", type="integer"),
 *      @OA\Property(property="status", type="integer"),
 *      @OA\Property(property="start_at", type="string", pattern="\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}", nullable="true"),
 *      @OA\Property(property="started_at", type="string", pattern="\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}", nullable="true", readOnly="true"),
 *      @OA\Property(property="completed_at", type="string", pattern="\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}", nullable="true", readOnly="true"),
 *      @OA\Property(property="created_at", type="string", pattern="\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}", readOnly="true"),
 *      @OA\Property(property="updated_at", type="string", pattern="\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}", readOnly="true"),
 * )
 */
class Campaign extends Model
{
    /**
     * @var string
     */
    protected $table = 'campaigns';

    const STATUS_DRAFT = 1;
    const STATUS_CANCELED = 2;
    const STATUS_READY_TO_START = 3;
    const STATUS_COLLECT_STORES = 4;
    const STATUS_WAITING_TO_COLLECT_EMAILS = 5;
    const STATUS_COLLECT_EMAILS = 6;
    const STATUS_WAITING_TO_PREPARE_EMAIL_VALIDATES = 7;
    const STATUS_PREPARE_EMAIL_VALIDATES = 8;
    const STATUS_WAITING_TO_VALIDATE_EMAILS = 9;
    const STATUS_VALIDATE_EMAILS = 10;
    const STATUS_WAITING_TO_CREATE_MAILLIST = 11;
    const STATUS_CREATE_MAILLIST = 12;
    const STATUS_WAITING_TO_UPLOAD_MEMBERS = 13;
    const STATUS_UPLOAD_MEMBERS = 14;
    const STATUS_WAITING_TO_CHECK_MAILLIST = 15;
    const STATUS_CHECK_MAILLIST = 16;
    const STATUS_WAITING_TO_SENDING_EMAIL = 17;
    const STATUS_SENDING_EMAIL = 18;
    const STATUS_WAITING_TO_COMPLETE = 19;
    const STATUS_DONE = 20;

    const TYPE_EMAIL = 'email';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'description',
        'subject',
        'type',
        'template',
        'author_id',
        'status',
        'from',
        'sender',
        'reply_to',
        'start_at',
        'started_at',
        'completed_at'
    ];

    /**
     *
     * @var array
     */
    protected $attributes = [
        "start_at" => null,
        "status"   => self::STATUS_DRAFT
    ];

    /**
     * @var array
     */
    protected $casts = [
        "start_at"     => 'datetime',
        "started_at"   => 'datetime',
        "completed_at" => 'datetime'
    ];

    public function campaign_domains()
    {
        return $this->hasMany('App\Models\CampaignDomain', 'campaign_id', 'id')->select('id', 'campaign_id', 'domain');
    }

    public function recipients_filter()
    {
        return $this->hasOne(CampaignRecipientsFilter::class, 'campaign_id', 'id');
    }

    public function author()
    {
        return $this->hasOne(User::class, "id", 'author_id');
    }
}

