<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StoreUser extends Model
{
    /**
     * @var string
     */
    protected $connection = 'langshop';

    /**
     * @var string
     */
    protected $table = 'stores_users';

    /**
     * @var bool
     */
    public $timestamps = false;

}
