<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class WorkflowActionLogs extends Model
{

    const STATUS_PENDING = 1;
    const STATUS_SUCCESS = 2;
    const STATUS_FAILED = 3;
    /**
     * @var string
     */
    protected $table = 'workflow_action_logs';

    /**
     * @var array
     */
    protected $fillable = [
        "store_id",
        "workflow_id",
        'triggered_at',
        'condition_group_id',
        "action_id",
        'status'
    ];

    protected $attributes = [
        "status" => self::STATUS_PENDING
    ];
}
