<?php

namespace App\Models;

use App\Helpers\DateTimeSerializer;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use InvalidArgumentException;

/**
 * @OA\Schema(
 *      schema="StoreModel",
 *      type="object",
 *      @OA\Property(property="name", type="string"),
 *      @OA\Property(property="app", type="string"),
 *      @OA\Property(property="active", type="boolean"),
 *      @OA\Property(property="interacted_at", type="string", pattern="\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}", nullable="true"),
 *      @OA\Property(property="activation_time", type="string", pattern="\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}", nullable="true", readOnly="true"),
 * )
 */
class Store extends Model
{

    const SHOPIFY_AFFILIATE = 'affiliate';
    const SHOPIFY_BASIC = 'basic';
    const SHOPIFY_STANDART = 'professional';
    const SHOPIFY_ADVANCED = 'unlimited';
    const SHOPIFY_CANCELLED = 'cancelled';
    const SHOPIFY_DEVELOPER_PREVIEW = 'partner_test';
    const SHOPIFY_STUFF = 'staff';
    const SHOPIFY_FROZEN = 'frozen';
    const SHOPIFY_PLUS = 'shopify_plus';
    const SHOPIFY_PLUS_PARTNER_SANDBOX = 'plus_partner_sandbox';
    /**
     * @var string
     */
    protected $connection = 'langshop';

    /**
     * @var string
     */
    protected $table = 'stores';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $hidden = [
        'storeData',
        'storeSubscription',
        'token',
        'scopes'
    ];

    /**
     * @var array
     */
    protected $appends = [
        'data',
        'subscription',
    ];

    /**
     * @return HasOne
     */
    public function trial()
    {
        return $this->hasOne(StoreTrial::class, 'name', 'name');
    }

    /**
     * @return HasMany
     */
    public function storeData()
    {
        return $this->hasMany(StoreData::class, 'store_id', 'id');
    }

    /**
     * @return HasOne
     */
    public function storeDemo()
    {
        return $this->hasOne(StoreDemo::class, 'name', 'name');
    }

    /**
     * @return HasOne
     */
    public function storeDiscount()
    {
        return $this->hasOne(StoreDiscount::class, 'name', 'name');
    }

    /**
     * @return HasMany
     */
    public function storeCoupon()
    {
        return $this->hasMany(StoreCoupon::class, 'store_id', 'id');
    }

    /**
     * @return BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(Customer::class, 'stores_users', 'store_id', 'user_id');
    }

    /**
     * @return HasMany
     */
    public function storeHistory()
    {
        return $this->hasMany(StoreHistory::class, 'store_id', 'id');
    }

    /**
     * @return HasMany
     */
    public function referrals()
    {
        return $this->hasMany(ReferralStores::class, 'store_id', 'id');
    }

    /**
     * @return HasOne
     */
    public function storeSubscription()
    {
        return $this->hasOne(StoreSubscription::class, 'store_id', 'id')
            ->orderBy('created_at', 'desc')
            ->whereIn('status', [StoreSubscription::STATUS_ACTIVE, StoreSubscription::STATUS_CANCELLED]);
    }

    /**
     * @return HasOne
     */
    public function lastStoreSubscription()
    {
        return $this->hasOne(StoreSubscription::class, 'store_id', 'id')
            ->orderBy('created_at', 'desc');
    }


    /**
     * @return HasOne
     */
    public function storeStatus()
    {
        return $this->hasOne(StoreHistory::class, 'store_id', 'id')
            ->where("source", "!=", "internal")
            ->whereIn("type", [StoreHistory::TYPE_STORE_CLOSED, StoreHistory::TYPE_STORE_REOPENED, StoreHistory::TYPE_STORE_INSTALLED, StoreHistory::TYPE_STORE_UNINSTALLED])
            ->orderBy('timestamp', 'desc');
    }

    /**
     * @return array
     */
    public function getDataAttribute()
    {
        $storeData = [];

        $this->storeData->each(function (StoreData $data) use (&$storeData) {
            $storeData[$data->key] = $data->value;
        });

        return $storeData;
    }

    /**
     * @return array
     */
    public function getSubscriptionAttribute()
    {
        return $this->storeSubscription;
    }

    /**
     * @return bool
     */
    public function isEnterprisePlan()
    {
        return $this->storeSubscription->plan_id === Plan::PLAN_ENTERPRISE;
    }

    /**
     * @return bool
     */
    public function isFreePlan()
    {
        return $this->storeSubscription->plan_id === Plan::PLAN_FREE;
    }

    /**
     * @return int
     */
    public function getPlanId()
    {
        if (!$this->storeSubscription) {
            return 0;
        }

        return $this->storeSubscription->plan_id;
    }

    /**
     * @return string
     */
    public function getShopifyPlan()
    {
        if (!$this->data) {
            return null;
        }

        return $this->data['plan_display_name'];
    }

    /**
     * @return string
     */
    public function getStoreCurrency()
    {
        if (!$this->data) {
            return null;
        }

        return $this->data['currency'];
    }

    /**
     * @return string
     */
    public function getStoreCustomerEmail()
    {
        if (!$this->data) {
            return null;
        }

        return $this->data['customer_email'];
    }

    /**
     * @return string
     */
    public function getStoreDomain()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getStoreEmail()
    {
        if (!$this->data) {
            return null;
        }

        return $this->data['email'];
    }

    /**
     * @return string
     */
    public function getStoreName()
    {
        if (!$this->data) {
            return null;
        }

        return $this->data['name'];
    }

    /**
     * @return bool
     */
    public function getStorePasswordEnabled()
    {
        if (!$this->data) {
            return null;
        }

        return $this->data['password_enabled'];
    }

    /**
     * @return string
     */
    public function getStorePrimaryLocale()
    {
        if (!$this->data) {
            return null;
        }

        return $this->data['primary_locale'];
    }

    /**
     * @return string
     */
    public function getStoreTimezone()
    {
        if (!$this->data) {
            return null;
        }

        return $this->data['iana_timezone'];
    }

    /**
     * @return HasOne
     */
    public function lastEvent()
    {
        return $this->hasOne(StoreHistory::class, 'store_id', 'id')
            ->orderBy('timestamp', 'DESC');
    }

    /**
     * @param string $key
     * @return string
     * @throws InvalidArgumentException
     */
    public function getDataValue(string $key): string
    {
        $data = $this->storeData->where('key', $key)->first();
        if (!$data) {
            throw new InvalidArgumentException('Missing store data property');
        }

        return $data->value;
    }

    /**
     * @return string
     */
    public function getPlan(): string
    {
        try {
            return $this->getDataValue('plan_display_name');
        } catch (InvalidArgumentException $exception) {
            return "";
        }

    }

    /**
     * @param string $name
     * @return bool
     */
    public function isPlan(string $name): bool
    {
        return $this->getPlan() === $name;
    }

    /**
     * @return bool
     */
    public function isDemo(): bool
    {
        return !empty($this->demo);
    }

    /**
     * @return bool
     */
    public function isPartnerFriendly(): bool
    {
        return $this->isPlan(self::SHOPIFY_AFFILIATE)
            || $this->isPlan(self::SHOPIFY_STUFF)
            || $this->isPlan(self::SHOPIFY_PLUS_PARTNER_SANDBOX)
            || $this->isPlan(self::SHOPIFY_DEVELOPER_PREVIEW);
    }

    /**
     * @return bool
     */
    public function isDevelopment(): bool
    {
        return $this->isPlan(self::SHOPIFY_AFFILIATE)
            || $this->isPlan(self::SHOPIFY_DEVELOPER_PREVIEW);
    }

    /**
     * @return bool
     */
    public function isFrozen(): bool
    {
        return $this->isPlan(self::SHOPIFY_FROZEN);
    }

    /**
     * @return bool
     */
    public function isBillable(): bool
    {
        return !$this->isDemo()
            && !$this->isPartnerFriendly();
    }

    /**
     * @param int $options
     * @return string
     */
    public function toJson($options = 0)
    {
        return parent::toJson(JSON_FORCE_OBJECT);
    }

    /**
     * @return Carbon|null
     */
    public function getFirstInstallationDate(): ?Carbon
    {
        $history = $this->storeHistory()
            ->where('type', "installed")
            ->orderBy('timestamp', 'asc')
            ->first();

        if (!$this->active && !$history) {
            return null;
        }

        $timestamp = ($history)
            ? $history->timestamp
            : 1537510259; // falback for old clients (September 21, 2018)

        return \Illuminate\Support\Carbon::createFromTimestamp($timestamp);
    }

    /**
     * @param int $trialDays
     * @return int
     */
    public function getAvailableTrial(int $trialDays): int
    {
        if (empty($trialDays)) {
            return 0;
        }

        /**
         * @var StoreTrial|null $storeTrial
         */
        $storeTrial = $this->trial;
        if ($storeTrial) {
            $trialDays = $storeTrial->value;
            $liveDays = (int)((time() - strtotime($storeTrial->updated_at)) / 86400);
        } elseif ($firstInstallationDate = $this->getFirstInstallationDate()) {
            $liveDays = (int)((time() - $firstInstallationDate->unix()) / 86400);
        } else {
            $liveDays = 0;
        }

        return $trialDays > $liveDays ? $trialDays - $liveDays : 0;
    }

}
