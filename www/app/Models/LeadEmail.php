<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LeadEmail extends Model
{
    /**
     * @var string
     */
    protected $table = 'leads_emails';

    /**
     * @var array
     */
    protected $fillable = [
        "domain",
        "email"
    ];

    public $timestamps = false;
}

