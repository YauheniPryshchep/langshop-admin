<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Scope extends Model
{
    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'scopes';

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
    ];

    /**
     * @var array
     */
    protected $hidden = [
        'pivot'
    ];

    /**
     * @return HasOne
     */
    public function type()
    {
        return $this->hasOne(
            ScopeType::class,
            'id',
            'scopes_types_id'
        );
    }
}
