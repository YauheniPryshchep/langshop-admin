<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class WebHookLog extends Model
{

    protected $table    = 'webhook_log';
    protected $fillable = [
        'store', 'app', 'type', 'created_at', 'updated_at', 'data'
    ];


}
