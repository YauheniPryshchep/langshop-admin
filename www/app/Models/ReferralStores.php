<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @OA\Schema(
 *      schema="ReferralStoreModel",
 *      type="object",
 *      allOf={
 *      @OA\Schema(ref="#/components/schemas/StoreModel"),
 *      @OA\Schema( @OA\Property(property="referrals", type="array", @OA\Items(
 *              @OA\Property(property="id", type="number"),
 *              @OA\Property(property="store_id", type="number"),
 *              @OA\Property(property="ref", type="string"),
 *              @OA\Property(property="status", type="number", enum={1,2,3}),
 *              @OA\Property(property="start_at", type="string", pattern="\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}", nullable="true"),
 *              @OA\Property(property="created_at", type="string", pattern="\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}", readOnly="true"),
 *              @OA\Property(property="updated_at", type="string", pattern="\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}", readOnly="true"),
 *              @OA\Property(property="canceled_at", type="string", pattern="\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}", readOnly="true"),
 *          )
 * )
 * )
 * })
 */
class ReferralStores extends Model
{
    const STATUS_DECLINED = 1;
    const STATUS_ACCEPTED = 2;
    const STATUS_CANCELLED = 3;

    /**
     * @var string
     */
    protected $connection = 'langshop';

    /**
     * @var string
     */
    protected $table = 'referral_stores';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'ref',
        'status',
        'created_at',
        'store_id'
    ];

    /**
     * @var array
     */
    protected $casts = [
        'cancelled_at' => 'datetime'
    ];

    /**
     * @return BelongsTo
     */
    public function store()
    {
        return $this->belongsTo(Store::class, 'store_id');
    }

}
