<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StoreTrial extends Model
{
    /**
     * @var string
     */
    protected $connection = 'langshop';

    /**
     * @var string
     */
    protected $table = 'stores_trials';

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'value',
    ];

    /**
     * @var string
     */
    protected $primaryKey = 'name';

    /**
     * @var string
     */
    protected $keyType = 'string';

    /**
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var bool
     */
    public $timestamps = false;
}
