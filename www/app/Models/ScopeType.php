<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ScopeType extends Model
{
    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'scopes_types';

    /**
     * @var array
     */
    protected $fillable = [
        'name'
    ];
}
