<?php

namespace App\Models;


use Jenssegers\Mongodb\Eloquent\Model;
/**
 * @OA\Schema(
 *      schema="VocabularyModel",
 *      type="object",
 *      @OA\Property(property="_id", type="string"),
 *      @OA\Property(property="hash", type="string"),
 *      @OA\Property(property="original", type="string"),
 *      @OA\Property(property="translated", type="string"),
 *      @OA\Property(property="from", type="string"),
 *      @OA\Property(property="to", type="string"),
 *      @OA\Property(property="source", type="string", enum={"machine", "agency", "human"}),
 *      @OA\Property(property="sourceId", type="integer")
 * )
 */
class Vocabulary extends Model
{
    const SOURCE_MACHINE = 'machine';
    const SOURCE_AGENCY = 'agency';
    const SOURCE_HUMAN = 'human';

    /**
     * @var string
     */
    protected $connection = 'mongodb';

    /**
     * @var string
     */
    protected $table = 'vocabulary';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = [
        '_id',
        'hash',
        'original',
        'translated',
        'from',
        'to',
        'source',
        'sourceId',
        "sourceAttributes"
    ];

    /**
     * @var array
     */
    protected $attributes = [
        'sourceAttributes'   => null
    ];

    public function getDateFormat()
    {
        return parent::getDateFormat();
    }
}