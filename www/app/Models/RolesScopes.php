<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class RolesScopes extends Model
{
    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'roles_scopes';

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'description'
    ];

    /**
     * @return HasOne
     */
    public function scope()
    {
        return $this->hasOne(Scope::class);
    }

    /**
     * @return HasOne
     */
    public function rolesItems()
    {
        return $this->hasOne(Role::class);
    }
}
