<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LeadPhone extends Model
{
    /**
     * @var string
     */
    protected $table = 'leads_phones';

    /**
     * @var array
     */
    protected $fillable = [
        "domain",
        "phone"
    ];

    public $timestamps = false;
}

