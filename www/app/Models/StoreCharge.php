<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class StoreCharge extends Model
{
    /**
     * @var string
     */
    protected $connection = 'langshop';

    /**
     * @var string
     */
    protected $table = 'store_charges';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $hidden = [
        'store_id'
    ];

    /**
     * @return BelongsTo
     */
    public function store() {
        return $this->belongsTo(Store::class, 'store_id', 'id');
    }
}
