<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Role extends Model
{
    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'roles';

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'description'
    ];

    /**
     * @var array
     */
    protected $hidden = [
        'pivot'
    ];

    /**
     * @return BelongsToMany
     */
    public function scopes()
    {
        return $this->belongsToMany(
            Scope::class,
            'roles_scopes',
            'roles_id',
            'scopes_id'
        );
    }
}
