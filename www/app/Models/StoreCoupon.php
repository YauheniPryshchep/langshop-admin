<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class StoreCoupon extends Model
{
    /**
     * @var string
     */
    protected $connection = 'langshop';

    /**
     * @var string
     */
    protected $table = 'stores_coupons';

    /**
     * @var bool
     */
    public $timestamps = false;

    public $hidden = [
        'store_id',
        'coupon_id'
    ];

    /**
     * @return BelongsTo
     */
    public function store()
    {
        return $this->belongsTo(Store::class, 'store_id');
    }
}
