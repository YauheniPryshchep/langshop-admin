<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PartnerToken extends Model
{

    /**
     * @var string
     */
    protected $table = 'partners_token';

    /**
     * @var bool
     */
    public $timestamps = false;

    protected $fillable = [
        'token',
        'user_id',
        'locked',
        'timestamp'
    ];

}
