<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 *      schema="CouponModel",
 *      type="object",
 *      @OA\Property(property="name", type="string"),
 *      @OA\Property(property="status", type="integer" , enum={1,2,3}),
 *      @OA\Property(property="discount_type", type="integer", enum={1,2}),
 *      @OA\Property(property="discount_value", type="number"),
 *      @OA\Property(property="usage_limit", type="integer"),
 *      @OA\Property(property="reusable", type="boolean"),
 *      @OA\Property(property="start_at", type="string", pattern="\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}", nullable="true"),
 *      @OA\Property(property="end_at", type="string", pattern="\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}", nullable="true", readOnly="true"),
 *      @OA\Property(property="created_at", type="string", pattern="\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}", readOnly="true"),
 *      @OA\Property(property="updated_at", type="string", pattern="\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}", readOnly="true"),
 * )
 */
class Coupon extends Model
{
    const STATUS_DRAFT = 1;
    const STATUS_ACTIVE = 2;
    const STATUS_BLOCKED = 3;

    const TYPE_FIXED = 1;
    const TYPE_PERCENT = 2;
    /**
     * @var string
     */
    protected $connection = 'langshop';

    /**
     * @var string
     */
    protected $table = 'coupons';

    /**
     * @var array
     */
    protected $casts = [
        'reusable' => 'boolean',
        'start_at' => 'datetime',
        'end_at'   => 'datetime',
        'plans'    => 'array'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'status',
        'discount_value',
        'discount_type',
        'usage_limit',
        'reusable',
        'start_at',
        'end_at',
        'plans'
    ];
}
