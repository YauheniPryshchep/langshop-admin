<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CampaignUnsubscribeList extends Model
{
    /**
     * @var string
     */
    protected $table = 'campaigns_unsubscribe_list';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'domain',
        'status'
    ];

    public $timestamps = false;

}

