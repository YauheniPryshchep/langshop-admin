<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class StoreHistory extends Model
{

    const TYPE_STORE_INSTALLED = "relationship-installed";
    const TYPE_STORE_UNINSTALLED = "relationship-uninstalled";
    const TYPE_STORE_REOPENED = 'relationship-reactivated';
    const TYPE_STORE_CLOSED = 'relationship-deactivated';

    /**
     * @var string
     */
    protected $connection = 'langshop';

    /**
     * @var string
     */
    protected $table = 'store_history';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $hidden = [
        'store_id'
    ];

    /**
     * @return BelongsTo
     */
    public function store()
    {
        return $this->belongsTo(Store::class, 'store_id', 'id');
    }
}
