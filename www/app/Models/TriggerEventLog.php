<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * @OA\Schema(
 *      schema="ActionModel",
 *      type="object",
 *      @OA\Property(property="action_slug", type="string"),
 *      @OA\Property(property="action_data", type="string"),
 * )
 */
class TriggerEventLog extends Model
{

    const STATUS_WAITING_IN_START = 1;
    const STATUS_WAITING_DELAYED_CONDITION = 2;
    const STATUS_PROCESSING = 3;
    const STATUS_DONT_HAS_WORKFLOW = 4;
    const STATUS_DONE = 5;
    const STATUS_FAILED = 6;

    /**
     * @var string
     */
    protected $table = 'trigger_event_logs';

    /**
     * @var array
     */
    protected $fillable = [
        'event_slug',
        'event_data',
        'workflow_id',
        'status',
        'check_at'
    ];

    /**
     * @var array
     */
    protected $hidden = [
        'workflowData',
        'workflow_id',
    ];

    /**
     * @var array
     */
    protected $appends = [
        'workflow'
    ];

    protected $casts = [
        'event_data' => "array"
    ];

    protected $attributes = [
        "status" => self::STATUS_WAITING_IN_START
    ];

    /**
     * @return HasOne
     */
    public function workflowData()
    {
        return $this->hasOne(
            Workflow::class,
            'id',
            'workflow_id'
        );
    }

    /**
     * @return array
     */
    public function getWorkflowAttribute()
    {
        return $this->workflowData;
    }
}

