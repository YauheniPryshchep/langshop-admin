<?php

namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOneThrough;
use Illuminate\Support\Facades\DB;
use Throwable;
use Illuminate\Database\Eloquent\Model;


class CampaignProcess extends Model
{
    const STATUS_PENDING = 1;
    const STATUS_DISPATCHING = 2;
    const STATUS_FAILED = 3;
    const STATUS_PROCESSED = 4;
    const STATUS_SKIPPED = 5;

    /**
     * @var string
     */
    protected $table = 'campaigns_processes';

    /**
     * @var array
     */
    protected $casts = [
        'data'   => 'array',
        'result' => 'array',
    ];

    /**
     * @var array
     */
    protected $attributes = [
        'result' => null
    ];

    protected $fillable = [
        'campaign_id',
        'processor',
        'status',
        'parent_id',
        'data'
    ];

    /**
     * @return BelongsTo
     */
    public function campaign()
    {
        return $this->belongsTo(Campaign::class, 'campaign_id', 'id');
    }

    public function children()
    {
        return CampaignProcess::query()
            ->where('parent_id', $this->id)
            ->get();
    }

}