<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 *      schema="ActionModel",
 *      type="object",
 *      @OA\Property(property="action_slug", type="string"),
 *      @OA\Property(property="action_data", type="string"),
 * )
 */
class WorkflowAction extends Model
{

    const CUSTOM_EMAIL_SEND_ACTION = 'send_custom_email';
    const EMAIL_SEND_ACTION = 'send_email';

    /**
     * @var string
     */
    protected $table = 'workflows_actions';

    /**
     * @var array
     */
    protected $fillable = [
        'workflow_id',
        'conditions_group_id',
        'action_slug',
        'action_data'
    ];

    protected $casts = [
        'action_data' => "array"
    ];
}
