<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
    const WITHOUT_PLAN = 0;
    const PLAN_FREE = 1;
    const PLAN_STANDARD = 2;
    const PLAN_ADVANCED = 3;
    const PLAN_ENTERPRISE = 4;

    const WITHOUT_PLAN_SLUG = "null";
    const PLAN_FREE_SLUG = "free";
    const PLAN_STANDARD_SLUG = "standard";
    const PLAN_ADVANCED_SLUG = "advanced";
    const PLAN_ENTERPRISE_SLUG = "enterprise";

    const PLAN_SLUG = [
        self::WITHOUT_PLAN    => self::WITHOUT_PLAN_SLUG,
        self::PLAN_FREE       => self::PLAN_FREE_SLUG,
        self::PLAN_STANDARD   => self::PLAN_STANDARD_SLUG,
        self::PLAN_ADVANCED   => self::PLAN_ADVANCED_SLUG,
        self::PLAN_ENTERPRISE => self::PLAN_ENTERPRISE_SLUG,
    ];

    protected $connection = 'langshop';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'plans';

    /**
     * @param int $id
     * @return bool
     */
    public function isPlan(int $id): bool
    {
        return $this->id === $id;
    }

    /**
     * @return bool
     */
    public function isFree(): bool
    {
        return $this->isPlan(self::PLAN_FREE);
    }

    /**
     * @param int $planId
     * @return mixed|string
     */
    public static function getPlanSlug(int $planId)
    {
        return self::PLAN_SLUG[$planId] ?? "";
    }
}