<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class WorkflowLogs extends Model
{

    const STATUS_PENDING = 1;
    const STATUS_PROCESSING = 2;
    const STATUS_SATISFIED = 3;
    const STATUS_NOT_SATISFIED = 4;
    /**
     * @var string
     */
    protected $table = 'workflow_logs';

    /**
     * @var array
     */
    protected $hidden = [
        'workflowConditions',
        'workflowData',
        'workflow_id',
        'condition_group_id'
    ];

    /**
     * @var array
     */
    protected $appends = [
        'conditions',
        'workflow'
    ];

    /**
     * @var array
     */
    protected $fillable = [
        "trigger_id",
        "workflow_id",
        'triggered_at',
        'condition_group_id',
        "check_at",
        'status'
    ];

    protected $attributes = [
        "status" => self::STATUS_PENDING
    ];

    /**
     * @return HasMany
     */
    public function workflowConditions()
    {
        return $this->hasMany(
            WorkflowConditionGroup::class,
            'id',
            'condition_group_id'
        );
    }

    /**
     * @return HasOne
     */
    public function workflowData()
    {
        return $this->hasOne(
            Workflow::class,
            'id',
            'workflow_id'
        );
    }


    /**
     * @return HasOne
     */
    public function store()
    {
        return $this->hasOne(
            Store::class,
            'id',
            'store_id'
        );
    }

    /**
     * @return array
     */
    public function getConditionsAttribute()
    {
        return $this->workflowConditions;
    }

    /**
     * @return array
     */
    public function getWorkflowAttribute()
    {
        return $this->workflowData;
    }


}
