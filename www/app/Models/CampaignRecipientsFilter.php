<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 *      schema="CampaignRecipientsFilterModel",
 *      type="object",
 *      @OA\Property(property="campaign_id", type="integer"),
 *      @OA\Property(property="page", type="integer"),
 *      @OA\Property(property="limit", type="integer"),
 *      @OA\Property(property="filters", type="array",
 *          @OA\Items(
 *              @OA\Property(property="key", type="string"),
 *              @OA\Property(property="value", type="string"),
 *              @OA\Property(property="comparator", type="string"),
 *          )
 *      ),
 * )
 */
class CampaignRecipientsFilter extends Model
{
    /**
     * @var string
     */
    protected $table = 'campaign_recipients_filters';

    /**
     * @var boolean
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'campaign_id',
        'limit',
        'filters',
    ];

    /**
     * @var array
     */
    protected $attributes = [
        'filters' => "[]",
        'limit'   => null
    ];

    /**
     * @var array
     */
    protected $casts = [
        'filters' => 'array'
    ];

    /**
     * @var array
     */
    protected $hidden = [
        'campaign_id',
        'id'
    ];

}

