<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 *      schema="CampaignDomainModel",
 *      type="object",
 *      @OA\Property(property="campaign_id", type="integer"),
 *      @OA\Property(property="domain", type="array", @OA\Items(
 *          type="string"
 *      ))
 * )
 */
class CampaignDomain extends Model
{

    /**
     * @var string
     */
    protected $table = 'campaign_domains';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'campaign_id',
        'domain',
    ];

    public function campaigns()
    {
        return $this->hasOne(Campaign::class, 'id', 'campaign_id');
    }

}
