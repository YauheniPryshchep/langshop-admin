<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 *      schema="StoreEventsModel",
 *      type="object",
 *      @OA\Property(property="domain", type="string"),
 *      @OA\Property(property="type", type="integer"),
 *      @OA\Property(property="timestamp", type="integer"),
 *      @OA\Property(property="event_data", type="object", nullable="true"),
 * )
 */
class StoreEvents extends Model
{
    const STORE_ADD_TO_CAMPAIGN = 1;
    const UNSUBSCRIBED_FROM_MAILING = 2;
    const ONE_TIME_PAYMENT_CHARGE = 3;
    const ASSIGN_REFERRAL = 4;
    const CANCEL_REFERRAL = 5;
    const DECLINED_REFERRAL = 6;
    const CHANGE_PLAN = 7;
    const CHANGE_ENTERPRISE_PRICE = 8;

    /**
     * @var string
     */
    protected $table = 'stores_events';

    /**
     * @var array
     */
    protected $fillable = [
        'domain',
        'type',
        'event_data',
        'timestamp'
    ];

    protected $casts = [
        'event_data' => 'array',
    ];


    /**
     * @var bool
     */
    public $timestamps = false;
}
