<?php

namespace App\Collections;

use function count;

trait AbstractCollectionCountable
{
    /**
     * {@inheritdoc}
     */
    public function count()
    {
        return count($this->collection);
    }
}
