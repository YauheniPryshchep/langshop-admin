<?php

namespace App\Collections;

use App\Factories\Interfaces\ObjectFactoryInterface;

class ObjectsCollection extends AbstractCollection
{
    /**
     * @param ObjectFactoryInterface $itemFactory
     * @param array $nodes
     * @return ObjectsCollection
     */
    public static function create(ObjectFactoryInterface $itemFactory, array $nodes = [])
    {
        $collection = new ObjectsCollection();

        foreach ($nodes as $node) {
            $collection->addOne($itemFactory->object($node));
        }

        return $collection;
    }
}
