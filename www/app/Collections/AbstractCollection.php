<?php

namespace App\Collections;

use ArrayAccess;
use Countable;
use Iterator;
use JsonSerializable;

abstract class AbstractCollection implements
    Countable,
    Iterator,
    ArrayAccess,
    JsonSerializable,
    AbstractCollectionInterface
{
    use AbstractCollectionCountable;
    use AbstractCollectionArrayAccess;
    use AbstractCollectionSerializable;
    use AbstractCollectionIterator;

    /**
     * @var AbstractEntity[]
     */
    protected $collection = [];

    /**
     * AbstractCollection constructor.
     * @param AbstractEntity[] $collection
     */
    public function __construct(array $collection = [])
    {
        $this->collection = $collection;
    }

    /**
     * @return AbstractEntity[]
     */
    public function getItems()
    {
        return $this->collection;
    }

    /**
     * @param AbstractEntity $entry
     * @return static
     */
    public function addOne(AbstractEntity $entry)
    {
        $this->collection[] = $entry;

        return $this;
    }

    /**
     * @param array $entries
     * @return static
     */
    public function addMany(array $entries)
    {
        foreach ($entries as $entry) {
            $this->addOne($entry);
        }

        return $this;
    }

    /**
     * Execute a callback over each item.
     *
     * @param callable|array|string $callback
     * @return static
     */
    public function each($callback)
    {
        foreach ($this->collection as $key => $item) {
            if (call_user_func($callback, $item, $key) === false) {
                break;
            }
        }

        return $this;
    }

    /**
     * Execute a callback over each item.
     *
     * @param callable|array|string $callback
     * @return static
     */
    public function map($callback)
    {
        $this->collection = array_map(function ($item) use ($callback) {
            return call_user_func($callback, $item);
        }, $this->collection);

        return $this;
    }

    /**
     * @param callable|array|string $callback
     * @return static
     */
    public function filter($callback)
    {
        $abstractCollection = new static();

        foreach ($this->collection as $key => $item) {
            if (call_user_func($callback, $item, $key) === false) {
                continue;
            }

            $abstractCollection->addOne($item);
        }

        return $abstractCollection;
    }

    /**
     * Sort the collection using the given callback.
     *
     * @param  callable|string|array  $callback
     * @param  int  $options
     * @param  bool  $descending
     * @return static
     */
    public function sortBy($callback, $options = SORT_REGULAR, $descending = false)
    {
        $results = [];

        // First we will loop through the items and get the comparator from a callback
        // function which we were given. Then, we will sort the returned values and
        // and grab the corresponding values for the sorted keys from this array.
        foreach ($this->collection as $key => $value) {
            $results[$key] = call_user_func($callback, $value, $key);
        }

        $descending ? arsort($results, $options)
            : asort($results, $options);

        // Once we have sorted all of the keys in the array, we will loop through them
        // and grab the corresponding model so we can set the underlying items list
        // to the sorted version. Then we'll just return the collection instance.
        foreach (array_keys($results) as $key) {
            $results[$key] = $this->collection[$key];
        }

        return new static($results);
    }

    /**
     * @param string $key
     * @param mixed $value
     * @return AbstractEntity[]
     */
    public function find(string $key, $value): array
    {
        $objects = [];

        foreach ($this->collection as $item) {
            if ($item->$key != $value) {
                continue;
            }

            $objects[] = $item;
        }

        return $objects;
    }

    /**
     * @param string $key
     * @param mixed $value
     * @return AbstractEntity|null
     */
    public function first(string $key, $value): ?AbstractEntity
    {
        foreach ($this->collection as $item) {
            if ($item->$key != $value) {
                continue;
            }

            return $item;
        }

        return null;
    }

    /**
     * @param string $key
     * @return array
     */
    public function column(string $key): array
    {
        $column = [];

        foreach ($this->collection as $item) {
            $column[] = $item->$key;
        }

        return $column;
    }

    /**
     * @param string $key
     * @param mixed $value
     * @return static
     */
    public function remove(string $key, $value)
    {
        foreach ($this->collection as $index => $item) {
            if ($item->$key != $value) {
                continue;
            }

            $this->splice($index);
        }

        return $this;
    }

    /**
     * @param int $offset
     * @param int $length
     * @return static
     */
    public function splice(int $offset, int $length = 1)
    {
        array_splice($this->collection, $offset, $length);

        return $this;
    }

    /**
     * @param AbstractCollection $abstractCollection
     * @return static
     */
    public function merge(AbstractCollection $abstractCollection)
    {
        $this->collection = array_merge($this->collection, $abstractCollection->getItems());

        return $this;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return $this->jsonSerialize();
    }
}
