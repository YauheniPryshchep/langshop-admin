<?php

namespace App\Collections;

use App\Factories\Interfaces\ItemFactoryInterface;

class ItemsCollection extends AbstractCollection
{
    /**
     * @param ItemFactoryInterface $itemFactory
     * @param array $edges
     * @return ItemsCollection
     */
    public static function create(ItemFactoryInterface $itemFactory, array $edges = [])
    {
        $collection = new ItemsCollection();

        foreach ($edges as $edge) {
            $collection->addOne($itemFactory->item($edge));
        }

        return $collection;
    }
}
