<?php

namespace App\Collections;

use JsonSerializable;

trait AbstractCollectionSerializable
{
    /**
     * {@inheritdoc}
     */
    public function jsonSerialize()
    {
        $items = [];

        foreach ($this->collection as $entry) {
            if ($entry instanceof JsonSerializable) {
                $items[] = $entry->jsonSerialize();
            } else {
                $items[] = $entry;
            }
        }

        return $items;
    }
}
