<?php

namespace App\Collections;

interface AbstractCollectionInterface
{
    /**
     * Add one word at a time
     *
     * @param AbstractEntity $entry
     */
    public function addOne(AbstractEntity $entry);

    /**
     * Add several words at once
     *
     * @param AbstractEntity[] $entries
     */
    public function addMany(array $entries);
}
