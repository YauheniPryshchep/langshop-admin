<?php

namespace App\Collections;

use JsonSerializable;

abstract class AbstractEntity implements JsonSerializable
{
    /**
     * @var string[]
     */
    private $autoAssigned = [];

    /**
     * @var string[]
     */
    protected $hiddenKeys = [
        'cursor'
    ];

    /**
     * AbstractEntity constructor.
     * @param array $properties
     */
    public function __construct(array $properties = [])
    {
        $this->assignProperties($properties);
    }

    /**
     * @param array $properties
     * @return static
     */
    public function assignProperties(array $properties = [])
    {
        foreach ($properties as $key => $value) {
            $setter = 'set' . ucwords($key);

            if (!method_exists($this, $setter)) {
                continue;
            }

            $this->autoAssigned[] = $key;

            call_user_func_array([$this, $setter], [$value]);
        }

        return $this;
    }

    /**
     * @return string[]
     */
    public function getHiddenKeys(): array
    {
        return $this->hiddenKeys;
    }

    /**
     * @return string[]
     */
    public function getAutoAssigned(): array
    {
        return $this->autoAssigned;
    }

    /**
     * Convert the object into something JSON serializable.
     *
     * @return array
     */
    public function jsonSerialize()
    {
        $array = get_object_vars($this);

        $hidden = array_merge(
            $this->getHiddenKeys(),
            [
                'autoAssigned',
                'hiddenKeys'
            ]
        );

        foreach ($hidden as $key) {
            if (!array_key_exists($key, $array)) {
                continue;
            }

            unset($array[$key]);
        }

        return $array;
    }

    /**
     * Convert the object into array.
     *
     * @return array
     */
    public function toArray(): array
    {
        return $this->jsonSerialize();
    }
}
