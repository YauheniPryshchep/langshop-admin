<?php

namespace App\Objects;

use JsonSerializable;

class RecipientsFilter implements JsonSerializable
{

    /**
     * @var int
     */
    public $campaignId;

    /**
     * @var int
     */
    public $currentPage;

    /**
     * @var int
     */
    public $limit;

    /**
     * @var array
     */
    public $items;

    public function __construct(array $params)
    {
        $this->filters = $params['filters'];
        $this->currentPage = $params['page'];
        $this->campaignId = $params['campaign_id'];
        $this->limit = $params['limit'];
    }

    public function jsonSerialize()
    {
        return [
            'filters' => $this->filters,
            'campaign_id' => $this->campaignId,
            'page' => $this->currentPage,
            'limit' => $this->limit,
        ];
    }

}
