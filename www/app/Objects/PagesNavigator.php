<?php

namespace App\Objects;

use JsonSerializable;

class PagesNavigator implements JsonSerializable
{

    /**
     * @var int
     */
    public $total;

    /**
     * @var int
     */
    public $currentPage;

    /**
     * @var int
     */
    public $limit;

    /**
     * @var array
     */
    public $items;

    public function __construct(array $items, int $currentPage, int $total, int $limit)
    {
        $this->items = $items;
        $this->currentPage = $currentPage;
        $this->total = $total;
        $this->limit = $limit;
    }

    public function jsonSerialize()
    {
        return [
            'items' => $this->items,
            'total' => $this->total,
            'page' => $this->currentPage,
            'limit' => $this->limit,
        ];
    }

}
