<?php

namespace App\Objects;

class SimplePaginationSortObject
{
    /**
     * @var array
     */
    protected $allowedDirections = ['asc', 'desc'];

    /**
     * @var string
     */
    public $field = '';

    /**
     * @var string
     */
    public $direction = 'asc';

    /**
     * SimplePaginationSortObject constructor.
     * @param string $field
     * @param string $direction
     */
    public function __construct($field = '', $direction = 'asc')
    {
        $this->field     = $field;
        $this->direction = $direction;
        $this->normalize();
    }

    /**
     * @param array $allowedFields
     */
    public function normalize($allowedFields = [])
    {
        if (!in_array($this->direction, $this->allowedDirections)) {
            $this->direction = 'asc';
        }

        if (count($allowedFields) > 0) {
            if (!in_array($this->field, $allowedFields)) {
                $this->field = $allowedFields[0];
            }
        }
    }
}