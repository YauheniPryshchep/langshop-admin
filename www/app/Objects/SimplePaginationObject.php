<?php

namespace App\Objects;

use Illuminate\Http\Request;

class SimplePaginationObject
{
    /**
     * @var string
     */
    public $filter = '';

    /**
     * @var SimplePaginationSortObject
     */
    public $sort;

    /**
     * @OA\Parameter(
     *      parameter="query_page",
     *      name="page",
     *      description="Page number for pagination",
     *      @OA\Schema(
     *          type="integer"
     *      ),
     *      in="query"
     * )
     *
     * @var int
     */
    public $page = 1;

    /**
     * @OA\Parameter(
     *      parameter="query_limit",
     *      name="limit",
     *      description="Limit items per page",
     *      @OA\Schema(
     *          type="integer"
     *      ),
     *      in="query"
     * )
     *
     * @var int
     */
    public $limit = 25;

    /**
     * SimplePaginationObject constructor.
     */
    public function __construct()
    {
        $this->sort = new SimplePaginationSortObject();
    }

    /**
     * @param Request $request
     * @param array $allowedSortFields
     * @param array $limitRange
     * @param array $pageRange
     */
    public function fillFromRequest(
        Request $request,
        $allowedSortFields = [],
        $limitRange = [1, 250],
        $pageRange = [1, 999999]
    ) {
        $this->filter = trim($request->input('filter', ''));
        $this->limit  = filter_var(
            (int)$request->input('limit', 0),
            FILTER_VALIDATE_INT,
            array(
                'options' => array(
                    'min_range' => $limitRange[0],
                    'max_range' => $limitRange[1]
                )
            )
        );
        if ($this->limit === false) {
            $this->limit = 25;
        }

        $this->page = filter_var(
            (int)$request->input('page', 1),
            FILTER_VALIDATE_INT,
            array(
                'options' => array(
                    'min_range' => $pageRange[0],
                    'max_range' => $pageRange[1]
                )
            )
        );

        if ($this->page === false) {
            $this->page = 1;
        }

        if(empty($allowedSortFields)) {
            return;
        }

        $sortData = $request->input('sort', []);

        $this->sort = new SimplePaginationSortObject($allowedSortFields[0]);

        if (is_array($sortData) && count($sortData) > 0) {
            if (isset($sortData['field']) && isset($sortData['direction'])) {
                $this->sort = new SimplePaginationSortObject($sortData['field'], $sortData['direction']);
                $this->sort->normalize($allowedSortFields);
            }
        }
    }
}