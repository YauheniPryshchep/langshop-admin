<?php

namespace App\Objects;

use JsonSerializable;

class MailingListObject implements JsonSerializable
{

    /**
     *
     * @var string 
     */
    protected $access_level;

    /**
     *
     * @var string 
     */
    protected $address;

    /**
     *
     * @var string 
     */
    protected $description;

    /**
     *
     * @var string 
     */
    protected $name;

    /**
     *
     * @var string 
     */
    protected $reply_preference;

    /**
     *
     * @var integer 
     */
    protected $members_count;

    public function __construct(array $mailingList)
    {
        $this->access_level = $mailingList['access_level'];
        $this->address = $mailingList['address'];
        $this->description = $mailingList['description'];
        $this->members_count = $mailingList['members_count'];
        $this->name = $mailingList['name'];
        $this->reply_preference = $mailingList['reply_preference'];
    }

    /**
     *
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            'access_level' => $this->access_level,
            'address' => $this->address,
            'description' => $this->description,
            'members_count' => $this->members_count,
            'name' => $this->name,
            'reply_preference' => $this->reply_preference
        ];
    }

    /**
     * @return int
     */
    public function getMembersCount(): int
    {
        return $this->members_count;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getAddress(): string
    {
        return $this->address;
    }

}
