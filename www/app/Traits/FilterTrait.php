<?php

namespace App\Traits;
use Illuminate\Database\Eloquent\Builder;

trait FilterTrait
{
    /**
     * @param array $filters
     * @param Builder $query
     * @param array $relatedFilters
     * @return Builder
     */
    public function filterBy(array $filters, Builder $query, array $relatedFilters)
    {
        return $this->apply($filters, $query, $relatedFilters);
    }

    /**
     * @param array $filters
     * @param Builder $query
     * @param array $relatedFilters
     * @return Builder
     */
    private function apply(array $filters, Builder $query, array $relatedFilters)
    {
        foreach ($filters as $name => $value) {
            $class = $relatedFilters[$name];

            if (!class_exists($class)) {
                continue;
            }

            if (strlen($value)) {
                (new $class($query))->handle($value);
            } else {
                (new $class($query))->handle();
            }
        }

        return $query;
    }
}
