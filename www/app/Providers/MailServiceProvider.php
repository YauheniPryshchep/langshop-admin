<?php

namespace App\Providers;

use App\Services\MailGunService;

class MailServiceProvider
{
    /**
     * @var MailGunService
     */
    public $mailService = null;

    public function __construct()
    {
        if (is_null($this->mailService)) {
            $this->mailService = new MailGunService();
        }
    }

    public function testMessage($title, $content, $email, $from)
    {
//        $email = 'taras.developer.it@gmail.com';
        $this->mailService->sendMail(
            $from,
            $email,
            $title,
            $content
        );
    }

    public function historyComeBackMessage($title, $message, $email, $from)
    {
//        $email = 'taras.developer.it@gmail.com';
        $this->mailService->sendMail(
            $from,
            $email,
            $title,
            $message
        );

    }

    public function historyInstalledMessage($title, $message, $email, $from)
    {
//        $email = 'taras.developer.it@gmail.com';
        $this->mailService->sendMail(
            $from,
            $email,
            $title,
            $message
        );

    }
}
