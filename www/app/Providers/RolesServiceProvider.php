<?php

namespace App\Providers;

use App\Models\Role;
use App\Objects\SimplePaginationObject;
use Illuminate\Database\Eloquent\Builder;

class RolesServiceProvider
{
    public function getAll(SimplePaginationObject $pagination)
    {
        return $this->_getAll($pagination);
    }

    public function getInfo($id)
    {
        return Role::with(['scopes'])->where('id', $id)->get();
    }

    private function _getAllCount(SimplePaginationObject $pagination)
    {
        $query = Role::query();

        if (!empty($pagination->filter)) {
            $query->where(function (Builder $query) use ($pagination) {
                $query->where('name', 'like', '%' . $pagination->filter . '%')
                    ->orWhere('description', 'like', '%' . $pagination->filter . '%');
            });
        }

        return $query->count();
    }

    private function _getAll(SimplePaginationObject $pagination)
    {
        $limit         = $pagination->limit;
        $offset        = $pagination->limit * ($pagination->page - 1);
        $sortField     = (!empty($pagination->sort->field)) ? $pagination->sort->field : 'name';
        $sortDirection = $pagination->sort->direction;
        $count         = $this->_getAllCount($pagination);
        $pages         = ceil($count / $pagination->limit);

        $query = Role::with(['scopes']);

        if (!empty($pagination->filter)) {
            $query->where(function (Builder $query) use ($pagination) {
                $query->where('name', 'like', '%' . $pagination->filter . '%')
                    ->orWhere('description', 'like', '%' . $pagination->filter . '%');
            });
        }

        $query->orderBy($sortField, $sortDirection);

        $query->offset($offset)
            ->limit($limit);

        return [
            'items'      => $query->get(),
            'count'      => $count,
            'page'       => $pagination->page,
            'totalPages' => $pages
        ];

    }
}
