<?php

namespace App\Providers;

use App\Models\NewsItem;
use App\Objects\SimplePaginationObject;
use Illuminate\Database\Eloquent\Builder;

class NewsServiceProvider
{
    public function getAll(string $appId, SimplePaginationObject $pagination)
    {
        return $this->_getAll($appId, $pagination);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getInfo($id)
    {
        return NewsItem::where('id', $id)->get();
    }

    private function _getAllCount(string $appId, SimplePaginationObject $pagination)
    {
        $query = NewsItem::query();

        if (!empty($pagination->filter)) {
            $query->where(function (Builder $query) use ($pagination) {
                $query->where('title', 'like', '%' . $pagination->filter . '%')
                    ->orWhere('description', 'like', '%' . $pagination->filter . '%');
            });
        }

        $query->where('app', $appId);

        return $query->count();
    }

    private function _getAll(string $appId, SimplePaginationObject $pagination)
    {
        $limit         = $pagination->limit;
        $offset        = $pagination->limit * ($pagination->page - 1);
        $sortField     = (!empty($pagination->sort->field)) ? $pagination->sort->field : 'id';
        $sortDirection = $pagination->sort->direction;
        $count         = $this->_getAllCount($appId, $pagination);
        $pages         = ceil($count / $pagination->limit);
        $query         = NewsItem::query();

        if (!empty($pagination->filter)) {
            $query->where(function (Builder $query) use ($pagination) {
                $query->where('title', 'like', '%' . $pagination->filter . '%')
                    ->orWhere('description', 'like', '%' . $pagination->filter . '%');
            });
        }

        $query->where('app', $appId);

        if ($sortField === "position") {
            if ($sortDirection === "asc") {
                $query->orderBy("sticky", "desc");
                $query->orderBy($sortField, $sortDirection);
            } else {
                $query->orderBy("sticky", "asc");
                $query->orderBy($sortField, $sortDirection);
            }
        } else {
            $query->orderBy($sortField, $sortDirection);
        }

        $query->offset($offset)
            ->limit($limit);

        return [
            'count'      => $count,
            'items'      => $query->get(),
            'page'       => $pagination->page,
            'totalPages' => $pages
        ];
    }
}
