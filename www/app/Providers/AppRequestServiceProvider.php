<?php

namespace App\Providers;

use App\Exceptions\Http\BadRequestError;
use App\Exceptions\Http\InternalError;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Exception\GuzzleException;

class AppRequestServiceProvider
{
    /**
     * @var string
     */
    public $domain;

    /**
     * @var string
     */
    protected $apiKey;

    /**
     * @var Client
     */
    protected $httpClient;

    /**
     * AppRequestServiceProvider constructor.
     */
    public function __construct()
    {
        $this->domain = getenv('LANGSHOP_APP_URL') . '/system';
        $this->apiKey = getenv('LANGSHOP_APP_KEY');
        $this->httpClient = new Client();

    }

    public function changePlan($store, $data = [])
    {
        return $this->post('/change_plan', $store, $data);
    }

    public function charge($shop, $data)
    {
        return $this->post('/charge', $shop, $data);
    }

    public function get($uri, $shop)
    {
        return $this->call($uri, $shop, 'GET');
    }

    public function post($uri, $shop, $data = [])
    {

        return $this->call($uri, $shop, 'POST', $data);
    }

    public function put($uri, $shop, $data = [])
    {
        return $this->call($uri, $shop, 'PUT', $data);
    }

    public function delete($uri, $shop)
    {
        return $this->call($uri, $shop, 'DELETE');
    }

    private function call($uri, $shop, $type = 'GET', $data = [])
    {
        try {
            $request_headers = [
                "X-Shopify-Hmac-Sha256" => $this->apiKey,
                "X-Shopify-Shop-Domain" => $shop,
                "Accept"                => "application/json"

            ];

            $options = [
                "headers" => $request_headers
            ];

            if ($type == 'POST' || $type == 'PUT') {
                $options['form_params'] = $data;
            }
            $response = $this->httpClient->request($type, $this->domain . $uri, $options);

            return (string)$response->getBody();

        } catch (BadResponseException $exception) {
            $response = json_decode($exception->getResponse()->getBody()->getContents());
            throw new InternalError(
                $response->internal,
                $response->message,
                $response->errors,
                $response->code
            );

        } catch (GuzzleException $e) {
            return false;
        }
    }

}
