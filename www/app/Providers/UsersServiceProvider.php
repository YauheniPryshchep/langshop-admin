<?php

namespace App\Providers;

use App\Models\User;
use App\Objects\SimplePaginationObject;
use Illuminate\Database\Eloquent\Builder;

class UsersServiceProvider
{
    public function getAll(SimplePaginationObject $pagination, bool $unassigned = false)
    {
        return $this->_getAll($pagination, $unassigned);
    }

    public function getInfo(int $id)
    {
        return User::with(['roles', 'scopes'])
            ->where('id', $id)
            ->get();
    }

    private function _getAllCount(SimplePaginationObject $pagination)
    {
        $query = User::query();

        if (!empty($pagination->filter)) {
            $query->where(function (Builder $query) use ($pagination) {
                $query->where('name', 'like', '%' . $pagination->filter . '%')
                    ->orWhere('email', 'like', '%' . $pagination->filter . '%');
            });
        }

        return $query->count();
    }

    private function _getAll(SimplePaginationObject $pagination, bool $unassigned = false)
    {
        $limit         = $pagination->limit;
        $offset        = $pagination->limit * ($pagination->page - 1);
        $sortField     = (!empty($pagination->sort->field)) ? $pagination->sort->field : 'name';
        $sortDirection = $pagination->sort->direction;
        $count         = $this->_getAllCount($pagination);

        $pages = ceil($count / $pagination->limit);

        $query = User::query()->with(['roles']);

        if (!empty($pagination->filter)) {
            $query->where(function (Builder $query) use ($pagination) {
                $query->where('name', 'like', '%' . $pagination->filter . '%')
                    ->orWhere('email', 'like', '%' . $pagination->filter . '%');
            });
        }

        $query->orderBy($sortField, $sortDirection);

        $query->offset($offset)
            ->limit($limit);

        return [
            "count" => $count,
            "items" => $query->get(),
            "page" => $pagination->page,
            "totalPages" => $pages
        ];
    }
}
