<?php

namespace App\Providers;

use App\Extensions\KeyCloakUserProvider;
use App\Extensions\LdapUserProvider;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;
use Vizir\KeycloakWebGuard\Auth\KeycloakWebUserProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Boot the authentication services for the application.
     *
     * @return void
     */
    public function boot()
    {
        // Here you may define how you wish users to be authenticated for your Lumen
        // application. The callback which receives the incoming request instance
        // should return either a User instance or null. You're free to obtain
        // the User instance via an API token or any other method necessary.

        $this->app['auth']->viaRequest('api', function ($request) {
            if ($request->input('email')) {
                return User::where('email', $request->input('email'))->first();
            }
        });

        $this->app['auth']->provider('ldap-eloquent', function ($app, array $config) {
            return new LdapUserProvider($app['hash'], $config['model']);
        });

        $this->app['auth']->provider('keycloak-eloquent', function ($app, array $config) {
            return new KeyCloakUserProvider($app['hash'], $config['model']);
        });

    }
}
