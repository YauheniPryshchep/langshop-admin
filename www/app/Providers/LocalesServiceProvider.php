<?php

namespace App\Providers;

use App\Exceptions\CustomException;
use App\Services\AppsLocales\LocalesService;
use Illuminate\Contracts\Filesystem\FileNotFoundException;

class LocalesServiceProvider
{
    /**
     * @var LocalesService
     */
    private $service;

    /**
     * LocalesServiceProvider constructor.
     * @throws CustomException
     */
    public function __construct()
    {
        $this->service = new LocalesService();
    }

    /**
     * @return array
     * @throws FileNotFoundException
     */
    public function getAll()
    {
        $items = $this->service->getLocales();
        return [
            'items' => $items,
            'total' => count($items)
        ];
    }

    /**
     * @param string $locale
     * @return array
     * @throws FileNotFoundException
     */
    public function getLocaleFiles(string $locale)
    {
        return [
            'items'  => $this->service->getLocaleFiles($locale),
            'total'  => $this->service->getTotalLocaleFiles(),
            'locale' => locale_get_display_language($locale)
        ];
    }

    /**
     * @param string $locale
     * @param string $file
     * @return array
     * @throws FileNotFoundException
     */
    public function getFile(string $locale, string $file)
    {
        return [
            'items'    => $this->service->getFile($locale, $file),
            'language' => locale_get_display_language($locale)
        ];
    }

    /**
     * @param string $locale
     * @param string $file
     * @param array $items
     * @return array
     * @throws CustomException
     * @throws FileNotFoundException
     */
    public function updateFile(string $locale, string $file, array $items)
    {
        return [
            'items'    => $this->service->updateFile($locale, $file, $items),
            'language' => locale_get_display_language($locale)
        ];
    }
}