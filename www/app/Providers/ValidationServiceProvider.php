<?php

namespace App\Providers;

use App\Validations\MailGunTemplate;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class ValidationServiceProvider extends ServiceProvider
{
    /**
     * @return void
     */
    public function boot()
    {
        Validator::extend(MailGunTemplate::RULE, MailGunTemplate::class . '@validate');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
    }
}
