<?php

namespace App\Logging;

use Illuminate\Support\Str;
use Throwable;

class LogsStackContainer
{
    /**
     * @var array
     */
    private static $stack = [];

    /**
     * @return void
     */
    public static function reset()
    {
        static::$stack = [];
    }

    /**
     * @param string $message
     * @param Throwable|null $exception
     * @param mixed $output
     */
    public static function log(string $message, Throwable $exception = null, $output = null)
    {
        static::$stack[] = [
            'message'   => $message,
            'exception' => $exception,
            'dump'      => $output
        ];
    }

    /**
     * @param string $message
     * @param mixed $output
     */
    public static function dump(string $message, $output)
    {
        static::$stack[] = [
            'message' => $message,
            'dump'    => $output
        ];
    }

    /**
     * @return array
     */
    public static function toArray(): array
    {
        return array_map(
            function ($error) {
                $row = [
                    'message' => $error['message']
                ];

                if (isset($error['dump'])) {
                    $row['dump'] = $error['dump'];
                }

                if (isset($error['exception'])) {
                    /**
                     * @var Throwable $exception
                     */
                    $exception = $error['exception'];

                    $row['exception'] = [
                        'class'   => get_class($exception),
                        'code'    => $exception->getCode(),
                        'message' => $exception->getMessage(),
                        'file'    => $exception->getFile(),
                        'line'    => $exception->getLine(),
                        'trace'   => Str::substr(
                            $exception->getTraceAsString(),
                            0,
                            2000
                        )
                    ];
                }

                return $row;
            },
            static::$stack
        );
    }
}