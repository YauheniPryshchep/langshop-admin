<?php

namespace App\Utilities;

use App\Models\StoreSubscription;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Query\Builder as QueryBuilder;

class IsTrialFilter extends QueryFilter implements FilterContract
{

    /**
     * @param $value
     */
    public function handle($value): void
    {
        $this->query->whereHas(
            'storeSubscription',
            function (Builder $q) use ($value) {
                $q->where('id', function (QueryBuilder $q) use ($value) {
                    $q->from('stores_subscriptions as sub')
                        ->selectRaw('max(id)')
                        ->whereRaw('stores_subscriptions.store_id = sub.store_id')
                        ->whereIn('sub.status', [StoreSubscription::STATUS_ACTIVE, StoreSubscription::STATUS_CANCELLED]);
                })
                    ->whereHas('charge',
                        function (Builder $q) {
                            $q->where('trial_ends_on', ">", Carbon::now());
                        }
                    );
            }
        );

    }
}
