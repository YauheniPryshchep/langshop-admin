<?php

namespace App\Utilities;

class UsageDiscountFilter extends QueryFilter implements FilterContract
{


    /**
     * @param $value
     */
    public function handle($value): void
    {
        $this->query
            ->whereHas(
                'storeDiscount'
            );
    }
}
