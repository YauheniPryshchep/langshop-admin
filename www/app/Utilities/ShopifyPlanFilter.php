<?php

namespace App\Utilities;

use Illuminate\Database\Eloquent\Builder;

class ShopifyPlanFilter extends QueryFilter implements FilterContract
{
    /**
     * @param $value
     */
    public function handle($value): void
    {
        if ($value === 'all') {
            return;
        }

        $this->query->whereHas(
            'storeData',
            function (Builder $q) use ($value) {
                $q
                    ->where('key', 'plan_display_name')
                    ->where('value', $value);
            }
        );
    }
}
