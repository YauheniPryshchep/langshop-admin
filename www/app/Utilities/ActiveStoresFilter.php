<?php

namespace App\Utilities;

class ActiveStoresFilter extends QueryFilter implements FilterContract
{

    const ACTIVE_STATUS = [
        "installed"   => true,
        "uninstalled" => false,
    ];

    /**
     * @param $value
     */
    public function handle($value): void
    {
        if (!array_key_exists($value, self::ACTIVE_STATUS)) {
            return;
        }

        $this->query
            ->where('active', self::ACTIVE_STATUS[$value]);

    }
}
