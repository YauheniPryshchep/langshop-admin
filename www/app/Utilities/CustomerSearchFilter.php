<?php

namespace App\Utilities;

use Illuminate\Database\Eloquent\Builder;

class CustomerSearchFilter extends QueryFilter implements FilterContract
{
    /**
     * @param $value
     */
    public function handle($value): void
    {
        $this->query->where('email', 'like', '%' . $value . '%')
            ->orWhereRaw("CONCAT(users.first_name, ' ', users.last_name) like '%" . $value . " %'")
            ->orWhereRaw("CONCAT(users.last_name, ' ', users.first_name) like '%" . $value . " %'");
    }
}
