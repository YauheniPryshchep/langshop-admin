<?php

namespace App\Utilities;

use Illuminate\Database\Eloquent\Builder;

class LangShopVersionFilter extends QueryFilter implements FilterContract
{

    /**
     * @param $value
     */
    public function handle($value): void
    {
        if (!$value) {
            return;
        }

        $this->query
            ->where('app', $value);

    }
}
