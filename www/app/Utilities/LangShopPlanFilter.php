<?php

namespace App\Utilities;

use App\Models\StoreSubscription;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Query\Builder as QueryBuilder;

class LangShopPlanFilter extends QueryFilter implements FilterContract
{

    const LANGSHOP_PLANS = [
        'without_plan'    => null,
        'free_plan'       => 1,
        'standard_plan'   => 2,
        "advanced_plan"   => 3,
        "enterprise_plan" => 4
    ];

    /**
     * @param $value
     */
    public function handle($value): void
    {
        if (!array_key_exists($value, self::LANGSHOP_PLANS)) {
            return;
        }

        if ($value === 'without_plan') {
            $this->query->whereDoesntHave(
                'storeSubscription',
                function (Builder $q) {
                    $q->where('id', function (QueryBuilder $q) {
                        $q->from('stores_subscriptions as sub')
                            ->selectRaw('max(id)')
                            ->whereRaw('stores_subscriptions.store_id = sub.store_id')
                            ->whereIn('sub.status', [StoreSubscription::STATUS_ACTIVE, StoreSubscription::STATUS_CANCELLED]);
                    });
                }
            );
        } else {
            $this->query->whereHas(
                'storeSubscription',
                function (Builder $q) use ($value) {
                    $q->where('id', function (QueryBuilder $q) use ($value) {
                        $q->from('stores_subscriptions as sub')
                            ->selectRaw('max(id)')
                            ->whereRaw('stores_subscriptions.store_id = sub.store_id')
                            ->whereIn('sub.status', [StoreSubscription::STATUS_ACTIVE, StoreSubscription::STATUS_CANCELLED]);
                    })->where('plan_id', self::LANGSHOP_PLANS[$value]);
                }
            );
        }
    }
}
