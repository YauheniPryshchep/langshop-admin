<?php

namespace App\Utilities;

use Illuminate\Database\Eloquent\Builder;

class CouponSearch extends QueryFilter implements FilterContract
{
    /**
     * @param $value
     */
    public function handle($value): void
    {
        $this->query
            ->where("name", 'like', '%' . $value . '%');
    }
}
