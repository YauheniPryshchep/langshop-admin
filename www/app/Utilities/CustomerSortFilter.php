<?php

namespace App\Utilities;

use Illuminate\Database\Query\JoinClause;
use Illuminate\Support\Facades\DB;

class CustomerSortFilter extends QueryFilter implements FilterContract
{

    const SORT_OPTIONS = [
        'first-name-asc'  => [
            'field' => "first_name",
            'value' => "asc"
        ],
        'first-name-desc' => [
            'field' => "first_name",
            'value' => "desc"
        ],
    ];

    /**
     * @param $value
     */
    public function handle($value): void
    {
        $option = $this->getSortOption($value);
        $this->query->orderBy($option['field'], $option['value']);
    }


    /**
     * @param string $value
     * @return mixed|null
     */
    public function getSortOption(string $value)
    {
        if (!self::SORT_OPTIONS[$value]) {
            return null;
        }

        return self::SORT_OPTIONS[$value];
    }
}
