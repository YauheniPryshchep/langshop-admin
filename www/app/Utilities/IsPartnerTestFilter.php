<?php

namespace App\Utilities;

use App\Models\Store;
use Illuminate\Database\Eloquent\Builder;

class IsPartnerTestFilter extends QueryFilter implements FilterContract
{


    /**
     * @param $value
     */
    public function handle($value): void
    {
        $this->query->whereHas(
            'storeData',
            function (Builder $q) use ($value) {
                $q
                    ->where('key', 'plan_display_name')
                    ->whereIn('value', [
                        Store::SHOPIFY_AFFILIATE,
                        Store::SHOPIFY_STUFF,
                        Store::SHOPIFY_PLUS_PARTNER_SANDBOX,
                        Store::SHOPIFY_DEVELOPER_PREVIEW]);
            }
        );
    }
}
