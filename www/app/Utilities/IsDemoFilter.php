<?php

namespace App\Utilities;

class IsDemoFilter extends QueryFilter implements FilterContract
{


    /**
     * @param $value
     */
    public function handle($value): void
    {
        $this->query
            ->whereHas(
                'storeDemo'
            );
    }
}
