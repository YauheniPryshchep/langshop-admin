<?php

namespace App\Utilities;

use Illuminate\Database\Query\JoinClause;
use Illuminate\Support\Facades\DB;

class StoreSortFilter extends QueryFilter implements FilterContract
{

    const SORT_OPTIONS = [
        'last-event-asc'  => [
            'field' => "timestamp",
            'value' => "asc"
        ],
        'last-event-desc' => [
            'field' => "timestamp",
            'value' => "desc"
        ],
        'domain-asc'      => [
            'field' => "name",
            'value' => "asc"
        ],
        'domain-desc'     => [
            'field' => "name",
            'value' => "desc"
        ],
    ];

    /**
     * @param $value
     */
    public function handle($value): void
    {
        $option = $this->getSortOption($value);

        if (in_array($value, ['last-event-asc', 'last-event-desc'])) {
            $sub = DB::table('store_history')
                ->select('store_id', DB::raw('MAX(`store_history`.`timestamp`) as timestamp'))
                ->groupBy('store_id');

            $this->query
                ->select('stores.*')
                ->leftJoinSub($sub, 'last_event', function (JoinClause $join) {
                    $join->on('stores.id', '=', 'last_event.store_id');
                })
                ->orderBy('timestamp', $option['value']);
        } else {
            $this->query->orderBy($option['field'], $option['value']);
        }
    }


    /**
     * @param string $value
     * @return mixed|null
     */
    public function getSortOption(string $value)
    {
        if (!self::SORT_OPTIONS[$value]) {
            return null;
        }

        return self::SORT_OPTIONS[$value];
    }
}
