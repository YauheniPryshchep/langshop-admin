<?php

namespace App\Utilities;

class UsageCouponFilter extends QueryFilter implements FilterContract
{


    /**
     * @param $value
     */
    public function handle($value): void
    {
        $this->query
            ->whereHas(
                'storeCoupon'
            );
    }
}
