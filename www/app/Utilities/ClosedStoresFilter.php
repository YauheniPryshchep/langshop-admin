<?php

namespace App\Utilities;

class ClosedStoresFilter extends QueryFilter implements FilterContract
{

    const CLOSED_STATUS = [
        "closed" => true,
        "open"   => false,
    ];

    /**
     * @param $value
     */
    public function handle($value): void
    {
        if (!array_key_exists($value, self::CLOSED_STATUS)) {
            return;
        }

        $this->query
            ->where('closed', self::CLOSED_STATUS[$value]);

    }
}
