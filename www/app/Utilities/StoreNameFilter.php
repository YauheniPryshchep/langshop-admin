<?php

namespace App\Utilities;

use Illuminate\Database\Eloquent\Builder;

class StoreNameFilter extends QueryFilter implements FilterContract
{
    /**
     * @param $value
     */
    public function handle($value): void
    {
        $this->query->whereHas('storeData',
            function (Builder $q) use ($value) {
                $q
                    ->where(function (Builder $q) use ($value) {
                        $q->where(function (Builder $q) use ($value) {
                            $q->where('key', 'name');
                            $q->where('value', 'like', '%' . $value . '%');
                        })
                            ->orWhere(function (Builder $q) use ($value) {
                                $q->where('key', 'domain');
                                $q->where('value', 'like', '%' . $value . '%');
                            });
                    });
            });
    }
}
