<?php

namespace App\Utilities;

use Illuminate\Database\Query\JoinClause;
use Illuminate\Support\Facades\DB;

class CouponSort extends QueryFilter implements FilterContract
{

    const SORT_OPTIONS = [
        'created-asc'  => [
            'field' => "created_at",
            'value' => "asc"
        ],
        'created-desc' => [
            'field' => "created_at",
            'value' => "desc"
        ],
        'name-asc'     => [
            'field' => "name",
            'value' => "asc"
        ],
        'name-desc'    => [
            'field' => "name",
            'value' => "desc"
        ],
    ];

    /**
     * @param $value
     */
    public function handle($value): void
    {
        $option = $this->getSortOption($value);
        $this->query->orderBy($option['field'], $option['value']);
    }


    /**
     * @param string $value
     * @return mixed|null
     */
    public function getSortOption(string $value)
    {
        if (!self::SORT_OPTIONS[$value]) {
            return null;
        }

        return self::SORT_OPTIONS[$value];
    }
}
