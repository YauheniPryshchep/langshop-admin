<?php

namespace App\Policies;

use App\Models\User;

class VocabulariesPolicy
{
    /**
     * @param User $user
     * @return bool
     */
    public function show(User $user)
    {
        return $user->haveScope('vocabularies-show');
    }

    /**
     * @param User $user
     * @return bool
     */
    public function update(User $user)
    {
        return $user->haveScope('vocabularies-update');
    }
}