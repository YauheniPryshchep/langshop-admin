<?php

namespace App\Policies;

use App\Models\User;

class StoresTrialsPolicy
{
    /**
     * @param User $user
     * @return bool
     */
    public function show(User $user)
    {
        return $user->haveScope('langshop-stores-trials-show');
    }

    /**
     * @param User $user
     * @return bool
     */
    public function update(User $user)
    {
        return $user->haveScope('langshop-stores-trials-update');
    }
}