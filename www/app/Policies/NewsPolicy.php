<?php

namespace App\Policies;

use App\Models\User;

class NewsPolicy
{
    /**
     * @param User $user
     * @return bool
     */
    public function show(User $user)
    {
        return $user->haveScope('main-news-show');
    }

    /**
     * @param User $user
     * @return bool
     */
    public function update(User $user)
    {
        return $user->haveScope('main-news-update');
    }
}