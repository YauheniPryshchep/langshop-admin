<?php

namespace App\Policies;

use App\Models\User;

class AnalyticsPolicy
{
    /**
     * @param User $user
     * @return bool
     */
    public function show(User $user)
    {
        return $user->haveScope('analytics-show');
    }

}