<?php

namespace App\Policies;

use App\Models\User;

class StoresChargePolicy
{
    /**
     * @param User $user
     * @return bool
     */
    public function show(User $user)
    {
        return $user->haveScope('langshop-stores-charges-show');
    }
}