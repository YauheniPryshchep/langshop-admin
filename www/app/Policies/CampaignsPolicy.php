<?php

namespace App\Policies;

use App\Models\User;

class CampaignsPolicy
{
    /**
     * @param User $user
     * @return bool
     */
    public function show(User $user)
    {
        return $user->haveScope('campaigns-show');
    }

    /**
     * @param User $user
     * @return bool
     */
    public function update(User $user)
    {
        return $user->haveScope('campaigns-update');
    }
}