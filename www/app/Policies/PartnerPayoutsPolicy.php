<?php

namespace App\Policies;

use App\Models\User;

class PartnerPayoutsPolicy
{
    /**
     * @param User $user
     * @return bool
     */
    public function show(User $user)
    {
        return $user->haveScope('partners-payouts-show') && $user->haveScope('financial-show');
    }

    /**
     * @param User $user
     * @return bool
     */
    public function update(User $user)
    {
        return $user->haveScope('partners-payouts-update');
    }
}