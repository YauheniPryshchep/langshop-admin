<?php

namespace App\Policies;

use App\Models\User;

class FinancialPolicy
{
    /**
     * @param User $user
     * @return bool
     */
    public function show(User $user)
    {
        return $user->haveScope('financial-show');
    }

}