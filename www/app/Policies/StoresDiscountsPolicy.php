<?php

namespace App\Policies;

use App\Models\User;

class StoresDiscountsPolicy
{
    /**
     * @param User $user
     * @return bool
     */
    public function show(User $user)
    {
        return $user->haveScope('langshop-stores-discounts-show');
    }

    /**
     * @param User $user
     * @return bool
     */
    public function update(User $user)
    {
        return $user->haveScope('langshop-stores-discounts-update');
    }
}