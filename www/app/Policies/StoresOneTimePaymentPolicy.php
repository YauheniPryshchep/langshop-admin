<?php

namespace App\Policies;

use App\Models\User;

class StoresOneTimePaymentPolicy
{
    /**
     * @param User $user
     * @return bool
     */
    public function update(User $user)
    {
        return $user->haveScope('langshop-stores-one-time-payment-update');
    }
}