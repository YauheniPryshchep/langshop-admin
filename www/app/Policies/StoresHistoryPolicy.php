<?php

namespace App\Policies;

use App\Models\User;

class StoresHistoryPolicy
{
    /**
     * @param User $user
     * @return bool
     */
    public function show(User $user)
    {
        return $user->haveScope('langshop-storeshistory-show');
    }

    /**
     * @param User $user
     * @return bool
     */
    public function update(User $user)
    {
        return $user->haveScope('langshop-storeshistory-update');
    }
}