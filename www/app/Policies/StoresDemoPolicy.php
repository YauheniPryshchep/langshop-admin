<?php

namespace App\Policies;

use App\Models\User;

class StoresDemoPolicy
{
    /**
     * @param User $user
     * @return bool
     */
    public function show(User $user)
    {
        return $user->haveScope('langshop-stores-demo-show');
    }

    /**
     * @param User $user
     * @return bool
     */
    public function update(User $user)
    {
        return $user->haveScope('langshop-stores-demo-update');
    }
}