<?php

namespace App\Policies;

use App\Models\User;

class PartnersUserPolicy
{
    /**
     * @param User $user
     * @return bool
     */
    public function show(User $user)
    {
        return $user->haveScope('partners-users-show');
    }

    /**
     * @param User $user
     * @return bool
     */
    public function showStores(User $user)
    {
        return $user->haveScope('partners-stores-show');
    }

    /**
     * @param User $user
     * @return bool
     */
    public function update(User $user)
    {
        return $user->haveScope('partners-users-update');
    }
}