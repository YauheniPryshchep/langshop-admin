<?php

namespace App\Policies;

use App\Models\User;

class CouponsPolicy
{
    /**
     * @param User $user
     * @return bool
     */
    public function show(User $user)
    {
        return $user->haveScope('coupons-show');
    }

    /**
     * @param User $user
     * @return bool
     */
    public function update(User $user)
    {
        return $user->haveScope('coupons-update');
    }
}