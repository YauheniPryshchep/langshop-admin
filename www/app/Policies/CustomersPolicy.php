<?php

namespace App\Policies;

use App\Models\User;

class CustomersPolicy
{
    /**
     * @param User $user
     * @return bool
     */
    public function show(User $user)
    {
        return $user->haveScope('customers-show');
    }
}