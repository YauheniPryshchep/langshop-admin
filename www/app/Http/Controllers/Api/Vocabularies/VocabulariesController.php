<?php

namespace App\Http\Controllers\Api\Vocabularies;

use App\Exceptions\Http\ForbiddenError;
use App\Http\Controllers\Api\ApiController;
use App\Services\VocabularyService;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class VocabulariesController extends ApiController
{
    /**
     * @var Request
     */
    protected $request;

    /**
     * @var VocabularyService
     */
    protected $service;

    /**
     * VocabulariesController constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->service = new VocabularyService();
    }

    /**
     * @OA\Get(
     *      path="/api/vocabularies",
     *      summary="Get vocabularies items",
     *      tags={"Vocabularies"},
     *      description="Get Vocabularies items",
     *      operationId="getVocabularies",
     *      security={
     *          {"JWT-Header": {}}
     *      },
     *       @OA\Parameter(
     *          name="filters",
     *          in="query",
     *          description="Available filter keys: _id, from, to, source, translated, original",
     *          @OA\Schema(type="array", @OA\Items(ref="#/components/schemas/QueryFilter"))
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\JsonContent(
     *              @OA\Property(property="items", type="array", @OA\Items(ref="#/components/schemas/VocabularyModel")),
     *              @OA\Property(property="count", type="integer"),
     *              @OA\Property(property="page", type="integer"),
     *              @OA\Property(property="totalPages", type="integer")
     *          )
     *      )
     * )
     *
     * @return JsonResponse
     */
    public function index()
    {
        if (!$this->request->user()->can('show', self::class)) {
            throw new ForbiddenError();
        }

        $vocabularies = $this->service->getItems($this->request->all());
        return $this->response($vocabularies);
    }

    /**
     * @OA\Get(
     *      path="/api/vocabularies/directions",
     *      summary="Get existing directions",
     *      tags={"Vocabularies"},
     *      description="Get existing directions",
     *      operationId="getDirections",
     *      security={
     *          {"JWT-Header": {}}
     *      },
     *
     *      @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\JsonContent(
     *              @OA\Property(property="from", type="string"),
     *              @OA\Property(property="to", type="array", @OA\Items(type="string")),
     *          )
     *      )
     * )
     *
     * @return JsonResponse
     */
    public function directions()
    {
        if (!$this->request->user()->can('show', self::class)) {
            throw new ForbiddenError();
        }

        $directions = $this->service->getDirections();
        return $this->response($directions);
    }

    /**
     * @OA\Put(
     *      path="/api/vocabularies/{vocabularyID}",
     *      summary="Update vocabulary item by ID",
     *      tags={"Vocabularies"},
     *      description="Update vocabulary item by ID",
     *      operationId="updateVocabularyById",
     *      security={
     *          {"JWT-Header": {}}
     *      },
     *      @OA\Parameter(
     *          description="ID of vocabulary item",
     *          in="path",
     *          name="vucabularyId",
     *          required=true,
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          description="Request body",
     *          @OA\JsonContent(ref="#/components/schemas/CampaignModel")
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/CampaignModel")
     *      )
     * )
     *
     * @param string $id
     * @return JsonResponse
     */
    public function update(string $id)
    {
        if (!$this->request->user()->can('update', self::class)) {
            throw new ForbiddenError();
        }

        $vocabulary = $this->service->update($this->request->all(), $id);
        return $this->response($vocabulary, __("messages.updated"));
    }

    /**
     * @OA\Post(
     *      path="/api/vocabularies",
     *      summary="Create vocaubulary item",
     *      tags={"Vocabularies"},
     *      description="Create vocaubulary item",
     *      operationId="createVocabulary item",
     *      security={
     *          {"JWT-Header": {}}
     *      },
     *      @OA\RequestBody(
     *          required=true,
     *          description="Request body",
     *          @OA\JsonContent(ref="#/components/schemas/VocabularyModel")
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/VocabularyModel")
     *      )
     * )
     *
     *
     * @return JsonResponse
     */
    public function store()
    {
        if (!$this->request->user()->can('update', self::class)) {
            throw new ForbiddenError();
        }

        $vocabulary = $this->service->store($this->request->all());
        return $this->response($vocabulary, __("messages.created"));
    }

    /**
     * @OA\Delete(
     *      path="/api/vocabularies/{vocabularyID}",
     *      summary="Delete vocabulary item by ID",
     *      tags={"Vocabularies"},
     *      description="Delete vocabulary item by ID",
     *      operationId="deleteVocabularyById",
     *      security={
     *          {"JWT-Header": {}}
     *      },
     *      @OA\Parameter(
     *          description="ID of vocabulary item",
     *          in="path",
     *          name="vucabularyId",
     *          required=true,
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="successful operation",
     *           @OA\JsonContent(
     *              @OA\Property(property="id", type="string"),
     *          )
     *      )
     * )
     *
     * @param string $id
     * @return JsonResponse
     * @throws Exception
     */
    public function delete(string $id)
    {
        if (!$this->request->user()->can('update', self::class)) {
            throw new ForbiddenError();
        }

        $this->service->delete($id);
        return $this->response(["id" => $id]);
    }

    /**
     * @OA\Patch(
     *      path="/api/directions/delete",
     *      summary="Delete vocabulary direction",
     *      tags={"Vocabularies"},
     *      description="Delete vocabulary direction",
     *      operationId="deleteVocabularyDirection",
     *      security={
     *          {"JWT-Header": {}}
     *      },
     *     @OA\RequestBody(
     *          required=true,
     *          description="Request body",
     *          @OA\JsonContent(
     *              @OA\Property(property="from", type="string"),
     *              @OA\Property(property="to", type="string"),
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="successful operation",
     *      )
     * )
     *
     * @param string $id
     * @return JsonResponse
     * @throws Exception
     */

    public function deleteDirections()
    {
        if (!$this->request->user()->can('update', self::class)) {
            throw new ForbiddenError();
        }

        $this->service->deleteDirections($this->request->all());
        return $this->response(true);
    }
}
