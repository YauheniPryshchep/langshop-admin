<?php

namespace App\Http\Controllers\Api\Customers;

use App\Exceptions\Http\ForbiddenError;
use App\Http\Controllers\Api\ApiController;
use App\Services\CustomersService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class CustomersController extends ApiController
{
    /**
     * @var Request
     */
    protected $request;

    /**
     * @var CustomersService
     */
    protected $customerService;

    /**
     * CustomersController constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->customerService = new CustomersService();
    }

    /**
     * @return JsonResponse
     */
    public function index()
    {
        if (!$this->request->user()->can('show', self::class)) {
            throw new ForbiddenError();
        }

        $customers = $this->customerService->getItems($this->request->all());
        return $this->response($customers);
    }

    /**
     * @param int $id
     * @return JsonResponse
     */
    public function show(int $id)
    {
        if (!$this->request->user()->can('show', self::class)) {
            throw new ForbiddenError();
        }

        $customer = $this->customerService->show($id);
        return $this->response($customer);
    }
}