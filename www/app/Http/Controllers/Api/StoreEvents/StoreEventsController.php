<?php

namespace App\Http\Controllers\Api\StoreEvents;

use App\Exceptions\Http\ForbiddenError;
use App\Http\Controllers\Api\ApiController;
use App\Http\Controllers\Api\Stores\StoresHistoryController;
use App\Services\StoreEventsService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class StoreEventsController extends ApiController
{
    /**
     * @OA\Get(
     *      path="/api/store-events/{domain}",
     *      summary="Get events by domain",
     *      tags={"Store events"},
     *      description="Get events by domain",
     *      operationId="getStoreEventsByDomain",
     *      security={
     *          {"JWT-Header": {}}
     *      },
     *      @OA\Parameter(
     *          description="Domain of store event",
     *          in="path",
     *          name="domain",
     *          required=true,
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/StoreEventsModel")
     *      )
     * )
     *
     * @param Request $request
     * @param string $domain
     * @return JsonResponse
     */
    public function show(Request $request, string $domain)
    {
        if (!$request->user()->can('show', StoresHistoryController::class)) {
            throw new ForbiddenError();
        }

        $storeEventsService = new StoreEventsService($request->all(), $domain);
        $events = $storeEventsService->getEvents();

        return $this->response($events);
    }
}
