<?php


namespace App\Http\Controllers\Api;

use Illuminate\Http\JsonResponse;
use Laravel\Lumen\Routing\Controller;

abstract class ApiController extends Controller
{
    /**
     * @param mixed $response
     * @param string|null $message
     * @param array $headers
     * @return JsonResponse
     */
    protected function response($response, string $message = null, array $headers = [])
    {
        if (is_null($message)) {
            $message = __("messages.loaded");
        }

        $toJson = [
            'message' => $message,
            'code'    => 200,
            'data'    => $response
        ];

        /**
         * @OA\Schema(
         *      schema="RequestSuccess",
         *      type="object",
         *      @OA\Property(property="code", format="int64", type="integer"),
         *      @OA\Property(property="message", type="string")
         * )
         */
        return response()->json($toJson)->withHeaders($headers);
    }
}