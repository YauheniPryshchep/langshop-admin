<?php

namespace App\Http\Controllers\Api\Campaigns\Templates;

use App\Exceptions\Http\ForbiddenError;
use App\Http\Controllers\Api\ApiController;
use App\Http\Controllers\Api\Controller;
use App\Services\MailingListService;
use Exception;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Services\MailGunTemplateService;
use App\Services\MailGunService;
use \App\Http\Controllers\Api\Campaigns\CampaignsController;
use Faker\Factory as Faker;

/** @OA\Schema(
 *      schema = "MainData",
 *      type="object",
 *      @OA\Property(property = "name", type = "string"),
 *      @OA\Property(property = "description", type = "string"),
 *      @OA\Property(property = "createdAt", type = "string"),
 *      @OA\Property(property = "id", type = "string"),
 * )
 *
 * @OA\Schema(
 *      schema = "WithVersion",
 *      type="object",
 *      allOf={
 *          @OA\Schema(ref="#/components/schemas/MainData"),
 *          @OA\Schema(
 *              @OA\Property(
 *                  property="version",
 *                  type="object",
 *                  @OA\Property(property="tag", type="string"),
 *                  @OA\Property(property="template", type="string"),
 *                  @OA\Property(property="engine", type="string"),
 *                  @OA\Property(property="createdAt", type="string"),
 *                  @OA\Property(property="comment", type="string"),
 *                  @OA\Property(property="active", type="boolean"),
 *                  @OA\Property(property="id", type="string")
 *              )
 *          )
 *      }
 * )
 *
 * @OA\Parameter(
 *      description="Name of template",
 *      in="path",
 *      name="templateName",
 *      required=true,
 *      @OA\Schema(
 *          type="string"
 *      )
 * )
 *
 *
 */
class TemplatesController extends ApiController
{

    protected $service;

    public function __construct()
    {
        $this->service = new MailGunTemplateService('default');
    }

    /**
     * @OA\Get(
     *      path="/api/campaigns/templates",
     *      summary="Get templates",
     *      tags={"Campaign templates"},
     *      description="Get templates",
     *      operationId="getTemplates",
     *      security={
     *          {"JWT-Header": {}}
     *      },
     *      @OA\Response(
     *          response=200,
     *          description="Successfully received all campaigns templates",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="items",
     *                  type="array",
     *                  @OA\Items(ref="#/components/schemas/MainData")
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Attemp of unauthorized request",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Access denied",
     *      ),
     *      @OA\Response(
     *          response=500,
     *          description="Internal Server Error",
     *      )
     * )
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request)
    {
        if (!$request->user()->can('show', CampaignsController::class)) {
            throw new ForbiddenError();
        }

        $templates = $this->service->getTemplates();
        return $this->response(['items' => $templates]);
    }

    /**
     * @OA\Post(
     *      path="/api/campaigns/templates",
     *      summary="Create template",
     *      tags={"Campaign templates"},
     *      description="Create template",
     *      operationId="createCampaignTemplate",
     *      security={
     *          {"JWT-Header": {}}
     *      },
     *      @OA\RequestBody(
     *          required=true,
     *          description="Request body",
     *          @OA\JsonContent(
     *              @OA\Property(property="name", type="string"),
     *              @OA\Property(property="description", type="string"),
     *              @OA\Property(property="comment", type="string"),
     *              @OA\Property(property="template", type="string"),
     *              @OA\Property(property="engine", type="string"),
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successfully stored campaign template",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/MainData"
     *          )
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Attemp of unauthorized request",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Access denied",
     *      ),
     *      @OA\Response(
     *          response=422,
     *          description="Unprocessable Entity",
     *      ),
     *      @OA\Response(
     *          response=500,
     *          description="Internal Server Error",
     *      )
     * )
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        if (!$request->user()->can('update', CampaignsController::class)) {
            throw new ForbiddenError();
        }

        $template = $this->service->storeTemplate($request->all());
        return $this->response($template);
    }

    /**
     * @OA\Get(
     *      path="/api/campaigns/templates/{templateName}",
     *      summary="Get template by Name",
     *      tags={"Campaign templates"},
     *      description="Get template by Name",
     *      operationId="getTemplateByName",
     *      security={
     *          {"JWT-Header": {}}
     *      },
     *      @OA\Parameter(ref="#/components/parameters/templateName"),
     *      @OA\Parameter(
     *          description="Key for additional parameters",
     *          in="query",
     *          name="active",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successfully received campaign template",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="template",
     *                  type="object",
     *                  ref="#/components/schemas/WithVersion"
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Attemp of unauthorized request",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Access denied",
     *      ),
     *      @OA\Response(
     *          response=500,
     *          description="Internal Server Error",
     *      )
     * )
     *
     * @param Request $request
     * @param string $name
     * @return JsonResponse
     */
    public function show(Request $request, string $name)
    {
        if (!$request->user()->can('show', CampaignsController::class)) {
            throw new ForbiddenError();
        }

        $template = $this->service->getTemplate($name, $request->active);
        return $this->response($template);
    }

    /**
     * @OA\Put(
     *      path="/api/campaigns/templates/{templateName}",
     *      summary="Update template by Name",
     *      tags={"Campaign templates"},
     *      description="Update template by Name",
     *      operationId="updateTemplateByName",
     *      security={
     *          {"JWT-Header": {}}
     *      },
     *      @OA\Parameter(ref="#/components/parameters/templateName"),
     *      @OA\RequestBody(
     *          required=true,
     *          description="Request body",
     *          @OA\JsonContent(
     *              @OA\Property(property="description", type="string"),
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successfully updated campaign template",
     *          @OA\JsonContent(
     *              @OA\Property(property="name", type="string")
     *          )
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Attemp of unauthorized request",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Access denied",
     *      ),
     *      @OA\Response(
     *          response=422,
     *          description="Unprocessable Entity",
     *      ),
     *      @OA\Response(
     *          response=500,
     *          description="Internal Server Error",
     *      )
     * )
     *
     * @param Request $request
     * @param string $name
     * @return JsonResponse
     */
    public function update(Request $request, string $name)
    {
        if (!$request->user()->can('update', CampaignsController::class)) {
            throw new ForbiddenError();
        }
        $template = $this->service->updateTemplate($name, $request->all());
        return $this->response($template);
    }

    /**
     * @OA\Delete(
     *      path="/api/campaigns/templates/{templateName}",
     *      summary="Delete template by Name",
     *      tags={"Campaign templates"},
     *      description="Delete template by Name",
     *      operationId="deleteTemplateByName",
     *      security={
     *          {"JWT-Header": {}}
     *      },
     *      @OA\Parameter(ref="#/components/parameters/templateName"),
     *      @OA\Response(
     *          response=200,
     *          description="Successfully deleted campaign template",
     *          @OA\JsonContent(
     *                  @OA\Property(property="name", type="string")
     *          )
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Attemp of unauthorized request",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Access denied",
     *      ),
     *      @OA\Response(
     *          response=500,
     *          description="Internal Server Error",
     *      )
     * )
     *
     * @param Request $request
     * @param string $name
     * @return JsonResponse
     */
    public function destroy(Request $request, string $name)
    {
        if (!$request->user()->can('update', CampaignsController::class)) {
            throw new ForbiddenError();
        }
        $template = $this->service->deleteTemplate($name);
        return $this->response($template);
    }

    /**
     * @OA\Post(
     *      path="/api/campaigns/templates/{templateName}/test-email",
     *      summary="Sending campaign test email",
     *      tags={"Campaign templates"},
     *      description="Sending campaign test email",
     *      operationId="sendCampaignTestEmail",
     *      security={
     *          {"JWT-Header": {}}
     *      },
     *      @OA\RequestBody(
     *          required=true,
     *          description="Request body",
     *          @OA\JsonContent(
     *              @OA\Property(property="to", type="string"),
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Email successfully sent",
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Attemp of unauthorized request",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Access denied",
     *      ),
     *      @OA\Response(
     *          response=422,
     *          description="Unprocessable Entity",
     *      ),
     *      @OA\Response(
     *          response=500,
     *          description="Internal Server Error",
     *      )
     * )
     *
     * @param Request $request
     * @param string $name
     * @return JsonResponse
     * @throws Exception
     */
    public function testEmail(Request $request, string $name)
    {
        $mailService = new MailGunService('default');

        if (!$request->user()->can('show', CampaignsController::class)) {
            throw new ForbiddenError();
        }

        $faker = Faker::create();

        $response = $mailService->send([
            'to'                    => $request->to,
            'subject'               => "This is test email",
            'template'              => $name ?? 'default',
            'h:X-Mailgun-Variables' => json_encode([
                'subject'           => "This is test email",
                'message'           => "This is test email",
                "domain"            => $faker->domainName,
                "store_name"        => $faker->domainName,
                "email"             => $request->to,
                "iana_timezone"     => $faker->timezone,
                "plan_display_name" => "plan_free",
                "primary_locale"    => "en",
                "currency"          => "USD",
                "customer_email"    => $request->to,
                "password_enabled"  => "1",
                "utm_source"        => "partners",
                "utm_medium"        => $campaign->type ?? "email",
                "utm_campaign"      => $faker->uuid
            ])
        ]);

        return $this->response($response);
    }

}
