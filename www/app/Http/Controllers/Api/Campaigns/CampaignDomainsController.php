<?php

namespace App\Http\Controllers\Api\Campaigns;

use App\Exceptions\CustomException;
use App\Exceptions\Http\ForbiddenError;
use App\Http\Controllers\Api\ApiController;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Services\CampaignDomainsService;

class CampaignDomainsController extends ApiController
{
    /**
     * @var CampaignDomainsService
     */
    protected $service;

    /**
     * CampaignDomainsController constructor.
     */
    public function __construct()
    {
        $this->service = new CampaignDomainsService();
    }

    /**
     * @OA\Get(
     *      path="/api/campaigns/campaign-domains",
     *      summary="Get domains to campaign",
     *      tags={"Campaign domains"},
     *      description="GET domains to campaign",
     *      operationId="getDomainsToCampaign",
     *      security={
     *          {"JWT-Header": {}}
     *      },
     *      @OA\RequestBody(
     *          required=true,
     *          description="Request body",
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successfully added domains to campaign",
     *          @OA\JsonContent(ref="#/components/schemas/CampaignDomainModel")
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Attemp of unauthorized request",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Access denied",
     *      ),
     *      @OA\Response(
     *          response=422,
     *          description="Unprocessable Entity",
     *      ),
     *      @OA\Response(
     *          response=500,
     *          description="Internal Server Error",
     *      )
     * )
     *
     * @param Request $request
     */
    public function index(Request $request)
    {

    }

    /**
     * @OA\Post(
     *      path="/api/campaigns/campaign-domains",
     *      summary="Add domains to campaign",
     *      tags={"Campaign domains"},
     *      description="Add domains to campaign",
     *      operationId="addDomainsToCampaign",
     *      security={
     *          {"JWT-Header": {}}
     *      },
     *      @OA\RequestBody(
     *          required=true,
     *          description="Request body",
     *          @OA\JsonContent(ref="#/components/schemas/CampaignDomainModel")
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successfully added domains to campaign",
     *          @OA\JsonContent(ref="#/components/schemas/CampaignDomainModel")
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Attemp of unauthorized request",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Access denied",
     *      ),
     *      @OA\Response(
     *          response=422,
     *          description="Unprocessable Entity",
     *      ),
     *      @OA\Response(
     *          response=500,
     *          description="Internal Server Error",
     *      )
     * )
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        if (!$request->user()->can('update', CampaignsController::class)) {
            throw new ForbiddenError();
        }
        $domains = $this->service->storeCampaignDomains($request->all());
        return $this->response($domains);
    }

    /**
     * @OA\Delete(
     *      path="/api/campaigns/campaign-domains/{id}",
     *      summary="Domain from campaign by ID",
     *      tags={"Campaign domains"},
     *      description="Delete domain from campaign by ID",
     *      operationId="deleteDomainFromCampaignById",
     *      security={
     *          {"JWT-Header": {}}
     *      },
     *      @OA\Parameter(
     *          description="ID of record in campaign_domains table",
     *          in="path",
     *          name="id",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *       @OA\Response(
     *          response=200,
     *          description="Successfully deleted domain from campaign",
     *          @OA\JsonContent(
     *                  @OA\Property(property="id", type="integer")
     *          )
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Attemp of unauthorized request",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Access denied",
     *      ),
     *      @OA\Response(
     *          response=500,
     *          description="Internal Server Error",
     *      )
     * )
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     * @throws CustomException
     */
    public function destroy(Request $request, int $id)
    {
        if (!$request->user()->can('update', CampaignsController::class)) {
            throw new ForbiddenError();
        }

        $this->service->deleteCampaignDomain($id);
        return $this->response(['id' => $id]);
    }

}
