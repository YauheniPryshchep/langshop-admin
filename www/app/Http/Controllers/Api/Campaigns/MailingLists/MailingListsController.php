<?php

namespace App\Http\Controllers\Api\Campaigns\MailingLists;

use App\Http\Controllers\Api\Controller;
use Illuminate\Http\Request;
use \App\Http\Controllers\Api\Campaigns\CampaignsController;
use App\Services\MailingListService;

class MailingListsController extends Controller
{

    protected $service;

    public function __construct()
    {
        $this->service = new MailingListService('default');
    }

    public function index(Request $request)
    {
        if (!$request->user()->can('show', CampaignsController::class)) {
            return response([trans('auth.access_denied')], 403);
        }

        $mailingLists = $this->service->getMailingLists();
        return response()->json(['items' => $mailingLists]);
    }

    public function store(Request $request)
    {
        if (!$request->user()->can('update', CampaignsController::class)) {
            return response([trans('auth.access_denied')], 403);
        }

        $mailingList = $this->service->storeMailingList($request->all());
        return response()->json($mailingList);
    }

    public function show(Request $request, string $address)
    {
        if (!$request->user()->can('show', CampaignsController::class)) {
            return response([trans('auth.access_denied')], 403);
        }
        $mailingList = $this->service->getMailingList($address);
        if (!$mailingList) {
            throw new CustomException(trans('errors.campaigns.404'), 404);
        }

        return response()->json($mailingList);
    }

    public function update(Request $request, string $address)
    {
        if (!$request->user()->can('update', CampaignsController::class)) {
            return response([trans('auth.access_denied')], 403);
        }
        $mailingList = $this->service->updateMailingList($address, $request->all());
        return response()->json($mailingList);
    }

    public function destroy(Request $request, string $address)
    {
        if (!$request->user()->can('update', CampaignsController::class)) {
            return response([trans('auth.access_denied')], 403);
        }
        $mailingList = $this->service->deleteMailingList($address);
        return response()->json(['name' => $address]);
    }

}
