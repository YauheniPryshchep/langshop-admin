<?php

namespace App\Http\Controllers\Api\Campaigns;

use App\Http\Controllers\Api\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\Api\Campaigns\CampaignsController;
use App\Services\RecipientsFilterService;

class RecipientsFilterController extends Controller
{

    protected $service;

    public function __construct()
    {
        $this->service = new RecipientsFilterService();
    }

    /**
     * @OA\Post(
     *      path="/api/campaigns/recipients-filter",
     *      summary="Create campaign recipients filter",
     *      tags={"Campaign recipients filters"},
     *      description="Create campaign recipients filter",
     *      operationId="createCampaignRecipientsFilter",
     *      security={
     *          {"JWT-Header": {}}
     *      },
     *      @OA\RequestBody(
     *          required=true,
     *          description="Request body",
     *          @OA\JsonContent(ref="#/components/schemas/CampaignRecipientsFilterModel")
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successfully stored campaign template",
     *          @OA\JsonContent(ref="#/components/schemas/CampaignRecipientsFilterModel")
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Attemp of unauthorized request",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Access denied",
     *      ),
     *      @OA\Response(
     *          response=422,
     *          description="Unprocessable Entity",
     *      ),
     *      @OA\Response(
     *          response=500,
     *          description="Internal Server Error",
     *      )
     * )
     *
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function store(Request $request)
    {
        if (!$request->user()->can('update', CampaignsController::class)) {
            return response([trans('auth.access_denied')], 403);
        }
        $filter = $this->service->storeFilter($request->all());
        return response()->json($filter);
    }

    /**
     * @OA\Get(
     *      path="/api/campaigns/recipients-filter/{id}",
     *      summary="Get campaign recipients filter by ID",
     *      tags={"Campaign recipients filters"},
     *      description="Get campaign recipients filter by ID",
     *      operationId="getCampaignRecipientsFilterById",
     *      security={
     *          {"JWT-Header": {}}
     *      },
     *      @OA\Parameter(
     *          description="ID of campaign recipients filter",
     *          in="path",
     *          name="id",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successfully got campaign recipients filter",
     *          @OA\JsonContent(ref="#/components/schemas/CampaignRecipientsFilterModel")
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Attemp of unauthorized request",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Access denied",
     *      ),
     *      @OA\Response(
     *          response=500,
     *          description="Internal Server Error",
     *      )
     * )
     *
     * @param Request $request
     * @param $id
     * @return JsonResponse
     * @throws CustomException
     */
    public function show(Request $request, $id)
    {
        if (!$request->user()->can('show', CampaignsController::class)) {
            return response([trans('auth.access_denied')], 403);
        }

        $filter = $this->service->getFilter($id);
        return response()->json($filter);
    }

    /**
     * @OA\Put(
     *      path="/api/campaigns/recipients-filter/{id}",
     *      summary="Update campaign recipients filter by ID",
     *      tags={"Campaign recipients filters"},
     *      description="Update campaign recipients filter by ID",
     *      operationId="updateCampaignRecipientsFilterById",
     *      security={
     *          {"JWT-Header": {}}
     *      },
     *      @OA\Parameter(
     *          description="ID of campaign recipients filter",
     *          in="path",
     *          name="id",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),         
     *      @OA\RequestBody(
     *          required=true,
     *          description="Request body",
     *          @OA\JsonContent(ref="#/components/schemas/CampaignRecipientsFilterModel")
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successfully updated campaign recipients filter",
     *          @OA\JsonContent(ref="#/components/schemas/CampaignRecipientsFilterModel")
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Attemp of unauthorized request",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Access denied",
     *      ),
     *      @OA\Response(
     *          response=422,
     *          description="Unprocessable Entity",
     *      ),
     *      @OA\Response(
     *          response=500,
     *          description="Internal Server Error",
     *      )
     * )
     *
     * @param Request $request
     * @param $id
     * @return JsonResponse
     * @throws ValidationException
     */
    public function update(Request $request, $id)
    {
        if (!$request->user()->can('update', CampaignsController::class)) {
            return response([trans('auth.access_denied')], 403);
        }

        $filter = $this->service->updateFilter($id, $request->all());
        return response()->json($filter);
    }

    /**
     * @OA\Delete(
     *      path="/api/campaigns/recipients-filter/{id}",
     *      summary="Delete campaign recipients filter by ID",
     *      tags={"Campaign recipients filters"},
     *      description="Delete campaign recipients filter by ID",
     *      operationId="deleteCampaignRecipientsFilterById",
     *      security={
     *          {"JWT-Header": {}}
     *      },
     *      @OA\Parameter(
     *          description="ID of campaign recipients filter",
     *          in="path",
     *          name="id",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),        
     *       @OA\Response(
     *          response=200,
     *          description="Successfully deleted campaign recipients filter",
     *          @OA\JsonContent(
     *                  @OA\Property(property="id", type="integer")
     *          )
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Attemp of unauthorized request",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Access denied",
     *      ),
     *      @OA\Response(
     *          response=500,
     *          description="Internal Server Error",
     *      )
     * )
     *
     * @param Request $request
     * @param $id
     * @return JsonResponse
     */
    public function destroy(Request $request, $id)
    {
        if (!$request->user()->can('update', CampaignsController::class)) {
            return response([trans('auth.access_denied')], 403);
        }

        $this->service->deleteFilter($id);
        return response()->json(['id' => $id]);
    }

}
