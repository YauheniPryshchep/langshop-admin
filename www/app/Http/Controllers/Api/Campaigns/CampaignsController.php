<?php


namespace App\Http\Controllers\Api\Campaigns;

use App\Exceptions\Http\ForbiddenError;
use App\Exceptions\Http\NotFoundError;
use App\Http\Controllers\Api\ApiController;
use App\Models\Campaign;
use App\Services\MailgunStatsService;
use Dotenv\Exception\ValidationException;
use Exception;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Services\CampaignService;
use App\Exceptions\Http\HttpError;
use Symfony\Component\HttpFoundation\Response;

class CampaignsController extends ApiController
{

    /**
     * @var Request
     */
    private $request;

    /**
     * @var CampaignService
     */

    protected $service;

    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->service = new CampaignService();
    }

    /**
     * @OA\Get(
     *      path="/api/campaigns",
     *      summary="Get campaigns",
     *      tags={"Campaigns"},
     *      description="Get campaigns",
     *      operationId="getCampaigns",
     *      security={
     *          {"JWT-Header": {}}
     *      },
     *      @OA\Parameter(ref="#/components/parameters/query_page"),
     *      @OA\Parameter(ref="#/components/parameters/query_limit"),
     *      @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\JsonContent(
     *              @OA\Property(property="items", type="array", @OA\Items(ref="#/components/schemas/CampaignModel")),
     *              @OA\Property(property="count", type="integer")
     *          )
     *      )
     * )
     *
     * @param Request $request
     * @return JsonResponse|Response|ResponseFactory
     */
    public function index(Request $request)
    {
        if (!$request->user()->can('show', self::class)) {
            throw new ForbiddenError();
        }

        $campaigns = $this->service->getCampaigns($request);

        return $this->response($campaigns);
    }

    /**
     * @OA\Get(
     *      path="/api/campaigns/{campaignId}",
     *      summary="Get campaign by ID",
     *      tags={"Campaigns"},
     *      description="Get campaign by ID",
     *      operationId="getCampaignById",
     *      security={
     *          {"JWT-Header": {}}
     *      },
     *      @OA\Parameter(
     *          description="ID of campaign",
     *          in="path",
     *          name="campaignId",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/CampaignModel")
     *      )
     * )
     *
     * @param $id
     * @return JsonResponse
     */
    public function show($id)
    {
        if (!$this->request->user()->can('show', self::class)) {
            throw new ForbiddenError();
        }

        $campaign = $this->service->getCampaign($id);

        return $this->response($campaign);
    }

    /**
     * @OA\Post(
     *      path="/api/campaigns",
     *      summary="Create campaign",
     *      tags={"Campaigns"},
     *      description="Create campaign",
     *      operationId="createCampaign",
     *      security={
     *          {"JWT-Header": {}}
     *      },
     *      @OA\RequestBody(
     *          required=true,
     *          description="Request body",
     *          @OA\JsonContent(ref="#/components/schemas/CampaignModel")
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/CampaignModel")
     *      )
     * )
     *
     *
     * @return JsonResponse
     * @throws HttpError
     */
    public function store()
    {
        if (!$this->request->user()->can('update', self::class)) {
            throw new ForbiddenError();
        }

        $campaign = $this->service->storeCampaign($this->request->all());

        return $this->response($campaign, __("campaigns.messages.created"));
    }

    /**
     * @OA\Put(
     *      path="/api/campaigns/{campaignId}",
     *      summary="Update campaign by ID",
     *      tags={"Campaigns"},
     *      description="Update campaign by ID",
     *      operationId="updateCampaignById",
     *      security={
     *          {"JWT-Header": {}}
     *      },
     *      @OA\Parameter(
     *          description="ID of campaign",
     *          in="path",
     *          name="campaignId",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          description="Request body",
     *          @OA\JsonContent(ref="#/components/schemas/CampaignModel")
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/CampaignModel")
     *      )
     * )
     *
     * @param $id
     * @return JsonResponse
     * @throws ValidationException
     */
    public function update($id)
    {
        if (!$this->request->user()->can('update', self::class)) {
            throw new ForbiddenError();
        }

        $campaign = $this->service->updateCampaign($this->request->all(), $id);

        return $this->response($campaign);
    }

    /**
     * @OA\Delete(
     *      path="/api/campaigns/{campaignId}",
     *      summary="Delete campaign by ID",
     *      tags={"Campaigns"},
     *      description="Delete campaign by ID",
     *      operationId="deleteCampaignById",
     *      security={
     *          {"JWT-Header": {}}
     *      },
     *      @OA\Parameter(
     *          description="ID of campaign",
     *          in="path",
     *          name="campaignId",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="successful operation"
     *      )
     * )
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws GuzzleException
     */
    public function destroy(Request $request, $id)
    {
        if (!$request->user()->can('update', self::class)) {
            throw new ForbiddenError();
        }

        $this->service->deleteCampaign($id);

        return $this->response(["id" => $id]);
    }

    /**
     * @OA\Get(
     *      path="/api/campaigns/{campaignId}/stats",
     *      summary="Get campaign base analytics",
     *      tags={"Campaigns"},
     *      description="Get campaign base analytics",
     *      operationId="statsCampaignById",
     *      security={
     *          {"JWT-Header": {}}
     *      },
     *      @OA\Parameter(
     *          description="ID of campaign",
     *          in="path",
     *          name="campaignId",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\JsonContent(
     *              @OA\Property(property="items", type="array",
     *                  @OA\Items(
     *                      @OA\Property(property="time", type="string"),
     *                      @OA\Property(property="accepted", type="array",
     *                          @OA\Items(
     *                               @OA\Property(property="outgoing", type="integer"),
     *                               @OA\Property(property="incoming", type="integer"),
     *                               @OA\Property(property="total", type="integer"),
     *                          )
     *                      ),
     *
     *                       @OA\Property(property="delivered", type="array",
     *                          @OA\Items(
     *                              @OA\Property(property="smtp", type="integer"),
     *                              @OA\Property(property="http", type="integer"),
     *                              @OA\Property(property="total", type="integer"),
     *                          )
     *                      ),
     *                      @OA\Property(property="failed", type="array",
     *                          @OA\Items(
     *                              @OA\Property(property="permanent", type="array",
     *                                  @OA\Items(
     *                                      @OA\Property(property="bounce", type="integer"),
     *                                      @OA\Property(property="delayed-bounce", type="integer"),
     *                                      @OA\Property(property="suppress-bounce", type="integer"),
     *                                      @OA\Property(property="suppress-unsubscribe", type="integer"),
     *                                      @OA\Property(property="suppress-complaint", type="integer"),
     *                                      @OA\Property(property="total", type="integer"),
     *                                  )
     *                              ),
     *                              @OA\Property(property="temporary", type="array",
     *                                   @OA\Items(
     *                                      @OA\Property(property="espblock", type="integer"),
     *                                      @OA\Property(property="total", type="integer"),
     *                                  )
     *                              )
     *                          )
     *                      )
     *                  )
     *              ),
     *          )
     *      )
     * )
     *
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function stats(Request $request, $id)
    {
        if (!$request->user()->can('show', self::class)) {
            throw new ForbiddenError();
        }

        $campaign = Campaign::query()
            ->where('id', $id)
            ->first();

        if (!$campaign) {
            throw new NotFoundError();
        }
        try {
            $mailgunDomain = config('mailgun.default.domain');
            $tag = 'campaign-' . $campaign->id . '@' . $mailgunDomain;
            $mailgunStatsService = new MailgunStatsService('default');

            $stats = $mailgunStatsService->getTagStats($tag, $campaign->started_at);

            return $this->response([
                "items" => $stats['stats']
            ]);
        } catch (Exception $e) {
            throw new NotFoundError();
        }
    }
}
