<?php

namespace App\Http\Controllers\Api\Campaigns;

use App\Exceptions\Http\ForbiddenError;
use App\Http\Controllers\Api\ApiController;
use App\Http\Controllers\Api\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use \App\Http\Controllers\Api\Campaigns\CampaignsController;
use App\Services\Filters\Resolver;

class RecipientsController extends ApiController
{

    /**
     * @OA\Post(
     *      path="/api/campaigns/recipients",
     *      summary="Recipients filter",
     *      tags={"Campaigns"},
     *      description="Filtered campaigns recipients",
     *      operationId="getCampaignsRecipients",
     *      security={
     *          {"JWT-Header": {}}
     *      },
     *      @OA\RequestBody(
     *          required=true,
     *          description="Request body",
     *          @OA\JsonContent(
     *              @OA\Property(property="page", type="integer"),
     *              @OA\Property(property="limit", type="integer"),
     *              @OA\Property(property="filters", type="array",
     *                  @OA\Items(
     *                      @OA\Property(property="comparator", type="string"),
     *                      @OA\Property(property="key", type="string"),
     *                      @OA\Property(property="value", type="string")
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful received recipients list",
     *          @OA\JsonContent(
     *              @OA\Property(property="limit", type="integer"),
     *              @OA\Property(property="page", type="integer"),
     *              @OA\Property(property="total", type="integer"),
     *              @OA\Property(property="items", type="array",
     *                  @OA\Items(type="string")
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Attemp of unauthorized request",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Access denied",
     *      ),
     *      @OA\Response(
     *          response=422,
     *          description="Unprocessed entity",
     *      ),
     *      @OA\Response(
     *          response=500,
     *          description="Internal Server Error",
     *      )
     * )
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request)
    {

        if (!$request->user()->can('show', CampaignsController::class)) {
            throw new ForbiddenError();
        }

        $resolver = new Resolver($request->all());
        $recipients = $resolver->resolve();

        return $this->response($recipients);
    }

}
