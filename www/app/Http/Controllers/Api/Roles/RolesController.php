<?php

namespace App\Http\Controllers\Api\Roles;

use App\Exceptions\Http\BadRequestError;
use App\Exceptions\Http\ForbiddenError;
use App\Exceptions\Http\NotFoundError;
use App\Http\Controllers\Api\ApiController;
use App\Models\Role;
use App\Models\UsersRoles;
use App\Objects\SimplePaginationObject;
use App\Providers\RolesServiceProvider;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;
use Laravel\Lumen\Http\ResponseFactory;

class RolesController extends ApiController
{
    /**
     * @var RolesServiceProvider
     */
    private $rolesServiceProvider = null;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->rolesServiceProvider = app(RolesServiceProvider::class);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request)
    {
        if (!$request->user()->can('show', self::class)) {
            throw new ForbiddenError();
        }

        $pagination = new SimplePaginationObject();
        $pagination->fillFromRequest($request, [
            'id',
            'name',
            'description',
        ]);

        return $this->response($this->rolesServiceProvider->getAll($pagination));
    }

    /**
     * @param Request $request
     * @param $id integer
     * @return JsonResponse|Response|ResponseFactory
     */
    public function show(Request $request, $id)
    {
        if (!$request->user()->can('update', self::class)) {
            throw new ForbiddenError();
        }

        $userInfo = $this->rolesServiceProvider->getInfo($id);

        if ($userInfo->isEmpty()) {
            throw new NotFoundError();
        }

        return $this->response($userInfo->first());
    }

    /**
     * @param Request $request
     * @return JsonResponse|Response|ResponseFactory
     * @throws ValidationException
     */
    public function store(Request $request)
    {
        if (!$request->user()->can('update', self::class)) {
            throw new ForbiddenError();
        }

        $this->validate($request,
            [
                'name'        => [
                    'required',
                    'alpha_dash',
                    'max:255',
                    Rule::unique('roles', 'name'),
                ],
                'description' => 'required|max:255',
            ]
        );

        /**
         * @var Role $role
         */
        $role = Role::query()
            ->create(
                $request->only(
                    [
                        'name',
                        'description'
                    ]
                )
            );

        if (empty($role->id)) {
            throw new BadRequestError();
        }

        $idsList = array_map(function ($item) {
            return (int)$item['id'];
        }, $request->get('scopes', []));

        $role->scopes()->sync($idsList);

        return $this->response(Role::with(['scopes'])->where('id', $role->id)->get()->first());
    }

    /**
     * @param Request $request
     * @param $id integer
     * @return JsonResponse|Response|ResponseFactory
     * @throws Exception
     */
    public function update(Request $request, $id)
    {
        if (!$request->user()->can('update', self::class)) {
            throw new ForbiddenError();
        }

        $this->validate($request,
            [
                'name'         => [
                    'required',
                    'alpha_dash',
                    'max:255',
                    Rule::unique('roles', 'name')->ignore($id)
                ],
                'description'  => 'required|max:255',
                'scopes' => 'array',
            ]
        );

        /**
         * @var Role $role
         */
        $role = Role::find($id);

        if (is_null($role)) {
            throw new NotFoundError();
        }

        if ($role->name == 'sadmin' && !$request->user()->isSAdmin()) {
            throw new ForbiddenError();
        }

        $role->update(['description' => $request->get('description')]);

        if ($role->name !== 'sadmin' || $request->user()->isSAdmin()) {
            $idsList = array_map(function ($item) {
                return (int)$item['id'];
            }, $request->get('scopes', []));

            $role->scopes()->sync($idsList);
        }

        return $this->response( Role::with(['scopes'])->where('id', $id)->get()->first());
    }

    /**
     * @param Request $request
     * @param $id integer
     * @return JsonResponse|Response|ResponseFactory
     * @throws Exception
     */
    public function destroy(Request $request, $id)
    {
        if (!$request->user()->can('update', self::class)) {
            throw new ForbiddenError();
        }

        /**
         * @var Role $role
         */
        $role = Role::query()
            ->find($id);

        if (is_null($role)) {
            throw new NotFoundError();
        }

        if ($role->name == 'sadmin' && !$request->user()->isSAdmin()) {
            throw new ForbiddenError();
        }

        UsersRoles::query()
            ->where('roles_id', $role->id)
            ->update([
                'roles_id' => 2
            ]);

        $role->scopes()->sync([]);
        $role->delete();

        return $this->response(['id' => $role->id]);
    }
}
