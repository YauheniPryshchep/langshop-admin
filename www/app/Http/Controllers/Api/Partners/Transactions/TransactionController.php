<?php

namespace App\Http\Controllers\Api\Partners\Transactions;

use App\Http\Controllers\Api\ApiController;
use App\Models\Store;
use App\Models\User;
use App\Services\Partners\PartnerApiService;
use App\Services\Shopify\TransactionService;
use App\Services\StoreReferralService;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class TransactionController extends ApiController
{
    /**
     * @var Request
     */
    protected $request;

    /**
     * @var PartnerApiService
     */
    private $partnerApiService;

    /**
     * TransactionController constructor.
     * @param Request $request
     * @param PartnerApiService $partnerApiService
     */
    public function __construct(Request $request, PartnerApiService $partnerApiService)
    {
        $this->request = $request;
        $this->partnerApiService = $partnerApiService;
    }

    /**
     * @param int $id
     * @return JsonResponse
     * @throws GuzzleException
     */
    public function show(int $id)
    {
        $transactions = $this->partnerApiService->balance($id);
        return $this->response($transactions);
    }

    /**
     * @return JsonResponse
     * @throws GuzzleException
     */
    public function index()
    {
        $transactions = $this->partnerApiService->totalBalance();

        return $this->response($transactions);
    }
}
