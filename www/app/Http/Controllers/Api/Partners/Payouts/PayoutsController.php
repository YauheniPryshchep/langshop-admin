<?php

namespace App\Http\Controllers\Api\Partners\Payouts;

use App\Exceptions\Http\ForbiddenError;
use App\Http\Controllers\Api\ApiController;
use App\Models\Partners\Payout;
use App\Objects\SimplePaginationObject;
use App\Services\Partners\PartnerApiService;
use App\Services\Partners\PayoutsService;
use App\Validators\QueryFilter;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;

class PayoutsController extends ApiController
{
    /**
     * @var Request
     */
    protected $request;
    /**
     * @var PayoutsService
     */
    protected $payoutsService;
    /**
     * @var PartnerApiService
     */
    private $partnerService;

    /**
     * PayoutController constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->payoutsService = new PayoutsService();
        $this->partnerService = new PartnerApiService();
    }

    /**
     * @OA\Get(
     *      path="/api/payouts",
     *      summary="Get payouts",
     *      tags={"Payouts"},
     *      description="List of payouts",
     *      operationId="getPayouts",
     *      security={
     *          {"JWT-Header": {}}
     *      },
     *      @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\JsonContent(
     *              allOf={
     *                  @OA\Schema(ref="#/components/schemas/RequestSuccess"),
     *                  @OA\Schema(
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          @OA\Property(
     *                              property="items",
     *                              type="array",
     *                              @OA\Items(ref="#/components/schemas/PayoutsModel")
     *                          )
     *                      )
     *                  )
     *              }
     *          )
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthorized",
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(ref="#/components/schemas/HttpError")
     *          )
     *      )
     * )
     *
     * @param Request $request
     * @return JsonResponse
     * @throws GuzzleException
     */
    public function index(Request $request): JsonResponse
    {
        if (!$request->user()->can('show', self::class)) {
            throw new ForbiddenError();
        }

        $pagination = new SimplePaginationObject();
        //TODO: REWRITE THAN
        $status = $request->get('status');

        $payouts = $this->payoutsService->getAll($status, $pagination);
        return $this->response($payouts);
    }


    /**
     * @OA\Get(
     *      path="/api/payouts/{id}",
     *      summary="Get payout by id",
     *      tags={"Payouts"},
     *      description="Payout by id",
     *      operationId="getPayoutsById",
     *      security={
     *          {"JWT-Header": {}}
     *      },
     *      @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\JsonContent(
     *              allOf={
     *                  @OA\Schema(ref="#/components/schemas/RequestSuccess"),
     *                  @OA\Schema(
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          ref="#/components/schemas/PayoutsModel"
     *                          )
     *                      )
     *              }
     *          )
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthorized",
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(ref="#/components/schemas/HttpError")
     *          )
     *      )
     * )
     *
     * @param int $id
     * @param Request $request
     * @return JsonResponse
     */
    public function show(int $id, Request $request): JsonResponse
    {

        if (!$request->user()->can('show', self::class)) {
            throw new ForbiddenError();
        }

        $payout = $this->payoutsService->getInfo($id);
        return $this->response($payout);
    }

    /**
     * @OA\Put(
     *      path="/api/payouts/{id}",
     *      summary="Get payout by id",
     *      tags={"Payouts"},
     *      description="Payout by id",
     *      operationId="getPayoutsById",
     *      security={
     *          {"JWT-Header": {}}
     *      },
     *      @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\JsonContent(
     *              allOf={
     *                  @OA\Schema(ref="#/components/schemas/RequestSuccess"),
     *                  @OA\Schema(
     *                      @OA\Property(
     *                          property="data",
     *                          type="object",
     *                          ref="#/components/schemas/PayoutsModel"
     *                          )
     *                      )
     *              }
     *          )
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthorized",
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(ref="#/components/schemas/HttpError")
     *          )
     *      )
     * )
     *
     * @param int $id
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     * @throws GuzzleException
     */
    public function update(int $id, Request $request): JsonResponse
    {

        if (!$request->user()->can('update', self::class)) {
            throw new ForbiddenError();
        }

        $this->validate($request,
            [
                'status' => [
                    Rule::in([
                        Payout::PAID_STATUS,
                        Payout::DECLINED_STATUS,
                    ])
                ]
            ]
        );

        $payout = $this->payoutsService->update($id, $request->get('status'));
        return $this->response($payout);
    }

}
