<?php

namespace App\Http\Controllers\Api\Partners\Users;

use App\Exceptions\Http\ForbiddenError;
use App\Exceptions\Http\NotFoundError;
use App\Http\Controllers\Api\ApiController;
use App\Http\Controllers\Api\Partners\Payouts\PayoutsController;
use App\Objects\SimplePaginationObject;
use App\Services\Partners\PartnersUserService;
use App\Services\Partners\PayoutsService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Laravel\Lumen\Http\ResponseFactory;

class UserController extends ApiController
{
    /**
     * @var PartnersUserService
     */
    private $partnersUserService;

    /**
     * @var PayoutsService
     */
    private $payoutService;
    /**
     * @var Request
     */
    private $request;


    /**
     * UsersController constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->partnersUserService = new PartnersUserService();
        $this->payoutService = new PayoutsService();
        $this->request = $request;
    }

    /**
     * @param Request $request
     * @return JsonResponse|Response|ResponseFactory
     */
    public function index(Request $request)
    {
        if (!$request->user()->can('show', self::class)) {
            throw new ForbiddenError();
        }

        $pagination = new SimplePaginationObject();
        $pagination->fillFromRequest($request, [
            'id',
            'email',
            'created_at',
            'updated_at',
        ]);

        return $this->response($this->partnersUserService->getAll($pagination));
    }

    /**
     * @param Request $request
     * @param int $id
     * @return JsonResponse|Response|ResponseFactory
     */
    public function show(Request $request, int $id)
    {
        if (!$request->user()->can('show', self::class)) {
            throw new ForbiddenError();
        }

        $userInfo = $this->partnersUserService->getInfo($id);

        if ($userInfo->isEmpty()) {
            throw new NotFoundError();
        }

        return $this->response($userInfo->first());
    }

    /**
     * @param Request $request
     * @param int $id
     * @return JsonResponse|Response|ResponseFactory
     */
    public function stores(Request $request, int $id)
    {
        if (!$request->user()->can('showStores', self::class)) {
            throw new ForbiddenError();
        }

        $pagination = new SimplePaginationObject();

        return $this->response($this->partnersUserService->getStores($id, $pagination));
    }

    /**
     * @param Request $request
     * @param int $id
     * @return JsonResponse|Response|ResponseFactory
     */
    public function payouts(Request $request, int $id)
    {
        if (!$request->user()->can('show', PayoutsController::class)) {
            throw new ForbiddenError();
        }

        $pagination = new SimplePaginationObject();

        return $this->response($this->payoutService->getUserPayouts($id, $pagination));
    }

    /**
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     */
    public function commissions(Request $request, int $id)
    {
        if (!$request->user()->can('show', self::class)) {
            throw new ForbiddenError();
        }

        return $this->response($this->partnersUserService->setNewCommission($id, $request->all()));

    }
}
