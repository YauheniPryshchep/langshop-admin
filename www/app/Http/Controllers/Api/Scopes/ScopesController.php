<?php

namespace App\Http\Controllers\Api\Scopes;

use App\Exceptions\Http\ForbiddenError;
use App\Exceptions\Http\NotFoundError;
use App\Http\Controllers\Api\ApiController;
use App\Models\Scope;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Laravel\Lumen\Http\ResponseFactory;

class ScopesController extends ApiController
{
    /**
     * @param Request $request
     * @return JsonResponse|Response|ResponseFactory
     */
    public function index(Request $request)
    {
        if (!$request->user()->can('show', self::class)) {
            throw new ForbiddenError();
        }

        return $this->response(Scope::query()
            ->select('scopes.*', 'scopes_types.name as type')
            ->join('scopes_types', 'scopes_types.id', '=', 'scopes.scopes_types_id')
            ->get());
    }

    /**
     * @param Request $request
     * @param int $id integer
     * @return JsonResponse|Response|ResponseFactory
     */
    public function show(Request $request, int $id)
    {
        if (!$request->user()->can('show', self::class)) {
            throw new ForbiddenError();
        }

        $scopeInfo = Scope::query()
            ->select('scopes.*', 'scopes_types.name as type')
            ->join('scopes_types', 'scopes_types.id', '=', 'scopes.scopes_types_id')
            ->where('id', $id)
            ->get();

        if ($scopeInfo->isEmpty()) {
            throw new NotFoundError();
        }

        return $this->response($scopeInfo->first());
    }
}
