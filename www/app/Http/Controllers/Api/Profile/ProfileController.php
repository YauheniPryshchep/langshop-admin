<?php

namespace App\Http\Controllers\Api\Profile;

use App\Http\Controllers\Api\ApiController;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

class ProfileController extends ApiController
{
    /**
     * @return JsonResponse
     */
    public function show()
    {

        /**
         * @var User $user
         */
        $user = User::with(['scopes'])
            ->where('id', Auth::user()->getAuthIdentifier())
            ->first();

        $user->settings = [
            "mailgunDomain" => config('mailgun.default.domain')
        ];

        return $this->response($user);
    }
}
