<?php

namespace App\Http\Controllers\Api\Locales;

use App\Exceptions\CustomException;
use App\Exceptions\Http\ForbiddenError;
use App\Http\Controllers\Api\Controller;
use App\Providers\LocalesServiceProvider;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Http\Request;

class LocalesController extends Controller
{

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws FileNotFoundException
     * @throws CustomException
     */
    public function index(Request $request)
    {
        if (!$request->user()->can('show', self::class)) {
            throw new ForbiddenError();
        }

        $provider = new LocalesServiceProvider();

        return response()->json(
            $provider->getAll()
        );
    }
}