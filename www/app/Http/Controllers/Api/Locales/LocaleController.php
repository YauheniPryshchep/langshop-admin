<?php

namespace App\Http\Controllers\Api\Locales;

use App\Exceptions\Http\ForbiddenError;
use App\Exceptions\Http\NotFoundError;
use App\Http\Controllers\Api\Controller;
use App\Providers\LocalesServiceProvider;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;

class LocaleController extends Controller
{
    /**
     * @param Request $request
     * @param string $locale
     * @return JsonResponse|Response|ResponseFactory
     * @throws FileNotFoundException
     */
    public function index(Request $request, string $locale)
    {
        if (!$request->user()->can('show', self::class)) {
           throw new ForbiddenError();
        }

        $provider = new LocalesServiceProvider();

        return response()->json(
            $provider->getLocaleFiles($locale)
        );
    }

    /**
     * @param Request $request
     * @param string $iso
     * @param string $file
     * @return JsonResponse|Response|ResponseFactory
     */
    public function show(Request $request, string $iso, string $file)
    {
        if (!$request->user()->can('update', self::class)) {
            throw new ForbiddenError();
        }

        $provider = new LocalesServiceProvider();

        try {
            $data = $provider->getFile($iso, $file);
        } catch (FileNotFoundException $e) {
           throw new NotFoundError();
        }

        return response()->json($data);
    }

    /**
     * @param Request $request
     * @param string $iso
     * @param string $file
     * @return JsonResponse|Response|ResponseFactory
     * @throws ValidationException
     */
    public function update(Request $request, string $iso, string $file)
    {
        if (!$request->user()->can('update', self::class)) {
           throw new ForbiddenError();
        }

        $provider = new LocalesServiceProvider();

        $data = $this->validate($request, [
            'items'          => 'required|array',
            'items.*.key'    => 'required|string',
            'items.*.target' => 'string',
        ]);

        try {
            $data = $provider->updateFile($iso, $file, $data['items']);
        } catch (FileNotFoundException $e) {
            throw new NotFoundError();
        }

        return response()->json($data);
    }
}