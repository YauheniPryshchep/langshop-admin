<?php

namespace App\Http\Controllers\Api\Assets;

use App\Http\Controllers\Api\ApiController;
use Illuminate\Support\Facades\Storage;

class AssetsController extends ApiController
{
    public function index()
    {
        $files = collect(Storage::disk('s3Assets')->allFiles())->map(function ($file) {
            return Storage::disk('s3Assets')->url($file);
        });

        return response()->json(
            $files
        );
    }
}
