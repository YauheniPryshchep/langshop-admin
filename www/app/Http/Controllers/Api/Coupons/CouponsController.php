<?php

namespace App\Http\Controllers\Api\Coupons;

use App\Exceptions\Http\ForbiddenError;
use App\Http\Controllers\Api\ApiController;
use App\Services\CouponsService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class CouponsController extends ApiController
{

    /**
     * @var CouponsService
     */
    protected $couponsService;

    /**
     * @var Request
     */
    protected $request;

    /**
     * CouponsController constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->couponsService = new CouponsService();
    }

    /**
     * @OA\Get(
     *      path="/api/coupons",
     *      summary="Get coupouns",
     *      tags={"Coupons"},
     *      description="Get coupons",
     *      operationId="getCoupons",
     *      security={
     *          {"JWT-Header": {}}
     *      },
     *      @OA\Parameter(ref="#/components/parameters/query_page"),
     *      @OA\Parameter(ref="#/components/parameters/query_limit"),
     *      @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\JsonContent(
     *              @OA\Property(property="items", type="array", @OA\Items(ref="#/components/schemas/CouponModel")),
     *              @OA\Property(property="count", type="integer")
     *          )
     *      )
     * )
     *
     *
     * @return JsonResponse
     */
    public function index()
    {
        if (!$this->request->user()->can('show', self::class)) {
            throw new ForbiddenError();
        }

        $coupons = $this->couponsService->getItems($this->request->all());
        return $this->response($coupons);
    }

    /**
     * @OA\Put(
     *      path="/api/coupons/{couponId}",
     *      summary="Update coupon by ID",
     *      tags={"Coupons"},
     *      description="Update coupon by ID",
     *      operationId="updateCouponById",
     *      security={
     *          {"JWT-Header": {}}
     *      },
     *      @OA\Parameter(
     *          description="ID of coupon",
     *          in="path",
     *          name="couponId",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          description="Request body",
     *          @OA\JsonContent(ref="#/components/schemas/CouponModel")
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/CouponModel")
     *      )
     * )
     *
     * @param int $id
     * @return JsonResponse
     */
    public function update(int $id)
    {
        if (!$this->request->user()->can('update', self::class)) {
            throw new ForbiddenError();
        }

        $coupon = $this->couponsService->update($id, $this->request->all());
        return $this->response($coupon);
    }

    /**
     * @OA\Get(
     *      path="/api/coupons/{couponId}",
     *      summary="Get coupon by ID",
     *      tags={"Coupons"},
     *      description="Get coupon by ID",
     *      operationId="getCouponById",
     *      security={
     *          {"JWT-Header": {}}
     *      },
     *      @OA\Parameter(
     *          description="ID of coupon",
     *          in="path",
     *          name="couponId",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/CouponModel")
     *      )
     * )
     *
     * @param int $id
     * @return JsonResponse
     */
    public function show(int $id)
    {
        if (!$this->request->user()->can('show', self::class)) {
            throw new ForbiddenError();
        }

        $coupon = $this->couponsService->show($id);
        return $this->response($coupon);
    }

    /**
     * @OA\Post(
     *      path="/api/coupons",
     *      summary="Create coupon",
     *      tags={"Coupons"},
     *      description="Create coupon",
     *      operationId="createCoupon",
     *      security={
     *          {"JWT-Header": {}}
     *      },
     *      @OA\RequestBody(
     *          required=true,
     *          description="Request body",
     *          @OA\JsonContent(ref="#/components/schemas/CouponModel")
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/CouponModel")
     *      )
     * )
     *
     * @return JsonResponse
     */
    public function store()
    {
        if (!$this->request->user()->can('update', self::class)) {
            throw new ForbiddenError();
        }

        $coupon = $this->couponsService->store($this->request->all());
        return $this->response($coupon);
    }

    /**
     * @OA\Delete(
     *      path="/api/coupons/{couponId}",
     *      summary="Delete coupon by ID",
     *      tags={"Coupons"},
     *      description="Delete coupon by ID",
     *      operationId="deleteCampaignById",
     *      security={
     *          {"JWT-Header": {}}
     *      },
     *      @OA\Parameter(
     *          description="ID of coupon",
     *          in="path",
     *          name="couponId",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="successful operation"
     *      )
     * )
     *
     * @param int $id
     * @return JsonResponse
     * @throws \Exception
     */
    public function destroy(int $id)
    {
        if (!$this->request->user()->can('update', self::class)) {
            throw new ForbiddenError();
        }

        $this->couponsService->destroy($id);
        return $this->response(["id" => $id]);
    }

    /**
     * @OA\Get(
     *      path="/api/coupons/{couponId}/usage",
     *      summary="Get coupon usage",
     *      tags={"Coupons"},
     *      description="Get coupon usage",
     *      operationId="getCouponUsageById",
     *      security={
     *          {"JWT-Header": {}}
     *      },
     *      @OA\Parameter(
     *          description="ID of coupon",
     *          in="path",
     *          name="couponId",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/StoreModel")
     *      )
     * )
     *
     * @param int $id
     * @return JsonResponse
     */
    public function usage(int $id)
    {
        if (!$this->request->user()->can('update', self::class)) {
            throw new ForbiddenError();
        }

        $stores = $this->couponsService->usage($id, $this->request->all());
        return $this->response($stores);
    }
}