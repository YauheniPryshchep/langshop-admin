<?php


namespace App\Http\Controllers\Api\Fullstory;

use App\Exceptions\Http\ForbiddenError;
use App\Exceptions\Http\NotFoundError;
use App\Http\Controllers\Api\ApiController;
use App\Models\Campaign;
use App\Services\FullstoryService;
use App\Services\MailgunStatsService;
use Dotenv\Exception\ValidationException;
use Exception;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Services\CampaignService;
use App\Exceptions\Http\HttpError;
use Symfony\Component\HttpFoundation\Response;

class FullstoryController extends ApiController
{

    /**
     * @var Request
     */
    private $request;

    /**
     * @var FullstoryService
     */
    protected $service;

    /**
     * FullstoryController constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->service = new FullstoryService();
    }

    /**
     * @OA\Get(
     *      path="/api/fullstory/{uid}/sessions",
     *      summary="Get fullstory user sesions",
     *      tags={"Fullstory"},
     *      description="Get fullstory sessions by userId",
     *      operationId="getUserSessions",
     *      security={
     *          {"JWT-Header": {}}
     *      },
     *      @OA\Parameter(
     *          description="UID of user",
     *          in="path",
     *          name="uid",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\JsonContent(
     *              @OA\Property(property="items", type="array",
     *                  @OA\Items(
     *                      @OA\Property(property="UserId", type="integer"),
     *                      @OA\Property(property="SessionId", type="integer"),
     *                      @OA\Property(property="CreatedTime", type="integer"),
     *                      @OA\Property(property="FsUrl", type="string")
     *                   ),
     *              )
     *          )
     *      )
     *  )
     *
     * @param Request $request
     * @param string $uid
     * @return JsonResponse|Response|ResponseFactory
     */
    public function sessions(Request $request, string $uid)
    {
//        if (!$request->user()->can('show', self::class)) {
//            throw new ForbiddenError();
//        }

        $sessions = $this->service->getUserSessions($uid);

        return $this->response($sessions);
    }
}
