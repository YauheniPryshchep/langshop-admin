<?php

namespace App\Http\Controllers\Api\Users;

use App\Exceptions\CustomException;
use App\Exceptions\Http\ForbiddenError;
use App\Exceptions\Http\NotFoundError;
use App\Http\Controllers\Api\ApiController;
use App\Models\Role;
use App\Models\User;
use App\Objects\SimplePaginationObject;
use App\Providers\UsersServiceProvider;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;
use Laravel\Lumen\Http\ResponseFactory;

class UsersController extends ApiController
{
    /**
     * @var UsersServiceProvider
     */
    private $usersServiceProvider;

    /**
     * UsersController constructor.
     * @param UsersServiceProvider $usersServiceProvider
     */
    public function __construct(UsersServiceProvider $usersServiceProvider)
    {
        $this->usersServiceProvider = $usersServiceProvider;
    }

    /**
     * @param Request $request
     * @return JsonResponse|Response|ResponseFactory
     */
    public function index(Request $request)
    {
        if (!$request->user()->can('show', self::class)) {
            throw new ForbiddenError();
        }

        $pagination = new SimplePaginationObject();
        $pagination->fillFromRequest($request, [
            'id',
            'name',
            'email',
            'created_at',
            'updated_at',
        ]);

        return $this->response($this->usersServiceProvider->getAll($pagination));
    }

    /**
     * @param Request $request
     * @param int $id
     * @return JsonResponse|Response|ResponseFactory
     */
    public function show(Request $request, int $id)
    {
        if (!$request->user()->can('show', self::class)) {
            throw new ForbiddenError();
        }

        $userInfo = $this->usersServiceProvider->getInfo($id);

        if ($userInfo->isEmpty()) {
           throw new NotFoundError();
        }

        return $this->response($userInfo->first());
    }

    /**
     * @param Request $request
     * @param int $id
     * @return JsonResponse|Response|ResponseFactory
     * @throws NotFoundError|ForbiddenError
     */
    public function assign(Request $request, int $id)
    {
        if (!$request->user()->can('update', self::class)) {
            throw new ForbiddenError();
        }

        /**
         * @var User $user
         */
        $user = User::query()
            ->find($id);

        if (is_null($user)) {
            throw new NotFoundError();
        }

        return $this->response($user);
    }


    /**
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     */
    public function unassign(Request $request, int $id)
    {
        if (!$request->user()->can('update', self::class)) {
            throw new ForbiddenError();
        }

        /**
         * @var User $user
         */
        $user = User::query()
            ->find($id);

        if (is_null($user)) {
            throw new NotFoundError();
        }

        return $this->response($user);
    }

    /**
     * @param Request $request
     * @param $id
     * @return JsonResponse|Response|ResponseFactory
     * @throws CustomException
     * @throws ValidationException
     */
    public function update(Request $request, $id)
    {
        if (!$request->user()->can('show', self::class)) {
            throw new ForbiddenError();
        }

        $this->validate($request,
            [
                'name'        => [
                    'required',
                    'max:255',
                ],
                'email'       => [
                    'required',
                    'email',
                    'max:255',
                    Rule::unique('users', 'email')->ignore($id),
                ],
                'newPassword' => [
                    'min:2',
                    'max:255',
                ],
                'roles'       => 'array',
            ]
        );

        /**
         * @var User $user
         */
        $user = User::query()
            ->find($id);

        if (is_null($user)) {
            throw new NotFoundError();
        }

        if ($user->isSAdmin() && !$request->user()->isSAdmin()) {
            throw new ForbiddenError();
        }

        $roles = $request->get('roles', []);
        if (count($roles) > 0) {
            $role = Role::find($roles[0]['id']);

            if (!is_null($role)) {
                if (!($role->name == 'sadmin' && !$request->user()->isSAdmin())) {
                    $user->roles()->sync([$role->id]);
                }
            }
        }

        $newPassword = $request->get('newPassword', '');
        if (!empty($newPassword)) {
            $user->password = app('hash')->make($newPassword);
            $user->save();
        }

        $user->update([
            'name'  => $request->get('name'),
            'email' => $request->get('email'),
        ]);

        return $this->response($user->load(['roles', 'scopes']));
    }
}
