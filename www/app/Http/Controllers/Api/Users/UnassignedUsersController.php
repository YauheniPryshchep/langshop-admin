<?php

namespace App\Http\Controllers\Api\Users;

use App\Exceptions\Http\ForbiddenError;
use App\Http\Controllers\Api\ApiController;
use App\Objects\SimplePaginationObject;
use App\Providers\UsersServiceProvider;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Laravel\Lumen\Http\ResponseFactory;

class UnassignedUsersController extends ApiController
{
    /**
     * @var UsersServiceProvider
     */
    private $usersServiceProvider;

    /**
     * UsersController constructor.
     * @param UsersServiceProvider $usersServiceProvider
     */
    public function __construct(UsersServiceProvider $usersServiceProvider)
    {
        $this->usersServiceProvider = $usersServiceProvider;
    }

    /**
     * @param Request $request
     * @return JsonResponse|Response|ResponseFactory
     */
    public function index(Request $request)
    {
        if (!$request->user()->can('update', self::class)) {
            throw new ForbiddenError();
        }

        $pagination = new SimplePaginationObject();
        $pagination->fillFromRequest($request, [
            'id',
            'name',
            'email',
            'created_at',
            'updated_at',
        ]);

        return $this->response( $this->usersServiceProvider->getAll($pagination, true));
    }
}
