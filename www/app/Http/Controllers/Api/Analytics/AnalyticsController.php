<?php

namespace App\Http\Controllers\Api\Analytics;

use App\Exceptions\Http\ForbiddenError;
use App\Http\Controllers\Api\ApiController;
use App\Services\Analytics\AnalyticsService;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class AnalyticsController extends ApiController
{
    /**
     * @var Request
     */
    private $request;
    /**
     *
     * @var AnalyticsService
     */
    private $analyticService;

    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->analyticService = new AnalyticsService();
    }

    /**
     * @return JsonResponse
     */
    public function plansQuantity()
    {
        if (!$this->request->user()->can('show', self::class)) {
            throw new ForbiddenError();
        }

        $date = $this->request->get('date');

        if (!$date) {
            $quantities = Cache::remember('plansQuantity', 360, function () {
                return $this->analyticService->plansQuantities();
            });


        } else {
            $quantities = Cache::remember('plansQuantityAgo', 360, function () use ($date) {
                return $this->analyticService->plansQuantitiesDaysAgo(Carbon::parse($date));
            });
        }

        return $this->response($quantities);
    }

}