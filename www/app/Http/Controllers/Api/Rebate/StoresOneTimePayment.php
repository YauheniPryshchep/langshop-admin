<?php

namespace App\Http\Controllers\Api\Rebate;

use App\Exceptions\Http\BadRequestError;
use App\Exceptions\Http\ForbiddenError;
use App\Http\Controllers\Api\ApiController;
use App\Models\StoreEvents;
use App\Providers\AppRequestServiceProvider;
use http\Exception\InvalidArgumentException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class StoresOneTimePayment extends ApiController
{
    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function store(Request $request)
    {
        if (!$request->user()->can('update', self::class)) {
            throw new ForbiddenError();
        }

        $attributes = $this->validate($request,
            [
                'shop'  => [
                    'required',
                    'max:255'
                ],
                'name'  => [
                    'required',
                    'string'
                ],
                'price' => [
                    'required',
                    'numeric',
                    'min:1',
                    'max:10000',
                ]
            ]
        );

        $response = app(AppRequestServiceProvider::class)
            ->charge($attributes['shop'], $request->only(['name', 'price']));

        if (!$response) {
            throw new BadRequestError();
        }

        $response = json_decode($response, true);

        StoreEvents::query()->create([
            "domain"     => $attributes['shop'],
            'type'       => StoreEvents::ONE_TIME_PAYMENT_CHARGE,
            "event_data" => ["charge_id" => $response['data']['id']],
            'timestamp'  => time()
        ]);

        return $response;
    }
}
