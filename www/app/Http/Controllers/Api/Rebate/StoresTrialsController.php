<?php

namespace App\Http\Controllers\Api\Rebate;

use App\Exceptions\CustomException;
use App\Exceptions\Http\ForbiddenError;
use App\Exceptions\Http\NotFoundError;
use App\Http\Controllers\Api\ApiController;
use App\Models\StoreTrial;
use App\Objects\SimplePaginationObject;
use App\Providers\AppRequestServiceProvider;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;
use Laravel\Lumen\Http\ResponseFactory;

class StoresTrialsController extends ApiController
{
    /**
     * @param Request $request
     * @return JsonResponse|Response|ResponseFactory
     */
    public function index(Request $request)
    {
        if (!$request->user()->can('show', self::class)) {
            throw new ForbiddenError(Response::HTTP_FORBIDDEN);
        }

        $pagination = new SimplePaginationObject();
        $pagination->fillFromRequest($request, [
            'name',
        ]);


        $count = StoreTrial::query()->where('name', 'like', '%' . $pagination->filter . '%')->count();

        $pages = ceil($count / $pagination->limit);
        if ($pagination->page > $pages) {
            $pagination->page = $pages;
        }

        $items = StoreTrial::query()->where('name', 'like', '%' . $pagination->filter . '%')
            ->orderBy($pagination->sort->field, $pagination->sort->direction)
            ->limit($pagination->limit)
            ->offset($pagination->limit * ($pagination->page - 1))
            ->get();


        return $this->response([
            'count'      => $count,
            'items'      => $items,
            'page'       => $pagination->page,
            'totalPages' => $pages
        ]);
    }

    /**
     * @param Request $request
     * @return JsonResponse|Response|ResponseFactory
     * @throws CustomException
     * @throws ValidationException
     */
    public function store(Request $request)
    {
        if (!$request->user()->can('update', self::class)) {
            throw new ForbiddenError();
        }

        $this->validate($request,
            [
                'name'  => [
                    'required',
                    'max:255',
                    Rule::unique("langshop.stores_trials", 'name'),
                ],
                'value' => [
                    'required',
                    'numeric',
                    'min:0',
                    'max:365',
                ]
            ]
        );

        $storeTrial = StoreTrial::query()
            ->create($request->only(['name', 'value']));

        app(AppRequestServiceProvider::class)
            ->changePlan($storeTrial->name);

        return $this->response($storeTrial);
    }

    /**
     * @param Request $request
     * @param string $domain
     * @return JsonResponse|Response|ResponseFactory
     * @throws CustomException
     * @throws Exception
     */
    public function destroy(Request $request, string $domain)
    {
        if (!$request->user()->can('update', self::class)) {
            throw new ForbiddenError();
        }

        /**
         * @var StoreTrial $storeTrial
         */
        $storeTrial = StoreTrial::query()->find($domain);

        if (is_null($storeTrial)) {
            throw new NotFoundError();
        }

        $storeTrial->delete();

        app(AppRequestServiceProvider::class)
            ->changePlan($storeTrial->name);

        return $this->response(['id' => $storeTrial->id]);
    }

    /**
     * @param Request $request
     * @param string $domain
     * @return JsonResponse|Response|ResponseFactory
     * @throws ValidationException
     */
    public function update(Request $request, string $domain)
    {
        if (!$request->user()->can('update', self::class)) {
            throw new ForbiddenError();
        }

        $this->validate($request,
            [
                'value' => [
                    'required',
                    'numeric',
                    'min:0',
                    'max:365',
                ]
            ]
        );

        $storeTrial = StoreTrial::query()->find($domain);

        if (is_null($storeTrial)) {
            throw new NotFoundError();
        }

        $storeTrial->update($request->only(['value']));

        app(AppRequestServiceProvider::class)
            ->changePlan($storeTrial->name);

        return $this->response($storeTrial);
    }
}
