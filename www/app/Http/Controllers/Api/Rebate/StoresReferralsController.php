<?php

namespace App\Http\Controllers\Api\Rebate;

use App\Exceptions\Http\BadRequestError;
use App\Exceptions\Http\ForbiddenError;
use App\Exceptions\Http\NotFoundError;
use App\Http\Controllers\Api\ApiController;
use App\Http\Controllers\Api\Partners\Users\UserController;
use App\Http\Controllers\Api\Stores\StoresController;
use App\Models\Partners\User;
use App\Models\ReferralStores;
use App\Models\Store;
use App\Models\StoreEvents;
use App\Providers\AppRequestServiceProvider;
use Carbon\Carbon;
use http\Exception\InvalidArgumentException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class StoresReferralsController extends ApiController
{
    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function store(Request $request)
    {
        if (!$request->user()->can('showStores', UserController::class)) {
            throw new ForbiddenError();
        }

        $attributes = $this->validate($request,
            [
                'shop'       => [
                    'required',
                    'max:255'
                ],
                'author'     => [
                    'required',
                    'string'
                ],
                'user_id'    => [
                    'required',
                ],
                'reason'     => [
                    'required',
                    'string'
                ],
                'created_at' => [
                    'required',
                ]
            ]
        );

        $user = User::query()
            ->where("id", $attributes['user_id'])
            ->with('userSettings')
            ->first();

        if (empty($user)) {
            throw new NotFoundError();
        }

        $store = Store::query()
            ->where('name', $attributes['shop'])
            ->first();


        if (empty($store)) {
            throw new NotFoundError();
        }

        $ref = ReferralStores::query()
            ->where('store_id', $store->id)
            ->where('status', ReferralStores::STATUS_ACCEPTED)
            ->first();

        if ($ref) {
            throw new BadRequestError(400, "Store already assign to another affiliate program");
        }


        $referral = ReferralStores::query()
            ->create([
                "store_id"   => $store->id,
                "ref"        => $user->settings['referral_link'],
                "status"     => ReferralStores::STATUS_ACCEPTED,
                "created_at" => Carbon::parse($attributes['created_at'])
            ]);

        $author = \App\Models\User::query()
            ->where('id', $attributes['author'])
            ->first();

        StoreEvents::query()->create([
            "domain"     => $attributes['shop'],
            'type'       => StoreEvents::ASSIGN_REFERRAL,
            "event_data" => [
                "author" => $author->name,
                "author_id" => $author->id,
                "reason" => $attributes['reason'],
            ],
            'timestamp'  => time()
        ]);

        return $this->response($referral);
    }
}
