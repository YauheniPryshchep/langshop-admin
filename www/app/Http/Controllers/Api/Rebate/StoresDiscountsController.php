<?php

namespace App\Http\Controllers\Api\Rebate;

use App\Exceptions\Http\ForbiddenError;
use App\Exceptions\Http\NotFoundError;
use App\Http\Controllers\Api\ApiController;
use App\Models\StoreDiscount;
use App\Objects\SimplePaginationObject;
use App\Providers\AppRequestServiceProvider;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;
use Laravel\Lumen\Http\ResponseFactory;

class StoresDiscountsController extends ApiController
{
    /**
     * @param Request $request
     * @return JsonResponse|Response|ResponseFactory
     */
    public function index(Request $request)
    {
        if (!$request->user()->can('show', self::class)) {
            throw new ForbiddenError();
        }

        $pagination = new SimplePaginationObject();
        $pagination->fillFromRequest($request, [
            'name',
        ]);
        $count = $this->getQuery($pagination)->count();
        $pages = ceil($count / $pagination->page);

        if($pagination->page > $pages){
            $pagination->page = $pages;
        }

        $items = $this->getQuery($pagination)
            ->orderBy($pagination->sort->field, $pagination->sort->direction)
            ->limit($pagination->limit)
            ->offset($pagination->limit * ($pagination->page - 1))
            ->get();

        return $this->response([
            'count'      => $count,
            'items'      => $items,
            'page'       => $pagination->page,
            'totalPages' => ceil($count / $pagination->limit)
        ]);
    }

    /**
     * @param Request $request
     * @return JsonResponse|Response|ResponseFactory
     * @throws ValidationException
     */
    public function store(Request $request)
    {
        if (!$request->user()->can('update', self::class)) {
            throw new ForbiddenError();
        }

        $this->validate($request,
            [
                'name'  => [
                    'required',
                    'max:255',
                    Rule::unique("langshop.stores_discounts", 'name'),
                ],
                'type'  => [
                    'required',
                    Rule::in(['fixed', 'percent']),
                ],
                'value' => [
                    'required',
                    'numeric',
                    'min:1',
                    'max:90',
                ]
            ]
        );

        $storeDiscount = StoreDiscount::query()
            ->create($request->only(['name', 'type', 'value']));

        app(AppRequestServiceProvider::class)
            ->changePlan($storeDiscount->name);

        return $this->response($storeDiscount);
    }

    /**
     * @param Request $request
     * @param string $domain
     * @return JsonResponse|Response|ResponseFactory
     * @throws Exception
     */
    public function destroy(Request $request, string $domain)
    {
        if (!$request->user()->can('update', self::class)) {
            throw new ForbiddenError();
        }

        /**
         * @var StoreDiscount $storeDiscount
         */
        $storeDiscount = StoreDiscount::query()
            ->find($domain);

        if (is_null($storeDiscount)) {
            throw new NotFoundError();
        }

        $storeDiscount->delete();

        app(AppRequestServiceProvider::class)
            ->changePlan($storeDiscount->name);

        return $this->response(['id' => $storeDiscount->id]);
    }

    /**
     * @param Request $request
     * @param string $domain
     * @return JsonResponse|Response|ResponseFactory
     * @throws ValidationException
     */
    public function update(Request $request, string $domain)
    {
        if (!$request->user()->can('update', self::class)) {
            throw new ForbiddenError();
        }
        $this->validate($request,
            [
                'type'  => [
                    'required',
                    Rule::in(['fixed', 'percent']),
                ],
                'value' => [
                    'required',
                    'numeric',
                    'min:1',
                    'max:90',
                ]
            ]
        );

        $storeDiscount = StoreDiscount::query()
            ->find($domain);

        if (is_null($storeDiscount)) {
            throw new NotFoundError();
        }

        $storeDiscount->update($request->only(['type', 'value']));

        app(AppRequestServiceProvider::class)
            ->changePlan($storeDiscount->name);

        return $this->response($storeDiscount);
    }

    /**
     * @param SimplePaginationObject $pagination
     * @return Builder
     */
    public function getQuery(SimplePaginationObject $pagination): Builder
    {
        return StoreDiscount::query()->where('name', 'like', '%' . $pagination->filter . '%');
    }

}
