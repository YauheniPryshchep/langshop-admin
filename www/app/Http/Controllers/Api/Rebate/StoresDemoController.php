<?php

namespace App\Http\Controllers\Api\Rebate;

use App\Exceptions\Http\ForbiddenError;
use App\Exceptions\Http\NotFoundError;
use App\Http\Controllers\Api\ApiController;
use App\Models\StoreDemo;
use App\Objects\SimplePaginationObject;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;
use Laravel\Lumen\Http\ResponseFactory;

class StoresDemoController extends ApiController
{
    /**
     * @param Request $request
     * @return JsonResponse|Response|ResponseFactory
     */
    public function index(Request $request)
    {
        if (!$request->user()->can('show', self::class)) {
            throw new ForbiddenError();
        }

        $pagination = new SimplePaginationObject();
        $pagination->fillFromRequest($request, [
            'name',
        ]);


        $count = $this->getQuery($pagination)->count();

        $pages = ceil($count / $pagination->limit);
        if ($pagination->page > $pages) {
            $pagination->page = $pages;
        }

        $items = $this->getQuery($pagination)
            ->orderBy($pagination->sort->field, $pagination->sort->direction)
            ->limit($pagination->limit)
            ->offset($pagination->limit * ($pagination->page - 1))
            ->get();

        return $this->response([
            'count'      => $count,
            'items'      => $items,
            'page'       => $pagination->page,
            'totalPages' => $pages
        ]);
    }

    /**
     * @param Request $request
     * @return JsonResponse|Response|ResponseFactory
     * @throws ValidationException
     */
    public function store(Request $request)
    {
        if (!$request->user()->can('update', self::class)) {
            throw new ForbiddenError();
        }

        $this->validate($request,
            [
                'name' => [
                    'required',
                    'max:255',
                    Rule::unique("langshop.stores_demo", 'name'),
                ]
            ]
        );

        $demoStore = StoreDemo::query()
            ->create($request->only(['name']));

        return $this->response($demoStore);
    }

    /**
     * @param Request $request
     * @param string $domain
     * @return JsonResponse|Response|ResponseFactory
     * @throws Exception
     */
    public function destroy(Request $request, string $domain)
    {
        if (!$request->user()->can('update', self::class)) {
            throw new ForbiddenError();
        }

        /**
         * @var StoreDemo $demoStore
         */
        $demoStore = StoreDemo::query()->find($domain);

        if (is_null($demoStore)) {
            throw new NotFoundError();
        }

        $demoStore->delete();

        return $this->response(['id' => $demoStore->id]);
    }

    /**
     * @param SimplePaginationObject $pagination
     * @return Builder
     */
    public function getQuery(SimplePaginationObject $pagination): Builder
    {
        return StoreDemo::query()->where('name', 'like', '%' . $pagination->filter . '%');
    }

}
