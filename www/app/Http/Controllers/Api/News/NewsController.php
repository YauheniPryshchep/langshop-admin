<?php

namespace App\Http\Controllers\Api\News;

use App\Exceptions\CustomException;
use App\Exceptions\Http\BadRequestError;
use App\Exceptions\Http\ForbiddenError;
use App\Exceptions\Http\NotFoundError;
use App\Http\Controllers\Api\ApiController;
use App\Models\NewsItem;
use App\Objects\SimplePaginationObject;
use App\Providers\NewsServiceProvider;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;
use Laravel\Lumen\Http\ResponseFactory;

class NewsController extends ApiController
{
    /**
     * @var NewsServiceProvider
     */
    private $newsServiceProvider;

    /**
     * NewsController constructor.
     * @param NewsServiceProvider $newsServiceProvider
     */
    public function __construct(NewsServiceProvider $newsServiceProvider)
    {
        $this->newsServiceProvider = $newsServiceProvider;
    }

    /**
     * @param Request $request
     * @return JsonResponse|Response|ResponseFactory
     */
    public function index(Request $request)
    {
        if (!$request->user()->can('show', self::class)) {
            return response(['Access denied.'], 403);
        }

        $pagination = new SimplePaginationObject();

        $pagination->fillFromRequest($request, [
            'id',
            'title',
            'type',
            'created_at',
            'updated_at',
            'position'
        ]);

        $appId = 'langshop' . $request->get('appSuffix', '');

        return $this->response(
            $this->newsServiceProvider->getAll($appId, $pagination)
        );
    }

    /**
     * @param Request $request
     * @param $id integer
     * @return JsonResponse|Response|ResponseFactory
     */
    public function show(Request $request, int $id)
    {
        if (!$request->user()->can('show', self::class)) {
            return response(['Access denied.'], 403);
        }

        $articleInfo = $this->newsServiceProvider->getInfo($id);

        if ($articleInfo->isEmpty()) {
            return response(['Article not found.'], 404);
        }

        return $this->response(
            $articleInfo->first()
        );
    }

    /**
     * @param Request $request
     * @return JsonResponse|Response|ResponseFactory
     * @throws ValidationException
     */
    public function store(Request $request)
    {

        if (!$request->user()->can('update', self::class)) {
            throw new ForbiddenError();
        }


        $data = $this->validate($request,
            [
                'title'         => [
                    'string',
                    'required',
                    'max:255',
                ],
                'description'   => [
                    'string',
                    'nullable',
                ],
                'primaryLink'   => [
                    'max:255',
                    'string',
                    'nullable',
                ],
                'primaryText'   => [
                    'max:255',
                    'string',
                    'nullable',
                ],
                'secondaryLink' => [
                    'max:255',
                    'string',
                    'nullable',
                ],
                'secondaryText' => [
                    'max:255',
                    'string',
                    'nullable',
                ],
                'src'           => [
                    'imageOrString',
                    'nullable',
                ],
                'video'         => [
                    'string',
                    'nullable',
                ],
                'type'          => [
                    'required',
                    'in:article,video'
                ],
                'layout'        => [
                    'required',
                    'in:top,left,right,bottom'
                ],
                'app'           => [
                    'required',
                    'string'
                ],
                'sticky'        => [
                    'boolean'
                ],
                'position'      => [
                    'min:1',
                    'integer'
                ]
            ]
        );


        $newsItem = NewsItem::query()
            ->create($data);

        if (empty($newsItem->id)) {
            throw new BadRequestError();
        }

        return $this->response($newsItem);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return JsonResponse|Response|ResponseFactory
     * @throws ValidationException
     */
    public function update(Request $request, int $id)
    {


        if (!$request->user()->can('update', self::class)) {
            throw new ForbiddenError();
        }


        $data = $this->validate($request,
            [
                'title'         => [
                    'string',
                    'required',
                    'max:255',
                ],
                'description'   => [
                    'string',
                    'nullable',
                ],
                'primaryLink'   => [
                    'max:255',
                    'string',
                    'nullable',
                ],
                'primaryText'   => [
                    'max:255',
                    'string',
                    'nullable',
                ],
                'secondaryLink' => [
                    'max:255',
                    'string',
                    'nullable',
                ],
                'secondaryText' => [
                    'max:255',
                    'string',
                    'nullable',
                ],
                'src'           => [
                    'imageOrString',
                    'nullable',
                ],
                'video'         => [
                    'string',
                    'nullable',
                ],
                'type'          => [
                    'required',
                    'in:article,video'
                ],
                'layout'        => [
                    'required',
                    'in:top,left,right,bottom'
                ],
                'app'           => [
                    'required',
                    'string'
                ],
                'sticky'        => [
                    'boolean'
                ],
                'position'      => [
                    'min:1',
                    'integer'
                ]
            ]
        );

        /**
         * @var NewsItem $newsItem
         */
        $newsItem = NewsItem::query()
            ->find($id);


        if (is_null($newsItem)) {
            throw new NotFoundError();
        }

        $newsItem->update($data);

        return $this->response($newsItem);
    }

    /**
     * @param Request $request
     * @param $id
     * @return JsonResponse|Response|ResponseFactory
     * @throws Exception
     */
    public function delete(Request $request, $id)
    {
        if (!$request->user()->can('update', self::class)) {
            throw new ForbiddenError();
        }

        /**
         * @var NewsItem $newsItem
         */
        $newsItem = NewsItem::query()
            ->find($id);

        if (is_null($newsItem)) {
            throw new NotFoundError();
        }

        $newsItem->delete();

        return $this->response(['id' => $newsItem->id]);
    }
}
