<?php

namespace App\Http\Controllers\Api\Stores;

use App\Exceptions\Http\ForbiddenError;
use App\Http\Controllers\Api\ApiController;
use App\Models\Store;
use App\Services\StoreSubscribeService;
use Illuminate\Http\JsonResponse;
use Laravel\Lumen\Http\Request;

class StoresSubscribeController extends ApiController
{
    /**
     * @var StoreSubscribeService
     */
    private $service;
    /**
     * @var Request
     */
    private $request;

    public function __construct(Request $request)
    {
        $this->service = new StoreSubscribeService();
        $this->request = $request;
    }

    /**
     * @param string $domain
     * @return JsonResponse
     */
    public function update(string $domain)
    {
        if (!$this->request->user()->can('show', Store::class)) {
            throw new ForbiddenError();
        }

        $subscribe = $this->service->update($this->request->all(), $domain);

        return $this->response($subscribe);
    }
}
