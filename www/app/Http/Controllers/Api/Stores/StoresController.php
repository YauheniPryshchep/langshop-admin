<?php

namespace App\Http\Controllers\Api\Stores;

use App\Exceptions\CustomException;
use App\Exceptions\Http\BadRequestError;
use App\Exceptions\Http\ForbiddenError;
use App\Exceptions\Http\NotFoundError;
use App\Http\Controllers\Api\ApiController;
use App\Http\Controllers\Api\Rebate\StoresDiscountsController;
use App\Models\Plan;
use App\Models\Store;
use App\Models\StoreDiscount;
use App\Models\StoreEvents;
use App\Models\User;
use App\Objects\SimplePaginationObject;
use App\Providers\AppRequestServiceProvider;
use App\Services\StoreService;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Query\JoinClause;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;

class StoresController extends ApiController
{

    /**
     * @var Request
     */
    protected $request;

    /**
     * @var StoreService
     */
    protected $storeReferralService;

    /**
     * ReferralController constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->storeReferralService = new StoreService();
    }

    /**
     * @return JsonResponse
     */
    public function index()
    {
        $stores = $this->storeReferralService->stores($this->request->all());
        return $this->response($stores);
    }

    /**
     * @param string $domain
     * @return JsonResponse
     * @throws NotFoundError
     */
    public function show(string $domain)
    {
        /**
         * @var Store $store
         */
        $store = Store::query()
            ->where('name', $domain)->first();

        if (is_null($store)) {
            throw new NotFoundError();
        }

        return $this->response($store);
    }

    /**
     * @param string $domain
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function changePlan(string $domain, Request $request)
    {


        if (!$request->user()->can('update', StoresDiscountsController::class)) {
            throw new ForbiddenError();
        }
        /**
         * @var Store $store
         */
        $store = Store::query()
            ->where('name', $domain)->first();

        if (is_null($store)) {
            throw new NotFoundError();
        }

        $attributes = $this->validate($request,
            [
                'planId' => [
                    'required',
                    Rule::in(
                        [
                            Plan::PLAN_FREE,
                        ]
                    ),
                ],
                "price"  => [
                    "nullable",
                    "integer"
                ],
                "note"   => [
                    'required',
                    "string",
                ]
            ]
        );

        if ($store->isFreePlan()) {
            throw new BadRequestError(400,
                "It is impossible to change the tariff to free, as this is the current tariff plan of the store");
        }

        app(AppRequestServiceProvider::class)
            ->changePlan($store->name, $attributes);


        $author = Auth::user();

        StoreEvents::query()->create([
            "domain"     => $store->name,
            'type'       => StoreEvents::CHANGE_PLAN,
            "event_data" => [
                "author"       => $author->name,
                "author_id"    => $author->id,
                "author_photo" => $author->photo,
                "reason"       => $attributes['note'],
                'planId'       => $attributes['planId']
            ],
            'timestamp'  => time()
        ]);

        return $this->response($store->refresh());
    }

    /**
     * @param string $domain
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function changePrice(string $domain, Request $request)
    {


        if (!$request->user()->can('update', StoresDiscountsController::class)) {
            throw new ForbiddenError();
        }
        /**
         * @var Store $store
         */
        $store = Store::query()
            ->where('name', $domain)->first();


        if (is_null($store)) {
            throw new NotFoundError();
        }

        $attributes = $this->validate($request,
            [
                'planId' => [
                    'required',
                    Rule::in(
                        [
                            Plan::PLAN_ENTERPRISE
                        ]
                    ),
                ],
                "price"  => [
                    "integer",
                    "min:399",
                    "max:1999"
                ],
                "note"   => [
                    'required',
                    "string",
                ]
            ]
        );

        if ($store->isEnterprisePlan()) {
            throw new BadRequestError(400,
                "It is not possible to change the price of an enterprise tariff plan, since the current plan is an enterprise");
        }

        app(AppRequestServiceProvider::class)
            ->changePlan($store->name, $attributes);

        $author = Auth::user();

        StoreEvents::query()->create([
            "domain"     => $store->name,
            'type'       => StoreEvents::CHANGE_ENTERPRISE_PRICE,
            "event_data" => [
                "author"       => $author->name,
                "author_id"    => $author->id,
                "author_photo" => $author->photo,
                "reason"       => $attributes['note'],
                'planId'       => $attributes['planId'],
                'price'        => $attributes['price']
            ],
            'timestamp'  => time()
        ]);


        return $this->response($store->refresh());
    }

}
