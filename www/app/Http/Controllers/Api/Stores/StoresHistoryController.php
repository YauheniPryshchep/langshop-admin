<?php

namespace App\Http\Controllers\Api\Stores;

use App\Exceptions\Http\ForbiddenError;
use App\Http\Controllers\Api\ApiController;
use App\Models\StoreHistory;
use App\Objects\SimplePaginationObject;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Laravel\Lumen\Http\ResponseFactory;

class StoresHistoryController extends ApiController
{
    /**
     * @param Request $request
     * @param string $domain
     * @return JsonResponse|Response|ResponseFactory
     */
    public function index(Request $request, string $domain)
    {
        if (!$request->user()->can('show', self::class)) {
            throw new ForbiddenError();
        }

        $pagination = new SimplePaginationObject();
        $pagination->fillFromRequest($request);

        $count = StoreHistory::query()
            ->where('source', 'internal')
            ->whereHas(
                'store',
                function (Builder $builder) use ($domain) {
                    return $builder->where('name', $domain);
                }
            )
            ->count();

        $pages = ceil($count / $pagination->limit);
        if ($pagination->page > $pages) {
            $pagination->page = $pages;
        }

        $items = StoreHistory::query()
            ->where('source', 'internal')
            ->whereHas(
                'store',
                function (Builder $builder) use ($domain) {
                    return $builder->where('name', $domain);
                }
            )
            ->orderBy('timestamp', 'desc')
            ->limit($pagination->limit)
            ->offset($pagination->limit * ($pagination->page - 1))
            ->get();

        return $this->response([
            'count'      => $count,
            'items'      => $items,
            'page'       => $pagination->page,
            'totalPages' => $pages
        ]);
    }
}
