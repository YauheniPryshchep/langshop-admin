<?php

namespace App\Http\Controllers\Api\Stores;

use App\Exceptions\Http\ForbiddenError;
use App\Http\Controllers\Api\ApiController;
use App\Models\StoreCharge;
use App\Objects\SimplePaginationObject;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Laravel\Lumen\Http\ResponseFactory;

class StoresChargesController extends ApiController
{
    /**
     * @param Request $request
     * @param string $domain
     * @return JsonResponse|Response|ResponseFactory
     */
    public function index(Request $request, string $domain)
    {
        if (!$request->user()->can('show', self::class)) {
            throw new ForbiddenError();
        }

        $pagination = new SimplePaginationObject();
        $pagination->fillFromRequest($request);

        $count = StoreCharge::query()
            ->where('type', 2)
            ->whereHas(
                'store',
                function (Builder $builder) use ($domain) {
                    return $builder
                        ->where('name', $domain);
                }
            )
            ->count();

        $pages = ceil($count / $pagination->limit);
        if ($pagination->page > $pages) {
            $pagination->page = $pages;
        }

        $items = StoreCharge::query()
            ->where('type', 2)
            ->whereHas(
                'store',
                function (Builder $builder) use ($domain) {
                    return $builder->where('name', $domain);
                }
            )
            ->get();

        return $this->response([
            'count'      => $count,
            'items'      => $items,
            'page'       => $pagination->page,
            'totalPages' => $pages
        ]);
    }
}
