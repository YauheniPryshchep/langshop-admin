<?php

namespace App\Http\Controllers\Auth;

use App\Services\Partners\SystemPartnersService;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;

class AuthController extends Controller
{
    /**
     * The request instance.
     *
     * @var Request
     */
    private $request;

    /**
     * @var SystemPartnersService
     */
    private $systemPartnersService;

    /**
     * Create a new controller instance.
     *
     * @param Request $request
     * @throws Exception
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->systemPartnersService = new SystemPartnersService();

    }


    /**
     * @return JsonResponse
     * @throws ValidationException
     */
    public function login()
    {
        $this->validate($this->request, [
            'email'    => 'required',
            'password' => 'required',
        ]);


        if (!$token = Auth::attempt($this->request->only(['email', 'password']))) {
            return response()->json(['Invalid credentials'], 401);
        }

        return $this->respondWithToken($token);
    }

    /**
     * @param int $id
     * @return RedirectResponse
     */
    public function loginToPartners(int $id)
    {
        $token = $this->systemPartnersService->login($id);
        return redirect()->to($this->getLoginPartnersUrl($token));
    }


    /**
     * @return JsonResponse
     */
    public function refresh()
    {
        try {
            return $this->respondWithToken(Auth::guard('api')->refresh());
        } catch (TokenExpiredException $e) {
            return response()->json(['Unauthorized'], 401);
        } catch (Exception $e) {
            return response()->json(['Unauthorized'], 401);
        }
    }

    public function logout()
    {
        return Auth::guard('api')->logout();
    }

    /**
     * Get the token array structure.
     *
     * @param string $token
     *
     * @return JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'token' => $token
        ]);
    }

    /**
     * @param string $token
     * @return string
     */
    private function getLoginPartnersUrl(string $token)
    {
        return config('partners.url') . '/verify/sign-in/admin?token=' . $token;
    }
}
