<?php

namespace App\Http\Controllers\System\StoreEvents;

use App\Http\Controllers\Api\ApiController;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class StoreEventsController extends ApiController
{

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        return $this->response([]);
    }
}