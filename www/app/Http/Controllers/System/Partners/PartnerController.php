<?php

namespace App\Http\Controllers\System\Partners;

use App\Services\Partners\SystemPartnersService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class PartnerController extends Controller
{
    /**
     * @var Request
     */
    private $request;
    /**
     * @var SystemPartnersService
     */
    private $systemPartnersService;

    /**
     * PartnerController constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->systemPartnersService = new SystemPartnersService();
    }


    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function verify(Request $request)
    {
        $verify = $this->systemPartnersService->verify($request->all());
        return response()->json($verify);
    }

}