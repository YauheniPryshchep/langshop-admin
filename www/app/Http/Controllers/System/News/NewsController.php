<?php

namespace App\Http\Controllers\System\News;

use App\Models\NewsItem;
use App\Objects\SimplePaginationObject;
use App\Providers\NewsServiceProvider;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class NewsController extends Controller
{
    /**
     * @var NewsServiceProvider
     */
    private $newsServiceProvider;

    /**
     * NewsController constructor.
     */
    public function __construct()
    {
        $this->newsServiceProvider = app(NewsServiceProvider::class);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request)
    {
        $pagination = new SimplePaginationObject();
        $pagination->fillFromRequest($request, [
            'id',
            'title',
            'type',
            'app',
            'created_at',
            'updated_at',
        ]);

        $pagination = new SimplePaginationObject();
        $pagination->fillFromRequest($request, [
            'id',
            'title',
            'type',
            'app',
            'created_at',
            'updated_at',
        ]);

        $appId = $request->get('app');

        $count = NewsItem::query()
            ->where('app', $appId)
            ->count();

        $pages = ceil($count / $pagination->limit);
        if ($pagination->page > $pages) {
            $page = $pages;
        } else {
            $page = $pagination->page;
        }

        $limit  = $pagination->limit;
        $offset = $limit * ($page - 1);

        $items = NewsItem::query()
            ->where('app', $appId)
            ->orderBy('sticky', 'desc')
            ->orderBy('position', 'asc')
            ->offset($offset)
            ->limit($limit)
            ->get();

        return response()->json(compact('count', 'items'));
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function count(Request $request)
    {
        $appId   = $request->get('app');
        $sinceId = $request->get('since_id');

        $query = NewsItem::query()
            ->where('app', $appId)
            ->select(DB::raw("MAX(id) as lastId, COUNT(*) as count"));

        if ($sinceId) {
            $query->where('id', '>', $sinceId);
        }

        return response()->json($query->first());
    }
}