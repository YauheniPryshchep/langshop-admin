<?php

namespace App\Http\Controllers\System\Mailgun;

use App\Http\Controllers\Api\ApiController;
use App\Models\CampaignDomain;
use App\Models\CampaignUnsubscribeList;
use App\Models\Lead;
use App\Models\LeadEmail;
use App\Models\NewsItem;
use App\Models\StoreEvents;
use App\Objects\SimplePaginationObject;
use App\Providers\NewsServiceProvider;
use App\Services\MailgunWebhookService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class MailgunController extends ApiController
{

    /**
     * @param Request $request
     */
    public function unsubscribe(Request $request)
    {

        $mailgunWebhookService = new MailgunWebhookService();

        $domains = $mailgunWebhookService->getDomains('andrey.yeryomin@devit-team.com');

        foreach ($domains as $domain) {

            $campaignSubscribe = CampaignUnsubscribeList::query()
                ->where('domain', '=', $domain)
                ->first();

            if ($campaignSubscribe) {
                continue;
            }

            $subscribe = new CampaignUnsubscribeList([
                'domain' => $domain,
                'status' => 0
            ]);

            $event = new StoreEvents([
                'domain'     => $domain,
                'event_data' => null,
                'type'       => StoreEvents::UNSUBSCRIBED_FROM_MAILING,
                'timestamp'  => time()
            ]);

            $event->save();
            $subscribe->save();
        }

    }
}