<?php

namespace App\Http\Middleware;

use Closure;
use Exception;
use Illuminate\Http\Request;

class AppAuthMiddleware
{
    /**
     * @var string
     */
    private $validApiKey;

    /**
     * @var Request
     */
    private $request;

    public function __construct()
    {
        $this->validApiKey = env('APPS_AUTH_TOKEN', '##asdae114_d;a,19!!314134__13jk,aa.SSav321##');
    }

    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     * @throws Exception;
     */
    public function handle($request, Closure $next)
    {
        $this->request = $request;
        if ($this->apiKeyExistCheck()) {
            return response(['Go fuck from here'], 401);
        }
        $content = $next($request);

        return $content;
    }

    /**
     * @throws Exception
     */
    private function apiKeyExistCheck()
    {
        $this->apiKey = $this->request->header('Api-Key', null);
        return (empty($this->apiKey) || $this->apiKey != $this->validApiKey);
    }
}
