<?php

namespace App\Http\Middleware;

use App\Exceptions\Http\ForbiddenError;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class MailgunMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $params = $request->all();

        $token = Arr::get($params, 'signature.token');
        $timestamp = Arr::get($params, 'signature.timestamp');
        $signature = Arr::get($params, 'signature.signature');

        if($this->verify('' , $token, $timestamp, $signature)){
            throw new ForbiddenError();
        }

        $next($request);
    }

    private function verify($signingKey, $token, $timestamp, $signature)
    {
        if (\abs(\time() - $timestamp) > 15) {
            return false;
        }

        return \hash_equals(\hash_hmac('sha256', $timestamp . $token, $signingKey), $signature);
    }
}