<?php

namespace App\Http\Middleware;

use App\Http\Controllers\Api\Analytics\AnalyticsController;
use App\Http\Controllers\Api\Campaigns\CampaignsController;
use App\Http\Controllers\Api\Coupons\CouponsController;
use App\Http\Controllers\Api\Customers\CustomersController;
use App\Http\Controllers\Api\Partners\Payouts\PayoutsController;
use App\Http\Controllers\Api\Partners\Users\UserController;
use App\Http\Controllers\Api\Rebate\StoresDemoController;
use App\Http\Controllers\Api\Rebate\StoresDiscountsController;
use App\Http\Controllers\Api\Rebate\StoresOneTimePayment;
use App\Http\Controllers\Api\Stores\StoresChargesController;
use App\Http\Controllers\Api\Stores\StoresHistoryController;
use App\Http\Controllers\Api\Rebate\StoresTrialsController;
use App\Http\Controllers\Api\News\NewsController;
use App\Http\Controllers\Api\Locales\LocalesController;
use App\Http\Controllers\Api\Locales\LocaleController;
use App\Http\Controllers\Api\Users\UnassignedUsersController;
use App\Http\Controllers\Api\Roles\RolesController;
use App\Http\Controllers\Api\Scopes\ScopesController;
use App\Http\Controllers\Api\Users\UsersController;
use App\Http\Controllers\Api\Vocabularies\VocabulariesController;
use App\Http\Controllers\Api\Workflows\WorkflowsController;
use App\Models\User;
use App\Policies\AnalyticsPolicy;
use App\Policies\CampaignsPolicy;
use App\Policies\CouponsPolicy;
use App\Policies\CustomersPolicy;
use App\Policies\PartnerPayoutsPolicy;
use App\Policies\PartnersUserPolicy;
use App\Policies\StoresChargePolicy;
use App\Policies\StoresDemoPolicy;
use App\Policies\StoresDiscountsPolicy;
use App\Policies\StoresHistoryPolicy;
use App\Policies\StoresOneTimePaymentPolicy;
use App\Policies\StoresTrialsPolicy;
use App\Policies\NewsPolicy;
use App\Policies\RolesPolicy;
use App\Policies\ScopesPolicy;
use App\Policies\UsersPolicy;
use App\Policies\LocalesPolicy;
use App\Policies\VocabulariesPolicy;
use App\Policies\WorkflowsPolicy;
use Closure;
use Illuminate\Contracts\Auth\Factory as Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

/**
 * @OA\SecurityScheme(
 *   securityScheme="JWT-Header",
 *   in="header",
 *   name="bearerAuth",
 *   type="http",
 *   scheme="bearer",
 *   bearerFormat="JWT",
 * )
 */
class Authenticate
{
    /**
     * The authentication guard factory instance.
     *
     * @var Auth
     */
    protected $auth;

    /**
     * Create a new middleware instance.
     *
     * @param Auth $auth
     * @return void
     */
    public function __construct(Auth $auth)
    {
        Gate::policy(StoresDemoController::class, StoresDemoPolicy::class);
        Gate::policy(StoresDiscountsController::class, StoresDiscountsPolicy::class);
        Gate::policy(StoresTrialsController::class, StoresTrialsPolicy::class);
        Gate::policy(StoresHistoryController::class, StoresHistoryPolicy::class);
        Gate::policy(StoresOneTimePayment::class, StoresOneTimePaymentPolicy::class);
        Gate::policy(StoresChargesController::class, StoresChargePolicy::class);

        Gate::policy(UsersController::class, UsersPolicy::class);
        Gate::policy(UnassignedUsersController::class, UsersPolicy::class);
        Gate::policy(RolesController::class, RolesPolicy::class);
        Gate::policy(ScopesController::class, ScopesPolicy::class);

        Gate::policy(NewsController::class, NewsPolicy::class);

        Gate::policy(LocalesController::class, LocalesPolicy::class);
        Gate::policy(LocaleController::class, LocalesPolicy::class);

        Gate::policy(CampaignsController::class, CampaignsPolicy::class);

        Gate::policy(VocabulariesController::class, VocabulariesPolicy::class);

        Gate::policy(CouponsController::class, CouponsPolicy::class);

        Gate::policy(CustomersController::class, CustomersPolicy::class);
        Gate::policy(UserController::class, PartnersUserPolicy::class);
        Gate::policy(PayoutsController::class, PartnerPayoutsPolicy::class);
        Gate::policy(WorkflowsController::class, WorkflowsPolicy::class);
        Gate::policy(AnalyticsController::class, AnalyticsPolicy::class);

        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @param string|null $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if ($this->auth->guard($guard)->guest()) {
            return response(['Unauthorized.'], 401);
        }

        return $next($request);
    }
}
