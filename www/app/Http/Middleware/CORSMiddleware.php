<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CORSMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $origin = $request->header('Origin', '*');

        if ($request->method() !== "OPTIONS") {
            $content = $next($request);
        } else {
            $content = new Response('OK', 200);
        }

        $content->headers->set('Access-Control-Allow-Origin', $origin, true);
        $content->headers->set('Access-Control-Allow-Headers', 'Accept,Authorization,X-Authorization,DNT,X-CustomHeader,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Content-Range,Range', true);
        $content->headers->set('Access-Control-Allow-Methods', 'PUT,GET,POST,DELETE,OPTIONS', true);
        $content->headers->set('Access-Control-Max-Age', '1728000', true);
        $content->headers->set('Allow', 'PUT,GET,POST,DELETE,OPTIONS', true);

        return $content;
    }
}