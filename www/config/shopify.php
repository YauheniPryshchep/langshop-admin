<?php

return [
    "partner_id"     => env("SHOPIFY_PARTNER_ID", 0),
    "partner_secret" => env("SHOPIFY_PARTNER_SECRET", ''),
];
