<?php

return [
    'langshop' => [
        'endpoint' => env('LANGSHOP_MAILGUN_ENDPOINT', 'api.mailgun.net'),
        'domain' => env('LANGSHOP_MAILGUN_DOMAIN'),
        'secret' => env('LANGSHOP_MAILGUN_SECRET'),
        'from' => env('LANGSHOP_MAILGUN_FROM'),
        'reply-to' => env('LANGSHOP_MAILGUN_REPLY_TO')
    ],
    'buildify' => [
        'endpoint' => env('BUILDIFY_MAILGUN_ENDPOINT', 'api.mailgun.net'),
        'domain' => env('BUILDIFY_MAILGUN_DOMAIN'),
        'secret' => env('BUILDIFY_MAILGUN_SECRET'),
        'from' => env('BUILDIFY_MAILGUN_FROM'),
        'reply-to' => env('BUILDIFY_MAILGUN_REPLY_TO')
    ],
    'default' => [
        'endpoint' => env('DEFAULT_MAILGUN_ENDPOINT', 'api.mailgun.net'),
        'domain' => env('DEFAULT_MAILGUN_DOMAIN', 'devit-team.com'),
        'url' => env('DEFAULT_MAILGUN_API_URL', 'https://api.mailgun.net/v3/'),
        'secret' => env('DEFAULT_MAILGUN_SECRET'),
        'sending_key' => env('DEFAULT_MAILGUN_SENDING_KEY'),
        'from' => env('DEFAULT_MAILGUN_FROM'),
        'reply-to' => env('DEFAULT_MAILGUN_REPLY_TO')
    ]
];

