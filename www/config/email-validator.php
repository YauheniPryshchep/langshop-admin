<?php

return [
    "apiKey"     => env("EMAIL_VALIDATE_API", null),
    "batchLimit" => env("EMAIL_VALIDATE_LIMIT_PER_REQUEST", 10),
];