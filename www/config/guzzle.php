<?php

return [
    /**
     * Describes the SSL certificate verification behavior of a request.
     */
    'verify'          => true,

    /**
     * Float describing the timeout of the request in seconds. Use 0 to wait indefinitely (the default behavior).
     */
    'timeout'         => 300,

    /**
     * Float describing the number of seconds to wait while trying to connect to a server. Use 0 to wait indefinitely (the default behavior).
     */
    'connect_timeout' => 3,

    /**
     * Array of available proxies to use in guzzle requests
     */
    'available_proxies'   => !empty(env('GUZZLE_LOCAL_PROXIES'))
        ? array_reduce(
            explode(',', env('GUZZLE_LOCAL_PROXIES')),
            function (array $carry, string $item) {
                $item = trim($item);

                if (empty($item)) {
                    return $carry;
                }

                $carry[] = $item;

                return $carry;
            },
            []
        )
        : []
];
