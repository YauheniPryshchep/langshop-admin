<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default filesystem disk that should be used
    | by the framework. The "local" disk, as well as a variety of cloud
    | based disks are available to your application. Just store away!
    |
    */

    'default' => env('FILESYSTEM_DRIVER', 'local'),

    /*
    |--------------------------------------------------------------------------
    | Default Cloud Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Many applications store files both locally and in the cloud. For this
    | reason, you may specify a default "cloud" driver here. This driver
    | will be bound as the Cloud disk implementation in the container.
    |
    */

    'cloud' => env('FILESYSTEM_CLOUD', 's3'),

    /*
    |--------------------------------------------------------------------------
    | Filesystem Disks
    |--------------------------------------------------------------------------
    |
    | Here you may configure as many filesystem "disks" as you wish, and you
    | may even configure multiple disks of the same driver. Defaults have
    | been setup for each driver as an example of the required options.
    |
    | Supported Drivers: "local", "ftp", "sftp", "s3", "rackspace"
    |
    */

    'disks' => [

        'local' => [
            'driver' => 'local',
            'root' => storage_path('app'),
        ],

        'public' => [
            'driver' => 'local',
            'root' => base_path('public/api/storage'),
            'url' => env('APP_URL').'/storage',
            'visibility' => 'public',
        ],

        'git' => [
            'driver' => 'local',
            'root' => storage_path('git')
        ],

        's3Assets' => [
            'driver' => 's3',
            'key' => env('S3_ASSETS_ACCESS_KEY_ID'),
            'secret' => env('S3_ASSETS_SECRET_ACCESS_KEY'),
            'region' => env('S3_ASSETS_DEFAULT_REGION'),
            'bucket' => env('S3_ASSETS_BUCKET'),
            'url' => env('S3_ASSETS_URL'),
        ],

    ],

];