<?php

return [
    'default_locale' => 'en',
    'git'            => [
        'folder' => 'www/v2/resources/lang',
    ]
];
