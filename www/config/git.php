<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Logging
    |--------------------------------------------------------------------------
    |
    | Add Laravel's default logger to the GitWrapper instance. You may
    | also specify a channel to use, or a channel stack.
    |
    */

    'logger' => false,

    /*
    |--------------------------------------------------------------------------
    | Default Connection Name
    |--------------------------------------------------------------------------
    |
    | Here you may specify which of the connections below you wish to use as
    | your default connection for all work. Of course, you may use many
    | connections at once using the manager class.
    |
    */

    'default' => 'langshop',

    /*
    |--------------------------------------------------------------------------
    | Git Connections
    |--------------------------------------------------------------------------
    |
    | Here are each of the connections setup for your application. Example
    | configuration has been included, but you may add as many
    | connections as you would like. Note that the 2 supported
    | authentication methods are: "ssh_key", and "none".
    |
    */

    'connections' => [
        'langshop' => [
            'auth'       => 'ssh_key',
            'key_path'   => storage_path('ssh/id_langshop_rsa'),
            'path'       => storage_path('git/langshop'),
            'repository' => 'git@gitlab.com:devit.team/shopify/langshop/LangShop.git',
            'branch'     => 'localization',
        ],
    ],
];
