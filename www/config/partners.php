<?php

return [
    "url"              => env("PARTNERS_URL", 'http://127.0.0.1:8138'),
    "partners_cdn_url" => env("PARTNERS_CDN_PHOTO_URL", ""),
    "langshop_app_id"  => env("LANGSHOP_SHOPIFY_ID", ""),
    'secret_key'       => env("APPS_AUTH_TOKEN", "")
];