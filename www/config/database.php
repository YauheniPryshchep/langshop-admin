<?php

use Illuminate\Support\Str;

return [
    'default'     => env('DB_CONNECTION', 'dashboard'),
    'migrations'  => 'migrations',
    'connections' => [
        'dashboard' => [
            'driver'    => 'mysql',
            'host'      => env('DB_HOST'),
            'port'      => env('DB_PORT', 3306),
            'database'  => env('DB_DATABASE'),
            'username'  => env('DB_USERNAME'),
            'password'  => env('DB_PASSWORD'),
            'charset'   => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
        ],
        'langshop'  => [
            'driver'    => 'mysql',
            'host'      => env('LANGSHOP_DB_HOST'),
            'port'      => env('LANGSHOP_DB_PORT', 3306),
            'database'  => env('LANGSHOP_DB_DATABASE'),
            'username'  => env('LANGSHOP_DB_USERNAME'),
            'password'  => env('LANGSHOP_DB_PASSWORD'),
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
        ],
        'partners'  => [
            'driver'    => 'mysql',
            'host'      => env('PARTNERS_DB_HOST'),
            'port'      => env('PARTNERS_DB_PORT', 3306),
            'database'  => env('PARTNERS_DB_DATABASE'),
            'username'  => env('PARTNERS_DB_USERNAME'),
            'password'  => env('PARTNERS_DB_PASSWORD'),
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
        ],
        'mongodb'   => [
            'driver'    => 'mongodb',
            'host'      => env('LANGSHOP_MONGO_DB_HOST'),
            'port'      => env('LANGSHOP_MONGO_DB_PORT', 27017),
            'database'  => env('LANGHOP_MONGO_DB_DATABASE'),
            'username'  => env('LANGHOP_MONGO_DB_USERNAME'),
            'password'  => env('LANGHOP_MONGO_DB_PASSWORD'),
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
        ],
    ],

    'redis' => [

        'client' => env('REDIS_CLIENT', 'phpredis'),

        'options' => [
            'cluster' => env('REDIS_CLUSTER', 'redis'),
            'prefix'  => env('REDIS_PREFIX', Str::slug(env('APP_NAME', 'laravel'), '_') . '_database_'),
        ],

        'default' => [
            'url'      => env('REDIS_URL'),
            'host'     => env('REDIS_HOST', '127.0.0.1'),
            'password' => env('REDIS_PASSWORD', null),
            'port'     => env('REDIS_PORT', '6379'),
            'database' => env('REDIS_DB', '0'),
        ],

        'cache' => [
            'url'      => env('REDIS_URL'),
            'host'     => env('REDIS_HOST', '127.0.0.1'),
            'password' => env('REDIS_PASSWORD', null),
            'port'     => env('REDIS_PORT', '6379'),
            'database' => env('REDIS_CACHE_DB', '1'),
        ],
    ]
];
