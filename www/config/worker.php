<?php

return [
    'leads_worker_execute_interval' => env('INTERVAL_LEADS_WORKER_EXECUTE'),
    'leads_search_execute_delay'    => env('INTERVAL_LEADS_SEARCH_EXECUTE')
];
