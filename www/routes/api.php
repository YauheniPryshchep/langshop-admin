<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It is a breeze. Simply tell Lumen the URIs it should respond to
  | and give it the Closure to call when that URI is requested.
  |
 */

use Illuminate\Support\Facades\Route;

require_once __DIR__ . '/api/profile.php';
require_once __DIR__ . '/api/roles.php';
require_once __DIR__ . '/api/customers.php';
require_once __DIR__ . '/api/scopes.php';
require_once __DIR__ . '/api/assets.php';
require_once __DIR__ . '/api/news.php';
require_once __DIR__ . '/api/users.php';
require_once __DIR__ . '/api/stores.php';
require_once __DIR__ . '/api/rebate.php';
require_once __DIR__ . '/api/coupon.php';
require_once __DIR__ . '/api/locales.php';
require_once __DIR__ . '/api/campaigns.php';
require_once __DIR__ . '/api/store-events.php';
require_once __DIR__ . '/api/vocabularies.php';
require_once __DIR__ . '/api/fullstory.php';
require_once __DIR__ . '/api/partners.php';
require_once __DIR__ . '/api/analytics.php';

/**
 * base route
 */
Route::get('/', 'HomeController@index');
