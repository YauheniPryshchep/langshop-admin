<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It is a breeze. Simply tell Lumen the URIs it should respond to
  | and give it the Closure to call when that URI is requested.
  |
 */

use Illuminate\Support\Facades\Route;

Route::group([
    'prefix'    => 'partners',
    'namespace' => 'Partners',
], function () {

    Route::group([
        'prefix'    => 'users',
        'namespace' => 'Users',
    ], function () {
        Route::get('/', 'UserController@index');
        Route::get('/{id}', 'UserController@show');
        Route::get('/{id}/stores', 'UserController@stores');
        Route::get('/{id}/payouts', 'UserController@payouts');
        Route::post('/{id}/commissions', 'UserController@commissions');
    });

    Route::group([
        'prefix'    => 'transactions',
        'namespace' => 'Transactions',
    ], function () {
        Route::get('/', 'TransactionController@index');
        Route::get('/{id}', 'TransactionController@show');
    });

    Route::group([
        'prefix'    => 'payouts',
        'namespace' => 'Payouts',
    ], function () {
        Route::get('/', 'PayoutsController@index');
        Route::get('/{id}', 'PayoutsController@show');
        Route::put('/{id}', 'PayoutsController@update');
    });
});