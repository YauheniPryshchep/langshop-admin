<?php

use Illuminate\Support\Facades\Route;

Route::group([
    'prefix'    => 'rebate',
    'namespace' => 'Rebate',
], function () {
    Route::get('/demo-stores', 'StoresDemoController@index');
    Route::post('/demo-stores', 'StoresDemoController@store');
    Route::delete('/demo-stores/{domain}', 'StoresDemoController@destroy');

    Route::get('/stores-discounts', 'StoresDiscountsController@index');
    Route::post('/stores-discounts', 'StoresDiscountsController@store');
    Route::delete('/stores-discounts/{domain}', 'StoresDiscountsController@destroy');
    Route::put('/stores-discounts/{domain}', 'StoresDiscountsController@update');

    Route::get('/stores-trials', 'StoresTrialsController@index');
    Route::post('/stores-trials', 'StoresTrialsController@store');
    Route::delete('/stores-trials/{domain}', 'StoresTrialsController@destroy');
    Route::put('/stores-trials/{domain}', 'StoresTrialsController@update');

    Route::post('/stores-one-time-payment', "StoresOneTimePayment@store");

    Route::post('/assign-referral', "StoresReferralsController@store");
});