<?php

use Illuminate\Support\Facades\Route;

Route::group([
    'prefix'    => 'campaigns',
    'namespace' => 'Campaigns',
], function () {

    Route::group([
        'prefix'    => 'templates',
        'namespace' => 'Templates',
    ], function () {
        Route::get('/', 'TemplatesController@index');
        Route::post('/{name}/test-email', 'TemplatesController@testEmail');
        Route::post('/', 'TemplatesController@store');
        Route::get('/{name}', 'TemplatesController@show');
        Route::put('/{name}', 'TemplatesController@update');
        Route::delete('/{name}', 'TemplatesController@destroy');
    });

    Route::group([
        'prefix'    => 'mailing-lists',
        'namespace' => 'MailingLists',
    ], function () {
        Route::get('/', 'MailingListsController@index');
        Route::post('/', 'MailingListsController@store');
        Route::get('/{address}', 'MailingListsController@show');
        Route::put('/{address}', 'MailingListsController@update');
        Route::delete('/{address}', 'MailingListsController@destroy');
    });

    Route::get('/recipients', 'RecipientsController@index');

    Route::group([
        'prefix' => 'campaign-domains'
    ], function () {
        Route::get('/', 'CampaignDomainsController@index');
        Route::post('/', 'CampaignDomainsController@store');
        Route::delete('/{id}', 'CampaignDomainsController@destroy');
    });

    Route::group([
        'prefix' => 'recipients-filter'
    ], function () {

        Route::post('/', 'RecipientsFilterController@store');
        Route::get('/{id}', 'RecipientsFilterController@show');
        Route::put('/{id}', 'RecipientsFilterController@update');
        Route::delete('/{id}', 'RecipientsFilterController@destroy');
    });


    Route::get('/', 'CampaignsController@index');
    Route::post('/', 'CampaignsController@store');
    Route::get('/{id}', 'CampaignsController@show');
    Route::put('/{id}', 'CampaignsController@update');
    Route::delete('/{id}', 'CampaignsController@destroy');
    Route::get('/{id}/stats', 'CampaignsController@stats');
});
