<?php

use Illuminate\Support\Facades\Route;

Route::group([
    'prefix'    => 'roles',
    'namespace' => 'Roles',
], function () {
    Route::post('/', 'RolesController@store');
    Route::get('/', 'RolesController@index');
    Route::get('/{id}', 'RolesController@show');
    Route::put('/{id}', 'RolesController@update');
    Route::delete('/{id}', 'RolesController@destroy');
});