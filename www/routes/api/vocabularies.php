<?php

use Illuminate\Support\Facades\Route;

Route::group([
    'prefix'    => 'vocabularies',
    'namespace' => 'Vocabularies',

], function () {
    Route::get('/', 'VocabulariesController@index');
    Route::post('/', 'VocabulariesController@store');
    Route::put('/{id}', 'VocabulariesController@update');
    Route::delete('/{id}', 'VocabulariesController@delete');
    Route::get('/directions', 'VocabulariesController@directions');
    Route::patch('/directions/delete', 'VocabulariesController@deleteDirections');
});