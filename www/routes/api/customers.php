<?php

Route::group([
    'prefix'    => 'customers',
    'namespace' => 'Customers',
], function () {
    Route::get('/', 'CustomersController@index');
    Route::get('/{id}', 'CustomersController@show');
});