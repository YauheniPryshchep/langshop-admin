<?php

use Illuminate\Support\Facades\Route;

Route::group([
    'prefix'    => 'stores',
    'namespace' => 'Stores',
], function () {
    Route::get('/', 'StoresController@index');
    Route::get('/{domain}', 'StoresController@show');
    Route::post('/{domain}/plan', 'StoresController@changePlan');
    Route::post('/{domain}/price', 'StoresController@changePrice');

    Route::get('/{domain}/history', 'StoresHistoryController@index');
    Route::get('/{domain}/charges', 'StoresChargesController@index');
    Route::put('/{domain}/subscribe', 'StoresSubscribeController@update');
});