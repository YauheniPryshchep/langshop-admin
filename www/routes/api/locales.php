<?php

use Illuminate\Support\Facades\Route;

Route::group([
    'prefix'    => 'locales',
    'namespace' => 'Locales',
], function () {
    Route::get('/', 'LocalesController@index');
    Route::get('/{iso}', 'LocaleController@index');
    Route::get('/{iso}/{file}', 'LocaleController@show');
    Route::put('/{iso}/{file}', 'LocaleController@update');
});