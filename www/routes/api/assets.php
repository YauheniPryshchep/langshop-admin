<?php

use Illuminate\Support\Facades\Route;

Route::group([
    'prefix'    => 'assets',
    'namespace' => 'Assets',
], function () {
    Route::get('/', 'AssetsController@index');
});