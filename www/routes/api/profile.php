<?php

use Illuminate\Support\Facades\Route;

Route::group([
    'prefix'    => 'profile',
    'namespace' => 'Profile',
], function () {
    Route::get('/', 'ProfileController@show');
});

