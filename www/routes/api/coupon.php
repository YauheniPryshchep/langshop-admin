<?php

use Illuminate\Support\Facades\Route;

Route::group([
    'prefix'    => 'coupons',
    'namespace' => 'Coupons'
], function () {
    Route::get('/', 'CouponsController@index');
    Route::put('/{id}', 'CouponsController@update');
    Route::get('/{id}', 'CouponsController@show');
    Route::get('/{id}/usage', 'CouponsController@usage');
    Route::post('/', 'CouponsController@store');
    Route::delete('/{id}', 'CouponsController@destroy');
});