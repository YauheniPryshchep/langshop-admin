<?php

use Illuminate\Support\Facades\Route;

Route::group([
    'prefix'    => 'scopes',
    'namespace' => 'Scopes',
], function () {
    Route::get('/', 'ScopesController@index');
    Route::get('/{id}', 'ScopesController@show');
});