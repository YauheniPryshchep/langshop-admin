<?php

use Illuminate\Support\Facades\Route;

Route::group([
    'prefix'    => 'fullstory',
    'namespace' => 'Fullstory',

], function () {
    Route::get('/{uid}/sessions', 'FullstoryController@sessions');
});