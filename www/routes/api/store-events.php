<?php

use Illuminate\Support\Facades\Route;

Route::group([
    'prefix'    => 'store-events',
    'namespace' => 'StoreEvents'
], function () {
    Route::get('/{domain}', 'StoreEventsController@show');
});