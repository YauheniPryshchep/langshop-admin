<?php

use Illuminate\Support\Facades\Route;

Route::group([
    'prefix'    => 'users',
    'namespace' => 'Users',
], function () {
    Route::get('/unassigned', 'UnassignedUsersController@index');

    Route::get('/', 'UsersController@index');
    Route::get('/{id}', 'UsersController@show');
    Route::put('/{id}', 'UsersController@update');
    Route::post('/{id}', 'UsersController@assign');
    Route::delete('/{id}', 'UsersController@unassign');
});