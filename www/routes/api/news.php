<?php

use Illuminate\Support\Facades\Route;

Route::group([
    'prefix'    => 'news',
    'namespace' => 'News',
], function () {
    Route::get('/', 'NewsController@index');
    Route::post('/', 'NewsController@store');
    Route::get('/{id}', 'NewsController@show');
    Route::post('/{id}', 'NewsController@update');
    Route::put('/{id}', 'NewsController@update');
    Route::delete('/{id}', 'NewsController@delete');
});