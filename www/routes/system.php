<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

use Illuminate\Support\Facades\Route;

Route::group([
    'prefix'    => 'news',
    'namespace' => 'News'
], function () {
    Route::get('/', 'NewsController@index');
    Route::get('/count', 'NewsController@count');
});

Route::group([
    'prefix'    => 'partners',
    'namespace' => 'Partners'
], function () {
    Route::get('/verify', 'PartnerController@verify');
});

Route::group([
    'prefix'    => 'web-hook',
    'namespace' => "StoreEvents"
], function () {
    Route::post('/events', 'StoreEventsController@store');
});

/**
 * base route
 */
Route::get('/', 'HomeController@index');