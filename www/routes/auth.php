<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

use Illuminate\Support\Facades\Route;

Route::post('/login', [
    'as'   => 'api.jwt.login',
    'uses' => 'AuthController@login'
]);

Route::get('/redirect/login', [
    'as'   => 'api.jwt.login',
    'uses' => 'AuthController@redirect'
]);

Route::get('/refresh', [
    'as'   => 'api.jwt.refresh',
    'uses' => 'AuthController@refresh'
]);

Route::get('/login-partners/{id}', 'AuthController@loginToPartners');


Route::get('/logout', [
    'as'   => 'api.jwt.logout',
    'uses' => 'AuthController@logout'
]);