<?php

use Adldap\Laravel\AdldapAuthServiceProvider;
use Adldap\Laravel\AdldapServiceProvider;
use App\Http\Middleware\AppAuthMiddleware;
use App\Http\Middleware\Authenticate;
use App\Http\Middleware\CORSMiddleware;
use App\Http\Middleware\MailgunMiddleware;
use App\Providers\AppServiceProvider;
use App\Providers\AuthServiceProvider;
use App\Providers\EventServiceProvider;
use App\Providers\RememberableQueryServiceProvider;
use App\Providers\ValidationServiceProvider;
use CupOfTea\GitWrapper\GitWrapperServiceProvider;
use KeycloakGuard\KeycloakGuardServiceProvider;
use SwaggerLume\ServiceProvider;
use Tymon\JWTAuth\Providers\LumenServiceProvider;

require_once __DIR__ . '/../vendor/autoload.php';

(new Laravel\Lumen\Bootstrap\LoadEnvironmentVariables(
    dirname(__DIR__)
))->bootstrap();

/*
  |--------------------------------------------------------------------------
  | Create The Application
  |--------------------------------------------------------------------------
  |
  | Here we will load the environment and create the application instance
  | that serves as the central piece of this framework. We'll use this
  | application as an "IoC" container and router for this framework.
  |
 */

$app = new Laravel\Lumen\Application(
    realpath(__DIR__ . '/../')
);

$app->withFacades();

/*
  |--------------------------------------------------------------------------
  | Register Service Providers
  |--------------------------------------------------------------------------
  |
  | Here we will register all of the application's service providers which
  | are used to bind services into the container. Service providers are
  | totally optional, so you are not required to uncomment this line.
  |
 */


$app->register(Jenssegers\Mongodb\MongodbServiceProvider::class);

if ($app->environment() !== 'production') {
    $app->register(ServiceProvider::class);
}

$app->withEloquent();

$app->register(AppServiceProvider::class);
$app->register(EventServiceProvider::class);
$app->register(AuthServiceProvider::class);

$app->register(LumenServiceProvider::class);
$app->register(AdldapServiceProvider::class);
$app->register(AdldapAuthServiceProvider::class);
$app->register(GitWrapperServiceProvider::class);
$app->register(ValidationServiceProvider::class);
$app->register(RememberableQueryServiceProvider::class);
$app->register(Illuminate\Redis\RedisServiceProvider::class);

$app->configure('filesystems');
$app->configure('queue');
$app->configure('git');
$app->configure('localization');
$app->configure('swagger-lume');
$app->configure('mailgun');
$app->configure('worker');
$app->configure('database');
$app->configure('fullstory');
$app->configure('partners');
$app->configure('guzzle');
$app->configure('shopify');
$app->configure('cache');
$app->configure('email-validator');

/*
  |--------------------------------------------------------------------------
  | Register Container Bindings
  |--------------------------------------------------------------------------
  |
  | Now we will register a few bindings in the service container. We will
  | register the exception handler and the console kernel. You may add
  | your own bindings here if you like or you can make another file.
  |
 */

$app->singleton(
    Illuminate\Contracts\Debug\ExceptionHandler::class,
    App\Exceptions\Handler::class
);

$app->singleton(
    Illuminate\Contracts\Console\Kernel::class,
    App\Console\Kernel::class
);

$app->singleton('filesystem', function ($app) {
    return $app->loadComponent(
        'filesystems',
        Illuminate\Filesystem\FilesystemServiceProvider::class,
        'filesystem'
    );
});

/*
  |--------------------------------------------------------------------------
  | Register Middleware
  |--------------------------------------------------------------------------
  |
  | Next, we will register the middleware with the application. These can
  | be global middleware that run before and after each request into a
  | route or middleware that'll be assigned to some specific routes.
  |
 */

$app->middleware([
    CORSMiddleware::class,
]);

$app->routeMiddleware([
    'auth'    => Authenticate::class,
    'system'  => AppAuthMiddleware::class,
    'mailgun' => MailgunMiddleware::class
]);

/*
  |--------------------------------------------------------------------------
  | Load The Application Routes
  |--------------------------------------------------------------------------
  |
  | Next we will include the routes file so that they can all be added to
  | the application. This will provide all of the URLs the application
  | can respond to, as well as the controllers that may handle them.
  |
 */

$app->router->group([
    'namespace'  => 'App\Http\Controllers\Api',
    'middleware' => 'auth'
], function () {
    require __DIR__ . '/../routes/api.php';
});

$app->router->group([
    'prefix'     => 'system',
    'namespace'  => 'App\Http\Controllers\System',
    'middleware' => 'system'
], function () {
    require __DIR__ . '/../routes/system.php';
});

$app->router->group([
    'prefix'    => 'auth',
    'namespace' => 'App\Http\Controllers\Auth',
], function () {
    require __DIR__ . '/../routes/auth.php';
});

$app->router->group([
    'prefix'     => 'mailgun',
    'middleware' => 'mailgun',
    'namespace'  => 'App\Http\Controllers\System\Mailgun',
], function () {
    require __DIR__ . '/../routes/mailgun.php';
});

return $app;
