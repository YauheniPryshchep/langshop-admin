<?php

return [
    "header"   => "Translation memories",
    "resource" => "translation memory|translation memories",
    "empty"    => [
        "title"       => "No translation memory found",
        "description" => "Add your translation memory"
    ],
    "fields"   => [
        "original" => "Source",
        "translated" => "Target",
    ],
    "messages" => [
        "updated"                        => "Blog successfully updated",
        "created"                        => "Blog successfully created",
        "deleted"                        => "Blog successfully deleted",
        "translating_started"            => "Translating blog content has been launched",
        "removing_translations_started"  => "Removing blog translations has been launched",
        "translating_is_already_running" => "Translating store blogs to :languages is already running",
        "exporting_started"              => "Exporting blog content translations has been launched",
        "exporting_is_already_running"   => "Exporting blogs translations for :languages is already running",
        "importing_started"              => "Importing blog content translations has been launched",
        "importing_is_already_running"   => "Importing blogs translations to :language is already running",
    ]
];