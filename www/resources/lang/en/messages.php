<?php

return [
    "loaded"  => "Successfully loaded",
    "created" => "Successfully created",
    "updated" => "Successfully updated",
    "deleted" => "Successfully deleted",
];