<?php
return [
    "messages" => [
        "created" => "Campaign successfully created!",
        "updated" => "Campaign successfully updated!",
        "deleted" => "Campaign successfully deleted!"
    ]
];