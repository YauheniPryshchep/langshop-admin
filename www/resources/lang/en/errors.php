<?php

use App\Exceptions\Contracts\ApplicationException;
use Symfony\Component\HttpFoundation\Response;

return [
    Response::HTTP_BAD_REQUEST => "Bad request",
    Response::HTTP_FORBIDDEN => "Forbidden",
    Response::HTTP_TOO_MANY_REQUESTS => "Too many requests",
    Response::HTTP_UNPROCESSABLE_ENTITY => "Incorrect data",
    Response::HTTP_NOT_FOUND => "Not found",
    Response::HTTP_UNAUTHORIZED => "Unauthorized",
    Response::HTTP_PAYMENT_REQUIRED => "Payment Required",
    Response::HTTP_INTERNAL_SERVER_ERROR => "Internal server error",
    Response::HTTP_CONFLICT => "Resource already exist",
    ApplicationException::CODE_TOKEN_NOT_PROVIDED => "Token not provided",
    ApplicationException::CODE_TOKEN_IS_EXPIRED => "Provided token is expired",
    ApplicationException::CODE_TOKEN_IS_INVALID => "Provided token is invalid",
    ApplicationException::CODE_INVALID_CREDENTIALS => "Invalid credentials",
    ApplicationException::CODE_INVALID_ROUTE => "Not found",
    ApplicationException::CODE_STORE_IS_FROZEN => "The requested shop is currently frozen. The shop owner needs to log in to the shop's admin and pay the outstanding balance to unfreeze the shop",
    ApplicationException::CODE_INCORRECT_REQUEST_DATA => "Incorrect request data",
    ApplicationException::CODE_UNHANDLED_EXCEPTION => "Internal server error occurred. Try to repeat your action or report to application support",
   ];