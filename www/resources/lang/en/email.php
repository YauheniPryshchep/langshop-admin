<?php

return [
    'test_subject' => 'Test email',
    'test_body' => 'This is test email',
    'successfully_sent' => 'Email successfully sent'
];

