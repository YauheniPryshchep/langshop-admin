<!doctype html>
<html>
<head>
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>[Partners LangShop] Withdraw request</title>
    <style media="all" type="text/css">
        @media only screen and (max-width: 460px) {
            table[class=body] h1,
            table[class=body] h2,
            table[class=body] h3,
            table[class=body] h4 {
                font-weight: 600 !important;
            }

            table[class=body] h1 {
                font-size: 22px !important;
            }

            table[class=body] h2 {
                font-size: 18px !important;
            }

            table[class=body] h3 {
                font-size: 16px !important;
            }

            table[class=body] .content,
            table[class=body] .wrapper {
                padding: 20px !important;
            }

            table[class=body] .container {
                padding: 0 !important;
                width: 100% !important;
            }

            table[class=body] .btn table,
            table[class=body] .btn a {
                width: 100% !important;
            }
        }
    </style>
</head>

<body
        style="margin: 0; font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; height: 100% !important; line-height: 1.6em; -webkit-font-smoothing: antialiased; padding: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; width: 100% !important; background-color: #f6f6f6;">

<table class="body"
       style="box-sizing: border-box; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; background-color: #f6f6f6;"
       width="100%" bgcolor="#f6f6f6">
    <tr>
        <td style="box-sizing: border-box; font-family: 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif; font-size: 14px; vertical-align: top; padding: 0;"
            valign="top"></td>
        <td class="container"
            style="box-sizing: border-box; font-family: 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif; font-size: 14px; vertical-align: top; padding: 10px; display: block; Margin: 0 auto !important; max-width: 580px; width: 580px;"
            width="580" valign="top">
            <div class="content"
                 style="box-sizing: border-box; display: block; margin: 0 auto; max-width: 580px; padding: 10px;">
                <span class="preheader"
                      style="color: transparent; display: none; height: 0; max-height: 0; max-width: 0; opacity: 0; overflow: hidden; mso-hide: all; visibility: hidden; width: 0;">Withdraw request accepted</span>

                <table class="main"
                       style="box-sizing: border-box; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; background: #ffffff; border: 0;"
                       width="100%">
                    <tr>
                        <td style="box-sizing: border-box; font-family: 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif; font-size: 14px; vertical-align: top; padding: 0;"
                            valign="top">
                            <table class="header"
                                   style="box-sizing: border-box; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; background: #007b5c;"
                                   width="100%">
                                <tbody>
                                <tr>
                                    <td class="wrapper"
                                        style="box-sizing: border-box; font-family: 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif; font-size: 14px; vertical-align: top; padding: 40px;"
                                        valign="top">
                                        <table
                                                style="box-sizing: border-box; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;"
                                                width="100%">
                                            <tbody>
                                            <tr>
                                                <td style="box-sizing: border-box; font-family: 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif; font-size: 14px; vertical-align: top; padding: 0;"
                                                    valign="top">
                                                    <table
                                                            style="box-sizing: border-box; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;"
                                                            width="100%">
                                                        <tr>
                                                            <td class="width-half"
                                                                style="box-sizing: border-box; font-family: 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif; font-size: 14px; vertical-align: top; padding: 0; width: 50%;"
                                                                width="50%" valign="top">
                                                                <table align="left"
                                                                       style="box-sizing: border-box; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;"
                                                                       width="100%">
                                                                    <tbody>
                                                                    <tr>
                                                                        <td align="left"
                                                                            style="box-sizing: border-box; font-family: 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif; font-size: 14px; vertical-align: top; padding: 0;"
                                                                            valign="top">
                                                                            <a href="<?php echo htmlspecialchars(config('application.client_url'), ENT_QUOTES, false); ?>"
                                                                               style="box-sizing: border-box; color: #fff; text-decoration: none; font-weight: bold;">
                                                                                <img class="header-application"
                                                                                     src="<?php echo htmlspecialchars(config('partners.url') . '/assets/logo/langshop.svg', ENT_QUOTES, false); ?>"
                                                                                     alt="Application Logo"
                                                                                     style="-ms-interpolation-mode: bicubic; max-width: 100%; height: 45px; width: auto;">
                                                                            </a>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td height="22"
                                                                            style="box-sizing: border-box; font-family: 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif; font-size: 14px; vertical-align: top; padding: 0;"
                                                                            valign="top"></td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>


                                            <tr>
                                                <td height="60"
                                                    style="box-sizing: border-box; font-family: 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif; font-size: 14px; vertical-align: top; padding: 0;"
                                                    valign="top"></td>
                                            </tr>
                                            <tr>
                                                <td height="40"
                                                    style="box-sizing: border-box; font-family: 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif; font-size: 14px; vertical-align: top; padding: 0;"
                                                    valign="top"></td>
                                            </tr>

                                            <tr>
                                                <td style="box-sizing: border-box; font-family: 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif; font-size: 14px; vertical-align: top; padding: 0;"
                                                    valign="top">
                                                    <h1 class="align-center header-title"
                                                        style="margin-bottom: 25px; line-height: 1.4em; margin: 0; font-family: 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif; margin-top: 5px; text-transform: capitalize; text-align: center; font-size: 30px; color: #ffffff; font-weight: 700; mso-line-height-rule: exactly;">
                                                        Withdraw Request is accepted
                                                    </h1>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td style="box-sizing: border-box; font-family: 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif; font-size: 14px; vertical-align: top; padding: 0;"
                                                    valign="top">
                                                    <h5 class="align-center"
                                                        style="text-align: center; margin: 0; color: #ffffff; font-size: 14px; font-weight: 400; mso-line-height-rule: exactly;">
                                                        <?php echo date("j M, Y"); ?>
                                                    </h5>
                                                </td>
                                            </tr>

                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="50"
                                        style="box-sizing: border-box; font-family: 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif; font-size: 14px; vertical-align: top; padding: 0;"
                                        valign="top"></td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="wrapper"
                            style="box-sizing: border-box; font-family: 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif; font-size: 14px; vertical-align: top; padding: 40px;"
                            valign="top">
                            <table
                                    style="box-sizing: border-box; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;"
                                    width="100%">
                                <tr>
                                    <td style="box-sizing: border-box; font-family: 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif; font-size: 14px; vertical-align: top; padding: 0;"
                                        valign="top">

                                        <h1 class="align-center"
                                            style="color: #111111; font-family: 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif; font-weight: 300; line-height: 1.4em; margin: 0; margin-bottom: 25px; margin-top: 5px; font-size: 38px; text-transform: capitalize; text-align: center;">
                                            <?php echo $userName; ?>,</h1>
                                        <p style="font-family: 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px; padding: 0;">
                                            Your request to withdraw money is accepted</p>
                                        <p style="font-family: 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px; padding: 0;">
                                            We have sent money to your PayPal wallet (<?php echo $paypal; ?>)</p>

                                        <table class="btn btn-primary" cellpadding="0" cellspacing="0"
                                               style="box-sizing: border-box; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;"
                                               width="100%">
                                            <tr>
                                                <td align="center"
                                                    style="box-sizing: border-box; font-family: 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif; font-size: 14px; vertical-align: top; padding: 0; padding-bottom: 15px;"
                                                    valign="top">
                                                    <table cellpadding="0" cellspacing="0"
                                                           style="box-sizing: border-box; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: auto;">
                                                        <tr>
                                                            <td style="box-sizing: border-box; font-family: 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif; font-size: 14px; vertical-align: top; padding: 0; background-color: #007b5c; border-radius: 5px; text-align: center;"
                                                                valign="top" bgcolor="#007b5c" align="center">
                                                                <a href="<?php echo htmlspecialchars($payoutLink, ENT_QUOTES, false); ?>"
                                                                   style="box-sizing: border-box; border-color: #007b5c; text-decoration: none; background-color: #007b5c; border: solid 1px #007b5c; border-radius: 5px; cursor: pointer; color: #ffffff; font-size: 14px; font-weight: bold; margin: 0; padding: 12px 25px; text-transform: capitalize; display: inline-block;">Payout
                                                                    Link</a>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                        <p style="font-family: 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px; padding: 0;">
                                            Button not working? Paste this into your browser.</p>

                                        <p style="font-family: 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px; padding: 0;">
                                            <?php echo $payoutLink; ?>
                                        </p>

                                        <p class="last"
                                           style="font-family: 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif; font-size: 14px; font-weight: normal; margin: 0; padding: 0; margin-bottom: 0;">
                                            With the best wishes,<br>
                                            LangShop Team.
                                        </p></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
        </td>
        <td style="box-sizing: border-box; font-family: 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif; font-size: 14px; vertical-align: top; padding: 0;"
            valign="top"></td>
    </tr>
</table>

</body>
</html>
