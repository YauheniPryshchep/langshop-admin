import React from "react";
import ReactDOM from "react-dom";
import store from "@store";
import App from "./src";
import { Provider } from "react-redux";
import { ToastContainer } from "react-toastify";
import "./styles.scss";

ReactDOM.render(
  <Provider store={store}>
    <App />
    <ToastContainer />
  </Provider>,
  document.getElementById("root")
);
