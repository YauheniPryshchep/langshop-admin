import { fetchVocabulariesAction, resetVocabulariesAction, updateVocabulariesActions } from "./actions";

import { get } from "lodash";
import { handleActions } from "redux-actions";

const defaultState = {
  isFetched: false,
  isLoading: false,
  items: [],
  total: 0,
};

const loadingStartHandler = state => {
  return {
    ...state,
    isLoading: true,
  };
};

const loadingEndHandler = state => {
  return {
    ...state,
    isFetched: true,
    isLoading: false,
  };
};

const fetchVocabulariesSuccessHandler = (state, { payload }) => {
  const data = get(payload, "data.data");

  return {
    ...state,
    items: data.items,
    total: data.count,
    isFetched: true,
    isLoading: false,
  };
};

const updateVocabulariesHandler = (state, { payload }) => {
  const data = get(payload, "data.data");
  return {
    ...state,
    items: state.items.map(i => {
      // eslint-disable-next-line no-underscore-dangle
      if (i._id === data._id) {
        return { ...data };
      }
      return { ...i };
    }),
  };
};

const resetVocabulariesHandler = () => {
  return defaultState;
};

export const vocabularies = handleActions(
  {
    [fetchVocabulariesAction]: loadingStartHandler,
    [fetchVocabulariesAction.success]: fetchVocabulariesSuccessHandler,
    [fetchVocabulariesAction.fail]: loadingEndHandler,
    [resetVocabulariesAction]: resetVocabulariesHandler,
    [updateVocabulariesActions.success]: updateVocabulariesHandler,
  },
  defaultState
);
