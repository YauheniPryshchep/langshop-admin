import { createRequestAction } from "@store/createRequestAction";
import { toQueryString } from "../recipients";
import { createAction } from "redux-actions";

export const VOCABULARIES_FETCH = "VOCABULARIES_FETCH";
export const VOCABULARIES_RESET = "VOCABULARIES_RESET";

export const fetchVocabulariesAction = createRequestAction(VOCABULARIES_FETCH, (from, to, params, cancelToken) => {
  return {
    request: {
      method: "GET",
      url: `/api/vocabularies?${toQueryString({
        filters: [
          ...params["filters"],
          {
            key: "from",
            comparator: "equal",
            value: from,
          },

          {
            key: "to",
            comparator: "equal",
            value: to,
          },
        ],
      })}`,
      params: {
        limit: params["limit"] || 25,
        filter: params["filter"] || null,
        page: params["page"] || 1,
      },
      cancelToken,
    },
  };
});

export const createVocabulariesActions = createRequestAction("CREATE" + VOCABULARIES_FETCH, (data, cancelToken) => {
  return {
    request: {
      method: "POST",
      url: `/api/vocabularies`,
      data,
      cancelToken,
    },
  };
});

export const updateVocabulariesActions = createRequestAction("UPDATE" + VOCABULARIES_FETCH, (id, data) => {
  return {
    request: {
      method: "PUT",
      url: `/api/vocabularies/${id}`,
      data,
    },
  };
});

export const deleteVocabulariesActions = createRequestAction("REMOVE" + VOCABULARIES_FETCH, id => {
  return {
    request: {
      method: "DELETE",
      url: `/api/vocabularies/${id}`,
    },
  };
});

export const deleteDirectionActions = createRequestAction("REMOVE_DIRECTION" + VOCABULARIES_FETCH, data => {
  return {
    request: {
      method: "PATCH",
      url: `/api/vocabularies/directions/delete`,
      data,
    },
  };
});

export const resetVocabulariesAction = createAction(VOCABULARIES_RESET);
