import { createRequestAction } from "@store/createRequestAction";
import { createAction } from "redux-actions";

export const COUPONS_FETCH = "COUPONS_FETCH";
export const COUPONS_RESET = "COUPONS__RESET";

export const fetchCouponsAction = createRequestAction(COUPONS_FETCH, (params, cancelToken) => {
  return {
    request: {
      method: "GET",
      url: `/api/coupons`,
      params,
      cancelToken,
    },
  };
});

export const resetCouponsAction = createAction(COUPONS_RESET);
