import {createSelector} from "reselect";

import {get} from "lodash";

const baseState = state => get(state, "coupons", null);

export const isFetched = createSelector(baseState, state => get(state, "isFetched", false));

export const isLoading = createSelector(baseState, state => get(state, "isLoading", false));

export const isLoaded = createSelector(isLoading, isFetched, (isLoading, isFetched) => !isLoading && isFetched);

export const total = createSelector(baseState, state => get(state, "total", 0));
export const page = createSelector(baseState, state => get(state, "page", 0));
export const pages = createSelector(baseState, state => get(state, "pages", 0));

export const coupons = createSelector(baseState, state => get(state, "coupons", []));
