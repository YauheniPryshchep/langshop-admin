import {fetchCouponsAction, resetCouponsAction} from "./actions";
import {get} from "lodash";
import {handleActions} from "redux-actions";

const defaultState = {
  isFetched: false,
  isLoading: false,
  coupons: [],
  total: 0,
  page: 0,
  pages: 0,
};

const loadingStartHandler = state => {
  return {
    ...state,
    isLoading: true,
  };
};

const loadingEndHandler = state => {
  return {
    ...state,
    isFetched: true,
    isLoading: false,
  };
};

const fetchCouponsSuccessHandler = (state, {payload}) => {
  const data = get(payload, "data.data");

  return {
    ...state,
    coupons: data.items,
    total: data.count,
    page: data.page,
    pages: data.pages,
    isFetched: true,
    isLoading: false,
  };
};

const resetCouponsHandler = () => {
  return defaultState;
};

export const coupons = handleActions(
  {
    [fetchCouponsAction]: loadingStartHandler,
    [fetchCouponsAction.success]: fetchCouponsSuccessHandler,
    [fetchCouponsAction.fail]: loadingEndHandler,

    [resetCouponsAction]: resetCouponsHandler,
  },
  defaultState
);
