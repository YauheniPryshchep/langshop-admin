import {appTopBarSearch} from "@store/topbar-search/reducer";
import {users} from "@store/users/reducer";
import {user} from "@store/user/reducer";
import {roles} from "@store/roles/reducer";
import {role} from "@store/role/reducer";
import {scopes} from "@store/scopes/reducer";
import {profile} from "@store/profile/reducer";
import {news} from "@store/news/reducer";
import {locales} from "@store/locales/reducer";
import {locale} from "@store/locale/reducer";
import {localeFile} from "@store/locale-file/reducer";
import {auth} from "@store/auth/reducer";
import {stores} from "@store/stores/reducer";
import {store} from "@store/store/reducer";
import {storesTrials} from "@store/stores-trials/reducer";
import {storeTrial} from "@store/store-trial/reducer";
import {storesDiscounts} from "@store/stores-discounts/reducer";
import {storeDiscount} from "@store/store-discount/reducer";
import {demoStores} from "@store/demo-stores/reducer";
import {demoStore} from "@store/demo-store/reducer";
import {newsItem} from "@store/news-item/reducer";
import {storeHistory} from "@store/store-history/reducer";
import {unassignedUsers} from "@store/unassigned-users/reducer";
import {campaigns} from "@store/campaigns/reducer";
import {campaign} from "@store/campaign/reducer";
import {templates} from "@store/templates/reducer";
import {recipients} from "@store/recipients/reducer";
import {vocabularies} from "@store/vocabularies/reducer";
import {vocabulariesDirections} from "@store/vocabularies-directions/reducer";
import {storeOneTimePayment} from "@store/one-time-charge/reducer";
import {storeCharge} from "@store/store-charge/reducer";
import {campaignStats} from "@store/campaign-stats/reducer";
import {fullstorySessions} from "@store/fullstory-sessions/reducer";
import {partnersNotifications} from "@store/partners-notifications/reducer";
import {coupons} from "@store/coupons/reducer";
import {coupon} from "@store/coupon/reducer";
import {couponUsage} from "@store/coupon-usage/reducer";
import {customers} from "@store/customers/reducer";
import {customer} from "@store/customer/reducer";
import {partnersUsers} from "@store/partners/users/reducer";
import {partnersUser} from "@store/partners/user/reducer";
import {partnersUserStores} from "@store/partners/referral-stores/reducer";
import {payouts} from "@store/partners/payouts/reducer";
import {payout} from "@store/partners/payout/reducer";
import {balance} from "@store/balance/reducer";
import {planQuantities} from "@store/analytics/plan-quantities/reducer";

import {reducer as form} from "redux-form";
import {combineReducers} from "redux";

export const reducers = combineReducers({
  auth,
  profile,
  news,
  newsItem,
  locales,
  locale,
  localeFile,
  unassignedUsers,
  users,
  user,
  scopes,
  roles,
  role,
  appTopBarSearch,
  stores,
  store,
  storeHistory,
  storesTrials,
  storeTrial,
  storesDiscounts,
  storeDiscount,
  demoStores,
  demoStore,
  campaigns,
  campaign,
  templates,
  recipients,
  vocabularies,
  vocabulariesDirections,
  storeOneTimePayment,
  storeCharge,
  campaignStats,
  fullstorySessions,
  partnersNotifications,
  coupons,
  coupon,
  couponUsage,
  customers,
  customer,
  partnersUsers,
  partnersUser,
  partnersUserStores,
  payouts,
  payout,
  balance,
  planQuantities,
  form,
});
