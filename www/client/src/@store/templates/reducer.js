import { fetchTemplatesAction, resetTemplatesAction } from "./actions";
import { get } from "lodash";
import { handleActions } from "redux-actions";

const defaultState = {
  isFetched: false,
  isLoading: false,
  templates: [],
  total: 0,
};

const loadingStartHandler = state => {
  return {
    ...state,
    isLoading: true,
  };
};

const loadingEndHandler = state => {
  return {
    ...state,
    isFetched: true,
    isLoading: false,
  };
};

const fetchTemplatesSuccessHandler = (state, { payload }) => {
  const data = get(payload, "data.data");

  return {
    ...state,
    templates: data.items,
    total: data.count,
    isFetched: true,
    isLoading: false,
  };
};

const resetTemplatesHandler = () => {
  return defaultState;
};

export const templates = handleActions(
  {
    [fetchTemplatesAction]: loadingStartHandler,
    [fetchTemplatesAction.success]: fetchTemplatesSuccessHandler,
    [fetchTemplatesAction.fail]: loadingEndHandler,

    [resetTemplatesAction]: resetTemplatesHandler,
  },
  defaultState
);
