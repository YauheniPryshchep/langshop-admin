import { createRequestAction } from "@store/createRequestAction";
import { createAction } from "redux-actions";

export const TEMPLATES_FETCH = "TEMPLATES_FETCH";
export const TEMPLATES_TEST_FETCH = "TEMPLATES_TEST_FETCH";
export const TEMPLATES_RESET = "TEMPLATES__RESET";

export const fetchTemplatesAction = createRequestAction(TEMPLATES_FETCH, (params, cancelToken) => {
  return {
    request: {
      method: "GET",
      url: `/api/campaigns/templates`,
      params,
      cancelToken,
    },
  };
});

export const fetchTestTemplateAction = createRequestAction(TEMPLATES_TEST_FETCH, (template, data, cancelToken) => {
  return {
    request: {
      method: "POST",
      url: `/api/campaigns/templates/${template}/test-email`,
      data,
      cancelToken,
    },
  };
});

export const resetTemplatesAction = createAction(TEMPLATES_RESET);
