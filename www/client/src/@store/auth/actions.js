import { createRequestAction } from "@store/createRequestAction";
import { createAction } from "redux-actions";

export const LOGIN = "LOGIN";
export const LOGIN_TO_PARTNERS = "LOGIN_TO_PARTNERS";
export const LOGOUT = "LOGOUT";
export const REFRESH = "REFRESH";

export const logoutAction = createAction(LOGOUT);

export const loginAction = createRequestAction(LOGIN, data => {
  return {
    request: {
      method: "POST",
      url: "/api/auth/login",
      data,
    },
  };
});

export const loginToPartnersAction = createRequestAction(LOGIN_TO_PARTNERS, id => {
  return {
    request: {
      method: "GET",
      url: `/api/auth/login-partners/${id}`,
    },
  };
});

export const refreshAction = createRequestAction(REFRESH, () => {
  return {
    request: {
      method: "GET",
      url: "/api/auth/refresh",
    },
  };
});
