import { getToken } from "@utils/jwt";
import { loginAction, logoutAction, refreshAction } from "./actions";
import { get } from "lodash";
import { handleActions } from "redux-actions";

const loginRequestHandler = state => {
  return {
    ...state,
    isLoading: true,
    isFailed: false,
  };
};

const loginSuccessHandler = (state, { payload }) => {
  const data = get(payload, "data");

  return {
    ...state,
    isLoading: false,
    token: data.token,
  };
};

const loginFailHandler = state => {
  return {
    ...state,
    isFailed: true,
    isLoading: false,
  };
};

const refreshRequestHandler = state => {
  return {
    ...state,
    isLoading: true,
  };
};

const refreshSuccessHandler = (state, { payload }) => {
  const data = get(payload, "data");

  return {
    ...state,
    isLoading: false,
    token: data.token,
  };
};

const refreshFailHandler = state => {
  return {
    ...state,
    isLoading: false,
    token: null,
  };
};

const logoutHandler = state => {
  return {
    ...state,
    token: null,
  };
};

export const auth = handleActions(
  {
    [loginAction]: loginRequestHandler,
    [loginAction.success]: loginSuccessHandler,
    [loginAction.fail]: loginFailHandler,

    [refreshAction]: refreshRequestHandler,
    [refreshAction.success]: refreshSuccessHandler,
    [refreshAction.fail]: refreshFailHandler,

    [logoutAction]: logoutHandler,
  },
  {
    isLoading: false,
    token: getToken(),
  }
);
