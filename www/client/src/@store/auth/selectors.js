import { createSelector } from "reselect";

import { get } from "lodash";

const baseState = state => get(state, "auth", null);

export const token = createSelector(baseState, state => get(state, "token", null));
export const isFailed = createSelector(baseState, state => get(state, "isFailed", null));
