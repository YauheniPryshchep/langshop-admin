import { logoutAction } from "@store/auth/actions";
import { resetProfileAction } from "@store/profile";

export const logout = () => dispatch => {
  dispatch(resetProfileAction());
  dispatch(logoutAction());
};
