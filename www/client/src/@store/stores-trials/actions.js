import { createRequestAction } from "@store/createRequestAction";
import { createAction } from "redux-actions";

export const STORES_TRIALS_FETCH = "STORES_TRIALS_FETCH";
export const STORES_TRIALS_CREATE = "STORES_TRIALS_CREATE";
export const STORES_TRIALS_REMOVE = "STORES_TRIALS_REMOVE";
export const STORES_TRIALS_RESET = "STORES_TRIALS_RESET";

export const fetchStoresTrialsAction = createRequestAction(STORES_TRIALS_FETCH, (params, cancelToken) => {
  return {
    request: {
      method: "GET",
      url: `/api/rebate/stores-trials`,
      params,
      cancelToken,
    },
  };
});

export const createStoreTrialAction = createRequestAction(STORES_TRIALS_CREATE, data => {
  return {
    request: {
      method: "POST",
      url: `/api/rebate/stores-trials`,
      data,
    },
  };
});

export const removeStoreTrialAction = createRequestAction(STORES_TRIALS_REMOVE, domain => {
  return {
    request: {
      method: "DELETE",
      url: `/api/rebate/stores-trials/${domain}`,
    },
  };
});

export const resetStoresTrialsAction = createAction(STORES_TRIALS_RESET);
