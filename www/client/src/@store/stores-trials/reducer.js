import {
  fetchStoresTrialsAction,
  resetStoresTrialsAction,
  createStoreTrialAction,
  removeStoreTrialAction,
} from "./actions";
import { get } from "lodash";
import { handleActions } from "redux-actions";

const defaultState = {
  isFetched: false,
  isLoading: false,
  storesTrials: [],
  total: 0,
};

const loadingStartHandler = state => {
  return {
    ...state,
    isLoading: true,
  };
};

const loadingEndHandler = state => {
  return {
    ...state,
    isFetched: true,
    isLoading: false,
  };
};

const fetchStoresTrialsSuccessHandler = (state, { payload }) => {
  const data = get(payload, "data.data");

  return {
    ...state,
    storesTrials: data.items,
    total: data.count,
    isFetched: true,
    isLoading: false,
  };
};

const resetStoresTrialsHandler = () => {
  return defaultState;
};

export const storesTrials = handleActions(
  {
    [fetchStoresTrialsAction]: loadingStartHandler,
    [fetchStoresTrialsAction.success]: fetchStoresTrialsSuccessHandler,
    [fetchStoresTrialsAction.fail]: loadingEndHandler,

    [createStoreTrialAction]: loadingStartHandler,
    [createStoreTrialAction.success]: loadingEndHandler,
    [createStoreTrialAction.fail]: loadingEndHandler,

    [removeStoreTrialAction]: loadingStartHandler,
    [removeStoreTrialAction.success]: loadingEndHandler,
    [removeStoreTrialAction.fail]: loadingEndHandler,

    [resetStoresTrialsAction]: resetStoresTrialsHandler,
  },
  defaultState
);
