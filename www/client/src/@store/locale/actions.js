import { createRequestAction } from "@store/createRequestAction";
import { createAction } from "redux-actions";

export const LOCALE_FETCH = "LOCALE_FETCH";
export const LOCALE_RESET = "LOCALE_RESET";

export const fetchLocaleAction = createRequestAction(LOCALE_FETCH, (iso, params, cancelToken) => {
  return {
    request: {
      method: "GET",
      url: `/api/locales/${iso}`,
      params,
      cancelToken,
    },
  };
});

export const resetLocaleAction = createAction(LOCALE_RESET);
