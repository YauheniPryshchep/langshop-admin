import { fetchLocaleAction, resetLocaleAction } from "./actions";
import { get } from "lodash";
import { handleActions } from "redux-actions";

const defaultState = {
  isFetched: false,
  isLoading: false,
  files: [],
  total: 0,
  locale: "",
};
const loadingStartHandler = state => {
  return {
    ...state,
    isLoading: true,
  };
};

const loadingEndHandler = state => {
  return {
    ...state,
    isFetched: true,
    isLoading: false,
  };
};

const fetchLocaleSuccessHandler = (state, { payload }) => {
  const data = get(payload, "data");
  return {
    ...state,
    total: data.total,
    files: data.items,
    locale: data.locale,
    isFetched: true,
    isLoading: false,
  };
};

const resetLocaleHandler = () => {
  return defaultState;
};

export const locale = handleActions(
  {
    [fetchLocaleAction]: loadingStartHandler,
    [fetchLocaleAction.success]: fetchLocaleSuccessHandler,
    [fetchLocaleAction.fail]: loadingEndHandler,

    [resetLocaleAction]: resetLocaleHandler,
  },
  defaultState
);
