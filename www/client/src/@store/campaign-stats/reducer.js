import { fetchCampaignStatsAction, resetCampaignStatsAction } from "./actions";
import { get } from "lodash";
import { handleActions } from "redux-actions";

const defaultState = {
  isFetched: false,
  isLoading: false,
  items: [],
};

const loadingStartHandler = state => {
  return {
    ...state,
    isLoading: true,
  };
};

const loadingEndHandler = state => {
  return {
    ...state,
    isFetched: true,
    isLoading: false,
  };
};

const fetchCampaignStatsSuccessHandler = (state, { payload }) => {
  const data = get(payload, "data.data");

  return {
    ...state,
    items: data,
    isFetched: true,
    isLoading: false,
  };
};

const resetCampaignStatsHandler = () => {
  return defaultState;
};

export const campaignStats = handleActions(
  {
    [fetchCampaignStatsAction]: loadingStartHandler,
    [fetchCampaignStatsAction.success]: fetchCampaignStatsSuccessHandler,
    [fetchCampaignStatsAction.fail]: loadingEndHandler,

    [resetCampaignStatsAction]: resetCampaignStatsHandler,
  },
  defaultState
);
