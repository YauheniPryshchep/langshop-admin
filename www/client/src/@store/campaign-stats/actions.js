import { createRequestAction } from "@store/createRequestAction";
import { createAction } from "redux-actions";

export const CAMPAIGN_STATS_FETCH = "CAMPAIGN_STATS_FETCH";
export const CAMPAIGN_STATS_RESET = "CAMPAIGN_STATS__RESET";

export const fetchCampaignStatsAction = createRequestAction(CAMPAIGN_STATS_FETCH, campaignId => {
  return {
    request: {
      method: "GET",
      url: `/api/campaigns/${campaignId}/stats`,
    },
  };
});

export const resetCampaignStatsAction = createAction(CAMPAIGN_STATS_RESET);
