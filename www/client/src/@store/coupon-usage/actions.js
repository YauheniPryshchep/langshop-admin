import { createRequestAction } from "@store/createRequestAction";
import { createAction } from "redux-actions";

export const COUPONS_FETCH = "COUPONS_USAGE_FETCH";
export const COUPONS_RESET = "COUPONS_USAGE_RESET";

export const fetchCouponsUsageAction = createRequestAction(COUPONS_FETCH, (couponId, params, cancelToken) => {
  return {
    request: {
      method: "GET",
      url: `/api/coupons/${couponId}/usage`,
      params,
      cancelToken,
    },
  };
});

export const resetCouponsUsageAction = createAction(COUPONS_RESET);
