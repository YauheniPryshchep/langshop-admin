import { fetchCouponsUsageAction, resetCouponsUsageAction } from "./actions";
import { get } from "lodash";
import { handleActions } from "redux-actions";

const defaultState = {
  isFetched: false,
  isLoading: false,
  stores: [],
  total: 0,
};

const loadingStartHandler = state => {
  return {
    ...state,
    isLoading: true,
  };
};

const loadingEndHandler = state => {
  return {
    ...state,
    isFetched: true,
    isLoading: false,
  };
};

const fetchCouponsSuccessHandler = (state, { payload }) => {
  const data = get(payload, "data.data");

  return {
    ...state,
    stores: data.items,
    total: data.count,
    isFetched: true,
    isLoading: false,
  };
};

const resetCouponsHandler = () => {
  return defaultState;
};

export const couponUsage = handleActions(
  {
    [fetchCouponsUsageAction]: loadingStartHandler,
    [fetchCouponsUsageAction.success]: fetchCouponsSuccessHandler,
    [fetchCouponsUsageAction.fail]: loadingEndHandler,

    [resetCouponsUsageAction]: resetCouponsHandler,
  },
  defaultState
);
