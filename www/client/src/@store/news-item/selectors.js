import { createSelector } from "reselect";

import { get } from "lodash";

const baseState = state => get(state, "newsItem", null);

export const isFetched = createSelector(baseState, state => get(state, "isFetched", false));

export const isLoading = createSelector(baseState, state => get(state, "isLoading", false));

export const isLoaded = createSelector(isLoading, isFetched, (isLoading, isFetched) => !isLoading && isFetched);

export const newsItem = createSelector(baseState, state => get(state, "newsItem", null));

export const initialValues = createSelector(
  newsItem,
  state =>
    state || {
      type: "article",
      title: "",
      description: "",
      src: "",
      video: "",
      layout: "left",
      primaryLink: "",
      primaryText: "",
      secondaryLink: "",
      secondaryText: "",
      sticky: false,
    }
);
