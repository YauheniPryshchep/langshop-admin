import { createRequestAction } from "@store/createRequestAction";
import { createAction } from "redux-actions";

export const NEWS_ITEM_FETCH = "NEWS_ITEM_FETCH";
export const NEWS_ITEM_CREATE = "NEWS_ITEM_CREATE";
export const NEWS_ITEM_UPDATE = "NEWS_ITEM_UPDATE";
export const NEWS_ITEM_REMOVE = "NEWS_ITEM_REMOVE";
export const NEWS_ITEM_RESET = "NEWS_ITEM_RESET";

const formDataRequest = {
  method: "POST",
  headers: {
    "Content-Type": "multipart/form-data",
  },
  transformRequest: [
    function(data) {
      const formData = new FormData();

      Object.keys(data).forEach(key =>
        formData.append(key, typeof data[key] !== "boolean" ? data[key] : Number(data[key]))
      );

      return formData;
    },
  ],
};

export const fetchNewsItemAction = createRequestAction(NEWS_ITEM_FETCH, itemId => {
  return {
    request: {
      method: "GET",
      url: `/api/news/${itemId}`,
    },
  };
});

export const createNewsItemAction = createRequestAction(NEWS_ITEM_CREATE, data => {
  return {
    request: {
      ...formDataRequest,
      url: `/api/news`,
      data,
    },
  };
});

export const updateNewsItemAction = createRequestAction(NEWS_ITEM_UPDATE, (itemId, data) => {
  return {
    request: {
      ...formDataRequest,
      url: `/api/news/${itemId}`,
      data,
    },
  };
});

export const removeNewsItemAction = createRequestAction(NEWS_ITEM_REMOVE, itemId => {
  return {
    request: {
      method: "DELETE",
      url: `/api/news/${itemId}`,
    },
  };
});

export const resetNewsItemAction = createAction(NEWS_ITEM_RESET);
