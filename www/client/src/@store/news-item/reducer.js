import {
  fetchNewsItemAction,
  createNewsItemAction,
  updateNewsItemAction,
  removeNewsItemAction,
  resetNewsItemAction,
} from "./actions";
import { get } from "lodash";
import { handleActions } from "redux-actions";

const defaultState = {
  isFetched: false,
  isLoading: false,
  newsItem: null,
};

const loadingStartHandler = state => {
  return {
    ...state,
    isLoading: true,
  };
};

const loadingEndHandler = state => {
  return {
    ...state,
    isFetched: true,
    isLoading: false,
  };
};

const fetchNewsItemSuccessHandler = (state, { payload }) => {
  const data = get(payload, "data.data");

  return {
    ...state,
    newsItem: data,
    isFetched: true,
    isLoading: false,
  };
};

const createNewsItemSuccessHandler = (state, { payload }) => {
  const data = get(payload, "data.data");

  return {
    ...state,
    newsItem: data,
    isFetched: true,
    isLoading: false,
  };
};

const updateNewsItemSuccessHandler = (state, { payload }) => {
  const data = get(payload, "data.data");

  return {
    ...state,
    newsItem: data,
    isFetched: true,
    isLoading: false,
  };
};

const resetNewsItemHandler = () => {
  return defaultState;
};

export const newsItem = handleActions(
  {
    [fetchNewsItemAction]: loadingStartHandler,
    [fetchNewsItemAction.success]: fetchNewsItemSuccessHandler,
    [fetchNewsItemAction.fail]: loadingEndHandler,

    [createNewsItemAction]: loadingStartHandler,
    [createNewsItemAction.success]: createNewsItemSuccessHandler,
    [createNewsItemAction.fail]: loadingEndHandler,

    [updateNewsItemAction]: loadingStartHandler,
    [updateNewsItemAction.success]: updateNewsItemSuccessHandler,
    [updateNewsItemAction.fail]: loadingEndHandler,

    [removeNewsItemAction]: loadingStartHandler,
    [removeNewsItemAction.success]: resetNewsItemHandler,
    [removeNewsItemAction.fail]: loadingEndHandler,

    [resetNewsItemAction]: resetNewsItemHandler,
  },
  defaultState
);
