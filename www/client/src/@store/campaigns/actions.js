import { createRequestAction } from "@store/createRequestAction";
import { createAction } from "redux-actions";

export const CAMPAIGNS_FETCH = "CAMPAIGNS_FETCH";
export const CAMPAIGNS_RESET = "CAMPAIGNS__RESET";

export const fetchCampaignsAction = createRequestAction(CAMPAIGNS_FETCH, (params, cancelToken) => {
  return {
    request: {
      method: "GET",
      url: `/api/campaigns`,
      params,
      cancelToken,
    },
  };
});

export const resetCampaignsAction = createAction(CAMPAIGNS_RESET);
