import { fetchCampaignsAction, resetCampaignsAction } from "./actions";
import { get } from "lodash";
import { handleActions } from "redux-actions";

const defaultState = {
  isFetched: false,
  isLoading: false,
  campaigns: [],
  total: 0,
};

const loadingStartHandler = state => {
  return {
    ...state,
    isLoading: true,
  };
};

const loadingEndHandler = state => {
  return {
    ...state,
    isFetched: true,
    isLoading: false,
  };
};

const fetchCampaignsSuccessHandler = (state, { payload }) => {
  const data = get(payload, "data.data");

  return {
    ...state,
    campaigns: data.items,
    total: data.count,
    isFetched: true,
    isLoading: false,
  };
};

const resetCampaignsHandler = () => {
  return defaultState;
};

export const campaigns = handleActions(
  {
    [fetchCampaignsAction]: loadingStartHandler,
    [fetchCampaignsAction.success]: fetchCampaignsSuccessHandler,
    [fetchCampaignsAction.fail]: loadingEndHandler,

    [resetCampaignsAction]: resetCampaignsHandler,
  },
  defaultState
);
