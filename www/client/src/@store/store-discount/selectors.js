import { createSelector } from "reselect";

import { get } from "lodash";

const baseState = state => get(state, "storeDiscount", null);

export const isFetched = createSelector(baseState, state => get(state, "isFetched", false));

export const isLoading = createSelector(baseState, state => get(state, "isLoading", false));

export const isLoaded = createSelector(isLoading, isFetched, (isLoading, isFetched) => !isLoading && isFetched);

export const storeDiscount = createSelector(baseState, state => get(state, "storeDiscount", null));

export const initialValues = initialValues => {
  return {
    type: "percent",
    ...initialValues,
  };
};
