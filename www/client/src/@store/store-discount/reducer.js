import {
  fetchStoreDiscountAction,
  createStoreDiscountAction,
  removeStoreDiscountAction,
  resetStoreDiscountAction,
} from "./actions";
import { get, first } from "lodash";
import { handleActions } from "redux-actions";

const defaultState = {
  isFetched: false,
  isLoading: false,
  storeDiscount: null,
};

const loadingStartHandler = state => {
  return {
    ...state,
    isLoading: true,
  };
};

const loadingEndHandler = state => {
  return {
    ...state,
    isFetched: true,
    isLoading: false,
  };
};

const fetchStoreDiscountSuccessHandler = (state, { payload }) => {
  const data = get(payload, "data.data");

  return {
    ...state,
    storeDiscount: first(data.items),
    isFetched: true,
    isLoading: false,
  };
};

const createStoreDiscountSuccessHandler = (state, { payload }) => {
  const data = get(payload, "data.data");

  return {
    ...state,
    storeDiscount: data,
    isLoading: false,
  };
};

const removeStoreDiscountSuccessHandler = state => {
  return {
    ...state,
    storeDiscount: null,
    isLoading: false,
  };
};

const resetStoreDiscountHandler = () => {
  return defaultState;
};

export const storeDiscount = handleActions(
  {
    [fetchStoreDiscountAction]: loadingStartHandler,
    [fetchStoreDiscountAction.success]: fetchStoreDiscountSuccessHandler,
    [fetchStoreDiscountAction.fail]: loadingEndHandler,

    [createStoreDiscountAction]: loadingStartHandler,
    [createStoreDiscountAction.success]: createStoreDiscountSuccessHandler,
    [createStoreDiscountAction.fail]: loadingEndHandler,

    [removeStoreDiscountAction]: loadingStartHandler,
    [removeStoreDiscountAction.success]: removeStoreDiscountSuccessHandler,
    [removeStoreDiscountAction.fail]: loadingEndHandler,

    [resetStoreDiscountAction]: resetStoreDiscountHandler,
  },
  defaultState
);
