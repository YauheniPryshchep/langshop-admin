import { createRequestAction } from "@store/createRequestAction";
import { createAction } from "redux-actions";

export const STORE_DISCOUNT_FETCH = "STORE_DISCOUNT_FETCH";
export const STORE_DISCOUNT_CREATE = "STORE_DISCOUNT_CREATE";
export const STORE_DISCOUNT_REMOVE = "STORE_DISCOUNT_REMOVE";
export const STORE_DISCOUNT_RESET = "STORE_DISCOUNT_RESET";

export const fetchStoreDiscountAction = createRequestAction(STORE_DISCOUNT_FETCH, (domain, cancelToken) => {
  return {
    request: {
      method: "GET",
      url: `/api/rebate/stores-discounts`,
      params: {
        filter: domain,
        limit: 1,
      },
      cancelToken,
    },
  };
});

export const createStoreDiscountAction = createRequestAction(STORE_DISCOUNT_CREATE, data => {
  return {
    request: {
      method: "POST",
      url: `/api/rebate/stores-discounts`,
      data,
    },
  };
});

export const removeStoreDiscountAction = createRequestAction(STORE_DISCOUNT_REMOVE, domain => {
  return {
    request: {
      method: "DELETE",
      url: `/api/rebate/stores-discounts/${domain}`,
    },
  };
});

export const resetStoreDiscountAction = createAction(STORE_DISCOUNT_RESET);
