import { createRequestAction } from "@store/createRequestAction";
import { createAction } from "redux-actions";

export const PARTNERS_USER_FETCH = "PARTNERS_USER_FETCH";
export const PARTNERS_USER_RESET = "PARTNERS_USER_RESET";

export const fetchPartnersUsersAction = createRequestAction(PARTNERS_USER_FETCH, (params, cancelToken) => {
  return {
    request: {
      method: "GET",
      url: `/api/partners/users`,
      params,
      cancelToken,
    },
  };
});

export const resetPartnersUsersAction = createAction(PARTNERS_USER_RESET);
