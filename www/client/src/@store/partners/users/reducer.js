import { fetchPartnersUsersAction, resetPartnersUsersAction } from "./actions";
import { get } from "lodash";
import { handleActions } from "redux-actions";

const defaultState = {
  isFetched: false,
  isLoading: false,
  users: [],
  total: 0,
};

const loadingStartHandler = state => {
  return {
    ...state,
    isLoading: true,
  };
};

const loadingEndHandler = state => {
  return {
    ...state,
    isFetched: true,
    isLoading: false,
  };
};

const fetchUsersSuccessHandler = (state, { payload }) => {
  const data = get(payload, "data.data");

  return {
    ...state,
    users: data.items,
    total: data.count,
    isFetched: true,
    isLoading: false,
  };
};

const resetUsersHandler = () => {
  return defaultState;
};

export const partnersUsers = handleActions(
  {
    [fetchPartnersUsersAction]: loadingStartHandler,
    [fetchPartnersUsersAction.success]: fetchUsersSuccessHandler,
    [fetchPartnersUsersAction.fail]: loadingEndHandler,

    [resetPartnersUsersAction]: resetUsersHandler,
  },
  defaultState
);
