import { fetchPartnersUserAction, resetPartnersUserAction, setNewCommissionAction } from "./actions";
import { get } from "lodash";
import { handleActions } from "redux-actions";

const defaultState = {
  isFetched: false,
  isLoading: false,
  isUpdating: false,
  user: {},
};

const loadingStartHandler = state => {
  return {
    ...state,
    isLoading: true,
  };
};

const loadingEndHandler = state => {
  return {
    ...state,
    isFetched: true,
    isLoading: false,
  };
};

const loadingCommissionStartHandler = state => {
  return {
    ...state,
    isUpdating: true,
  };
};

const loadingCommissionEndHandler = state => {
  return {
    ...state,
    isFetched: true,
    isUpdating: false,
  };
};

const fetchUsersSuccessHandler = (state, { payload }) => {
  const data = get(payload, "data.data");

  return {
    ...state,
    user: {
      ...data,
    },
    isFetched: true,
    isLoading: false,
    isUpdating: false,
  };
};

const resetUsersHandler = () => {
  return defaultState;
};

export const partnersUser = handleActions(
  {
    [fetchPartnersUserAction]: loadingStartHandler,
    [fetchPartnersUserAction.success]: fetchUsersSuccessHandler,
    [fetchPartnersUserAction.fail]: loadingEndHandler,

    [setNewCommissionAction]: loadingCommissionStartHandler,
    [setNewCommissionAction.success]: fetchUsersSuccessHandler,
    [setNewCommissionAction.fail]: loadingCommissionEndHandler,

    [resetPartnersUserAction]: resetUsersHandler,
  },
  defaultState
);
