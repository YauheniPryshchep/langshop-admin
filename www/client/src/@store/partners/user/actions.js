import { createRequestAction } from "@store/createRequestAction";
import { createAction } from "redux-actions";

export const PARTNER_USER_FETCH = "PARTNER_USER_FETCH";
export const PARTNER_USER_COMMISSION_FETCH = "PARTNER_USER_COMMISSION_FETCH";
export const PARTNER_USER_RESET = "PARTNER_USER_RESET";

export const fetchPartnersUserAction = createRequestAction(PARTNER_USER_FETCH, (id, cancelToken) => {
  return {
    request: {
      method: "GET",
      url: `/api/partners/users/${id}`,
      cancelToken,
    },
  };
});

export const setNewCommissionAction = createRequestAction(PARTNER_USER_COMMISSION_FETCH, (id, data) => {
  return {
    request: {
      method: "POST",
      url: `/api/partners/users/${id}/commissions`,
      data,
    },
  };
});

export const resetPartnersUserAction = createAction(PARTNER_USER_RESET);
