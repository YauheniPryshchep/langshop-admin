import { createSelector } from "reselect";

import { get } from "lodash";

const baseState = state => get(state, "partnersUser", null);

export const isFetched = createSelector(baseState, state => get(state, "isFetched", false));

export const isLoading = createSelector(baseState, state => get(state, "isLoading", false));
export const isUpdating = createSelector(baseState, state => get(state, "isUpdating", false));

export const isLoaded = createSelector(isLoading, isFetched, (isLoading, isFetched) => !isLoading && isFetched);

export const total = createSelector(baseState, state => get(state, "total", 0));

export const user = createSelector(baseState, state => get(state, "user", []));

export const userCommissions = createSelector(user, state => get(state, "commissions", []));
