import { fetchPayoutAction, resetPayoutAction, updatePayoutAction } from "./actions";
import get from "lodash/get";
import { handleActions } from "redux-actions";

const defaultState = {
  payout: null,
  isFetched: false,
  isLoading: false,
  isUpdating: false,
};

const fetchPayoutRequestHandler = state => {
  return {
    ...state,
    isLoading: true,
  };
};

const fetchUpdatePayoutRequestHandler = state => {
  return {
    ...state,
    isUpdating: true,
  };
};

const fetchPayoutSuccessHandler = (state, { payload }) => {
  const data = get(payload, "data.data", []);

  return {
    ...state,
    payout: {
      ...data,
    },
    isFetched: true,
    isUpdating: false,
    isLoading: false,
  };
};

const fetchPayoutFailHandler = state => {
  return {
    ...state,
    isUpdating: false,
  };
};

const fetchUpdatePayoutFailHandler = state => {
  return {
    ...state,
    isFetched: true,
    isLoading: false,
  };
};

const resetPayoutHandler = () => {
  return defaultState;
};

export const payout = handleActions(
  {
    [fetchPayoutAction]: fetchPayoutRequestHandler,
    [fetchPayoutAction.success]: fetchPayoutSuccessHandler,
    [fetchPayoutAction.fail]: fetchPayoutFailHandler,

    [updatePayoutAction]: fetchUpdatePayoutRequestHandler,
    [updatePayoutAction.success]: fetchPayoutSuccessHandler,
    [updatePayoutAction.fail]: fetchUpdatePayoutFailHandler,

    [resetPayoutAction]: resetPayoutHandler,
  },
  defaultState
);
