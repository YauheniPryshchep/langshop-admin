import { createRequestAction } from "@store/createRequestAction";
import { createAction } from "redux-actions";

export const PAYOUT_FETCH = "PAYOUT_FETCH";
export const UPDATE_PAYOUT = "UPDATE_PAYOUT";
export const PAYOUT_RESET = "PAYOUT_RESET";

export const fetchPayoutAction = createRequestAction(PAYOUT_FETCH, id => {
  return {
    request: {
      method: "GET",
      url: `/api/partners/payouts/${id}`,
    },
  };
});

export const updatePayoutAction = createRequestAction(UPDATE_PAYOUT, (id, data) => {
  return {
    request: {
      method: "PUT",
      url: `/api/partners/payouts/${id}`,
      data,
    },
  };
});

export const resetPayoutAction = createAction(PAYOUT_RESET);
