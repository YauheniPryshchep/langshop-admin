import {
  fetchPayoutsAction,
  fetchUserPayoutsAction,
  fetchPayoutCountAction,
  updatePayout,
  resetPayouts,
} from "./actions";
import { get, reduce } from "lodash";
import { handleActions } from "redux-actions";

const defaultState = {
  isFetched: false,
  isLoading: false,
  isUpdating: false,
  payouts: [],
  total: 0,
};

const loadingStartHandler = state => {
  return {
    ...state,
    isLoading: true,
  };
};

const loadingEndHandler = state => {
  return {
    ...state,
    isFetched: true,
    isLoading: false,
  };
};

const updatingStartHandler = state => {
  return {
    ...state,
    isUpdating: true,
  };
};

const updatingEndHandler = state => {
  return {
    ...state,
    isUpdating: false,
  };
};

const fetchPayoutsSuccessHandler = (state, { payload }) => {
  const data = get(payload, "data.data");

  return {
    ...state,
    payouts: data.items,
    total: data.count,
    isFetched: true,
    isLoading: false,
  };
};

const fetchPayoutsCountSuccessHandler = (state, { payload }) => {
  const data = get(payload, "data.data");

  return {
    ...state,
    count: data,
    isFetched: true,
    isLoading: false,
  };
};

const updatePayoutSuccess = (state, { payload }) => {
  const data = get(payload, "data.data");

  return {
    ...state,
    payouts: [
      ...reduce(
        state.payouts,
        (acc, item) => (item.id === data.id ? [...acc, { ...data }] : [...acc, { ...item }]),
        []
      ),
    ],
    isFetched: true,
    isLoading: false,
  };
};

const resetpayoutsHandler = () => {
  return defaultState;
};

export const payouts = handleActions(
  {
    [fetchPayoutsAction]: loadingStartHandler,
    [fetchPayoutsAction.success]: fetchPayoutsSuccessHandler,
    [fetchPayoutsAction.fail]: loadingEndHandler,

    [fetchUserPayoutsAction]: loadingStartHandler,
    [fetchUserPayoutsAction.success]: fetchPayoutsSuccessHandler,
    [fetchUserPayoutsAction.fail]: loadingEndHandler,

    [fetchPayoutCountAction]: loadingStartHandler,
    [fetchPayoutCountAction.success]: fetchPayoutsCountSuccessHandler,
    [fetchPayoutCountAction.fail]: loadingEndHandler,

    [updatePayout]: updatingStartHandler,
    [updatePayout.success]: updatePayoutSuccess,
    [updatePayout.fail]: updatingEndHandler,

    [resetPayouts]: resetpayoutsHandler,
  },
  defaultState
);
