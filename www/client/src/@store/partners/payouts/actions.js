import { createRequestAction } from "@store/createRequestAction";
import { createAction } from "redux-actions";

export const USER_PAYOUTS_FETCH = "USER_PAYOUTS_FETCH";
export const PAYOUTS_FETCH = "PAYOUTS_FETCH";
export const PAYOUTS_FETCH_COUNT = "PAYOUTS_FETCH_COUNT";
export const UPDATE_PAYOUT = "UPDATE_PAYOUT";
export const PAYOUTS_RESET = "PAYOUTS_RESET";

export const fetchUserPayoutsAction = createRequestAction(USER_PAYOUTS_FETCH, (id, params, cancelToken) => {
  return {
    request: {
      method: "GET",
      url: `/api/partners/users/${id}/payouts`,
      params,
      cancelToken,
    },
  };
});

export const fetchPayoutsAction = createRequestAction(PAYOUTS_FETCH, (params, cancelToken) => {
  return {
    request: {
      method: "GET",
      url: `/api/partners/payouts`,
      params,
      cancelToken,
    },
  };
});

export const fetchPayoutCountAction = createRequestAction(PAYOUTS_FETCH_COUNT, cancelToken => {
  return {
    request: {
      method: "GET",
      url: `/api/partners/payouts/count`,
      cancelToken,
    },
  };
});

export const updatePayout = createRequestAction(UPDATE_PAYOUT, (id, data, cancelToken) => {
  return {
    request: {
      method: "PUT",
      url: `/api/partners/payouts/${id}`,
      data,
      cancelToken,
    },
  };
});

export const resetPayouts = createAction(PAYOUTS_RESET);
