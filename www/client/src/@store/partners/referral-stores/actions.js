import { createRequestAction } from "@store/createRequestAction";
import { createAction } from "redux-actions";

export const PARTNER_USER_STORES_FETCH = "PARTNER_USER_STORES_FETCH";
export const PARTNER_USER_STORES_RESET = "PARTNER_USER_STORES_RESET";

export const fetchPartnerUserStores = createRequestAction(PARTNER_USER_STORES_FETCH, (id, params, cancelToken) => {
  return {
    request: {
      method: "GET",
      url: `/api/partners/users/${id}/stores`,
      params,
      cancelToken,
    },
  };
});

export const resetPartnerUserStoresAction = createAction(PARTNER_USER_STORES_RESET);
