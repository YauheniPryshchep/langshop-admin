import { createRequestAction } from "@store/createRequestAction";
import { createAction } from "redux-actions";

export const STORES_FETCH = "STORES_FETCH";
export const STORES_RESET = "STORES_RESET";

export const fetchStoresAction = createRequestAction(STORES_FETCH, (params, cancelToken) => {
  return {
    request: {
      method: "GET",
      url: `/api/stores`,
      params,
      cancelToken,
    },
  };
});

export const resetStoresAction = createAction(STORES_RESET);
