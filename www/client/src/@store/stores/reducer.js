import { fetchStoresAction, resetStoresAction } from "./actions";
import { get } from "lodash";
import { handleActions } from "redux-actions";

const defaultState = {
  isFetched: false,
  isLoading: false,
  stores: [],
  total: 0,
};

const loadingStartHandler = state => {
  return {
    ...state,
    isLoading: true,
  };
};

const loadingEndHandler = state => {
  return {
    ...state,
    isFetched: true,
    isLoading: false,
  };
};

const fetchStoresSuccessHandler = (state, { payload }) => {
  const data = get(payload, "data.data");

  return {
    ...state,
    stores: data.items,
    total: data.count,
    isFetched: true,
    isLoading: false,
  };
};

const resetStoresHandler = () => {
  return defaultState;
};

export const stores = handleActions(
  {
    [fetchStoresAction]: loadingStartHandler,
    [fetchStoresAction.success]: fetchStoresSuccessHandler,
    [fetchStoresAction.fail]: loadingEndHandler,

    [resetStoresAction]: resetStoresHandler,
  },
  defaultState
);
