import { createOneTimePaymentAction } from "./actions";
import { get, first } from "lodash";
import { handleActions } from "redux-actions";

const defaultState = {
  isFetched: false,
  isLoading: false,
  storePayment: null,
};

const loadingStartHandler = state => {
  return {
    ...state,
    isLoading: true,
  };
};

const loadingEndHandler = state => {
  return {
    ...state,
    isFetched: true,
    isLoading: false,
  };
};

const createStorePaymentSuccessHandler = state => {
  return {
    ...state,
    isLoading: false,
  };
};

export const storeOneTimePayment = handleActions(
  {
    [createOneTimePaymentAction]: loadingStartHandler,
    [createOneTimePaymentAction.success]: createStorePaymentSuccessHandler,
    [createOneTimePaymentAction.fail]: loadingEndHandler,
  },
  defaultState
);
