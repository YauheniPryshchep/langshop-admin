import { createRequestAction } from "@store/createRequestAction";

export const STORE_ONE_TIME_PAYMENT_CREATE = "STORE_ONE_TIME_PAYMENT_CREATE";

export const createOneTimePaymentAction = createRequestAction(STORE_ONE_TIME_PAYMENT_CREATE, data => {
  return {
    request: {
      method: "POST",
      url: `/api/rebate/stores-one-time-payment`,
      data,
    },
  };
});
