import { createSelector } from "reselect";

import { get } from "lodash";

const baseState = state => get(state, "companyItem", null);

export const isFetched = createSelector(baseState, state => get(state, "isFetched", false));

export const isLoading = createSelector(baseState, state => get(state, "isLoading", false));

export const isLoaded = createSelector(isLoading, isFetched, (isLoading, isFetched) => !isLoading && isFetched);

export const companyItem = createSelector(baseState, state => get(state, "companyItem", null));

export const templateID = createSelector(baseState, state => get(state, "templateID", 0));

export const initialValues = createSelector(
  companyItem,
  state =>
    state || {
      title: "",
      description: "",
      date: "",
      subject: "",
    }
);
