import { createRequestAction } from "@store/createRequestAction";
import { createAction } from "redux-actions";

export const COMPANY_ITEM_FETCH = "COMPANY_ITEM_FETCH";
export const COMPANY_ITEM_UPDATE = "COMPANY_ITEM_UPDATE";
export const COMPANY_ITEM_REMOVE = "COMPANY_ITEM_REMOVE";
export const COMPANY_ITEM_RESET = "COMPANY_ITEM_RESET";
export const COMPANY_CREATE = "COMPANY_CREATE";

export const fetchCompanyItemAction = createRequestAction(COMPANY_ITEM_FETCH, itemId => {
  return {
    request: {
      method: "GET",
      url: `/api/companies/${itemId}`,
    },
  };
});

export const updateCompanyItemAction = createRequestAction(COMPANY_ITEM_UPDATE, (itemId, data) => {
  return {
    request: {
      method: "PUT",
      url: `/api/companies/${itemId}`,
      data,
    },
  };
});

export const createCompanyAction = createRequestAction(COMPANY_CREATE, data => {
  return {
    request: {
      method: "POST",
      url: `/api/companies`,
      data,
    },
  };
});

export const removeCompanyItemAction = createRequestAction(COMPANY_ITEM_REMOVE, itemId => {
  return {
    request: {
      method: "DELETE",
      url: `/api/companies/${itemId}`,
    },
  };
});

export const resetCompanyItemAction = createAction(COMPANY_ITEM_RESET);
