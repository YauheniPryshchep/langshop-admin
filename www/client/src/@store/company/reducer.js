import {
  fetchCompanyItemAction,
  updateCompanyItemAction,
  removeCompanyItemAction,
  resetCompanyItemAction,
  createCompanyAction,
} from "./actions";
import { get } from "lodash";
import { handleActions } from "redux-actions";

const defaultState = {
  isFetched: false,
  isLoading: false,
  companyItem: null,
  templateID: 4,
};

const loadingStartHandler = state => {
  return {
    ...state,
    isLoading: true,
  };
};

const loadingEndHandler = state => {
  return {
    ...state,
    isFetched: true,
    isLoading: false,
  };
};

const fetchCompanyItemSuccessHandler = (state, { payload }) => {
  const data = get(payload, "data");

  return {
    ...state,
    companyItem: data,
    isFetched: true,
    isLoading: false,
  };
};

const updateCompanyItemSuccessHandler = (state, { payload }) => {
  const data = get(payload, "data");

  return {
    ...state,
    companyItem: data,
    isFetched: true,
    isLoading: false,
  };
};

const resetCompanyItemHandler = () => {
  return defaultState;
};

export const companyItem = handleActions(
  {
    [fetchCompanyItemAction]: loadingStartHandler,
    [fetchCompanyItemAction.success]: fetchCompanyItemSuccessHandler,
    [fetchCompanyItemAction.fail]: loadingEndHandler,

    [createCompanyAction]: loadingStartHandler,
    [createCompanyAction.success]: loadingEndHandler,
    [createCompanyAction.fail]: loadingEndHandler,

    [updateCompanyItemAction]: loadingStartHandler,
    [updateCompanyItemAction.success]: updateCompanyItemSuccessHandler,
    [updateCompanyItemAction.fail]: loadingEndHandler,

    [removeCompanyItemAction]: loadingStartHandler,
    [removeCompanyItemAction.success]: resetCompanyItemHandler,
    [removeCompanyItemAction.fail]: loadingEndHandler,

    [resetCompanyItemAction]: resetCompanyItemHandler,
  },
  defaultState
);
