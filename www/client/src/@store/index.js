import multiClientMiddleware from "@middlewares/axios";
import tokenRefreshMiddleware from "@middlewares/store/tokenRefresh";
import tokenUpdateMiddleware from "@middlewares/store/tokenUpdate";
import unauthorizedHandleMiddleware from "@middlewares/store/unauthorizedHandle";
import errorHandleMiddleware from "@middlewares/store/errorHandle";
import { reducers } from "./reducers";
import { applyMiddleware, compose, createStore as createStoreRedux } from "redux";
import thunkMiddleware from "redux-thunk";

const createStore = initialState => {
  return createStoreRedux(
    reducers,
    initialState,
    compose(
      applyMiddleware(
        tokenRefreshMiddleware,
        unauthorizedHandleMiddleware,
        errorHandleMiddleware,
        thunkMiddleware,
        multiClientMiddleware,
        tokenUpdateMiddleware
      )
    )
  );
};

export default createStore();
