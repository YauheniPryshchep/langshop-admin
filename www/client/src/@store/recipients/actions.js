import { createRequestAction } from "@store/createRequestAction";
import { createAction } from "redux-actions";

export const RECIPIENTS_FETCH = "RECIPIENTS_FETCH";
export const RECIPIENTS_RESET = "RECIPIENTS__RESET";

export const fetchRecipientsAction = createRequestAction(RECIPIENTS_FETCH, (params) => {
  return {
    request: {
      method: "GET",
      url: `/api/campaigns/recipients?${toQueryString({ filters: params["filters"] })}`,
      params: {
        limit: params["limit"],
        page: params["page"],
      }
    },
  };
});

export const resetRecipientsAction = createAction(RECIPIENTS_RESET);

export const toQueryString = data => {
  if (typeof data !== "object" || !data.hasOwnProperty("filters")) {
    return "";
  }
  let query = [];
  data.filters.map((filter, filterId) => {
    Object.keys(filter).map(filterKey => {
      if (!filter[filterKey]) {
        return "";
      }

      if (typeof filter[filterKey] === "object") {
        Object.keys(filter[filterKey]).map(v => {
          return query.push(`filters[${filterId}][${filterKey}][${v}]=${filter[filterKey][v]}`);
        });
      } else {
        query.push(`filters[${filterId}][${filterKey}]=${filter[filterKey]}`);
      }
    });
  });

  return query.join("&");
};
