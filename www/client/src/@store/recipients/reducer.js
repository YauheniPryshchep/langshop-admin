import { fetchRecipientsAction, resetRecipientsAction } from "./actions";
import { get } from "lodash";
import { handleActions } from "redux-actions";

const defaultState = {
  isFetched: false,
  isLoading: false,
  recipients: [],
  total: 0,
};

const loadingStartHandler = state => {
  return {
    ...state,
    isLoading: true,
  };
};

const loadingEndHandler = state => {
  return {
    ...state,
    isFetched: true,
    isLoading: false,
  };
};

const fetchRecipientsSuccessHandler = (state, { payload }) => {
  const data = get(payload, "data.data");

  return {
    ...state,
    recipients: data.items,
    total: data.count,
    isFetched: true,
    isLoading: false,
  };
};

const resetRecipientsHandler = () => {
  return defaultState;
};

export const recipients = handleActions(
  {
    [fetchRecipientsAction]: loadingStartHandler,
    [fetchRecipientsAction.success]: fetchRecipientsSuccessHandler,
    [fetchRecipientsAction.fail]: loadingEndHandler,

    [resetRecipientsAction]: resetRecipientsHandler,
  },
  defaultState
);
