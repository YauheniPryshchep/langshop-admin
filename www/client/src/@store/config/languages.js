export const languages = [
  {
    code: "an",
    title: "Aragonese",
  },
  {
    code: "af",
    title: "Afrikaans",
  },
  {
    code: "sq",
    title: "Albanian",
  },
  {
    code: "am",
    title: "Amharic",
  },
  {
    code: "ar",
    title: "Arabic",
  },
  {
    code: "ast",
    title: "Asturian",
  },
  {
    code: "ar-YE",
    title: "Arabic (Yemen)",
  },
  {
    code: "ar-TN",
    title: "Arabic (Tunisia)",
  },
  {
    code: "ar-SY",
    title: "Arabic (Syria)",
  },
  {
    code: "ar-SA",
    title: "Arabic (Saudi Arabia)",
  },
  {
    code: "ar-QA",
    title: "Arabic (Qatar)",
  },
  {
    code: "ar-OM",
    title: "Arabic (Oman)",
  },
  {
    code: "ar-MA",
    title: "Arabic (Morocco)",
  },
  {
    code: "ar-LY",
    title: "Arabic (Libya)",
  },
  {
    code: "ar-LB",
    title: "Arabic (Lebanon)",
  },
  {
    code: "ar-KW",
    title: "Arabic (Kuwait)",
  },
  {
    code: "ar-JO",
    title: "Arabic (Jordan)",
  },
  {
    code: "ar-IQ",
    title: "Arabic (Iraq)",
  },
  {
    code: "ar-EG",
    title: "Arabic (Egypt)",
  },
  {
    code: "ar-DZ",
    title: "Arabic (Algeria)",
  },
  {
    code: "ar-BH",
    title: "Arabic (Bahrain)",
  },
  {
    code: "ar-AE",
    title: "Arabic (United Arab Emirates)",
  },
  {
    code: "hy",
    title: "Armenian",
  },
  {
    code: "az",
    title: "Azerbaijani",
  },
  {
    code: "eu",
    title: "Basque",
  },
  {
    code: "be",
    title: "Belarusian",
  },
  {
    code: "bn",
    title: "Bengali",
  },
  {
    code: "bh",
    title: "Bihari",
  },
  {
    code: "bs",
    title: "Bosnian",
  },
  {
    code: "br",
    title: "Breton",
  },
  {
    code: "bg",
    title: "Bulgarian",
  },
  {
    code: "bg-BG",
    title: "Bulgarian (Bulgaria)",
  },
  {
    code: "km",
    title: "Cambodian",
  },
  {
    code: "ca",
    title: "Catalan",
  },
  {
    code: "ckb",
    title: "Kurdish (Sorani)",
  },
  {
    code: "zh",
    title: "Chinese",
  },
  {
    code: "zh-CN",
    title: "Chinese (China)",
  },
  {
    code: "zh-HK",
    title: "Chinese (Hong Kong)",
  },
  {
    code: "zh-TW",
    title: "Chinese (Taiwan)",
  },
  {
    code: "zh-SG",
    title: "Chinese (Singapore)",
  },
  {
    code: "zh-MO",
    title: "Chinese (Macau)",
  },
  {
    code: "co",
    title: "Corsican",
  },
  {
    code: "hr",
    title: "Croatian",
  },
  {
    code: "hr-HR",
    title: "Croatian (Croatia)",
  },
  {
    code: "cs",
    title: "Czech",
  },
  {
    code: "da",
    title: "Danish",
  },
  {
    code: "nl",
    title: "Dutch",
  },
  {
    code: "nl-NL",
    title: "Dutch (Netherlands)",
  },
  {
    code: "nl-BE",
    title: "Dutch (Belgium)",
  },
  {
    code: "en",
    title: "English",
  },
  {
    code: "en-AU",
    title: "English (Australia)",
  },
  {
    code: "en-CA",
    title: "English (Canada)",
  },
  {
    code: "en-IN",
    title: "English (India)",
  },
  {
    code: "en-NZ",
    title: "English (New Zealand)",
  },
  {
    code: "en-ZA",
    title: "English (South Africa)",
  },
  {
    code: "en-GB",
    title: "English (Great Britain)",
  },
  {
    code: "en-US",
    title: "English (United States)",
  },
  {
    code: "en-TT",
    title: "English (Trinidad)",
  },
  {
    code: "en-PH",
    title: "English (Phillippines)",
  },
  {
    code: "en-JM",
    title: "English (Jamaica)",
  },
  {
    code: "en-IE",
    title: "English (Ireland)",
  },
  {
    code: "en-CB",
    title: "English (Caribbean)",
  },
  {
    code: "en-BZ",
    title: "English (Belize)",
  },
  {
    code: "eo",
    title: "Esperanto",
  },
  {
    code: "et",
    title: "Estonian",
  },
  {
    code: "fo",
    title: "Faroese",
  },
  {
    code: "fil",
    title: "Filipino",
  },
  {
    code: "fi",
    title: "Finnish",
  },
  {
    code: "fr",
    title: "French",
  },
  {
    code: "fr-CA",
    title: "French (Canada)",
  },
  {
    code: "fr-FR",
    title: "French (France)",
  },
  {
    code: "fr-CH",
    title: "French (Switzerland)",
  },
  {
    code: "fr-LU",
    title: "French (Luxembourg)",
  },
  {
    code: "fr-BE",
    title: "French (Belgium)",
  },
  {
    code: "fy",
    title: "Frisian",
  },
  {
    code: "gl",
    title: "Galician",
  },
  {
    code: "ka",
    title: "Georgian",
  },
  {
    code: "de",
    title: "German",
  },
  {
    code: "de-AT",
    title: "German (Austria)",
  },
  {
    code: "de-DE",
    title: "German (Germany)",
  },
  {
    code: "de-LI",
    title: "German (Liechtenstein)",
  },
  {
    code: "de-CH",
    title: "German (Switzerland)",
  },
  {
    code: "de-LU",
    title: "German (Luxembourg)",
  },
  {
    code: "el",
    title: "Greek",
  },
  {
    code: "gn",
    title: "Guarani",
  },
  {
    code: "gu",
    title: "Gujarati",
  },
  {
    code: "ha",
    title: "Hausa",
  },
  {
    code: "haw",
    title: "Hawaiian",
  },
  {
    code: "he",
    title: "Hebrew",
  },
  {
    code: "hi",
    title: "Hindi",
  },
  {
    code: "hu",
    title: "Hungarian",
  },
  {
    code: "is",
    title: "Icelandic",
  },
  {
    code: "id",
    title: "Indonesian",
  },
  {
    code: "ia",
    title: "Interlingua",
  },
  {
    code: "ga",
    title: "Irish",
  },
  {
    code: "it",
    title: "Italian",
  },
  {
    code: "it-IT",
    title: "Italian (Italy)",
  },
  {
    code: "it-CH",
    title: "Italian (Switzerland)",
  },
  {
    code: "ja",
    title: "Japanese",
  },
  {
    code: "jv",
    title: "Javanese",
  },
  {
    code: "kn",
    title: "Kannada",
  },
  {
    code: "kk",
    title: "Kazakh",
  },
  {
    code: "ko",
    title: "Korean",
  },
  {
    code: "ku",
    title: "Kurdish (Kurmanji)",
  },
  {
    code: "ky",
    title: "Kyrgyz",
  },
  {
    code: "lo",
    title: "Laothian",
  },
  {
    code: "la",
    title: "Latin",
  },
  {
    code: "lv",
    title: "Latvian",
  },
  {
    code: "ln",
    title: "Lingala",
  },
  {
    code: "lt",
    title: "Lithuanian",
  },
  {
    code: "lt-LT",
    title: "Lithuanian (Lithuania)",
  },
  {
    code: "mk",
    title: "Macedonian",
  },
  {
    code: "ms",
    title: "Malay",
  },
  {
    code: "ms-MY",
    title: "Malay (Malaysia)",
  },
  {
    code: "ms-BN",
    title: "Malay (Brunei)",
  },
  {
    code: "ml",
    title: "Malayalam",
  },
  {
    code: "mt",
    title: "Maltese",
  },
  {
    code: "mr",
    title: "Marathi",
  },
  {
    code: "mo",
    title: "Moldavian",
  },
  {
    code: "mn",
    title: "Mongolian",
  },
  {
    code: "ne",
    title: "Nepali",
  },
  {
    code: "no",
    title: "Norwegian",
  },
  {
    code: "nb",
    title: "Norwegian (Bokmal)",
  },
  {
    code: "nn",
    title: "Norwegian (Nynorsk)",
  },
  {
    code: "oc",
    title: "Occitan",
  },
  {
    code: "or",
    title: "Oriya",
  },
  {
    code: "om",
    title: "Oromo",
  },
  {
    code: "ps",
    title: "Pashto",
  },
  {
    code: "fa",
    title: "Persian",
  },
  {
    code: "pl",
    title: "Polish",
  },
  {
    code: "pt",
    title: "Portuguese",
  },
  {
    code: "pt-BR",
    title: "Portuguese (Brazil)",
  },
  {
    code: "pt-PT",
    title: "Portuguese (Portugal)",
  },
  {
    code: "pa",
    title: "Punjabi",
  },
  {
    code: "qu",
    title: "Quechua",
  },
  {
    code: "ro",
    title: "Romanian",
  },
  {
    code: "ro-MO",
    title: "Romanian (Moldova)",
  },
  {
    code: "ro-RO",
    title: "Romanian (Romania)",
  },
  {
    code: "rm",
    title: "Romansh",
  },
  {
    code: "ru",
    title: "Russian",
  },
  {
    code: "ru-MO",
    title: "Russian (Moldova)",
  },
  {
    code: "gd",
    title: "Gaelic",
  },
  {
    code: "gd-IE",
    title: "Gaelic (Ireland)",
  },
  {
    code: "sr",
    title: "Serbian",
  },
  {
    code: "sh",
    title: "Serbo-Croatian",
  },
  {
    code: "st",
    title: "Sesotho",
  },
  {
    code: "sn",
    title: "Shona",
  },
  {
    code: "sd",
    title: "Sindhi",
  },
  {
    code: "si",
    title: "Sinhalese",
  },
  {
    code: "sk",
    title: "Slovak",
  },
  {
    code: "sk-SK",
    title: "Slovak (Slovakia)",
  },
  {
    code: "sl",
    title: "Slovenian",
  },
  {
    code: "sl-SI",
    title: "Slovenian (Slovenia)",
  },
  {
    code: "so",
    title: "Somali",
  },
  {
    code: "es",
    title: "Spanish",
  },
  {
    code: "es-AR",
    title: "Spanish (Argentina)",
  },
  {
    code: "es-CL",
    title: "Spanish (Chile)",
  },
  {
    code: "es-CO",
    title: "Spanish (Colombia)",
  },
  {
    code: "es-CR",
    title: "Spanish (Costa Rica)",
  },
  {
    code: "es-HN",
    title: "Spanish (Honduras)",
  },
  {
    code: "es-419",
    title: "Spanish (Latin America)",
  },
  {
    code: "es-MX",
    title: "Spanish (Mexico)",
  },
  {
    code: "es-PE",
    title: "Spanish (Peru)",
  },
  {
    code: "es-ES",
    title: "Spanish (Spain)",
  },
  {
    code: "es-US",
    title: "Spanish (US)",
  },
  {
    code: "es-UY",
    title: "Spanish (Uruguay)",
  },
  {
    code: "es-VE",
    title: "Spanish (Venezuela)",
  },
  {
    code: "es-SV",
    title: "Spanish (El Salvador)",
  },
  {
    code: "es-PY",
    title: "Spanish (Paraguay)",
  },
  {
    code: "es-PR",
    title: "Spanish (Puerto Rico)",
  },
  {
    code: "es-PA",
    title: "Spanish (Panama)",
  },
  {
    code: "es-NI",
    title: "Spanish (Nicaragua)",
  },
  {
    code: "es-GT",
    title: "Spanish (Guatemala)",
  },
  {
    code: "es-EC",
    title: "Spanish (Ecuador)",
  },
  {
    code: "es-DO",
    title: "Spanish (Dominican Republic)",
  },
  {
    code: "es-BO",
    title: "Spanish (Bolivia)",
  },
  {
    code: "su",
    title: "Sundanese",
  },
  {
    code: "sw",
    title: "Swahili",
  },
  {
    code: "sv",
    title: "Swedish",
  },
  {
    code: "sv-SE",
    title: "Swedish (Sweden)",
  },
  {
    code: "sv-FI",
    title: "Swedish (Finland)",
  },
  {
    code: "tg",
    title: "Tajik",
  },
  {
    code: "ta",
    title: "Tamil",
  },
  {
    code: "tt",
    title: "Tatar",
  },
  {
    code: "te",
    title: "Telugu",
  },
  {
    code: "th",
    title: "Thai",
  },
  {
    code: "ti",
    title: "Tigrinya",
  },
  {
    code: "to",
    title: "Tonga",
  },
  {
    code: "tr",
    title: "Turkish",
  },
  {
    code: "tk",
    title: "Turkmen",
  },
  {
    code: "tw",
    title: "Twi",
  },
  {
    code: "ug",
    title: "Uighur",
  },
  {
    code: "uk",
    title: "Ukrainian",
  },
  {
    code: "ur",
    title: "Urdu",
  },
  {
    code: "uz",
    title: "Uzbek",
  },
  {
    code: "vi",
    title: "Vietnamese",
  },
  {
    code: "wa",
    title: "Walloon",
  },
  {
    code: "cy",
    title: "Welsh",
  },
  {
    code: "xh",
    title: "Xhosa",
  },
  {
    code: "yi",
    title: "Yiddish",
  },
  {
    code: "yo",
    title: "Yoruba",
  },
  {
    code: "zu",
    title: "Zulu",
  },
  {
    code: "mi",
    title: "Maori",
  },
  {
    code: "mhr",
    title: "Eastern Mari",
  },
  {
    code: "ba",
    title: "Bashkir",
  },
  {
    code: "my",
    title: "Burmese",
  },
  {
    code: "pap",
    title: "Papiamento",
  },
  {
    code: "ht",
    title: "Haitian",
  },
  {
    code: "ceb",
    title: "Cebuano",
  },
  {
    code: "mrj",
    title: "Hill Mari",
  },
  {
    code: "tl",
    title: "Tagalog",
  },
  {
    code: "udm",
    title: "Udmurt",
  },
  {
    code: "lb",
    title: "Luxembourgish",
  },
  {
    code: "mg",
    title: "Malagasy",
  },
  {
    code: "yue",
    title: "Cantonese",
  },
  {
    code: "lzh",
    title: "Chinese (Classical)",
  },
  {
    code: "hmn",
    title: "Hmong",
  },
  {
    code: "ig",
    title: "Igbo",
  },
  {
    code: "ny",
    title: "Nyanja",
  },
  {
    code: "sm",
    title: "Samoan",
  },
  {
    code: "fj",
    title: "Fijian",
  },
  {
    code: "ty",
    title: "Tahitian",
  },

  {
    code: "ak",
    title: "Akan",
  },
  {
    code: "as",
    title: "Assamese",
  },
  {
    code: "bm",
    title: "Bambara",
  },
  {
    code: "ce",
    title: "Chechen",
  },
  {
    code: "cu",
    title: "Church Slavic",
  },
  {
    code: "kw",
    title: "Cornish",
  },
  {
    code: "dz",
    title: "Dzongkha",
  },
  {
    code: "ee",
    title: "Ewe",
  },
  {
    code: "ff",
    title: "Fulah",
  },
  {
    code: "lg",
    title: "Ganda",
  },
  {
    code: "kl",
    title: "Kalaallisut",
  },
  {
    code: "ks",
    title: "Kashmiri",
  },
  {
    code: "ki",
    title: "Kikuyu",
  },
  {
    code: "rw",
    title: "Kinyarwanda",
  },
  {
    code: "lu",
    title: "Luba-Katanga",
  },
  {
    code: "gv",
    title: "Manx",
  },
  {
    code: "nd",
    title: "North Ndebele",
  },
  {
    code: "se",
    title: "Northern Sami",
  },
  {
    code: "os",
    title: "Ossetic",
  },
  {
    code: "rn",
    title: "Rundi",
  },
  {
    code: "sg",
    title: "Sango",
  },
  {
    code: "ii",
    title: "Sichuan Yi",
  },
  {
    code: "bo",
    title: "Tibetan",
  },
  {
    code: "vo",
    title: "Volapük",
  },
  {
    code: "wo",
    title: "Wolof",
  },
];
