import { createSelector } from "reselect";

import { get } from "lodash";

const baseState = state => get(state, "config", null);

export const config = createSelector(baseState, () => {
  return {
    shopifyPartnerId: 365634,
  };
});
