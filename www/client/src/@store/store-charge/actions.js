import { createRequestAction } from "@store/createRequestAction";
import { createAction } from "redux-actions";

export const STORE_CHARGE_FETCH = "STORE_CHARGE_FETCH";
export const STORE_CHARGE_RESET = "STORE_CHARGE_RESET";

export const fetchStoreChargeAction = createRequestAction(STORE_CHARGE_FETCH, (domain, params, cancelToken) => {
  return {
    request: {
      method: "GET",
      url: `/api/stores/${domain}/charges`,
      params,
      cancelToken,
    },
  };
});
export const resetStoreChargeAction = createAction(STORE_CHARGE_RESET);
