import { fetchStoreChargeAction, resetStoreChargeAction } from "./actions";
import { get } from "lodash";
import { handleActions } from "redux-actions";

const defaultState = {
  isFetched: false,
  isLoading: false,
  storeCharge: [],
  total: 0,
};

const loadingStartHandler = state => {
  return {
    ...state,
    isLoading: true,
  };
};

const loadingEndHandler = state => {
  return {
    ...state,
    isFetched: true,
    isLoading: false,
  };
};

const fetchStoreChargeSuccessHandler = (state, { payload }) => {
  const data = get(payload, "data.data");

  return {
    ...state,
    storeCharge: data.items,
    total: data.count,
    isFetched: true,
    isLoading: false,
  };
};

const resetStoreChargeHandler = () => {
  return defaultState;
};

export const storeCharge = handleActions(
  {
    [fetchStoreChargeAction]: loadingStartHandler,
    [fetchStoreChargeAction.success]: fetchStoreChargeSuccessHandler,
    [fetchStoreChargeAction.fail]: loadingEndHandler,

    [resetStoreChargeAction]: resetStoreChargeHandler,
  },
  defaultState
);
