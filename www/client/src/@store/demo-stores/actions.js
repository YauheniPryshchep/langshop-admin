import { createRequestAction } from "@store/createRequestAction";
import { createAction } from "redux-actions";

export const DEMO_STORES_FETCH = "DEMO_STORES_FETCH";
export const DEMO_STORES_CREATE = "DEMO_STORES_CREATE";
export const DEMO_STORES_REMOVE = "DEMO_STORES_REMOVE";
export const DEMO_STORES_RESET = "DEMO_STORES_RESET";

export const fetchDemoStoresAction = createRequestAction(DEMO_STORES_FETCH, (params, cancelToken) => {
  return {
    request: {
      method: "GET",
      url: `/api/rebate/demo-stores`,
      params,
      cancelToken,
    },
  };
});

export const createDemoStoreAction = createRequestAction(DEMO_STORES_CREATE, data => {
  return {
    request: {
      method: "POST",
      url: `/api/rebate/demo-stores`,
      data,
    },
  };
});

export const removeDemoStoreAction = createRequestAction(DEMO_STORES_REMOVE, domain => {
  return {
    request: {
      method: "DELETE",
      url: `/api/rebate/demo-stores/${domain}`,
    },
  };
});

export const resetDemoStoresAction = createAction(DEMO_STORES_RESET);
