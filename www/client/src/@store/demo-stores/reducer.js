import { fetchDemoStoresAction, createDemoStoreAction, removeDemoStoreAction, resetDemoStoresAction } from "./actions";
import { get } from "lodash";
import { handleActions } from "redux-actions";

const defaultState = {
  isFetched: false,
  isLoading: false,
  demoStores: [],
  total: 0,
};

const loadingStartHandler = state => {
  return {
    ...state,
    isLoading: true,
  };
};

const loadingEndHandler = state => {
  return {
    ...state,
    isFetched: true,
    isLoading: false,
  };
};

const fetchDemoStoresSuccessHandler = (state, { payload }) => {
  const data = get(payload, "data.data");

  return {
    ...state,
    demoStores: data.items,
    total: data.count,
    isFetched: true,
    isLoading: false,
  };
};

const resetDemoStoresHandler = () => {
  return defaultState;
};

export const demoStores = handleActions(
  {
    [fetchDemoStoresAction]: loadingStartHandler,
    [fetchDemoStoresAction.success]: fetchDemoStoresSuccessHandler,
    [fetchDemoStoresAction.fail]: loadingEndHandler,

    [createDemoStoreAction]: loadingStartHandler,
    [createDemoStoreAction.success]: loadingEndHandler,
    [createDemoStoreAction.fail]: loadingEndHandler,

    [removeDemoStoreAction]: loadingStartHandler,
    [removeDemoStoreAction.success]: loadingEndHandler,
    [removeDemoStoreAction.fail]: loadingEndHandler,

    [resetDemoStoresAction]: resetDemoStoresHandler,
  },
  defaultState
);
