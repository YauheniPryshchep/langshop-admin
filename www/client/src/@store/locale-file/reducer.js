import { fetchLocaleFileAction, updateLocaleFileAction, resetLocaleFileAction } from "./actions";
import { get } from "lodash";
import { handleActions } from "redux-actions";

const defaultState = {
  isFetched: false,
  isLoading: false,
  localeFile: null,
};

const loadingStartHandler = state => {
  return {
    ...state,
    isLoading: true,
  };
};

const loadingEndHandler = state => {
  return {
    ...state,
    isFetched: true,
    isLoading: false,
  };
};

const fetchLocaleFileSuccessHandler = (state, { payload }) => {
  const data = get(payload, "data");

  return {
    ...state,
    localeFile: data,
    isFetched: true,
    isLoading: false,
  };
};

const updateLocaleFileSuccessHandler = (state, { payload }) => {
  const data = get(payload, "data");

  return {
    ...state,
    localeFile: data,
    isFetched: true,
    isLoading: false,
  };
};

const resetLocaleFileHandler = () => {
  return defaultState;
};

export const localeFile = handleActions(
  {
    [fetchLocaleFileAction]: loadingStartHandler,
    [fetchLocaleFileAction.success]: fetchLocaleFileSuccessHandler,
    [fetchLocaleFileAction.fail]: loadingEndHandler,

    [updateLocaleFileAction]: loadingStartHandler,
    [updateLocaleFileAction.success]: updateLocaleFileSuccessHandler,
    [updateLocaleFileAction.fail]: loadingEndHandler,

    [resetLocaleFileAction]: resetLocaleFileHandler,
  },
  defaultState
);
