import { createSelector } from "reselect";

import { get } from "lodash";

const baseState = state => get(state, "localeFile", null);

export const isFetched = createSelector(baseState, state => get(state, "isFetched", false));

export const isLoading = createSelector(baseState, state => get(state, "isLoading", false));

export const isLoaded = createSelector(isLoading, isFetched, (isLoading, isFetched) => !isLoading && isFetched);

export const localeFile = createSelector(baseState, state => get(state, "localeFile", null));

export const initialValues = createSelector(
  localeFile,
  state =>
    state || {
      language: "",
      items: [],
    }
);
