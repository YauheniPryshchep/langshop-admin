import { createRequestAction } from "@store/createRequestAction";
import { createAction } from "redux-actions";

export const LOCALE_FILE_FETCH = "LOCALE_FILE_FETCH";
export const LOCALE_FILE_UPDATE = "LOCALE_FILE_UPDATE";
export const LOCALE_FILE_RESET = "LOCALE_FILE_RESET";

export const fetchLocaleFileAction = createRequestAction(LOCALE_FILE_FETCH, (locale, file, cancelToken) => {
  return {
    request: {
      method: "GET",
      url: `/api/locales/${locale}/${file}`,
      cancelToken,
    },
  };
});

export const updateLocaleFileAction = createRequestAction(LOCALE_FILE_UPDATE, (locale, file, data) => {
  return {
    request: {
      method: "PUT",
      url: `/api/locales/${locale}/${file}`,
      data,
    },
  };
});

export const resetLocaleFileAction = createAction(LOCALE_FILE_RESET);
