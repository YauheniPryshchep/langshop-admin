import { createSelector } from "reselect";

import { get } from "lodash";

const baseState = state => get(state, "news", null);

export const isFetched = createSelector(baseState, state => get(state, "isFetched", false));

export const isLoading = createSelector(baseState, state => get(state, "isLoading", false));

export const isLoaded = createSelector(isLoading, isFetched, (isLoading, isFetched) => !isLoading && isFetched);

export const news = createSelector(baseState, state => get(state, "news", []));

export const total = createSelector(baseState, state => get(state, "total", 0));
