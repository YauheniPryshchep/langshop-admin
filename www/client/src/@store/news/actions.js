import { createRequestAction } from "@store/createRequestAction";
import { createAction } from "redux-actions";

export const NEWS_FETCH = "NEWS_FETCH";
export const NEWS_CREATE = "NEWS_CREATE";
export const NEWS_REMOVE = "NEWS_REMOVE";
export const NEWS_RESET = "NEWS_RESET";

export const fetchNewsAction = createRequestAction(NEWS_FETCH, (params, cancelToken) => {
  return {
    request: {
      method: "GET",
      url: `/api/news`,
      params,
      cancelToken,
    },
  };
});

export const createNewsItemAction = createRequestAction(NEWS_CREATE, data => {
  return {
    request: {
      method: "POST",
      url: `/api/news`,
      data,
    },
  };
});

export const removeNewsItemAction = createRequestAction(NEWS_REMOVE, itemId => {
  return {
    request: {
      method: "DELETE",
      url: `/api/news/${itemId}`,
    },
  };
});

export const resetNewsAction = createAction(NEWS_RESET);
