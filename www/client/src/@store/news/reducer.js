import { fetchNewsAction, createNewsItemAction, removeNewsItemAction, resetNewsAction } from "./actions";
import { get } from "lodash";
import { handleActions } from "redux-actions";

const defaultState = {
  isFetched: false,
  isLoading: false,
  news: [],
  total: 0,
};

const loadingStartHandler = state => {
  return {
    ...state,
    isLoading: true,
  };
};

const loadingEndHandler = state => {
  return {
    ...state,
    isFetched: true,
    isLoading: false,
  };
};

const fetchNewsSuccessHandler = (state, { payload }) => {
  const data = get(payload, "data.data");

  return {
    ...state,
    news: data.items,
    total: data.count,
    isFetched: true,
    isLoading: false,
  };
};

const resetNewsHandler = () => {
  return defaultState;
};

export const news = handleActions(
  {
    [fetchNewsAction]: loadingStartHandler,
    [fetchNewsAction.success]: fetchNewsSuccessHandler,
    [fetchNewsAction.fail]: loadingEndHandler,

    [createNewsItemAction]: loadingStartHandler,
    [createNewsItemAction.success]: loadingEndHandler,
    [createNewsItemAction.fail]: loadingEndHandler,

    [removeNewsItemAction]: loadingStartHandler,
    [removeNewsItemAction.success]: fetchNewsSuccessHandler,
    [removeNewsItemAction.fail]: loadingEndHandler,

    [resetNewsAction]: resetNewsHandler,
  },
  defaultState
);
