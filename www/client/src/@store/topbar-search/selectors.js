import { createSelector } from "reselect";

import { get } from "lodash";

const baseState = state => get(state, "appTopBarSearch", null);

export const isSearched = createSelector(baseState, state => get(state, "isSearched", false));

export const isSearching = createSelector(baseState, state => get(state, "isSearching", false));

export const searchResults = createSelector(baseState, state => get(state, "searchResults", []));
