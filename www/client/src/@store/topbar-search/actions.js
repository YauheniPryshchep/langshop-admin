import { createRequestAction } from "@store/createRequestAction";
import { createAction } from "redux-actions";

export const APP_TOP_BAR_SEARCH_FETCH = "APP_TOP_BAR_SEARCH_FETCH";
export const APP_TOP_BAR_SEARCH_RESET = "APP_TOP_BAR_SEARCH_RESET";

export const fetchSearchResultsAction = createRequestAction(APP_TOP_BAR_SEARCH_FETCH, domain => {
  return {
    request: {
      method: "GET",
      url: `/api/stores`,
      params: {
        filter: domain,
        page: 0,
        limit: 250,
        sort: {
          field: "name",
          direction: "asc",
        },
      },
    },
  };
});

export const resetSearchResultsAction = createAction(APP_TOP_BAR_SEARCH_RESET);
