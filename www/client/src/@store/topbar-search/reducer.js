import { fetchSearchResultsAction, resetSearchResultsAction } from "./actions";
import { get } from "lodash";
import { handleActions } from "redux-actions";

const defaultState = {
  isSearched: false,
  isSearching: false,
  searchResults: [],
};

const loadingStartHandler = state => {
  return {
    ...state,
    isSearching: true,
  };
};

const loadingEndHandler = state => {
  return {
    ...state,
    isSearched: true,
    isSearching: false,
  };
};

const fetchSearchResultsSuccessHandler = (state, { payload }) => {
  const data = get(payload, "data.data");

  return {
    ...state,
    searchResults: data.items,
    isSearched: true,
    isSearching: false,
  };
};

const resetSearchResultsHandler = () => {
  return defaultState;
};

export const appTopBarSearch = handleActions(
  {
    [fetchSearchResultsAction]: loadingStartHandler,
    [fetchSearchResultsAction.success]: fetchSearchResultsSuccessHandler,
    [fetchSearchResultsAction.fail]: loadingEndHandler,

    [resetSearchResultsAction]: resetSearchResultsHandler,
  },
  defaultState
);
