import { fetchLocalesAction, resetLocalesAction } from "./actions";
import { get } from "lodash";
import { handleActions } from "redux-actions";

const defaultState = {
  isFetched: false,
  isLoading: false,
  locales: [],
  total: 0,
};
const loadingStartHandler = state => {
  return {
    ...state,
    isLoading: true,
  };
};

const loadingEndHandler = state => {
  return {
    ...state,
    isFetched: true,
    isLoading: false,
  };
};

const fetchLocalesSuccessHandler = (state, { payload }) => {
  const data = get(payload, "data");

  return {
    ...state,
    locales: data.items,
    total: data.total,
    isFetched: true,
    isLoading: false,
  };
};

const resetLocalesHandler = () => {
  return defaultState;
};

export const locales = handleActions(
  {
    [fetchLocalesAction]: loadingStartHandler,
    [fetchLocalesAction.success]: fetchLocalesSuccessHandler,
    [fetchLocalesAction.fail]: loadingEndHandler,

    [resetLocalesAction]: resetLocalesHandler,
  },
  defaultState
);
