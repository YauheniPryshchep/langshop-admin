import { createRequestAction } from "@store/createRequestAction";
import { createAction } from "redux-actions";

export const LOCALES_FETCH = "LOCALES_FETCH";
export const LOCALES_RESET = "LOCALES_RESET";

export const fetchLocalesAction = createRequestAction(LOCALES_FETCH, (params, cancelToken) => {
  return {
    request: {
      method: "GET",
      url: `/api/locales`,
      params,
      cancelToken,
    },
  };
});

export const resetLocalesAction = createAction(LOCALES_RESET);
