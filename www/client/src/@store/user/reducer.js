import { fetchUserAction, updateUserAction, resetUserAction } from "./actions";
import { get } from "lodash";
import { handleActions } from "redux-actions";

const defaultState = {
  isFetched: false,
  isLoading: false,
  user: null,
};

const loadingStartHandler = state => {
  return {
    ...state,
    isLoading: true,
  };
};

const loadingEndHandler = state => {
  return {
    ...state,
    isFetched: true,
    isLoading: false,
  };
};

const fetchUserSuccessHandler = (state, { payload }) => {
  const data = get(payload, "data.data");

  return {
    ...state,
    user: data,
    isFetched: true,
    isLoading: false,
  };
};

const updateUserSuccessHandler = (state, { payload }) => {
  const data = get(payload, "data.data");

  return {
    ...state,
    user: data,
    isFetched: true,
    isLoading: false,
  };
};

const resetUserHandler = () => {
  return defaultState;
};

export const user = handleActions(
  {
    [fetchUserAction]: loadingStartHandler,
    [fetchUserAction.success]: fetchUserSuccessHandler,
    [fetchUserAction.fail]: loadingEndHandler,

    [updateUserAction]: loadingStartHandler,
    [updateUserAction.success]: updateUserSuccessHandler,
    [updateUserAction.fail]: loadingEndHandler,

    [resetUserAction]: resetUserHandler,
  },
  defaultState
);
