import { createSelector } from "reselect";

import { get } from "lodash";

const baseState = state => get(state, "user", null);

export const isFetched = createSelector(baseState, state => get(state, "isFetched", false));

export const isLoading = createSelector(baseState, state => get(state, "isLoading", false));

export const isLoaded = createSelector(isLoading, isFetched, (isLoading, isFetched) => !isLoading && isFetched);

export const user = createSelector(baseState, state => get(state, "user", null));

export const initialValues = createSelector(
  user,
  state =>
    state || {
      name: "",
      email: "",
      photo: "",
      roles: [],
      scopes: [],
    }
);
