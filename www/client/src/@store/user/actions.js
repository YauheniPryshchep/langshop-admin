import { createRequestAction } from "@store/createRequestAction";
import { createAction } from "redux-actions";

export const USER_FETCH = "USER_FETCH";
export const USER_UPDATE = "USER_UPDATE";
export const USER_UNASSIGN = "USER_UNASSIGN";
export const USER_RESET = "USER_RESET";

export const fetchUserAction = createRequestAction(USER_FETCH, userId => {
  return {
    request: {
      method: "GET",
      url: `/api/users/${userId}`,
    },
  };
});

export const updateUserAction = createRequestAction(USER_UPDATE, (userId, data) => {
  return {
    request: {
      method: "PUT",
      url: `/api/users/${userId}`,
      data,
    },
  };
});

export const resetUserAction = createAction(USER_RESET);
