import { fetchUserAction, resetUserAction } from "@store/user/actions";
import { fetchRolesAction, resetRolesAction } from "@store/roles";
import { fetchScopesAction, resetScopesAction } from "@store/scopes";
import { isLoaded as isRolesLoaded } from "@store/roles";
import { isLoaded as isScopesLoaded } from "@store/scopes";
import { isLoaded as isUserLoaded } from "@store/user";
import { createSelector } from "reselect";

export const isFormLoaded = createSelector(
  isRolesLoaded,
  isUserLoaded,
  isScopesLoaded,
  (isRolesLoaded, isUserLoaded, isScopesLoaded) => isRolesLoaded && isUserLoaded && isScopesLoaded
);

export const fetchUserForm = userId => dispatch => {
  return Promise.all([dispatch(fetchRolesAction()), dispatch(fetchUserAction(userId)), dispatch(fetchScopesAction())]);
};

export const resetUserForm = () => dispatch => {
  return Promise.all([dispatch(resetUserAction()), dispatch(resetRolesAction()), dispatch(resetScopesAction())]);
};
