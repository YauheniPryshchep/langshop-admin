import { fetchCompaniesAction, resetCompaniesAction, removeCompanyAction } from "./actions";
import { get } from "lodash";
import { handleActions } from "redux-actions";

const defaultState = {
  isFetched: false,
  isLoading: false,
  companies: [],
};

const loadingStartHandler = state => {
  return {
    ...state,
    isLoading: true,
  };
};

const loadingEndHandler = state => {
  return {
    ...state,
    isFetched: true,
    isLoading: false,
  };
};

const fetchCompaniesSuccessHandler = (state, { payload }) => {
  const data = get(payload, "data");

  return {
    ...state,
    companies: data.items,
    isFetched: true,
    isLoading: false,
  };
};

const resetCompaniesHandler = () => {
  return defaultState;
};

export const companies = handleActions(
  {
    [fetchCompaniesAction]: loadingStartHandler,
    [fetchCompaniesAction.success]: fetchCompaniesSuccessHandler,
    [fetchCompaniesAction.fail]: loadingEndHandler,

    [removeCompanyAction]: loadingStartHandler,
    [removeCompanyAction.success]: loadingEndHandler,
    [removeCompanyAction.fail]: loadingEndHandler,

    [resetCompaniesAction]: resetCompaniesHandler,
  },
  defaultState
);
