import { createRequestAction } from "@store/createRequestAction";
import { createAction } from "redux-actions";

export const COMPANIES_FETCH = "COMPANIES_FETCH";
export const COMPANIES_RESET = "COMPANIES_RESET";
export const COMPANY_REMOVE = "COMPANY_REMOVE";

export const fetchCompaniesAction = createRequestAction(COMPANIES_FETCH, (params, cancelToken) => {
  return {
    request: {
      method: "GET",
      url: `/api/companies`,
      params,
      cancelToken,
    },
  };
});

export const removeCompanyAction = createRequestAction(COMPANY_REMOVE, id => {
  return {
    request: {
      method: "DELETE",
      url: `/api/companies/${id}`,
    },
  };
});

export const resetCompaniesAction = createAction(COMPANIES_RESET);
