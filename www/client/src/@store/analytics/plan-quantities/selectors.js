import {createSelector} from "reselect";

import get from "lodash/get";
import reduce from "lodash/reduce";

const baseState = state => get(state, "planQuantities", null);

export const now = createSelector(baseState, state => get(state, "now", null));
export const sevenDaysAgo = createSelector(baseState, state => get(state, "sevenDaysAgo", null));
export const isLoading = createSelector(baseState, state => get(state, "isLoading", null));
export const isFetched = createSelector(baseState, state => get(state, "isFetched", null));
export const isFailed = createSelector(baseState, state => get(state, "isFailed", null));

export const planQuantities = createSelector(now, sevenDaysAgo, (now, sevenDaysAgo) => {
  return reduce(now, (acc, value, key) => {
    const type = getType(value, sevenDaysAgo[key]);
    const diff = changedInValue(value, sevenDaysAgo[key]);
    return [
      ...acc,
      {
        key,
        value,
        diff,
        type
      }]
  }, [])
});

const getType = (currentValue, prevValue) => {
  if (currentValue === prevValue) {
    return 'equal'
  }

  if (currentValue < prevValue) {
    return "decrease"
  }
  return "increase"
};

const changedInValue = (currentValue, prevValue) => {
  if (currentValue === prevValue) {
    return 0;
  }

  if (currentValue < prevValue) {
    return prevValue - currentValue;
  }

  return currentValue - prevValue;
};