import {createRequestAction} from "@store/createRequestAction";
import {createAction} from "redux-actions";
import moment from "moment";

export const PLAN_QUANTITIES = "PLAN_QUANTITIES";
export const PLAN_QUANTITIES_RESET = "PLAN_QUANTITIES_RESET";


export const planQuantitiesAction = createRequestAction(PLAN_QUANTITIES, () => {
  return {
    request: {
      method: "GET",
      url: "/api/analytics/plan-quantities",
    },
  };
});

export const resetPlanQuantitiesAction = createAction(PLAN_QUANTITIES_RESET);

export const planQuantitiesSevenDaysAgoAction = createRequestAction(PLAN_QUANTITIES, () => {
  return {
    request: {
      method: "GET",
      url: "/api/analytics/plan-quantities",
      params: {
        date: moment().subtract(7, 'd').toDate()
      }
    },
  };
});
