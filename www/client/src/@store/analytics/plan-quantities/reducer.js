import {planQuantitiesAction, planQuantitiesSevenDaysAgoAction, resetPlanQuantitiesAction} from "./actions";
import {get} from "lodash";
import {handleActions} from "redux-actions";

const defaultState = {
  isFetched: false,
  isLoading: false,
  now: {},
  sevenDaysAgo: {}
};

const loadingStartHandler = state => {
  return {
    ...state,
    isLoading: true,
  };
};

const loadingEndHandler = state => {
  return {
    ...state,
    isFetched: true,
    isLoading: false,
  };
};

const fetchPlanQuantitiesSuccessHandler = (state, {payload}) => {
  const data = get(payload, "data.data");
  return {
    ...state,
    ...data,
    isFetched: true,
    isLoading: false,
  };
};

const resetPlanQuantitiesHandler = () => {
  return defaultState;
};

export const planQuantities = handleActions(
  {
    [planQuantitiesAction]: loadingStartHandler,
    [planQuantitiesAction.success]: fetchPlanQuantitiesSuccessHandler,
    [planQuantitiesAction.fail]: loadingEndHandler,

    [planQuantitiesSevenDaysAgoAction]: loadingStartHandler,
    [planQuantitiesSevenDaysAgoAction.success]: fetchPlanQuantitiesSuccessHandler,
    [planQuantitiesSevenDaysAgoAction.fail]: loadingEndHandler,

    [resetPlanQuantitiesAction]: resetPlanQuantitiesHandler,
  },
  defaultState
);
