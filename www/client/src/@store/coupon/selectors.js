import {createSelector} from "reselect";

import {get} from "lodash";
import moment from "moment-timezone";

const baseState = state => get(state, "coupon", null);

export const isFetched = createSelector(baseState, state => get(state, "isFetched", false));

export const isLoading = createSelector(baseState, state => get(state, "isLoading", false));

export const isLoaded = createSelector(isLoading, isFetched, (isLoading, isFetched) => !isLoading && isFetched);

export const coupon = createSelector(baseState, state => {
  const coupon = get(state, "coupon", null);
  if (coupon) {

    return {
      ...coupon,
      start_at: moment.utc(coupon.start_at).local().toDate(),
      end_at: coupon.end_at ? moment.utc(coupon.end_at).local().toDate() : null,
    }
  }
  return coupon;
});

export const initialValues = createSelector(
  coupon,
  state =>
    state || {
      name: "",
      status: 1,
      discount_type: 1,
      discount_value: 0,
      usage_limit: 1,
      reusable: false,
      start_at: moment.utc().format("YYYY-MM-DD HH:mm:ss"),
      end_at: null,
      plans: [],
    }
);
