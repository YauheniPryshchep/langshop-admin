import {
  fetchCouponAction,
  createCouponAction,
  updateCouponAction,
  removeCouponAction,
  resetCouponAction,
} from "./actions";
import { get } from "lodash";
import { handleActions } from "redux-actions";

const defaultState = {
  isFetched: false,
  isLoading: false,
  coupon: null,
};

const loadingStartHandler = state => {
  return {
    ...state,
    isLoading: true,
  };
};

const loadingEndHandler = state => {
  return {
    ...state,
    isFetched: true,
    isLoading: false,
  };
};

const fetchCouponSuccessHandler = (state, { payload }) => {
  const data = get(payload, "data.data");

  return {
    ...state,
    coupon: data,
    isFetched: true,
    isLoading: false,
  };
};

const createCouponSuccessHandler = (state, { payload }) => {
  const data = get(payload, "data.data");

  return {
    ...state,
    coupon: data,
    isFetched: true,
    isLoading: false,
  };
};

const updateCouponSuccessHandler = (state, { payload }) => {
  const data = get(payload, "data.data");

  return {
    ...state,
    coupon: data,
    isFetched: true,
    isLoading: false,
  };
};

const resetCouponHandler = () => {
  return defaultState;
};

export const coupon = handleActions(
  {
    [fetchCouponAction]: loadingStartHandler,
    [fetchCouponAction.success]: fetchCouponSuccessHandler,
    [fetchCouponAction.fail]: loadingEndHandler,

    [updateCouponAction]: loadingStartHandler,
    [updateCouponAction.success]: updateCouponSuccessHandler,
    [updateCouponAction.fail]: loadingEndHandler,

    [createCouponAction]: loadingStartHandler,
    [createCouponAction.success]: createCouponSuccessHandler,
    [createCouponAction.fail]: loadingEndHandler,

    [removeCouponAction]: loadingStartHandler,
    [removeCouponAction.success]: resetCouponHandler,
    [removeCouponAction.fail]: loadingEndHandler,

    [resetCouponAction]: resetCouponHandler,
  },
  defaultState
);
