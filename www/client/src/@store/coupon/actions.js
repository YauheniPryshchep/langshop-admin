import { createRequestAction } from "@store/createRequestAction";
import { createAction } from "redux-actions";

export const COUPON_FETCH = "COUPON_FETCH";
export const COUPON_CREATE = "COUPON_CREATE";
export const COUPON_UPDATE = "COUPON_UPDATE";
export const COUPON_DELETE = "COUPON_DELETE";
export const COUPON_RESET = "COUPON__RESET";

export const fetchCouponAction = createRequestAction(COUPON_FETCH, couponId => {
  return {
    request: {
      method: "GET",
      url: `/api/coupons/${couponId}`,
    },
  };
});

export const createCouponAction = createRequestAction(COUPON_CREATE, data => {
  return {
    request: {
      method: "POST",
      url: `/api/coupons`,
      data,
    },
  };
});

export const updateCouponAction = createRequestAction(COUPON_UPDATE, (couponId, data) => {
  return {
    request: {
      method: "PUT",
      url: `/api/coupons/${couponId}`,
      data,
    },
  };
});

export const removeCouponAction = createRequestAction(COUPON_DELETE, couponId => {
  return {
    request: {
      method: "DELETE",
      url: `/api/coupons/${couponId}`,
    },
  };
});

export const resetCouponAction = createAction(COUPON_RESET);
