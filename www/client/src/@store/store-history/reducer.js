import { fetchStoreHistoryAction, resetStoreHistoryAction } from "./actions";
import { get } from "lodash";
import { handleActions } from "redux-actions";

const defaultState = {
  isFetched: false,
  isLoading: false,
  storeHistory: [],
  total: 0,
};

const loadingStartHandler = state => {
  return {
    ...state,
    isLoading: true,
  };
};

const loadingEndHandler = state => {
  return {
    ...state,
    isFetched: true,
    isLoading: false,
  };
};

const fetchStoreHistorySuccessHandler = (state, { payload }) => {
  const data = get(payload, "data.data");

  return {
    ...state,
    storeHistory: data.items,
    total: data.count,
    isFetched: true,
    isLoading: false,
  };
};

const resetStoreHistoryHandler = () => {
  return defaultState;
};

export const storeHistory = handleActions(
  {
    [fetchStoreHistoryAction]: loadingStartHandler,
    [fetchStoreHistoryAction.success]: fetchStoreHistorySuccessHandler,
    [fetchStoreHistoryAction.fail]: loadingEndHandler,

    [resetStoreHistoryAction]: resetStoreHistoryHandler,
  },
  defaultState
);
