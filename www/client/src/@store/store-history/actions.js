import { createRequestAction } from "@store/createRequestAction";
import { createAction } from "redux-actions";

export const STORE_HISTORY_FETCH = "STORE_HISTORY_FETCH";
export const STORE_HISTORY_RESET = "STORE_HISTORY_RESET";

export const fetchStoreHistoryAction = createRequestAction(STORE_HISTORY_FETCH, (domain, params, cancelToken) => {
  return {
    request: {
      method: "GET",
      url: `/api/store-events/${domain}`,
      params,
      cancelToken,
    },
  };
});
export const resetStoreHistoryAction = createAction(STORE_HISTORY_RESET);
