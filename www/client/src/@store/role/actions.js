import { createRequestAction } from "@store/createRequestAction";
import { createAction } from "redux-actions";

export const ROLE_FETCH = "ROLE_FETCH";
export const ROLE_UPDATE = "ROLE_UPDATE";
export const ROLE_CREATE = "ROLE_CREATE";
export const ROLE_REMOVE = "ROLE_REMOVE";
export const ROLE_RESET = "ROLE_RESET";

export const fetchRoleAction = createRequestAction(ROLE_FETCH, roleId => {
  return {
    request: {
      method: "GET",
      url: `/api/roles/${roleId}`,
    },
  };
});

export const createRoleAction = createRequestAction(ROLE_CREATE, data => {
  return {
    request: {
      method: "POST",
      url: `/api/roles`,
      data,
    },
  };
});

export const updateRoleAction = createRequestAction(ROLE_UPDATE, (roleId, data) => {
  return {
    request: {
      method: "PUT",
      url: `/api/roles/${roleId}`,
      data,
    },
  };
});

export const removeRoleAction = createRequestAction(ROLE_REMOVE, roleId => {
  return {
    request: {
      method: "DELETE",
      url: `/api/roles/${roleId}`,
    },
  };
});

export const resetRoleAction = createAction(ROLE_RESET);
