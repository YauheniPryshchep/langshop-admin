import { fetchRoleAction, createRoleAction, updateRoleAction, removeRoleAction, resetRoleAction } from "./actions";
import { get } from "lodash";
import { handleActions } from "redux-actions";

const defaultState = {
  isFetched: false,
  isLoading: false,
  role: null,
};

const loadingStartHandler = state => {
  return {
    ...state,
    isLoading: true,
  };
};

const loadingEndHandler = state => {
  return {
    ...state,
    isFetched: true,
    isLoading: false,
  };
};

const fetchRoleSuccessHandler = (state, { payload }) => {
  const data = get(payload, "data.data");

  return {
    ...state,
    role: data,
    isFetched: true,
    isLoading: false,
  };
};

const createRoleSuccessHandler = (state, { payload }) => {
  const data = get(payload, "data.data");

  return {
    ...state,
    role: data,
    isFetched: true,
    isLoading: false,
  };
};

const updateRoleSuccessHandler = (state, { payload }) => {
  const data = get(payload, "data.data");

  return {
    ...state,
    role: data,
    isFetched: true,
    isLoading: false,
  };
};

const resetRoleHandler = () => {
  return defaultState;
};

export const role = handleActions(
  {
    [fetchRoleAction]: loadingStartHandler,
    [fetchRoleAction.success]: fetchRoleSuccessHandler,
    [fetchRoleAction.fail]: loadingEndHandler,

    [updateRoleAction]: loadingStartHandler,
    [updateRoleAction.success]: updateRoleSuccessHandler,
    [updateRoleAction.fail]: loadingEndHandler,

    [createRoleAction]: loadingStartHandler,
    [createRoleAction.success]: createRoleSuccessHandler,
    [createRoleAction.fail]: loadingEndHandler,

    [removeRoleAction]: loadingStartHandler,
    [removeRoleAction.success]: resetRoleHandler,
    [removeRoleAction.fail]: loadingEndHandler,

    [resetRoleAction]: resetRoleHandler,
  },
  defaultState
);
