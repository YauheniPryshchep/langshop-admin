import { fetchFullstorySessionsAction, resetFullstorySessionsAction } from "./actions";
import { get } from "lodash";
import { handleActions } from "redux-actions";

const defaultState = {
  isFetched: false,
  isLoading: false,
  sessions: [],
  total: 0,
};

const loadingStartHandler = state => {
  return {
    ...state,
    isLoading: true,
  };
};

const loadingEndHandler = state => {
  return {
    ...state,
    isFetched: true,
    isLoading: false,
  };
};

const fetchRecipientsSuccessHandler = (state, { payload }) => {
  const data = get(payload, "data.data");

  return {
    ...state,
    sessions: data,
    total: data.count,
    isFetched: true,
    isLoading: false,
  };
};

const resetRecipientsHandler = () => {
  return defaultState;
};

export const fullstorySessions = handleActions(
  {
    [fetchFullstorySessionsAction]: loadingStartHandler,
    [fetchFullstorySessionsAction.success]: fetchRecipientsSuccessHandler,
    [fetchFullstorySessionsAction.fail]: loadingEndHandler,

    [resetFullstorySessionsAction]: resetRecipientsHandler,
  },
  defaultState
);
