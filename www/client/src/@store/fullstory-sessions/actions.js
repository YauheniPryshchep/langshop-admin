import { createRequestAction } from "@store/createRequestAction";
import { createAction } from "redux-actions";

export const FULLSTORY_SESSIONS_FETCH = "FULLSTORY_SESSIONS_FETCH";
export const FULLSTORY_SESSIONS_RESET = "FULLSTORY_SESSIONS__RESET";

export const fetchFullstorySessionsAction = createRequestAction(FULLSTORY_SESSIONS_FETCH, (uid, cancelToken) => {
  return {
    request: {
      method: "GET",
      url: `/api/fullstory/${uid}/sessions`,
      cancelToken,
    },
  };
});

export const resetFullstorySessionsAction = createAction(FULLSTORY_SESSIONS_RESET);
