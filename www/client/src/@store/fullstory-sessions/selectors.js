import { createSelector } from "reselect";

import { get } from "lodash";

const baseState = state => get(state, "fullstorySessions", null);

export const isFetched = createSelector(baseState, state => get(state, "isFetched", false));

export const isLoading = createSelector(baseState, state => get(state, "isLoading", false));

export const isLoaded = createSelector(isLoading, isFetched, (isLoading, isFetched) => !isLoading && isFetched);

export const fullstorySessions = createSelector(baseState, state => get(state, "sessions", []));
