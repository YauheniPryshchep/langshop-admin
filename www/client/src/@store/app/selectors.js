import LangShop from "@svg/langshop.svg";
import { createSelector } from "reselect";

import { get } from "lodash";

const baseState = state => get(state, "app", null);

export const app = createSelector(baseState, () => {
  return {
    title: "LangShop",
    id: "langshop",
    description: "LangShop app description",
    logo: "/assets/logo/langshop.svg",
    icon: LangShop,
    versions: [
      {
        version: "v2",
        appSuffix: "-2",
        title: "Version 2",
      },
      {
        version: "v1",
        appSuffix: "",
        title: "Version 1",
      },
    ],
  };
});

export const getApp = createSelector(app, app => () => app);

export const getVersion = createSelector(getApp, getApp => appVersion => {
  const app = getApp();

  return app.versions.find(version => version.version === appVersion) || app.versions[0];
});
