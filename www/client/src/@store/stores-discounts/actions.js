import { createRequestAction } from "@store/createRequestAction";
import { createAction } from "redux-actions";

export const STORES_DISCOUNTS_FETCH = "STORES_DISCOUNTS_FETCH";
export const STORES_DISCOUNTS_CREATE = "STORES_DISCOUNTS_CREATE";
export const STORES_DISCOUNTS_REMOVE = "STORES_DISCOUNTS_REMOVE";
export const STORES_DISCOUNTS_RESET = "STORES_DISCOUNTS_RESET";

export const fetchStoresDiscountsAction = createRequestAction(STORES_DISCOUNTS_FETCH, (params, cancelToken) => {
  return {
    request: {
      method: "GET",
      url: `/api/rebate/stores-discounts`,
      params,
      cancelToken,
    },
  };
});

export const createStoreDiscountAction = createRequestAction(STORES_DISCOUNTS_CREATE, data => {
  return {
    request: {
      method: "POST",
      url: `/api/rebate/stores-discounts`,
      data,
    },
  };
});

export const removeStoreDiscountAction = createRequestAction(STORES_DISCOUNTS_REMOVE, domain => {
  return {
    request: {
      method: "DELETE",
      url: `/api/rebate/stores-discounts/${domain}`,
    },
  };
});

export const resetStoresDiscountsAction = createAction(STORES_DISCOUNTS_RESET);
