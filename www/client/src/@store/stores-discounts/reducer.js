import {
  fetchStoresDiscountsAction,
  createStoreDiscountAction,
  removeStoreDiscountAction,
  resetStoresDiscountsAction,
} from "./actions";
import { get } from "lodash";
import { handleActions } from "redux-actions";

const defaultState = {
  isFetched: false,
  isLoading: false,
  storesDiscounts: [],
  total: 0,
};

const loadingStartHandler = state => {
  return {
    ...state,
    isLoading: true,
  };
};

const loadingEndHandler = state => {
  return {
    ...state,
    isFetched: true,
    isLoading: false,
  };
};

const fetchStoresDiscountsSuccessHandler = (state, { payload }) => {
  const data = get(payload, "data.data");

  return {
    ...state,
    storesDiscounts: data.items,
    total: data.count,
    isFetched: true,
    isLoading: false,
  };
};

const resetStoresDiscountsHandler = () => {
  return defaultState;
};

export const storesDiscounts = handleActions(
  {
    [fetchStoresDiscountsAction]: loadingStartHandler,
    [fetchStoresDiscountsAction.success]: fetchStoresDiscountsSuccessHandler,
    [fetchStoresDiscountsAction.fail]: loadingEndHandler,

    [createStoreDiscountAction]: loadingStartHandler,
    [createStoreDiscountAction.success]: loadingEndHandler,
    [createStoreDiscountAction.fail]: loadingEndHandler,

    [removeStoreDiscountAction]: loadingStartHandler,
    [removeStoreDiscountAction.success]: loadingEndHandler,
    [removeStoreDiscountAction.fail]: loadingEndHandler,

    [resetStoresDiscountsAction]: resetStoresDiscountsHandler,
  },
  defaultState
);
