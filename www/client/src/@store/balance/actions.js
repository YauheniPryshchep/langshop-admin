import { createRequestAction } from "@store/createRequestAction";
import { createAction } from "redux-actions";

export const BALANCE_FETCH = "BALANCE_FETCH";
export const BALANCE_STORE_FETCH = "BALANCE_STORE_FETCH";
export const BALANCE_RESET = "BALANCE_RESET";

export const fetchBalanceAction = createRequestAction(BALANCE_FETCH, () => {
  return {
    request: {
      method: "GET",
      url: `/api/partners/transactions`,
    },
  };
});

export const fetchBalanceStoreAction = createRequestAction(BALANCE_STORE_FETCH, userId => {
  return {
    request: {
      method: "GET",
      url: `/api/partners/transactions/${userId}`,
    },
  };
});

export const resetBalanceAction = createAction(BALANCE_RESET);
