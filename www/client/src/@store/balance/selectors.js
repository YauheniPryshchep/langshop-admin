import { createSelector } from "reselect";
import get from "lodash/get";

const baseState = state => get(state, "balance", null);

export const balance = createSelector(baseState, state => get(state, "balance", 0));

export const transactions = createSelector(baseState, state => get(state, "transactions", []));

export const isFetched = createSelector(baseState, state => get(state, "isFetched", false));

export const isLoading = createSelector(baseState, state => get(state, "isLoading", false));

export const isLoaded = createSelector(isLoading, isFetched, (isLoading, isFetched) => !isLoading && isFetched);
