import { fetchBalanceAction, fetchBalanceStoreAction, resetBalanceAction } from "./actions";

import get from "lodash/get";
import { handleActions } from "redux-actions";

const defaultState = {
  items: [],
  balance: 0,
  transactions: [],
  isFetched: false,
  isLoading: false,
};

const fetchBalanceRequestHandler = state => {
  return {
    ...state,
    isLoading: true,
  };
};

const fetchBalanceSuccessHandler = (state, { payload }) => {
  const data = get(payload, "data.data", 0);
  const balance = data.reduce((acc, item) => (acc += item.balance), 0);
  const transactions = data.reduce((acc, item) => [...acc, ...item.transactions], []);
  return {
    ...state,
    balance: balance,
    transactions: transactions,
    isFetched: true,
    isLoading: false,
  };
};

const fetchBalanceStoreSuccessHandler = (state, { payload }) => {
  const balance = get(payload, "data.data.balance", 0);
  const transactions = get(payload, "data.data.transactions", []);

  return {
    ...state,
    balance: balance,
    transactions: transactions,
    isFetched: true,
    isLoading: false,
  };
};

const fetchBalanceFailHandler = state => {
  return {
    ...state,
    isFetched: true,
    isLoading: false,
  };
};

const resetBalanceHandler = () => {
  return defaultState;
};

export const balance = handleActions(
  {
    [fetchBalanceAction]: fetchBalanceRequestHandler,
    [fetchBalanceAction.success]: fetchBalanceSuccessHandler,
    [fetchBalanceAction.fail]: fetchBalanceFailHandler,

    [fetchBalanceStoreAction]: fetchBalanceRequestHandler,
    [fetchBalanceStoreAction.success]: fetchBalanceStoreSuccessHandler,
    [fetchBalanceStoreAction.fail]: fetchBalanceFailHandler,

    [resetBalanceAction]: resetBalanceHandler,
  },
  defaultState
);
