import { createSelector } from "reselect";

import { get } from "lodash";

const baseState = state => get(state, "customer", null);

export const isFetched = createSelector(baseState, state => get(state, "isFetched", false));

export const isLoading = createSelector(baseState, state => get(state, "isLoading", false));

export const isLoaded = createSelector(isLoading, isFetched, (isLoading, isFetched) => !isLoading && isFetched);

export const total = createSelector(baseState, state => get(state, "total", 0));

export const customer = createSelector(baseState, state => get(state, "customer", []));

export const initialValues = createSelector(
  customer,
  state =>
    state || {
      first_name: "",
      last_name: "",
      email: "",
      email_verified: 0,
      locale: "",
      id: 0,
      external_id: 0,
      stores: [],
    }
);
