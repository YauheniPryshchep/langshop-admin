import { createRequestAction } from "@store/createRequestAction";
import { createAction } from "redux-actions";

export const CUSTOMER_FETCH = "CUSTOMER_FETCH";
export const CUSTOMER_RESET = "CUSTOMER_RESET";

export const fetchCustomerAction = createRequestAction(CUSTOMER_FETCH, (customerId, cancelToken) => {
  return {
    request: {
      method: "GET",
      url: `/api/customers/${customerId}`,
      cancelToken,
    },
  };
});

export const resetCustomerAction = createAction(CUSTOMER_RESET);
