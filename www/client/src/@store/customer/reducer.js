import {fetchCustomerAction, resetCustomerAction} from "./actions";
import {get} from "lodash";
import {handleActions} from "redux-actions";

const defaultState = {
  isFetched: false,
  isLoading: false,
  customer: null,
};

const loadingStartHandler = state => {
  return {
    ...state,
    isLoading: true,
  };
};

const loadingEndHandler = state => {
  return {
    ...state,
    isFetched: true,
    isLoading: false,
  };
};

const fetchCustomerSuccessHandler = (state, {payload}) => {
  const data = get(payload, "data.data");

  return {
    ...state,
    customer: data.items,
    total: data.count,
    page: data.page,
    pages: data.pages,
    isFetched: true,
    isLoading: false,
  };
};

const resetCustomerHandler = () => {
  return defaultState;
};

export const customer = handleActions(
  {
    [fetchCustomerAction]: loadingStartHandler,
    [fetchCustomerAction.success]: fetchCustomerSuccessHandler,
    [fetchCustomerAction.fail]: loadingEndHandler,

    [resetCustomerAction]: resetCustomerHandler,
  },
  defaultState
);
