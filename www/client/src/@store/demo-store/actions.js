import { createRequestAction } from "@store/createRequestAction";
import { createAction } from "redux-actions";

export const DEMO_STORE_FETCH = "DEMO_STORE_FETCH";
export const DEMO_STORE_CREATE = "DEMO_STORE_CREATE";
export const DEMO_STORE_REMOVE = "DEMO_STORE_REMOVE";
export const DEMO_STORE_RESET = "DEMO_STORE_RESET";

export const fetchDemoStoreAction = createRequestAction(DEMO_STORE_FETCH, (domain, cancelToken) => {
  return {
    request: {
      method: "GET",
      url: `/api/rebate/demo-stores`,
      params: {
        filter: domain,
        limit: 1,
      },
      cancelToken,
    },
  };
});

export const createDemoStoreAction = createRequestAction(DEMO_STORE_CREATE, data => {
  return {
    request: {
      method: "POST",
      url: `/api/rebate/demo-stores`,
      data,
    },
  };
});

export const removeDemoStoreAction = createRequestAction(DEMO_STORE_REMOVE, domain => {
  return {
    request: {
      method: "DELETE",
      url: `/api/rebate/demo-stores/${domain}`,
    },
  };
});

export const resetDemoStoreAction = createAction(DEMO_STORE_RESET);
