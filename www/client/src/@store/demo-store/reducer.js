import { fetchDemoStoreAction, createDemoStoreAction, removeDemoStoreAction, resetDemoStoreAction } from "./actions";
import { get, first } from "lodash";
import { handleActions } from "redux-actions";

const defaultState = {
  isFetched: false,
  isLoading: false,
  demoStore: null,
};

const loadingStartHandler = state => {
  return {
    ...state,
    isLoading: true,
  };
};

const loadingEndHandler = state => {
  return {
    ...state,
    isFetched: true,
    isLoading: false,
  };
};

const fetchDemoStoreSuccessHandler = (state, { payload }) => {
  const data = get(payload, "data.data");

  return {
    ...state,
    demoStore: first(data.items),
    isFetched: true,
    isLoading: false,
  };
};

const createDemoStoreSuccessHandler = (state, { payload }) => {
  const data = get(payload, "data.data");

  return {
    ...state,
    demoStore: data,
    isLoading: false,
  };
};

const removeDemoStoreSuccessHandler = state => {
  return {
    ...state,
    demoStore: null,
    isLoading: false,
  };
};

const resetDemoStoreHandler = () => {
  return defaultState;
};

export const demoStore = handleActions(
  {
    [fetchDemoStoreAction]: loadingStartHandler,
    [fetchDemoStoreAction.success]: fetchDemoStoreSuccessHandler,
    [fetchDemoStoreAction.fail]: loadingEndHandler,

    [createDemoStoreAction]: loadingStartHandler,
    [createDemoStoreAction.success]: createDemoStoreSuccessHandler,
    [createDemoStoreAction.fail]: loadingEndHandler,

    [removeDemoStoreAction]: loadingStartHandler,
    [removeDemoStoreAction.success]: removeDemoStoreSuccessHandler,
    [removeDemoStoreAction.fail]: loadingEndHandler,

    [resetDemoStoreAction]: resetDemoStoreHandler,
  },
  defaultState
);
