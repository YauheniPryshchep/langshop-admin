import { createAction } from "redux-actions";
const CREATE_TOAST = "CREATE_TOAST";
const REMOVE_TOAST = "REMOVE_TOAST";

export const createToast = createAction(CREATE_TOAST, ({ message, action }) => ({ message, action }));

export const removeToast = createAction(REMOVE_TOAST, id => ({ id }));
