import { createSelector } from "reselect";

import { get } from "lodash";

const baseState = state => get(state, "partnersNotifications", null);

export const notifications = createSelector(baseState, state => get(state, "notifications", []));
