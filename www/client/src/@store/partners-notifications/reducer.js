import { createToast, removeToast } from "./actions";
import { handleActions } from "redux-actions";
import { uniqueId } from "lodash";

const defaultState = {
  isFetched: false,
  isLoading: false,
  notifications: [],
  total: 0,
};

const createToastHandler = (state, { payload }) => {
  return {
    ...state,
    notifications: [
      ...state.notifications,
      {
        id: uniqueId("notification"),
        ...payload,
      },
    ],
  };
};

const removeToastHandler = (state, { payload: { id } }) => {
  return {
    ...state,
    notifications: [...state.notifications.filter(notification => notification.id !== id)],
  };
};

export const partnersNotifications = handleActions(
  {
    [createToast]: createToastHandler,
    [removeToast]: removeToastHandler,
  },
  defaultState
);
