import { fetchCustomersAction, resetCustomersAction } from "./actions";
import { get } from "lodash";
import { handleActions } from "redux-actions";

const defaultState = {
  isFetched: false,
  isLoading: false,
  customers: [],
  total: 0,
};

const loadingStartHandler = state => {
  return {
    ...state,
    isLoading: true,
  };
};

const loadingEndHandler = state => {
  return {
    ...state,
    isFetched: true,
    isLoading: false,
  };
};

const fetchCustomersSuccessHandler = (state, { payload }) => {
  const data = get(payload, "data.data");

  return {
    ...state,
    customers: data.items,
    total: data.count,
    isFetched: true,
    isLoading: false,
  };
};

const resetCustomersHandler = () => {
  return defaultState;
};

export const customers = handleActions(
  {
    [fetchCustomersAction]: loadingStartHandler,
    [fetchCustomersAction.success]: fetchCustomersSuccessHandler,
    [fetchCustomersAction.fail]: loadingEndHandler,

    [resetCustomersAction]: resetCustomersHandler,
  },
  defaultState
);
