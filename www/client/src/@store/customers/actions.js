import { createRequestAction } from "@store/createRequestAction";
import { createAction } from "redux-actions";

export const CUSTOMERS_FETCH = "CUSTOMERS_FETCH";
export const CUSTOMERS_RESET = "CUSTOMERS__RESET";

export const fetchCustomersAction = createRequestAction(CUSTOMERS_FETCH, (params, cancelToken) => {
  return {
    request: {
      method: "GET",
      url: `/api/customers`,
      params,
      cancelToken,
    },
  };
});

export const resetCustomersAction = createAction(CUSTOMERS_RESET);
