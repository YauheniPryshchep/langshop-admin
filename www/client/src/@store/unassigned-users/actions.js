import { createRequestAction } from "@store/createRequestAction";
import { createAction } from "redux-actions";

export const UNASSIGNED_USERS_FETCH = "UNASSIGNED_USERS_FETCH";
export const UNASSIGNED_USERS_RESET = "UNASSIGNED_USERS_RESET";

export const fetchUnassignedUsersAction = createRequestAction(UNASSIGNED_USERS_FETCH, (params, cancelToken) => {
  return {
    request: {
      method: "GET",
      url: `/api/users/unassigned`,
      params,
      cancelToken,
    },
  };
});

export const resetUnassignedUsersAction = createAction(UNASSIGNED_USERS_RESET);
