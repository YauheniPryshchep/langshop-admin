import { fetchUnassignedUsersAction, resetUnassignedUsersAction } from "./actions";
import { get } from "lodash";
import { handleActions } from "redux-actions";

const defaultState = {
  isFetched: false,
  isLoading: false,
  unassignedUsers: [],
  total: 0,
};

const loadingStartHandler = state => {
  return {
    ...state,
    isLoading: true,
  };
};

const loadingEndHandler = state => {
  return {
    ...state,
    isFetched: true,
    isLoading: false,
  };
};

const fetchUnassignedUsersSuccessHandler = (state, { payload }) => {
  const data = get(payload, "data.data");

  return {
    ...state,
    unassignedUsers: data.items,
    total: data.count,
    isFetched: true,
    isLoading: false,
  };
};

const resetUnassignedUsersHandler = () => {
  return defaultState;
};

export const unassignedUsers = handleActions(
  {
    [fetchUnassignedUsersAction]: loadingStartHandler,
    [fetchUnassignedUsersAction.success]: fetchUnassignedUsersSuccessHandler,
    [fetchUnassignedUsersAction.fail]: loadingEndHandler,

    [resetUnassignedUsersAction]: resetUnassignedUsersHandler,
  },
  defaultState
);
