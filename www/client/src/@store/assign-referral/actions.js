import { createRequestAction } from "@store/createRequestAction";

export const ASSIGN_REFERRAL = "ASSIGN_REFERRAL";

export const createAssignReferral = createRequestAction(ASSIGN_REFERRAL, data => {
  return {
    request: {
      method: "POST",
      url: `/api/rebate/assign-referral`,
      data,
    },
  };
});
