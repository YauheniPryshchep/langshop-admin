import { createRequestAction } from "@store/createRequestAction";
import { createAction } from "redux-actions";

export const STORE_TRIAL_FETCH = "STORE_TRIAL_FETCH";
export const STORE_TRIAL_CREATE = "STORE_TRIAL_CREATE";
export const STORE_TRIAL_REMOVE = "STORE_TRIAL_REMOVE";
export const STORE_TRIAL_RESET = "STORE_TRIAL_RESET";

export const fetchStoreTrialAction = createRequestAction(STORE_TRIAL_FETCH, (domain, cancelToken) => {
  return {
    request: {
      method: "GET",
      url: `/api/rebate/stores-trials`,
      params: {
        filter: domain,
        limit: 1,
      },
      cancelToken,
    },
  };
});

export const createStoreTrialAction = createRequestAction(STORE_TRIAL_CREATE, data => {
  return {
    request: {
      method: "POST",
      url: `/api/rebate/stores-trials`,
      data,
    },
  };
});

export const removeStoreTrialAction = createRequestAction(STORE_TRIAL_REMOVE, domain => {
  return {
    request: {
      method: "DELETE",
      url: `/api/rebate/stores-trials/${domain}`,
    },
  };
});

export const resetStoreTrialAction = createAction(STORE_TRIAL_RESET);
