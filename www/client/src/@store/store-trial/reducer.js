import {
  fetchStoreTrialAction,
  createStoreTrialAction,
  removeStoreTrialAction,
  resetStoreTrialAction,
} from "./actions";
import { get, first } from "lodash";
import { handleActions } from "redux-actions";

const defaultState = {
  isFetched: false,
  isLoading: false,
  storeTrial: null,
};

const loadingStartHandler = state => {
  return {
    ...state,
    isLoading: true,
  };
};

const loadingEndHandler = state => {
  return {
    ...state,
    isFetched: true,
    isLoading: false,
  };
};

const fetchStoreTrialSuccessHandler = (state, { payload }) => {
  const data = get(payload, "data.data");

  return {
    ...state,
    storeTrial: first(data.items),
    isFetched: true,
    isLoading: false,
  };
};

const createStoreTrialSuccessHandler = (state, { payload }) => {
  const data = get(payload, "data.data");

  return {
    ...state,
    storeTrial: data,
    isLoading: false,
  };
};

const removeStoreTrialSuccessHandler = state => {
  return {
    ...state,
    storeTrial: null,
    isLoading: false,
  };
};

const resetStoreTrialHandler = () => {
  return defaultState;
};

export const storeTrial = handleActions(
  {
    [fetchStoreTrialAction]: loadingStartHandler,
    [fetchStoreTrialAction.success]: fetchStoreTrialSuccessHandler,
    [fetchStoreTrialAction.fail]: loadingEndHandler,

    [createStoreTrialAction]: loadingStartHandler,
    [createStoreTrialAction.success]: createStoreTrialSuccessHandler,
    [createStoreTrialAction.fail]: loadingEndHandler,

    [removeStoreTrialAction]: loadingStartHandler,
    [removeStoreTrialAction.success]: removeStoreTrialSuccessHandler,
    [removeStoreTrialAction.fail]: loadingEndHandler,

    [resetStoreTrialAction]: resetStoreTrialHandler,
  },
  defaultState
);
