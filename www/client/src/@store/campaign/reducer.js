import {
  fetchCampaignAction,
  createCampaignAction,
  updateCampaignAction,
  removeCampaignAction,
  resetCampaignAction,
} from "./actions";
import { get } from "lodash";
import { handleActions } from "redux-actions";

const defaultState = {
  isFetched: false,
  isLoading: false,
  campaign: null,
};

const loadingStartHandler = state => {
  return {
    ...state,
    isLoading: true,
  };
};

const loadingEndHandler = state => {
  return {
    ...state,
    isFetched: true,
    isLoading: false,
  };
};

const fetchCampaignSuccessHandler = (state, { payload }) => {
  const data = get(payload, "data.data");

  return {
    ...state,
    campaign: data,
    isFetched: true,
    isLoading: false,
  };
};

const createCampaignSuccessHandler = (state, { payload }) => {
  const data = get(payload, "data.data");

  return {
    ...state,
    campaign: data,
    isFetched: true,
    isLoading: false,
  };
};

const updateCampaignSuccessHandler = (state, { payload }) => {
  const data = get(payload, "data.data");

  return {
    ...state,
    campaign: data,
    isFetched: true,
    isLoading: false,
  };
};

const resetCampaignHandler = () => {
  return defaultState;
};

export const campaign = handleActions(
  {
    [fetchCampaignAction]: loadingStartHandler,
    [fetchCampaignAction.success]: fetchCampaignSuccessHandler,
    [fetchCampaignAction.fail]: loadingEndHandler,

    [updateCampaignAction]: loadingStartHandler,
    [updateCampaignAction.success]: updateCampaignSuccessHandler,
    [updateCampaignAction.fail]: loadingEndHandler,

    [createCampaignAction]: loadingStartHandler,
    [createCampaignAction.success]: createCampaignSuccessHandler,
    [createCampaignAction.fail]: loadingEndHandler,

    [removeCampaignAction]: loadingStartHandler,
    [removeCampaignAction.success]: resetCampaignHandler,
    [removeCampaignAction.fail]: loadingEndHandler,

    [resetCampaignAction]: resetCampaignHandler,
  },
  defaultState
);
