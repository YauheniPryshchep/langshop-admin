import { createRequestAction } from "@store/createRequestAction";
import { createAction } from "redux-actions";

export const CAMPAIGN_FETCH = "CAMPAIGN_FETCH";
export const CAMPAIGN_CREATE = "CAMPAIGN_CREATE";
export const CAMPAIGN_UPDATE = "CAMPAIGN_UPDATE";
export const CAMPAIGN_DELETE = "CAMPAIGN_DELETE";
export const CAMPAIGN_RESET = "CAMPAIGN__RESET";

export const fetchCampaignAction = createRequestAction(CAMPAIGN_FETCH, campaignId => {
  return {
    request: {
      method: "GET",
      url: `/api/campaigns/${campaignId}`,
    },
  };
});

export const createCampaignAction = createRequestAction(CAMPAIGN_CREATE, data => {
  return {
    request: {
      method: "POST",
      url: `/api/campaigns`,
      data,
    },
  };
});

export const updateCampaignAction = createRequestAction(CAMPAIGN_UPDATE, (campaignId, data) => {
  return {
    request: {
      method: "PUT",
      url: `/api/campaigns/${campaignId}`,
      data,
    },
  };
});

export const removeCampaignAction = createRequestAction(CAMPAIGN_DELETE, campaignId => {
  return {
    request: {
      method: "DELETE",
      url: `/api/campaigns/${campaignId}`,
    },
  };
});

export const resetCampaignAction = createAction(CAMPAIGN_RESET);
