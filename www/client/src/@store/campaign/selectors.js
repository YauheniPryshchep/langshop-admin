import { createSelector } from "reselect";

import { get } from "lodash";

const baseState = state => get(state, "campaign", null);

export const isFetched = createSelector(baseState, state => get(state, "isFetched", false));

export const isLoading = createSelector(baseState, state => get(state, "isLoading", false));

export const isLoaded = createSelector(isLoading, isFetched, (isLoading, isFetched) => !isLoading && isFetched);

export const campaign = createSelector(baseState, state => get(state, "campaign", null));

export const initialValues = createSelector(
  campaign,
  state =>
    state || {
      title: "",
      description: "",
      subject: "",
      template: "",
      author_id: "",
      recipients_filter: {
        limit: 20,
        filters: [],
      },
      campaign_domains: [],
      status: 1,
      type: "email",
    }
);
