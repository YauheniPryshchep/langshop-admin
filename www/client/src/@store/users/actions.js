import { createRequestAction } from "@store/createRequestAction";
import { createAction } from "redux-actions";

export const USERS_FETCH = "USERS_FETCH";
export const USERS_RESET = "USERS_RESET";

export const fetchUsersAction = createRequestAction(USERS_FETCH, (params, cancelToken) => {
  return {
    request: {
      method: "GET",
      url: `/api/users`,
      params,
      cancelToken,
    },
  };
});

export const resetUsersAction = createAction(USERS_RESET);
