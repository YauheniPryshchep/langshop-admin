import { createRequestAction } from "@store/createRequestAction";
import { createAction } from "redux-actions";

export const DIRECTIONS_FETCH = "DIRECTIONS_FETCH";
export const DIRECTIONS_RESET = "DIRECTIONS_RESET";
export const CREATE_DIRECTION = "CREATE_DIRECTION";
export const fetchVocabulariesDirectionsAction = createRequestAction(DIRECTIONS_FETCH, cancelToken => {
  return {
    request: {
      method: "GET",
      url: `/api/vocabularies/directions`,
      cancelToken,
    },
  };
});

export const resetVocabulariesDirectionsAction = createAction(DIRECTIONS_RESET);
export const addDirection = createAction(CREATE_DIRECTION);
