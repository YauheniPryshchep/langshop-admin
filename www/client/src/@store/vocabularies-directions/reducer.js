import { addDirection, fetchVocabulariesDirectionsAction, resetVocabulariesDirectionsAction } from "./actions";

import { get } from "lodash";
import { handleActions } from "redux-actions";

const defaultState = {
  isFetched: false,
  isLoading: false,
  items: [],
};

const loadingStartHandler = state => {
  return {
    ...state,
    isLoading: true,
  };
};

const loadingEndHandler = state => {
  return {
    ...state,
    isFetched: true,
    isLoading: false,
  };
};

const fetchDirectionsSuccessHandler = (state, { payload }) => {
  const data = get(payload, "data.data");

  return {
    ...state,
    directions: data,
    isFetched: true,
    isLoading: false,
  };
};

const resetDirectionsHandler = () => {
  return defaultState;
};

const handleAddDirection = (state, { payload }) => {
  return {
    ...state,
    directions: [...payload],
  };
};

export const vocabulariesDirections = handleActions(
  {
    [fetchVocabulariesDirectionsAction]: loadingStartHandler,
    [fetchVocabulariesDirectionsAction.success]: fetchDirectionsSuccessHandler,
    [fetchVocabulariesDirectionsAction.fail]: loadingEndHandler,
    [resetVocabulariesDirectionsAction]: resetDirectionsHandler,
    [addDirection]: handleAddDirection,
  },
  defaultState
);
