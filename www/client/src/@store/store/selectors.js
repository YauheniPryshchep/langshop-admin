import { createSelector } from "reselect";

import { get } from "lodash";

const baseState = state => get(state, "store", null);

export const isFetched = createSelector(baseState, state => get(state, "isFetched", false));

export const isLoading = createSelector(baseState, state => get(state, "isLoading", false));
export const isUpdating = createSelector(baseState, state => get(state, "isUpdating", false));

export const isLoaded = createSelector(isLoading, isFetched, (isLoading, isFetched) => !isLoading && isFetched);

export const store = createSelector(baseState, state => get(state, "store", null));
