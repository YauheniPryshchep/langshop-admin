import { fetchStoreAction, resetPlanToFree, resetStoreAction } from "./actions";
import { get } from "lodash";
import { handleActions } from "redux-actions";

const defaultState = {
  isFetched: false,
  isLoading: false,
  isUpdating: false,
  store: null,
};

const loadingStartHandler = state => {
  return {
    ...state,
    isLoading: true,
  };
};

const loadingEndHandler = state => {
  return {
    ...state,
    isFetched: true,
    isLoading: false,
  };
};

const updatingStartHandler = state => {
  return {
    ...state,
    isUpdating: true,
  };
};

const updatingEndHandler = state => {
  return {
    ...state,
    isUpdating: false,
  };
};

const fetchStoreSuccessHandler = (state, { payload }) => {
  const data = get(payload, "data.data");

  return {
    ...state,
    store: data,
    isFetched: true,
    isLoading: false,
    isUpdating: false,
  };
};

const resetStoreHandler = () => {
  return defaultState;
};

export const store = handleActions(
  {
    [fetchStoreAction]: loadingStartHandler,
    [fetchStoreAction.success]: fetchStoreSuccessHandler,
    [fetchStoreAction.fail]: loadingEndHandler,

    [resetPlanToFree]: updatingStartHandler,
    [resetPlanToFree.success]: fetchStoreSuccessHandler,
    [resetPlanToFree.fail]: updatingEndHandler,

    [resetStoreAction]: resetStoreHandler,
  },
  defaultState
);
