import {createRequestAction} from "@store/createRequestAction";
import {createAction} from "redux-actions";

export const STORE_FETCH = "STORE_FETCH";
export const STORE_RESET_PLAN = "STORE_RESET_PLAN";
export const STORE_RESET = "STORE_RESET";

export const fetchStoreAction = createRequestAction(STORE_FETCH, (domain, cancelToken) => {
  return {
    request: {
      method: "GET",
      url: `/api/stores/${domain}`,
      cancelToken,
    },
  };
});

export const resetPlanToFree = createRequestAction(STORE_RESET_PLAN, (domain, data, cancelToken) => {
  return {
    request: {
      method: "POST",
      url: `/api/stores/${domain}/plan`,
      cancelToken,
      data,
    },
  };
});

export const changePlanPrice = createRequestAction(STORE_RESET_PLAN, (domain, data, cancelToken) => {
  return {
    request: {
      method: "POST",
      url: `/api/stores/${domain}/price`,
      cancelToken,
      data,
    },
  };
});

export const resetStoreAction = createAction(STORE_RESET);
