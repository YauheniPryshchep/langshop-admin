import { createRequestAction } from "@store/createRequestAction";
import { createAction } from "redux-actions";

export const SCOPES_FETCH = "SCOPES_FETCH";
export const SCOPES_RESET = "SCOPES_RESET";

export const fetchScopesAction = createRequestAction(SCOPES_FETCH, params => {
  return {
    request: {
      method: "GET",
      url: `/api/scopes`,
      params,
    },
  };
});

export const resetScopesAction = createAction(SCOPES_RESET);
