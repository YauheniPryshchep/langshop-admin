import { fetchScopesAction, resetScopesAction } from "./actions";
import { get } from "lodash";
import { handleActions } from "redux-actions";

const defaultState = {
  isFetched: false,
  isLoading: false,
  scopes: [],
};

const loadingStartHandler = state => {
  return {
    ...state,
    isLoading: true,
  };
};

const loadingEndHandler = state => {
  return {
    ...state,
    isFetched: true,
    isLoading: false,
  };
};

const fetchScopesSuccessHandler = (state, { payload }) => {
  const data = get(payload, "data.data");

  return {
    ...state,
    scopes: data,
    isFetched: true,
    isLoading: false,
  };
};

const resetScopesHandler = () => {
  return defaultState;
};

export const scopes = handleActions(
  {
    [fetchScopesAction]: loadingStartHandler,
    [fetchScopesAction.success]: fetchScopesSuccessHandler,
    [fetchScopesAction.fail]: loadingEndHandler,

    [resetScopesAction]: resetScopesHandler,
  },
  defaultState
);
