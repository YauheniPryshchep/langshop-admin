import { createRequestAction } from "@store/createRequestAction";
import { createAction } from "redux-actions";

export const PROFILE_FETCH = "PROFILE_FETCH";
export const PROFILE_RESET = "PROFILE_RESET";

export const fetchProfileAction = createRequestAction(PROFILE_FETCH, () => {
  return {
    request: {
      method: "GET",
      url: "/api/profile",
    },
  };
});

export const resetProfileAction = createAction(PROFILE_RESET);
