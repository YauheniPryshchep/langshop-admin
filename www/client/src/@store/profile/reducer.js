import { fetchProfileAction, resetProfileAction } from "./actions";
import { get } from "lodash";
import { handleActions } from "redux-actions";

const defaultState = {
  isFetched: false,
  isLoading: false,
  profile: null,
};

const fetchProfileRequestHandler = state => {
  return {
    ...state,
    isLoading: true,
  };
};

const fetchProfileSuccessHandler = (state, { payload }) => {
  const data = get(payload, "data.data");

  return {
    ...state,
    profile: data,
    isFetched: true,
    isLoading: false,
  };
};

const fetchProfileFailHandler = state => {
  return {
    ...state,
    isFetched: true,
    isLoading: false,
  };
};

const resetProfileHandler = () => {
  return defaultState;
};

export const profile = handleActions(
  {
    [fetchProfileAction]: fetchProfileRequestHandler,
    [fetchProfileAction.success]: fetchProfileSuccessHandler,
    [fetchProfileAction.fail]: fetchProfileFailHandler,

    [resetProfileAction]: resetProfileHandler,
  },
  defaultState
);
