import { createSelector } from "reselect";

import { get } from "lodash";

const baseState = state => get(state, "profile", null);

export const isFetched = createSelector(baseState, state => get(state, "isFetched", false));

export const isLoading = createSelector(baseState, state => get(state, "isLoading", false));

export const isLoaded = createSelector(isLoading, isFetched, (isLoading, isFetched) => !isLoading && isFetched);

export const profile = createSelector(baseState, state => get(state, "profile", null));

export const profileName = createSelector(baseState, state => get(state, "profile.name", null));

export const profilePhoto = createSelector(baseState, state => get(state, "profile.photo", null));

export const profileScopes = createSelector(baseState, state => {
  return get(state, "profile.scopes", []).map(scope => scope.name);
});
