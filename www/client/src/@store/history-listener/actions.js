import { createAction } from "redux-actions";

export const SET_HISTORY = "SET_HISTORY";
export const RESET_HISTORY = "RESET_HISTORY";

export const setHistoryAction = createAction(SET_HISTORY);
export const resetHistoryAction = createAction(RESET_HISTORY);
