import { setHistoryAction, resetHistoryAction } from "./actions";
import { handleActions } from "redux-actions";

const defaultState = {
  storeHistory: [],
};

const setHistory = (state, { payload }) => {
  const storeHistory = [...state.storeHistory, payload].slice(Math.max(state.storeHistory.length - 5, 0));
  return {
    ...state,
    storeHistory,
  };
};

const resetHistory = () => {
  return defaultState;
};

export const historyListener = handleActions(
  {
    [setHistoryAction]: setHistory,

    [resetHistoryAction]: resetHistory,
  },
  defaultState
);
