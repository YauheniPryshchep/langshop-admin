import { createSelector } from "reselect";

import { get } from "lodash";

const baseState = state => get(state, "historyListener", null);

export const backHistory = createSelector(baseState, state => (allowed, defaultRoute) => {
  const storeHistory = get(state, "storeHistory", []);

  for (let i = storeHistory.length - 1; i >= 0; i--) {
    const location = storeHistory[i];

    if (!allowed.includes(location.path)) {
      continue;
    }

    return {
      content: location.title || "Back",
      url: location.path + location.search,
    };
  }

  return defaultRoute;
});

export const storeHistory = createSelector(baseState, state => {
  get(state, "storeHistory", []);
});
