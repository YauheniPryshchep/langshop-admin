import { createRequestAction } from "@store/createRequestAction";
import { createAction } from "redux-actions";

export const ROLES_FETCH = "ROLES_FETCH";
export const ROLES_RESET = "ROLES_RESET";

export const fetchRolesAction = createRequestAction(ROLES_FETCH, (params, cancelToken) => {
  return {
    request: {
      method: "GET",
      url: `/api/roles`,
      params,
      cancelToken,
    },
  };
});

export const resetRolesAction = createAction(ROLES_RESET);
