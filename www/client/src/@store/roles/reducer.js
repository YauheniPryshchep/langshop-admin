import { fetchRolesAction, resetRolesAction } from "./actions";
import { get } from "lodash";
import { handleActions } from "redux-actions";

const defaultState = {
  isFetched: false,
  isLoading: false,
  roles: [],
  total: 0,
};

const loadingStartHandler = state => {
  return {
    ...state,
    isLoading: true,
  };
};

const loadingEndHandler = state => {
  return {
    ...state,
    isFetched: true,
    isLoading: false,
  };
};

const fetchRolesSuccessHandler = (state, { payload }) => {
  const data = get(payload, "data.data");

  return {
    ...state,
    roles: data.items,
    total: data.count,
    isFetched: true,
    isLoading: false,
  };
};

const resetRolesHandler = () => {
  return defaultState;
};

export const roles = handleActions(
  {
    [fetchRolesAction]: loadingStartHandler,
    [fetchRolesAction.success]: fetchRolesSuccessHandler,
    [fetchRolesAction.fail]: loadingEndHandler,

    [resetRolesAction]: resetRolesHandler,
  },
  defaultState
);
