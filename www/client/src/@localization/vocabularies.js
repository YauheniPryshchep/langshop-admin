export const translationMemories = {
  header: "Translation memories",
  resource: "Translation memory|Translation memories",
  fields: {
    original: "Source",
    translated: "Target",
  },
  forbidden: {
    content: "You dont have access to translation memories page",
  },
  modals: {
    delete: {
      title: "Are you sure want to delete this items from translation memories",
      description:
        "Translation memory item  will be removed. |{count} translation memory items were selected will be removed.",
    },
    "delete-all": {
      title: "Are you sure want to delete translation memory direction",
      description: "Translation memory direction {from} -> {to} will be removed",
    },
  },
  messages: {
    "delete-directions": "Translation memory direction {from} -> {to} successfully deleted",
    update: "Successfully updated translation memory items",
    create: "Successfully created translation memory item",
    delete: "Successfully deleted translation memory item",
  },
};
