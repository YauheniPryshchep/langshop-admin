import { translationMemories } from "./vocabularies";
import { errors } from "./errors";
import { actions } from "./actions";
import { get } from "lodash";
import stringInject from "stringinject";

export const localization = {
  translationMemories,
  actions,
  errors,
};

export const t = (key, count, vars) => {
  let string = get(localization, key, "");
  if (string.includes("|")) {
    const plural = string.split("|");
    string = count > 1 ? plural[1] : plural[0];
  }
  return stringInject(string, { ...vars });
};
