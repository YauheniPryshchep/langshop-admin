export const limitOptions = ["5", "10", '25', "100"];

export const sortOptions = [
  {
    label: "Alphabetically (A-Z)",
    value: "asc",
    field: "email",
    direction: "asc",
  },
  {
    label: "Alphabetically (Z-A)",
    value: "desc",
    field: "email",
    direction: "desc",
  },
  {
    label: "Created at ASC",
    value: "created_at_asc",
    field: "created_at",
    direction: "asc",
  },
  {
    label: "Created at DESC",
    value: "created_at_desc",
    field: "created_at",
    direction: "desc",
  },
];
