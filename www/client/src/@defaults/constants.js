export const TRANSACTION_PENDING_STATUS = 1;
export const TRANSACTION_PAID_STATUS = 2;
export const TRANSACTION_DECLINED_STATUS = 3;
export const TRANSACTION_FROZEN_STATUS = 4;
export const TRANSACTION_ACCEPTED_STATUS = 5;
export const TRANSACTION_STATUSES = {
  [TRANSACTION_PENDING_STATUS]: "info",
  [TRANSACTION_PAID_STATUS]: "success",
  [TRANSACTION_DECLINED_STATUS]: "warning",
  [TRANSACTION_FROZEN_STATUS]: "attention",
  [TRANSACTION_ACCEPTED_STATUS]: "success",
};
export const CONVERSION_RATE = 0.3;
export const WITHOUT_PLAN = "0";
export const FREE_PLAN = "1";
export const STANDARD_PLAN = "2";
export const ADVANCED_PLAN = "3";
export const PLAN_ENTERPRISE = "4";

export const PLANS = {
  [WITHOUT_PLAN]: "Without plan",
  [FREE_PLAN]: "Free plan",
  [STANDARD_PLAN]: "Standard plan",
  [ADVANCED_PLAN]: "Advanced plan",
  [PLAN_ENTERPRISE]: "Enterprise plan",
};
