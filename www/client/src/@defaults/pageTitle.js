export const pageTitle = title => {
  return `${title} | LangShop Partners`;
};
