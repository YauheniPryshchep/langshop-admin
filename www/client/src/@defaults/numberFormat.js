export const numberFormat = number => {
  if (typeof number !== "number") {
    return number;
  }
  return number.toLocaleString();
};
