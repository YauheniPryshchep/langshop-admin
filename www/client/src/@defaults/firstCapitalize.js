export function firstCapitalize(string) {
  let splited = string.trim().split(" ");
  let first = splited[0];
  first = first[0].charAt(0).toUpperCase() + first.slice(1);
  splited[0] = first;
  return splited.join(" ");
}
