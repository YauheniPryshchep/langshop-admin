import md5 from "md5";

export const gravatarEmail = email => {
  return `//www.gravatar.com/avatar/${md5(email)}?d=mp&r=g&s=50`;
};
