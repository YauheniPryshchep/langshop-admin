export const translationClients = [
  { label: "Google Machine", value: "1" },
  { label: "Yandex Cloud", value: "2" },
  { label: "Baidu Translate", value: "3" },
  { label: "Bing Translate", value: "4" },
  { label: "Watson Translate", value: "5" },
  { label: "Kakao Translate", value: "6" },
  { label: "PROMT Translate", value: "7" },
  { label: "Youdao Translate", value: "8" },
  { label: "Google Professional", value: "9" },
  { label: "DeepL Professional", value: "10" },
];

export const translatioAgenceClients = [
  { label: "Agency:TextMaster", value: "1" },
  { label: "Agency:LangShop", value: "2" },
];
