//eslint-disable
/**
 * Used to match `RegExp`
 * [syntax characters](http://ecma-international.org/ecma-262/7.0/#sec-patterns).
 */
const reRegExpChar = /[\\^$.*+?()[\]{}|]/g;
const reHasRegExpChar = RegExp(reRegExpChar.source);
const intervalRegexp = /^({\s*(-?\d+(\.\d+)?[\s*,\s*\-?\d+(.\d+)?]*)\s*})|([[\]])\s*(-Inf|\*|-?\d+(\.\d+)?)\s*,\s*(\+?Inf|\*|-?\d+(\.\d+)?)\s*([[\]])$/;
const anyIntervalRegexp = /({\s*(-?\d+(\.\d+)?[\s*,\s*\-?\d+(.\d+)?]*)\s*})|([[\]])\s*(-Inf|\*|-?\d+(\.\d+)?)\s*,\s*(\+?Inf|\*|-?\d+(\.\d+)?)\s*([[\]])/;

/**
 * Escapes the `RegExp` special characters "^", "$", "\", ".", "*", "+",
 * "?", "(", ")", "[", "]", "{", "}", and "|" in `string`.
 *
 * @since 3.0.0
 * @category String
 * @param {string} [string=''] The string to escape.
 * @returns {string} Returns the escaped string.
 * @see escape, escapeRegExp, unescape
 * @example
 *
 * escapeRegExp('[lodash](https://lodash.com/)')
 * // => '\[lodash\]\(https://lodash\.com/\)'
 */
function escapeRegExp(string) {
  return string && reHasRegExpChar.test(string) ? string.replace(reRegExpChar, "\\$&") : string || "";
}

function flatten(arr) {
  return arr.reduce(function(flat, toFlatten) {
    return flat.concat(Array.isArray(toFlatten) ? flatten(toFlatten) : toFlatten);
  }, []);
}

function convertNumber(str) {
  if (str === "-Inf") {
    return -Infinity;
  } else if (str === "+Inf" || str === "Inf" || str === "*") {
    return Infinity;
  }
  return parseInt(str, 10);
}

function pluralize(message, number) {
  // Separate the plural from the singular, if any
  let messageParts = message.split("|");

  // Get the explicit rules, If any
  let explicitRules = [];

  for (let i = 0; i < messageParts.length; i++) {
    messageParts[i] = messageParts[i].trim();

    if (anyIntervalRegexp.test(messageParts[i])) {
      let messageSpaceSplit = messageParts[i].split(/\s/);
      explicitRules.push(messageSpaceSplit.shift());
      messageParts[i] = messageSpaceSplit.join(" ");
    }
  }

  // Check if there's only one message
  if (messageParts.length === 1) {
    // Nothing to do here
    return message;
  }

  // Check the explicit rules
  for (let j = 0; j < explicitRules.length; j++) {
    if (testInterval(number, explicitRules[j])) {
      return messageParts[j];
    }
  }

  let pluralForm = getPluralForm(number);

  return messageParts[pluralForm] || messageParts[messageParts.length - 1];
}

function testInterval(count, interval) {
  if (typeof interval !== "string") {
    throw "Invalid interval: should be a string.";
  }

  interval = interval.trim();

  let matches = interval.match(intervalRegexp);
  if (!matches) {
    throw "Invalid interval: " + interval;
  }

  if (matches[2]) {
    let items = matches[2].split(",");
    for (let i = 0; i < items.length; i++) {
      if (parseInt(items[i], 10) === count) {
        return true;
      }
    }
  } else {
    // Remove falsy values.
    matches = matches.filter(function(match) {
      return !!match;
    });

    let leftDelimiter = matches[1];
    let leftNumber = convertNumber(matches[2]);
    if (leftNumber === Infinity) {
      leftNumber = -Infinity;
    }
    let rightNumber = convertNumber(matches[3]);
    let rightDelimiter = matches[4];

    return (
      (leftDelimiter === "[" ? count >= leftNumber : count > leftNumber) &&
      (rightDelimiter === "]" ? count <= rightNumber : count < rightNumber)
    );
  }

  return false;
}

function getPluralForm(count) {
  const locale = window.AppData.app.locale.split("-")[0];

  switch (locale) {
    case "az":
    case "bo":
    case "dz":
    case "id":
    case "ja":
    case "jv":
    case "ka":
    case "km":
    case "kn":
    case "ko":
    case "ms":
    case "th":
    case "tr":
    case "vi":
    case "zh":
      return 0;

    case "af":
    case "bn":
    case "bg":
    case "ca":
    case "da":
    case "de":
    case "el":
    case "en":
    case "eo":
    case "es":
    case "et":
    case "eu":
    case "fa":
    case "fi":
    case "fo":
    case "fur":
    case "fy":
    case "gl":
    case "gu":
    case "ha":
    case "he":
    case "hu":
    case "is":
    case "it":
    case "ku":
    case "lb":
    case "ml":
    case "mn":
    case "mr":
    case "nah":
    case "nb":
    case "ne":
    case "nl":
    case "nn":
    case "no":
    case "om":
    case "or":
    case "pa":
    case "pap":
    case "ps":
    case "pt":
    case "so":
    case "sq":
    case "sv":
    case "sw":
    case "ta":
    case "te":
    case "tk":
    case "ur":
    case "zu":
      return count === 1 ? 0 : 1;

    case "am":
    case "bh":
    case "fil":
    case "fr":
    case "gun":
    case "hi":
    case "hy":
    case "ln":
    case "mg":
    case "nso":
    case "xbr":
    case "ti":
    case "wa":
      return count === 0 || count === 1 ? 0 : 1;

    case "be":
    case "bs":
    case "hr":
    case "ru":
    case "sr":
    case "uk":
      return count % 10 === 1 && count % 100 !== 11
        ? 0
        : count % 10 >= 2 && count % 10 <= 4 && (count % 100 < 10 || count % 100 >= 20)
        ? 1
        : 2;

    case "cs":
    case "sk":
      return count === 1 ? 0 : count >= 2 && count <= 4 ? 1 : 2;

    case "ga":
      return count === 1 ? 0 : count === 2 ? 1 : 2;

    case "lt":
      return count % 10 === 1 && count % 100 !== 11
        ? 0
        : count % 10 >= 2 && (count % 100 < 10 || count % 100 >= 20)
        ? 1
        : 2;

    case "sl":
      return count % 100 === 1 ? 0 : count % 100 === 2 ? 1 : count % 100 === 3 || count % 100 === 4 ? 2 : 3;

    case "mk":
      return count % 10 === 1 ? 0 : 1;

    case "mt":
      return count === 1
        ? 0
        : count === 0 || (count % 100 > 1 && count % 100 < 11)
        ? 1
        : count % 100 > 10 && count % 100 < 20
        ? 2
        : 3;

    case "lv":
      return count === 0 ? 0 : count % 10 === 1 && count % 100 !== 11 ? 1 : 2;

    case "pl":
      return count === 1 ? 0 : count % 10 >= 2 && count % 10 <= 4 && (count % 100 < 12 || count % 100 > 14) ? 1 : 2;

    case "cy":
      return count === 1 ? 0 : count === 2 ? 1 : count === 8 || count === 11 ? 2 : 3;

    case "ro":
      return count === 1 ? 0 : count === 0 || (count % 100 > 0 && count % 100 < 20) ? 1 : 2;

    case "ar":
      return count === 0
        ? 0
        : count === 1
        ? 1
        : count === 2
        ? 2
        : count % 100 >= 3 && count % 100 <= 10
        ? 3
        : count % 100 >= 11 && count % 100 <= 99
        ? 4
        : 5;

    default:
      return 0;
  }
}

function replaceString(string, find, replace) {
  if (!string) {
    return string;
  }

  if (typeof string !== "string") {
    return string;
  }

  let result = string.split(new RegExp("(" + escapeRegExp(find) + ")", "gi"));

  for (let i = 1, length = result.length; i < length; i += 2) {
    result[i] = replace;
  }

  return result;
}

function replacements(sources, replacements) {
  if (!Array.isArray(sources)) {
    sources = [sources];
  }

  for (let property in replacements) {
    if (!replacements.hasOwnProperty(property)) {
      continue;
    }

    for (let i = 0; i < sources.length; i++) {
      sources[i] = replaceString(sources[i], `:${property}`, replacements[property]);
    }

    sources = flatten(sources);
  }

  return sources;
}

function get(key) {
  let path = key.split("."),
    current = window.AppData.localization;

  for (let i = 0; i < path.length; i++) {
    if (!current[path[i]]) {
      return null;
    }

    current = current[path[i]];
  }

  return current;
}

export function t(key, ...options) {
  let string = get(key);

  return string;
}
