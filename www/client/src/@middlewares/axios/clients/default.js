import { getToken } from "@utils/jwt";
import axios from "axios";
import qs from "qs";

export default {
  client: axios.create({
    maxContentLength: 20000,
    timeout: 180000,
    headers: {
      "Content-Type": "application/json",
    },
    paramsSerializer: function(params) {
      return qs.stringify(params, { arrayFormat: "brackets" });
    },
  }),
  options: {
    interceptors: {
      request: [
        (_, req) => {
          const token = getToken();
          if (token) {
            req.headers.Authorization = `Bearer ${token}`;
          } else {
            req.withCredentials = false;
          }

          return req;
        },
      ],
      response: [],
    },
  },
};
