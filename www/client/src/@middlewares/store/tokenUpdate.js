import { LOGIN, LOGOUT, REFRESH } from "@store/auth";
import { removeToken, setToken } from "@utils/jwt";

export default () => next => async action => {
  switch (action.type) {
    case LOGOUT:
      removeToken();
      break;

    case `${LOGIN}_SUCCESS`:
    case `${REFRESH}_SUCCESS`:
      setToken(action.payload.data.token);
      break;
  }

  return next(action);
};
