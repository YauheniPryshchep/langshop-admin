import { LOGIN, logout, REFRESH, refreshAction } from "@store/auth";
import { getToken, getTokenExpiresAt } from "@utils/jwt";
import { isClientRequestAction, isSkippedAction } from "@utils/actions";

let refreshPromise = null;

export default ({ dispatch }) => next => async action => {
  if (isSkippedAction(action, [REFRESH, LOGIN])) {
    return next(action);
  }

  if (!isClientRequestAction(action)) {
    return next(action);
  }

  if (refreshPromise) {
    try {
      await refreshPromise;
    } catch (e) {
      return;
    }

    return next(action);
  }

  const expiresAt = getTokenExpiresAt(getToken());
  if (!expiresAt) {
    return next(logout());
  }

  const refreshThreshold = (new Date().getTime() + 300000) / 1000; // 5 minutes from now
  if (refreshThreshold > expiresAt) {
    try {
      refreshPromise = dispatch(refreshAction());
      await refreshPromise;
      refreshPromise = null;
    } catch (e) {
      refreshPromise = null;
      return next(logout());
    }
  }

  return next(action);
};
