/* eslint-disable */
import {isClientRequestAction, isRequestFailAction, isSkippedAction} from "@utils/actions";
import {isResponseError, NOT_FOUND} from "@utils/responses";
import isArray from "lodash/isArray";
import isObject from "lodash/isObject";
import head from "lodash/head";
import values from "lodash/values";
import get from "lodash/get";

export default store => next => async action => {

  if (!isClientRequestAction(action)) {
    return next(action);
  }

  let result;

  result = new Promise((resolve, reject) => {
    next(action)
      .then((response) => {
        resolve(response)
      })
      .catch((action) => {
        if (!isRequestFailAction(action) && !!action.error.message) {
          store.dispatch({type: "CREATE_TOAST", payload: {message: action.error.message, action: null, error: true}});
          return;
        }

        if (!isResponseError(action.error.response)) {
          store.dispatch({
            type: "CREATE_TOAST",
            payload: {message: "Internal server error", action: null, error: true},
          });
          return;
        }

        const response = action.error.response;


        if (response.status === NOT_FOUND) {
          return;
        }

        let errors = [];
        if (isArray(response.data.errors)) {
          let message = get(response, 'data.message', null);
          message = message ? [message] : [];
          errors = !response.data.errors.length ? [...message] : response.data.errors;
        } else if (response.data && isObject(response.data)) {
          errors = head(values(response.data));
        }

        store.dispatch({
          type: "CREATE_TOAST",
          payload: {message: head(errors) || "Internal server error", action: null, error: true},
        });

        reject(action.error);
      })
  });


  return result;
};