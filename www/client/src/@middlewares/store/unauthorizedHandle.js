import { LOGIN, logout, REFRESH } from "@store/auth";
import { isClientRequestAction, isSkippedAction } from "@utils/actions";
import { isResponseError, UNAUTHORIZED } from "@utils/responses";
import { isRequestFailAction } from "@utils/actions";

export default () => next => async action => {
  if (isSkippedAction(action, [LOGIN, REFRESH])) {
    return next(action);
  }

  if (!isClientRequestAction(action)) {
    return next(action);
  }

  let result;

  try {
    result = await next(action);
  } catch (action) {
    if (isRequestFailAction(action) && isResponseError(action.error.response, UNAUTHORIZED)) {
      return next(logout());
    }

    throw action;
  }

  return result;
};
