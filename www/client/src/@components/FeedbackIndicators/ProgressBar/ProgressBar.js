import React, { useCallback, useEffect, useState } from "react";
import { ProgressBar as PolarisProgressBar } from "@shopify/polaris";
import useToggle from "@hooks/useToggle";

export const ProgressBar = ({ isLoading }) => {
  const [progress, setProgress] = useState(0);
  const [isProgress, setIsProgress] = useToggle();

  const handleChangeProgress = useCallback(() => {
    if (isLoading && !isProgress && progress < 30) {
      setProgress(state => state + 5);
      setIsProgress();
    }

    if (isLoading && isProgress && progress < 95) {
      setTimeout(() => {
        setProgress(state => (state >= 90 ? 95 : state + 5));
      }, 50);
    }

    if (isLoading && isProgress && progress >= 95) {
      setTimeout(() => {
        setProgress(state => (state >= 99 ? 99 : state + 1));
      }, 50);
    }

    if (!isLoading && isProgress && progress < 100 && progress !== 0) {
      setTimeout(() => {
        setProgress(100);
      }, 500);
    }

    if (!isLoading && isProgress && progress === 100) {
      setTimeout(() => {
        setProgress(0);
      }, 700);
    }
  }, [isLoading, isProgress, setProgress, progress, setIsProgress]);

  useEffect(() => {
    handleChangeProgress();
  }, [handleChangeProgress]);

  if (!progress) {
    return null;
  }

  return (
    <div className={"TopProgressBar"}>
      <PolarisProgressBar size={"small"} progress={progress} />
    </div>
  );
};
