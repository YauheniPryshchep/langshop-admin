import React from "react";
import { EmptySearchResult } from "@shopify/polaris";

export const EmptyTableState = ({ title, description }) => {
  return <EmptySearchResult title={title} description={description} withIllustration={true} />;
};
