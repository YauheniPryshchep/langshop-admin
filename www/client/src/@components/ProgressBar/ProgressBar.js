import React from "react";
import { ProgressBar as PolarisProgressBar } from "@shopify/polaris";
import "./styles.scss";

export const ProgressBar = ({ progress, size, color = "default" }) => {
  return (
    <div className={`Colored-Progress-Bar Colored-Progress-Bar--${color}`}>
      <PolarisProgressBar progress={progress} size={size} />
    </div>
  );
};
