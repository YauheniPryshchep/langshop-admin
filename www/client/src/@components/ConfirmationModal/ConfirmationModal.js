import React from "react";
import { Modal } from "@shopify/polaris";

export const ConfirmationModal = ({
  onCancel,
  onConfirm,
  cancel = "Cancel",
  confirm = "Confirm",
  content,
  destructive,
  loading = false,
  ...props
}) => {
  return (
    <Modal
      {...props}
      onClose={onCancel}
      secondaryActions={[
        {
          content: cancel,
          onAction: onCancel,
        },
      ]}
      primaryAction={{
        content: confirm,
        loading,
        onAction: onConfirm,
        destructive,
      }}
    >
      <Modal.Section>{content}</Modal.Section>
    </Modal>
  );
};
