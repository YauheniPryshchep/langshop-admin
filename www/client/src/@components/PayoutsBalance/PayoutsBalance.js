import React from "react";
import { Card, DisplayText, TextStyle } from "@shopify/polaris";
import { moneyFormat } from "@utils/moneyFormat";
import Padding from "@components/Padding";

export const PayoutsBalance = ({ balance }) => {
  return (
    <Card.Section>
      <DisplayText>{moneyFormat(balance)}</DisplayText>
      <Padding />
      <TextStyle variation={"subdued"}>Available for withdrawal</TextStyle>
    </Card.Section>
  );
};
