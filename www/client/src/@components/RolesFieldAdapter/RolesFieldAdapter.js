import React, { useCallback, useMemo } from "react";
import { Select } from "@shopify/polaris";

export const RolesFieldAdapter = ({ input: { value, onChange }, meta, roles, ...rest }) => {
  const isError = (meta.dirty || meta.touched || meta.submitting) && meta.invalid;

  const valueField = useMemo(() => value[0].id, [value]);

  const handleChange = useCallback(
    value => {
      const role = roles.find(role => role.id === parseInt(value, 10));
      onChange([{ ...role }]);
    },
    [onChange, roles, valueField]
  );

  return <Select {...rest} value={valueField} error={isError ? meta.error : false} onChange={handleChange} />;
};
