import React, {useCallback, useEffect, useMemo, useState} from "react";
import {Filters} from "@shopify/polaris";
import ActiveStatus from "@common/CampaignFilters/Filters/ActiveStatus";
import ChargeStatus from "@common/CampaignFilters/Filters/ChargeStatus";
import StorePlan from "@common/CampaignFilters/Filters/StorePlan";
import InstallationDate from "../../@common/CampaignFilters/Filters/InstallationDate";
import Timezone from "../../@common/CampaignFilters/Filters/Timezone";
import LastEvent from "../../@common/CampaignFilters/Filters/LastEvent";
import InCampaign from "../../@common/CampaignFilters/Filters/InCampaign";
import Limit from "../../@common/CampaignFilters/Filters/Limit";
import {firstCapitalize} from "../../@defaults/firstCapitalize";
import Subscribe from "../../@common/CampaignFilters/Filters/Subscribe";
import Email from "../../@common/CampaignFilters/Filters/Email";
import Domain from "../../@common/CampaignFilters/Filters/Domain";
import {campaigns} from "../../@store/campaigns";
import {debounce, get, map, find} from "lodash";
import {useSelector} from "react-redux";
import {numberFormat} from "../../@utils/numberFormat";

const FILTERS_MESSAGE = {
  installation_date: (key, comparator, date) => {
    return `Installation date: ${comparator.split("_").join(" ")} ${date["start"]} - ${date["end"]}`;
  },
  last_event: (key, comparator, date) => {
    return `Last event: ${comparator.split("_").join(" ")} between ${date["start"]} - ${date["end"]}`;
  },
  emails: (key, comparator, emails) => {
    return `${comparator === "is" ? "Only selected emails: " : "Without selected emails: "} ${emails.join(" ,")}`;
  },
  domains: (key, comparator, stores) => {
    return `${comparator === "is" ? "Only selected stores: " : "Without selected stores: "} ${stores.join(" ,")}`;
  },
  in_campaign: (key, comparator, campaignIds, campaignSelector) => {
    const titles = map(campaignIds, campaign => {
      let title = find(campaignSelector, item => item.id === parseInt(campaign));
      return get(title, "title", campaign);
    });

    return `${comparator === "in" ? "Stores in campaigns:" : "Stores in not campaigns:"} ${titles.join(" ,")}`;
  },
  default: (key, comparator, value) => {
    return `${firstCapitalize(key.split("_").join(" "))}: ${comparator.split("_").join(" ")} ${value
      .split("_")
      .join(" ")}`;
  },
};

export const CampaignFiltersAdapter = ({recipients_filter: {limit, filters}, input: {onChange}}) => {
  const [search, setSearch] = useState("");
  const campaignsSelector = useSelector(campaigns);

  useEffect(() => {
    const filter = filters.find(filter => filter.key === "search");
    setSearch(get(filter, "value", ""));
  }, [filters]);

  const handleChange = useCallback(
    value => {
      let cloneFilters = [...filters];
      const filter = filters.find(f => f.key === value.key);

      if (!filter) {
        cloneFilters = [
          ...cloneFilters,
          {
            ...value,
          },
        ];
      } else if (!value.value) {
        cloneFilters = [...cloneFilters.filter(f => f.key !== value.key)];
      } else if (Array.isArray(value.value) && !value.value.length) {
        cloneFilters = [...cloneFilters.filter(f => f.key !== value.key)];
      } else {
        cloneFilters = [
          ...cloneFilters.map(f => {
            if (f.key === value.key) {
              return {
                ...value,
              };
            }

            return {...f};
          }),
        ];
      }

      onChange({
        limit,
        filters: cloneFilters,
      });
    },
    [filters, onChange, limit]
  );

  const handleFilterRemove = useCallback(
    key => {
      onChange({
        limit: limit,
        filters: filters.filter(filter => filter.key !== key),
      });
    },
    [filters, onChange, limit]
  );

  const handleChangeLimit = useCallback(
    value => {
      let limit = !value ? 1 : value > 200 ? 200 : value;
      onChange({
        limit,
        filters,
      });
    },
    [filters, onChange, limit]
  );

  const options = [
    {
      key: "limit",
      label: "Limit",
      filter: <Limit limit={limit.toString() || 1} onChange={handleChangeLimit}/>,
      shortcut: true,
    },
    {
      key: "active_status",
      label: "Active status",
      filter: <ActiveStatus filters={filters} onChange={handleChange}/>,
      shortcut: true,
    },
    {
      key: "charge_status",
      label: "Charge status",
      filter: <ChargeStatus filters={filters} onChange={handleChange}/>,
      shortcut: true,
    },
    {
      key: "shopify_plan",
      label: "Shopify plan",
      filter: <StorePlan filters={filters} onChange={handleChange}/>,
      shortcut: true,
    },
    {
      key: "installation_date",
      label: "Installation date",
      filter: <InstallationDate filters={filters} onChange={handleChange}/>,
      shortcut: true,
    },
    {
      key: "timezone",
      label: "Timezone",
      filter: <Timezone filters={filters} onChange={handleChange}/>,
      shortcut: true,
    },
    {
      key: "last_event",
      label: "Last event",
      filter: <LastEvent filters={filters} onChange={handleChange}/>,
      shortcut: true,
    },
    {
      key: "in_campaign",
      label: "In campaign",
      filter: <InCampaign filters={filters} onChange={handleChange}/>,
      shortcut: true,
    },
    {
      key: "subscribe",
      label: "Subscribe",
      filter: <Subscribe filters={filters} onChange={handleChange}/>,
      shortcut: true,
    },
    {
      key: "emails",
      label: "Include/Exclude emails",
      filter: <Email filters={filters} onChange={handleChange}/>,
      shortcut: true,
    },
    {
      key: "domains",
      label: "Include/Exclude domain",
      filter: <Domain filters={filters} onChange={handleChange}/>,
      shortcut: true,
    },
  ];

  const handleFiltersClearAll = useCallback(() => {
    onChange({
      limit,
      filters: [],
    });
  }, [onChange, limit]);

  const debounceSearchFilter = useCallback(
    debounce(value => handleChange({key: "search", comparator: "like", value}), 500),
    [handleChange]
  );

  const onSearchChange = useCallback(
    value => {
      setSearch(value);
      debounceSearchFilter(value);
    },
    [setSearch, debounceSearchFilter]
  );

  const handleQueryValueRemove = useCallback(() => {
    onSearchChange("");
  }, [onSearchChange]);

  const appliedFilters = useMemo(() => {
    let appliedFilters = [];
    for (let i = 0, length = filters.length; i < length; i++) {
      const filter = filters[i];

      if (filter.key === "in_campaign") {
        appliedFilters.push({
          key: filter.key,
          label: FILTERS_MESSAGE[filter.key](filter.key, filter.comparator, filter.value, campaignsSelector),
          onRemove: handleFilterRemove,
        });
      } else if (["installation_date", "last_event", "emails", "domains", "in_campaign"].includes(filter.key)) {
        appliedFilters.push({
          key: filter.key,
          label: FILTERS_MESSAGE[filter.key](filter.key, filter.comparator, filter.value),
          onRemove: handleFilterRemove,
        });
      } else {
        appliedFilters.push({
          key: filter.key,
          label: FILTERS_MESSAGE["default"](filter.key, filter.comparator, filter.value),
          onRemove: handleFilterRemove,
        });
      }
    }

    if (limit) {
      appliedFilters.push({
        key: "limit",
        label: `Limit is ${limit ? numberFormat(limit) : "unlimited"}`,
        onRemove: () => false,
      });
    }

    return appliedFilters;
  }, [filters, limit, campaignsSelector]);

  return (
    <Filters
      filters={options}
      appliedFilters={appliedFilters}
      queryValue={search}
      onQueryChange={onSearchChange}
      onQueryClear={handleQueryValueRemove}
      onClearAll={handleFiltersClearAll}
    />
  );
};
