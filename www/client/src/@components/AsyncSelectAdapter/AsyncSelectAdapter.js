import React from "react";
import { fetchStoresAction } from "../../@store/stores";
import AsyncSelect from "react-select/async/dist/react-select.esm";
import { useDispatch } from "react-redux";
import { get, filter } from "lodash";

export const AsyncSelectAdapter = ({ input: { onChange } }) => {
  const dispatch = useDispatch();

  const customStyles = {
    menu: () => ({
      zIndex: 99999,
    }),
    singleValue: (provided, state) => {
      const opacity = state.isDisabled ? 0.5 : 1;
      const transition = "opacity 300ms";

      return { ...provided, opacity, transition };
    },
  };

  const getOptions = input => {
    return dispatch(fetchStoresAction({ filter: input, limit: 10 }))
      .then(({ payload }) => {
        const data = get(payload, "data.data.items", []);
        return data.map(item => ({
          label: item.name,
          value: item.name,
        }));
      })
      .catch(() => {
        return [];
      });
  };

  return (
    <AsyncSelect
      styles={customStyles}
      cacheOptions
      defaultOptions
      loadOptions={getOptions}
      onChange={({ value }) => onChange(value)}
    />
  );
};
