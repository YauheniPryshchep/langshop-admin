import React, { useCallback, useMemo } from "react";
import { Tabs } from "@shopify/polaris";

export const LanguageTabs = ({ languages, langCode, handleChange }) => {
  const tabs = useMemo(() => {
    return languages.map(lang => ({
      id: lang.code,
      content: lang.title,
    }));
  }, [languages]);

  const selected = useMemo(() => {
    let index = languages.findIndex(l => l.code === langCode);
    return index;
  }, [langCode, languages]);

  const onSelect = useCallback(
    index => {
      handleChange(languages[index].code || languages[0].code);
    },
    [selected, languages]
  );

  return <Tabs selected={selected} tabs={tabs} onSelect={onSelect} />;
};
