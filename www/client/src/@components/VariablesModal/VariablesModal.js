import React from "react";
import { Modal, TextContainer, TextStyle } from "@shopify/polaris";
import { TemplateVariables } from "../../@common/TemplateVariables/TemplateVariables";

export const VariablesModal = ({ onCancel, onConfirm, confirm = "Close", ...props }) => {
  return (
    <Modal
      {...props}
      onClose={onCancel}
      secondaryActions={[
        {
          content: confirm,
          onAction: onConfirm,
        },
      ]}
    >
      <Modal.Section>
        <TemplateVariables />
        <TextContainer>
          <TextStyle variation={"strong"}>*- variable always available</TextStyle>
        </TextContainer>
      </Modal.Section>
    </Modal>
  );
};
