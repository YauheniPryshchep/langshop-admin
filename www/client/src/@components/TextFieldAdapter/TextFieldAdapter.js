import React from "react";
import { TextField } from "@shopify/polaris";

export const TextFieldAdapter = ({ input: { value, onChange }, meta, ...rest }) => {
  const isError = (meta.dirty || meta.touched || meta.submitting) && meta.invalid;
  return <TextField {...rest} value={value} error={isError ? meta.error : false} onChange={onChange} />;
};
