import React from "react";
import { Checkbox } from "@shopify/polaris";

export const CheckboxFieldAdapter = ({ input: { value, onChange }, meta, ...rest }) => {
  const isError = (meta.dirty || meta.touched || meta.submitting) && meta.invalid;

  return <Checkbox {...rest} checked={value} error={isError ? meta.error : false} onChange={onChange} />;
};
