import React from 'react';
import {ContextualSaveBar} from "@shopify/polaris";

export const ContextSaveBar = (
  {
    onSuccess,
    onDiscard,
    loading,
    disabled

  }
) => {
  return (
    <ContextualSaveBar
      message="Unsaved changes"
      saveAction={{
        onAction: onSuccess,
        loading,
        disabled,
      }}
      discardAction={{
        onAction: onDiscard,
        disabled: loading
      }}
    />
  );
};
