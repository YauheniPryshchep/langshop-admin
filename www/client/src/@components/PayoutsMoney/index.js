import { payouts, isLoading as payoutsLoading } from "@store/partners/payouts";
import { isLoading as balanceLoading } from "@store/balance";
import { PayoutsMoney } from "./PayoutsMoney";
import { createStructuredSelector } from "reselect";
import { connect } from "react-redux";

const mapState = createStructuredSelector({
  balanceLoading,
  payouts,
  payoutsLoading,
});

export default connect(mapState, null)(PayoutsMoney);
