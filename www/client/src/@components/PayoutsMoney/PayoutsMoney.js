import React, { useCallback, useMemo } from "react";
import { Badge, Card, Stack } from "@shopify/polaris";
import { moneyFormat } from "../../@utils/moneyFormat";
import {
  CONVERSION_RATE,
  TRANSACTION_ACCEPTED_STATUS,
  TRANSACTION_FROZEN_STATUS,
  TRANSACTION_PAID_STATUS,
  TRANSACTION_PENDING_STATUS,
  TRANSACTION_STATUSES,
} from "../../@defaults/constants";
import { diffInDaysFromNow } from "../../@utils/diffInDays";
import { useTransactionCommission } from "../../@hooks/useTransactionCommission";
import reduce from "lodash/reduce";
import sumBy from "lodash/sumBy";
import filter from "lodash/filter";

export const PayoutsMoney = ({ balanceLoading, payouts, payoutsLoading, transactions, commissions }) => {
  const [getTransactionCommission] = useTransactionCommission(commissions);
  const pendingMoney = useMemo(() => {
    let pendingPayouts = filter(payouts, payout => payout.status === TRANSACTION_PENDING_STATUS);
    return reduce(
      pendingPayouts,
      (sum, { payout_transactions }) =>
        sum +
        sumBy(
          payout_transactions,
          // eslint-disable-next-line
          t => t.amount
        ),
      0
    );
  }, [payouts]);

  const paidMoney = useMemo(() => {
    let pendingPayouts = filter(payouts, payout => payout.status === TRANSACTION_PAID_STATUS);
    return reduce(
      pendingPayouts,
      (sum, { payout_transactions }) =>
        sum +
        sumBy(
          payout_transactions,
          // eslint-disable-next-line
          t => t.amount
        ),
      0
    );
  }, [payouts]);

  const acceptedMoney = useMemo(() => {
    let frozenTransactions = filter(transactions, ({ createdAt }) => diffInDaysFromNow(createdAt) >= 14);
    return sumBy(frozenTransactions, t => t.commission);
  }, [transactions, getTransactionCommission]);

  const frozenMoney = useMemo(() => {
    let frozenTransactions = filter(transactions, ({ createdAt }) => diffInDaysFromNow(createdAt) < 14);
    return sumBy(frozenTransactions, t => t.commission);
  }, [transactions, getTransactionCommission]);

  const totalMoney = useMemo(() => acceptedMoney + frozenMoney + paidMoney + pendingMoney, [
    acceptedMoney,
    frozenMoney,
    paidMoney,
    pendingMoney,
  ]);

  const getSectionTitle = useCallback((status, amount) => {
    const TITLES = {
      "1": "Pending",
      "2": "Paid",
      "3": "Declined",
      "4": "Frozen",
      "5": "Money are ready for withdrawal.",
    };
    return (
      <Stack distribution={"equalSpacing"}>
        <Stack.Item>{amount}</Stack.Item>
        <Stack.Item>
          <Badge status={TRANSACTION_STATUSES[status]}>{TITLES[status]}</Badge>
        </Stack.Item>
      </Stack>
    );
  }, []);

  const paidMoneySection = useMemo(() => {
    if (!paidMoney) {
      return null;
    }

    return <Card.Section title={getSectionTitle(TRANSACTION_PAID_STATUS, moneyFormat(paidMoney))} />;
  }, [paidMoney, getSectionTitle]);

  const pendingMoneySection = useMemo(() => {
    if (!pendingMoney) {
      return null;
    }

    return <Card.Section title={getSectionTitle(TRANSACTION_PENDING_STATUS, moneyFormat(pendingMoney))} />;
  }, [pendingMoney, getSectionTitle]);

  const acceptedMoneySection = useMemo(() => {
    if (!acceptedMoney) {
      return null;
    }

    return <Card.Section title={getSectionTitle(TRANSACTION_ACCEPTED_STATUS, moneyFormat(acceptedMoney))} />;
  }, [acceptedMoney, getSectionTitle]);

  const frozenMoneySection = useMemo(() => {
    if (!frozenMoney) {
      return null;
    }

    return <Card.Section title={getSectionTitle(TRANSACTION_FROZEN_STATUS, moneyFormat(frozenMoney))} />;
  }, [frozenMoney, getSectionTitle]);

  if (payoutsLoading || balanceLoading) {
    return null;
  }

  return (
    <React.Fragment>
      <Card.Section title={"Total"}>{moneyFormat(totalMoney)}</Card.Section>
      {paidMoneySection}
      {pendingMoneySection}
      {acceptedMoneySection}
      {frozenMoneySection}
    </React.Fragment>
  );
};
