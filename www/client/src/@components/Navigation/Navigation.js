import React from "react";
import { Navigation as PolarisNavigation } from "@shopify/polaris";
import { useLocation } from "react-router-dom";

export const Navigation = ({ navigation = [] }) => {
  const location = useLocation();

  return (
    <PolarisNavigation location={location.pathname}>
      {navigation.map((section, i) => (
        <PolarisNavigation.Section {...section} key={i} />
      ))}
    </PolarisNavigation>
  );
};
