import useStoreData from "@hooks/useStoreData";

export const StorePlan = ({ store }) => {
  const { planDisplayName } = useStoreData(store);

  return planDisplayName;
};
