import moment from "moment";

export const CreatedAt = ({ createdAt, format = "ll" }) => {
  return moment(createdAt).format(format);
};
