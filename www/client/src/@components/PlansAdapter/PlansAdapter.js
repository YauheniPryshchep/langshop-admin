import React, { useMemo } from "react";
import { ChoiceList } from "@shopify/polaris";

export const PlansAdapter = ({ input: { value, onChange }, ...rest }) => {
  const choices = useMemo(() => {
    return [
      {
        label: "Standard Plan",
        value: "2",
      },
      {
        label: "Advanced Plan",
        value: "3",
      },
      {
        label: "Enterprise Plan",
        value: "4",
      },
    ];
  }, []);

  return <ChoiceList choices={choices} selected={value} onChange={onChange} allowMultiple={true} {...rest} />;
};
