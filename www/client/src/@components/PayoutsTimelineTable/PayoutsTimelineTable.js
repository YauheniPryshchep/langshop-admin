import React, { useMemo } from "react";
import { DataTable, Link, SkeletonBodyText, Card } from "@shopify/polaris";
import PayoutStatus from "@components/PayoutStatus";
import PayoutAmount from "@components/PayoutAmount";
import EmptyTableState from "@components/FeedbackIndicators/EmptyTableState";
import Pagination from "@components/Pagination";
import get from "lodash/get";
import map from "lodash/map";
import moment from "moment";

export const PayoutsTimelineTable = ({ payouts, isLoading, pagination, perPage }) => {
  const loadingRows = useMemo(() => {
    return map(Array.apply(null, { length: 10 }), i => [
      <SkeletonBodyText key={`name-${i}`} lines={1} />,
      <SkeletonBodyText key={`plan-${i}`} lines={1} />,
      <SkeletonBodyText key={`date-${i}`} lines={1} />,
    ]);
  }, []);

  const rows = useMemo(() => {
    if (isLoading) {
      return loadingRows;
    }

    return map(payouts, item => {
      return [
        <Link key={item.id + "link"} url={`/partners/payouts/${get(item, "id", 0)}`}>
          {moment(get(item, "created_at", new Date())).format("ll")}
        </Link>,
        <PayoutStatus key={item.id + "status"} status={get(item, "status", null)} />,
        <PayoutAmount key={item.id + "amount"} transactions={get(item, "payout_transactions", [])} />,
      ];
    });
  }, [payouts, isLoading, loadingRows]);

  const paginationMarkup =
    payouts && payouts.length ? (
      <Card.Section>
        <Pagination pagination={pagination} perPage={perPage} />
      </Card.Section>
    ) : null;

  if (!rows.length) {
    return (
      <Card.Section>
        <EmptyTableState title={"No payouts find"} description={"Payouts list is empty"} />
      </Card.Section>
    );
  }

  return (
    <React.Fragment>
      <Card.Section>
        <DataTable
          headings={["Created at", "Status", "Amount"]}
          columnContentTypes={["text", "text", "numeric"]}
          rows={rows}
        />
      </Card.Section>
      {paginationMarkup}
    </React.Fragment>
  );
};
