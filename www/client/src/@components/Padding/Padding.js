import React from "react";

export const Padding = ({ value = 2, dimension = "rem" }) => {
  return <div style={{ paddingTop: `${value}${dimension}` }} />;
};
