import React, { useCallback } from "react";
import { SettingToggle } from "@shopify/polaris";

export const ToggleFieldAdapter = ({
  disabled = false,
  contentStatus,
  textStatus,
  input: { value, onChange },
  ...rest
}) => {
  const handleToggle = useCallback(() => onChange(!value), [value]);

  if (!contentStatus) {
    contentStatus = !value ? "Enable" : "Disable";
  }

  return (
    <SettingToggle
      {...rest}
      action={{
        content: contentStatus,
        onAction: handleToggle,
        disabled: disabled,
      }}
      enabled={value}
    >
      {textStatus}
    </SettingToggle>
  );
};
