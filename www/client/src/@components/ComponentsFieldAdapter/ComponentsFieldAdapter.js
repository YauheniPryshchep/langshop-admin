import React, { useCallback } from "react";
import "./styles.scss";

export const ComponentsFieldAdapter = ({ elements, input: { value, onChange } }) => {
  const elementClassName = useCallback(
    element => {
      let classNames = ["Components-Field"];

      if (element.id === value) {
        classNames.push("Components-Field--Selected");
      }

      if (element.classNames) {
        classNames = [...classNames, ...element.classNames];
      }

      return classNames.join(" ");
    },
    [value]
  );

  const handleChange = useCallback(element => onChange(element.id), [onChange]);

  return (
    <div className={"Components-Fields"}>
      {elements.map(element => (
        <div
          key={element.id}
          className={`Components-Field__Wrapper Components-Field--value-${element.id}`}
          style={{
            width: `${100 / elements.length}%`,
          }}
        >
          <div className={elementClassName(element)} onClick={() => handleChange(element)}>
            {element.component}
          </div>
        </div>
      ))}
    </div>
  );
};
