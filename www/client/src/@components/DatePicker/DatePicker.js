import React, {useCallback, useMemo, useState} from "react";
import {Button, ChoiceList, DatePicker, FormLayout, Popover, TextField} from "@shopify/polaris";
import {CalendarMajor} from "@shopify/polaris-icons";
import useToggle from "../../@hooks/useToggle";
import moment from "moment-timezone";

export const CustomDatePicker = ({
                                   input: {value, onChange},
                                   isDisabled,
                                   title = "Campaign start",
                                   fieldLabel = "Start at",
                                   defaultLabel = "Immediately",
                                   defaultValue = "immediately",
                                   isShowDefault = "true",
                                   ...rest
                                 }) => {

  const [selected, setSelected] = useState(value || !isShowDefault ? ["on-date"] : defaultValue);
  const [{month, year}, setDate] = useState({
    month: moment().month(),
    year: moment().year(),
  });

  const [active, handleToggle] = useToggle();

  const valueTextField = useMemo(() => {
    return value ? moment(value).format("DD MMMM YYYY") : defaultLabel;
  }, [value]);
  const activator = useMemo(() => {
    return <Button icon={CalendarMajor} onClick={handleToggle} disabled={isDisabled}/>;
  }, [handleToggle, isDisabled]);

  const handleMonthChange = useCallback(
    (month, year) => {
      setDate({month, year});
    },
    [month, year]
  );

  const onChangeDate = useCallback(date => {
    const {start} = date;
    onChange(moment(start).toDate());
  }, []);
  const handleChange = useCallback(
    option => {
      if (option[0] === defaultValue) {
        setSelected(option);
        onChange(null);
      } else {
        setSelected(option);
      }
    },
    [setSelected, onChange]
  );

  return (
    <FormLayout>
      {!!isShowDefault && (
        <ChoiceList
          title={title}
          choices={[
            {label: defaultLabel, value: defaultValue},
            {
              label: `${fieldLabel}: ${value ? moment(value).format("YYYY-MM-DD HH:mm:ss") : ""}`,
              value: "on-date"
            },
          ]}
          selected={selected}
          onChange={handleChange}
          disabled={isDisabled}
        />
      )}
      {selected[0] === "on-date" || !isShowDefault ? (
        <React.Fragment>
          <TextField
            label={fieldLabel}
            type={"text"}
            readOnly={true}
            value={valueTextField}
            connectedRight={
              <Popover active={active} onClose={handleToggle} activator={activator}>
                <DatePicker
                  {...rest}
                  month={month}
                  year={year}
                  onMonthChange={handleMonthChange}
                  selected={value ? moment(value).toDate() : moment(null).toDate()}
                  onChange={onChangeDate}
                />
              </Popover>
            }
          />
        </React.Fragment>
      ) : (
        ""
      )}
    </FormLayout>
  );
};
