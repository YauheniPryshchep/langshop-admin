import React, {useEffect, useMemo} from "react";
import {Select} from "@shopify/polaris";

export const FlowMailgunTemplate = ({
                                      templates,
                                      fetchTemplatesAction,
                                      isLoading,
                                      isFetched,
                                      isDisabled,
                                      template,
                                      onChange,
                                    }) => {
  useEffect(() => {
    fetchTemplatesAction();
  }, []);

  const options = useMemo(() => {
    return [
      {label: "Select template", value: ""},
      ...templates.map(template => ({
        value: template.name,
        label: template.name,
      })),
    ];
  }, [templates]);

  return (
    <Select
      label={"Template"}
      options={options}
      value={template}
      disabled={isLoading || !isFetched || isDisabled}
      onChange={onChange}
    />
  );
};
