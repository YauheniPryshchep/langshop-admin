import { templates, fetchTemplatesAction, isLoading, isFetched } from "@store/templates";
import { FlowMailgunTemplate } from "./FlowMailgunTemplate";
import { createStructuredSelector } from "reselect";
import { connect } from "react-redux";

const mapState = createStructuredSelector({
  templates,
  isLoading,
  isFetched,
});

const mapDispatch = {
  fetchTemplatesAction,
};

export default connect(mapState, mapDispatch)(FlowMailgunTemplate);
