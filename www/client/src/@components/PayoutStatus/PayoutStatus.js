import React from "react";
import { Badge } from "@shopify/polaris";
import { TRANSACTION_STATUSES } from "@defaults/constants";

const TITLES = {
  "1": "Pending",
  "2": "Paid",
  "3": "Declined",
  "4": "Frozen",
  "5": "Money are ready for withdrawal.",
};

export const PayoutStatus = ({ status }) => {
  return <Badge status={TRANSACTION_STATUSES[status]}>{TITLES[status]}</Badge>;
};
