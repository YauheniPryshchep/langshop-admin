import { moneyFormat } from "@utils/moneyFormat";
import reduce from "lodash/reduce";

export const PayoutAmount = ({ transactions }) => {
  return moneyFormat(reduce(transactions, (sum, { amount }) => sum + amount, 0));
};
