import React from "react";
import { Layout } from "@shopify/polaris";

export const GridLayout = ({ items }) => {
  if (!items.length) {
    return null;
  }

  return (
    <Layout>
      {items.map((item, i) => (
        <Layout.Section key={i} oneThird>
          {item}
        </Layout.Section>
      ))}
    </Layout>
  );
};
