import React from "react";
import { TextStyle } from "@shopify/polaris";
import useStoreData from "@hooks/useStoreData";

export const StoreName = ({ store }) => {
  const { name } = useStoreData(store);

  return (
    <h3>
      <TextStyle variation="strong">{name}</TextStyle>
    </h3>
  );
};
