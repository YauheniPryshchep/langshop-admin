import React, { useCallback, useMemo, useState } from "react";
import { Button, DatePicker, Popover, TextField } from "@shopify/polaris";
import { CalendarMajor } from "@shopify/polaris-icons";
import useToggle from "@hooks/useToggle";
import moment from "moment-timezone";

export const DatePickerFieldAdapter = ({
  input: { value, onChange },
  fieldLabel = "Referral from",
  fieldHelpText = "The date from which the store is considered registered in our referral program",
  isDisabled = false,
  ...rest
}) => {
  const [{ month, year }, setDate] = useState({
    month: moment().month(),
    year: moment().year(),
  });
  const [active, handleToggle] = useToggle();

  const valueTextField = useMemo(() => {
    return !value ? "" : moment(value).format("DD MMMM YYYY");
  }, [value]);

  const activator = useMemo(() => {
    return <Button icon={CalendarMajor} onClick={handleToggle} disabled={isDisabled} />;
  }, [handleToggle, isDisabled]);

  const handleMonthChange = useCallback(
    (month, year) => {
      setDate({ month, year });
    },
    [month, year]
  );

  const onChangeDate = useCallback(date => {
    const { start } = date;
    onChange(moment(start).format("YYYY-MM-DD HH:mm:ss"));
  }, []);

  return (
    <React.Fragment>
      <TextField
        label={fieldLabel}
        type={"text"}
        readOnly={true}
        value={valueTextField}
        helpText={fieldHelpText}
        connectedRight={
          <Popover active={active} onClose={handleToggle} activator={activator}>
            <DatePicker
              month={month}
              year={year}
              onMonthChange={handleMonthChange}
              selected={value ? new Date(value) : new Date()}
              onChange={onChangeDate}
              {...rest}
            />
          </Popover>
        }
      />
    </React.Fragment>
  );
};
