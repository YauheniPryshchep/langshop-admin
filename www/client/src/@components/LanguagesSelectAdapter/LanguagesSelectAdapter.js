import React from "react";
import { Select } from "@shopify/polaris";
import { languages } from "@store/config/languages";

export const LanguagesSelectAdapter = ({ input: { value, onChange }, readOnly, ...rest }) => {
  return (
    <Select
      {...rest}
      options={[
        { label: "Select language", value: "" },
        ...languages.map(lang => ({
          label: lang.title,
          value: lang.code,
        })),
      ]}
      disabled={readOnly}
      value={value}
      onChange={onChange}
    />
  );
};
