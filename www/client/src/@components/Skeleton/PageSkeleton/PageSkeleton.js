import React from "react";

import { SkeletonPage, SkeletonBodyText, TextContainer } from "@shopify/polaris";

export const PageSkeleton = () => {
  return (
    <SkeletonPage>
      <TextContainer spacing={"loose"}>
        <SkeletonBodyText lines={5} />
        <SkeletonBodyText lines={5} />
        <SkeletonBodyText lines={5} />
      </TextContainer>
    </SkeletonPage>
  );
};
