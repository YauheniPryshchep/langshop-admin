import React from "react";

import { AppProvider, Frame, TopBar } from "@shopify/polaris";
import en from "@shopify/polaris/locales/en";
import PageSkeleton from "@components/Skeleton/PageSkeleton";
import { theme } from "@utils/theme";

export const FrameSkeleton = () => {
  return (
    <AppProvider i18n={en} theme={theme} features={{ newDesignLanguage: false }}>
      <Frame topBar={<TopBar />} navigation={[]}>
        <PageSkeleton />
      </Frame>
    </AppProvider>
  );
};
