import React from "react";
import { SkeletonBodyText, Card, SkeletonDisplayText } from "@shopify/polaris";

export const AuthSkeleton = () => {
  return (
    <Card>
      <Card.Section>
        <SkeletonDisplayText size="extraLarge" />
      </Card.Section>
      <Card.Section>
        <SkeletonBodyText lines={10} />
      </Card.Section>
    </Card>
  );
};
