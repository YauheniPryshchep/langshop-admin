import React, { useMemo } from "react";
import { Pagination as PolarisPagination, Select, Stack } from "@shopify/polaris";

export const Pagination = ({ pagination, perPage, distribution = "equalSpacing" }) => {
  const paginationMarkup = useMemo(() => {

    if (!pagination) {
      return null;
    }

    return (
      <Stack.Item>
        <PolarisPagination {...pagination} />
      </Stack.Item>
    );
  }, [pagination]);


  const limitSelectionMarkup = useMemo(() => {
    if (!perPage) {
      return null;
    }

    const { onChange, limit, options } = perPage;

    return (
      <Stack.Item>
        <Stack alignment="center" spacing={"extraTight"}>
          <Select
            label="Limit"
            labelHidden
            options={options.map(option => {
              return {
                label: option,
                value: option,
              };
            })}
            onChange={onChange}
            value={limit}
          />
        </Stack>
      </Stack.Item>
    );
  }, [perPage]);

  return (
    <Stack distribution={distribution} alignment="center">
      {limitSelectionMarkup}
      {paginationMarkup}
    </Stack>
  );
};
