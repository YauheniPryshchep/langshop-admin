import React from "react";
import { Avatar } from "@shopify/polaris";

export const LanguagesFlagIcon = ({ code }) => {
  return (
    <Avatar source={`https://cdn.langshop.app/buckets/app/img/svg/languages/flags/circle/${code.split("-")[0]}.svg`} />
  );
};
