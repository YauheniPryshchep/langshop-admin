import React, { useMemo } from "react";
import { Editor } from "@tinymce/tinymce-react/lib/es2015/main/ts";

// Import TinyMCE
import "tinymce/tinymce";
import "tinymce/themes/silver";

// Any plugins you want to use has to be imported
import "tinymce/plugins/lists";
import "tinymce/plugins/link";
import "tinymce/plugins/code";
import "tinymce/plugins/autoresize";

import "tinymce/skins/ui/oxide/skin.css";

export const EditorFieldAdapter = ({ labelHidden, label, input: { value, onChange, onBlur }, init = {}, ...rest }) => {
  const id = useMemo(() => {
    return Math.random()
      .toString(36)
      .substring(7);
  }, []);

  return (
    <div className={labelHidden ? "Polaris-Labelled--hidden" : ""}>
      <div className="Polaris-Labelled__LabelWrapper">
        <div className="Polaris-Label">
          <label className="Polaris-Label__Text" htmlFor={id}>
            {label}
          </label>
        </div>
      </div>
      <Editor
        {...rest}
        init={{
          menubar: false,
          plugins: ["lists link", "code autoresize"],
          toolbar: "undo redo | formatselect | bold italic | \
            bullist numlist | removeformat code",
          autoresize_bottom_margin: 20,
          branding: false,
          skin: false,
          content_css: false,
          ...init,
        }}
        id={id}
        value={value}
        onEditorChange={onChange}
        onBlue={onBlur}
      />
    </div>
  );
};
