import React, { useCallback, useEffect, useMemo, useState } from "react";
import { Icon, Thumbnail } from "@shopify/polaris";
import { DeleteMinor } from "@shopify/polaris-icons";
import ImagePicker from "@svg/image-picker.svg";
import "./styles.scss";

export const ImageFieldAdapter = ({ input: { value, onChange }, ...rest }) => {
  const [source, setSource] = useState(value);

  const id = useMemo(() => {
    return Math.random()
      .toString(36)
      .substring(7);
  }, []);

  useEffect(() => {
    if (!(value instanceof File)) {
      setSource(value);
      return;
    }

    const reader = new FileReader();
    reader.readAsDataURL(value);
    reader.onload = () => {
      setSource(reader.result);
    };
  }, [value]);

  const handleChange = useCallback(
    e => {
      let target = e.currentTarget;
      const file = target.files[0];

      onChange(file);

      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => {
        setSource(reader.result);
        target.value = null;
      };
    },
    [onChange, setSource]
  );

  const handleClear = useCallback(() => {
    onChange("");
    setSource("");
  }, [setSource]);

  const isEmpty = useMemo(() => !source, [source]);

  const classNames = ["Image-Picker"];

  if (isEmpty) {
    classNames.push("Image-Picker--empty");
  }

  return (
    <div className={classNames.join(" ")}>
      <input id={id} type="file" onChange={handleChange} accept="image/x-png,image/gif,image/jpeg" />
      <label htmlFor={id}>
        {!isEmpty ? (
          <Thumbnail {...rest} source={source} size={"large"} />
        ) : (
          <span className="Polaris-Thumbnail Polaris-Thumbnail--sizeLarge">
            <span className="Polaris-Thumbnail__Image" dangerouslySetInnerHTML={{ __html: ImagePicker }} />
          </span>
        )}
      </label>
      {!isEmpty && (
        <span className={"Polaris-Icon-Wrapper"} onClick={handleClear}>
          <Icon source={DeleteMinor} color={"white"} />
        </span>
      )}
    </div>
  );
};
