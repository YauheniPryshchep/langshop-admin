import React, { useMemo } from "react";
import { DataTable, Link, SkeletonBodyText } from "@shopify/polaris";
import { storeName } from "@utils/routes";
import { moneyFormat } from "@utils/moneyFormat";
import { CreatedAt } from "../CreatedAt/CreatedAt";
import map from "lodash/map";
import get from "lodash/get";
import reduce from "lodash/reduce";

export const PayoutInformation = ({ items, isLoading }) => {
  const loadingRows = useMemo(() => {
    return map(Array.apply(null, { length: 10 }), i => [
      <SkeletonBodyText key={`name-${i}`} lines={1} />,
      <SkeletonBodyText key={`plan-${i}`} lines={1} />,
      <SkeletonBodyText key={`date-${i}`} lines={1} />,
    ]);
  }, []);

  const rows = useMemo(() => {
    if (isLoading) {
      return loadingRows;
    }

    return map(items, item => {
      const name = get(item, "myshopifyDomain");
      return [
        <Link key={`link-${item.id}`} url={`/stores/${storeName(name)}`}>
          {name}
        </Link>,
        <CreatedAt key={`createdAt-${item.created_at}`} createdAt={get(item, "created_at", new Date())} />,
        moneyFormat(get(item, "amount", 0)),
      ];
    });
  }, [items, isLoading, loadingRows]);

  const totals = useMemo(() => {
    return reduce(items, (acc, { amount }) => acc + amount, 0);
  }, [items]);

  return (
    <DataTable
      headings={["Store Name", "Created at", "Amount"]}
      columnContentTypes={["text", "text", "numeric"]}
      rows={rows}
      totals={["", "", `${moneyFormat(totals)}`]}
      showTotalsInFooter={true}
    />
  );
};
