import React from "react";
import { Select } from "@shopify/polaris";

export const SelectFieldAdapter = ({ input: { value, onChange, onBlur }, meta, ...rest }) => {
  const isError = (meta.dirty || meta.touched || meta.submitting) && meta.invalid;

  return <Select {...rest} value={value} error={isError ? meta.error : false} onChange={onChange} />;
};
