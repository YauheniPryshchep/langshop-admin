import React from "react";
import { Link } from "react-router-dom";

export const LinkAdapter = ({ url, external, ...rest }) => {
  if (external) {
    return <a href={url} {...rest} target="_blank" rel="noreferrer noopener" />;
  }

  return <Link to={url} {...rest} />;
};
