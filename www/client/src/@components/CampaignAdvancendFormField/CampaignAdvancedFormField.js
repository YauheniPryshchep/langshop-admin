import React from "react";
import {Card, FormLayout} from "@shopify/polaris";
import TextFieldAdapter from "../TextFieldAdapter";
import {Field} from "redux-form";
import {email} from "redux-form-validators";
import {FromAdvancedCampaignAdapter} from "../../@common/FromAdvancedCampaignAdapter/FromAdvancedCampaignAdapter";

const validateEmail = value => {
  if (!value) {
    return undefined;
  }
  return email()(value);
};

export const CampaignAdvancedFormField = ({isDisabled}) => {

  return (
    <Card title={"Advanced field for campaigns"}>
      <Card.Section>
        <FormLayout>
          <Field
            component={FromAdvancedCampaignAdapter}
            name="from"
            label="From (optional)"
            type="email"
            readOnly={isDisabled}
            validate={validateEmail}
          />
          <Field
            component={TextFieldAdapter}
            name="sender"
            label="Sender (optional)"
            type="text"
            readOnly={isDisabled}
          />
          <Field
            component={TextFieldAdapter}
            name="reply_to"
            label="Reply To (optional)"
            type="text"
            readOnly={isDisabled}
          />
        </FormLayout>
      </Card.Section>
    </Card>
  );
};
