import React, { useCallback, useMemo } from "react";
import { Tabs } from "@shopify/polaris";

export const TabsFieldAdapter = ({ tabs, input: { value, onChange }, disabled, ...rest }) => {
  const selected = useMemo(
    () =>
      tabs.findIndex(tab => {
        return tab.id === value;
      }),
    [tabs, value]
  );

  const handleChange = useCallback(
    selected => {
      if (disabled) {
        return;
      }

      onChange(tabs[selected].id);
    },
    [tabs, disabled, onChange]
  );

  return <Tabs {...rest} tabs={tabs} selected={selected} onSelect={handleChange} />;
};
