import {ResourceFilter} from './ResourceFilter'
import PropTypes from 'prop-types'

ResourceFilter.propTypes = {
  onClearAll: PropTypes.func.isRequired,
  queryPlaceholder: PropTypes.string.isRequired,
  handleChange: PropTypes.func.isRequired,
  skipKeys: PropTypes.arrayOf(PropTypes.string),
  options: PropTypes.arrayOf(PropTypes.object)
};
export default ResourceFilter;
