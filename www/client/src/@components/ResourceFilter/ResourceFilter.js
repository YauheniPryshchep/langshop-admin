import React, {createElement, useCallback, useMemo, useState} from 'react';
import {Filters} from "@shopify/polaris";
import get from "lodash/get";
import find from "lodash/find";
import filter from "lodash/filter";
import map from "lodash/map";
import debounce from "lodash/debounce";
import includes from "lodash/includes";
import isEmpty from "lodash/isEmpty";
import reduce from "lodash/reduce";
import trim from "lodash/trim";

const defaultSearchFilter = {
  key: 'search_term',
  value: '',
  badge: false
};

export const ResourceFilter = ({
                                 options,
                                 queryPlaceholder,
                                 handleChange,
                                 onClearAll,
                                 skipKeys = ['search_term', 'sort_order', 'tab', 'page']
                               }) => {

    const searchFilter = useMemo(() => {
      return find(options, option => option.key === 'search_term') || defaultSearchFilter
    }, [options]);

    const [query, setQuery] = useState(get(searchFilter, 'value', ''));

    const debounceSearchChange = useMemo(() => debounce(
      value => handleChange({...searchFilter, value: trim(value, ' ')}), 500),
      [handleChange, searchFilter]);

    const handleChangeQuery = useCallback((value) => {
      const newValue = value.length >= 255 ? query : value;
      setQuery(newValue);
      debounceSearchChange(newValue)
    }, [debounceSearchChange, query]);

    const handleQueryValueRemove = useCallback(() => {
      setQuery("");
      handleChange({
        ...searchFilter,
        value: ""
      })
    }, [searchFilter, handleChange, setQuery]);

    const filters = useMemo(() => {
      return filter(options, option => !includes(skipKeys, option.key))
    }, [options, skipKeys]);

    const createdFilter = useMemo(() => {
      return map(filters, (filter) => {
        const value = get(filter, 'value', '');

        return {
          ...filter,
          filter: (createElement(filter.component, {
            ...filter.props,
            [filter.valueKey]: value,
            onChange: (value) => handleChange({
              ...filter,
              value
            })
          }))
        }
      })
    }, [filters, handleChange]);

    const appliedFilters = useMemo(() => {
      return reduce(filters, (acc, filter) => isEmpty(filter.value)
        ? [...acc]
        : [
          ...acc,
          {
            key: filter.key,
            label: filter.appliedFilter(filter.value),
            onRemove: () => handleChange({
              ...filter,
              value: filter.initialValue
            })
          }], [])
    }, [filters, handleChange]);

    return (
      <Filters
        queryValue={query}
        appliedFilters={appliedFilters}
        filters={createdFilter}
        queryPlaceholder={queryPlaceholder}
        onQueryChange={handleChangeQuery}
        onQueryClear={handleQueryValueRemove}
        onClearAll={onClearAll}
      />
    );
  }
;
