import React, { useMemo } from "react";
import { DataTable, SkeletonBodyText } from "@shopify/polaris";
import EmptyTableState from "@components/FeedbackIndicators/EmptyTableState";
import CreatedAt from "@components/CreatedAt";
import StoreLink from "@components/StoreLink";
import StorePlan from "@components/StorePlan";
import getActiveReferral from "@utils/getActiveReferral";
import map from "lodash/map";
import get from "lodash/get";

export const ReferralHistoryTable = ({ stores, isLoading }) => {
  const loadingRows = useMemo(() => {
    return map(Array.apply(null, { length: 10 }), i => [
      <SkeletonBodyText key={`name-${i}`} lines={1} />,
      <SkeletonBodyText key={`plan-${i}`} lines={1} />,
      <SkeletonBodyText key={`date-${i}`} lines={1} />,
    ]);
  }, []);

  const rows = useMemo(() => {
    if (isLoading) {
      return loadingRows;
    }
    return map(stores, store => {
      const referral = getActiveReferral(store);
      return [
        <StoreLink key={`link-${store.id}`} store={store} />,
        <StorePlan key={`plan-${store.id}`} store={store} />,
        <CreatedAt key={`createdAt-${store.id}`} createdAt={get(referral, "created_at", null)} />,
      ];
    });
  }, [stores, isLoading, loadingRows]);

  const headings = useMemo(() => ["Store Name", "Shopify Plan", "Referral Date"], []);

  if (!rows.length) {
    return <EmptyTableState title={"Referral stores no found"} description={"Change filters"} />;
  }

  return <DataTable columnContentTypes={["text", "text", "text"]} headings={headings} rows={rows} />;
};
