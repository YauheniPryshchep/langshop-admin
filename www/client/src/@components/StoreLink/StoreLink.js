import React from "react";
import {Link} from "@shopify/polaris";
import useStoreData from "@hooks/useStoreData";
import {storeDomain, storeName} from "../../@utils/routes";

export const StoreLink = ({store}) => {
  const {name, myshopifyDomain} = useStoreData(store);

  return <Link url={`/stores/${storeName(myshopifyDomain)}`}>{name}</Link>;
};
