import {
  CustomersMajor,
  HomeMajor,
  SocialPostMajor,
  OnlineStoreMajor,
  TextMajor,
  MarketingMajor,
  ReferralMajor,
  ChannelsMajor
} from "@shopify/polaris-icons";
import {
  hasScope,
  hasSomeScope,
  SHOW_DEMO_STORES,
  SHOW_NEWS,
  SHOW_STORES_DISCOUNTS,
  SHOW_STORES_TRIALS,
  SHOW_USERS,
  SHOW_LOCALES,
  SHOW_CAMPAIGNS,
  SHOW_VOCABULARIES,
  SHOW_COUPONS,
  SHOW_PARTNERS_USERS,
  SHOW_PARTNERS_PAYOUTS, SHOW_WORKFLOWS,
} from "@utils/scopes";
import {SHOW_CUSTOMERS} from "@utils/scopes";

export default [
  {
    label: "Dashboard",
    icon: HomeMajor,
    url: "/",
    exactMatch: true,
  },
  {
    label: "News",
    icon: SocialPostMajor,
    url: "/news",
    allowed: profileScopes => hasScope(profileScopes, SHOW_NEWS),
  },
  {
    label: "Users",
    icon: CustomersMajor,
    url: "/users",
    allowed: profileScopes => hasScope(profileScopes, SHOW_USERS),
  },
  {
    label: "Stores",
    icon: OnlineStoreMajor,
    url: "/stores",
    subNavigationItems: [
      {
        label: "All stores",
        url: "/stores",
        allowed: () => true,
      },
      {
        label: "Demo",
        url: "/discounts/demo-stores",
        allowed: profileScopes => hasScope(profileScopes, SHOW_DEMO_STORES),
      },
      {
        label: "Discounts",
        url: "/discounts/discount-stores",
        allowed: profileScopes => hasScope(profileScopes, SHOW_STORES_DISCOUNTS),
      },
      {
        label: "Trials",
        url: "/discounts/trial-stores",
        allowed: profileScopes => hasScope(profileScopes, SHOW_STORES_TRIALS),
      },
      {
        label: "Customers",
        url: "/customers",
        allowed: profileScopes => hasScope(profileScopes, SHOW_CUSTOMERS),
      },
    ],
  },
  {
    label: "Marketing",
    icon: MarketingMajor,
    allowed: profileScopes => hasSomeScope(profileScopes, [SHOW_CAMPAIGNS]),
    subNavigationItems: [
      {
        label: "Campaigns",
        url: "/marketing/campaigns",
        allowed: profileScopes => hasScope(profileScopes, SHOW_CAMPAIGNS),
      },
      {
        label: "Coupons",
        url: "/marketing/coupons",
        allowed: profileScopes => hasScope(profileScopes, SHOW_COUPONS),
      },
    ],
  },
  {
    label: "Affiliate",
    icon: ReferralMajor,
    allowed: profileScopes => hasSomeScope(profileScopes, [SHOW_PARTNERS_USERS, SHOW_PARTNERS_PAYOUTS]),
    subNavigationItems: [
      {
        label: "Partners",
        url: "/partners/users",
        allowed: profileScopes => hasScope(profileScopes, SHOW_PARTNERS_USERS),
      },
      {
        label: "Payouts",
        url: "/partners/payouts",
        allowed: profileScopes => hasScope(profileScopes, SHOW_PARTNERS_PAYOUTS),
      },
    ],
  },
];

export const footerSectionNavigation = [
  {
    label: "Misc",
    icon: TextMajor,
    url: "/misc",
    allowed: profileScopes => hasScope(profileScopes, SHOW_VOCABULARIES) || hasScope(profileScopes, SHOW_LOCALES),
  },
];

export const miscNavigationItems = [
  {
    label: "Locales",
    icon: TextMajor,
    description: "",
    url: "/misc/locales",
    allowed: profileScopes => hasScope(profileScopes, SHOW_LOCALES),
  },
  {
    label: "Translation memories",
    description: "",
    icon: TextMajor,
    url: "/misc/memories",
    allowed: profileScopes => hasScope(profileScopes, SHOW_VOCABULARIES),
  },
];
