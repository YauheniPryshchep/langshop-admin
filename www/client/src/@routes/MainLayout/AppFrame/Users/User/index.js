import { initialValues, fetchUserForm, resetUserForm, isFormLoaded, updateUserAction } from "@store/user";
import { roles } from "@store/roles";
import { scopes } from "@store/scopes";
import { User } from "./User";
import { createStructuredSelector } from "reselect";
import { connect } from "react-redux";
import { reduxForm } from "redux-form";

const mapState = createStructuredSelector({
  roles,
  scopes,
  isFormLoaded,
  initialValues,
});

const mapDispatch = {
  fetchUserForm,
  resetUserForm,
  updateUserAction,
};

export default connect(
  mapState,
  mapDispatch
)(
  reduxForm({
    form: "user-form",
    enableReinitialize: true,
  })(User)
);
