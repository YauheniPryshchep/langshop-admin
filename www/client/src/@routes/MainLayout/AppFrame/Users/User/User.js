import React, { useCallback, useEffect, useMemo } from "react";
import { Card, EmptyState, Layout, Page, PageActions, Select, TextContainer } from "@shopify/polaris";
import PageSkeleton from "@components/Skeleton/PageSkeleton";
import TextFieldAdapter from "@components/TextFieldAdapter";
import useHasScope from "@hooks/useHasScope";
import { UPDATE_USERS } from "@utils/scopes";
import RolesFieldAdapter from "@components/RolesFieldAdapter";
import { pageTitle } from "../../../../../@defaults/pageTitle";
import { useParams } from "react-router-dom";
import { useSelector } from "react-redux";
import { Field, getFormValues } from "redux-form";
import { first, get } from "lodash";
import { required } from "redux-form-validators";

export const User = ({
  title,
  roles,
  scopes,
  updateUserAction,
  fetchUserForm,
  resetUserForm,
  isFormLoaded,
  form,
  reset,
  handleSubmit,
  dirty,
}) => {
  const { userId } = useParams();
  const hasScope = useHasScope();
  const user = useSelector(getFormValues(form));
  const hasUpdateScope = hasScope(UPDATE_USERS);

  useEffect(() => {
    fetchUserForm(userId);
  }, [userId]);

  // On unmount
  useEffect(() => resetUserForm, [userId]);

  const handleSave = useCallback(
    async data => {
      await updateUserAction(userId, data);
      reset();
    },
    [userId, reset]
  );

  const userScopes = useMemo(() => {
    const roleId = get(first(user.roles), "id", null);
    return get(
      roles.find(role => role.id === roleId),
      "scopes",
      []
    );
  }, [roles, user]);

  const primaryAction = useMemo(() => {
    if (!hasUpdateScope) {
      return null;
    }

    return {
      content: "Save",
      disabled: !isFormLoaded || !dirty,
      onAction: handleSubmit(handleSave),
    };
  }, [handleSubmit, handleSave, dirty, isFormLoaded, hasUpdateScope]);

  if (!isFormLoaded) {
    return <PageSkeleton />;
  }

  if (!user.id) {
    return (
      <EmptyState
        heading="User not found"
        action={{
          content: "Show users",
          url: "/users",
        }}
        image="https://cdn.shopify.com/s/files/1/0757/9955/files/empty-state.svg"
      >
        <p>Go to users list to show all</p>
      </EmptyState>
    );
  }

  return (
    <Page
      title={user ? pageTitle(user.name) : pageTitle(title)}
      breadcrumbs={[
        {
          content: "Users",
          url: "/users",
        },
      ]}
      primaryAction={primaryAction}
      separator
    >
      <Layout>
        <Layout.Section>
          <Card title={"Information"} sectioned>
            <TextContainer>
              <Field
                component={TextFieldAdapter}
                name="name"
                label="Name"
                type="text"
                readOnly={!hasUpdateScope}
                validate={[required()]}
              />
              <Field component={TextFieldAdapter} name="email" label="Email" type="email" readOnly disabled />
              <Field
                name="roles"
                component={RolesFieldAdapter}
                user={user}
                label="Role"
                options={roles.map(role => ({
                  value: role.id,
                  label: role.description,
                }))}
                roles={roles}
                disabled={!hasUpdateScope}
              />
            </TextContainer>
          </Card>
        </Layout.Section>
        <Layout.Section secondary>
          <Card title={"Scopes"} sectioned>
            <TextContainer spacing={"tight"}>
              {userScopes.length ? (
                userScopes.map(scope => <p key={scope.id}>{scope.description}</p>)
              ) : (
                <p>There are no any scopes for this role</p>
              )}
            </TextContainer>
          </Card>
        </Layout.Section>
        <Layout.Section>
          <PageActions primaryAction={primaryAction} />
        </Layout.Section>
      </Layout>
    </Page>
  );
};
