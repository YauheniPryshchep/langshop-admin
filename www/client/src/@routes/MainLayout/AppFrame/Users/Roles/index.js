import { roles, fetchRolesAction, resetRolesAction, isFetched, isLoading, total } from "@store/roles";
import { Roles } from "./Roles";
import { createStructuredSelector } from "reselect";
import { connect } from "react-redux";

const mapState = createStructuredSelector({
  roles,
  isFetched,
  isLoading,
  total,
});

const mapDispatch = {
  fetchRolesAction,
  resetRolesAction,
};

export default connect(mapState, mapDispatch)(Roles);
