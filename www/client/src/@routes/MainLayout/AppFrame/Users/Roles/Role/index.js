import {
  fetchRoleAction,
  isFetched,
  isLoading,
  removeRoleAction,
  resetRoleAction,
  initialValues,
  createRoleAction,
  updateRoleAction,
} from "@store/role";
import { scopes } from "@store/scopes";
import { Role } from "./Role";
import { createStructuredSelector } from "reselect";
import { connect } from "react-redux";
import { reduxForm } from "redux-form";

const mapState = createStructuredSelector({
  initialValues,
  isLoading,
  isFetched,
  scopes,
});

const mapDispatch = {
  fetchRoleAction,
  createRoleAction,
  updateRoleAction,
  removeRoleAction,
  resetRoleAction,
};

export default connect(
  mapState,
  mapDispatch
)(
  reduxForm({
    form: "role-form",
    enableReinitialize: true,
  })(Role)
);
