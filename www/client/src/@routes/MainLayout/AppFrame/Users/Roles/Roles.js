import React, { useCallback, useEffect, useMemo } from "react";
import { Card, Filters, Page, ResourceItem, ResourceList, Stack, TextStyle } from "@shopify/polaris";

import useQuery from "@hooks/useQuery";
import useFetch from "@hooks/useFetch";
import Pagination from "@components/Pagination";
import useHasScope from "@hooks/useHasScope";
import { UPDATE_USERS_ROLES } from "@utils/scopes";
import useCancelToken from "@hooks/useCancelToken";
import { sortOptions, limitOptions } from "@defaults/options";
import { numberFormat } from "../../../../../@defaults/numberFormat";
import { pageTitle } from "../../../../../@defaults/pageTitle";

export const Roles = ({ title, roles, fetchRolesAction, resetRolesAction, isFetched, isLoading, total }) => {
  const [query, setQuery] = useQuery();
  const hasScope = useHasScope();
  const [cancelToken, cancelRequests] = useCancelToken();

  const {
    page,
    limit,
    filter,
    sort,
    search,
    pages,
    onPreviousPage,
    onNextPage,
    onLimitChange,
    onSortChange,
    onSearchChange,
    onSearchClear,
  } = useFetch({
    query,
    sortOptions,
    limitOptions,
    isLoading,
    total,
  });

  useEffect(() => {
    const sortOption = sortOptions.find(option => sort === option.value);

    fetchRolesAction(
      {
        filter,
        limit,
        page: page,
        sort: {
          field: sortOption.field,
          direction: sortOption.direction,
        },
      },
      cancelToken
    );
  }, [page, limit, sort, filter]);

  // On unmount
  useEffect(
    () => () => {
      cancelRequests();
      resetRolesAction();
    },
    []
  );

  useEffect(() => {
    if (!isFetched) {
      return;
    }

    setQuery(getRequestQuery());
  }, [isFetched, roles]);

  const getRequestQuery = useCallback(() => {
    let currentPage = page;

    if (currentPage < 1) {
      currentPage = 1;
    } else if (currentPage > pages) {
      currentPage = pages;
    }

    const requestQuery = {
      page: currentPage,
      limit: limit,
      sort: sort,
    };

    if (filter) {
      requestQuery["q"] = filter;
    }

    return requestQuery;
  }, [filter, limit, sort, page, pages]);

  const pagination = useMemo(() => {
    return {
      hasPrevious: pages > 1 && page > 1,
      hasNext: pages > 1 && page < pages,
      onPrevious: onPreviousPage,
      onNext: onNextPage,
    };
  }, [pages, page, onPreviousPage, onNextPage]);

  const perPage = useMemo(() => {
    return {
      limit: limit,
      options: limitOptions,
      onChange: onLimitChange,
    };
  }, [limit, onLimitChange]);

  const filterControl = (
    <Filters
      queryValue={search}
      queryPlaceholder="Search roles ..."
      filters={[]}
      appliedFilters={[]}
      onQueryChange={onSearchChange}
      onQueryClear={onSearchClear}
      onClearAll={onSearchClear}
    />
  );

  const renderItem = item => {
    const { id, name, description } = item;

    return (
      <ResourceItem id={id} url={`/users/roles/${id}`}>
        <Stack distribution={"equalSpacing"}>
          <Stack.Item>
            <TextStyle variation="strong">{name}</TextStyle>
          </Stack.Item>
          <Stack.Item>{description}</Stack.Item>
        </Stack>
      </ResourceItem>
    );
  };

  const resourceListMarkup = (
    <ResourceList
      showHeader
      resourceName={{
        singular: "role",
        plural: "roles",
      }}
      sortOptions={sortOptions}
      sortValue={sort}
      onSortChange={onSortChange}
      totalItemsCount={numberFormat(total)}
      loading={isLoading}
      items={roles}
      filterControl={filterControl}
      renderItem={renderItem}
    />
  );

  const paginationMarkup =
    roles && roles.length ? (
      <Card.Section>
        <Pagination pagination={pagination} perPage={perPage} />
      </Card.Section>
    ) : null;

  return (
    <Page
      title={pageTitle(title)}
      breadcrumbs={[
        {
          content: "Users",
          url: "/users",
        },
      ]}
      primaryAction={
        hasScope(UPDATE_USERS_ROLES)
          ? {
              content: "Add new",
              url: "/users/roles/new",
            }
          : null
      }
      separator
    >
      <Card>
        {resourceListMarkup}
        {paginationMarkup}
      </Card>
    </Page>
  );
};
