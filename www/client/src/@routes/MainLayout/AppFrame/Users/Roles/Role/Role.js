import React, { useCallback, useEffect, useMemo } from "react";
import { Card, EmptyState, Layout, Page, PageActions, TextContainer } from "@shopify/polaris";
import PageSkeleton from "@components/Skeleton/PageSkeleton";
import RoleScopes from "@common/RoleScopes";
import useHasScope from "@hooks/useHasScope";
import { UPDATE_USERS_ROLES } from "@utils/scopes";
import TextFieldAdapter from "@components/TextFieldAdapter";
import ConfirmationModal from "@components/ConfirmationModal";
import useActive from "@hooks/useActive";
import { pageTitle } from "../../../../../../@defaults/pageTitle";
import { Field, getFormValues } from "redux-form";
import { useHistory, useParams } from "react-router-dom";
import { useSelector } from "react-redux";
import { required } from "redux-form-validators";

const validate = {
  name: [
    required({
      message: "Role name is required",
    }),
  ],
  description: [
    required({
      message: "Role description is required",
    }),
  ],
};

export const Role = ({
  handleSubmit,
  fetchRoleAction,
  scopes,
  createRoleAction,
  removeRoleAction,
  updateRoleAction,
  resetRoleAction,
  isLoading,
  isFetched,
  change,
  form,
  dirty,
  invalid,
}) => {
  const history = useHistory();
  const hasScope = useHasScope();
  const hasUpdateScope = hasScope(UPDATE_USERS_ROLES);
  const { roleId } = useParams();
  const [removeModalOpened, openRemoveModal, closeRemoveModal] = useActive();

  const loading = useMemo(() => {
    if (isLoading) {
      return true;
    }

    if (!roleId) {
      return false;
    }

    return !isFetched;
  }, [roleId, isLoading, isFetched]);

  const role = useSelector(getFormValues(form));

  // On loading by route params
  useEffect(() => {
    if (!roleId) {
      return;
    }

    fetchRoleAction(roleId);
  }, [roleId]);

  // On new created
  useEffect(() => {
    if (roleId || !role.id) {
      return;
    }

    history.replace(`/users/roles/${role.id}`);
  }, [roleId, role.id]);

  // On unmount
  useEffect(() => resetRoleAction, [roleId]);

  const isProtectedRole = useMemo(() => (role.id ? ["sadmin", "nobody"].includes(role.name) : false), [role]);

  const handleSave = useCallback(
    async data => {
      if (roleId) {
        updateRoleAction(roleId, data);
      } else {
        createRoleAction(data);
      }
    },
    [roleId, role.id]
  );

  const handleRemove = useCallback(async () => {
    await removeRoleAction(roleId);
    history.replace("/users/roles");
  }, [roleId]);

  const isUpdateRole = useCallback(scopeName => {
    const name = scopeName.split("-");
    return name[name.length - 1] === "update";
  }, []);

  const getShowRole = useCallback(scopeName => {
    const name = scopeName.split("-");
    name[name.length - 1] = "show";
    return name.join("-");
  }, []);

  const handleAddScope = useCallback(
    scope => {
      if (isUpdateRole(scope.name)) {
        let showRoleName = getShowRole(scope.name);
        let showRole = role.scopes.find(s => s.name === showRoleName);
        if (!showRole) {
          let showScope = scopes.find(s => s.name === showRoleName);
          if (showScope) {
            change("scopes", value => [...value, showScope]);
          }
        }
      }
      change("scopes", value => [...value, scope]);
    },
    [change, scopes, role]
  );

  const handleRemoveScope = useCallback(
    scope => change("scopes", value => value.filter(item => item.id !== scope.id)),
    [change]
  );

  const primaryAction = useMemo(() => {
    if (!hasUpdateScope) {
      return null;
    }

    return {
      content: "Save",
      onAction: handleSubmit(handleSave),
      disabled: !dirty || loading || invalid,
    };
  }, [handleSubmit, handleSave, dirty, loading, hasUpdateScope, invalid]);

  const secondaryActions = useMemo(() => {
    if (!roleId || isProtectedRole || !hasUpdateScope) {
      return [];
    }

    return [
      {
        content: "Remove",
        destructive: true,
        onAction: openRemoveModal,
        disabled: loading,
      },
    ];
  }, [loading, roleId, isProtectedRole, hasUpdateScope, openRemoveModal]);

  if (loading) {
    return <PageSkeleton />;
  }

  if (!hasUpdateScope) {
    return (
      <EmptyState
        heading="Forbidden!"
        action={{
          content: "Show roles",
          url: "/users/roles",
        }}
        image="https://cdn.shopify.com/s/files/1/0757/9955/files/empty-state.svg"
      >
        <p></p>
      </EmptyState>
    );
  }

  if (roleId && !role.id) {
    return (
      <EmptyState
        heading="Role not found"
        action={{
          content: "Show roles",
          url: "/users/roles",
        }}
        image="https://cdn.shopify.com/s/files/1/0757/9955/files/empty-state.svg"
      >
        <p>Go to roles list to show all</p>
      </EmptyState>
    );
  }

  return (
    <Page
      title={role.name ? pageTitle(`Edit role ${role.name}`) : pageTitle("New Role")}
      primaryAction={primaryAction}
      breadcrumbs={[
        {
          content: "Roles",
          url: "/users/roles",
        },
      ]}
      separator
    >
      <Layout>
        <Layout.Section>
          <RoleScopes
            editable={hasUpdateScope && !isProtectedRole}
            role={role}
            handleAddScope={handleAddScope}
            handleRemoveScope={handleRemoveScope}
          />
        </Layout.Section>
        <Layout.Section secondary>
          <Card title={"Information"} sectioned>
            <TextContainer>
              <Field
                component={TextFieldAdapter}
                name="name"
                label="Name"
                type="text"
                readOnly={!!roleId}
                validate={validate.name}
                normalize={value => value.replace(/[^a-zA-Z_-]/g, "")}
              />
              <Field
                component={TextFieldAdapter}
                name="description"
                label="Description"
                type="text"
                validate={validate.description}
                readOnly={!hasUpdateScope}
              />
            </TextContainer>
          </Card>
        </Layout.Section>
        <Layout.Section>
          <PageActions primaryAction={primaryAction} secondaryActions={secondaryActions} />
        </Layout.Section>
      </Layout>
      <ConfirmationModal
        destructive
        title="Remove role"
        content="Are you sure you want to remove role?"
        confirm="Remove"
        open={removeModalOpened}
        onCancel={closeRemoveModal}
        onConfirm={handleRemove}
      />
    </Page>
  );
};
