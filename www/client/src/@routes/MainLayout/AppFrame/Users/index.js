import { users, fetchUsersAction, resetUsersAction, isFetched, isLoading, total } from "@store/users";
import { Users } from "./Users";
import { createStructuredSelector } from "reselect";
import { connect } from "react-redux";

const mapState = createStructuredSelector({
  users,
  isFetched,
  isLoading,
  total,
});

const mapDispatch = {
  fetchUsersAction,
  resetUsersAction,
};

export default connect(mapState, mapDispatch)(Users);
