import { store, isLoaded, resetPlanToFree, isUpdating, changePlanPrice } from "@store/store";
import { demoStore } from "@store/demo-store";
import { Store } from "./Store";
import { createStructuredSelector } from "reselect";
import { connect } from "react-redux";

const mapState = createStructuredSelector({
  store,
  demoStore,
  isLoaded,
  isUpdating,
});

const mapDispatch = {
  resetPlanToFree,
  changePlanPrice,
};

export default connect(mapState, mapDispatch)(Store);
