import React, {useCallback, useMemo} from "react";
import {Layout, Page} from "@shopify/polaris";
import {shopifyPartnerUrl, storeDomain} from "@utils/routes";
import useConfig from "@hooks/useConfig";
import StoreInfoCard from "@common/StoreInfoCard";
import StoreRebateCard from "@common/StoreRebateCard";
import StoreHistoryCard from "@common/StoreHistoryCard";
import useHasScope from "@hooks/useHasScope";
import {SHOW_STORES_HISTORY} from "@utils/scopes";
import {backHistory} from "@store/history-listener";
import StoresSessions from "@common/StoresSessions";
import {pageTitle} from "@defaults/pageTitle";
import useActive from "@hooks/useActive";
import {UPDATE_STORES_DISCOUNTS} from "@utils/scopes";
import ChangePlanModal from "@common/Modals/ChangePlanModal";
import {get} from "lodash";
import {useParams} from "react-router-dom";
import {useSelector} from "react-redux";
import {PLAN_ENTERPRISE} from "@defaults/constants";
import {useNotification} from "@hooks/useNotification";

export const Store = ({title, store, isLoaded, isUpdating, resetPlanToFree, demoStore, changePlanPrice}) => {
  const {shopifyPartnerId} = useConfig();
  const [openConfirm, handleOpen, handleClose] = useActive();
  const hasScope = useHasScope();
  const {handleNotify} = useNotification();
  const hasUpdateScope = hasScope(UPDATE_STORES_DISCOUNTS);

  const {name} = useParams();
  const domain = useMemo(() => storeDomain(name), [name]);

  const subscription = useMemo(() => {
    return get(store, "subscription", null);
  }, [store]);

  const isActiveSubscription = useMemo(() => {
    const planId = get(subscription, "plan_id", 0);
    const status = get(subscription, "status", 0);

    return planId > 0 && status === 2;
  }, [subscription]);

  const backButton = useSelector(backHistory)(
    ["/discounts/demo-stores", "/discounts/trial-stores", "/discounts/discount-stores", "/stores"],
    {
      content: "Stores",
      url: `/stores`,
    }
  );

  const resetPlan = useCallback(
    values => {
      const { planId } = values;

      if (planId === PLAN_ENTERPRISE) {
        changePlanPrice(domain, {
          ...values,
        }).then(() => {
          handleNotify("Successfully change external plan price.");

        });
      } else {
        resetPlanToFree(domain, {
          ...values,
        }).then(() => {
          handleNotify("Successfully change plan to free");
        });
      }

      handleClose();
    },
    [resetPlanToFree, handleClose]
  );

  return (
    <Page
      title={isLoaded && store && store.data.name ? pageTitle(store.data.name) : pageTitle(title)}
      breadcrumbs={[backButton]}
      primaryAction={
        isLoaded && store
          ? {
            content: "Login",
            url: `https://${store.name}/admin`,
            external: true,
          }
          : null
      }
      secondaryActions={
        isLoaded && store && store.data.id
          ? [
            {
              content: "Shopify partner",
              url: shopifyPartnerUrl(shopifyPartnerId, `/stores/${store.data.id}`),
              external: true,
            },
            {
              content: "Change Plan",
              onAction: handleOpen,
              disabled: !hasUpdateScope || !isActiveSubscription,
            },
          ]

          : []
      }
      separator
    >
      <Layout>
        {hasScope(SHOW_STORES_HISTORY) && (
          <Layout.Section>
            <StoreHistoryCard domain={domain} store={store} />
          </Layout.Section>
        )}
        <Layout.Section secondary>
          <StoreInfoCard domain={domain}/>
          <StoreRebateCard domain={domain} active={get(store, "active", false)}/>
          <StoresSessions storeId={get(store, "id", null)} customerEmail={get(store, "data.customer_email", "")}/>
        </Layout.Section>
      </Layout>
      <ChangePlanModal
        store={store}
        demoStore={demoStore}
        open={openConfirm}
        onClose={handleClose}
        loading={isUpdating}
        handleCreate={resetPlan}
      />
    </Page>
  );
};
