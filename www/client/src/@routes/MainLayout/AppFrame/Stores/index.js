import { stores, fetchStoresAction, resetStoresAction, isFetched, isLoading, total } from "@store/stores";
import { Stores } from "./Stores";
import { createStructuredSelector } from "reselect";
import { connect } from "react-redux";

const mapState = createStructuredSelector({
  stores,
  isFetched,
  isLoading,
  total,
});

const mapDispatch = {
  fetchStoresAction,
  resetStoresAction,
};

export default connect(mapState, mapDispatch)(Stores);
