import React, {useCallback, useEffect, useMemo} from "react";
import {Card, Page, ResourceItem, ResourceList, TextStyle} from "@shopify/polaris";
import Pagination from "@components/Pagination";
import {storeName} from "@utils/routes";
import StoreStatus from "@common/StoreStatus";
import StorePlan from "@common/StorePlan";
import {numberFormat} from "@defaults/numberFormat";
import {pageTitle} from "@defaults/pageTitle";
import {Row, Col} from "react-flexbox-grid";
import {initialFilters} from "./constants";

import moment from "moment-timezone";
import useQueryString from "@hooks/useQueryString";
import useFilters from "@hooks/useFilters";
import {mergeFilters} from "@utils/mergeFilters";
import useNavigation from "@hooks/useNavigation";
import ResourceFilter from "@components/ResourceFilter";
import find from "lodash/find";
import get from "lodash/get";


const sortDefault = "last-event-desc";

const sortOptions = [
  {
    label: "Last event (Newest)",
    value: "last-event-desc",
    field: "last_event.timestamp",
    direction: "desc",
  },
  {
    label: "Last event (Oldest)",
    value: "last-event-asc",
    field: "last_event.timestamp",
    direction: "asc",
  },
  {
    label: "Alphabetically (A-Z)",
    value: "domain-asc",
    field: "name",
    direction: "asc",
  },
  {
    label: "Alphabetically (Z-A)",
    value: "domain-desc",
    field: "name",
    direction: "desc",
  },
];

export const Stores = ({title, fetchStoresAction, resetStoresAction, stores, isLoading, total, page, pages}) => {

  const [query, setQuery, convertFilters] = useQueryString();
  const [filters, handleChangeFilters, onClearAll] = useFilters(mergeFilters(initialFilters, query), isLoading);
  const [pagination, isHasPagination] = useNavigation(page, pages, handleChangeFilters);

  const sort = useMemo(() => {
    return get(find(filters, option => option.key === 'sort'), 'value') || sortDefault
  }, [filters]);


  const handleFetchData = useCallback(() => {
    setQuery(convertFilters(filters));
    fetchStoresAction(convertFilters(filters));
  }, [filters, setQuery, convertFilters, fetchStoresAction]);

  const handleChangeSort = useCallback((value) => {
    handleChangeFilters({
      key: "sort",
      value
    })
  }, [handleChangeFilters]);

  useEffect(() => {
    handleFetchData();
    return () => resetStoresAction();
  }, [handleFetchData, resetStoresAction]);

  const storeLastEvent = store => (store.last_event ? moment.unix(store.last_event.timestamp).fromNow() : null);

  const renderItem = item => {
    const {name} = item;

    return (
      <ResourceItem id={name} url={`/stores/${storeName(name)}`}>
        <Row style={{alignItems: "center"}}>
          <Col xs={6}>
            <TextStyle variation="strong">{name}</TextStyle>
            <div>
              <TextStyle variation="subdued">{storeLastEvent(item)}</TextStyle>
            </div>
          </Col>
          <Col xs={3}>
            <StorePlan store={item} badge={false}/>
          </Col>
          <Col xs={3} style={{textAlign: "right"}}>
            <StoreStatus store={item}/>
          </Col>
        </Row>
      </ResourceItem>
    );
  };

  const resourceListMarkup = (
    <ResourceList
      showHeader
      resourceName={{
        singular: "store",
        plural: "stores",
      }}
      sortOptions={sortOptions}
      sortValue={sort}
      onSortChange={handleChangeSort}
      totalItemsCount={numberFormat(total)}
      loading={isLoading}
      items={stores}
      filterControl={<ResourceFilter
        options={filters}
        queryPlaceholder={"Search stores..."}
        handleChange={handleChangeFilters}
        onClearAll={onClearAll}
        skipKeys={['search_term', 'sort', 'tab', "page"]}
      />}
      renderItem={renderItem}
    />
  );

  const paginationMarkup =
    isHasPagination ? (
      <Card.Section>
        <Pagination pagination={pagination}/>
      </Card.Section>
    ) : null;

  return (
    <Page title={pageTitle(title)} separator>
      <Card>
        {resourceListMarkup}
        {paginationMarkup}
      </Card>
    </Page>
  );
};
