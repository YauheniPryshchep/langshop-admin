import {ChoiceList} from "@shopify/polaris";
import split from "lodash/split";
import {plans} from "@utils/humanShopifyPlan";

export const initialFilters = [
  {
    key: "search_term",
    value: "",
    initialValue: "",
    badge: false,
  },
  {
    key: "sort",
    value: 'last-event-desc',
    initialValue: "last-event-desc",
    badge: false,
  },
  {
    key: "page",
    value: 1,
    initialValue: 1,
    badge: false
  },
  {
    key: 'active_status',
    value: '',
    initialValue: '',
    badge: true,
    label: "Active Status",
    component: ChoiceList,
    props: {
      title: "Store active status",
      titleHidden: true,
      choices: [
        {label: "Installed", value: 'installed'},
        {label: "Uninstalled", value: 'uninstalled'},
      ],
      allowMultiple: false
    },
    valueKey: "selected",
    replaceFilter: (value) => split(value, ','),
    appliedFilter: (value) => `Store is: ${split(value, ', ')}`

  },
  {
    key: 'langshop_plan',
    value: '',
    initialValue: '',
    badge: true,
    label: "Langshop Plan",
    component: ChoiceList,
    props: {
      title: "Langshop Plan",
      titleHidden: true,
      choices: [
        {label: "Without Plan", value: 'without_plan'},
        {label: "Free Plan", value: 'free_plan'},
        {label: "Standard Plan", value: 'standard_plan'},
        {label: "Advanced Plan", value: 'advanced_plan'},
        {label: "Enterprise Plan", value: 'enterprise_plan'},
      ],
      allowMultiple: false
    },
    valueKey: "selected",
    replaceFilter: (value) => split(value, ','),
    appliedFilter: (value) => `LangShop Plan  is: ${split(value, ', ')}`

  },
  {
    key: 'partner_test',
    value: "",
    initialValue: "",
    badge: true,
    label: "Developers stores",
    component: ChoiceList,
    props: {
      title: "Developers stores",
      titleHidden: true,
      choices: [
        {label: "Developers stores", value: 'partner_test'},
      ],
      allowMultiple: false
    },
    valueKey: "selected",
    replaceFilter: (value) => split(value, ','),
    appliedFilter: (value) => `Developers stores`
  },
  {
    key: 'trial',
    value: "",
    initialValue: '',
    badge: true,
    label: "Trial stores",
    component: ChoiceList,
    props: {
      title: "Trial stores",
      titleHidden: true,
      choices: [
        {label: "Trial stores", value: 'trial'},
      ],
      allowMultiple: false
    },
    valueKey: "selected",
    replaceFilter: (value) => split(value, ','),
    appliedFilter: (value) => `Trial stores`

  },
  {
    key: 'coupon',
    value: "",
    initialValue: '',
    badge: true,
    label: "Shows the stores that use the coupon",
    component: ChoiceList,
    props: {
      title: "Shows the stores that use the coupon",
      titleHidden: true,
      choices: [
        {label: "Shows the stores that use the coupon", value: 'coupon'},
      ],
      allowMultiple: false
    },
    valueKey: "selected",
    replaceFilter: (value) => split(value, ','),
    appliedFilter: (value) => `Shows the stores that use the coupon`
  },
  {
    key: 'discount',
    value: "",
    initialValue: '',
    badge: true,
    label: "Shows the stores that use the discount",
    component: ChoiceList,
    props: {
      title: "Shows the stores that use the discount",
      titleHidden: true,
      choices: [
        {label: "Shows the stores that use the discount", value: 'discount'},
      ],
      allowMultiple: false
    },
    valueKey: "selected",
    replaceFilter: (value) => split(value, ','),
    appliedFilter: (value) => `Shows the stores that use the discount`
  },
  {
    key: 'demo',
    value: '',
    initialValue: '',
    badge: true,
    label: "Demo stores",
    component: ChoiceList,
    props: {
      title: "Demo stores",
      titleHidden: true,
      choices: [
        {label: "Demo stores", value: 'demo'},
      ],
      allowMultiple: false
    },
    valueKey: "selected",
    replaceFilter: (value) => split(value, ','),
    appliedFilter: (value) => `Demo stores`

  },
  {
    key: 'shopify_plan',
    value: '',
    initialValue: '',
    badge: true,
    label: "Shopify Plan",
    component: ChoiceList,
    props: {
      title: "Shopify Plan",
      titleHidden: true,
      choices: Object.keys(plans).map(key => ({
        label: plans[key],
        value: key
      })),
      allowMultiple: false
    },
    valueKey: "selected",
    replaceFilter: (value) => split(value, ','),
    appliedFilter: (value) => `Shopify Plan  is: ${value}`

  },
  {
    key: 'closed_status',
    value: '',
    initialValue: '',
    badge: true,
    label: "Closed Status",
    component: ChoiceList,
    props: {
      title: "Closed status",
      titleHidden: true,
      choices: [
        {label: "Closed", value: 'closed'},
        {label: "Open", value: 'open'},
      ],
      allowMultiple: false
    },
    valueKey: "selected",
    replaceFilter: (value) => split(value, ','),
    appliedFilter: (value) => `Store is: ${split(value, ', ')}`

  },
  {
    key: 'app_version',
    value: '',
    initialValue: '',
    badge: true,
    label: "LangShop version",
    component: ChoiceList,
    props: {
      title: "LangShop version",
      titleHidden: true,
      choices: [
        {label: "V1", value: 'v1'},
        {label: "V2", value: 'v2'},
      ],
      allowMultiple: false
    },
    valueKey: "selected",
    replaceFilter: (value) => split(value, ','),
    appliedFilter: (value) => `LangShop version: ${split(value, ', ')}`

  }
];

