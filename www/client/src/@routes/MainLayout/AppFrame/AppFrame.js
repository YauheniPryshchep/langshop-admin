import React, { useCallback, useEffect, useMemo } from "react";
import { LogOutMinor } from "@shopify/polaris-icons";
import Frame from "@common/Frame";
import { storeName } from "@utils/routes";
import { getApp } from "@store/app";
import { theme } from "@utils/theme";
import navigationItems, { footerSectionNavigation } from "./navigation";
import ToastProvider from "../../../@providers/ToastProvider";
import { useHistory } from "react-router-dom";
import { cloneDeep, debounce } from "lodash";
import { useSelector } from "react-redux";

export const AppFrame = ({
  children,
  profileScopes,
  profileName,
  profilePhoto,
  logout,
  fetchSearchResultsAction,
  resetSearchResultsAction,
  isSearched,
  isSearching,
  searchResults,
}) => {
  const history = useHistory();

  const app = useSelector(getApp);
  const selectedApp = app();

  useEffect(() => {
    resetSearchResultsAction();
  }, []);

  const userMenuActions = useMemo(() => {
    return [
      {
        items: [
          {
            content: "Logout",
            icon: LogOutMinor,
            onAction: logout,
          },
        ],
      },
    ];
  }, []);

  const navigation = useMemo(() => {
    if (!selectedApp) {
      return null;
    }

    const selectedAppId = selectedApp.id;
    const getNavigationItem = item => {
      if (item.allowed && !item.allowed(profileScopes, selectedAppId)) {
        return null;
      }

      return item;
    };

    return [
      {
        items: cloneDeep(navigationItems).reduce((items, item) => {
          if (!getNavigationItem(item)) {
            return items;
          }

          if (item.subNavigationItems) {
            item.subNavigationItems = item.subNavigationItems.reduce((subNavigationItems, subNavigationItem) => {
              if (!getNavigationItem(subNavigationItem)) {
                return subNavigationItems;
              }

              subNavigationItems.push(subNavigationItem);

              return subNavigationItems;
            }, []);

            if (typeof item.url !== "string" && item.subNavigationItems.length) {
              item.url = item.subNavigationItems[0].url;
            }
          }

          items.push(item);

          return items;
        }, []),
      },
      {
        items: cloneDeep(footerSectionNavigation).reduce((items, item) => {
          if (!getNavigationItem(item)) {
            return items;
          }

          if (item.subNavigationItems) {
            item.subNavigationItems = item.subNavigationItems.reduce((subNavigationItems, subNavigationItem) => {
              if (!getNavigationItem(subNavigationItem)) {
                return subNavigationItems;
              }

              subNavigationItems.push(subNavigationItem);

              return subNavigationItems;
            }, []);

            if (typeof item.url !== "string" && item.subNavigationItems.length) {
              item.url = item.subNavigationItems[0].url;
            }
          }

          items.push(item);

          return items;
        }, []),
      },
    ];
  }, [selectedApp, profileScopes]);

  const handleSearchChange = useCallback(
    debounce(value => {
      fetchSearchResultsAction(value);
    }, 500),
    []
  );

  const handleSearchClear = useCallback(() => {
    resetSearchResultsAction();
  }, []);

  const searchResultsItemAction = item => {
    return {
      content: item.name,
      onAction: () => history.push(`/stores/${storeName(item.name)}`),
    };
  };

  const searchResultsEmptyAction = store => {
    return {
      content: `Show rebut info about "${store}" domain`,
      helpText: "Searched store domain is not found in the stores base.",
      onAction: () => history.push(`/stores/${storeName(store)}`),
    };
  };

  return (
    <Frame
      theme={theme}
      userDetail={selectedApp ? selectedApp.title : undefined}
      userName={profileName}
      userPhoto={profilePhoto}
      navigation={navigation}
      userMenuActions={userMenuActions}
      handleSearchChange={handleSearchChange}
      handleSearchClear={handleSearchClear}
      isSearched={isSearched}
      isSearching={isSearching}
      searchResults={searchResults}
      searchResultsItemAction={searchResultsItemAction}
      searchResultsEmptyAction={searchResultsEmptyAction}
    >
      <ToastProvider>{children}</ToastProvider>
    </Frame>
  );
};
