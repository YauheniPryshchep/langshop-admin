import {
  payout,
  isLoading,
  isLoaded,
  isFetched,
  isUpdating,
  fetchPayoutAction,
  updatePayoutAction,
  resetPayoutAction,
} from "@store/partners/payout";
import { Payout } from "./Payout";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";

const mapState = createStructuredSelector({
  payout,
  isLoading,
  isLoaded,
  isFetched,
  isUpdating,
});

const mapDispatch = {
  fetchPayoutAction,
  resetPayoutAction,
  updatePayoutAction,
};

export default connect(mapState, mapDispatch)(Payout);
