import React from "react";
import { Card, Link } from "@shopify/polaris";

export const UserInformation = ({ user }) => {
  return (
    <React.Fragment>
      <Card.Section title={"Partner"}>
        <Link url={`/partners/users/${user.id}`}>{user.email}</Link>
      </Card.Section>
    </React.Fragment>
  );
};
