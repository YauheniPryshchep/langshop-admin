import React, { useCallback, useEffect, useMemo } from "react";
import { TextStyle, Page, Stack, Card, Layout } from "@shopify/polaris";
import PayoutInformation from "@components/PayoutInformation";
import PayoutStatus from "@components/PayoutStatus";
import ProgressBar from "@components/FeedbackIndicators/ProgressBar";
import PageSkeleton from "@components/Skeleton/PageSkeleton";
import { TRANSACTION_PAID_STATUS, TRANSACTION_PENDING_STATUS } from "@defaults/constants";
import { useNotification } from "@hooks/useNotification";
import { SHOW_FINANCIAL, SHOW_PARTNERS_PAYOUTS, UPDATE_PARTNERS_PAYOUTS } from "@utils/scopes";
import useHasScope from "@hooks/useHasScope";
import { UserInformation } from "./UserInformation/UserInformation";
import ManageStatusModal from "../../../../../@common/Modals/ManageStatusModal";
import useActive from "../../../../../@hooks/useActive";
import moment from "moment";
import get from "lodash/get";
import { useParams } from "react-router-dom";

export const Payout = ({
  payout,
  title,
  isLoading,
  isLoaded,
  isUpdating,
  fetchPayoutAction,
  resetPayoutAction,
  updatePayoutAction,
}) => {
  const { payoutId } = useParams();
  const hasScope = useHasScope();
  const { handleNotify } = useNotification();
  const hasShowPayoutsScope = hasScope(SHOW_PARTNERS_PAYOUTS);
  const hasUpdatePayoutsScope = hasScope(UPDATE_PARTNERS_PAYOUTS);
  const hasShowFinancialScope = hasScope(SHOW_FINANCIAL);
  const [isOpen, handleOpen, handleClose] = useActive();

  useEffect(() => {
    if (hasShowPayoutsScope && hasShowFinancialScope) {
      fetchPayoutAction(payoutId);
    }
    return () => resetPayoutAction();
  }, [payoutId, fetchPayoutAction, resetPayoutAction]);

  useEffect(() => {
    document.title = title;
  }, [title]);

  const items = useMemo(() => {
    return get(payout, "payout_transactions", []);
  }, [payout]);

  const user = useMemo(() => get(payout, "user"), [payout]);

  const status = useMemo(() => get(payout, "status", null), [payout]);

  const handlePaid = useCallback(
    async values => {
      await updatePayoutAction(payoutId, values);
      handleClose();
      handleNotify("Status successfully changed");
    },
    [handleNotify, updatePayoutAction, payoutId, handleClose]
  );

  const primaryAction = useMemo(() => {
    return {
      content: "Paid",
      disabled: status === TRANSACTION_PAID_STATUS || !hasUpdatePayoutsScope,
      loading: isLoading || isUpdating,
      onAction: handleOpen,
    };
  }, [status, isLoading, isUpdating, handleOpen]);

  if (isLoading || !isLoaded) {
    return (
      <React.Fragment>
        <PageSkeleton />
      </React.Fragment>
    );
  }

  return (
    <Page
      title={
        <Stack alignment={"center"}>
          <TextStyle>Payout for {moment(get(payout, "created_at", new Date())).format("ll")}</TextStyle>
          <PayoutStatus key={payout.id + "status"} status={get(payout, "status", null)} />
        </Stack>
      }
      primaryAction={primaryAction}
      breadcrumbs={[
        {
          content: "Payouts",
          url: "/partners/payouts",
        },
      ]}
    >
      <Layout>
        <Layout.Section>
          <Card>
            <Card.Section>
              <PayoutInformation items={items} isLoading={isLoading} />
            </Card.Section>
          </Card>
        </Layout.Section>
        <Layout.Section secondary>
          <Card>
            <Card.Section title={"Paypal"}>{get(payout, "paypal")}</Card.Section>
          </Card>
          <Card title={"User Information"}>
            <UserInformation user={user} />
          </Card>
        </Layout.Section>
        <ManageStatusModal
          open={isOpen}
          onClose={handleClose}
          loading={isLoading || isUpdating}
          handleCreate={handlePaid}
        />
      </Layout>
    </Page>
  );
};
