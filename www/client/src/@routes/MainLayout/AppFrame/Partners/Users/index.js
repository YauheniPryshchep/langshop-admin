import {
  users,
  fetchPartnersUsersAction,
  resetPartnersUsersAction,
  isFetched,
  isLoading,
  total,
} from "@store/partners/users";
import { Users } from "./Users";
import { createStructuredSelector } from "reselect";
import { connect } from "react-redux";

const mapState = createStructuredSelector({
  users,
  isFetched,
  isLoading,
  total,
});

const mapDispatch = {
  fetchPartnersUsersAction,
  resetPartnersUsersAction,
};

export default connect(mapState, mapDispatch)(Users);
