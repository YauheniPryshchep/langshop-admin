import React, { useCallback, useEffect, useMemo } from "react";
import { Avatar, Card, Filters, Page, ResourceItem, ResourceList, Stack, TextStyle } from "@shopify/polaris";

import useFetch from "@hooks/useFetch";
import useQuery from "@hooks/useQuery";
import Pagination from "@components/Pagination";
import useHasScope from "@hooks/useHasScope";
import useCancelToken from "@hooks/useCancelToken";
import { sortOptions, limitOptions } from "@defaults/options";
import { numberFormat } from "@defaults/numberFormat";
import { pageTitle } from "@defaults/pageTitle";
import { SHOW_PARTNERS_USERS } from "@utils/scopes";
import get from "lodash/get";
import moment from "moment";

export const Users = ({
  title,
  users,
  fetchPartnersUsersAction,
  resetPartnersUsersAction,
  isFetched,
  isLoading,
  total,
}) => {
  const [query, setQuery] = useQuery();
  const hasScope = useHasScope();
  const [cancelToken, cancelRequests] = useCancelToken();
  const loading = useMemo(() => !isFetched || isLoading);

  const {
    page,
    limit,
    filter,
    sort,
    search,
    pages,
    onPreviousPage,
    onNextPage,
    onLimitChange,
    onSortChange,
    onSearchChange,
    onSearchClear,
  } = useFetch({
    query,
    sortOptions,
    limitOptions,
    loading,
    total,
  });

  const fetchUsers = useCallback(() => {
    const sortOption = sortOptions.find(option => sort === option.value);
    if (hasScope(SHOW_PARTNERS_USERS)) {
      fetchPartnersUsersAction(
        {
          filter,
          limit,
          page: page,
          sort: {
            field: sortOption.field,
            direction: sortOption.direction,
          },
        },
        cancelToken
      );
    }
  }, [page, limit, sort, filter]);

  useEffect(() => {
    fetchUsers();
  }, [fetchUsers]);

  // On unmount
  useEffect(
    () => () => {
      cancelRequests();
      resetPartnersUsersAction();
    },
    []
  );

  useEffect(() => {
    if (!isFetched) {
      return;
    }

    setQuery(getRequestQuery());
  }, [isFetched, users]);

  const getRequestQuery = useCallback(() => {
    let currentPage = page;

    if (currentPage < 1) {
      currentPage = 1;
    } else if (currentPage > pages) {
      currentPage = pages;
    }

    const requestQuery = {
      page: currentPage,
      limit: limit,
      sort: sort,
    };

    if (filter) {
      requestQuery["q"] = filter;
    }

    return requestQuery;
  }, [filter, limit, sort, page, pages]);

  const pagination = useMemo(() => {
    return {
      hasPrevious: pages > 1 && page > 1,
      hasNext: pages > 1 && page < pages,
      onPrevious: onPreviousPage,
      onNext: onNextPage,
    };
  }, [pages, page, onPreviousPage, onNextPage]);

  const perPage = useMemo(() => {
    return {
      limit: limit,
      options: limitOptions,
      onChange: onLimitChange,
    };
  }, [limit, onLimitChange]);

  const filterControl = (
    <Filters
      queryValue={search}
      queryPlaceholder="Search users ..."
      filters={[]}
      appliedFilters={[]}
      onQueryChange={onSearchChange}
      onQueryClear={onSearchClear}
      onClearAll={onSearchClear}
    />
  );

  const renderItem = item => {
    const { id, email, data, created_at } = item;
    const firstName = get(data, "first_name", "");
    const lastName = get(data, "last_name", "");
    const avatar = get(data, "avatar", null);

    return (
      <ResourceItem id={id} url={`/partners/users/${id}`} media={<Avatar source={avatar} name={email} customer />}>
        <Stack distribution={"equalSpacing"}>
          <Stack.Item>
            <TextStyle variation="strong">
              {firstName} {lastName}
            </TextStyle>{" "}
            {email}
          </Stack.Item>
          <Stack.Item>{moment(created_at).format("ll")}</Stack.Item>
        </Stack>
      </ResourceItem>
    );
  };

  const resourceListMarkup = (
    <ResourceList
      showHeader
      resourceName={{
        singular: "user",
        plural: "users",
      }}
      sortOptions={sortOptions}
      sortValue={sort}
      onSortChange={onSortChange}
      totalItemsCount={numberFormat(total)}
      loading={isLoading}
      items={users}
      filterControl={filterControl}
      renderItem={renderItem}
    />
  );

  const paginationMarkup =
    users && users.length ? (
      <Card.Section>
        <Pagination pagination={pagination} perPage={perPage} />
      </Card.Section>
    ) : null;

  return (
    <Page title={pageTitle(title)} separator>
      <Card>
        {resourceListMarkup}
        {paginationMarkup}
      </Card>
    </Page>
  );
};
