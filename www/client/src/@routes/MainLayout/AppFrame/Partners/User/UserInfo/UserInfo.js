import React, { useMemo } from "react";
import { Card } from "@shopify/polaris";
import get from "lodash/get";

export const UserInfo = ({ user }) => {
  const userSettings = useMemo(() => {
    return {
      referralLink: get(user, "settings.referral_link", ""),
      paypal: get(user, "settings.paypal_account", ""),
      clientAddress: get(user, "settings.client_address", ""),
      companyName: get(user, "settings.company_name", ""),
      tax: get(user, "settings.tax", ""),
    };
  }, [user]);
  return (
    <React.Fragment>
      <Card title={"User Information"}>
        <Card.Section title={"Referral link"}>{userSettings.referralLink}</Card.Section>
        <Card.Section title={"Paypal Account"}>{userSettings.paypal}</Card.Section>
      </Card>
      <Card title={"Company Info"}>
        <Card.Section title={"Company Name"}>{userSettings.companyName}</Card.Section>
        <Card.Section title={"Client Address"}>{userSettings.clientAddress}</Card.Section>
        <Card.Section title={"TAX VAD id"}>{userSettings.tax}</Card.Section>
      </Card>
    </React.Fragment>
  );
};
