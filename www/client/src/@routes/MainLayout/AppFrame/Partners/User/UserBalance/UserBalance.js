import React, { useEffect } from "react";
import { Card } from "@shopify/polaris";
import PayoutsBalance from "@components/PayoutsBalance";
import PayoutsMoney from "@components/PayoutsMoney";

export const UserBalance = ({
  balance,
  transactions,
  commissions,
  isLoaded,
  isLoadingBalance,
  fetchBalanceStoreAction,
  resetBalanceAction,
  userId,
}) => {
  useEffect(() => {
    fetchBalanceStoreAction(userId);
    return () => {
      return resetBalanceAction();
    };
  }, [userId, fetchBalanceStoreAction, resetBalanceAction]);

  if (isLoadingBalance || !isLoaded) {
    return null;
  }

  return (
    <Card>
      <PayoutsBalance balance={balance} />
      <PayoutsMoney balance={balance} transactions={transactions} commissions={commissions} />
    </Card>
  );
};
