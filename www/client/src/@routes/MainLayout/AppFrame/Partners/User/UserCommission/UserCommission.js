import React, {useCallback, useMemo} from "react";
import {Badge, Card, DisplayText} from "@shopify/polaris";
import CommissionModal from "@common/Modals/CommissionModal";
import moment from "moment";
import map from "lodash/map";
import filter from "lodash/filter";
import orderBy from "lodash/orderBy";

const commissionStatus = {
  active: "success",
  future: "info",
  past: "warning",
};

export const UserCommission = ({
                                 userId,
                                 userCommissions,
                                 openModal,
                                 handleCloseModal,
                                 isUpdating,
                                 setNewCommissionAction,
                               }) => {
  const isFutureCommission = useCallback(commission => {
    return moment(commission.created_at).utc() > moment().utc();
  }, []);

  const isPastCommission = useCallback(
    commission => {
      if (moment(commission.created_at).utc() > moment().utc()) {
        return false;
      }

      const greaterCommission = filter(
        userCommissions,
        c =>
          moment(c.created_at).utc() > moment(commission.created_at).utc() &&
          moment(c.created_at).utc() < moment().utc()
      );

      return !!greaterCommission.length;
    },
    [userCommissions]
  );

  const filterCommissions = useMemo(() => {
    const orderedCommissions = orderBy(userCommissions, [commission => commission.created_at], ["desc"]);
    return map(orderedCommissions, commission => {
      if (isFutureCommission(commission)) {
        return {
          ...commission,
          type: "future",
        };
      }

      if (isPastCommission(commission)) {
        return {
          ...commission,
          type: "past",
        };
      }

      if (!isPastCommission(commission)) {
        return {
          ...commission,
          type: "active",
        };
      }

      return {
        ...commission,
      };
    });
  }, [userCommissions, isFutureCommission, isPastCommission]);

  const handleSetCommission = useCallback(
    async values => {
      await setNewCommissionAction(userId, values);
      handleCloseModal();
    },
    [setNewCommissionAction, userId, handleCloseModal]
  );

  return (
    <Card title={"User Commissions"}>
      {map(filterCommissions, (commission, index) => {
        return (
          <Card.Section
            key={index}
            title={
              <Badge status={commissionStatus[commission.type]}>
                Since {moment(commission.created_at).format("ll")}
              </Badge>
            }
          >
            <DisplayText size={"medium"}>{commission.commission}%</DisplayText>
          </Card.Section>
        );
      })}
      <CommissionModal
        open={openModal}
        onClose={handleCloseModal}
        handleCreate={handleSetCommission}
        loading={isUpdating}
        disableDatesBefore={filterCommissions.length ? filterCommissions[0]["created_at"] : new Date()}
      />
    </Card>
  );
};
