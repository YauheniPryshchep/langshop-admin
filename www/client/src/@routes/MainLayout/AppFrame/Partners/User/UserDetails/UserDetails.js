import React, { useMemo } from "react";
import { Card, FormLayout, Stack, TextField } from "@shopify/polaris";
import get from "lodash/get";

export const UserDetails = ({ user }) => {
  const userData = useMemo(() => {
    return {
      firstName: get(user, "data.first_name", ""),
      lastName: get(user, "data.last_name", ""),
      phone: get(user, "data.phone", ""),
      email: get(user, "email", ""),
    };
  }, [user]);

  return (
    <Card.Section>
      <FormLayout>
        <FormLayout.Group condensed>
          <TextField label={"First Name"} readOnly={true} value={userData.firstName} />
          <TextField label={"Last Name"} readOnly={true} value={userData.lastName} />
        </FormLayout.Group>
        <FormLayout.Group condensed>
          <Stack vertical={true} spacing={"extraTight"}>
            <Stack.Item>Email</Stack.Item>
            <Stack.Item>{userData.email}</Stack.Item>
          </Stack>
          <TextField label={"Phone (optional)"} readOnly={true} value={userData.phone} />
        </FormLayout.Group>
      </FormLayout>
    </Card.Section>
  );
};
