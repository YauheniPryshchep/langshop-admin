import React, { useCallback, useEffect, useMemo } from "react";
import { Card, EmptyState, Layout, Page } from "@shopify/polaris";
import PageSkeleton from "@components/Skeleton/PageSkeleton";
import useHasScope from "@hooks/useHasScope";
import { SHOW_FINANCIAL, SHOW_PARTNERS_PAYOUTS, SHOW_PARTNERS_STORES, SHOW_PARTNERS_USERS } from "@utils/scopes";
import { pageTitle } from "@defaults/pageTitle";
import AssignReferralModal from "@common/Modals/AssignReferralModal";
import { initialValues } from "@store/store-discount";
import useActive from "@hooks/useActive";
import UserDetails from "./UserDetails";
import UserInfo from "./UserInfo";
import ReferralStores from "./ReferralStores";
import Payouts from "./Payouts";
import useToggle from "../../../../../@hooks/useToggle";
import UserBalance from "./UserBalance";
import UserCommission from "./UserCommission";
import { useParams } from "react-router-dom";
import get from "lodash/get";

export const User = ({
  title,
  fetchPartnersUserAction,
  resetPartnersUserAction,
  loginToPartnersAction,
  createAssignReferral,
  user,
  isLoading,
  profile,
}) => {
  const { userId } = useParams();
  const hasScope = useHasScope();
  const hasShowScope = hasScope(SHOW_PARTNERS_USERS);
  const hasShowStoresScope = hasScope(SHOW_PARTNERS_STORES);
  const hasShowPayoutsScope = hasScope(SHOW_PARTNERS_PAYOUTS);
  const hasShowFinancialScope = hasScope(SHOW_FINANCIAL);
  const [open, openModal, closeModal] = useActive();
  const [openCommissionModal, handleOpenCommissionModal, handleCloseCommissionModal] = useActive();
  const [loading, setLoading] = useToggle();

  const commissions = useMemo(() => {
    return get(user, "commissions", []);
  }, [user]);

  useEffect(() => {
    if (hasShowScope) {
      fetchPartnersUserAction(userId);
    }
  }, [userId]);

  // On unmount
  useEffect(() => resetPartnersUserAction, [userId]);

  const primaryAction = useMemo(() => {
    return {
      content: "Login",
      onAction: () => window.open(`/api/auth/login-partners/${userId}`, "_blank"),
      disabled: !hasShowScope,
    };
  }, [loginToPartnersAction, userId, hasShowScope]);

  const handleAssign = useCallback(
    values => {
      setLoading();
      createAssignReferral(values)
        .then(() => {
          closeModal();
          setLoading();
          fetchPartnersUserAction(userId);
        })
        .catch(() => {
          closeModal();
          setLoading();
        });
    },
    [closeModal, setLoading, fetchPartnersUserAction, userId]
  );

  if (isLoading) {
    return <PageSkeleton />;
  }

  if (!user.id) {
    return (
      <EmptyState
        heading="User not found"
        action={{
          content: "Show users",
          url: "/partners/users",
        }}
        image="https://cdn.shopify.com/s/files/1/0757/9955/files/empty-state.svg"
      >
        <p>Go to users list to show all</p>
      </EmptyState>
    );
  }

  return (
    <Page
      title={user ? pageTitle(user.email) : pageTitle(title)}
      primaryAction={primaryAction}
      secondaryActions={[
        {
          content: "Assign",
          onAction: openModal,
          disabled: !hasShowStoresScope,
        },
        {
          content: "Set New Commission",
          onAction: handleOpenCommissionModal,
          disabled: !hasShowStoresScope,
        },
      ]}
      breadcrumbs={[
        {
          content: "Users",
          url: "/partners/users",
        },
      ]}
      separator
    >
      <Layout>
        <Layout.Section>
          <Card>
            <UserDetails user={user} />
          </Card>
          {!!hasShowStoresScope && <ReferralStores userId={user.id} />}
          {!!(hasShowPayoutsScope && hasShowFinancialScope) && <Payouts userId={user.id} />}
        </Layout.Section>
        <Layout.Section secondary={true}>
          <UserCommission
            openModal={openCommissionModal}
            handleCloseModal={handleCloseCommissionModal}
            userId={userId}
          />
          <UserInfo user={user} />
          <UserBalance userId={userId} commissions={commissions} />
        </Layout.Section>
      </Layout>
      <AssignReferralModal
        open={open}
        onClose={closeModal}
        loading={loading}
        handleCreate={handleAssign}
        readOnly={[]}
        initialValues={initialValues({
          user_id: userId,
          author: `${profile.id}`,
        })}
      />
    </Page>
  );
};
