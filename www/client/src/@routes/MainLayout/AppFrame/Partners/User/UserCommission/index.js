import { setNewCommissionAction, userCommissions, isUpdating } from "@store/partners/user";
import { UserCommission } from "./UserCommission";
import { createStructuredSelector } from "reselect";
import { connect } from "react-redux";

const mapState = createStructuredSelector({
  userCommissions,
  isUpdating,
});

const mapDispatch = {
  setNewCommissionAction,
};

export default connect(mapState, mapDispatch)(UserCommission);
