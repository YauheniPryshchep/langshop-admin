import React, { useCallback, useEffect, useMemo, useState } from "react";
import { Card } from "@shopify/polaris";
import useCancelToken from "@hooks/useCancelToken";
import useFetch from "@hooks/useFetch";
import ReferralHistoryTable from "@components/ReferralHistoryTable";
import Pagination from "@components/Pagination";

const limitOptions = [5, 10, 25, 100];

export const ReferralStores = ({
  userId,
  stores,
  isLoading,
  total,
  fetchPartnerUserStores,
  resetPartnerUserStoresAction,
}) => {
  const [cancelToken, cancelRequests] = useCancelToken();

  const { page, limit, pages, onLimitChange } = useFetch({
    limitOptions,
    isLoading,
    total,
  });

  const [requestData, setRequestData] = useState({
    limit,
    page,
  });

  const onPreviousPage = useCallback(() => {
    if (isLoading) {
      return;
    }

    setRequestData(state => ({
      ...state,
      page: state.page - 1 < pages ? pages : state.page - 1,
    }));
  }, [isLoading, pages, setRequestData]);

  const onNextPage = useCallback(() => {
    if (isLoading) {
      return;
    }

    setRequestData(state => ({
      ...state,
      page: state.page + 1,
    }));
  }, [isLoading, setRequestData]);

  const handleLimitChange = useCallback(
    limit => {
      if (isLoading) {
        return;
      }
      onLimitChange(limit);
      setRequestData(state => ({
        ...state,
        limit,
        page: 1,
      }));
    },
    [isLoading, requestData, onLimitChange]
  );

  useEffect(() => {
    if (userId) {
      fetchPartnerUserStores(userId, requestData, cancelToken);
    }
  }, [requestData, userId]);

  // On unmount
  useEffect(
    () => () => {
      cancelRequests();
      resetPartnerUserStoresAction();
    },
    []
  );

  const pagination = useMemo(() => {
    const { page } = requestData;
    return {
      hasPrevious: pages > 1 && page > 1,
      hasNext: pages > 1 && page < pages,
      onPrevious: onPreviousPage,
      onNext: onNextPage,
    };
  }, [pages, requestData.page, onPreviousPage, onNextPage]);

  const perPage = useMemo(() => {
    return {
      limit: limit,
      options: limitOptions,
      onChange: handleLimitChange,
    };
  }, [limit, handleLimitChange]);

  return (
    <Card title={"Referral Stores"}>
      <ReferralHistoryTable isLoading={isLoading} stores={stores} />
      <Card.Section>
        <Pagination pagination={pagination} perPage={perPage} />
      </Card.Section>
    </Card>
  );
};
