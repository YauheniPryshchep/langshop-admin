import { user, fetchPartnersUserAction, resetPartnersUserAction, isFetched, isLoading } from "@store/partners/user";
import { loginToPartnersAction } from "@store/auth";
import { profile } from "@store/profile";
import { createAssignReferral } from "@store/assign-referral";
import { User } from "./User";
import { createStructuredSelector } from "reselect";
import { connect } from "react-redux";

const mapState = createStructuredSelector({
  user,
  profile,
  isFetched,
  isLoading,
});

const mapDispatch = {
  fetchPartnersUserAction,
  resetPartnersUserAction,
  loginToPartnersAction,
  createAssignReferral,
};

export default connect(mapState, mapDispatch)(User);
