import {
  balance,
  transactions,
  fetchBalanceStoreAction,
  resetBalanceAction,
  isLoaded,
  isLoading as isLoadingBalance,
} from "@store/balance";
import { UserBalance } from "./UserBalance";
import { createStructuredSelector } from "reselect";
import { connect } from "react-redux";

const mapState = createStructuredSelector({
  balance,
  transactions,
  isLoaded,
  isLoadingBalance,
});

const mapDispatch = {
  fetchBalanceStoreAction,
  resetBalanceAction,
};

export default connect(mapState, mapDispatch)(UserBalance);
