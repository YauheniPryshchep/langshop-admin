import { payouts, fetchUserPayoutsAction, resetPayouts, isFetched, isLoading, total } from "@store/partners/payouts";
import { Payouts } from "./Payouts";
import { createStructuredSelector } from "reselect";
import { connect } from "react-redux";

const mapState = createStructuredSelector({
  payouts,
  isFetched,
  isLoading,
  total,
});

const mapDispatch = {
  fetchUserPayoutsAction,
  resetPayouts,
};

export default connect(mapState, mapDispatch)(Payouts);
