import {
  stores,
  fetchPartnerUserStores,
  resetPartnerUserStoresAction,
  isFetched,
  isLoading,
  total,
} from "@store/partners/referral-stores";
import { ReferralStores } from "./ReferralStores";
import { createStructuredSelector } from "reselect";
import { connect } from "react-redux";

const mapState = createStructuredSelector({
  stores,
  isFetched,
  isLoading,
  total,
});

const mapDispatch = {
  fetchPartnerUserStores,
  resetPartnerUserStoresAction,
};

export default connect(mapState, mapDispatch)(ReferralStores);
