import React, {useCallback, useEffect, useMemo, useState} from "react";
import {Card, Filters, Layout, Link, Page, Select} from "@shopify/polaris";
import useCancelToken from "@hooks/useCancelToken";
import useFetch from "@hooks/useFetch";
import PayoutsTimelineTable from "@components/PayoutsTimelineTable";
import {pageTitle} from "@defaults/pageTitle";
import useHasScope from "@hooks/useHasScope";
import {SHOW_PARTNERS_PAYOUTS} from "@utils/scopes";
import PayoutsBalance from "../../../../../@components/PayoutsBalance";
import PayoutsMoney from "../../../../../@components/PayoutsMoney";

const limitOptions = [5, 10, 25, 100];

export const Payouts = ({
                          fetchPayoutsAction,
                          resetPayouts,
                          payouts,
                          isLoading,
                          total,
                          title,
                          balance,
                          transactions,
                          isLoaded,
                          isLoadingBalance,
                          fetchBalanceAction,
                          resetBalanceAction,
                        }) => {
  const [cancelToken, cancelRequests] = useCancelToken();
  const hasScope = useHasScope();
  const [status, setStatus] = useState("");

  useEffect(() => {
    fetchBalanceAction();
    return () => resetBalanceAction();
  }, [fetchBalanceAction, resetBalanceAction]);

  const {page, limit, pages, onLimitChange} = useFetch({
    limitOptions,
    isLoading,
    total,
  });


  const [requestData, setRequestData] = useState({
    status,
    limit,
    page,
  });

  const handleLimitChange = useCallback(async (limit) => {
    await onLimitChange(limit);
    setRequestData(state => ({
      ...state,
      limit,
      page: 1
    }));
  }, [onLimitChange, setRequestData]);


  const handleClearFilters = useCallback(() => {
    setStatus("");
    setRequestData(state => ({
      ...state,
      status: "",
      page: 1
    }))
  }, [setStatus, setRequestData]);

  const handleChangeStatus = useCallback(
    status => {
      setStatus(status);
      setRequestData(state => ({
        ...state,
        status,
        page: 1
      }))
    },
    [setStatus, setRequestData]
  );

  const onPreviousPage = useCallback(() => {
    if (isLoading) {
      return;
    }

    let page = requestData.page - 1;
    if (page > pages) {
      page = pages;
    }

    setRequestData({
      ...requestData,
      page: page,
    });
  }, [isLoading, requestData, setRequestData]);

  const onNextPage = useCallback(() => {
    if (isLoading) {
      return;
    }

    setRequestData({
      ...requestData,
      page: requestData.page + 1,
    });
  }, [isLoading, requestData, setRequestData]);


  const handleFetchPayouts = useCallback(() => {
    if (hasScope(SHOW_PARTNERS_PAYOUTS)) {
      fetchPayoutsAction(requestData, cancelToken);
    }
  }, [requestData]);

  useEffect(() => {
    if (hasScope(SHOW_PARTNERS_PAYOUTS)) {
      handleFetchPayouts();
    }
  }, [handleFetchPayouts]);


  // On unmount
  useEffect(
    () => () => {
      cancelRequests();
      resetPayouts();
    },
    []
  );

  const pagination = useMemo(() => {
    const {page} = requestData;
    return {
      hasPrevious: pages > 1 && page > 1,
      hasNext: pages > 1 && page < pages,
      onPrevious: onPreviousPage,
      onNext: onNextPage,
    };
  }, [pages, requestData.page, onPreviousPage, onNextPage]);

  const perPage = useMemo(() => {
    return {
      limit: limit,
      options: limitOptions,
      onChange: handleLimitChange,
    };
  }, [limit, handleLimitChange]);

  const appliedFilters = useMemo(() => {
    if (!status) {
      return [];
    }
    const TITLES = {
      "1": "pending",
      "2": "paid",
      "3": "declined",
      "4": "frozen",
      "5": "money are ready for withdrawal.",
    };
    return [
      {
        key: "status",
        label: "Status is " + TITLES[status],
        onRemove: handleClearFilters,
      },
    ];
  }, [status, handleClearFilters]);

  const filters = useMemo(
    () => [
      {
        key: "status",
        label: "Payout status",
        filter: (
          <Select
            label={"Payout status"}
            options={[
              {label: "Select status", value: ""},
              {label: "Pending", value: "1"},
              {label: "Paid", value: "2"},
              {label: "Declined", value: "3"},
            ]}
            value={status}
            onChange={handleChangeStatus}
          />
        ),
      },
    ],
    [status]
  );

  const filterControl = (
    <Filters
      queryValue={""}
      queryPlaceholder="Search payouts ..."
      filters={filters}
      appliedFilters={appliedFilters}
      onQueryChange={() => false}
      onQueryClear={() => false}
      onClearAll={handleClearFilters}
    />
  );

  const payoutsBalance = useMemo(() => {
    if (isLoadingBalance) {
      return null;
    }

    return (
      <Card>
        <PayoutsBalance balance={balance}/>
        <PayoutsMoney balance={balance} transactions={transactions}/>
      </Card>
    );
  }, [balance, transactions, isLoadingBalance]);

  return (
    <Page title={pageTitle(title)}>
      <div className="PayoutsPage">
        <Layout>
          <Layout.Section>
            <Card>
              <Card.Section>{filterControl}</Card.Section>
              <PayoutsTimelineTable perPage={perPage} payouts={payouts} isLoading={isLoading} pagination={pagination}/>
            </Card>
          </Layout.Section>
          <Layout.Section secondary={true}>{payoutsBalance}</Layout.Section>
        </Layout>
      </div>
    </Page>
  );
};
