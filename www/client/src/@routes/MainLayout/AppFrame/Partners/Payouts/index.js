import { payouts, fetchPayoutsAction, resetPayouts, isFetched, isLoading, total } from "@store/partners/payouts";
import {
  balance,
  transactions,
  fetchBalanceAction,
  resetBalanceAction,
  isLoaded,
  isLoading as isLoadingBalance,
} from "@store/balance";
import { Payouts } from "./Payouts";
import { createStructuredSelector } from "reselect";
import { connect } from "react-redux";

const mapState = createStructuredSelector({
  payouts,
  balance,
  transactions,
  isLoaded,
  isLoadingBalance,
  isFetched,
  isLoading,
  total,
});

const mapDispatch = {
  fetchPayoutsAction,
  fetchBalanceAction,
  resetBalanceAction,
  resetPayouts,
};

export default connect(mapState, mapDispatch)(Payouts);
