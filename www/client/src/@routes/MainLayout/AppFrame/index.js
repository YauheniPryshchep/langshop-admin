import { profileName, profilePhoto, profileScopes } from "@store/profile";
import { logout } from "@store/auth";
import {
  fetchSearchResultsAction,
  isSearched,
  isSearching,
  resetSearchResultsAction,
  searchResults,
} from "@store/topbar-search";
import { AppFrame } from "./AppFrame";
import { createStructuredSelector } from "reselect";
import { connect } from "react-redux";

const mapState = createStructuredSelector({
  profileName,
  profilePhoto,
  profileScopes,
  isSearched,
  isSearching,
  searchResults,
});

const mapDispatch = {
  logout,
  fetchSearchResultsAction,
  resetSearchResultsAction,
};

export default connect(mapState, mapDispatch)(AppFrame);
