import {
  fetchCampaignAction,
  isFetched,
  isLoading,
  removeCampaignAction,
  resetCampaignAction,
  initialValues,
  createCampaignAction,
  updateCampaignAction,
} from "@store/campaign";
import { profile } from "@store/profile";
import { Campaign } from "./Campaign";
import { createStructuredSelector } from "reselect";
import { connect } from "react-redux";
import { reduxForm } from "redux-form";

const mapState = createStructuredSelector({
  initialValues,
  isLoading,
  isFetched,
  profile,
});

const mapDispatch = {
  fetchCampaignAction,
  createCampaignAction,
  updateCampaignAction,
  removeCampaignAction,
  resetCampaignAction,
};

export default connect(
  mapState,
  mapDispatch
)(
  reduxForm({
    form: "campaign-form",
    enableReinitialize: true,
  })(Campaign)
);
