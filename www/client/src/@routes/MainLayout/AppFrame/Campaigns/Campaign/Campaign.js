import React, { useCallback, useEffect, useMemo } from "react";
import { Card, EmptyState, FormLayout, Layout, Page, PageActions, TextContainer } from "@shopify/polaris";
import PageSkeleton from "@components/Skeleton/PageSkeleton";
import useHasScope from "@hooks/useHasScope";
import { UPDATE_CAMPAIGNS } from "@utils/scopes";
import ConfirmationModal from "@components/ConfirmationModal";
import useActive from "@hooks/useActive";
import TextFieldAdapter from "@components/TextFieldAdapter";
import SelectFieldAdapter from "@components/SelectFieldAdapter";
import CampaignAuthor from "@common/CampaignAuthor";
import CampaignTemplate from "@common/CampaignTemplate";
import CampaignPreview from "@common/CampaignPreview";
import CampaignStatus from "@common/CampaignStatus";
import CampaignTestEmail from "@common/CampaignTestEmail";
import { CampaignVariables } from "@common/CampaignVariables/CampaignVariables";
import CampaignStats from "@common/CampaignStats";
import { useNotification } from "@hooks/useNotification";
import CampaignAdvancedFormField from "@components/CampaignAdvancendFormField";
import { campaigns, fetchCampaignsAction, isFetched as isFetchedSelectors } from "@store/campaigns";
import { pageTitle } from "../../../../../@defaults/pageTitle";
import { STATUS_DONE, STATUS_DRAFT } from "../Campaigns";
import { Field, getFormValues } from "redux-form";
import { useHistory, useParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { required } from "redux-form-validators";
import { get } from "lodash";
import moment from "moment";

const validate = {
  title: [
    required({
      message: "Campaign title is required",
    }),
  ],
  subject: [
    required({
      message: "Campaign subject is required",
    }),
  ],
  description: [
    required({
      message: "Campaign description is required",
    }),
  ],
  type: [
    required({
      message: "Campaign type is required",
    }),
  ],
  template: [
    required({
      message: "Campaign template is required",
    }),
  ],
};

export const Campaign = ({
  handleSubmit,
  fetchCampaignAction,
  createCampaignAction,
  removeCampaignAction,
  updateCampaignAction,
  resetCampaignAction,
  isLoading,
  isFetched,
  profile,
  form,
  dirty,
  invalid,
}) => {
  const history = useHistory();
  const hasScope = useHasScope();
  const hasUpdateScope = hasScope(UPDATE_CAMPAIGNS);
  const { campaignId } = useParams();
  const [removeModalOpened, openRemoveModal, closeRemoveModal] = useActive();
  const [templateModalOpened, openTemplateModal, closeTemplateModal] = useActive();
  const { handleNotify } = useNotification();
  const dispatch = useDispatch();
  const campaignsSelector = useSelector(campaigns);
  const isFetchedSelector = useSelector(isFetchedSelectors);

  useEffect(() => {
    if (!campaignsSelector.length && !isFetchedSelector) {
      dispatch(fetchCampaignsAction());
    }
  }, [campaignsSelector, isFetchedSelector]);

  const loading = useMemo(() => {
    if (isLoading) {
      return true;
    }

    if (!campaignId) {
      return false;
    }

    return !isFetched;
  }, [campaignId, isLoading, isFetched]);

  const campaign = useSelector(getFormValues(form));

  const isDisabled = useMemo(() => ![STATUS_DRAFT].includes(campaign.status), [campaign.status]);

  // On loading by route params
  useEffect(() => {
    if (!campaignId) {
      return;
    }

    fetchCampaignAction(campaignId);
  }, [campaignId]);

  // On new created
  useEffect(() => {
    if (campaignId || !campaign.id) {
      return;
    }

    history.replace(`/marketing/campaigns/${campaign.id}`);
  }, [campaignId, campaign.id]);

  // On unmount
  useEffect(() => resetCampaignAction, [campaignId]);

  const handleSave = useCallback(
    async data => {
      if (campaignId) {
        await updateCampaignAction(campaignId, data);
        handleNotify("Campaign successfully updated");
      } else {
        await createCampaignAction({
          ...data,
          start_at: moment(data.start_at).utc().format("YYYY-MM-DD HH:mm:ss"),
          author_id: profile.id,
        });

        handleNotify("Campaign successfully created");
      }
    },
    [campaignId, campaign.id]
  );

  const handleRemove = useCallback(async () => {
    await removeCampaignAction(campaignId);
    handleNotify("Campaign successfully deleted");
    history.replace("/marketing/campaigns");
  }, [campaignId]);

  const primaryAction = useMemo(() => {
    if (!hasUpdateScope) {
      return null;
    }

    return {
      content: "Save",
      onAction: handleSubmit(handleSave),
      disabled: !dirty || loading || invalid || isDisabled,
    };
  }, [handleSubmit, handleSave, dirty, loading, hasUpdateScope, invalid, isDisabled]);

  const secondaryActions = useMemo(() => {
    if (!campaignId || !hasUpdateScope) {
      return [];
    }

    return [
      {
        content: "Remove",
        destructive: true,
        onAction: openRemoveModal,
        disabled: loading,
      },
    ];
  }, [loading, campaignId, hasUpdateScope, openRemoveModal]);

  const headerSecondaryActions = useMemo(() => {
    return [
      {
        content: "Send test email",
        onAction: openTemplateModal,
      },
    ];
  }, [isDisabled]);

  if (loading) {
    return <PageSkeleton />;
  }

  if (campaignId && !campaign.id) {
    return (
      <EmptyState
        heading="Campaign not found"
        action={{
          content: "Show campaigns",
          url: "/marketing/campaigns",
        }}
        image="https://cdn.shopify.com/s/files/1/0757/9955/files/empty-state.svg"
      >
        <p>Go to campaigns list to show all</p>
      </EmptyState>
    );
  }

  return (
    <Page
      title={campaign.title ? pageTitle(`Edit Campaign ${campaign.title}`) : pageTitle("New Campaign")}
      primaryAction={primaryAction}
      secondaryActions={headerSecondaryActions}
      breadcrumbs={[
        {
          content: "Campaigns",
          url: "/marketing/campaigns",
        },
      ]}
      separator
    >
      <Layout>
        <Layout.Section>
          <Card sectioned>
            <TextContainer>
              <Field
                component={TextFieldAdapter}
                name="title"
                label="Title"
                type="text"
                readOnly={isDisabled}
                validate={validate.title}
              />
              <Field
                component={TextFieldAdapter}
                name="subject"
                label="Subject"
                type="text"
                readOnly={isDisabled}
                validate={validate.subject}
              />
              <Field
                component={TextFieldAdapter}
                name="description"
                label="Description"
                type="text"
                readOnly={isDisabled}
                validate={validate.description}
                multiline={4}
              />
            </TextContainer>
          </Card>
          <CampaignAdvancedFormField />

          <Card sectioned>
            <FormLayout>
              <Field
                component={SelectFieldAdapter}
                name="type"
                label="Campaign type"
                disabled={isDisabled}
                options={[
                  {
                    label: "Email",
                    value: "email",
                  },
                ]}
                validate={validate.type}
              />
              <CampaignTemplate
                isDisabled={isDisabled}
                mailgunDomain={get(profile, "settings.mailgunDomain", "")}
                template={campaign.template}
                validate={validate}
              />
            </FormLayout>
          </Card>
          <CampaignPreview
            filters={campaign.recipients_filter}
            campaignId={campaignId}
            isDisabled={isDisabled}
            domains={get(campaign, "campaign_domains", [])}
          />
        </Layout.Section>
        <Layout.Section secondary={true}>
          <CampaignStatus
            start_at={campaign.start_at}
            status={campaign.status}
            validate={validate}
            campaignId={campaignId}
            isDisabled={isDisabled}
          />
          <Card>
            <Card.Section title={"Author"}>
              <CampaignAuthor author_id={campaign.author_id} />
            </Card.Section>
          </Card>
          <CampaignVariables />
          {[STATUS_DONE].includes(campaign.status) && (
            <CampaignStats status={campaign.status} campaignId={campaignId} profile={profile} />
          )}
        </Layout.Section>
        <Layout.Section>
          <PageActions primaryAction={primaryAction} secondaryActions={secondaryActions} />
        </Layout.Section>
      </Layout>
      <CampaignTestEmail
        template={campaign.template}
        open={templateModalOpened}
        handleClose={closeTemplateModal}
        isDisabled={false}
        onConfirm={closeTemplateModal}
      />
      <ConfirmationModal
        destructive
        title="Remove campaign"
        content="Are you sure you want to remove campaign?"
        confirm="Remove"
        open={removeModalOpened}
        onCancel={closeRemoveModal}
        onConfirm={handleRemove}
      />
    </Page>
  );
};
