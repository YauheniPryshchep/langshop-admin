import { campaigns, fetchCampaignsAction, resetCampaignsAction, isFetched, isLoading, total } from "@store/campaigns";
import { removeCampaignAction } from "@store/campaign";
import { Campaigns } from "./Campaigns";
import { createStructuredSelector } from "reselect";
import { connect } from "react-redux";

const mapState = createStructuredSelector({
  campaigns,
  isFetched,
  isLoading,
  total,
});

const mapDispatch = {
  fetchCampaignsAction,
  resetCampaignsAction,
  removeCampaignAction,
};

export default connect(mapState, mapDispatch)(Campaigns);
