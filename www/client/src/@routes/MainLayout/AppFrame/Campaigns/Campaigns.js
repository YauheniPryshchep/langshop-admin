import React, {useCallback, useEffect, useMemo, useState} from "react";
import {Avatar, Badge, Card, Filters, Page, ResourceItem, ResourceList, Stack, TextStyle} from "@shopify/polaris";
import useFetch from "@hooks/useFetch";
import useQuery from "@hooks/useQuery";
import Pagination from "@components/Pagination";
import useCancelToken from "@hooks/useCancelToken";
import {limitOptions, sortOptions} from "@defaults/options";
import {UPDATE_CAMPAIGNS} from "@utils/scopes";
import useHasScope from "@hooks/useHasScope";
import useActive from "@hooks/useActive";
import useQueue from "@hooks/useQueue";
import ConfirmationModal from "@components/ConfirmationModal";
import {numberFormat} from "@defaults/numberFormat";
import {pageTitle} from "@defaults/pageTitle";

export const STATUS_DRAFT = 1;
export const STATUS_CANCELED = 2;
export const STATUS_READY_TO_START = 3;
export const STATUS_COLLECT_STORES = 4;
export const STATUS_WAITING_TO_COLLECT_EMAILS = 5;
export const STATUS_COLLECT_EMAILS = 6;
export const STATUS_WAITING_TO_PREPARE_EMAIL_VALIDATES = 7;
export const STATUS_PREPARE_EMAIL_VALIDATES = 8;
export const STATUS_WAITING_TO_VALIDATE_EMAILS = 9;
export const STATUS_VALIDATE_EMAILS = 10;
export const STATUS_WAITING_TO_CREATE_MAILLIST = 11;
export const STATUS_CREATE_MAILLIST = 12;
export const STATUS_WAITING_TO_UPLOAD_MEMBERS = 13;
export const STATUS_UPLOAD_MEMBERS = 14;
export const STATUS_WAITING_TO_CHECK_MAILLIST = 15;
export const STATUS_CHECK_MAILLIST = 16;
export const STATUS_WAITING_TO_SENDING_EMAIL = 17;
export const STATUS_SENDING_EMAIL = 18;
export const STATUS_WAITING_TO_COMPLETE = 19;
export const STATUS_DONE = 20;

export const CAMPAIGNS_STATUS = {
  [STATUS_DRAFT]: "Draft",
  [STATUS_CANCELED]: "Canceled",
  [STATUS_READY_TO_START]: "Waiting for the start",
  [STATUS_COLLECT_STORES]: "Collect campaign store",
  [STATUS_WAITING_TO_COLLECT_EMAILS]: "Waiting to collect emails",
  [STATUS_COLLECT_EMAILS]: "Collect campaign email",
  [STATUS_WAITING_TO_VALIDATE_EMAILS]: "Waiting for validate emails",
  [STATUS_VALIDATE_EMAILS]: "Validate emails",
  [STATUS_WAITING_TO_CREATE_MAILLIST]: "Waiting to create mail list",
  [STATUS_CREATE_MAILLIST]: "Create mail list",
  [STATUS_WAITING_TO_UPLOAD_MEMBERS]: "Waiting to upload members",
  [STATUS_UPLOAD_MEMBERS]: "Upload members to mail list",
  [STATUS_WAITING_TO_CHECK_MAILLIST]: "Waiting to check mail list",
  [STATUS_CHECK_MAILLIST]: "Check mail list",
  [STATUS_WAITING_TO_SENDING_EMAIL]: "Waiting to sending email",
  [STATUS_SENDING_EMAIL]: "Sending email",
  [STATUS_WAITING_TO_COMPLETE]: "Waiting to complete",
  [STATUS_DONE]: "Complete",
  [STATUS_WAITING_TO_PREPARE_EMAIL_VALIDATES]: "Waiting to prepare email validates",
  [STATUS_PREPARE_EMAIL_VALIDATES]: "Prepare email validates",
};

export const Campaigns = ({
                            title,
                            campaigns,
                            fetchCampaignsAction,
                            resetCampaignsAction,
                            isFetched,
                            isLoading,
                            total,
                            removeCampaignAction,
                          }) => {
  const [query, setQuery] = useQuery();
  const [cancelToken, cancelRequests] = useCancelToken();
  const loading = useMemo(() => !isFetched || isLoading);
  const hasScope = useHasScope();
  const [isQueueing, inQueue] = useQueue(5);
  const [selectedCompanies, setSelectedCompanies] = useState([]);
  const [removeModalOpened, openRemoveModal, closeRemoveModal] = useActive();

  const removeCompany = useCallback(async () => {
    closeRemoveModal();
    if (!selectedCompanies.length) {
      return false;
    }
    await inQueue(selectedCompanies.map(company => () => removeCampaignAction(company)));

    setSelectedCompanies([]);
    fetchCampaigns();
  }, [selectedCompanies, closeRemoveModal, setSelectedCompanies]);

  const {
    page,
    limit,
    filter,
    sort,
    search,
    pages,
    onPreviousPage,
    onNextPage,
    onLimitChange,
    onSortChange,
    onSearchChange,
    onSearchClear,
  } = useFetch({
    query,
    sortOptions,
    limitOptions,
    loading,
    total,
  });

  const fetchCampaigns = useCallback(() => {
    const sortOption = sortOptions.find(option => sort === option.value);

    fetchCampaignsAction(
      {
        filter,
        limit,
        page: page,
        sort: {
          field: sortOption.field,
          direction: sortOption.direction,
        },
      },
      cancelToken
    );
  }, [page, limit, sort, filter]);

  useEffect(() => {
    fetchCampaigns();
  }, [fetchCampaigns]);

  // On unmount
  useEffect(
    () => () => {
      cancelRequests();
      resetCampaignsAction();
    },
    []
  );

  useEffect(() => {
    if (!isFetched) {
      return;
    }

    setQuery(getRequestQuery());
  }, [isFetched, campaigns]);

  const getRequestQuery = useCallback(() => {
    let currentPage = page;

    if (currentPage < 1) {
      currentPage = 1;
    } else if (currentPage > pages) {
      currentPage = pages;
    }

    const requestQuery = {
      page: currentPage,
      limit: limit,
      sort: sort,
    };

    if (filter) {
      requestQuery["q"] = filter;
    }

    return requestQuery;
  }, [filter, limit, sort, page, pages]);

  const pagination = useMemo(() => {
    return {
      hasPrevious: pages > 1 && page > 1,
      hasNext: pages > 1 && page < pages,
      onPrevious: onPreviousPage,
      onNext: onNextPage,
    };
  }, [pages, page, onPreviousPage, onNextPage]);

  const perPage = useMemo(() => {
    return {
      limit: limit,
      options: limitOptions,
      onChange: onLimitChange,
    };
  }, [limit, onLimitChange]);

  const filterControl = (
    <Filters
      queryValue={search}
      queryPlaceholder="Search campaigns ..."
      filters={[]}
      appliedFilters={[]}
      onQueryChange={onSearchChange}
      onQueryClear={onSearchClear}
      onClearAll={onSearchClear}
    />
  );

  const getBadgeType = useCallback(status => {
    if (status === STATUS_CANCELED) {
      return {
        status: "warning",
      };
    }

    if (status === STATUS_DONE) {
      return {
        status: "success",
      };
    }

    return {
      status: "info",
    };
  }, []);

  const renderItem = item => {
    const {
      id,
      title,
      status,
      author: {photo, name},
    } = item;

    return (
      <ResourceItem id={id} url={`/marketing/campaigns/${id}`} media={<Avatar source={photo} name={name}/>}>
        <Stack distribution={"equalSpacing"}>
          <Stack.Item>
            <TextStyle variation="strong">{title}</TextStyle>
          </Stack.Item>
          <Stack.Item>
            <TextStyle variation="strong">
              {<Badge {...getBadgeType(status)}>{CAMPAIGNS_STATUS[status]}</Badge>}
            </TextStyle>
          </Stack.Item>
        </Stack>
      </ResourceItem>
    );
  };

  const resourceListMarkup = (
    <ResourceList
      showHeader
      resourceName={{
        singular: "campaign",
        plural: "campaigns",
      }}
      sortOptions={sortOptions}
      sortValue={sort}
      onSortChange={onSortChange}
      totalItemsCount={numberFormat(total)}
      loading={isLoading || isQueueing}
      items={campaigns}
      filterControl={filterControl}
      renderItem={renderItem}
      onSelectionChange={setSelectedCompanies}
      selectedItems={selectedCompanies}
      idForItem={item => item.id}
      bulkActions={[
        {
          content: "Remove",
          onAction: openRemoveModal,
        },
      ]}
    />
  );

  const paginationMarkup =
    campaigns && campaigns.length ? (
      <Card.Section>
        <Pagination pagination={pagination} perPage={perPage}/>
      </Card.Section>
    ) : null;

  return (
    <Page
      title={pageTitle(title)}
      primaryAction={
        hasScope(UPDATE_CAMPAIGNS)
          ? {
            content: "Add new",
            url: `/marketing/campaigns/new`,
          }
          : null
      }
      separator
    >
      <Card>
        {resourceListMarkup}
        {paginationMarkup}
      </Card>
      <ConfirmationModal
        destructive
        title="Remove selected companies"
        content="Are you sure you want to delete the selected companies?"
        confirm="Remove"
        open={removeModalOpened}
        onCancel={closeRemoveModal}
        onConfirm={removeCompany}
      />
    </Page>
  );
};
