import {
  coupons,
  fetchCouponsAction,
  resetCouponsAction,
  isFetched,
  isLoading,
  total,
  page,
  pages
} from "@store/coupons";
import {removeCouponAction} from "@store/coupon";
import {Coupons} from "./Coupons";
import {createStructuredSelector} from "reselect";
import {connect} from "react-redux";

const mapState = createStructuredSelector({
  coupons,
  isFetched,
  isLoading,
  total,
  page,
  pages
});

const mapDispatch = {
  fetchCouponsAction,
  resetCouponsAction,
  removeCouponAction,
};

export default connect(mapState, mapDispatch)(Coupons);
