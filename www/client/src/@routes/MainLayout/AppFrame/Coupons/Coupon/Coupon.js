import React, {useCallback, useEffect, useMemo} from "react";
import {Card, Layout, Page, PageActions, Stack, TextContainer} from "@shopify/polaris";
import PageSkeleton from "@components/Skeleton/PageSkeleton";
import {useNotification} from "@hooks/useNotification";
import useActive from "@hooks/useActive";
import {UPDATE_COUPONS} from "@utils/scopes";
import useHasScope from "@hooks/useHasScope";
import TextFieldAdapter from "@components/TextFieldAdapter";
import SelectFieldAdapter from "@components/SelectFieldAdapter";
import ConfirmationModal from "@components/ConfirmationModal";
import {CouponStatus} from "@common/CouponStatus/CouponStatus";
import {CouponActivity} from "@common/CouponActivity/CouponActivity";
import DatePicker from "@components/DatePicker";
import CouponUsage from "@common/CouponUsage";
import PlansAdapter from "@components/PlansAdapter";
import {CheckboxFieldAdapter} from "../../../../../@components/CheckboxFieldAdapter/CheckboxFieldAdapter";
import {pageTitle} from "../../../../../@defaults/pageTitle";
import {Field, getFormValues} from "redux-form";
import {useSelector} from "react-redux";
import {numericality, required} from "redux-form-validators";
import {useHistory, useParams} from "react-router-dom";
import {isNull, isEmpty} from "lodash";
import moment from "moment";

const validate = {
  name: [
    required({
      message: "Coupon title is required",
    }),
  ],
  type: [
    required({
      message: "Discount type is required",
    }),
  ],
  value: [
    required({
      message: "Discount value is required",
    }),
    numericality({
      greaterThan: 0,
      lessThan: 100,
      message: "Discount value must be greater than 0 and less then 100",
    }),
  ],
  usage_limit: [
    required({
      message: "Usage limit is required",
    }),
    numericality({
      greaterThan: 0,
    }),
  ]
};

export const Coupon = ({
                         handleSubmit,
                         fetchCouponAction,
                         createCouponAction,
                         updateCouponAction,
                         removeCouponAction,
                         resetCouponAction,
                         isLoading,
                         isFetched,
                         form,
                         dirty,
                         invalid,
                       }) => {
  const history = useHistory();
  const hasScope = useHasScope();
  const hasUpdateScope = hasScope(UPDATE_COUPONS);
  const {couponId} = useParams();
  const [removeModalOpened, openRemoveModal, closeRemoveModal] = useActive();
  const {handleNotify} = useNotification();

  const loading = useMemo(() => {
    if (isLoading) {
      return true;
    }

    if (!couponId) {
      return false;
    }

    return !isFetched;
  }, [couponId, isLoading, isFetched]);

  const coupon = useSelector(getFormValues(form));

  // On loading by route params
  useEffect(() => {
    if (!couponId) {
      return;
    }

    fetchCouponAction(couponId);
  }, [couponId]);

  // On new created
  useEffect(() => {
    if (couponId || !coupon.id) {
      return;
    }

    history.replace(`/marketing/coupons/${coupon.id}`);
  }, [couponId, coupon.id]);

  // On unmount
  useEffect(() => resetCouponAction, [couponId]);

  const handleSave = useCallback(
    async data => {
      if (couponId) {
        await updateCouponAction(couponId, {
          ...data,
          end_at: moment(data.end_at).utc().format("YYYY-MM-DD HH:mm:ss"),
          start_at: moment(data.start_at).utc().format("YYYY-MM-DD HH:mm:ss"),
        });
        handleNotify("Coupon successfully updated");
      } else {
        await createCouponAction({
          ...data,
        });

        handleNotify("Coupon successfully created");
      }
    },
    [couponId, coupon.id]
  );

  const handleRemove = useCallback(async () => {
    await removeCouponAction(couponId);
    handleNotify("Coupon successfully deleted");
    history.replace("/marketing/coupons");
  }, [couponId]);

  const primaryAction = useMemo(() => {
    if (!hasUpdateScope) {
      return null;
    }

    return {
      content: "Save",
      onAction: handleSubmit(handleSave),
      disabled: !dirty || loading || invalid,
    };
  }, [handleSubmit, handleSave, dirty, loading, hasUpdateScope, invalid]);

  const secondaryActions = useMemo(() => {
    if (!couponId || !hasUpdateScope) {
      return [];
    }

    return [
      {
        content: "Remove",
        destructive: true,
        onAction: openRemoveModal,
        disabled: loading,
      },
    ];
  }, [loading, couponId, hasUpdateScope, openRemoveModal]);

  if (loading) {
    return <PageSkeleton/>;
  }

  return (
    <Page
      title={coupon.name ? pageTitle(`Edit coupon ${coupon.name}`) : pageTitle("New Coupon")}
      primaryAction={primaryAction}
      breadcrumbs={[
        {
          content: "Coupon",
          url: "/marketing/coupons",
        },
      ]}
      separator
    >
      <Layout>
        <Layout.Section>
          <Card sectioned>
            <TextContainer>
              <Field component={TextFieldAdapter} name="name" label="Name" type="text" validate={validate.name}/>
              <Field
                component={SelectFieldAdapter}
                name="discount_type"
                label="Discount Type"
                type="text"
                options={[
                  {
                    label: "Fixed",
                    value: "1",
                  },
                  {
                    label: "Percent",
                    value: "2",
                  },
                ]}
                validate={validate.type}
                format={(value) => value.toString()}
              />
              <Field
                component={TextFieldAdapter}
                name="discount_value"
                label="Discount value"
                type="number"
                validate={validate.value}
                format={value => value.toString()}
                parse={value => parseInt(value)}
              />
              <Field
                component={TextFieldAdapter}
                name="usage_limit"
                label="Usage limit"
                type="number"
                validate={validate.usage_limit}
                format={value => value.toString()}
                parse={value => parseInt(value)}
              />
              <Field component={CheckboxFieldAdapter} name="reusable" label="Reusable coupon"/>
              <Field
                component={PlansAdapter}
                name="plans"
                label="Plans"
                format={value => (isNull(value) ? [] : value)}
                parse={value => (isEmpty(value) ? null : value)}
              />
            </TextContainer>
          </Card>
          {couponId && (
            <Card>
              <CouponUsage couponId={couponId}/>
            </Card>
          )}
        </Layout.Section>
        <Layout.Section secondary={true}>
          <Card>
            <Card.Section>
              <Stack>
                <CouponStatus status={coupon.status}/>
                <CouponActivity status={coupon.status} couponId={couponId} updateCouponAction={updateCouponAction}/>
              </Stack>
            </Card.Section>
            <Card.Section>
              <Field
                component={DatePicker}
                disableDatesBefore={moment().toDate()}
                isDisabled={loading}
                title="Coupon start"
                fieldLabel="Start at"
                defaultLabel="Immediately"
                defaultValue="immediately"
                name="start_at"
                isShowDefault={false}
                selected="Date"
              />
            </Card.Section>
            <Card.Section>
              <Field
                component={DatePicker}
                disableDatesBefore={moment(coupon.start_at).toDate()}
                isDisabled={loading}

                title="Coupon end"
                fieldLabel="End at"
                defaultLabel="Never"
                defaultValue="never"
                name="end_at"
                selected="Date"
              />
            </Card.Section>
          </Card>
        </Layout.Section>
        <Layout.Section>
          <PageActions primaryAction={primaryAction} secondaryActions={secondaryActions}/>
        </Layout.Section>
      </Layout>
      <ConfirmationModal
        destructive
        title="Remove coupon"
        content="Are you sure you want to remove coupon?"
        confirm="Remove"
        open={removeModalOpened}
        onCancel={closeRemoveModal}
        onConfirm={handleRemove}
      />
    </Page>
  );
};
