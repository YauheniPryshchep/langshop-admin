import {
  fetchCouponAction,
  isFetched,
  isLoading,
  removeCouponAction,
  resetCouponAction,
  initialValues,
  createCouponAction,
  updateCouponAction,
} from "@store/coupon";
import { Coupon } from "./Coupon";
import { createStructuredSelector } from "reselect";
import { connect } from "react-redux";
import { reduxForm } from "redux-form";

const mapState = createStructuredSelector({
  initialValues,
  isLoading,
  isFetched,
});

const mapDispatch = {
  fetchCouponAction,
  createCouponAction,
  updateCouponAction,
  removeCouponAction,
  resetCouponAction,
};

export default connect(
  mapState,
  mapDispatch
)(
  reduxForm({
    form: "coupon-form",
    enableReinitialize: true,
  })(Coupon)
);
