export const initialFilters = [
  {
    key: "search_term",
    value: "",
    initialValue: "",
    badge: false,
  },
  {
    key: "sort",
    value: 'created-desc',
    initialValue: "created-desc",
    badge: false,
  },
  {
    key: "page",
    value: 1,
    initialValue: 1,
    badge: false
  },
  {
    key: "limit",
    value: "25",
    initialValue: "25",
    badge: false
  },
];

