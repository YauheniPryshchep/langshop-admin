import React, {useCallback, useEffect, useMemo, useState} from "react";
import {Badge, Card, Page, ResourceItem, ResourceList, Stack, TextStyle} from "@shopify/polaris";
import Pagination from "@components/Pagination";
import {UPDATE_COUPONS} from "@utils/scopes";
import useHasScope from "@hooks/useHasScope";
import useActive from "@hooks/useActive";
import useQueue from "@hooks/useQueue";
import ConfirmationModal from "@components/ConfirmationModal";
import {numberFormat} from "@defaults/numberFormat";
import {pageTitle} from "@defaults/pageTitle";
import useQueryString from "@hooks/useQueryString";
import useFilters from "@hooks/useFilters";
import {mergeFilters} from "@utils/mergeFilters";
import useNavigation from "@hooks/useNavigation";
import ResourceFilter from "@components/ResourceFilter";
import {initialFilters} from "../Coupons/constants";
import get from "lodash/get";
import find from "lodash/find";

export const DRAFT_STATUS = 1;
export const ACTIVE_STATUS = 2;
export const BLOCKED_STATUS = 3;

export const COUPONS_STATUS = {
  [DRAFT_STATUS]: "Draft",
  [ACTIVE_STATUS]: "Active",
  [BLOCKED_STATUS]: "Blocked",
};

const limitOptions = ["5", "10", "25", "100"];

const sortDefault = "last-event-desc";

const sortOptions = [
  {
    label: "Created (Newest)",
    value: "created-desc",
    field: "last_event.timestamp",
    direction: "desc",
  },
  {
    label: "Created (Oldest)",
    value: "created-asc",
    field: "last_event.timestamp",
    direction: "asc",
  },
  {
    label: "Alphabetically (A-Z)",
    value: "name-asc",
    field: "name",
    direction: "asc",
  },
  {
    label: "Alphabetically (Z-A)",
    value: "name-desc",
    field: "name",
    direction: "desc",
  },
];
export const Coupons = ({
                          title,
                          coupons,
                          fetchCouponsAction,
                          resetCouponsAction,
                          isLoading,
                          total,
                          page,
                          pages,
                          removeCouponAction,
                        }) => {

  const hasScope = useHasScope();
  const [isQueueing, inQueue] = useQueue(5);
  const [selectedCompanies, setSelectedCompanies] = useState([]);
  const [removeModalOpened, openRemoveModal, closeRemoveModal] = useActive();

  const [query, setQuery, convertFilters] = useQueryString();
  const [filters, handleChangeFilters, onClearAll] = useFilters(mergeFilters(initialFilters, query), isLoading);

  const limit = useMemo(() => {
    return get(find(filters, option => option.key === 'limit'), 'value') || 25
  }, [filters]);

  const [pagination, isHasPagination, perPage] = useNavigation(page, pages, handleChangeFilters, limit, limitOptions);

  const sort = useMemo(() => {
    return get(find(filters, option => option.key === 'sort'), 'value') || sortDefault
  }, [filters]);


  const handleFetchData = useCallback(() => {
    setQuery(convertFilters(filters));
    fetchCouponsAction(convertFilters(filters));
  }, [filters, setQuery, convertFilters, fetchCouponsAction]);

  const handleChangeSort = useCallback((value) => {
    handleChangeFilters({
      key: "sort",
      value
    })
  }, [handleChangeFilters]);


  const removeCoupon = useCallback(async () => {
    closeRemoveModal();
    if (!selectedCompanies.length) {
      return false;
    }
    await inQueue(selectedCompanies.map(coupon => () => removeCouponAction(coupon)));

    setSelectedCompanies([]);
    handleFetchData();
  }, [selectedCompanies, closeRemoveModal, setSelectedCompanies, handleFetchData]);

  useEffect(() => {
    handleFetchData();
    return () => resetCouponsAction();
  }, [handleFetchData, resetCouponsAction]);

  const getBadgeType = useCallback(status => {
    if (status === DRAFT_STATUS) {
      return {
        status: "info",
      };
    }

    if (status === BLOCKED_STATUS) {
      return {
        status: "warning",
      };
    }

    return {
      status: "success",
    };
  }, []);

  const renderItem = item => {
    const {id, name, status} = item;

    return (
      <ResourceItem id={id} url={`/marketing/coupons/${id}`}>
        <Stack distribution={"equalSpacing"}>
          <Stack.Item>
            <TextStyle variation="strong">{name}</TextStyle>
          </Stack.Item>
          <Stack.Item>
            <TextStyle variation="strong">
              {<Badge {...getBadgeType(status)}>{COUPONS_STATUS[status]}</Badge>}
            </TextStyle>
          </Stack.Item>
        </Stack>
      </ResourceItem>
    );
  };

  const resourceListMarkup = (
    <ResourceList
      showHeader
      resourceName={{
        singular: "coupon",
        plural: "coupons",
      }}
      sortOptions={sortOptions}
      sortValue={sort}
      onSortChange={handleChangeSort}
      totalItemsCount={numberFormat(total)}
      loading={isLoading || isQueueing}
      items={coupons}
      filterControl={<ResourceFilter
        options={filters}
        queryPlaceholder={"Search stores..."}
        handleChange={handleChangeFilters}
        onClearAll={onClearAll}
        skipKeys={['search_term', 'sort', "page", "limit"]}
      />}
      renderItem={renderItem}
      onSelectionChange={setSelectedCompanies}
      selectedItems={selectedCompanies}
      idForItem={item => item.id}
      bulkActions={[
        {
          content: "Remove",
          onAction: openRemoveModal,
        },
      ]}
    />
  );
  const paginationMarkup =
    coupons && coupons.length ? (
      <Card.Section>
        <Pagination pagination={pagination} perPage={perPage}/>
      </Card.Section>
    ) : null;

  return (
    <Page
      title={pageTitle(title)}
      primaryAction={
        hasScope(UPDATE_COUPONS)
          ? {
            content: "Add new",
            url: `/marketing/coupons/new`,
          }
          : null
      }
      separator
    >
      <Card>
        {resourceListMarkup}
        {paginationMarkup}
      </Card>
      <ConfirmationModal
        destructive
        title="Remove selected coupons"
        content="Are you sure you want to delete the selected coupons?"
        confirm="Remove"
        open={removeModalOpened}
        onCancel={closeRemoveModal}
        onConfirm={removeCoupon}
      />
    </Page>
  );
};
