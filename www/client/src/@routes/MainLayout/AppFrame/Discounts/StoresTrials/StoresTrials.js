import React, { useCallback, useEffect, useMemo, useState } from "react";
import { Card, Filters, Page, ResourceItem, ResourceList, Stack, TextStyle } from "@shopify/polaris";

import useQuery from "@hooks/useQuery";
import useFetch from "@hooks/useFetch";
import Pagination from "@components/Pagination";
import useActive from "@hooks/useActive";
import useQueue from "@hooks/useQueue";
import ConfirmationModal from "@components/ConfirmationModal";
import CreateStoreTrialModal from "@common/Modals/CreateStoreTrialModal";
import { storeName } from "@utils/routes";
import useCancelToken from "@hooks/useCancelToken";
import { sortOptions, limitOptions } from "@defaults/options";
import useHasScope from "@hooks/useHasScope";
import { UPDATE_STORES_TRIALS } from "@utils/scopes";
import { numberFormat } from "../../../../../@defaults/numberFormat";
import { pageTitle } from "../../../../../@defaults/pageTitle";

export const StoresTrials = ({
  title,
  fetchStoresTrialsAction,
  createStoreTrialAction,
  removeStoreTrialAction,
  resetStoresTrialsAction,
  storesTrials,
  total,
  isFetched,
  isLoading,
}) => {
  const [query, setQuery] = useQuery();
  const hasScope = useHasScope();

  const [selectedStores, setSelectedStores] = useState([]);
  const [createModalOpened, openCreateModal, closeCreateModal] = useActive();
  const [removeModalOpened, openRemoveModal, closeRemoveModal] = useActive();

  const [isQueueing, inQueue] = useQueue(5);
  const [cancelToken, cancelRequests] = useCancelToken();

  const loading = useMemo(() => isLoading || isQueueing || !isFetched, [isLoading, isQueueing, isFetched]);

  const {
    page,
    limit,
    filter,
    sort,
    search,
    pages,
    onPreviousPage,
    onNextPage,
    onLimitChange,
    onSortChange,
    onSearchChange,
    onSearchClear,
  } = useFetch({
    query,
    sortOptions,
    limitOptions,
    loading,
    total,
  });

  const fetchStoresTrials = useCallback(() => {
    const sortOption = sortOptions.find(option => sort === option.value);

    return fetchStoresTrialsAction(
      {
        filter,
        limit,
        page: page,
        sort: {
          field: sortOption.field,
          direction: sortOption.direction,
        },
      },
      cancelToken
    );
  }, [page, limit, sort, filter]);

  const createStoreTrial = useCallback(
    async store => {
      try {
        await createStoreTrialAction(store);
      } catch (e) {
        openCreateModal();
        return;
      }

      fetchStoresTrials();
      closeCreateModal();
    },
    [openCreateModal, closeCreateModal]
  );

  const removeStoresTrials = useCallback(async () => {
    closeRemoveModal();

    if (!selectedStores.length) {
      return;
    }
    await inQueue(selectedStores.map(store => () => removeStoreTrialAction(store)));

    setSelectedStores([]);
    fetchStoresTrials();
  }, [selectedStores, closeRemoveModal, setSelectedStores]);

  useEffect(() => {
    fetchStoresTrials();
  }, [fetchStoresTrials]);

  // On unmount
  useEffect(
    () => () => {
      cancelRequests();
      resetStoresTrialsAction();
    },
    []
  );

  useEffect(() => {
    if (!isFetched) {
      return;
    }

    setQuery(getRequestQuery());
  }, [isFetched, storesTrials]);

  const getRequestQuery = useCallback(() => {
    let currentPage = page;

    if (currentPage < 1) {
      currentPage = 1;
    } else if (currentPage > pages) {
      currentPage = pages;
    }

    const requestQuery = {
      page: currentPage,
      limit: limit,
      sort: sort,
    };

    if (filter) {
      requestQuery["q"] = filter;
    }

    return requestQuery;
  }, [filter, limit, sort, page, pages]);

  const pagination = useMemo(() => {
    return {
      hasPrevious: pages > 1 && page > 1,
      hasNext: pages > 1 && page < pages,
      onPrevious: onPreviousPage,
      onNext: onNextPage,
    };
  }, [pages, page, onPreviousPage, onNextPage]);

  const perPage = useMemo(() => {
    return {
      limit: limit,
      options: limitOptions,
      onChange: onLimitChange,
    };
  }, [limit, onLimitChange]);

  const filterControl = (
    <Filters
      queryValue={search}
      queryPlaceholder="Search stores ..."
      filters={[]}
      appliedFilters={[]}
      onQueryChange={onSearchChange}
      onQueryClear={onSearchClear}
      onClearAll={onSearchClear}
    />
  );

  const renderItem = item => {
    const { name, value } = item;

    return (
      <ResourceItem id={name} url={`/stores/${storeName(name)}`}>
        <Stack distribution="equalSpacing">
          <Stack.Item>
            <TextStyle variation="strong">{name}</TextStyle>
          </Stack.Item>
          <Stack.Item>{value} days</Stack.Item>
        </Stack>
      </ResourceItem>
    );
  };

  const resourceListMarkup = (
    <ResourceList
      showHeader
      resourceName={{
        singular: "store",
        plural: "stores",
      }}
      sortOptions={sortOptions}
      sortValue={sort}
      onSortChange={onSortChange}
      totalItemsCount={numberFormat(total)}
      loading={loading}
      items={storesTrials}
      filterControl={filterControl}
      renderItem={renderItem}
      bulkActions={[
        {
          content: "Remove",
          onAction: openRemoveModal,
        },
      ]}
      onSelectionChange={setSelectedStores}
      selectedItems={selectedStores}
      idForItem={item => item.name}
    />
  );

  const paginationMarkup =
    storesTrials && storesTrials.length ? (
      <Card.Section>
        <Pagination pagination={pagination} perPage={perPage} />
      </Card.Section>
    ) : null;

  return (
    <Page
      title={pageTitle(title)}
      primaryAction={
        hasScope(UPDATE_STORES_TRIALS)
          ? {
              content: "Add new",
              disabled: loading,
              onAction: openCreateModal,
            }
          : null
      }
      separator
    >
      <Card>
        {resourceListMarkup}
        {paginationMarkup}
      </Card>

      <CreateStoreTrialModal
        open={createModalOpened}
        onClose={closeCreateModal}
        loading={loading}
        handleCreate={createStoreTrial}
      />

      <ConfirmationModal
        destructive
        title="Remove selected stores trials"
        content="Are you sure you want to delete the selected stores trials?"
        confirm="Remove"
        open={removeModalOpened}
        onCancel={closeRemoveModal}
        onConfirm={removeStoresTrials}
      />
    </Page>
  );
};
