import {
  storesTrials,
  fetchStoresTrialsAction,
  createStoreTrialAction,
  removeStoreTrialAction,
  resetStoresTrialsAction,
  isFetched,
  isLoading,
  total,
} from "@store/stores-trials";
import { StoresTrials } from "./StoresTrials";

import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";

const mapState = createStructuredSelector({
  storesTrials,
  isFetched,
  isLoading,
  total,
});

const mapDispatch = {
  fetchStoresTrialsAction,
  createStoreTrialAction,
  removeStoreTrialAction,
  resetStoresTrialsAction,
};

export default connect(mapState, mapDispatch)(StoresTrials);
