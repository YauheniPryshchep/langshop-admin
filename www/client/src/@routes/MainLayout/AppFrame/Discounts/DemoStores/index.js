import {
  fetchDemoStoresAction,
  createDemoStoreAction,
  removeDemoStoreAction,
  resetDemoStoresAction,
  demoStores,
  isFetched,
  isLoading,
  total,
} from "@store/demo-stores";
import { DemoStores } from "./DemoStores";
import { createStructuredSelector } from "reselect";
import { connect } from "react-redux";

const mapState = createStructuredSelector({
  demoStores,
  isFetched,
  isLoading,
  total,
});

const mapDispatch = {
  fetchDemoStoresAction,
  createDemoStoreAction,
  removeDemoStoreAction,
  resetDemoStoresAction,
};

export default connect(mapState, mapDispatch)(DemoStores);
