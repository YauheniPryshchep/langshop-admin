import React, { useCallback, useEffect, useMemo, useState } from "react";
import { Card, Filters, Page, ResourceItem, ResourceList, TextStyle } from "@shopify/polaris";

import Pagination from "@components/Pagination";
import useQuery from "@hooks/useQuery";
import useFetch from "@hooks/useFetch";
import useActive from "@hooks/useActive";
import CreateDemoStoreModal from "@common/Modals/CreateDemoStoreModal";
import ConfirmationModal from "@components/ConfirmationModal";
import useQueue from "@hooks/useQueue";
import { storeName } from "@utils/routes";
import useCancelToken from "@hooks/useCancelToken";
import { sortOptions, limitOptions } from "@defaults/options";
import { UPDATE_DEMO_STORES } from "../../../../../@utils/scopes";
import useHasScope from "../../../../../@hooks/useHasScope";
import { numberFormat } from "../../../../../@defaults/numberFormat";
import { pageTitle } from "../../../../../@defaults/pageTitle";

export const DemoStores = ({
  title,
  fetchDemoStoresAction,
  createDemoStoreAction,
  removeDemoStoreAction,
  resetDemoStoresAction,
  demoStores,
  total,
  isFetched,
  isLoading,
}) => {
  const [query, setQuery] = useQuery();

  const [selectedStores, setSelectedStores] = useState([]);
  const [createModalOpened, openCreateModal, closeCreateModal] = useActive();
  const [removeModalOpened, openRemoveModal, closeRemoveModal] = useActive();
  const hasScope = useHasScope();

  const [isQueueing, inQueue] = useQueue(5);
  const [cancelToken, cancelRequests] = useCancelToken();

  const loading = useMemo(() => isLoading || isQueueing || !isFetched, [isLoading, isQueueing, isFetched]);

  const {
    page,
    limit,
    filter,
    sort,
    search,
    pages,
    onPreviousPage,
    onNextPage,
    onLimitChange,
    onSortChange,
    onSearchChange,
    onSearchClear,
  } = useFetch({
    query,
    sortOptions,
    limitOptions,
    loading,
    total,
  });

  const fetchDemoStores = useCallback(() => {
    const sortOption = sortOptions.find(option => sort === option.value);

    return fetchDemoStoresAction(
      {
        filter,
        limit,
        page: page,
        sort: {
          field: sortOption.field,
          direction: sortOption.direction,
        },
      },
      cancelToken
    );
  }, [page, limit, sort, filter]);

  const createDemoStore = useCallback(
    async store => {
      try {
        await createDemoStoreAction(store);
      } catch (e) {
        openCreateModal();
        return;
      }

      fetchDemoStores();
      closeCreateModal();
    },
    [openCreateModal, closeCreateModal]
  );

  const removeDemoStores = useCallback(async () => {
    closeRemoveModal();

    if (!selectedStores.length) {
      return;
    }

    await inQueue(selectedStores.map(store => () => removeDemoStoreAction(store)));

    setSelectedStores([]);
    fetchDemoStores();
  }, [selectedStores, closeRemoveModal, setSelectedStores]);

  useEffect(() => {
    fetchDemoStores();
  }, [fetchDemoStores]);

  // On unmount
  useEffect(
    () => () => {
      cancelRequests();
      resetDemoStoresAction();
    },
    []
  );

  useEffect(() => {
    if (!isFetched) {
      return;
    }

    setQuery(getRequestQuery());
  }, [isFetched, demoStores]);

  const getRequestQuery = useCallback(() => {
    let currentPage = page;

    if (currentPage < 1) {
      currentPage = 1;
    } else if (currentPage > pages) {
      currentPage = pages;
    }

    const requestQuery = {
      page: currentPage,
      limit: limit,
      sort: sort,
    };

    if (filter) {
      requestQuery["q"] = filter;
    }

    return requestQuery;
  }, [filter, limit, sort, page, pages]);

  const pagination = useMemo(() => {
    return {
      hasPrevious: pages > 1 && page > 1,
      hasNext: pages > 1 && page < pages,
      onPrevious: onPreviousPage,
      onNext: onNextPage,
    };
  }, [pages, page, onPreviousPage, onNextPage]);

  const perPage = useMemo(() => {
    return {
      limit: limit,
      options: limitOptions,
      onChange: onLimitChange,
    };
  }, [limit, onLimitChange]);

  const filterControl = (
    <Filters
      queryValue={search}
      queryPlaceholder="Search stores ..."
      filters={[]}
      appliedFilters={[]}
      onQueryChange={onSearchChange}
      onQueryClear={onSearchClear}
      onClearAll={onSearchClear}
    />
  );

  const renderItem = item => {
    const { name } = item;

    return (
      <ResourceItem id={name} url={`/stores/${storeName(name)}`}>
        <h3>
          <TextStyle variation="strong">{name}</TextStyle>
        </h3>
      </ResourceItem>
    );
  };

  const resourceListMarkup = (
    <ResourceList
      showHeader
      resourceName={{
        singular: "store",
        plural: "stores",
      }}
      sortOptions={sortOptions}
      sortValue={sort}
      onSortChange={onSortChange}
      totalItemsCount={numberFormat(total)}
      loading={loading}
      items={demoStores}
      filterControl={filterControl}
      renderItem={renderItem}
      bulkActions={[
        {
          content: "Remove",
          onAction: openRemoveModal,
        },
      ]}
      onSelectionChange={setSelectedStores}
      selectedItems={selectedStores}
      idForItem={item => item.name}
    />
  );

  const paginationMarkup =
    demoStores && demoStores.length ? (
      <Card.Section>
        <Pagination pagination={pagination} perPage={perPage} />
      </Card.Section>
    ) : null;

  return (
    <Page
      title={pageTitle(title)}
      primaryAction={
        hasScope(UPDATE_DEMO_STORES)
          ? {
              content: "Add new",
              disabled: loading,
              onAction: openCreateModal,
            }
          : null
      }
      separator
    >
      <Card>
        {resourceListMarkup}
        {paginationMarkup}
      </Card>

      <CreateDemoStoreModal
        open={createModalOpened}
        onClose={closeCreateModal}
        loading={loading}
        handleCreate={createDemoStore}
      />

      <ConfirmationModal
        destructive
        title="Remove selected stores"
        content="Are you sure you want to delete the selected stores?"
        confirm="Remove"
        open={removeModalOpened}
        onCancel={closeRemoveModal}
        onConfirm={removeDemoStores}
      />
    </Page>
  );
};
