import React, { useCallback, useEffect, useMemo, useState } from "react";
import { Card, Filters, Page, ResourceItem, ResourceList, Stack, TextStyle } from "@shopify/polaris";

import useQuery from "@hooks/useQuery";
import useFetch from "@hooks/useFetch";
import Pagination from "@components/Pagination";
import { storeName } from "@utils/routes";
import useActive from "@hooks/useActive";
import useQueue from "@hooks/useQueue";
import ConfirmationModal from "@components/ConfirmationModal";
import CreateStoreDiscountModal from "@common/Modals/CreateStoreDiscountModal";
import useCancelToken from "@hooks/useCancelToken";
import { sortOptions, limitOptions } from "@defaults/options";
import { UPDATE_STORES_DISCOUNTS } from "@utils/scopes";
import useHasScope from "@hooks/useHasScope";
import { numberFormat } from "../../../../../@defaults/numberFormat";
import { pageTitle } from "../../../../../@defaults/pageTitle";

export const StoresDiscounts = ({
  title,
  fetchStoresDiscountsAction,
  createStoreDiscountAction,
  removeStoreDiscountAction,
  resetStoresDiscountsAction,
  storesDiscounts,
  total,
  isFetched,
  isLoading,
}) => {
  const [query, setQuery] = useQuery();
  const hasScope = useHasScope();

  const [selectedStores, setSelectedStores] = useState([]);
  const [createModalOpened, openCreateModal, closeCreateModal] = useActive();
  const [removeModalOpened, openRemoveModal, closeRemoveModal] = useActive();

  const [isQueueing, inQueue] = useQueue(5);
  const [cancelToken, cancelRequests] = useCancelToken();

  const loading = useMemo(() => isLoading || isQueueing || !isFetched, [isLoading, isQueueing, isFetched]);

  const {
    page,
    limit,
    filter,
    sort,
    search,
    pages,
    onPreviousPage,
    onNextPage,
    onLimitChange,
    onSortChange,
    onSearchChange,
    onSearchClear,
  } = useFetch({
    query,
    sortOptions,
    limitOptions,
    loading,
    total,
  });

  const fetchStoresDiscounts = useCallback(() => {
    const sortOption = sortOptions.find(option => sort === option.value);

    return fetchStoresDiscountsAction(
      {
        filter,
        limit,
        page: page,
        sort: {
          field: sortOption.field,
          direction: sortOption.direction,
        },
      },
      cancelToken
    );
  }, [page, limit, sort, filter]);

  const createStoreDiscount = useCallback(
    async store => {
      try {
        await createStoreDiscountAction(store);
      } catch (e) {
        openCreateModal();
        return;
      }

      fetchStoresDiscounts();
      closeCreateModal();
    },
    [openCreateModal, closeCreateModal]
  );

  const removeStoresDiscounts = useCallback(async () => {
    closeRemoveModal();

    if (!selectedStores.length) {
      return;
    }

    await inQueue(selectedStores.map(store => () => removeStoreDiscountAction(store)));

    setSelectedStores([]);
    fetchStoresDiscounts();
  }, [selectedStores, closeRemoveModal, setSelectedStores]);

  useEffect(() => {
    fetchStoresDiscounts();
  }, [fetchStoresDiscounts]);

  // On unmount
  useEffect(
    () => () => {
      cancelRequests();
      resetStoresDiscountsAction();
    },
    []
  );

  useEffect(() => {
    if (!isFetched) {
      return;
    }

    setQuery(getRequestQuery());
  }, [isFetched, storesDiscounts]);

  const getRequestQuery = useCallback(() => {
    let currentPage = page;

    if (currentPage < 1) {
      currentPage = 1;
    } else if (currentPage > pages) {
      currentPage = pages;
    }

    const requestQuery = {
      page: currentPage,
      limit: limit,
      sort: sort,
    };

    if (filter) {
      requestQuery["q"] = filter;
    }

    return requestQuery;
  }, [filter, limit, sort, page, pages]);

  const pagination = useMemo(() => {
    return {
      hasPrevious: pages > 1 && page > 1,
      hasNext: pages > 1 && page < pages,
      onPrevious: onPreviousPage,
      onNext: onNextPage,
    };
  }, [pages, page, onPreviousPage, onNextPage]);

  const perPage = useMemo(() => {
    return {
      limit: limit,
      options: limitOptions,
      onChange: onLimitChange,
    };
  }, [limit, onLimitChange]);

  const filterControl = (
    <Filters
      queryValue={search}
      queryPlaceholder="Search stores ..."
      filters={[]}
      appliedFilters={[]}
      onQueryChange={onSearchChange}
      onQueryClear={onSearchClear}
      onClearAll={onSearchClear}
    />
  );

  const renderItem = item => {
    const { name, value, type } = item;

    return (
      <ResourceItem id={name} url={`/stores/${storeName(name)}`}>
        <Stack distribution={"equalSpacing"}>
          <Stack.Item>
            <TextStyle variation="strong">{name}</TextStyle>
          </Stack.Item>
          <Stack.Item>
            <Stack.Item>{type === "percent" ? `${value}%` : `${value}$`}</Stack.Item>
          </Stack.Item>
        </Stack>
      </ResourceItem>
    );
  };

  const resourceListMarkup = (
    <ResourceList
      showHeader
      resourceName={{
        singular: "store",
        plural: "stores",
      }}
      sortOptions={sortOptions}
      sortValue={sort}
      onSortChange={onSortChange}
      totalItemsCount={numberFormat(total)}
      loading={loading}
      items={storesDiscounts}
      filterControl={filterControl}
      renderItem={renderItem}
      bulkActions={[
        {
          content: "Remove",
          onAction: openRemoveModal,
        },
      ]}
      onSelectionChange={setSelectedStores}
      selectedItems={selectedStores}
      idForItem={item => item.name}
    />
  );

  const paginationMarkup =
    storesDiscounts && storesDiscounts.length ? (
      <Card.Section>
        <Pagination pagination={pagination} perPage={perPage} />
      </Card.Section>
    ) : null;

  return (
    <Page
      title={pageTitle(title)}
      primaryAction={
        hasScope(UPDATE_STORES_DISCOUNTS)
          ? {
              content: "Add new",
              disabled: loading,
              onAction: openCreateModal,
            }
          : null
      }
      separator
    >
      <Card>
        {resourceListMarkup}
        {paginationMarkup}
      </Card>

      <CreateStoreDiscountModal
        open={createModalOpened}
        onClose={closeCreateModal}
        loading={loading}
        handleCreate={createStoreDiscount}
      />

      <ConfirmationModal
        destructive
        title="Remove selected stores discounts"
        content="Are you sure you want to delete the selected stores discounts?"
        confirm="Remove"
        open={removeModalOpened}
        onCancel={closeRemoveModal}
        onConfirm={removeStoresDiscounts}
      />
    </Page>
  );
};
