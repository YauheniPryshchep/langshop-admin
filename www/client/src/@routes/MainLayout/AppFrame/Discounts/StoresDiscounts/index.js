import {
  fetchStoresDiscountsAction,
  createStoreDiscountAction,
  removeStoreDiscountAction,
  resetStoresDiscountsAction,
  storesDiscounts,
  isFetched,
  isLoading,
  total,
} from "@store/stores-discounts";
import { StoresDiscounts } from "./StoresDiscounts";
import { createStructuredSelector } from "reselect";
import { connect } from "react-redux";

const mapState = createStructuredSelector({
  storesDiscounts,
  isFetched,
  isLoading,
  total,
});

const mapDispatch = {
  fetchStoresDiscountsAction,
  createStoreDiscountAction,
  removeStoreDiscountAction,
  resetStoresDiscountsAction,
};

export default connect(mapState, mapDispatch)(StoresDiscounts);
