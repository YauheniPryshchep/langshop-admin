import React, { useMemo } from "react";
import { Card, Icon, Page } from "@shopify/polaris";
import { miscNavigationItems } from "../navigation";
import { pageTitle } from "../../../../@defaults/pageTitle";
import { useHistory } from "react-router-dom";

export const Misc = () => {
  const history = useHistory();

  const settingsItems = useMemo(() => {
    return miscNavigationItems.map((item, index) => {
      return (
        <li key={index} className="area-settings-nav__item" onClick={() => history.push(item.url)}>
          <div className="area-settings-nav__media">{<Icon source={item.icon} />}</div>
          <div>
            <p className="area-settings-nav__title">{item.label}</p>
            <p className="area-settings-nav__description">{item.description}</p>
          </div>
        </li>
      );
    });
  }, [miscNavigationItems]);

  return (
    <Page title={pageTitle("Misc")}>
      <Card sectioned={true}>
        <ul className="area-settings-nav">{settingsItems}</ul>
      </Card>
    </Page>
  );
};
