import { fetchCustomerAction, resetCustomerAction, isFetched, isLoading, initialValues } from "@store/customer";
import { Customer } from "./Customer";
import { createStructuredSelector } from "reselect";
import { connect } from "react-redux";

const mapState = createStructuredSelector({
  initialValues,
  isLoading,
  isFetched,
});

const mapDispatch = {
  fetchCustomerAction,
  resetCustomerAction,
};

export default connect(mapState, mapDispatch)(Customer);
