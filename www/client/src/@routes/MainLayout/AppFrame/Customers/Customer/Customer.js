import React, { useCallback, useEffect, useMemo } from "react";
import { Badge, Card, DataTable, Layout, Link, Page, TextStyle } from "@shopify/polaris";
import PageSkeleton from "@components/Skeleton/PageSkeleton";
import { pageTitle } from "@defaults/pageTitle";
import { storeName } from "@utils/routes";
import CustomerInfoCard from "@common/CustomerInfoCard";
import { CustomerCampaigns } from "../../../../../@common/CustomerCampaigns/CustomerCampaigns";
import { EmptyTableState } from "../../../../../@components/FeedbackIndicators/EmptyTableState/EmptyTableState";
import { useParams } from "react-router-dom";
import get from "lodash/get";
import { Col } from "react-flexbox-grid";

export const Customer = ({ initialValues, isLoading, isFetched, fetchCustomerAction, resetCustomerAction }) => {
  const { customerId } = useParams();

  const loading = useMemo(() => {
    if (isLoading) {
      return true;
    }

    if (!customerId) {
      return false;
    }

    return !isFetched;
  }, [customerId, isLoading, isFetched]);

  const customer = useMemo(() => ({ ...initialValues }), [initialValues]);

  const stores = useMemo(() => get(customer, "stores", []), [customer]);

  const status = useCallback(owner => {
    return owner ? <Badge status={"success"}>Owner</Badge> : <Badge status={"info"}>Collaborator</Badge>;
  }, []);

  const rows = useMemo(() => {
    return stores.map(store => {
      const {
        name,
        pivot: { account_owner },
        id,
      } = store;
      return [
        <Col key={`name-${id}`}>
          <Link url={`/stores/${storeName(name)}`}>
            <TextStyle variation="strong">{name}</TextStyle>
          </Link>
        </Col>,
        <Col key={`status-${id}`}>{status(account_owner)}</Col>,
      ];
    });
  }, [stores]);

  const customerStores = useMemo(() => {
    if (!rows.length) {
      return <EmptyTableState title={"No stores found"} description={"Customer dont have any stores"} />;
    }
    return <DataTable columnContentTypes={["text", "text"]} headings={["Name", "Role"]} rows={rows} />;
  }, [rows]);

  // On loading by route params
  useEffect(() => {
    fetchCustomerAction(customerId);
    return () => resetCustomerAction();
  }, [customerId]);

  if (loading) {
    return <PageSkeleton />;
  }

  return (
    <Page
      title={pageTitle(`${customer.first_name} ${customer.last_name}`)}
      breadcrumbs={[
        {
          content: "Customers",
          url: "/customers",
        },
      ]}
      separator
    >
      <Layout>
        <Layout.Section>
          <Card>{customerStores}</Card>
          <CustomerCampaigns campaigns={customer.campaigns} />
        </Layout.Section>
        <Layout.Section secondary>
          <CustomerInfoCard customer={customer} />
        </Layout.Section>
      </Layout>
    </Page>
  );
};
