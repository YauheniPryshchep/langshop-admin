import React, {useCallback, useEffect, useMemo} from "react";
import {Avatar, Card, Page, ResourceItem, ResourceList, Stack, TextStyle} from "@shopify/polaris";
import {limitOptions} from "@defaults/options";
import {numberFormat} from "@defaults/numberFormat";
import Pagination from "@components/Pagination";
import {pageTitle} from "@defaults/pageTitle";
import {gravatarEmail} from "@defaults/gravatarEmail";
import useQueryString from "@hooks/useQueryString";
import useFilters from "@hooks/useFilters";
import {mergeFilters} from "@utils/mergeFilters";
import useNavigation from "@hooks/useNavigation";
import ResourceFilter from "@components/ResourceFilter";
import get from "lodash/get";
import find from "lodash/find";

const sortOptions = [
  {
    label: "Alphabetically (A-Z)",
    value: "first-name-asc",
    field: "first-name-asc",
    direction: "asc",
  },
  {
    label: "Alphabetically (Z-A)",
    value: "first-name-desc",
    field: "first-name-desc",
    direction: "desc",
  },
];

const initialFilters = [
  {
    key: "search_term",
    value: "",
    initialValue: "",
    badge: false,
  },
  {
    key: "sort",
    value: 'first-name-asc',
    initialValue: "first-name-asc",
    badge: false,
  },
  {
    key: "page",
    value: 1,
    initialValue: 1,
    badge: false
  },
  {
    key: "limit",
    value: "25",
    initialValue: "25",
    badge: false
  },
];

export const Customers = ({
                            title,
                            customers,
                            isLoading,
                            total,
                            page,
                            pages,
                            fetchCustomersAction,
                            resetCustomersAction,
                          }) => {

  const [query, setQuery, convertFilters] = useQueryString();
  const [filters, handleChangeFilters, onClearAll] = useFilters(mergeFilters(initialFilters, query), isLoading);

  const limit = useMemo(() => {
    return get(find(filters, option => option.key === 'limit'), 'value') || 25
  }, [filters]);

  const [pagination, isHasPagination, perPage] = useNavigation(page, pages, handleChangeFilters, limit, limitOptions);

  const sort = useMemo(() => {
    return get(find(filters, option => option.key === 'sort'), 'value') || 'first-name-asc'
  }, [filters]);


  const handleFetchData = useCallback(() => {
    setQuery(convertFilters(filters));
    fetchCustomersAction(convertFilters(filters));
  }, [filters, setQuery, convertFilters, fetchCustomersAction]);

  const handleChangeSort = useCallback((value) => {
    handleChangeFilters({
      key: "sort",
      value
    })
  }, [handleChangeFilters]);

  useEffect(() => {
    handleFetchData();
    return () => resetCustomersAction();
  }, [handleFetchData, resetCustomersAction]);

  const renderItem = item => {
    const {id, first_name, last_name, email} = item;

    return (
      <ResourceItem id={id} url={`/customers/${id}`} media={<Avatar source={gravatarEmail(email)}/>}>
        <Stack distribution={"equalSpacing"} alignment={"center"}>
          <Stack.Item>
            <TextStyle variation="strong">
              {first_name} {last_name}
            </TextStyle>
          </Stack.Item>
        </Stack>
      </ResourceItem>
    );
  };

  const resourceListMarkup = (
    <ResourceList
      showHeader
      resourceName={{
        singular: "customer",
        plural: "customers",
      }}
      sortOptions={sortOptions}
      sortValue={sort}
      onSortChange={handleChangeSort}
      totalItemsCount={numberFormat(total)}
      loading={isLoading}
      items={customers}
      filterControl={<ResourceFilter
        options={filters}
        queryPlaceholder={"Search customers..."}
        handleChange={handleChangeFilters}
        onClearAll={onClearAll}
        skipKeys={['search_term', 'sort', "page", "limit"]}
      />}
      renderItem={renderItem}
      idForItem={item => item.id}
    />
  );
  const paginationMarkup = useMemo(() => {
    return <Card.Section>
      <Pagination pagination={isHasPagination ? pagination : null} perPage={perPage}/>
    </Card.Section>
  }, [isHasPagination, pagination, perPage]);

  return (
    <Page title={pageTitle(title)} separator>
      <Card>
        {resourceListMarkup}
        {paginationMarkup}
      </Card>
    </Page>
  );
};
