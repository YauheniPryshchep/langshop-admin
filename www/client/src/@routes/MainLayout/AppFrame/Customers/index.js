import {
  customers,
  fetchCustomersAction,
  resetCustomersAction,
  isFetched,
  isLoading,
  total,
  page,
  pages
} from "@store/customers";
import {removeCampaignAction} from "@store/campaign";
import {Customers} from "./Customers";
import {createStructuredSelector} from "reselect";
import {connect} from "react-redux";

const mapState = createStructuredSelector({
  customers,
  isFetched,
  isLoading,
  total,
  page,
  pages
});

const mapDispatch = {
  fetchCustomersAction,
  resetCustomersAction,
  removeCampaignAction,
};

export default connect(mapState, mapDispatch)(Customers);
