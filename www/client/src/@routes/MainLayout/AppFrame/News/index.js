import { fetchNewsAction, removeNewsItemAction, resetNewsAction, news, isFetched, isLoading, total } from "@store/news";
import { News } from "./News";
import { createStructuredSelector } from "reselect";
import { connect } from "react-redux";

const mapState = createStructuredSelector({
  news,
  isFetched,
  isLoading,
  total,
});

const mapDispatch = {
  fetchNewsAction,
  removeNewsItemAction,
  resetNewsAction,
};

export default connect(mapState, mapDispatch)(News);
