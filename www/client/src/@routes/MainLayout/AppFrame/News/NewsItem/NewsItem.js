import React, { useCallback, useEffect, useMemo } from "react";
import {
  Button,
  Card,
  EmptyState,
  FormLayout,
  Layout,
  Page,
  PageActions,
  SkeletonBodyText,
  SkeletonThumbnail,
  Stack,
  TextContainer,
} from "@shopify/polaris";
import { ExternalSmallMinor } from "@shopify/polaris-icons";
import PageSkeleton from "@components/Skeleton/PageSkeleton";
import useHasScope from "@hooks/useHasScope";
import { UPDATE_NEWS } from "@utils/scopes";
import TextFieldAdapter from "@components/TextFieldAdapter";
import EditorFieldAdapter from "@components/EditorFieldAdapter";
import ImageFieldAdapter from "@components/ImageFieldAdapter";
import TabsFieldAdapter from "@components/TabsFieldAdapter";
import { ComponentsFieldAdapter } from "@components/ComponentsFieldAdapter/ComponentsFieldAdapter";
import { ToggleFieldAdapter } from "@components/ToggleFieldAdapter/ToggleFieldAdapter";
import { getVersion } from "@store/app";
import NewsPreview from "@common/NewsPreview";
import { backHistory } from "@store/history-listener";
import { Field, getFormValues } from "redux-form";
import { useHistory, useParams } from "react-router-dom";
import { useSelector } from "react-redux";
import { required, url } from "redux-form-validators";
import "./styles.scss";
import ConfirmationModal from "@components/ConfirmationModal";
import useActive from "@hooks/useActive";
import { pageTitle } from "@defaults/pageTitle";

const validate = {
  title: [
    required({
      message: "News item title is required",
    }),
  ],
};

const primaryLink = (value, allValues) => {
  if (allValues["primaryText"]) {
    if (!value) {
      return "News Primary Link is required";
    }
    return undefined;
  }

  return undefined;
};

const primaryLinkUrl = (value, allValues) => {
  if (allValues["primaryText"]) {
    return url({ message: "Primary link is not valid URL" })(value);
  }

  return undefined;
};

const secondaryLinkUrl = (value, allValues) => {
  if (allValues["secondaryText"]) {
    return url({ message: "Secondary link is not valid URL" })(value);
  }

  return undefined;
};

const secondaryLink = (value, allValues) => {
  if (allValues["secondaryText"]) {
    if (!value) {
      return "News Secondary Link is required";
    }
    return undefined;
  }
  return undefined;
};

const normalize = {
  video: value => {
    if (!value) {
      return value;
    }

    const videoIDParts = value.match(
      /^(?:https?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?vi?=|(?:embed|v|vi|user)\/))([^?&"'>]+)/
    );
    if (videoIDParts && videoIDParts[1]) {
      return videoIDParts[1];
    }

    return value.replace(/[^a-zA-Z-_0-9]/g, "");
  },
};

export const NewsItem = ({
  handleSubmit,
  isFetched,
  isLoading,
  fetchNewsItemAction,
  removeNewsItemAction,
  resetNewsItemAction,
  createNewsItemAction,
  updateNewsItemAction,
  form,
  dirty,
  invalid,
  change,
}) => {
  const history = useHistory();
  const hasScope = useHasScope();
  const hasUpdateScope = hasScope(UPDATE_NEWS);
  const { appVersion, itemId } = useParams();
  const selectedAppVersion = useSelector(getVersion)(appVersion);
  const [removeModalOpened, openRemoveModal, closeRemoveModal] = useActive();

  const backButton = useSelector(backHistory)(["/news"], {
    content: "News",
    url: `/${selectedAppVersion.version}/news`,
  });

  const loading = useMemo(() => {
    if (isLoading) {
      return true;
    }

    if (!itemId) {
      return false;
    }

    return !isFetched;
  }, [itemId, isLoading, isFetched]);

  const newsItem = useSelector(getFormValues(form));

  // On loading by route params
  useEffect(() => {
    if (!itemId) {
      return;
    }

    fetchNewsItemAction(itemId);
  }, [itemId]);

  // On new created
  useEffect(() => {
    if (itemId || !newsItem.id) {
      return;
    }

    history.replace(`/${selectedAppVersion.version}/news/${newsItem.id}`);
  }, [selectedAppVersion, itemId, newsItem.id]);

  // On unmount
  useEffect(() => resetNewsItemAction, [itemId]);

  const handleSave = useCallback(
    async data => {
      if (itemId) {
        updateNewsItemAction(itemId, {
          ...data,
          app: `langshop${selectedAppVersion.appSuffix}`,
        });
      } else {
        createNewsItemAction({
          ...data,
          app: `langshop${selectedAppVersion.appSuffix}`,
        });
      }
    },
    [itemId, newsItem.id]
  );

  const onPositionChange = useCallback(
    position => {
      change("position", position);
    },
    [newsItem]
  );

  const handleRemove = useCallback(async () => {
    await removeNewsItemAction(itemId);
    history.replace(`/${selectedAppVersion.version}/news`);
  }, [selectedAppVersion, itemId]);

  const primaryAction = useMemo(() => {
    if (!hasUpdateScope) {
      return null;
    }

    return {
      content: "Save",
      onAction: handleSubmit(handleSave),
      disabled: !dirty || loading || invalid,
    };
  }, [handleSubmit, handleSave, dirty, loading, hasUpdateScope, invalid]);

  const secondaryActions = useMemo(() => {
    if (!itemId || !hasUpdateScope) {
      return [];
    }

    return [
      {
        content: "Delete",
        destructive: true,
        onAction: openRemoveModal,
      },
    ];
  }, [itemId, hasUpdateScope, openRemoveModal]);

  if (loading) {
    return <PageSkeleton />;
  }

  if (itemId && !newsItem.id) {
    return (
      <EmptyState
        heading="News item not found"
        action={{
          content: "Show news",
          url: `/${selectedAppVersion.version}/news`,
        }}
        image="https://cdn.shopify.com/s/files/1/0757/9955/files/empty-state.svg"
      >
        <p>Go to news list to show all</p>
      </EmptyState>
    );
  }

  return (
    <div className={"News-Item-Page"}>
      <Page
        fullWidth
        title={newsItem.title ? pageTitle(newsItem.title) : pageTitle("New news item")}
        primaryAction={primaryAction}
        breadcrumbs={[
          {
            content: "News",
            url: `/${selectedAppVersion.version}/news`,
          },
          backButton,
        ]}
        separator
      >
        <Layout>
          <Layout.Section oneHalf>
            <Card sectioned>
              <FormLayout>
                <Field
                  component={TabsFieldAdapter}
                  name="type"
                  tabs={[
                    {
                      content: "Article",
                      id: "article",
                    },
                    {
                      content: "Video",
                      id: "video",
                    },
                  ]}
                  fitted
                  disabled={!hasUpdateScope}
                />
                <Field
                  component={ComponentsFieldAdapter}
                  name="layout"
                  elements={[
                    {
                      id: "top",
                      component: (
                        <TextContainer>
                          <SkeletonThumbnail size="small" />
                          <SkeletonBodyText lines={1} />
                        </TextContainer>
                      ),
                    },
                    {
                      id: "right",
                      component: (
                        <Stack distribution={"equalSpacing"} alignment={"center"}>
                          <SkeletonBodyText lines={1} />
                          <SkeletonThumbnail size="small" />
                        </Stack>
                      ),
                    },
                    {
                      id: "bottom",
                      component: (
                        <TextContainer>
                          <SkeletonBodyText lines={1} />
                          <SkeletonThumbnail size="small" />
                        </TextContainer>
                      ),
                    },
                    {
                      id: "left",
                      component: (
                        <Stack distribution={"equalSpacing"} alignment={"center"}>
                          <SkeletonThumbnail size="small" />
                          <SkeletonBodyText lines={1} />
                        </Stack>
                      ),
                    },
                  ]}
                  disabled={!hasUpdateScope}
                />

                <FormLayout.Group>
                  {newsItem.type === "article" ? (
                    <Field component={ImageFieldAdapter} name="src" label="Image" disabled={!hasUpdateScope} />
                  ) : (
                    <Field
                      component={TextFieldAdapter}
                      name="video"
                      label="Video"
                      normalize={normalize.video}
                      prefix="https://www.youtube.com/watch?v="
                      connectedRight={
                        <Button
                          icon={ExternalSmallMinor}
                          url={newsItem.video ? `https://www.youtube.com/watch?v=${newsItem.video}` : null}
                          external={true}
                        />
                      }
                      readOnly={!hasUpdateScope}
                    />
                  )}
                </FormLayout.Group>

                <FormLayout.Group>
                  <Field
                    component={ToggleFieldAdapter}
                    name="sticky"
                    label="Sticky"
                    textStatus={`The article ${newsItem.sticky ? "is featured (sticky)" : "is not featured (sticky)"}`}
                    disabled={!hasUpdateScope}
                  />
                </FormLayout.Group>

                <FormLayout.Group>
                  <Field
                    component={TextFieldAdapter}
                    name="title"
                    label="Title"
                    type="text"
                    readOnly={!hasUpdateScope}
                    validate={validate.title}
                  />
                </FormLayout.Group>

                <FormLayout.Group>
                  <Field label="Description" name="description" component={EditorFieldAdapter} />
                </FormLayout.Group>

                <FormLayout.Group>
                  <Field
                    component={TextFieldAdapter}
                    name="primaryText"
                    label="Primary Text"
                    type="text"
                    readOnly={!hasUpdateScope}
                  />
                  <Field
                    component={TextFieldAdapter}
                    name="primaryLink"
                    label="Primary Link"
                    type="text"
                    readOnly={!hasUpdateScope}
                    validate={[primaryLink, primaryLinkUrl]}
                  />
                </FormLayout.Group>

                <FormLayout.Group>
                  <Field
                    component={TextFieldAdapter}
                    name="secondaryText"
                    label="Secondary Text"
                    type="text"
                    readOnly={!hasUpdateScope}
                  />
                  <Field
                    component={TextFieldAdapter}
                    name="secondaryLink"
                    label="Secondary Link"
                    type="text"
                    readOnly={!hasUpdateScope}
                    validate={[secondaryLink, secondaryLinkUrl]}
                  />
                </FormLayout.Group>
              </FormLayout>
            </Card>
          </Layout.Section>
          <Layout.Section oneHalf>
            <NewsPreview
              appSuffix={selectedAppVersion.appSuffix}
              currentArticle={newsItem}
              onPositionChange={onPositionChange}
            />
          </Layout.Section>
          <Layout.Section fullWidth>
            <PageActions primaryAction={primaryAction} secondaryActions={secondaryActions} />
          </Layout.Section>
        </Layout>
      </Page>
      <ConfirmationModal
        destructive
        title="Remove selected news"
        content="Are you sure you want to delete the selected news?"
        confirm="Remove"
        open={removeModalOpened}
        onCancel={closeRemoveModal}
        onConfirm={handleRemove}
      />
    </div>
  );
};
