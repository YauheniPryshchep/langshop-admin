import {
  initialValues,
  isFetched,
  isLoading,
  fetchNewsItemAction,
  removeNewsItemAction,
  resetNewsItemAction,
  createNewsItemAction,
  updateNewsItemAction,
} from "@store/news-item";
import { NewsItem } from "./NewsItem";
import { createStructuredSelector } from "reselect";
import { connect } from "react-redux";
import { reduxForm } from "redux-form";

const mapState = createStructuredSelector({
  initialValues,
  isLoading,
  isFetched,
});

const mapDispatch = {
  fetchNewsItemAction,
  removeNewsItemAction,
  resetNewsItemAction,
  createNewsItemAction,
  updateNewsItemAction,
};

export default connect(
  mapState,
  mapDispatch
)(
  reduxForm({
    form: "news-item-form",
    enableReinitialize: true,
  })(NewsItem)
);
