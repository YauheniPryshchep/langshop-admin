import React, { useCallback, useEffect, useMemo, useState } from "react";
import { Card, Filters, Page, ResourceItem, ResourceList, TextStyle } from "@shopify/polaris";
import useQuery from "@hooks/useQuery";
import useActive from "@hooks/useActive";
import useQueue from "@hooks/useQueue";
import useFetch from "@hooks/useFetch";
import Pagination from "@components/Pagination";
import ConfirmationModal from "@components/ConfirmationModal";
import { UPDATE_NEWS } from "@utils/scopes";
import useHasScope from "@hooks/useHasScope";
import useCancelToken from "@hooks/useCancelToken";
import { getApp } from "@store/app";
import { numberFormat } from "../../../../@defaults/numberFormat";
import { pageTitle } from "../../../../@defaults/pageTitle";
import { useHistory, useParams } from "react-router-dom";
import { useSelector } from "react-redux";

const limitOptions = [5, 10, 25, 100];

const sortDefault = "position-asc";

const sortOptions = [
  {
    label: "Position (Sticky-1-9)",
    value: "position-asc",
    field: "position",
    direction: "asc",
  },
  {
    label: "Position (9-1-Sticky)",
    value: "position-desc",
    field: "position",
    direction: "desc",
  },
  {
    label: "Alphabetically (A-Z)",
    value: "title-asc",
    field: "title",
    direction: "asc",
  },
  {
    label: "Alphabetically (Z-A)",
    value: "title-desc",
    field: "title",
    direction: "desc",
  },
];

export const News = ({
  title,
  fetchNewsAction,
  removeNewsItemAction,
  resetNewsAction,
  news,
  total,
  isFetched,
  isLoading,
}) => {
  const { appVersion } = useParams();
  const [query, setQuery] = useQuery();
  const hasScope = useHasScope();

  const [selectedStores, setSelectedStores] = useState([]);
  const [removeModalOpened, openRemoveModal, closeRemoveModal] = useActive();

  const [isQueueing, inQueue] = useQueue(5);
  const [cancelToken, cancelRequests] = useCancelToken();
  const selectedApp = useSelector(getApp)();

  const loading = useMemo(() => isLoading || isQueueing || !isFetched, [isLoading, isQueueing, isFetched]);

  const selectedAppVersion = useMemo(
    () => selectedApp.versions.find(version => version.version === appVersion) || selectedApp.versions[0],
    [appVersion]
  );

  const {
    page,
    limit,
    filter,
    sort,
    search,
    pages,
    onPreviousPage,
    onNextPage,
    onLimitChange,
    onSortChange,
    onSearchChange,
    onSearchClear,
    resetPageToDefault,
  } = useFetch({
    sortDefault,
    query,
    sortOptions,
    limitOptions,
    loading,
    total,
  });

  const fetchNews = useCallback(() => {
    const sortOption = sortOptions.find(option => sort === option.value);

    return fetchNewsAction(
      {
        filter,
        limit,
        page: page,
        sort: {
          field: sortOption.field,
          direction: sortOption.direction,
        },
        appSuffix: selectedAppVersion.appSuffix,
      },
      cancelToken
    );
  }, [selectedAppVersion, page, limit, sort, filter]);

  const removeNews = useCallback(async () => {
    closeRemoveModal();
    if (!selectedStores.length) {
      return false;
    }
    await inQueue(selectedStores.map(store => () => removeNewsItemAction(store)));

    setSelectedStores([]);
    resetPageToDefault();
    fetchNews();
  }, [selectedStores, closeRemoveModal, setSelectedStores]);

  useEffect(() => {
    fetchNews();
  }, [fetchNews]);

  // On unmount
  useEffect(
    () => () => {
      cancelRequests();
      resetNewsAction();
    },
    []
  );

  useEffect(() => {
    if (!isFetched) {
      return;
    }

    setQuery(getRequestQuery());
  }, [isFetched, news]);

  const getRequestQuery = useCallback(() => {
    let currentPage = page;

    if (currentPage < 1) {
      currentPage = 1;
    } else if (currentPage > pages) {
      currentPage = pages;
    }

    const requestQuery = {
      page: currentPage,
      limit: limit,
      sort: sort,
    };

    if (filter) {
      requestQuery["q"] = filter;
    }

    return requestQuery;
  }, [filter, limit, sort, page, pages]);

  const pagination = useMemo(() => {
    return {
      hasPrevious: pages > 1 && page > 1,
      hasNext: pages > 1 && page < pages,
      onPrevious: onPreviousPage,
      onNext: onNextPage,
    };
  }, [pages, page, onPreviousPage, onNextPage]);

  const perPage = useMemo(() => {
    return {
      limit: limit,
      options: limitOptions,
      onChange: onLimitChange,
    };
  }, [limit, onLimitChange]);

  const filterControl = (
    <Filters
      queryValue={search}
      queryPlaceholder="Search news ..."
      filters={[]}
      appliedFilters={[]}
      onQueryChange={onSearchChange}
      onQueryClear={onSearchClear}
      onClearAll={onSearchClear}
    />
  );

  const renderItem = item => {
    const { id, title } = item;

    return (
      <ResourceItem id={id} url={`/${selectedAppVersion.version}/news/${id}`}>
        <h3>
          <TextStyle variation="strong">{title}</TextStyle>
        </h3>
      </ResourceItem>
    );
  };

  const resourceListMarkup = (
    <ResourceList
      showHeader
      resourceName={{
        singular: "news item",
        plural: "news items",
      }}
      sortOptions={sortOptions}
      sortValue={sort}
      onSortChange={onSortChange}
      totalItemsCount={numberFormat(total)}
      loading={loading}
      items={news}
      filterControl={filterControl}
      renderItem={renderItem}
      bulkActions={[
        {
          content: "Remove",
          onAction: openRemoveModal,
        },
      ]}
      onSelectionChange={setSelectedStores}
      selectedItems={selectedStores}
      idForItem={item => item.id}
    />
  );

  const paginationMarkup =
    news && news.length ? (
      <Card.Section>
        <Pagination pagination={pagination} perPage={perPage} />
      </Card.Section>
    ) : null;

  return (
    <Page
      title={pageTitle(title)}
      primaryAction={
        hasScope(UPDATE_NEWS)
          ? {
              content: "Add new",
              url: `/${selectedAppVersion.version}/news/new`,
            }
          : null
      }
      separator
    >
      <Card>
        {resourceListMarkup}
        {paginationMarkup}
      </Card>

      <ConfirmationModal
        destructive
        title="Remove selected news"
        content="Are you sure you want to delete the selected news?"
        confirm="Remove"
        open={removeModalOpened}
        onCancel={closeRemoveModal}
        onConfirm={removeNews}
      />
    </Page>
  );
};
