import {
  initialValues,
  isFetched,
  isLoading,
  fetchLocaleFileAction,
  resetLocaleFileAction,
  updateLocaleFileAction,
} from "@store/locale-file";
import { LocaleFile } from "./File";
import { createStructuredSelector } from "reselect";
import { connect } from "react-redux";
import { reduxForm } from "redux-form";

const mapState = createStructuredSelector({
  initialValues,
  isLoading,
  isFetched,
});

const mapDispatch = {
  fetchLocaleFileAction,
  resetLocaleFileAction,
  updateLocaleFileAction,
};

export default connect(
  mapState,
  mapDispatch
)(
  reduxForm({
    form: "locale-file-form",
    enableReinitialize: true,
  })(LocaleFile)
);
