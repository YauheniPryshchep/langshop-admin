import React, { useCallback, useEffect, useMemo } from "react";
import { Button, Card, EmptyState, FormLayout, Page, ResourceItem, ResourceList, TextField } from "@shopify/polaris";
import { RefreshMajor } from "@shopify/polaris-icons";
import PageSkeleton from "@components/Skeleton/PageSkeleton";
import useHasScope from "@hooks/useHasScope";
import { UPDATE_LOCALES } from "@utils/scopes";
import TextFieldAdapter from "@components/TextFieldAdapter";
import TranslationsProgressBar from "@common/TranslationsProgressBar";
import useCancelToken from "@hooks/useCancelToken";
import { Field, getFormValues } from "redux-form";
import { useParams } from "react-router-dom";
import { required } from "redux-form-validators";
import { useSelector } from "react-redux";
import "./styles.scss";
import { SaveBar } from "../../../../../@common/SaveBar/SaveBar";
import { pageTitle } from "../../../../../@defaults/pageTitle";

export const LocaleFile = ({
  form,
  initialValues,
  handleSubmit,
  isFetched,
  isLoading,
  fetchLocaleFileAction,
  resetLocaleFileAction,
  updateLocaleFileAction,
  dirty,
  reset,
  change,
  invalid,
}) => {
  const hasScope = useHasScope();
  const hasUpdateScope = hasScope(UPDATE_LOCALES);
  const { locale, file } = useParams();
  const [cancelToken, cancelRequests] = useCancelToken();
  const { language, items } = initialValues;
  const formValues = useSelector(getFormValues(form));

  // On loading by route params
  useEffect(() => {
    if (!file) {
      return;
    }

    fetchLocaleFileAction(locale, file, cancelToken);
  }, [file]);

  // On unmount
  useEffect(
    () => () => {
      cancelRequests();
      resetLocaleFileAction();
    },
    []
  );

  const handleSave = useCallback(
    data => {
      updateLocaleFileAction(locale, file, {
        items: items.reduce((items, item, i) => {
          if (data.items[i].target === item.target) {
            return items;
          }

          items.push(data.items[i]);

          return items;
        }, []),
      });
    },
    [items, file]
  );

  const handleDiscard = useCallback(() => {
    reset();
  }, [reset]);

  const progress = useMemo(() => {
    const count = formValues.items.length;
    if (!count) {
      return 100;
    }

    let translated = 0;
    for (let i = 0; i < count; i++) {
      const item = formValues.items[i];

      if (!item.source) {
        translated++;
        continue;
      }

      if (item.target) {
        translated++;
      }
    }

    return (translated / count) * 100;
  }, [formValues.items]);

  const renderItem = useCallback((item, id, i) => {
    return (
      <ResourceItem id={id}>
        {item.source ? (
          <FormLayout>
            <div className="Polaris-Label">
              <label htmlFor={item.key} className="Polaris-Label__Text">
                {item.label}
              </label>
            </div>
            <div className={"File-Item-Wrapper"}>
              <div className={"File-Item File-Item--Source"}>
                <TextField label={""} value={item.source} labelHidden readOnly multiline />
              </div>
              <div className={"File-Item File-Item--Button"}>
                <Button icon={RefreshMajor} onClick={() => change(`items.${i}.target`, item.source)} />
              </div>
              <div className={"File-Item File-Item--Target"}>
                <Field id={item.key} component={TextFieldAdapter} name={`items.${i}.target`} type="text" multiline />
              </div>
            </div>
          </FormLayout>
        ) : (
          <Field
            id={item.key}
            component={TextFieldAdapter}
            label={item.label}
            name={`items.${i}.target`}
            type="text"
            validate={[required()]}
            multiline
          />
        )}
      </ResourceItem>
    );
  }, []);

  if (!isFetched) {
    return <PageSkeleton />;
  }

  if (!items.length) {
    return (
      <EmptyState
        heading="There are no any locales for this file"
        action={{
          content: "Other locale files",
          url: `/locales/${locale}`,
        }}
        image="https://cdn.shopify.com/s/files/1/0757/9955/files/empty-state.svg"
      >
        <p>Go to locale list to show all</p>
      </EmptyState>
    );
  }

  return (
    <div>
      <TranslationsProgressBar progress={progress} size="small" />
      {dirty && (
        <SaveBar
          saveAction={handleSubmit(handleSave)}
          discardAction={handleDiscard}
          loading={isLoading}
          disabled={!dirty || isLoading || invalid}
        />
      )}
      <Page
        title={language ? pageTitle(`${language} file ${file}`) : pageTitle(`${file} items`)}
        breadcrumbs={[{ content: "Back", url: `/misc/locales/${locale}` }]}
        separator
      >
        <Card>
          <ResourceList
            showHeader
            resourceName={{
              singular: "File item",
              plural: "File items",
            }}
            loading={isLoading}
            items={items}
            renderItem={renderItem}
          />
        </Card>
      </Page>
    </div>
  );
};
