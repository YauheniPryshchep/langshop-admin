import { locales, fetchLocalesAction, resetLocalesAction, isFetched, isLoading, total } from "@store/locales";
import { Locales } from "./Locales";
import { createStructuredSelector } from "reselect";
import { connect } from "react-redux";

const mapState = createStructuredSelector({
  locales,
  isFetched,
  isLoading,
  total,
});

const mapDispatch = {
  fetchLocalesAction,
  resetLocalesAction,
};

export default connect(mapState, mapDispatch)(Locales);
