import React, { useCallback, useEffect, useMemo, useState } from "react";
import { Card, Filters, Page, ResourceItem, ResourceList, TextStyle, Avatar, Layout } from "@shopify/polaris";
import useQuery from "@hooks/useQuery";
import useCancelToken from "@hooks/useCancelToken";
import Pagination from "@components/Pagination";
import TranslationsProgressBar from "@common/TranslationsProgressBar";
import { usePaginator } from "../../../../@hooks/usePaginator";
import { LanguagesFlagIcon } from "../../../../@components/LanguagesFlagIcon/LanguagesFlagIcon";
import { numberFormat } from "../../../../@defaults/numberFormat";
import { pageTitle } from "../../../../@defaults/pageTitle";

export const Locales = ({ title, locales, fetchLocalesAction, isFetched, isLoading, total }) => {
  const [query, setQuery] = useQuery();
  const [cancelToken, cancelRequests] = useCancelToken();
  const [selectedStores, setSelectedStores] = useState([]);
  const loading = useMemo(() => isLoading || !isFetched, [isLoading, isFetched]);

  const limitOptions = [5, 10, 25, 100];
  const sortOptions = [
    {
      label: "Alphabetically (A-Z)",
      value: "asc",
      field: "name",
      direction: "asc",
    },
    {
      label: "Alphabetically (Z-A)",
      value: "desc",
      field: "name",
      direction: "desc",
    },
  ];

  const {
    items,
    page,
    pages,
    onChangePage,
    search,
    onLimitChange,
    onSortChange,
    onSearchChange,
    onSearchClear,
    sort,
    limit,
  } = usePaginator(locales, query, "title", limitOptions, sortOptions);

  const fetchLocales = useCallback(() => {
    fetchLocalesAction({}, cancelToken);
  }, []);

  useEffect(() => {
    fetchLocales();
  }, [fetchLocales]);

  // On unmount
  useEffect(
    () => () => {
      cancelRequests();
    },
    []
  );

  const filterControl = (
    <Filters
      queryValue={search}
      queryPlaceholder="Search locales ..."
      filters={[]}
      appliedFilters={[]}
      onQueryChange={onSearchChange}
      onQueryClear={onSearchClear}
      onClearAll={onSearchClear}
    />
  );

  const renderItem = item => {
    const { iso, title, progress } = item;

    return (
      <ResourceItem id={iso} url={`/misc/locales/${iso}`} media={<LanguagesFlagIcon code={iso} />}>
        <Layout>
          <Layout.Section>
            <TextStyle variation="strong">{title}</TextStyle>
          </Layout.Section>
          <Layout.Section secondary>
            <TranslationsProgressBar progress={progress} size="small" />
          </Layout.Section>
        </Layout>
      </ResourceItem>
    );
  };

  const pagination = useMemo(() => {
    return {
      hasPrevious: pages > 1 && page > 1,
      hasNext: pages > 1 && page < pages,
      onPrevious: () => onChangePage(page - 1),
      onNext: () => onChangePage(page + 1),
    };
  }, [pages, page]);

  const perPage = useMemo(() => {
    return {
      limit: limit,
      options: limitOptions,
      onChange: onLimitChange,
    };
  }, [limit, onLimitChange]);

  return (
    <Page title={pageTitle(title)} separator>
      <Card>
        <ResourceList
          showHeader
          resourceName={{
            singular: "locale item",
            plural: "locales items",
          }}
          sortOptions={sortOptions}
          sortValue={sort}
          onSortChange={onSortChange}
          totalItemsCount={numberFormat(total)}
          loading={loading}
          items={items}
          filterControl={filterControl}
          renderItem={renderItem}
          onSelectionChange={setSelectedStores}
          selectedItems={selectedStores}
          idForItem={item => item.iso}
        />
        <Card.Section>
          <Pagination pagination={pagination} perPage={perPage} />
        </Card.Section>
      </Card>
    </Page>
  );
};
