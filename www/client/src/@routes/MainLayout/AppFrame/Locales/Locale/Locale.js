import React, { useCallback, useEffect, useMemo, useState } from "react";
import { Card, Filters, Page, ResourceItem, ResourceList, TextStyle, Layout } from "@shopify/polaris";
import useFetch from "@hooks/useFetch";
import useQuery from "@hooks/useQuery";
import useCancelToken from "@hooks/useCancelToken";
import Pagination from "@components/Pagination";
import TranslationsProgressBar from "@common/TranslationsProgressBar";
import { usePaginator } from "../../../../../@hooks/usePaginator";
import { numberFormat } from "../../../../../@defaults/numberFormat";
import { pageTitle } from "../../../../../@defaults/pageTitle";
import { useParams } from "react-router-dom";

export const Locale = ({ files, locale, fetchLocaleAction, resetLocaleAction, isFetched, isLoading, total }) => {
  const { iso } = useParams();
  const [query, setQuery] = useQuery();
  const [cancelToken, cancelRequests] = useCancelToken();
  const [selectedStores, setSelectedStores] = useState([]);
  const loading = useMemo(() => isLoading || !isFetched, [isLoading, isFetched]);

  const limitOptions = [5, 10, 25, 100];
  const sortOptions = [
    {
      label: "Alphabetically (A-Z)",
      value: "asc",
      field: "name",
      direction: "asc",
    },
    {
      label: "Alphabetically (Z-A)",
      value: "desc",
      field: "name",
      direction: "desc",
    },
  ];

  const {
    items,
    page,
    pages,
    onChangePage,
    search,
    onLimitChange,
    onSortChange,
    onSearchChange,
    onSearchClear,
    sort,
    limit,
  } = usePaginator(files, query, "title", limitOptions, sortOptions);

  const fetchLocale = useCallback(() => {
    fetchLocaleAction(iso, {}, cancelToken);
  }, []);

  useEffect(() => {
    fetchLocale();
  }, [fetchLocale]);

  // On unmount
  useEffect(
    () => () => {
      cancelRequests();
      resetLocaleAction();
    },
    []
  );

  const filterControl = (
    <Filters
      queryValue={search}
      queryPlaceholder="Search Locale ..."
      filters={[]}
      appliedFilters={[]}
      onQueryChange={onSearchChange}
      onQueryClear={onSearchClear}
      onClearAll={onSearchClear}
    />
  );

  const renderItem = item => {
    const { name, title, progress } = item;

    return (
      <ResourceItem id={name} url={`/misc/locales/${iso}/${name}`}>
        <Layout>
          <Layout.Section>
            <TextStyle variation="strong">{title}</TextStyle>
          </Layout.Section>
          <Layout.Section secondary>
            <TranslationsProgressBar progress={progress} size="small" />
          </Layout.Section>
        </Layout>
      </ResourceItem>
    );
  };

  const pagination = useMemo(() => {
    return {
      hasPrevious: pages > 1 && page > 1,
      hasNext: pages > 1 && page < pages,
      onPrevious: () => onChangePage(page - 1),
      onNext: () => onChangePage(page + 1),
    };
  }, [pages, page]);

  const perPage = useMemo(() => {
    return {
      limit: limit,
      options: limitOptions,
      onChange: onLimitChange,
    };
  }, [limit, onLimitChange]);

  return (
    <Page
      title={locale ? pageTitle(`${locale} sections`) : pageTitle("Loading sections")}
      separator
      breadcrumbs={[{ content: "Back", url: "/misc/locales" }]}
    >
      <Card>
        <ResourceList
          showHeader
          resourceName={{
            singular: "Locale section",
            plural: "Locale sections",
          }}
          sortOptions={sortOptions}
          sortValue={sort}
          onSortChange={onSortChange}
          totalItemsCount={numberFormat(total)}
          loading={loading}
          items={items}
          filterControl={filterControl}
          renderItem={renderItem}
          onSelectionChange={setSelectedStores}
          selectedItems={selectedStores}
          idForItem={item => item.name}
        />
        <Card.Section>
          <Pagination pagination={pagination} perPage={perPage} />
        </Card.Section>
      </Card>
    </Page>
  );
};
