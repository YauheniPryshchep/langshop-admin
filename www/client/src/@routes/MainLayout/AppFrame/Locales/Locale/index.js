import { locale, files, fetchLocaleAction, resetLocaleAction, isFetched, isLoading, total } from "@store/locale";
import { Locale } from "./Locale";
import { createStructuredSelector } from "reselect";
import { connect } from "react-redux";

const mapState = createStructuredSelector({
  locale,
  files,
  isFetched,
  isLoading,
  total,
});

const mapDispatch = {
  fetchLocaleAction,
  resetLocaleAction,
};

export default connect(mapState, mapDispatch)(Locale);
