import {
  initialValues,
  fetchCompanyItemAction,
  isLoading,
  isFetched,
  companyItem,
  resetCompanyItemAction,
  updateCompanyItemAction,
  removeCompanyItemAction,
  createCompanyAction,
  templateID,
} from "@store/company";
import { CompanyItem } from "./CompanyItem";
import { createStructuredSelector } from "reselect";
import { connect } from "react-redux";
import { reduxForm } from "redux-form";

const mapState = createStructuredSelector({
  initialValues,
  companyItem,
  isLoading,
  isFetched,
  templateID,
});

const mapDispatch = {
  updateCompanyItemAction,
  removeCompanyItemAction,
  fetchCompanyItemAction,
  resetCompanyItemAction,
  createCompanyAction,
};

export default connect(
  mapState,
  mapDispatch
)(
  reduxForm({
    form: "company-item-form",
    enableReinitialize: true,
  })(CompanyItem)
);
