import React, { useMemo, useCallback, useState, useEffect } from "react";
import { Card, Page, ResourceItem, ResourceList, TextStyle, Filters } from "@shopify/polaris";

import useFetch from "@hooks/useFetch";
import useQueue from "@hooks/useQueue";
import useActive from "@hooks/useActive";
import useCancelToken from "@hooks/useCancelToken";
import useQuery from "@hooks/useQuery";
import { sortOptions, limitOptions } from "@defaults/options";
import ConfirmationModal from "@components/ConfirmationModal";

export const Companies = ({
  companies,
  title,
  isLoading,
  isFetched,
  fetchCompaniesAction,
  removeCompanyAction,
  resetCompaniesAction,
}) => {
  const loading = useMemo(() => isLoading || !isFetched, [isLoading, isFetched]);
  const [query, setQuery] = useQuery();
  const [inQueue] = useQueue(5);
  const [cancelToken, cancelRequests] = useCancelToken();
  const [selectedCompanies, setSelectedCompanies] = useState([]);
  const [removeModalOpened, openRemoveModal, closeRemoveModal] = useActive();

  const { page, limit, filter, sort, search, pages, onSearchChange, onSearchClear } = useFetch({
    query,
    sortOptions,
    limitOptions,
    loading,
  });

  useEffect(() => {
    fetchCompanies();
  }, [fetchCompanies]);

  const removeCompany = useCallback(async () => {
    closeRemoveModal();

    await inQueue(selectedCompanies.map(company => () => removeCompanyAction(company)));

    setSelectedCompanies([]);
    fetchCompanies();
  }, [selectedCompanies, closeRemoveModal, setSelectedCompanies]);

  const fetchCompanies = useCallback(() => {
    const sortOption = sortOptions.find(option => sort === option.value);

    fetchCompaniesAction(
      {
        filter,
        limit,
        page: page - 1,
        sort: {
          field: sortOption.field,
          direction: sortOption.direction,
        },
      },
      cancelToken
    );
  }, [page, limit, sort, filter]);

  useEffect(() => {
    if (!isFetched) {
      return;
    }

    setQuery(getRequestQuery());
  }, [isFetched, companies]);

  useEffect(
    () => () => {
      cancelRequests();
      resetCompaniesAction();
    },
    []
  );

  const getRequestQuery = useCallback(() => {
    let currentPage = page;

    if (currentPage < 1) {
      currentPage = 1;
    } else if (currentPage > pages) {
      currentPage = pages;
    }

    const requestQuery = {
      page: currentPage,
      limit: limit,
      sort: sort,
    };

    if (filter) {
      requestQuery["q"] = filter;
    }

    return requestQuery;
  }, [filter, limit, sort, page, pages]);

  const filterControl = (
    <Filters
      queryValue={search}
      queryPlaceholder="Search companies ..."
      filters={[]}
      appliedFilters={[]}
      onQueryChange={onSearchChange}
      onQueryClear={onSearchClear}
      onClearAll={onSearchClear}
    />
  );

  // const pagination = useMemo(() => {
  //   return {
  //     hasPrevious: pages > 1 && page > 1,
  //     hasNext: pages > 1 && page < pages,
  //     onPrevious: onPreviousPage,
  //     onNext: onNextPage,
  //   };
  // }, [pages, page, onPreviousPage, onNextPage]);

  // const perPage = useMemo(() => {
  //   return {
  //     limit: limit,
  //     options: limitOptions,
  //     onChange: onLimitChange,
  //   };
  // }, [limit, onLimitChange]);

  const renderItem = ({ title, id }) => (
    <ResourceItem id={id} url={`/marketing/companies/${id}`}>
      <h3>
        <TextStyle variation="strong">{title}</TextStyle>
      </h3>
    </ResourceItem>
  );

  const resourceListMarkup = (
    <ResourceList
      showHeader
      resourceName={{
        singular: "company",
        plural: "companies",
      }}
      loading={loading}
      items={companies}
      filterControl={filterControl}
      renderItem={renderItem}
      bulkActions={[
        {
          content: "Remove",
          onAction: openRemoveModal,
        },
      ]}
      onSelectionChange={setSelectedCompanies}
      selectedItems={selectedCompanies}
      idForItem={item => item.id}
    />
  );

  return (
    <Page
      title={title}
      primaryAction={{
        content: "Add new",
        disabled: loading,
        url: `/marketing/companies/new`,
      }}
      separator
    >
      <Card>{resourceListMarkup}</Card>

      <ConfirmationModal
        destructive
        title="Remove selected companies"
        content="Are you sure you want to delete the selected companies?"
        confirm="Remove"
        open={removeModalOpened}
        onCancel={closeRemoveModal}
        onConfirm={removeCompany}
      />
    </Page>
  );
};
