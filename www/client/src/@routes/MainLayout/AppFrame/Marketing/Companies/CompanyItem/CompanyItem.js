import React, { useEffect, useMemo, useCallback } from "react";
import { FormLayout, Layout, Page, PageActions } from "@shopify/polaris";
import PageSkeleton from "@components/Skeleton/PageSkeleton";
import { backHistory } from "@store/history-listener";
import TextFieldAdapter from "@components/TextFieldAdapter";
import DatePicker from "@components/DatePicker";
import { useParams, useHistory } from "react-router-dom";
import { useSelector } from "react-redux";
import { Field, getFormValues } from "redux-form";
import { required } from "redux-form-validators";

const validate = {
  title: [
    required({
      message: "Company title is required",
    }),
  ],
  description: [
    required({
      message: "Company description is required",
    }),
  ],
  subject: [
    required({
      message: "Company subject is required",
    }),
  ],
  start_at: [
    required({
      message: "Company start date is required",
    }),
  ],
};

export const CompanyItem = ({
  handleSubmit,
  templateID,
  form,
  fetchCompanyItemAction,
  resetCompanyItemAction,
  createCompanyAction,
  updateCompanyItemAction,
  removeCompanyItemAction,
  isLoading,
  isFetched,
  dirty,
  invalid,
}) => {
  const history = useHistory();
  const { itemId } = useParams();
  const loading = useMemo(() => {
    if (isLoading) {
      return true;
    }

    if (!itemId) {
      return false;
    }

    return !isFetched;
  }, [itemId, isLoading, isFetched]);

  const company = useSelector(getFormValues(form));
  const backButton = useSelector(backHistory)(["marketing", "marketing/companies"], {
    content: "Companies",
    url: "/marketing/companies",
  });

  // new
  useEffect(() => {
    if (itemId || !company.id) {
      return;
    }
    history.replace(`/marketing/companies/${company.id}`);
  }, [itemId, company.id]);

  // edit
  useEffect(() => {
    if (!itemId) {
      return;
    }

    fetchCompanyItemAction(itemId);
  }, [itemId]);

  useEffect(() => resetCompanyItemAction, [itemId]);

  const handleSave = useCallback(
    async data => {
      data["template_id"] = templateID;

      if (itemId) {
        await updateCompanyItemAction(itemId, {
          ...data,
        });
      } else {
        await createCompanyAction({
          ...data,
        });
      }
      history.goBack();
    },
    [itemId, company.id]
  );

  const handleRemove = useCallback(async () => {
    await removeCompanyItemAction(itemId);
    history.goBack();
  }, [itemId]);

  const primaryAction = useMemo(() => {
    return {
      content: "Save",
      onAction: handleSubmit(handleSave),
      disabled: !dirty || loading || invalid,
    };
  }, [handleSubmit, handleSave, dirty, loading, invalid]);

  const secondaryActions = useMemo(() => {
    if (!itemId) {
      return [];
    }

    return [
      {
        content: "Delete",
        destructive: true,
        onAction: handleRemove,
      },
    ];
  }, [itemId, handleRemove]);

  if (loading) {
    return <PageSkeleton />;
  }

  return (
    <div>
      <Page
        fullWidth
        title={company.title || "New company"}
        primaryAction={primaryAction}
        breadcrumbs={[
          {
            content: "Companies",
            url: "/marketing/companies",
          },
          backButton,
        ]}
        separator
      >
        <Layout>
          <FormLayout>
            <Field
              component={TextFieldAdapter}
              name="title"
              label="Company title"
              type="text"
              placeholder="E.g, some company"
              validate={validate.title}
              normalize={value => value.replace(/\s+/g, "")}
            />
            <Field
              component={TextFieldAdapter}
              name="description"
              label="Company description"
              type="text"
              placeholder="E.g, Company for promotion"
              validate={validate.description}
              normalize={value => value.replace(/\s+/g, "")}
            />
            <Field
              component={TextFieldAdapter}
              name="subject"
              label="Company subject"
              type="text"
              placeholder="E.g, promotion"
              validate={validate.subject}
              normalize={value => value.replace(/\s+/g, "")}
            />
            <Field
              component={DatePicker}
              disableDatesBefore={new Date()}
              name="start_at"
              selected="Date"
              validate={validate.start_at}
            />
            <Layout.Section fullWidth>
              <PageActions primaryAction={primaryAction} secondaryActions={secondaryActions} />
            </Layout.Section>
          </FormLayout>
        </Layout>
      </Page>
    </div>
  );
};
