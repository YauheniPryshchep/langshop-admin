import {
  fetchCompaniesAction,
  resetCompaniesAction,
  removeCompanyAction,
  companies,
  isFetched,
  isLoading,
} from "@store/companies";
import { Companies } from "./Companies";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";

const mapState = createStructuredSelector({
  companies,
  isFetched,
  isLoading,
});

const mapDispatch = {
  fetchCompaniesAction,
  resetCompaniesAction,
  removeCompanyAction,
};

export default connect(mapState, mapDispatch)(Companies);
