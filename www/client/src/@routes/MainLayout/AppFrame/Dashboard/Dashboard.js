import React, {useMemo} from "react";
import {Page} from "@shopify/polaris";
import useHasScope from "@hooks/useHasScope";
import PlanQuantities from "./components/PlanQuantities";
import {SHOW_ANALYTICS} from "@utils/scopes";

export const Dashboard = () => {
  const hasScope = useHasScope();

  const analyticsView = useMemo(() => {
    if (hasScope(SHOW_ANALYTICS)) {
      return <PlanQuantities/>
    }
    return null;
  }, [hasScope]);

  return <Page fullWidth>
    {analyticsView}
  </Page>;
};
