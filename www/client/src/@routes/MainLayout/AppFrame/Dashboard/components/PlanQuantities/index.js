import {
  planQuantitiesAction,
  resetPlanQuantitiesAction,
  planQuantitiesSevenDaysAgoAction,
  isFetched,
  isLoading,
  planQuantities
} from "@store/analytics/plan-quantities";
import {PlanQuantities} from "./PlanQuantities";
import {createStructuredSelector} from "reselect";
import {connect} from "react-redux";

const mapState = createStructuredSelector({
  planQuantities,
  isFetched,
  isLoading,
});

const mapDispatch = {
  planQuantitiesAction,
  planQuantitiesSevenDaysAgoAction,
  resetPlanQuantitiesAction,
};

export default connect(mapState, mapDispatch)(PlanQuantities);
