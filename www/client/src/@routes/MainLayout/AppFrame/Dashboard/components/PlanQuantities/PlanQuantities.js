import React, {useCallback, useEffect, useMemo} from 'react';
import {
  Card,
  DisplayText,
  Icon,
  SkeletonBodyText,
  SkeletonDisplayText,
  Stack,
  TextStyle
} from "@shopify/polaris";
import {CircleDownMajor, CircleMinusMajor, CircleUpMajor} from "@shopify/polaris-icons";
import Padding from "@components/Padding";
import useQueue from "@hooks/useQueue";
import map from 'lodash/map';

const TITLES = {
  "null": "Without plan",
  "free": "Free plan",
  "standard": "Standard plan",
  "advanced": "Advanced plan",
  "enterprise": "Enterprise plan",
};

const defaultPlans = [
  {
    key: "null",
    value: 0,
    diff: 0,
    type: "equal"
  },
  {
    key: "free",
    value: 0,
    diff: 0,
    type: "equal"
  },
  {
    key: "standard",
    value: 0,
    diff: 0,
    type: "equal"
  },
  {
    key: "advanced",
    value: 0,
    diff: 0,
    type: "equal"
  },
  {
    key: "enterprise",
    value: 0,
    diff: 0,
    type: "equal"
  }
];

export const PlanQuantities = ({
                                 planQuantities,
                                 planQuantitiesSevenDaysAgoAction,
                                 planQuantitiesAction,
                                 resetPlanQuantitiesAction,
                               }) => {

  const [isQueueing, inQueue] = useQueue(2);


  useEffect(() => {

    inQueue([planQuantitiesAction, planQuantitiesSevenDaysAgoAction]);

    return () => resetPlanQuantitiesAction();
  }, [planQuantitiesAction, resetPlanQuantitiesAction, planQuantitiesSevenDaysAgoAction]);

  const getIcon = useCallback((type) => {
    if (type === 'equal') {
      return <Icon
        source={CircleMinusMajor}
        color="base"/>
    }

    if (type === "decrease") {
      return <Icon
        source={CircleDownMajor}
        color="redDark"/>
    }
    return <Icon
      source={CircleUpMajor}
      color="green"/>

  }, []);

  const getTextType = useCallback((type) => {
    if (type === 'equal') {
      return "strong"
    }

    if (type === "decrease") {
      return "negative";
    }
    return "positive"

  }, []);

  const loadingView = useMemo(() => {
    return map(defaultPlans, (plan, id) => (
      <Stack.Item key={id} fill>
        <Card title={TITLES[plan.key]} actions={[{content: "View report"}]}>
          <Card.Section>
            <SkeletonDisplayText size="medium"/>
            <Padding/>
            <SkeletonBodyText lines={1}/>
          </Card.Section>
        </Card>
      </Stack.Item>
    ))
  }, [defaultPlans]);

  const analyticsView = useMemo(() => {
    return map(planQuantities, (plan, id) => (
      <Stack.Item key={id} fill>
        <Card title={TITLES[plan.key]} actions={[{content: "View report"}]}>
          <Card.Section>
            <DisplayText size="extraLarge"> {plan.value}</DisplayText>
            <Padding/>
            <Stack spacing={"tight"} alignment={"center"}>
              <Stack.Item>
                <TextStyle>
                  {getIcon(plan.type)}
                </TextStyle>
              </Stack.Item>
              <Stack.Item>
                <TextStyle variation={getTextType(plan.type)}>
                  <DisplayText size="small"> {plan.diff}</DisplayText>
                </TextStyle>
              </Stack.Item>
              <Stack.Item>
                <TextStyle>
                  in last 7 days
                </TextStyle>
              </Stack.Item>
            </Stack>
          </Card.Section>
        </Card>
      </Stack.Item>
    ))
  }, [planQuantities, getTextType, getIcon]);

  const view = useMemo(() => {
    if (isQueueing) {
      return loadingView
    }
    return analyticsView
  }, [isQueueing, analyticsView, loadingView]);


  return (
    <Stack distribution={"fillEvenly"}>
      {view}
    </Stack>
  );
};