import React, { useCallback, useMemo, useState } from "react";
import { Card, EmptyState, Page } from "@shopify/polaris";
import useHasScope from "@hooks/useHasScope";
import { SHOW_VOCABULARIES, UPDATE_VOCABULARIES } from "@utils/scopes";
import useActive from "@hooks/useActive";
import { languages } from "@store/config/languages";
import useToggle from "@hooks/useToggle";
import Vocabulary from "./Vocabulary";
import { VocabulariesDirections } from "../../../../@common/Vocabularies/VocabulariesDirections/VocabulariesDirections";
import { useNotification } from "../../../../@hooks/useNotification";
import { pageTitle } from "../../../../@defaults/pageTitle";
import { t } from "@localization";
import { get } from "lodash";

export const Vocabularies = ({ directions, isLoading, deleteDirectionActions }) => {
  const hasScope = useHasScope();
  const hasUpdateScope = hasScope(UPDATE_VOCABULARIES);
  const [newItemModalOpened, openNewItemModal, closeNewItemModal] = useActive();
  const [removeModalOpened, openRemoveModal, closeRemoveModal] = useActive();
  const [isRemoving, toogleRemoving] = useToggle();
  const { handleNotify } = useNotification();

  const [codes, setCodes] = useState({
    from: "en",
    to: "de",
  });

  const handleChangeDirections = useCallback(
    (key, value) => {
      if (key === "from") {
        let to = get(
          languages.filter(lang => lang.code !== value),
          "[0].code"
        );
        setCodes(() => ({
          from: value,
          to,
        }));
      } else {
        setCodes(state => ({
          ...state,
          [key]: value,
        }));
      }
    },
    [directions, setCodes]
  );

  const primaryAction = useMemo(() => {
    if (!hasUpdateScope) {
      return null;
    }

    return {
      content: t("actions.add"),
      onAction: openNewItemModal,
      disabled: isLoading || !codes.from || !codes.to,
    };
  }, [isLoading, codes]);

  const handleRemoveDirection = useCallback(async () => {
    await toogleRemoving();
    await deleteDirectionActions({ ...codes });
    toogleRemoving();
    closeRemoveModal();
    handleNotify(
      t("translationMemories.messages.delete-directions", 1, {
        ...codes,
      })
    );
  }, [toogleRemoving]);

  if (!hasScope(SHOW_VOCABULARIES)) {
    return (
      <EmptyState
        heading={t("errors.forbidden.title")}
        action={{
          content: t("translationMemories.forbidden.content"),
          url: "/",
        }}
        image="https://cdn.shopify.com/s/files/1/0757/9955/files/empty-state.svg"
      />
    );
  }

  return (
    <Page title={pageTitle(t("translationMemories.header"))} primaryAction={primaryAction}>
      <div style={{ marginTop: "2rem" }} />
      <VocabulariesDirections
        from={codes.from}
        to={codes.to}
        onChange={handleChangeDirections}
        deleteDirectionActions={deleteDirectionActions}
      />
      <Card>
        <Vocabulary
          from={codes.from}
          to={codes.to}
          newItemModalOpened={newItemModalOpened}
          closeNewItemModal={closeNewItemModal}
          closeRemoveDirectionModal={closeRemoveModal}
          removeDirectionModalOpened={removeModalOpened}
          handleRemoveDirection={handleRemoveDirection}
          openRemoveDirectionModal={openRemoveModal}
          isRemoveDirection={isRemoving}
        />
      </Card>
    </Page>
  );
};
