import {
  vocabulariesDirections,
  fetchVocabulariesDirectionsAction,
  resetVocabulariesDirectionsAction,
  addDirection,
  isFetched,
  isLoading,
} from "@store/vocabularies-directions";
import { deleteDirectionActions } from "@store/vocabularies";
import { Vocabularies } from "./Vocabularies";
import { createStructuredSelector } from "reselect";
import { connect } from "react-redux";

const mapState = createStructuredSelector({
  directions: vocabulariesDirections,
  isFetched,
  isLoading,
});

const mapDispatch = {
  fetchVocabulariesDirectionsAction,
  addDirection,
  resetVocabulariesDirectionsAction,
  deleteDirectionActions,
};

export default connect(mapState, mapDispatch)(Vocabularies);
