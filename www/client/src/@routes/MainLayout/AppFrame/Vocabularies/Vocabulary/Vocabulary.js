import React, {useCallback, useEffect, useMemo, useState} from "react";
import {Button, Card, FormLayout, ResourceItem, ResourceList} from "@shopify/polaris";
import {RefreshMajor} from "@shopify/polaris-icons";
import useHasScope from "@hooks/useHasScope";
import {UPDATE_VOCABULARIES} from "@utils/scopes";
import TextFieldAdapter from "@components/TextFieldAdapter";
import useCancelToken from "@hooks/useCancelToken";
import useFetch from "@hooks/useFetch";
import Pagination from "@components/Pagination";
import useQuery from "@hooks/useQuery";
import VocabularyItemModal from "@common/Modals/VocabularyItemModal";
import {VocabulariesFilters} from "@common/Vocabularies/VocabulariesFilters/VocabulariesFilters";
import "./styles.scss";
import useToggle from "@hooks/useToggle";
import {SaveBar} from "../../../../../@common/SaveBar/SaveBar";
import useQueue from "../../../../../@hooks/useQueue";
import useActive from "../../../../../@hooks/useActive";
import ConfirmationModal from "../../../../../@components/ConfirmationModal";
import {useNotification} from "../../../../../@hooks/useNotification";
import {numberFormat} from "../../../../../@defaults/numberFormat";
import {Field} from "redux-form";
import {required} from "redux-form-validators";
import {t} from "@localization";
import {get} from "lodash";

const validate = {
  original: [
    required({
      message: "Original value is required",
    }),
  ],
  translated: [
    required({
      message: "Translated value is required",
    }),
  ],
};
const limitOptions = [5, 10, 25, 100];
const sortOptions = [
  {
    label: "Alphabetically (A-Z)",
    value: "asc",
    field: "original",
    direction: "asc",
  },
  {
    label: "Alphabetically (Z-A)",
    value: "desc",
    field: "original",
    direction: "desc",
  },
];

export const Vocabulary = ({
                             initialValues,
                             handleSubmit,
                             isLoading,
                             fetchVocabulariesAction,
                             resetVocabulariesAction,
                             dirty,
                             reset,
                             change,
                             invalid,
                             total,
                             from,
                             to,
                             closeNewItemModal,
                             newItemModalOpened,
                             updateVocabulariesActions,
                             deleteVocabulariesActions,
                             handleRemoveDirection,
                             removeDirectionModalOpened,
                             closeRemoveDirectionModal,
                             openRemoveDirectionModal,
                             profile,
                             isRemoveDirection,
                           }) => {
  const hasScope = useHasScope();
  const [query, setQuery] = useQuery();
  const [isQueueing, inQueue] = useQueue(5);
  const hasUpdateScope = hasScope(UPDATE_VOCABULARIES);
  const [cancelToken, cancelRequests] = useCancelToken();
  const {items} = initialValues;
  const [selectedVocabulary, setSelectedVocabulary] = useState([]);
  const [removeModalOpened, openRemoveModal, closeRemoveModal] = useActive();
  const {handleNotify} = useNotification();
  const [isRemoving, toggleRemove] = useToggle();
  const {page, limit, pages, onLimitChange, filters, onChangeFilters} = useFetch({
    limitOptions,
    isLoading,
    sortOptions,
    total,
    query,
  });

  const [requestData, setRequestData] = useState({
    filters,
    limit,
    page,
  });

  const removeVocabularies = useCallback(async () => {
    if (!selectedVocabulary.length) {
      closeRemoveModal();

      return false;
    }

    await toggleRemove();
    await inQueue(selectedVocabulary.map(vocabulary => () => deleteVocabulariesActions(vocabulary)));
    await toggleRemove();
    handleNotify(
      t("translationMemories.messages.delete", selectedVocabulary.length, {
        count: selectedVocabulary.length,
      })
    );
    closeRemoveModal();

    fetchVocabulariesAction(from, to, requestData, cancelToken);
    setSelectedVocabulary([]);
  }, [selectedVocabulary, closeRemoveModal, setSelectedVocabulary, from, to, requestData, cancelToken, toggleRemove]);

  const onPreviousPage = useCallback(() => {
    if (isLoading) {
      return;
    }

    let page = requestData.page - 1;
    if (page > pages) {
      page = pages;
    }

    setRequestData(state => ({
      ...state,
      page: page,
    }));
  }, [isLoading, setRequestData]);

  const onNextPage = useCallback(() => {
    if (isLoading) {
      return;
    }

    setRequestData(state => ({
      ...state,
      page: requestData.page + 1,
    }));
  }, [isLoading, setRequestData]);

  useEffect(() => {
    if (requestData.limit !== limit || JSON.stringify(requestData.filters) !== JSON.stringify(filters)) {
      setRequestData(() => ({
        filters: filters,
        limit,
        page: 1,
      }));
    }
  }, [limit, filters]);

  // On loading by route params
  useEffect(() => {
    if (!from) {
      return;
    }
    setQuery(requestData);
    fetchVocabulariesAction(from, to, requestData, cancelToken);
  }, [requestData, from, to]);

  // On unmount
  useEffect(
    () => () => {
      cancelRequests();
      resetVocabulariesAction();
    },
    []
  );

  const handleSave = useCallback(
    async data => {
      let updateItems = data.items.reduce((acc, currentItem) => {
        // eslint-disable-next-line no-underscore-dangle
        let item = items.find(i => i._id === currentItem._id);
        if (!item) {
          return [...acc];
        }
        if (
          !item.hasOwnProperty("original") ||
          !item.hasOwnProperty("translated") ||
          !currentItem.hasOwnProperty("original") ||
          !currentItem.hasOwnProperty("translated")
        ) {
          return [...acc];
        }
        if (item.original !== currentItem.original || item.translated !== currentItem.translated) {
          return [
            ...acc,
            {
              ...currentItem,
            },
          ];
        }

        return [...acc];
      }, []);

      if (!updateItems.length) {
        return false;
      }

      // eslint-disable-next-line no-underscore-dangle
      await inQueue(updateItems.map(item => () => updateVocabulariesActions(item._id, item)));
      handleNotify(
        t("translationMemories.messages.update", updateItems.length, {
          count: updateItems.length,
          from,
          to,
        })
      );
    },
    [items]
  );

  const handleDiscard = useCallback(() => {
    reset();
  }, []);

  const renderItem = useCallback(
    (item, id, i) => {
      return (
        <ResourceItem id={id}>
          {item.original ? (
            <FormLayout>
              <div className={"File-Item-Wrapper"}>
                <div className={"File-Item File-Item--Source"}>
                  <Field
                    component={TextFieldAdapter}
                    name={`items.${i}.original`}
                    type="text"
                    readOnly={!hasUpdateScope || isQueueing}
                    validate={validate.original}
                    multiline
                  />
                </div>
                <div className={"File-Item File-Item--Button"}>
                  <Button icon={RefreshMajor} onClick={() => change(`items.${i}.translated`, item.original)}/>
                </div>
                <div className={"File-Item File-Item--Target"}>
                  <Field
                    component={TextFieldAdapter}
                    name={`items.${i}.translated`}
                    type="text"
                    readOnly={!hasUpdateScope || isQueueing}
                    validate={validate.translated}
                    multiline
                  />
                </div>
              </div>
            </FormLayout>
          ) : (
            <Field
              id={item.id}
              component={TextFieldAdapter}
              label={item.hash}
              name={`items.${i}.translated`}
              type="text"
              readOnly={!hasUpdateScope || isQueueing}
              validate={validate.translated}
              multiline
            />
          )}
        </ResourceItem>
      );
    },
    [isQueueing]
  );

  const pagination = useMemo(() => {
    const {page} = requestData;
    return {
      hasPrevious: pages > 1 && page > 1,
      hasNext: pages > 1 && page < pages,
      onPrevious: onPreviousPage,
      onNext: onNextPage,
    };
  }, [pages, requestData.page, onPreviousPage, onNextPage]);

  const perPage = useMemo(() => {
    return {
      limit: limit,
      options: limitOptions,
      onChange: onLimitChange,
    };
  }, [limit, onLimitChange]);

  const paginationMarkup =
    items && items.length ? (
      <Card.Section>
        <Pagination pagination={pagination} perPage={perPage}/>
      </Card.Section>
    ) : null;

  const handleCreate = useCallback(() => {
    closeNewItemModal();
    fetchVocabulariesAction(from, to, requestData, cancelToken);
    handleNotify(
      t("translationMemories.messages.create", 1, {
        from,
        to,
      })
    );
  }, [newItemModalOpened, closeNewItemModal, from, to, requestData, cancelToken]);

  return (
    <div>
      <Card>
        {dirty && (
          <SaveBar
            saveAction={handleSubmit(handleSave)}
            discardAction={handleDiscard}
            loading={isLoading || isQueueing}
            disabled={isLoading || invalid || isQueueing}
          />
        )}

        <ResourceList
          showHeader
          resourceName={{
            singular: t("translationMemories.resource", 1),
            plural: t("translationMemories.resource", 2),
          }}
          loading={isLoading || isQueueing}
          filterControl={<VocabulariesFilters filters={filters} onChange={onChangeFilters}/>}
          selectable={true}
          hasMoreItems={pages > 1}
          totalItemsCount={numberFormat(total)}
          items={items}
          renderItem={renderItem}
          onSelectionChange={setSelectedVocabulary}
          selectedItems={selectedVocabulary}
          idForItem={item => {
            /* eslint-disable-next-line no-underscore-dangle */
            return item._id;
          }}
          bulkActions={[
            {
              content: t("actions.delete"),
              onAction: selectedVocabulary === "All" ? openRemoveDirectionModal : openRemoveModal,
            },
          ]}
        />
        {paginationMarkup}
      </Card>
      <VocabularyItemModal
        open={newItemModalOpened}
        onClose={closeNewItemModal}
        readOnly={["from", "to"]}
        initialValues={{
          from,
          to,
          original: "",
          translated: "",
          source: "agency",
          sourceId: 2,
          sourceAttributes: {
            userId: get(profile, "id", 0),
          },
        }}
        handleCreate={handleCreate}
      />
      <ConfirmationModal
        destructive
        title={t("translationMemories.modals.delete.title")}
        content={t("translationMemories.modals.delete.description", selectedVocabulary.length, {
          count: selectedVocabulary.length,
        })}
        confirm={t("actions.delete")}
        loading={isRemoving}
        open={removeModalOpened}
        onCancel={closeRemoveModal}
        onConfirm={removeVocabularies}
      />
      <ConfirmationModal
        destructive
        title={t("translationMemories.modals.delete-all.title")}
        content={t("translationMemories.modals.delete-all.description", 1, {
          count: selectedVocabulary.length,
          from,
          to,
        })}
        confirm={t("actions.delete")}
        open={removeDirectionModalOpened}
        loading={isRemoveDirection}
        onCancel={closeRemoveDirectionModal}
        onConfirm={handleRemoveDirection}
      />
    </div>
  );
};
