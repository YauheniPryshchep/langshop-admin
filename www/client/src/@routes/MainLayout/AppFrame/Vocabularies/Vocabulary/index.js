import {
  initialValues,
  total,
  fetchVocabulariesAction,
  resetVocabulariesAction,
  isFetched,
  isLoading,
  updateVocabulariesActions,
  deleteVocabulariesActions,
} from "@store/vocabularies";
import { profile } from "@store/profile";
import { Vocabulary } from "./Vocabulary";
import { createStructuredSelector } from "reselect";
import { connect } from "react-redux";
import { reduxForm } from "redux-form";

const mapState = createStructuredSelector({
  initialValues,
  total,
  isFetched,
  isLoading,
  profile,
});

const mapDispatch = {
  fetchVocabulariesAction,
  resetVocabulariesAction,
  updateVocabulariesActions,
  deleteVocabulariesActions,
};

export default connect(
  mapState,
  mapDispatch
)(
  reduxForm({
    form: "vocabulary-form",
    enableReinitialize: true,
  })(Vocabulary)
);
