import React, { Suspense } from "react";

import NestedRoute from "@common/NestedRoute";
import { PageSkeleton } from "@components/Skeleton/PageSkeleton/PageSkeleton";
import AppFrame from "./AppFrame";
import { Switch } from "react-router-dom";

export const MainLayout = ({ routes }) => {
  return (
    <AppFrame>
      <Suspense fallback={<PageSkeleton />}>
        <Switch>
          {routes.map((route, i) => (
            <NestedRoute key={i} {...route} />
          ))}
        </Switch>
      </Suspense>
    </AppFrame>
  );
};
