import { setHistoryAction } from "@store/history-listener";
import { HistoryListener } from "./historyListener";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";

const mapDispatch = {
  setHistoryAction,
};

export default connect(null, mapDispatch)(withRouter(HistoryListener));
