import { useMemo } from "react";

export const HistoryListener = ({ history, children, setHistoryAction }) => {
  useMemo(() => {
    history.listen(location => {
      const historyPayload = {
        path: location.pathname,
        search: location.search,
        title: document.title,
      };
      setHistoryAction(historyPayload);
    });
  }, [setHistoryAction]);

  return children;
};
