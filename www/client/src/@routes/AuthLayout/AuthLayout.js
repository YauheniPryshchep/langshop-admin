import React, { Suspense } from "react";

import { AppProvider, Page } from "@shopify/polaris";
import en from "@shopify/polaris/locales/en";
import AuthSkeleton from "@components/Skeleton/AuthSkeleton";
import NestedRoute from "@common/NestedRoute";
import LinkAdapter from "@components/LinkAdapter";
import { theme } from "@utils/theme";
import { Switch } from "react-router-dom";

export const AuthLayout = ({ routes }) => {
  return (
    <AppProvider linkComponent={LinkAdapter} i18n={en} theme={theme} features={{ newDesignLanguage: true }}>
      <Page narrowWidth>
        <Suspense fallback={<AuthSkeleton />}>
          <Switch>
            {routes.map((route, i) => (
              <NestedRoute key={i} {...route} />
            ))}
          </Switch>
        </Suspense>
      </Page>
    </AppProvider>
  );
};
