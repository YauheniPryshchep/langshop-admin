import {loginAction} from '@store/auth'
import {SingIn} from "./SignIn";
import {
  reduxForm,
  stopSubmit,
} from "redux-form";


export default reduxForm({
  form: "sign-in",
  onChange: (_, dispatch, {form, submitFailed}) => {
    if (!submitFailed) {
      return;
    }

    if (form != null) {
      dispatch(stopSubmit(form));
    }
  },
  onSubmit: (values, dispatch) => {
    return dispatch(loginAction(values))
  },
})(SingIn);
