import React from "react";
import {
  Button,
  Card,
  DisplayText,
  Form,
  FormLayout,
  InlineError,
  Stack,
  TextStyle,
} from "@shopify/polaris";
import TextFieldAdapter from "@components/TextFieldAdapter";
import {Field} from "redux-form";
import {required, length} from "redux-form-validators";

const validate = {
  login: [
    required({
      message: "Email field is required",
    }),
  ],
  password: [
    required({
      message: "Password field is required",
    }),
    length({minimum: 8})
  ],
};

export const SingIn = ({form, handleSubmit, invalid, submitting, submitFailed, error}) => {

  return (
    <Card>
      <Card.Section>
        <Stack alignment={"center"}>
          <Stack.Item>
            <DisplayText size={"large"}>LangShop</DisplayText>
          </Stack.Item>
        </Stack>
        <Form onSubmit={handleSubmit}>
          <FormLayout>
            <DisplayText size={"large"}>Log in</DisplayText>
            <TextStyle variation={"subdued"}>
              <DisplayText size={"small"}>
                continue to LangShop Admin
              </DisplayText>
            </TextStyle>
            <Field
              component={TextFieldAdapter}
              name="email"
              label="Email"
              type="text"
              validate={validate.login}
            />
            <Field
              component={TextFieldAdapter}
              name="password"
              label="Password"
              type="password"
              validate={validate.password}
            />
            {submitFailed && error && <InlineError message={error} fieldID={form}/>}
            <Button size={"large"} submit primary fullWidth disabled={invalid || submitting}
                    loading={submitting}>
              Login
            </Button>
          </FormLayout>
        </Form>
      </Card.Section>
    </Card>
  );
};
