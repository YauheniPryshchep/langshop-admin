import React, {lazy} from "react";
import RedirectUnauthorized from "@guards/RedirectUnauthorized";
import RedirectAuthorized from "@guards/RedirectAuthorized";
import ProtectedAppRoute from "@guards/ProtectedAppRoute";
import AuthLayout from "@routes/AuthLayout";
import MainLayout from "@routes/MainLayout";
import {
  hasScope,
  SHOW_DEMO_STORES,
  SHOW_LOCALES,
  SHOW_NEWS,
  SHOW_STORES_DISCOUNTS,
  SHOW_STORES_TRIALS,
  SHOW_USERS,
  SHOW_USERS_ROLES,
  UPDATE_USERS_ROLES,
  SHOW_CAMPAIGNS,
  UPDATE_CAMPAIGNS,
  SHOW_VOCABULARIES,
  SHOW_COUPONS,
  UPDATE_COUPONS,
  SHOW_CUSTOMERS,
  SHOW_PARTNERS_USERS,
  SHOW_PARTNERS_PAYOUTS,
  SHOW_FINANCIAL,
} from "@utils/scopes";
import componentLoader from "../@utils/componentLoader";

const RETRY_CHUNK_LOAD_ATTEMPT = 5;

export default [
  {
    path: "/auth",
    component: AuthLayout,
    guards: [RedirectAuthorized],
    routes: [
      {
        title: "Sign In",
        path: "/auth/sign-in",
        component: lazy(() => componentLoader(() => import("@routes/AuthLayout/SignIn"), RETRY_CHUNK_LOAD_ATTEMPT)),
        exact: true,
      },
      {
        redirect: "/auth/sign-in",
      },
    ],
  },
  {
    path: "/",
    component: MainLayout,
    guards: [RedirectUnauthorized],
    routes: [
      {
        path: "/",
        component: lazy(() => componentLoader(() => import("@routes/MainLayout/AppFrame/Dashboard"), RETRY_CHUNK_LOAD_ATTEMPT)),
        exact: true,
      },
      {
        title: "News",
        path: "/:appVersion?/news",
        component: lazy(() => componentLoader(() => import("@routes/MainLayout/AppFrame/News"), RETRY_CHUNK_LOAD_ATTEMPT)),
        guards: [
          ({children}) => (
            <ProtectedAppRoute allowed={profileScopes => hasScope(profileScopes, SHOW_NEWS)}>
              {children}
            </ProtectedAppRoute>
          ),
        ],
        exact: true,
      },
      {
        title: "News Item",
        path: "/:appVersion/news/new",
        component: lazy(() => componentLoader(() => import("@routes/MainLayout/AppFrame/News/NewsItem"), RETRY_CHUNK_LOAD_ATTEMPT)),
        guards: [
          ({children}) => (
            <ProtectedAppRoute allowed={profileScopes => hasScope(profileScopes, SHOW_NEWS)}>
              {children}
            </ProtectedAppRoute>
          ),
        ],
        exact: true,
      },
      {
        title: "News Item",
        path: "/:appVersion/news/:itemId",
        component: lazy(() => componentLoader(() => import("@routes/MainLayout/AppFrame/News/NewsItem"), RETRY_CHUNK_LOAD_ATTEMPT)),
        guards: [
          ({children}) => (
            <ProtectedAppRoute allowed={profileScopes => hasScope(profileScopes, SHOW_NEWS)}>
              {children}
            </ProtectedAppRoute>
          ),
        ],
        exact: true,
      },
      {
        title: "Roles",
        path: "/users/roles",
        component: lazy(() => componentLoader(() => import("@routes/MainLayout/AppFrame/Users/Roles"), RETRY_CHUNK_LOAD_ATTEMPT)),
        guards: [
          ({children}) => (
            <ProtectedAppRoute allowed={profileScopes => hasScope(profileScopes, SHOW_USERS_ROLES)}>
              {children}
            </ProtectedAppRoute>
          ),
        ],
        exact: true,
      },
      {
        title: "Role",
        path: "/users/roles/new",
        component: lazy(() => componentLoader(() => import("@routes/MainLayout/AppFrame/Users/Roles/Role"), RETRY_CHUNK_LOAD_ATTEMPT)),
        guards: [
          ({children}) => (
            <ProtectedAppRoute allowed={profileScopes => hasScope(profileScopes, UPDATE_USERS_ROLES)}>
              {children}
            </ProtectedAppRoute>
          ),
        ],
        exact: true,
      },
      {
        title: "Role",
        path: "/users/roles/:roleId",
        component: lazy(() => componentLoader(() => import("@routes/MainLayout/AppFrame/Users/Roles/Role"), RETRY_CHUNK_LOAD_ATTEMPT)),
        guards: [
          ({children}) => (
            <ProtectedAppRoute allowed={profileScopes => hasScope(profileScopes, SHOW_USERS_ROLES)}>
              {children}
            </ProtectedAppRoute>
          ),
        ],
        exact: true,
      },
      {
        title: "Users",
        path: "/users",
        component: lazy(() => componentLoader(() => import("@routes/MainLayout/AppFrame/Users"), RETRY_CHUNK_LOAD_ATTEMPT)),
        guards: [
          ({children}) => (
            <ProtectedAppRoute allowed={profileScopes => hasScope(profileScopes, SHOW_USERS)}>
              {children}
            </ProtectedAppRoute>
          ),
        ],
        exact: true,
      },
      {
        title: "User",
        path: "/users/:userId",
        component: lazy(() => componentLoader(() => import("@routes/MainLayout/AppFrame/Users/User"), RETRY_CHUNK_LOAD_ATTEMPT)),
        guards: [
          ({children}) => (
            <ProtectedAppRoute allowed={profileScopes => hasScope(profileScopes, SHOW_USERS)}>
              {children}
            </ProtectedAppRoute>
          ),
        ],
        exact: true,
      },
      {
        title: "Stores",
        path: "/stores",
        component: lazy(() => componentLoader(() => import("@routes/MainLayout/AppFrame/Stores"), RETRY_CHUNK_LOAD_ATTEMPT)),
        exact: true,
      },
      {
        title: "Store",
        path: "/stores/:name",
        component: lazy(() => componentLoader(() => import("@routes/MainLayout/AppFrame/Stores/Store"), RETRY_CHUNK_LOAD_ATTEMPT)),
        exact: true,
      },
      {
        title: "Demo Stores",
        path: "/discounts/demo-stores",
        component: lazy(() => componentLoader(() => import("@routes/MainLayout/AppFrame/Discounts/DemoStores"), RETRY_CHUNK_LOAD_ATTEMPT)),
        guards: [
          ({children}) => (
            <ProtectedAppRoute allowed={profileScopes => hasScope(profileScopes, SHOW_DEMO_STORES)}>
              {children}
            </ProtectedAppRoute>
          ),
        ],
        exact: true,
      },
      {
        title: "Stores Discounts",
        path: "/discounts/discount-stores",
        component: lazy(() => componentLoader(() => import("@routes/MainLayout/AppFrame/Discounts/StoresDiscounts"), RETRY_CHUNK_LOAD_ATTEMPT)),
        guards: [
          ({children}) => (
            <ProtectedAppRoute allowed={profileScopes => hasScope(profileScopes, SHOW_STORES_DISCOUNTS)}>
              {children}
            </ProtectedAppRoute>
          ),
        ],
        exact: true,
      },
      {
        title: "Stores Trials",
        path: "/discounts/trial-stores",
        component: lazy(() => componentLoader(() => import("@routes/MainLayout/AppFrame/Discounts/StoresTrials"), RETRY_CHUNK_LOAD_ATTEMPT)),
        guards: [
          ({children}) => (
            <ProtectedAppRoute allowed={profileScopes => hasScope(profileScopes, SHOW_STORES_TRIALS)}>
              {children}
            </ProtectedAppRoute>
          ),
        ],
        exact: true,
      },
      {
        title: "Locales",
        path: "/misc/locales",
        component: lazy(() => componentLoader(() => import("@routes/MainLayout/AppFrame/Locales"), RETRY_CHUNK_LOAD_ATTEMPT)),
        guards: [
          ({children}) => (
            <ProtectedAppRoute allowed={profileScopes => hasScope(profileScopes, SHOW_LOCALES)}>
              {children}
            </ProtectedAppRoute>
          ),
        ],
        exact: true,
      },
      {
        title: "Locale items",
        path: "/misc/locales/:iso",
        component: lazy(() => componentLoader(() => import("@routes/MainLayout/AppFrame/Locales/Locale"), RETRY_CHUNK_LOAD_ATTEMPT)),
        guards: [
          ({children}) => (
            <ProtectedAppRoute allowed={profileScopes => hasScope(profileScopes, SHOW_LOCALES)}>
              {children}
            </ProtectedAppRoute>
          ),
        ],
        exact: true,
      },
      {
        title: "Locale file items",
        path: "/misc/locales/:locale/:file",
        component: lazy(() => componentLoader(() => import("@routes/MainLayout/AppFrame/Locales/File"), RETRY_CHUNK_LOAD_ATTEMPT)),
        guards: [
          ({children}) => (
            <ProtectedAppRoute allowed={profileScopes => hasScope(profileScopes, SHOW_LOCALES)}>
              {children}
            </ProtectedAppRoute>
          ),
        ],
        exact: true,
      },
      {
        title: "Campaigns",
        path: "/marketing/campaigns",
        component: lazy(() => componentLoader(() => import("@routes/MainLayout/AppFrame/Campaigns"), RETRY_CHUNK_LOAD_ATTEMPT)),
        guards: [
          ({children}) => (
            <ProtectedAppRoute allowed={profileScopes => hasScope(profileScopes, SHOW_CAMPAIGNS)}>
              {children}
            </ProtectedAppRoute>
          ),
        ],
        exact: true,
      },
      {
        title: "Campaign",
        path: "/marketing/campaigns/new",
        component: lazy(() => componentLoader(() => import("@routes/MainLayout/AppFrame/Campaigns/Campaign"), RETRY_CHUNK_LOAD_ATTEMPT)),
        guards: [
          ({children}) => (
            <ProtectedAppRoute allowed={profileScopes => hasScope(profileScopes, UPDATE_CAMPAIGNS)}>
              {children}
            </ProtectedAppRoute>
          ),
        ],
        exact: true,
      },
      {
        title: "Campaign",
        path: "/marketing/campaigns/:campaignId",
        component: lazy(() => componentLoader(() => import("@routes/MainLayout/AppFrame/Campaigns/Campaign"), RETRY_CHUNK_LOAD_ATTEMPT)),
        guards: [
          ({children}) => (
            <ProtectedAppRoute allowed={profileScopes => hasScope(profileScopes, UPDATE_CAMPAIGNS)}>
              {children}
            </ProtectedAppRoute>
          ),
        ],
        exact: true,
      },
      {
        title: "Translation memories",
        path: "/misc/memories",
        component: lazy(() => componentLoader(() => import("@routes/MainLayout/AppFrame/Vocabularies"), RETRY_CHUNK_LOAD_ATTEMPT)),
        guards: [
          ({children}) => (
            <ProtectedAppRoute allowed={profileScopes => hasScope(profileScopes, SHOW_VOCABULARIES)}>
              {children}
            </ProtectedAppRoute>
          ),
        ],
        exact: true,
      },
      {
        title: "Coupons",
        path: "/marketing/coupons",
        component: lazy(() => componentLoader(() => import("@routes/MainLayout/AppFrame/Coupons"), RETRY_CHUNK_LOAD_ATTEMPT)),
        guards: [
          ({children}) => (
            <ProtectedAppRoute allowed={profileScopes => hasScope(profileScopes, SHOW_COUPONS)}>
              {children}
            </ProtectedAppRoute>
          ),
        ],
        exact: true,
      },
      {
        title: "Coupon",
        path: "/marketing/coupons/new",
        component: lazy(() => componentLoader(() => import("@routes/MainLayout/AppFrame/Coupons/Coupon"), RETRY_CHUNK_LOAD_ATTEMPT)),
        guards: [
          ({children}) => (
            <ProtectedAppRoute allowed={profileScopes => hasScope(profileScopes, UPDATE_COUPONS)}>
              {children}
            </ProtectedAppRoute>
          ),
        ],
        exact: true,
      },
      {
        title: "Coupon",
        path: "/marketing/coupons/:couponId",
        component: lazy(() => componentLoader(() => import("@routes/MainLayout/AppFrame/Coupons/Coupon"), RETRY_CHUNK_LOAD_ATTEMPT)),
        guards: [
          ({children}) => (
            <ProtectedAppRoute allowed={profileScopes => hasScope(profileScopes, UPDATE_COUPONS)}>
              {children}
            </ProtectedAppRoute>
          ),
        ],
        exact: true,
      },
      {
        title: "Misc",
        path: "/misc",
        component: lazy(() => componentLoader(() => import("@routes/MainLayout/AppFrame/Misc"), RETRY_CHUNK_LOAD_ATTEMPT)),
        guards: [
          ({children}) => (
            <ProtectedAppRoute
              allowed={profileScopes =>
                hasScope(profileScopes, SHOW_VOCABULARIES) || hasScope(profileScopes, SHOW_LOCALES)
              }
            >
              {children}
            </ProtectedAppRoute>
          ),
        ],
        exact: true,
      },
      {
        title: "Customers",
        path: "/customers",
        component: lazy(() => componentLoader(() => import("@routes/MainLayout/AppFrame/Customers"), RETRY_CHUNK_LOAD_ATTEMPT)),
        guards: [
          ({children}) => (
            <ProtectedAppRoute allowed={profileScopes => hasScope(profileScopes, SHOW_CUSTOMERS)}>
              {children}
            </ProtectedAppRoute>
          ),
        ],
        exact: true,
      },
      {
        title: "Customer",
        path: "/customers/:customerId",
        component: lazy(() => componentLoader(() => import("@routes/MainLayout/AppFrame/Customers/Customer"), RETRY_CHUNK_LOAD_ATTEMPT)),
        guards: [
          ({children}) => (
            <ProtectedAppRoute allowed={profileScopes => hasScope(profileScopes, SHOW_CUSTOMERS)}>
              {children}
            </ProtectedAppRoute>
          ),
        ],
        exact: true,
      },
      {
        title: "Partners",
        path: "/partners/users",
        component: lazy(() => componentLoader(() => import("@routes/MainLayout/AppFrame/Partners/Users"), RETRY_CHUNK_LOAD_ATTEMPT)),
        guards: [
          ({children}) => (
            <ProtectedAppRoute allowed={profileScopes => hasScope(profileScopes, SHOW_PARTNERS_USERS)}>
              {children}
            </ProtectedAppRoute>
          ),
        ],
        exact: true,
      },
      {
        title: "Partners",
        path: "/partners/users/:userId",
        component: lazy(() => componentLoader(() => import("@routes/MainLayout/AppFrame/Partners/User"), RETRY_CHUNK_LOAD_ATTEMPT)),
        guards: [
          ({children}) => (
            <ProtectedAppRoute allowed={profileScopes => hasScope(profileScopes, SHOW_PARTNERS_USERS)}>
              {children}
            </ProtectedAppRoute>
          ),
        ],
        exact: true,
      },
      {
        title: "Payouts",
        path: "/partners/payouts",
        component: lazy(() => componentLoader(() => import("@routes/MainLayout/AppFrame/Partners/Payouts"), RETRY_CHUNK_LOAD_ATTEMPT)),
        guards: [
          ({children}) => (
            <ProtectedAppRoute
              allowed={profileScopes =>
                hasScope(profileScopes, SHOW_PARTNERS_PAYOUTS) && hasScope(profileScopes, SHOW_FINANCIAL)
              }
            >
              {children}
            </ProtectedAppRoute>
          ),
        ],
        exact: true,
      },
      {
        title: "Payout",
        path: "/partners/payouts/:payoutId",
        component: lazy(() => componentLoader(() => import("@routes/MainLayout/AppFrame/Partners/Payout"), RETRY_CHUNK_LOAD_ATTEMPT)),
        guards: [
          ({children}) => (
            <ProtectedAppRoute
              allowed={profileScopes =>
                hasScope(profileScopes, SHOW_PARTNERS_PAYOUTS) && hasScope(profileScopes, SHOW_FINANCIAL)
              }
            >
              {children}
            </ProtectedAppRoute>
          ),
        ],
        exact: true,
      },
    ],
  },
  {
    redirect: "/",
  },
];
