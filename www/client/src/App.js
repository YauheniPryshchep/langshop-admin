import React from "react";
import routes from "@routes";

import NestedRoute from "@common/NestedRoute";
import HistoryListener from "./@routes/HistoryListener";
import { BrowserRouter, Switch } from "react-router-dom";

export const App = () => {
  return (
    <BrowserRouter>
      <HistoryListener>
        <Switch>
          {routes.map((route, i) => (
            <NestedRoute key={i} {...route} />
          ))}
        </Switch>
      </HistoryListener>
    </BrowserRouter>
  );
};
