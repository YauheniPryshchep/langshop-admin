import React, { useMemo } from "react";
import ProgressBar from "@components/ProgressBar";

export const TranslationsProgressBar = ({ progress, ...rest }) => {
  const color = useMemo(() => {
    switch (true) {
      case progress === 100:
        return "success";

      case progress >= 60:
        return "warning";
    }

    return "alert";
  }, [progress]);

  return <ProgressBar {...rest} progress={progress} color={color} />;
};
