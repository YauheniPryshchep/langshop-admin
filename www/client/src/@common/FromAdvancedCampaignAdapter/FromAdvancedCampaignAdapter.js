import React, {useCallback, useMemo} from 'react';
import {Select, Stack, TextField} from "@shopify/polaris";
import split from 'lodash/split';

export const FromAdvancedCampaignAdapter = ({input: {value, onChange}, meta}) => {
  const isError = (meta.dirty || meta.touched || meta.submitting) && meta.invalid;

  const email = useMemo(() => {
    const email = split(value, '@');
    return email[0];

  }, [value]);

  const domain = useMemo(() => {
    const email = split(value, '@');
    return '@' + email[1];
  }, [value]);

  const handleChangeEmail = useCallback((newValue) => {
    if (newValue && newValue.match(/[a-zA-Z0-9_\.-]+/)[0] !== newValue) {
      onChange(value)
    } else {
      const email = split(value, '@');
      email[0] = newValue;
      onChange(email.join('@'));
    }
  }, [onChange, value]);

  const handleChangeDomain = useCallback((newValue) => {
    const email = split(value, '@');
    email[1] = newValue;
    onChange(email.join(''));
  }, [onChange, value]);

  return (
    <Stack alignment={"leading"} distribution={'fillEvenly'}>
      <Stack.Item fill>
        <TextField
          label={"From (optional)"}
          value={email}
          error={isError ? meta.error : false}
          onChange={handleChangeEmail}
        />
      </Stack.Item>
      <Stack.Item fill>
        <Select
          label={"Domain"}
          value={domain}
          options={[
            {
              label: "Select domain",
              value: ""
            },
            {
              label: "@langshop.marketing",
              value: "@langshop.marketing"
            },
            {
              label: "@help.langshop.app",
              value: "@help.langshop.app"
            },
            {
              label: "@community.langshop.app",
              value: "@community.langshop.app"
            },
            {
              label: "@partners.langshop.app",
              value: "@partners.langshop.app"
            },
            {
              label: "@promo.langshop.app",
              value: "@promo.langshop.app"
            }
          ]}
          onChange={handleChangeDomain}
        />
      </Stack.Item>
    </Stack>
  );
};