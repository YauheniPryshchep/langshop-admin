import React, { useRef } from "react";
import { AppProvider, Frame as PolarisFrame } from "@shopify/polaris";
import en from "@shopify/polaris/locales/en";
import Navigation from "@components/Navigation";
import TopBar from "@common/TopBar";
import LinkAdapter from "@components/LinkAdapter";
import useToggle from "@hooks/useToggle";

export const Frame = ({
  navigation,
  userMenuActions,
  userName,
  userDetail,
  userPhoto,
  theme,
  handleSearchChange,
  handleSearchClear,
  isSearched,
  isSearching,
  searchResults,
  searchResultsItemAction,
  searchResultsEmptyAction,
  children,
}) => {
  const skipToContentRef = useRef(null);

  const [mobileNavigationActive, toggleMobileNavigationActive] = useToggle();

  const skipToContentTarget = <a id="SkipToContentTarget" ref={skipToContentRef} tabIndex={-1} />;

  return (
    <AppProvider linkComponent={LinkAdapter} theme={theme} i18n={en} features={{ newDesignLanguage: true }}>
      <PolarisFrame
        topBar={
          <TopBar
            name={userName}
            detail={userDetail}
            photo={userPhoto}
            userMenuActions={userMenuActions}
            toggleMobileNavigationActive={toggleMobileNavigationActive}
            handleSearchChange={handleSearchChange}
            handleSearchClear={handleSearchClear}
            isSearched={isSearched}
            isSearching={isSearching}
            searchResults={searchResults}
            searchResultsItemAction={searchResultsItemAction}
            searchResultsEmptyAction={searchResultsEmptyAction}
          />
        }
        navigation={navigation ? <Navigation navigation={navigation} /> : null}
        showMobileNavigation={mobileNavigationActive}
        onNavigationDismiss={toggleMobileNavigationActive}
        skipToContentTarget={skipToContentRef.current}
      >
        {skipToContentTarget}
        {children}
      </PolarisFrame>
    </AppProvider>
  );
};
