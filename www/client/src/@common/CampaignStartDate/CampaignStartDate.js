import React from "react";
import DatePicker from "../../@components/DatePicker";
import { Field } from "redux-form";

export const CampaignStartDate = ({ isDisabled }) => {
  return (
    <Field
      component={DatePicker}
      disableDatesBefore={new Date()}
      isDisabled={isDisabled}
      name="start_at"
      selected="Date"
    />
  );
};
