import React, { useMemo } from "react";
import { Avatar, Card, Link, Stack } from "@shopify/polaris";
import { gravatarEmail } from "../../@defaults/gravatarEmail";

export const CustomerInfoCard = ({ customer }) => {
  const avatarSection = useMemo(
    () => (
      <Card.Section>
        <Stack alignment={"center"} distribution={"center"}>
          <Stack.Item>
            <Avatar source={gravatarEmail(customer.email)} size={"large"} />
          </Stack.Item>
        </Stack>
      </Card.Section>
    ),
    [customer]
  );

  const nameSection = useMemo(
    () => (
      <Card.Section title={"Name"}>
        {customer.first_name} {customer.last_name}
      </Card.Section>
    ),
    [customer]
  );

  const emailSection = useMemo(
    () => (
      <Card.Section title={"Email"}>
        <Link url={`https://support.devit-team.com/#ticket/create`} external={true}>
          {customer.email}
        </Link>
      </Card.Section>
    ),
    [customer]
  );

  return (
    <Card title={"Customer Information"}>
      {avatarSection}
      {nameSection}
      {emailSection}
    </Card>
  );
};
