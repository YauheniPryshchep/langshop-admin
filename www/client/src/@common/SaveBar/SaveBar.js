import React from "react";
import { ContextualSaveBar } from "@shopify/polaris";

export const SaveBar = ({ saveAction, discardAction, loading, disabled }) => {
  return (
    <ContextualSaveBar
      message="Unsaved changes"
      saveAction={{
        onAction: saveAction,
        loading: loading,
        disabled: disabled,
      }}
      discardAction={{
        onAction: discardAction,
        disabled: loading,
      }}
    />
  );
};
