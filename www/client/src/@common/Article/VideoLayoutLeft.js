import React from "react";

import { Card, Heading, TextContainer } from "@shopify/polaris";

import useToggle from "@hooks/useToggle";
import ArticleActions from "./ArticleActions";
import ArticleVideoActivator from "./ArticleVideoActivator";
import ArticleVideoWrapper from "./ArticleVideoWrapper";

const VideoLayoutLeft = ({ article }) => {
  const dangerDescriptionContent = { __html: article.description };
  const [isOpen, handleToggle] = useToggle();

  return (
    <Card>
      <ArticleVideoWrapper article={article} isOpen={isOpen} handleToggle={handleToggle} />
      <div className="article article-layout-left">
        <ArticleVideoActivator isOpen={isOpen} handelToggle={handleToggle} />
        <div className="article-container">
          <div className="article-content-holder">
            <TextContainer spacing="tight">
              <Heading>{article.title}</Heading>
              <p dangerouslySetInnerHTML={dangerDescriptionContent} />
            </TextContainer>
          </div>
          <ArticleActions article={article} />
        </div>
      </div>
    </Card>
  );
};

export default VideoLayoutLeft;
