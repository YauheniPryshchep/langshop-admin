import React from "react";

import { Card, Heading, TextContainer } from "@shopify/polaris";

import useToggle from "@hooks/useToggle";
import ArticleActions from "./ArticleActions";
import ArticleVideoWrapper from "./ArticleVideoWrapper";
import ArticleVideoActivator from "./ArticleVideoActivator";

const VideoLayoutRight = ({ article }) => {
  const dangerDescriptionContent = { __html: article.description };
  const [isOpen, handleToggle] = useToggle();

  return (
    <Card>
      <ArticleVideoWrapper article={article} isOpen={isOpen} handleToggle={handleToggle} />
      <div className="article article-layout-right">
        <div className="article-container">
          <div className="article-content-holder">
            <TextContainer spacing="tight">
              <Heading>{article.title}</Heading>
              <p dangerouslySetInnerHTML={dangerDescriptionContent} />
            </TextContainer>
          </div>
          <ArticleActions article={article} />
        </div>
        <ArticleVideoActivator isOpen={isOpen} handelToggle={handleToggle} />
      </div>
    </Card>
  );
};

export default VideoLayoutRight;
