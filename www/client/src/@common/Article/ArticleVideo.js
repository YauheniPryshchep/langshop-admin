import React from "react";

const ActiveVideo = ({ article }) => {
  const iframeUrl = "https://www.youtube.com/embed/" + article.video + "?autoplay=0&rel=0";
  return article.video !== "" ? (
    <div className="article-media-holder">
      <div className="article-video-wrapper">
        <iframe
          className="article-video"
          width="100%"
          height="100%"
          src={iframeUrl}
          style={{
            border: "0px",
          }}
        />
      </div>
    </div>
  ) : (
    ""
  );
};

export default ActiveVideo;
