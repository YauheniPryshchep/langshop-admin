import React from "react";

import { Card, Heading, TextContainer } from "@shopify/polaris";

import ArticleActions from "./ArticleActions";
import ArticleMedia from "./ArticleMedia";

const ArticleLayoutLeft = ({ article }) => {
  const dangerDescriptionContent = { __html: article.description };

  return (
    <Card>
      <div className="article article-layout-left">
        <ArticleMedia article={article} />
        <div className="article-container">
          <div className="article-content-holder">
            <TextContainer spacing="tight">
              <Heading>{article.title}</Heading>
              <p dangerouslySetInnerHTML={dangerDescriptionContent} />
            </TextContainer>
          </div>
          <ArticleActions article={article} />
        </div>
      </div>
    </Card>
  );
};

export default ArticleLayoutLeft;
