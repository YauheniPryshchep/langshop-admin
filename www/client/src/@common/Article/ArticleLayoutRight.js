import React from "react";

import { Card, Heading, TextContainer } from "@shopify/polaris";

import ArticleActions from "./ArticleActions";
import ArticleMedia from "./ArticleMedia";

const ArticleLayoutRight = ({ article }) => {
  const dangerDescriptionContent = { __html: article.description };

  return (
    <Card>
      <div className="article article-layout-right">
        <div className="article-container">
          <div className="article-content-holder">
            <TextContainer spacing="tight">
              <Heading>{article.title}</Heading>
              <p dangerouslySetInnerHTML={dangerDescriptionContent} />
            </TextContainer>
          </div>
          <ArticleActions article={article} />
        </div>
        <ArticleMedia article={article} />
      </div>
    </Card>
  );
};

export default ArticleLayoutRight;
