import React from "react";

import { Card, Heading, TextContainer } from "@shopify/polaris";

import ArticleActions from "./ArticleActions";
import ArticleMedia from "./ArticleMedia";

const ArticleLayoutTop = ({ article }) => {
  const dangerDescriptionContent = { __html: article.description };

  return (
    <Card sectioned>
      <div className="article article-layout-top">
        <ArticleMedia article={article} />
        <div className="article-content-holder">
          <TextContainer spacing="tight">
            <Heading>{article.title}</Heading>
            <p dangerouslySetInnerHTML={dangerDescriptionContent} />
          </TextContainer>
          <ArticleActions article={article} />
        </div>
      </div>
    </Card>
  );
};

export default ArticleLayoutTop;
