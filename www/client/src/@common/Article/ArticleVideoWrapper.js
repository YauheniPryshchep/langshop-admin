import React from "react";
import { Collapsible, Button } from "@shopify/polaris";
import { CancelSmallMinor } from "@shopify/polaris-icons";
import ArticleVideo from "./ArticleVideo";

const ArticleVideoWrapper = ({ article, isOpen, handleToggle }) => {
  return (
    <div className="article-video-collapsible">
      <Collapsible id={article.id} open={isOpen}>
        <ArticleVideo article={article} />
        <div className="article-video-close">
          <Button plain icon={CancelSmallMinor} onClick={handleToggle} />
        </div>
      </Collapsible>
    </div>
  );
};
export default ArticleVideoWrapper;
