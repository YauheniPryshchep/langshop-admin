import React from "react";

import ArticleLayoutBottom from "./ArticleLayoutBottom";
import ArticleLayoutLeft from "./ArticleLayoutLeft";
import ArticleLayoutRight from "./ArticleLayoutRight";
import ArticleLayoutTop from "./ArticleLayoutTop";
import VideoLayoutLeft from "./VideoLayoutLeft";
import VideoLayoutRight from "./VideoLayoutRight";
import "./styles.scss";

export const Article = ({ article }) => {
  let layout;

  switch (true) {
    case article.type === "video" && article.layout === "left":
      layout = <VideoLayoutLeft article={article} />;
      break;
    case article.type === "video" && article.layout === "right":
      layout = <VideoLayoutRight article={article} />;
      break;
    case article.layout === "top":
      layout = <ArticleLayoutTop article={article} />;
      break;
    case article.layout === "bottom":
      layout = <ArticleLayoutBottom article={article} />;
      break;
    case article.layout === "left":
      layout = <ArticleLayoutLeft article={article} />;
      break;
    case article.layout === "right":
      layout = <ArticleLayoutRight article={article} />;
      break;
  }

  return layout;
};
