import React from "react";

import { Card, Heading, TextContainer } from "@shopify/polaris";

import ArticleActions from "./ArticleActions";
import ArticleMedia from "./ArticleMedia";

const ArticleLayoutBottom = ({ article }) => {
  const dangerDescriptionContent = { __html: article.description };

  return (
    <Card sectioned>
      <div className="article article-layout-bottom">
        <div className="article-content-holder">
          <TextContainer spacing="tight">
            <Heading>{article.title}</Heading>
            <p dangerouslySetInnerHTML={dangerDescriptionContent} />
          </TextContainer>
        </div>
        <ArticleMedia article={article} />
        <ArticleActions article={article} />
      </div>
    </Card>
  );
};

export default ArticleLayoutBottom;
