import React from "react";

import ArticleImage from "./ArticleImage";
import ArticleVideo from "./ArticleVideo";

const ArticleMedia = ({ article }) => {
  if (article.type === "video") {
    return <ArticleVideo article={article} />;
  }

  return <ArticleImage article={article} />;
};

export default ArticleMedia;
