import React, { useEffect, useState } from "react";

const ArticleImage = ({ article }) => {
  const [source, setSource] = useState(article.src);

  useEffect(() => {
    if (!(article.src instanceof File)) {
      setSource(article.src);
      return;
    }

    const reader = new FileReader();
    reader.readAsDataURL(article.src);
    reader.onload = () => {
      setSource(reader.result);
    };
  }, [setSource, article.src]);

  if (!source) {
    return null;
  }

  return (
    <div className="article-media-holder">
      {["top", "bottom"].includes(article.layout) ? (
        <img className="article-image" src={source} alt={""} />
      ) : (
        <div className="article-image" style={{ backgroundImage: `url(${source})` }} />
      )}
    </div>
  );
};

export default ArticleImage;
