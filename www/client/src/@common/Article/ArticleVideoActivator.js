import React from "react";

const ArticleVideoActivator = ({ isOpen, handelToggle }) => {
  if (isOpen) {
    return "";
  }

  return (
    <div className="article-media-holder" onClick={handelToggle}>
      <div className="article-video-activator" style={{ backgroundImage: "url(/assets/images/youtube.jpg)" }} />
    </div>
  );
};
export default ArticleVideoActivator;
