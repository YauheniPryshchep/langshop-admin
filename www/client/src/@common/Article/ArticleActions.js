import React from "react";

import { Button, ButtonGroup } from "@shopify/polaris";

const ArticleActions = ({ article }) => {
  const primaryAction = article.primaryText !== "" ? <Button external={true}>{article.primaryText}</Button> : "";

  const secondaryAction =
    article.secondaryText !== "" ? (
      <Button plain external={true}>
        {article.secondaryText}
      </Button>
    ) : (
      ""
    );

  return primaryAction !== "" || secondaryAction !== "" ? (
    <div className="article-actions-holder">
      <ButtonGroup>
        {primaryAction}
        {secondaryAction}
      </ButtonGroup>
    </div>
  ) : (
    ""
  );
};

export default ArticleActions;
