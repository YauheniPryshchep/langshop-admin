import React, { createElement } from "react";

import { Redirect, Route } from "react-router-dom";

export const NestedRoute = ({ component, exact, path, guards, redirect, routes, title, ...rest }) => {
  const RouteComponent = component;

  if (redirect) {
    return <Redirect to={redirect} />;
  }

  if (!RouteComponent) {
    return <Redirect to="/404" />;
  }

  if (title) {
    document.title = `${title} | LangShop Partners`;
  }

  return (
    <Route
      {...rest}
      path={path}
      exact={exact}
      render={props => {
        let Component = <RouteComponent {...props} title={title} routes={routes} key={-1} />;

        if (!guards) {
          return Component;
        }

        for (let i = guards.length - 1; i >= 0; i--) {
          Component = createElement(
            guards[i],
            {
              key: i,
            },
            [Component]
          );
        }

        return Component;
      }}
    />
  );
};
