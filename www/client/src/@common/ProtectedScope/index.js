import { profileScopes } from "@store/profile";
import { ProtectedScope } from "./ProtectedScope";
import { createStructuredSelector } from "reselect";
import { connect } from "react-redux";

const mapState = createStructuredSelector({
  profileScopes,
});

export default connect(mapState, undefined)(ProtectedScope);
