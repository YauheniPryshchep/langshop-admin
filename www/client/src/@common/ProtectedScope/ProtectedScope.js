import { hasEveryScope } from "@utils/scopes";

export const ProtectedScope = ({ children, profileScopes, allowed = [] }) => {
  if (!hasEveryScope(profileScopes, allowed)) {
    return null;
  }

  return children;
};
