import React, { useCallback, useEffect, useState } from "react";
import { Button, TextField } from "@shopify/polaris";
import { ChevronDownMinor, ChevronUpMinor } from "@shopify/polaris-icons";
import "./styles.scss";

export const ArticlePositionControl = ({ articlePosition, maxPosition, onPositionChange }) => {
  const [position, setPosition] = useState(articlePosition || maxPosition);

  useEffect(() => {
    setPosition(articlePosition || maxPosition);
  }, [articlePosition, maxPosition]);

  const handlePositionChange = useCallback(
    value => {
      if (value === "") {
        setPosition(position);
        return;
      }

      if (value < 1) {
        value = 1;
      }

      if (value > maxPosition) {
        value = maxPosition;
      }

      onPositionChange(parseInt(value, 10));
    },
    [maxPosition, setPosition, onPositionChange, position]
  );

  return (
    <div className={"Article-Position-Control"}>
      <Button
        size="slim"
        icon={ChevronUpMinor}
        disabled={position <= 1}
        onClick={() => onPositionChange((position || maxPosition) - 1)}
      />
      <TextField
        type="number"
        labelHidden
        label=""
        value={position}
        placeholder={maxPosition}
        onChange={handlePositionChange}
        min={1}
      />
      <Button
        size="slim"
        icon={ChevronDownMinor}
        disabled={position >= maxPosition}
        onClick={() => onPositionChange((position || maxPosition) + 1)}
      />
    </div>
  );
};
