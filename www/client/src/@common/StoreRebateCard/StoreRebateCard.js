import React from "react";
import { Card, TextContainer } from "@shopify/polaris";
import StoreDiscount from "@common/StoreDiscount";
import StoreTrial from "@common/StoreTrial";
import DemoStore from "@common/DemoStore";
import useHasScope from "@hooks/useHasScope";
import {
  SHOW_DEMO_STORES,
  SHOW_STORES_DISCOUNTS,
  SHOW_STORES_TRIALS,
  CREATE_ONE_TIME_PAYMENT_SCOPES,
} from "@utils/scopes";
import StoreOneTimePayment from "../StoreOneTimePayment";

export const StoreRebateCard = ({ domain, active }) => {
  const hasScope = useHasScope();

  const hasDemoScope = hasScope(SHOW_DEMO_STORES);
  const hasDiscountScope = hasScope(SHOW_STORES_DISCOUNTS);
  const hasTrialScope = hasScope(SHOW_STORES_TRIALS);
  const hasOneTimePaymentScope = hasScope(CREATE_ONE_TIME_PAYMENT_SCOPES);
  const hasRebutScope = hasDemoScope || hasDiscountScope || hasTrialScope;

  if (!hasRebutScope) {
    return null;
  }

  return (
    <Card title={"Rebate"} sectioned>
      <TextContainer spacing={"tight"}>
        {hasDiscountScope && <StoreDiscount domain={domain} />}
        {hasTrialScope && <StoreTrial domain={domain} />}
        {hasDemoScope && <DemoStore domain={domain} />}
        {hasOneTimePaymentScope && active ? <StoreOneTimePayment domain={domain} /> : null}
      </TextContainer>
    </Card>
  );
};
