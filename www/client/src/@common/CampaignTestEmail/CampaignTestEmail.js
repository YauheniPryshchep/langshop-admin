import React, {useCallback, useEffect, useState} from "react";
import {FormLayout, Modal, TextField} from "@shopify/polaris";
import useToggle from "../../@hooks/useToggle";
import {mailRegexp} from "../../@defaults/regexp";
import {useNotification} from "../../@hooks/useNotification";

export const CampaignTestEmail = ({isDisabled, template, fetchTestTemplateAction, handleClose, ...props}) => {
  const [email, setEmail] = useState("");
  const [isLoading, setLoading] = useToggle();
  const [error, setError] = useState("");
  const {handleNotify} = useNotification();

  const handleCloseForm = useCallback(async () => {
    await setEmail('');
    await setError('');
    handleClose()
  }, [setEmail, setError]);

  const sendTestEmail = useCallback(() => {
    setLoading();
    fetchTestTemplateAction(template, {to: email})
      .then(() => {
        setLoading();
        handleNotify("Test email successfully send");
        handleChangeEmail("");
        handleCloseForm();
      })
      .catch(() => {
        setLoading();
        handleChangeEmail("");
        handleCloseForm();
      });
  }, [template, email, setLoading, handleCloseForm]);

  useEffect(() => {
    if (email && !mailRegexp.test(email)) {
      setError("Invalid email");
    } else {
      setError("");
    }
  }, [email]);


  const handleChangeEmail = useCallback(
    value => {
      setEmail(value);
    },
    [setEmail]
  );

  return (
    <Modal
      {...props}
      title={"Test template"}
      onClose={handleCloseForm}
      secondaryActions={[
        {
          content: "Cancel",
          onAction: handleCloseForm,
        },
      ]}
      primaryAction={{
        content: "Send",
        onAction: sendTestEmail,
        loading: isLoading,
        disabled: !!error || !email || !template,
      }}
    >
      <Modal.Section>
        <FormLayout>
          <TextField
            type={"email"}
            error={error}
            label={"Test email"}
            value={email}
            disabled={isDisabled}
            onChange={handleChangeEmail}
          />
        </FormLayout>
      </Modal.Section>
    </Modal>
  );
};
