import { fetchTestTemplateAction } from "@store/templates";
import { CampaignTestEmail } from "./CampaignTestEmail";
import { connect } from "react-redux";

const mapDispatch = {
  fetchTestTemplateAction,
};

export default connect(null, mapDispatch)(CampaignTestEmail);
