import React from "react";
import DomainsInCampaignsPreview from "./Preview/DomainsInCampaignsPreview";
import RecipientsPreview from "./Preview/RecipientsPreview";

export const CampaignPreview = ({
  filters,
  domains,
  recipients,
  fetchRecipientsAction,
  resetRecipientsAction,
  total,
  isLoading,
  isFetched,
}) => {
  if (domains.length) {
    return <DomainsInCampaignsPreview domains={domains} />;
  }

  return (
    <RecipientsPreview
      recipients={recipients}
      fetchRecipientsAction={fetchRecipientsAction}
      isLoading={isLoading}
      isFetched={isFetched}
      filters={filters}
      resetRecipientsAction={resetRecipientsAction}
      total={total}
    />
  );
};
