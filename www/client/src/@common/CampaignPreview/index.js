import {
  recipients,
  fetchRecipientsAction,
  isLoading,
  isFetched,
  total,
  resetRecipientsAction,
} from "@store/recipients";
import { CampaignPreview } from "./CampaignPreview";
import { createStructuredSelector } from "reselect";
import { connect } from "react-redux";

const mapState = createStructuredSelector({
  recipients,
  isLoading,
  isFetched,
  total,
});

const mapDispatch = {
  fetchRecipientsAction,
  resetRecipientsAction,
};

export default connect(mapState, mapDispatch)(CampaignPreview);
