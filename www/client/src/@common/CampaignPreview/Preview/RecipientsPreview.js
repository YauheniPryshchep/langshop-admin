import React, {useCallback, useEffect, useMemo, useState} from "react";
import {Card, Icon, Link, ResourceItem, ResourceList, Stack, TextStyle} from "@shopify/polaris";
import {ExternalMinor} from "@shopify/polaris-icons";
import useCancelToken from "../../../@hooks/useCancelToken";
import useFetch from "../../../@hooks/useFetch";
import StoreStatus from "../../StoreStatus";
import Pagination from "../../../@components/Pagination";
import {CampaignFiltersAdapter} from "../../../@components/CampaignFiltersAdapter/CampaignFiltersAdapter";
import {numberFormat} from "../../../@defaults/numberFormat";
import {Col, Row} from "react-flexbox-grid";
import {Field} from "redux-form";

const limitOptions = [5, 10, 25, 100];

const RecipientsPreview = ({
                             recipients,
                             filters,
                             resetRecipientsAction,
                             fetchRecipientsAction,
                             total,
                             isLoading,
                             isDisabled,
                           }) => {

  const {page, limit, pages, onLimitChange} = useFetch({
    limitOptions,
    isLoading,
    total,
  });

  const [requestData, setRequestData] = useState({
    filters: filters["filters"],
    limit,
    page,
  });

  const onPreviousPage = useCallback(() => {
    if (isLoading) {
      return;
    }

    let page = requestData.page - 1;
    if (page > pages) {
      page = pages;
    }

    setRequestData({
      ...requestData,
      page: page,
    });
  }, [isLoading, requestData, setRequestData]);

  const onNextPage = useCallback(() => {
    if (isLoading) {
      return;
    }

    setRequestData({
      ...requestData,
      page: requestData.page + 1,
    });
  }, [isLoading, requestData, setRequestData]);

  useEffect(() => {
    if (requestData.limit !== limit || JSON.stringify(requestData.filters) !== JSON.stringify(filters["filters"])) {
      setRequestData(() => ({
        filters: filters["filters"],
        limit,
        page: 1,
      }));
    }
  }, [filters, limit]);

  useEffect(() => {
    fetchRecipientsAction(requestData);
  }, [requestData]);

  // On unmount
  useEffect(
    () => () => {
      resetRecipientsAction();
    },
    []
  );

  const pagination = useMemo(() => {
    const {page} = requestData;
    return {
      hasPrevious: pages > 1 && page > 1,
      hasNext: pages > 1 && page < pages,
      onPrevious: onPreviousPage,
      onNext: onNextPage,
    };
  }, [pages, requestData.page, onPreviousPage, onNextPage]);

  const perPage = useMemo(() => {
    return {
      limit: limit,
      options: limitOptions,
      onChange: onLimitChange,
    };
  }, [limit, onLimitChange]);

  const renderItem = item => {
    const {name} = item;

    return (
      <ResourceItem id={name}>
        <Row style={{alignItems: "center"}}>
          <Col xs={9}>
            <Link url={`//${name}`} external>
              <Stack wrap={false}>
                <p
                  style={{
                    width: "auto",
                    whiteSpace: "nowrap",
                    overflow: "hidden",
                    textOverflow: "ellipsis",
                  }}
                >
                  <TextStyle variation="strong">{name}</TextStyle>
                </p>
                <Icon source={ExternalMinor}/>
              </Stack>
            </Link>
          </Col>
          <Col xs={3} style={{textAlign: "right"}}>
            <StoreStatus store={item}/>
          </Col>
        </Row>
      </ResourceItem>
    );
  };

  const resourceListMarkup = (
    <ResourceList
      showHeader
      resourceName={{
        singular: "recipient",
        plural: "recipients",
      }}
      filterControl={
        <Field
          name={"recipients_filter"}
          component={CampaignFiltersAdapter}
          isDisabled={isDisabled}
          recipients_filter={filters}
        />
      }
      totalItemsCount={numberFormat(total)}
      loading={isLoading}
      items={recipients}
      renderItem={renderItem}
    />
  );

  const paginationMarkup =
    recipients && recipients.length ? (
      <Card.Section>
        <Pagination pagination={pagination} perPage={perPage}/>
      </Card.Section>
    ) : null;

  return (
    <Card>
      {resourceListMarkup}
      {paginationMarkup}
    </Card>
  );
};

export default RecipientsPreview;
