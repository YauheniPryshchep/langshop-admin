import React, {useMemo} from "react";
import {Card, ResourceItem, ResourceList, TextStyle, Link, Stack} from "@shopify/polaris";
import {ExternalMinor} from "@shopify/polaris-icons";
import Pagination from "../../../@components/Pagination";
import usePagination from "../../../@hooks/usePagination";
import {numberFormat} from "../../../@defaults/numberFormat";
import {Col, Row} from "react-flexbox-grid";
import {get} from "lodash";

const limitOptions = [5, 10, 25, 100];

const DomainsInCampaignsPreview = ({domains}) => {
  const {items, page, limit, pages, onLimitChange, onNextPage, onPreviousPage} = usePagination({items: domains});

  const pagination = useMemo(() => {
    return {
      hasPrevious: pages > 1 && page > 1,
      hasNext: pages > 1 && page < pages,
      onPrevious: onPreviousPage,
      onNext: onNextPage,
    };
  }, [pages, page, onPreviousPage, onNextPage]);

  const perPage = useMemo(() => {
    return {
      limit: limit,
      options: limitOptions,
      onChange: onLimitChange,
    };
  }, [limit, onLimitChange]);

  const renderItem = item => {
    const {domain} = item;

    return (
      <ResourceItem id={domain}>
        <Row style={{alignItems: "center"}}>
          <Col xs={12}>
            <Link url={`//${domain}`} external>
              <Stack wrap={false}>
                <Stack.Item fill>
                  {" "}
                  <p
                    style={{
                      width: "auto",
                      whiteSpace: "nowrap",
                      overflow: "hidden",
                      textOverflow: "ellipsis",
                    }}
                  >
                    <TextStyle variation="strong"> {domain} </TextStyle>
                  </p>
                </Stack.Item>
                <Stack.Item>
                  <ExternalMinor fill={"#2c6ecb"} width={"2rem"} height={"2rem"}/>
                </Stack.Item>
              </Stack>
            </Link>
          </Col>
        </Row>
      </ResourceItem>
    );
  };

  const resourceListMarkup = (
    <ResourceList
      showHeader
      resourceName={{
        singular: "domain",
        plural: "domains",
      }}
      totalItemsCount={numberFormat(domains.length)}
      items={items}
      renderItem={renderItem}
    />
  );

  const paginationMarkup =
    domains && domains.length ? (
      <Card.Section>
        <Pagination pagination={pagination} perPage={perPage}/>
      </Card.Section>
    ) : null;

  return (
    <Card>
      {resourceListMarkup}
      {paginationMarkup}
    </Card>
  );
};

export default DomainsInCampaignsPreview;
