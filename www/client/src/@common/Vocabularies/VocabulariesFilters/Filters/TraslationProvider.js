import React, { useEffect, useMemo } from "react";
import { FormLayout, Select } from "@shopify/polaris";
import { translatioAgenceClients, translationClients } from "@defaults/translation-clients";

const TranslationProvider = ({ filters, onChange }) => {
  const optionsSource = useMemo(
    () => [
      { label: "Select charge status", value: "" },
      { label: "Machine translate", value: "machine" },
      { label: "Agency translate", value: "agency" },
    ],
    []
  );

  const valueSource = useMemo(() => {
    let source = filters.find(filter => filter.key === "source");

    if (!source) {
      return "";
    }

    return source.value.toString();
  }, [filters]);

  const options = useMemo(
    () =>
      valueSource === "agency"
        ? [{ label: "Select charge status", value: "" }, ...translatioAgenceClients]
        : [{ label: "Select charge status", value: "" }, ...translationClients],
    [valueSource]
  );

  const value = useMemo(() => {
    let source = filters.find(filter => filter.key === "sourceId");

    if (!source) {
      return "";
    }

    return source.value.toString();
  }, [filters]);

  return (
    <FormLayout>
      <Select
        label={"Source"}
        value={valueSource}
        options={optionsSource}
        onChange={value => {
          onChange({
            key: "source",
            comparator: "equal",
            value: value,
          });
          onChange({
            key: "sourceId",
            comparator: "equal",
            value: "",
          });
        }}
      />
      <Select
        label={"Translation Providers"}
        value={value}
        options={options}
        disabled={!valueSource}
        onChange={value =>
          onChange({
            key: "sourceId",
            comparator: "equal",
            value: parseInt(value),
          })
        }
      />
    </FormLayout>
  );
};

export default TranslationProvider;
