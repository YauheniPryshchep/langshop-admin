import React, { useMemo } from "react";
import { Select } from "@shopify/polaris";

const Source = ({ filters, onChange }) => {
  const options = useMemo(
    () => [
      { label: "Select charge status", value: "" },
      { label: "Machine translate", value: "machine" },
      { label: "Agency translate", value: "agency" },
    ],
    []
  );

  const value = useMemo(() => {
    let source = filters.find(filter => filter.key === "source");

    if (!source) {
      return "";
    }

    return source;
  }, [filters]);

  return (
    <Select
      label={"Source"}
      labelHidden={true}
      value={value}
      options={options}
      onChange={value =>
        onChange({
          key: "source",
          comparator: "equal",
          value: value,
        })
      }
    />
  );
};

export default Source;
