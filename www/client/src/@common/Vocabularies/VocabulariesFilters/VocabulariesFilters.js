import React, { useCallback, useEffect, useMemo, useState } from "react";
import { Filters } from "@shopify/polaris";
import { firstCapitalize } from "../../../@defaults/firstCapitalize";
import TranslationProvider from "./Filters/TraslationProvider";
import { translationClients, translatioAgenceClients } from "../../../@defaults/translation-clients";
import { debounce, get } from "lodash";

const FILTERS_MESSAGE = {
  sourceId: (key, comparator, value, additional) =>
    value
      ? `Translation provider is ${
          (additional === "agency" ? translatioAgenceClients : translationClients).find(client => client.value == value)
            .label
        }`
      : "",
  source: (key, comparator, value) => {
    return `${firstCapitalize(key.split("_").join(" "))}: ${comparator.split("_").join(" ")} ${value
      .split("_")
      .join(" ")}`;
  },
  original: (key, comparator, value) => {
    return `${firstCapitalize(key.split("_").join(" "))}: ${comparator.split("_").join(" ")} ${value
      .split("_")
      .join(" ")}`;
  },
};

export const VocabulariesFilters = ({ filters, onChange }) => {
  const [search, setSearch] = useState("");
  useEffect(() => {
    const filter = filters.find(filter => filter.key === "original");
    setSearch(get(filter, "value", ""));
  }, [filters]);

  const handleChange = useCallback(
    value => {
      let cloneFilters = [...filters];

      const filter = filters.find(f => f.key === value.key);

      if (!filter) {
        cloneFilters = [
          ...cloneFilters,
          {
            ...value,
          },
        ];
      } else if (!value.value) {
        cloneFilters = [...cloneFilters.filter(f => f.key !== value.key)];
      } else {
        cloneFilters = [
          ...cloneFilters.map(f => {
            if (f.key === value.key) {
              return {
                ...value,
              };
            }
            return { ...f };
          }),
        ];
      }
      onChange(cloneFilters);
    },
    [filters, onChange]
  );

  const handleFilterRemove = useCallback(() => {
    onChange([]);
  }, [filters, onChange]);

  const handleFiltersClearAll = useCallback(() => {
    onChange([]);
  }, [onChange]);

  const options = [
    {
      key: "sourceId",
      label: "Translation provider",
      filter: <TranslationProvider filters={filters} onChange={handleChange} onRemove={handleFiltersClearAll} />,
      shortcut: true,
    },
  ];

  const debounceSearchFilter = useCallback(
    debounce(value => handleChange({ key: "original", comparator: "contains", value }), 500),
    [handleChange]
  );

  const onSearchChange = useCallback(
    value => {
      setSearch(value);
      debounceSearchFilter(value);
    },
    [setSearch, debounceSearchFilter]
  );

  const handleQueryValueRemove = useCallback(() => {
    onSearchChange("");
  }, [onSearchChange]);

  const appliedFilters = useMemo(() => {
    let appliedFilters = [];
    for (let i = 0, length = filters.length; i < length; i++) {
      const filter = filters[i];
      const source = filters.find(filter => filter.key === "source");
      appliedFilters.push({
        key: filter.key,
        label: FILTERS_MESSAGE[filter.key](filter.key, filter.comparator, filter.value, get(source, "value", "")),
        onRemove: handleFilterRemove,
      });
    }
    return appliedFilters;
  }, [filters]);

  return (
    <Filters
      filters={options}
      appliedFilters={appliedFilters}
      queryValue={search}
      onQueryChange={onSearchChange}
      onQueryClear={handleQueryValueRemove}
      onClearAll={handleFiltersClearAll}
    />
  );
};
