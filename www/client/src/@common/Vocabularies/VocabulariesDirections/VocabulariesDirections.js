import React from "react";
import { Layout } from "@shopify/polaris";
import { languages } from "@store/config/languages";
import LanguagesSelect from "./common/LanguagesSelect";

export const VocabulariesDirections = ({ from, to, onChange }) => {
  return (
    <div style={{ marginBottom: "2rem" }}>
      <Layout>
        <Layout.Section oneHalf>
          <LanguagesSelect selected={from} options={languages} onChange={value => onChange("from", value)} />
        </Layout.Section>
        <Layout.Section oneHalf>
          <LanguagesSelect
            selected={to || ""}
            options={languages.filter(lang => lang.code !== from)}
            onChange={value => onChange("to", value)}
          />
        </Layout.Section>
      </Layout>
    </div>
  );
};
