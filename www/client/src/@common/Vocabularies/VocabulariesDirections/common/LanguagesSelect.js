import React, { useMemo } from "react";
import { Select } from "@shopify/polaris";
import { LanguagesFlagIcon } from "@components/LanguagesFlagIcon/LanguagesFlagIcon";
import { get } from "lodash";

const LanguagesSelect = ({ selected, options, onChange }) => {
  const items = useMemo(() => {
    return [
      { label: "Select language", value: "" },
      ...options.map(option => {
        const title = get(option, "title", "");
        return {
          label: title,
          value: option.code,
          prefix: <LanguagesFlagIcon code={option.code} />,
        };
      }),
    ];
  }, [options]);

  return (
    <Select label={selected} labelHidden={true} options={items} value={selected} onChange={value => onChange(value)} />
  );
};

export default LanguagesSelect;
