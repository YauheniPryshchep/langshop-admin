import React from "react";
import { Button, Card, List, TextContainer, TextStyle } from "@shopify/polaris";
import { VariablesModal } from "../../@components/VariablesModal/VariablesModal";
import useActive from "../../@hooks/useActive";

export const CampaignVariables = () => {
  const [variableModalOpened, openVariablesModal, closeVariablesModal] = useActive();

  return (
    <Card title={"Variables"}>
      <Card.Section>
        <TextContainer>You can use MailGun variables for your templates. The available variables are:</TextContainer>
        <div style={{ marginTop: "2rem" }} />
        <List>
          <List.Item>
            <TextStyle variation="code"> {"{{domain}}"} </TextStyle>
          </List.Item>
          <List.Item>
            {" "}
            <TextStyle variation="code"> {"{{store_name}}"} </TextStyle>
          </List.Item>
          <List.Item>
            {" "}
            <TextStyle variation="code"> {"{{email}}"} </TextStyle>
          </List.Item>
        </List>
        <div style={{ marginTop: "2rem" }} />

        <TextContainer>
          <Button plain onClick={openVariablesModal}>
            View all variables
          </Button>
        </TextContainer>
      </Card.Section>
      <VariablesModal
        title="Variables"
        open={variableModalOpened}
        onCancel={closeVariablesModal}
        onConfirm={closeVariablesModal}
      />
    </Card>
  );
};
