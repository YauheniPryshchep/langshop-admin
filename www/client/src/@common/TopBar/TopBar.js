import React, { useCallback, useEffect, useMemo, useState } from "react";
import { ActionList, Card, SkeletonBodyText, TopBar as PolarisTopBar } from "@shopify/polaris";
import useToggle from "@hooks/useToggle";

export const TopBar = ({
  toggleMobileNavigationActive,
  userMenuActions,
  name,
  detail,
  photo,
  handleSearchChange,
  handleSearchClear,
  searchResultsEmptyAction,
  searchResultsItemAction,
  isSearched,
  isSearching,
  searchResults,
}) => {
  const [userMenuActive, toggleUserMenuActive] = useToggle();
  const [searchValue, setSearchValue] = useState("");

  useEffect(() => {
    onSearchDismiss();
  }, [name, detail]);

  const isSearchActive = useMemo(() => isSearched || isSearching, [isSearched, isSearching]);

  const onSearchDismiss = useCallback(() => {
    setSearchValue("");
    handleSearchClear();
  }, [handleSearchClear]);

  const onSearchChange = useCallback(
    value => {
      value = value.trim();
      setSearchValue(value);
      handleSearchClear();

      if (value) {
        handleSearchChange(value);
      }
    },
    [handleSearchChange, handleSearchClear]
  );

  const initials = useMemo(() => {
    if (!name) {
      return null;
    }

    return name.charAt(0).toUpperCase();
  }, [name]);

  const userMenuMarkup = (
    <PolarisTopBar.UserMenu
      name={name}
      initials={initials}
      detail={detail}
      actions={userMenuActions}
      open={userMenuActive}
      onToggle={toggleUserMenuActive}
      avatar={photo}
    />
  );

  const searchFieldMarkup = (
    <PolarisTopBar.SearchField onChange={onSearchChange} value={searchValue} placeholder="Search store" />
  );

  const resultItems = useMemo(() => {
    if (!searchResults.length) {
      const emptyAction = searchResultsEmptyAction(searchValue);

      if (!emptyAction.onAction) {
        return [emptyAction];
      }

      return [
        {
          ...emptyAction,
          onAction: () => {
            onSearchDismiss();
            emptyAction.onAction();
          },
        },
      ];
    }

    return searchResults.map(item => {
      const itemAction = searchResultsItemAction(item);

      if (!itemAction.onAction) {
        return itemAction;
      }

      return {
        ...itemAction,
        onAction: () => {
          onSearchDismiss();
          itemAction.onAction();
        },
      };
    });
  }, [searchValue, searchResults, searchResultsEmptyAction, searchResultsItemAction, onSearchDismiss]);

  const searchResultsMarkup = (
    <Card>
      {!isSearching ? (
        <ActionList items={resultItems} />
      ) : (
        <Card.Section>
          <SkeletonBodyText />
        </Card.Section>
      )}
    </Card>
  );

  return (
    <PolarisTopBar
      showNavigationToggle
      searchResultsVisible={isSearchActive}
      searchField={searchFieldMarkup}
      searchResults={searchResultsMarkup}
      onSearchResultsDismiss={onSearchDismiss}
      userMenu={userMenuMarkup}
      onNavigationToggle={toggleMobileNavigationActive}
    />
  );
};
