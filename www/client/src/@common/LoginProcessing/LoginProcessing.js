import React from "react";
import { Frame, Loading } from "@shopify/polaris";

export const LoginProcessing = () => {
  return (
    <Frame>
      <Loading />
    </Frame>
  );
};
