import React, {useMemo} from "react";
import {Badge} from "@shopify/polaris";
import {PLANS, WITHOUT_PLAN} from "@defaults/constants";
import get from "lodash/get";

export const LangShopPlan = ({store, badge = true}) => {
  const subscription = useMemo(() => {
    return get(store, "subscription", {});
  }, [store]);

  const planId = useMemo(() => {
    return get(subscription, "plan_id", 0);
  }, [subscription]);


  if (badge) {

    if (!planId) {
      return <Badge>{PLANS[WITHOUT_PLAN]}</Badge>;
    }

    return <Badge>{PLANS[planId]}</Badge>;
  }
  if (!planId) {
    return PLANS[WITHOUT_PLAN];
  }

  return PLANS[planId];
};
