import React, { useCallback, useEffect, useMemo, useState } from "react";
import {
  Avatar,
  Badge,
  Card,
  DataTable,
  DisplayText,
  SkeletonBodyText,
  Stack,
  TextStyle,
  Tooltip,
} from "@shopify/polaris";
import useCancelToken from "@hooks/useCancelToken";
import Pagination from "@components/Pagination";
import { PLANS } from "@defaults/constants";
import moment from "moment-timezone";
import { get } from "lodash";
import includes from "lodash/includes";

const PER_PAGE = 20;
const HISTORY_TYPES = {
  "1": ({ campaign_id }) => `Add To Campaign #${campaign_id}`,
  "2": () => `Unsubscribe`,
  "3": ({ charge_id }) => `Charge #${charge_id}`,
  "4": () => `Assign to referrals`,
  "7": ({ planId }) => `Change Plan to ${PLANS[planId]}`,
  "8": ({ price }) => `Set Enterprise price to $${price}`,

  installed: () => "Installed",
  uninstalled: () => "Uninstalled",
  "referral-accepted": () => "Accepted referral",
  "referral-declined": () => "Declined referral",
  "referral-cancelled": () => "Cancelled referral",
};

export const StoreHistoryCard = ({
  domain,
  storeHistory,
  storeCharge,
  total,
  fetchStoreHistoryAction,
  isFetched,
  isLoading,
  resetStoreHistoryAction,
  fetchStoreChargeAction,
  resetStoreChargeAction,
  storeChargeIsFetched,
  storeChargeIsLoading,
  store,
}) => {
  const loading = useMemo(() => isLoading || !isFetched || !storeChargeIsFetched || storeChargeIsLoading, [
    isLoading,
    isFetched,
    storeChargeIsFetched,
    storeChargeIsLoading,
  ]);
  const [cancelToken, cancelRequests] = useCancelToken();
  const [page, setPage] = useState(1);

  useEffect(() => {
    fetchStoreHistoryAction(
      domain,
      {
        limit: PER_PAGE,
        page: page,
      },
      cancelToken
    );
  }, [domain, page, store]);

  useEffect(() => {
    fetchStoreChargeAction(domain, {}, cancelToken);
  }, [domain]);

  // On unmount
  useEffect(
    () => () => {
      cancelRequests();
      resetStoreHistoryAction();
      resetStoreChargeAction();
    },
    []
  );

  const onPreviousPage = useCallback(() => {
    if (isLoading) {
      return;
    }

    setPage(page);
  }, [isLoading, page, setPage]);

  const onNextPage = useCallback(() => {
    if (isLoading) {
      return;
    }

    setPage(page + 1);
  }, [isLoading, page, setPage]);

  const pages = useMemo(() => {
    return Math.ceil(total / PER_PAGE) || 1;
  }, [total]);

  const getBadgeStatus = useCallback(type => {
    if (["1", "3", "4", "installed", "referral-accepted", "7", "8"].includes(type)) {
      return "success";
    }
    return "warning";
  }, []);

  const getChargeStatus = useCallback(status => {
    if (status === "pending") {
      return "info";
    }

    if (["accepted, active"].includes(status)) {
      return "success";
    }
    return "warning";
  }, []);

  const getReferralDetails = useCallback(event => {
    return (
      <Tooltip key={`reason` - event.id} content={get(event, "event_data.reason", "")}>
        <p
          style={{
            width: "10rem",
            whiteSpace: "nowrap",
            overflow: "hidden",
            textOverflow: "ellipsis",
            cursor: "pointer",
          }}
        >
          {get(event, "event_data.reason", "")}
        </p>
      </Tooltip>
    );
  }, []);

  const getPlanDetails = useCallback(event => {
    const authorName = get(event, "event_data.author", "");
    const authorPhoto = get(event, "event_data.author_photo", null);
    const reason = get(event, "event_data.reason", "");
    const content = `${authorName}. Reason: ${reason}`;

    return (
      <Tooltip key={`reason` - event.id} content={content}>
        <p
          style={{
            width: "10rem",
            whiteSpace: "nowrap",
            overflow: "hidden",
            textOverflow: "ellipsis",
            cursor: "pointer",
          }}
        >
          <Stack wrap={false} alignment={"center"}>
            <Stack.Item>
              <Avatar source={authorPhoto} name={authorName} />
            </Stack.Item>
            <Stack.Item>{content}</Stack.Item>
          </Stack>
        </p>
      </Tooltip>
    );
  }, []);

  const getDetails = useCallback(
    event => {
      if (event.type === "3") {
        let charge = storeCharge.find(i => i.charge_id === get(event, "event_data.charge_id", 0));
        let status = get(charge, "status", "");
        return <Badge status={getChargeStatus(status)}>{status}</Badge>;
      }

      if (includes(["referral-accepted", "referral-declined", "referral-cancelled"], event.type)) {
        return getReferralDetails(event);
      }

      if (includes(["7", "8"], event.type)) {
        return getPlanDetails(event);
      }

      return "";
    },
    [storeCharge, getReferralDetails, getPlanDetails]
  );

  const getBadge = useCallback(({ type, event_data }) => {
    return <Badge status={getBadgeStatus(type)}>{HISTORY_TYPES[type](event_data)}</Badge>;
  }, []);

  const rows = useMemo(() => {
    return storeHistory.map(event => {
      return [moment.unix(event.timestamp).format("MMMM Do YYYY, h:mm a"), getBadge(event), getDetails(event)];
    });
  }, [storeHistory, getDetails, storeCharge, getBadge]);

  const pagination = useMemo(() => {
    return {
      hasPrevious: pages > 1 && page > 1,
      hasNext: pages > 1 && page < pages,
      onPrevious: onPreviousPage,
      onNext: onNextPage,
    };
  }, [pages, page, onPreviousPage, onNextPage]);

  if (loading) {
    return (
      <Card sectioned>
        <SkeletonBodyText lines={5} />
      </Card>
    );
  }

  const footerContentMarkup =
    pages > 1 ? (
      <Stack alignment="center" distribution="equalSpacing">
        <Stack.Item>
          Showing {page} of {pages} pages
        </Stack.Item>
        <Stack.Item>
          <Pagination pagination={pagination} />
        </Stack.Item>
      </Stack>
    ) : null;

  const dataTableMarkup = rows.length ? (
    <DataTable
      columnContentTypes={["text", "text", "text"]}
      headings={[<strong key={0}>Date</strong>, <strong key={1}>Event</strong>, <strong key={2}>Details</strong>]}
      footerContent={footerContentMarkup}
      rows={rows}
    />
  ) : (
    <div style={{ padding: "30px 0" }}>
      <Stack distribution="center">
        <DisplayText size="medium">No store events</DisplayText>
      </Stack>
      <Stack distribution="center">
        <DisplayText size={"small"}>
          <TextStyle variation="subdued">There are no events related to this store</TextStyle>
        </DisplayText>
      </Stack>
    </div>
  );

  return <Card sectioned>{dataTableMarkup}</Card>;
};
