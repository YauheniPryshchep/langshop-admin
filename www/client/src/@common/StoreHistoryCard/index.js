import {
  storeHistory,
  total,
  fetchStoreHistoryAction,
  isFetched,
  isLoading,
  resetStoreHistoryAction,
} from "@store/store-history";
import {
  storeCharge,
  fetchStoreChargeAction,
  isFetched as storeChargeIsFetched,
  isLoading as storeChargeIsLoading,
  resetStoreChargeAction,
} from "@store/store-charge";
import { StoreHistoryCard } from "./StoreHistoryCard";
import { createStructuredSelector } from "reselect";
import { connect } from "react-redux";

const mapState = createStructuredSelector({
  storeHistory,
  total,
  isFetched,
  isLoading,
  storeCharge,
  storeChargeIsFetched,
  storeChargeIsLoading,
});

const mapDispatch = {
  fetchStoreHistoryAction,
  resetStoreHistoryAction,
  fetchStoreChargeAction,
  resetStoreChargeAction,
};

export default connect(mapState, mapDispatch)(StoreHistoryCard);
