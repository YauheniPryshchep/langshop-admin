import React, {useEffect, useMemo} from "react";
import {Avatar, Stack, TextContainer} from "@shopify/polaris";
import useHasScope from "../../@hooks/useHasScope";
import {SHOW_USERS} from "../../@utils/scopes";

export const CampaignAuthor = ({author_id, size = "large", user, profile, fetchUserAction}) => {
  const hasScope = useHasScope();

  const author = useMemo(() => {
    if (!author_id || author_id === profile.id) {
      return profile;
    }
    return user;
  }, [author_id, user, profile]);

  const source = useMemo(() => {
    return author.photo || null;
  }, [author]);

  useEffect(() => {
    if (profile.id !== author.id && hasScope(SHOW_USERS)) {
      fetchUserAction(author_id);
    }
  }, [profile, author, author_id]);

  return (
    <Stack vertical={true} alignment={"center"}>
      <Avatar customer={true} size={size} source={source}/>
      <TextContainer>{author.name}</TextContainer>
    </Stack>
  );
};
