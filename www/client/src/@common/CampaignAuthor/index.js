import { profile } from "@store/profile";
import { initialValues as user, fetchUserAction, isLoading, isFetched } from "@store/user";
import { CampaignAuthor } from "./CampaignAuthor";
import { createStructuredSelector } from "reselect";
import { connect } from "react-redux";

const mapState = createStructuredSelector({
  profile,
  user,
  isLoading,
  isFetched,
});

const mapDispatch = {
  fetchUserAction,
};

export default connect(mapState, mapDispatch)(CampaignAuthor);
