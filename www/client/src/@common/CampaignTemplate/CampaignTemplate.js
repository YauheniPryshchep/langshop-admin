import React, {useEffect, useMemo} from "react";
import {Link} from "@shopify/polaris";
import {SelectFieldAdapter} from "@components/SelectFieldAdapter/SelectFieldAdapter";
import {Field} from "redux-form";

export const CampaignTemplate = ({
                                   templates,
                                   fetchTemplatesAction,
                                   isLoading,
                                   isFetched,
                                   isDisabled,
                                   mailgunDomain,
                                   template,
                                   validate,
                                 }) => {

  useEffect(() => {
    if (!templates.length) {
      fetchTemplatesAction();
    }
  }, [templates]);

  const options = useMemo(() => {
    return [
      {label: "Select template", value: ""},
      ...templates.map(template => ({
        value: template.name,
        label: template.name,
      })),
    ];
  }, [templates]);

  return (
    <Field
      name={"template"}
      label={"Template"}
      disabled={isLoading || !isFetched || isDisabled}
      options={options}
      helpText={
        <Link
          url={`https://app.mailgun.com/app/sending/domains/${mailgunDomain}/templates/details/${template}`}
          external
        >
          Preview template
        </Link>
      }
      component={SelectFieldAdapter}
      validate={validate.template}
    />
  );
};
