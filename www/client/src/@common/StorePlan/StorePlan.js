import React, { useMemo } from "react";
import { Badge } from "@shopify/polaris";
import { storePlan } from "@utils/stores";

export const StorePlan = ({ store, badge = true }) => {
  const plan = useMemo(() => storePlan(store), [store]);

  if (!plan) {
    return null;
  }

  if (badge) {
    return <Badge>{plan}</Badge>;
  }

  return plan;
};
