import { fetchNewsAction, resetNewsAction, news, isFetched, isLoading, total } from "@store/news";
import { NewsPreview } from "./NewsPreview";
import { createStructuredSelector } from "reselect";
import { connect } from "react-redux";

const mapState = createStructuredSelector({
  news,
  isFetched,
  isLoading,
  total,
});

const mapDispatch = {
  fetchNewsAction,
  resetNewsAction,
};

export default connect(mapState, mapDispatch)(NewsPreview);
