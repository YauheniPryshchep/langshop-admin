import React, { useEffect, useMemo, useState } from "react";
import { Card, SkeletonBodyText } from "@shopify/polaris";
import useCancelToken from "@hooks/useCancelToken";
import Article from "@common/Article";
import { scrollTo } from "@utils/scroll";
import ArticlePositionControl from "@common/ArticlePositionControl";
import { debounce } from "lodash";
import "./styles.scss";

export const NewsPreview = ({
  appSuffix,
  fetchNewsAction,
  currentArticle,
  news,
  isFetched,
  isLoading,
  total,
  onPositionChange,
}) => {
  const [cancelToken] = useCancelToken();
  const loading = useMemo(() => isLoading || !isFetched, [isLoading, isFetched]);
  const newsPreviewRef = React.createRef();
  const currentArticleRef = React.createRef();
  const [articles, setArticles] = useState([]);

  useEffect(() => {
    fetchNewsAction(
      {
        limit: 9999,
        page: 0,
        appSuffix,
      },
      cancelToken
    );
  }, [appSuffix]);

  const maxPosition = useMemo(() => (currentArticle.id ? total : total + 1), [currentArticle, total]);

  const mergeArticles = (items, current) => {
    return [...items.filter(item => item.id !== current.id), current].sort(
      (a, b) => Number(b.sticky) - Number(a.sticky) || a.position - b.position
    );
  };

  useEffect(() => {
    if (loading) {
      return;
    }

    setArticles(mergeArticles(news, currentArticle));
  }, [loading, news]);

  useEffect(() => {
    const article = articles.find(article => article.id === currentArticle.id);
    if (!article) {
      return;
    }

    let items = articles;
    let position = article.position || maxPosition;

    if (currentArticle.position > position) {
      items = items.map(item => {
        if (item.position <= currentArticle.position && item.position > position) {
          item.position--;
        }

        return item;
      });
    } else if (currentArticle.position < position) {
      items = items.map(item => {
        if (item.position >= currentArticle.position && item.position < position) {
          item.position++;
        }

        return item;
      });
    }

    setArticles(mergeArticles(items, currentArticle));
  }, [currentArticle, maxPosition]);

  useEffect(
    debounce(() => {
      if (!news.length) {
        return;
      }

      if (!newsPreviewRef.current || !currentArticleRef.current) {
        return;
      }

      const pPos = newsPreviewRef.current.getBoundingClientRect(),
        cPos = currentArticleRef.current.getBoundingClientRect();

      scrollTo(newsPreviewRef.current, cPos.top - pPos.top + currentArticleRef.current.parentNode.scrollTop - 3);
    }, 200),
    [newsPreviewRef, currentArticleRef, news]
  );

  if (loading) {
    return (
      <div>
        {Array.apply(null, Array(10)).map((_e, i) => (
          <Card key={i} sectioned>
            <SkeletonBodyText lines={10} />
          </Card>
        ))}
      </div>
    );
  }

  return (
    <div className={"News-Preview"} ref={newsPreviewRef}>
      {articles.map(article => (
        <div
          key={article.id}
          ref={article.id === currentArticle.id ? currentArticleRef : null}
          className={`News-Preview__Card ${article.id === currentArticle.id ? "News-Preview__Card--active" : ""}`}
        >
          <Article article={article} />
          {article.id === currentArticle.id && (
            <ArticlePositionControl
              articlePosition={currentArticle.position}
              maxPosition={maxPosition}
              onPositionChange={onPositionChange}
            />
          )}
        </div>
      ))}
    </div>
  );
};
