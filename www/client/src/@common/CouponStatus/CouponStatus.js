import React, { useCallback } from "react";
import { Badge } from "@shopify/polaris";
import { BLOCKED_STATUS, COUPONS_STATUS, DRAFT_STATUS } from "../../@routes/MainLayout/AppFrame/Coupons/Coupons";

export const CouponStatus = ({ status }) => {
  const getBadgeType = useCallback(status => {
    if (status === DRAFT_STATUS) {
      return {
        status: "info",
      };
    }

    if (status === BLOCKED_STATUS) {
      return {
        status: "warning",
      };
    }

    return {
      status: "success",
    };
  }, []);

  return <Badge {...getBadgeType(status)}>{COUPONS_STATUS[status]}</Badge>;
};
