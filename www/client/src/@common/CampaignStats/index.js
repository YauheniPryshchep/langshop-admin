import { stats, isLoading, isFetched, fetchCampaignStatsAction, resetCampaignStatsAction } from "@store/campaign-stats";
import { CampaignStats } from "./CampaignStats";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";

const mapState = createStructuredSelector({
  stats,
  isLoading,
  isFetched,
});

const mapDispatch = {
  fetchCampaignStatsAction,
  resetCampaignStatsAction,
};

export default connect(mapState, mapDispatch)(CampaignStats);
