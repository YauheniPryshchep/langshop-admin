import React, { useCallback, useEffect, useMemo } from "react";
import { Card, List, SkeletonBodyText, TextContainer, TextStyle } from "@shopify/polaris";
import { get } from "lodash";

export const CampaignStats = ({
  stats,
  isLoading,
  isFetched,
  fetchCampaignStatsAction,
  resetCampaignStatsAction,
  campaignId,
  profile,
}) => {
  const loading = useMemo(() => isLoading || !isFetched, [isLoading, isFetched]);

  useEffect(() => {
    if (campaignId) {
      fetchCampaignStatsAction(campaignId);
    }
    return () => {
      resetCampaignStatsAction();
    };
  }, [campaignId]);

  const getStatsCount = useCallback(
    (key, subKey = "") => {
      let count = 0;
      let items = get(stats, "items", []);
      for (let i = 0; i < items.length; i++) {
        if (key === "failed") {
          count += items[i][key][subKey]["total"];
        } else {
          count += items[i][key]["total"];
        }
      }
      return count;
    },
    [stats]
  );

  const campaignLink = useMemo(() => {
    const {
      settings: { mailgunDomain },
    } = profile;
    return `https://app.mailgun.com/app/sending/domains/${mailgunDomain}/analytics/campaign-${campaignId}@${mailgunDomain}/overview`;
  }, [campaignId, profile]);

  const items = useMemo(() => {
    return [
      {
        term: "Delivered",
        description: loading ? <SkeletonBodyText /> : getStatsCount("delivered"),
      },
      {
        term: "Failed Permanent",
        description: loading ? <SkeletonBodyText /> : getStatsCount("failed", "permanent"),
      },
      {
        term: "Failed Temporary",
        description: loading ? <SkeletonBodyText /> : getStatsCount("failed", "temporary"),
      },
    ];
  }, [stats, loading, getStatsCount]);

  return (
    <Card
      title={"Campaign base Analytics"}
      actions={[
        {
          content: "MailGun Analitics",
          url: campaignLink,
          external: true,
        },
      ]}
    >
      <Card.Section>
        <List spacing={"loose"} items={items}>
          {items.map(({ description, term }, index) => {
            return (
              <List.Item key={index}>
                <TextContainer>
                  <TextStyle variation={"strong"}>{term}:</TextStyle>
                  {description}
                </TextContainer>
              </List.Item>
            );
          })}
        </List>
      </Card.Section>
    </Card>
  );
};
