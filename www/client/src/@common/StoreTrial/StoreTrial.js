import React, { useCallback, useEffect, useMemo } from "react";
import { Button, SkeletonBodyText, Stack } from "@shopify/polaris";
import useActive from "@hooks/useActive";
import CreateStoreTrialModal from "@common/Modals/CreateStoreTrialModal";
import ConfirmationModal from "@components/ConfirmationModal";
import useCancelToken from "@hooks/useCancelToken";

export const StoreTrial = ({
  domain,
  isFetched,
  isLoading,
  storeTrial,
  fetchStoreTrialAction,
  createStoreTrialAction,
  removeStoreTrialAction,
  resetStoreTrialAction,
}) => {
  const loading = useMemo(() => isLoading || !isFetched, [isLoading, isFetched]);
  const [cancelToken, cancelRequests] = useCancelToken();

  const [createModalOpened, openCreateModal, closeCreateModal] = useActive();
  const [removeModalOpened, openRemoveModal, closeRemoveModal] = useActive();

  useEffect(() => {
    fetchStoreTrialAction(domain, cancelToken).catch(() => {});
  }, [domain]);

  // On unmount
  useEffect(
    () => () => {
      cancelRequests();
      resetStoreTrialAction();
    },
    []
  );

  const createStoreTrial = useCallback(async store => {
    try {
      await createStoreTrialAction(store);
    } catch (e) {
      openCreateModal();
      return;
    }

    closeCreateModal();
  }, []);

  const removeStoreTrial = useCallback(() => {
    closeRemoveModal();
    removeStoreTrialAction(domain);
  }, [domain]);

  if (loading) {
    return <SkeletonBodyText lines={1} />;
  }

  const valueMarkup = (
    <Stack.Item>
      Trial: <strong>{storeTrial ? `${storeTrial.value} days` : "-"}</strong>
    </Stack.Item>
  );

  const actionMarkup = (
    <Stack.Item>
      {storeTrial ? (
        <Button onClick={openRemoveModal} plain destructive>
          Remove
        </Button>
      ) : (
        <Button onClick={openCreateModal} plain primary>
          Create
        </Button>
      )}
    </Stack.Item>
  );

  const modalMarkup = storeTrial ? (
    <ConfirmationModal
      destructive
      title="Remove store trial"
      content="Are you sure you want to delete the store trial?"
      confirm="Remove"
      open={removeModalOpened}
      onCancel={closeRemoveModal}
      onConfirm={removeStoreTrial}
    />
  ) : (
    <CreateStoreTrialModal
      open={createModalOpened}
      onClose={closeCreateModal}
      loading={loading}
      handleCreate={createStoreTrial}
      readOnly={["name"]}
      initialValues={{
        name: domain,
      }}
    />
  );

  return (
    <div>
      <Stack distribution={"equalSpacing"}>
        {valueMarkup}
        {actionMarkup}
      </Stack>
      {modalMarkup}
    </div>
  );
};
