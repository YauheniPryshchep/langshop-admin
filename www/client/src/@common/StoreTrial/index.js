import {
  storeTrial,
  isFetched,
  isLoading,
  fetchStoreTrialAction,
  createStoreTrialAction,
  removeStoreTrialAction,
  resetStoreTrialAction,
} from "@store/store-trial";
import { StoreTrial } from "./StoreTrial";
import { createStructuredSelector } from "reselect";
import { connect } from "react-redux";

const mapState = createStructuredSelector({
  storeTrial,
  isFetched,
  isLoading,
});

const mapDispatch = {
  fetchStoreTrialAction,
  createStoreTrialAction,
  removeStoreTrialAction,
  resetStoreTrialAction,
};

export default connect(mapState, mapDispatch)(StoreTrial);
