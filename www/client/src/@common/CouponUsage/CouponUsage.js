import React, { useCallback, useEffect, useMemo } from "react";
import { Card, EmptyState, Icon, Link, ResourceItem, ResourceList, Stack, TextStyle } from "@shopify/polaris";
import { ExternalMinor } from "@shopify/polaris-icons";
import Pagination from "@components/Pagination";
import useFetch from "@hooks/useFetch";
import useHasScope from "@hooks/useHasScope";
import { limitOptions } from "@defaults/options";
import useCancelToken from "@hooks/useCancelToken";
import { UPDATE_COUPONS } from "../../@utils/scopes";
import { numberFormat } from "../../@defaults/numberFormat";

export const CouponUsage = ({
  isFetched,
  isLoading,
  total,
  couponUsageStores,
  fetchCouponsUsageAction,
  resetCouponsUsageAction,
  couponId,
}) => {
  const [cancelToken, cancelRequests] = useCancelToken();
  const loading = useMemo(() => !isFetched || isLoading, [isLoading, isFetched]);
  const hasScope = useHasScope();

  const { page, limit, pages, onPreviousPage, onNextPage, onLimitChange } = useFetch({
    limitOptions,
    loading,
    total,
  });

  const fetchCoupons = useCallback(() => {
    if (hasScope(UPDATE_COUPONS)) {
      fetchCouponsUsageAction(
        couponId,
        {
          limit,
          page: page,
        },
        cancelToken
      );
    }
  }, [page, limit, couponId]);

  useEffect(() => {
    fetchCoupons();
  }, [fetchCoupons]);

  // On unmount
  useEffect(
    () => () => {
      cancelRequests();
      resetCouponsUsageAction();
    },
    []
  );

  const pagination = useMemo(() => {
    return {
      hasPrevious: pages > 1 && page > 1,
      hasNext: pages > 1 && page < pages,
      onPrevious: onPreviousPage,
      onNext: onNextPage,
    };
  }, [pages, page, onPreviousPage, onNextPage]);

  const perPage = useMemo(() => {
    return {
      limit: limit,
      options: limitOptions,
      onChange: onLimitChange,
    };
  }, [limit, onLimitChange]);

  const renderItem = item => {
    const {
      id,
      store: { name },
    } = item;

    return (
      <ResourceItem id={id}>
        <Stack distribution={"equalSpacing"}>
          <Stack.Item>
            <Link url={`//${name}`} external>
              <Stack>
                <TextStyle variation="strong">{name}</TextStyle>
                <Icon source={ExternalMinor} />
              </Stack>
            </Link>
          </Stack.Item>
        </Stack>
      </ResourceItem>
    );
  };

  const resourceListMarkup = (
    <ResourceList
      showHeader
      resourceName={{
        singular: "store",
        plural: "stores",
      }}
      totalItemsCount={numberFormat(total)}
      loading={isLoading}
      items={couponUsageStores}
      renderItem={renderItem}
      idForItem={item => item.id}
    />
  );

  const paginationMarkup =
    couponUsageStores && couponUsageStores.length ? (
      <Card.Section>
        <Pagination pagination={pagination} perPage={perPage} />
      </Card.Section>
    ) : null;

  if (!couponUsageStores.length) {
    return (
      <Card>
        <Card.Section>
          <EmptyState heading="This coupon has not been used yet." />
        </Card.Section>
      </Card>
    );
  }

  return (
    <Card>
      {resourceListMarkup}
      {paginationMarkup}
    </Card>
  );
};
