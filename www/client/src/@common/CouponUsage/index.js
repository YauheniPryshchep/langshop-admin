import {
  fetchCouponsUsageAction,
  resetCouponsUsageAction,
  isFetched,
  isLoading,
  isLoaded,
  total,
  couponUsageStores,
} from "@store/coupon-usage";
import { CouponUsage } from "./CouponUsage";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";

const mapState = createStructuredSelector({
  isFetched,
  isLoading,
  isLoaded,
  total,
  couponUsageStores,
});

const mapDispatch = {
  fetchCouponsUsageAction,
  resetCouponsUsageAction,
};

export default connect(mapState, mapDispatch)(CouponUsage);
