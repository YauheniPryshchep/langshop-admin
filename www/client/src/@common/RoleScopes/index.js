import { isFetched, isLoading, scopes, fetchScopesAction } from "@store/scopes";
import { RoleScopes } from "./RoleScopes";
import { createStructuredSelector } from "reselect";
import { connect } from "react-redux";

const mapState = createStructuredSelector({
  scopes,
  isFetched,
  isLoading,
});

const mapDispatch = {
  fetchScopesAction,
};

export default connect(mapState, mapDispatch)(RoleScopes);
