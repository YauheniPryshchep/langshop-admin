import React, { useCallback, useEffect, useMemo } from "react";
import { Card, Checkbox, SkeletonBodyText, TextContainer } from "@shopify/polaris";

export const RoleScopes = ({
  fetchScopesAction,
  role,
  scopes,
  isFetched,
  isLoading,
  handleAddScope,
  handleRemoveScope,
  editable,
}) => {
  const loading = useMemo(() => isLoading || !isFetched, [isLoading, isFetched]);

  useEffect(() => {
    if (isFetched) {
      return;
    }

    fetchScopesAction();
  }, [isFetched]);

  const groups = useMemo(() => {
    if (loading) {
      return;
    }

    const groups = [];

    scopes.forEach(scope => {
      let children = groups,
        path = scope.description.split(":").map(part => part.trim());

      for (let i = 0, size = path.length; i < size; i++) {
        let part = path[i],
          group = children.find(group => group.title === part),
          scopes = [];

        if (!group) {
          group = {
            title: part,
            children: [],
            scopes: scopes,
          };

          children.push(group);
        }

        if (i + 1 === size - 1) {
          group.scopes.push({
            ...scope,
            description: path[i + 1],
          });

          break;
        }

        children = group.children;
      }
    });

    return groups;
  }, [loading, scopes]);

  const onChange = useCallback((scope, value) => (value ? handleAddScope(scope) : handleRemoveScope(scope)), [
    handleAddScope,
    handleRemoveScope,
  ]);

  const isShowRole = useCallback(scopeName => {
    const name = scopeName.split("-");
    return name[name.length - 1] === "show";
  }, []);

  const getUpdateRole = useCallback(scopeName => {
    const name = scopeName.split("-");
    name[name.length - 1] = "update";
    return name.join("-");
  }, []);

  const isChecked = useCallback(
    scope => {
      if (isShowRole(scope.name)) {
        const updateRoleName = getUpdateRole(scope.name);
        const updateRole = role.scopes.find(role => role.name === updateRoleName);
        return !!role.scopes.find(roleScope => roleScope.id === scope.id) || !!updateRole;
      }
      return !!role.scopes.find(roleScope => roleScope.id === scope.id);
    },
    [role.scopes]
  );

  const isDisabled = useCallback(
    scope => {
      if (isShowRole(scope.name)) {
        const updateRoleName = getUpdateRole(scope.name);
        const updateRole = role.scopes.find(role => role.name === updateRoleName);
        return !editable || !!updateRole;
      }
      return !editable;
    },
    [role.scopes, editable]
  );

  if (loading) {
    return <SkeletonBodyText />;
  }

  const groupMarkup = group => (
    <Card key={group.title} title={group.title} sectioned>
      <TextContainer>
        {!!group.scopes.length &&
          group.scopes.map(scope => (
            <TextContainer key={scope.id}>
              <Checkbox
                label={scope.description}
                checked={isChecked(scope)}
                onChange={value => onChange(scope, value)}
                disabled={isDisabled(scope)}
              />
            </TextContainer>
          ))}
      </TextContainer>
      {!!group.children.length && group.children.map(children => groupMarkup(children))}
    </Card>
  );

  return <div>{groups.map(group => groupMarkup(group))}</div>;
};
