import React, { useCallback, useMemo } from "react";
import { Button } from "@shopify/polaris";
import { DRAFT_STATUS, BLOCKED_STATUS, ACTIVE_STATUS } from "../../@routes/MainLayout/AppFrame/Coupons/Coupons";

export const CouponActivity = ({ status, couponId, loading, updateCouponAction }) => {
  if (!couponId) {
    return null;
  }

  const label = useMemo(() => {
    if ([DRAFT_STATUS, BLOCKED_STATUS].includes(status)) {
      return "Activate";
    }
    return "Block";
  }, [status]);

  const buttonType = useMemo(() => {
    if ([DRAFT_STATUS, BLOCKED_STATUS].includes(status)) {
      return {
        primary: true,
      };
    }
    return {
      destructive: true,
    };
  }, [status]);

  const handleClick = useCallback(() => {
    if ([DRAFT_STATUS, BLOCKED_STATUS].includes(status)) {
      updateCouponAction(couponId, { status: ACTIVE_STATUS });
    } else {
      updateCouponAction(couponId, { status: BLOCKED_STATUS });
    }
  }, [status]);

  return (
    <Button {...buttonType} loading={loading} onClick={handleClick}>
      {label}
    </Button>
  );
};
