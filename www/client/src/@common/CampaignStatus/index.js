import { isLoading, updateCampaignAction } from "@store/campaign";
import { CampaignStatus } from "./CampaignStatus";
import { createStructuredSelector } from "reselect";
import { connect } from "react-redux";

const mapState = createStructuredSelector({
  isLoading,
});

const mapDispatch = {
  updateCampaignAction,
};

export default connect(mapState, mapDispatch)(CampaignStatus);
