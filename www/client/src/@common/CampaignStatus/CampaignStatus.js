import React, { useCallback } from "react";
import { Badge, Button, Card, FormLayout } from "@shopify/polaris";
import {
  CAMPAIGNS_STATUS,
  STATUS_CANCELED,
  STATUS_DONE,
  STATUS_READY_TO_START,
} from "@routes/MainLayout/AppFrame/Campaigns/Campaigns";
import useActive from "@hooks/useActive";
import StartCampaignConfirmationModal from "@common/Modals/StartCampaignConfirmationModal";
import CampaignStartDate from "../CampaignStartDate";

export const CampaignStatus = ({ start_at, status, isDisabled, updateCampaignAction, isLoading, campaignId }) => {
  const [open, handleOpen, handleClose] = useActive();

  const getBadgeType = useCallback(status => {
    if (status === STATUS_CANCELED) {
      return {
        status: "warning",
      };
    }

    if (status === STATUS_DONE) {
      return {
        status: "success",
      };
    }

    return {
      status: "info",
    };
  }, []);

  const handleDeploy = useCallback(async () => {
    await updateCampaignAction(campaignId, { status: STATUS_READY_TO_START });
    handleClose();
  }, [updateCampaignAction, campaignId, handleClose]);

  return (
    <Card>
      <Card.Section>
        <Badge {...getBadgeType(status)}>{CAMPAIGNS_STATUS[status]}</Badge>
        <div style={{ marginTop: "3rem" }} />
        <FormLayout>
          <CampaignStartDate start_at={start_at} isDisabled={isDisabled} />
          {!(isDisabled || !campaignId) ? (
            <Button loading={isLoading} onClick={handleOpen} primary={true}>
              Deploy
            </Button>
          ) : (
            ""
          )}
        </FormLayout>
      </Card.Section>
      <StartCampaignConfirmationModal loading={isLoading} open={open} onSuccess={handleDeploy} onClose={handleClose} />
    </Card>
  );
};
