import React, { useCallback, useEffect, useMemo } from "react";
import { Button, SkeletonBodyText, Stack } from "@shopify/polaris";
import useActive from "@hooks/useActive";
import CreateStoreDiscountModal from "@common/Modals/CreateStoreDiscountModal";
import ConfirmationModal from "@components/ConfirmationModal";
import { initialValues } from "@store/store-discount";
import useCancelToken from "@hooks/useCancelToken";

export const StoreDiscount = ({
  domain,
  isFetched,
  isLoading,
  storeDiscount,
  fetchStoreDiscountAction,
  createStoreDiscountAction,
  removeStoreDiscountAction,
  resetStoreDiscountAction,
}) => {
  const loading = useMemo(() => isLoading || !isFetched, [isLoading, isFetched]);
  const [cancelToken, cancelRequests] = useCancelToken();

  const [createModalOpened, openCreateModal, closeCreateModal] = useActive();
  const [removeModalOpened, openRemoveModal, closeRemoveModal] = useActive();

  useEffect(() => {
    fetchStoreDiscountAction(domain, cancelToken).catch(() => {});
  }, [domain]);

  // On unmount
  useEffect(
    () => () => {
      cancelRequests();
      resetStoreDiscountAction();
    },
    []
  );

  const createStoreDiscount = useCallback(async store => {
    try {
      await createStoreDiscountAction(store);
    } catch (e) {
      openCreateModal();
      return;
    }

    closeCreateModal();
  }, []);

  const removeStoreDiscount = useCallback(() => {
    closeRemoveModal();
    removeStoreDiscountAction(domain);
  }, [domain]);

  if (loading) {
    return <SkeletonBodyText lines={1} />;
  }

  const valueMarkup = (
    <Stack.Item>
      Discount:{" "}
      <strong>
        {storeDiscount
          ? storeDiscount.type === "percent"
            ? `${storeDiscount.value}%`
            : `${storeDiscount.value}$`
          : "-"}
      </strong>
    </Stack.Item>
  );

  const actionMarkup = (
    <Stack.Item>
      {storeDiscount ? (
        <Button onClick={openRemoveModal} plain destructive>
          Remove
        </Button>
      ) : (
        <Button onClick={openCreateModal} plain primary>
          Create
        </Button>
      )}
    </Stack.Item>
  );

  const modalMarkup = storeDiscount ? (
    <ConfirmationModal
      destructive
      title="Remove store discount"
      content="Are you sure you want to delete the store discount?"
      confirm="Remove"
      open={removeModalOpened}
      onCancel={closeRemoveModal}
      onConfirm={removeStoreDiscount}
    />
  ) : (
    <CreateStoreDiscountModal
      open={createModalOpened}
      onClose={closeCreateModal}
      loading={loading}
      handleCreate={createStoreDiscount}
      readOnly={["name"]}
      initialValues={initialValues({
        name: domain,
      })}
    />
  );

  return (
    <div>
      <Stack distribution={"equalSpacing"}>
        {valueMarkup}
        {actionMarkup}
      </Stack>
      {modalMarkup}
    </div>
  );
};
