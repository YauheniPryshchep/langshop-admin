import {
  storeDiscount,
  isFetched,
  isLoading,
  fetchStoreDiscountAction,
  createStoreDiscountAction,
  removeStoreDiscountAction,
  resetStoreDiscountAction,
} from "@store/store-discount";
import { StoreDiscount } from "./StoreDiscount";
import { createStructuredSelector } from "reselect";
import { connect } from "react-redux";

const mapState = createStructuredSelector({
  storeDiscount,
  isFetched,
  isLoading,
});

const mapDispatch = {
  fetchStoreDiscountAction,
  createStoreDiscountAction,
  removeStoreDiscountAction,
  resetStoreDiscountAction,
};

export default connect(mapState, mapDispatch)(StoreDiscount);
