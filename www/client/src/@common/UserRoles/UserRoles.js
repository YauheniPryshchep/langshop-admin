import React from "react";
import { Badge, Stack } from "@shopify/polaris";

export const UserRoles = ({ roles }) => {
  const badgeStatus = role => {
    if (role.name === "sadmin") {
      return "success";
    }

    if (role.name === "nobody") {
      return "warning";
    }

    return null;
  };

  return (
    <Stack>
      {roles.map(role => (
        <Stack.Item key={role.id}>
          <Badge status={badgeStatus(role)}>{role.description}</Badge>
        </Stack.Item>
      ))}
    </Stack>
  );
};
