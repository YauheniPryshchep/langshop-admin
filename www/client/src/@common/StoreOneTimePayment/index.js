import { isFetched, isLoading, createOneTimePaymentAction } from "@store/one-time-charge";
import { fetchStoreHistoryAction } from "@store/store-history";
import { fetchStoreChargeAction } from "@store/store-charge";
import { StoreOneTimePayment } from "./StoreOneTimePayment";
import { createStructuredSelector } from "reselect";
import { connect } from "react-redux";

const mapState = createStructuredSelector({
  isFetched,
  isLoading,
});

const mapDispatch = {
  createOneTimePaymentAction,
  fetchStoreHistoryAction,
  fetchStoreChargeAction,
};

export default connect(mapState, mapDispatch)(StoreOneTimePayment);
