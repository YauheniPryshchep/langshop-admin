import React, { useCallback, useMemo } from "react";
import { Button, Stack } from "@shopify/polaris";
import useActive from "@hooks/useActive";
import { initialValues } from "@store/one-time-charge";
import CreateOneTimePaymentModal from "../Modals/CreateOneTimePaymentModal";
import { useNotification } from "../../@hooks/useNotification";

export const StoreOneTimePayment = ({
  domain,
  isLoading,
  createOneTimePaymentAction,
  fetchStoreHistoryAction,
  fetchStoreChargeAction,
}) => {
  const [createModalOpened, openCreateModal, closeCreateModal] = useActive();
  const loading = useMemo(() => isLoading, [isLoading]);
  const { handleNotify } = useNotification();

  const valueMarkup = <Stack.Item>One time Charge</Stack.Item>;
  const handleCreate = useCallback(async values => {
    const { name, amount, shopifyFee, description } = values;
    await createOneTimePaymentAction({
      shop: name,
      price: shopifyFee ? parseInt(amount) * (100 / 80) : parseInt(amount),
      name: description,
    });
    await fetchStoreHistoryAction(domain, {
      limit: 20,
      page: 1,
    });
    await fetchStoreChargeAction(domain, {});
    handleNotify(`Charge for ${domain} successfully created`);
    closeCreateModal();
  }, []);

  const actionMarkup = (
    <Stack.Item>
      <Button onClick={openCreateModal} plain primary>
        Create
      </Button>
    </Stack.Item>
  );

  const modalMarkup = (
    <CreateOneTimePaymentModal
      open={createModalOpened}
      loading={loading}
      onClose={closeCreateModal}
      handleCreate={handleCreate}
      readOnly={["name"]}
      initialValues={initialValues({
        name: domain,
      })}
    />
  );

  return (
    <div>
      <Stack distribution={"equalSpacing"}>
        {valueMarkup}
        {actionMarkup}
      </Stack>
      {modalMarkup}
    </div>
  );
};
