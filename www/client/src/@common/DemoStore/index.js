import {
  demoStore,
  isFetched,
  isLoading,
  fetchDemoStoreAction,
  createDemoStoreAction,
  removeDemoStoreAction,
  resetDemoStoreAction,
} from "@store/demo-store";
import { DemoStore } from "./DemoStore";
import { createStructuredSelector } from "reselect";
import { connect } from "react-redux";

const mapState = createStructuredSelector({
  demoStore,
  isFetched,
  isLoading,
});

const mapDispatch = {
  fetchDemoStoreAction,
  createDemoStoreAction,
  removeDemoStoreAction,
  resetDemoStoreAction,
};

export default connect(mapState, mapDispatch)(DemoStore);
