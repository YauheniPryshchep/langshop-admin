import React, { useCallback, useEffect, useMemo } from "react";
import { Button, SkeletonBodyText, Stack } from "@shopify/polaris";
import useActive from "@hooks/useActive";
import ConfirmationModal from "@components/ConfirmationModal";
import useCancelToken from "@hooks/useCancelToken";

export const DemoStore = ({
  domain,
  isFetched,
  isLoading,
  demoStore,
  fetchDemoStoreAction,
  createDemoStoreAction,
  removeDemoStoreAction,
  resetDemoStoreAction,
}) => {
  const loading = useMemo(() => isLoading || !isFetched, [isLoading, isFetched]);
  const [cancelToken, cancelRequests] = useCancelToken();

  const [createModalOpened, openCreateModal, closeCreateModal] = useActive();
  const [removeModalOpened, openRemoveModal, closeRemoveModal] = useActive();

  useEffect(() => {
    fetchDemoStoreAction(domain, cancelToken).catch(() => {});
  }, [domain]);

  // On unmount
  useEffect(
    () => () => {
      cancelRequests();
      resetDemoStoreAction();
    },
    []
  );

  const createDemoStore = useCallback(async () => {
    try {
      await createDemoStoreAction({
        name: domain,
      });
    } catch (e) {
      openCreateModal();
      return;
    }

    closeCreateModal();
  }, [domain]);

  const removeDemoStore = useCallback(() => {
    closeRemoveModal();
    removeDemoStoreAction(domain);
  }, [domain]);

  if (loading) {
    return <SkeletonBodyText lines={1} />;
  }

  const valueMarkup = (
    <Stack.Item>
      Demo: <strong>{demoStore ? "Yes" : "No"}</strong>
    </Stack.Item>
  );

  const actionMarkup = (
    <Stack.Item>
      {demoStore ? (
        <Button onClick={openRemoveModal} plain destructive>
          Disable
        </Button>
      ) : (
        <Button onClick={openCreateModal} plain primary>
          Enable
        </Button>
      )}
    </Stack.Item>
  );

  const modalMarkup = demoStore ? (
    <ConfirmationModal
      destructive
      title="Disable demo store"
      content="Are you sure you want to disable the store demo?"
      confirm="Disable"
      open={removeModalOpened}
      onCancel={closeRemoveModal}
      onConfirm={removeDemoStore}
    />
  ) : (
    <ConfirmationModal
      title="Enable demo store"
      content="Are you sure you want to enable the store demo?"
      confirm="Enable"
      open={createModalOpened}
      onCancel={closeCreateModal}
      onConfirm={createDemoStore}
    />
  );

  return (
    <div>
      <Stack distribution={"equalSpacing"}>
        {valueMarkup}
        {actionMarkup}
      </Stack>
      {modalMarkup}
    </div>
  );
};
