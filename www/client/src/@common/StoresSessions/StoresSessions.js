import React, { useEffect, useMemo } from "react";
import { Avatar, Card, EmptyState, ResourceList, TextStyle } from "@shopify/polaris";
import moment from "moment-timezone";
import { Col, Row } from "react-flexbox-grid";

export const StoresSessions = ({
  isLoading,
  isFetched,
  fetchFullstorySessionsAction,
  resetFullstorySessionsAction,
  fullstorySessions,
  storeId,
  customerEmail,
}) => {
  const loading = useMemo(() => isLoading || !isFetched, [isLoading, isFetched]);
  //TODO: REMOVE AFTER FIX ERROR HANDLER;
  useEffect(() => {
    if (storeId) {
      fetchFullstorySessionsAction(storeId)
        .then(() => {})
        .catch(() => {});
    }
    return () => resetFullstorySessionsAction();
  }, [storeId]);

  const userHeader = useMemo(() => {
    return (
      <ResourceList
        items={[customerEmail]}
        renderItem={() => {
          return (
            <ResourceList.Item id={customerEmail} media={<Avatar customer name={customerEmail} />}>
              <h3>
                <TextStyle variation="strong">Sessions for</TextStyle>
              </h3>
              <div style={{ color: "#1b92d9" }}>
                <TextStyle variation="strong">{customerEmail}</TextStyle>
              </div>
            </ResourceList.Item>
          );
        }}
      />
    );
  }, [customerEmail]);

  const resourceList = useMemo(() => {
    return (
      <ResourceList
        items={fullstorySessions}
        renderItem={({ SessionId, CreatedTime, FsUrl }) => {
          return (
            <ResourceList.Item id={SessionId} url={FsUrl} external={true}>
              <Row className={"FullStory_Sessions-Item"}>
                <Col>
                  <svg
                    className={"PlayButton__Svg"}
                    viewBox="0 0 38 38"
                    width={"4rem"}
                    height={"4rem"}
                    xmlns="http://www.w3.org/2000/svg"
                    fillRule="evenodd"
                    clipRule="evenodd"
                  >
                    <path
                      className={"PlayButton__Svg--white"}
                      d="M19 1C9.06 1 1 9.057 1 19c0 9.94 8.057 18 18 18 9.94 0 18-8.057 18-18 0-9.94-8.057-18-18-18z"
                      fill="#fff"
                    />
                    <path
                      d="M19 1C9.06 1 1 9.057 1 19c0 9.94 8.057 18 18 18 9.94 0 18-8.057 18-18 0-9.94-8.057-18-18-18z"
                      fill="none"
                      stroke="#1b92d9"
                    />
                    <path
                      className={"PlayButton__Svg--blue"}
                      d="M15 11.723c0-.605.7-.942 1.173-.564l10.93 7.215a.72.72 0 010 1.128l-10.93 7.216A.723.723 0 0115 26.153v-14.43z"
                      fill="#1b92d9"
                    />
                  </svg>
                </Col>
                <Col>
                  <TextStyle variation={"strong"}>{moment(CreatedTime * 1000).format("DD MMM, LT")}</TextStyle>
                </Col>
              </Row>
            </ResourceList.Item>
          );
        }}
      />
    );
  }, [fullstorySessions]);

  if (loading) {
    return null;
  }

  if (!fullstorySessions.length) {
    return (
      <Card>
        <Card.Section>
          <EmptyState heading="Sessions list is empty for this store" />
        </Card.Section>
      </Card>
    );
  }

  return (
    <Card>
      {userHeader}
      {resourceList}
    </Card>
  );
};
