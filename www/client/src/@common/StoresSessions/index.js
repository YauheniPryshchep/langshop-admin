import {
  isLoading,
  isFetched,
  fullstorySessions,
  fetchFullstorySessionsAction,
  resetFullstorySessionsAction,
} from "@store/fullstory-sessions";
import { StoresSessions } from "./StoresSessions";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";

const mapState = createStructuredSelector({
  isLoading,
  isFetched,
  fullstorySessions,
});

const mapDispatch = {
  fetchFullstorySessionsAction,
  resetFullstorySessionsAction,
};

export default connect(mapState, mapDispatch)(StoresSessions);
