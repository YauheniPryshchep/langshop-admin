import React, { useCallback } from "react";
import { Badge, Card, ResourceItem, ResourceList, Stack, TextStyle } from "@shopify/polaris";
import {
  CAMPAIGNS_STATUS,
  CANCELED_STATUS,
  DRAFT_STATUS,
  IN_PROGRESS_STATUS,
  WAITING_FOR_THE_START_STATUS,
} from "@routes/MainLayout/AppFrame/Campaigns/Campaigns";

import { numberFormat } from "../../@defaults/numberFormat";

export const CustomerCampaigns = ({ campaigns }) => {
  if (!campaigns.length) {
    return null;
  }

  const getBadgeType = useCallback(status => {
    if (status === DRAFT_STATUS) {
      return {
        progress: "incomplete",
        status: "info",
      };
    }

    if (status === WAITING_FOR_THE_START_STATUS) {
      return {
        progress: "partiallyComplete",
        status: "info",
      };
    }

    if (status === IN_PROGRESS_STATUS) {
      return {
        status: "info",
      };
    }

    if (status === CANCELED_STATUS) {
      return {
        status: "warning",
      };
    }

    return {
      status: "success",
    };
  }, []);

  const renderItem = item => {
    const { id, title, status } = item;

    return (
      <ResourceItem id={id} url={`/marketing/campaigns/${id}`}>
        <Stack distribution={"equalSpacing"}>
          <Stack.Item>
            <TextStyle variation="strong">{title}</TextStyle>
          </Stack.Item>
          <Stack.Item>
            <TextStyle variation="strong">
              {<Badge {...getBadgeType(status)}>{CAMPAIGNS_STATUS[status]}</Badge>}
            </TextStyle>
          </Stack.Item>
        </Stack>
      </ResourceItem>
    );
  };

  const resourceListMarkup = (
    <ResourceList
      showHeader
      resourceName={{
        singular: "campaign",
        plural: "campaigns",
      }}
      totalItemsCount={numberFormat(campaigns.length)}
      items={campaigns}
      renderItem={renderItem}
    />
  );

  return <Card title={"Campaigns"}>{resourceListMarkup}</Card>;
};
