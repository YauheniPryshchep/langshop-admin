import React from "react";
import { ResourceList, TextStyle } from "@shopify/polaris";
import { Col, Row } from "react-flexbox-grid";

export const TemplateVariables = () => {
  const items = [
    {
      term: <TextStyle variation="code"> {"{{subject}}*"} </TextStyle>,
      description: "Campaign subject",
    },
    {
      term: <TextStyle variation="code"> {"{{domain}}*"} </TextStyle>,
      description: "Store domain",
    },
    {
      term: <TextStyle variation="code"> {"{{store_name}}"} </TextStyle>,
      description: "Name of store",
    },
    {
      term: <TextStyle variation="code"> {"{{email}}*"} </TextStyle>,
      description: "Store email",
    },
    {
      term: <TextStyle variation="code"> {"{{iana_timezone}}"} </TextStyle>,
      description: "Store timezone",
    },
    {
      term: <TextStyle variation="code"> {"{{plan_display_name}}"} </TextStyle>,
      description: "Store plan",
    },
    {
      term: <TextStyle variation="code"> {"{{primary_locale}}"} </TextStyle>,
      description: "Store primary locale",
    },
    {
      term: <TextStyle variation="code"> {"{{currency}}"} </TextStyle>,
      description: "Store currency",
    },
    {
      term: <TextStyle variation="code"> {"{{customer_email}}"} </TextStyle>,
      description: "Store customer email",
    },
    {
      term: <TextStyle variation="code"> {"{{password_enabled}}"} </TextStyle>,
      description: "Return 1 - if password protect enabled, 0 - disabled",
    },
  ];

  return (
    <ResourceList
      items={items}
      renderItem={(item, id) => {
        return (
          <ResourceList.Item id={id}>
            <Row>
              <Col xs={5}>{item.term}</Col>
              <Col xs={7}>{item.description}</Col>
            </Row>
          </ResourceList.Item>
        );
      }}
    />
  );
};
