import React from "react";
import { Badge } from "@shopify/polaris";

export const StoreStatus = ({ store, badge = true }) => {
  if (!badge) {
    return store ? (parseInt(store.active) || 0 ? "Installed" : "Uninstalled") : "Not found";
  }

  return store ? (
    parseInt(store.active) || 0 ? (
      <Badge status={"success"}>Installed</Badge>
    ) : (
      <Badge status={"attention"}>Uninstalled</Badge>
    )
  ) : (
    <Badge status={"warning"}>Not found</Badge>
  );
};
