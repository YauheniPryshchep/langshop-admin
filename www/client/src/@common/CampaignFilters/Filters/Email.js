import React, { useCallback, useEffect, useMemo, useState } from "react";
import { Button, FormLayout, Select, Tag, TextContainer, TextField } from "@shopify/polaris";
import { mailRegexp } from "../../../@defaults/regexp";
import { get } from "lodash";

const Email = ({ filters, onChange }) => {
  const [comparator, setComparator] = useState("is");
  const [value, setValue] = useState("");
  const [error, setError] = useState("");

  const emails = useMemo(() => {
    let values = filters.find(filter => filter.key === "emails");
    return get(values, "value", []);
  }, [filters]);

  useEffect(() => {
    if (value && !mailRegexp.test(value)) {
      setError("Invalid email");
    } else {
      setError("");
    }
  }, [value]);

  const handleAddEmail = useCallback(
    email => {
      onChange({
        key: "emails",
        comparator,
        value: [...emails, email],
      });
      setValue("");
    },
    [emails, comparator, onChange, setValue]
  );

  const onRemoveEmail = useCallback(
    email => {
      onChange({
        key: "emails",
        comparator,
        value: emails.filter(e => e !== email).length ? [...emails.filter(e => e !== email)] : null,
      });
    },
    [emails, comparator, onChange]
  );

  return (
    <FormLayout>
      <Select
        label={"Comparator"}
        options={[
          {
            label: "Include",
            value: "is",
          },
          {
            label: "Exclude",
            value: "is_not",
          },
        ]}
        value={comparator}
        onChange={setComparator}
      />
      <TextField
        label={"Email"}
        value={value}
        onChange={setValue}
        error={error}
        connectedRight={
          <Button disabled={!!error || !value} onClick={() => handleAddEmail(value)}>
            Add
          </Button>
        }
      />
      <TextContainer>
        {emails.map((email, index) => {
          return (
            <Tag key={index} onRemove={() => onRemoveEmail(email)}>
              {email}
            </Tag>
          );
        })}
      </TextContainer>
    </FormLayout>
  );
};

export default Email;
