import React, { useMemo } from "react";
import { Select } from "@shopify/polaris";

const Subscribe = ({ filters, onChange }) => {
  const options = useMemo(
    () => [
      { label: "Select subscribe type", value: "" },
      { label: "Store is subscribe", value: "subscribe" },
      { label: "Store is unsubscribe", value: "unsubscribe" },
    ],
    []
  );

  const value = useMemo(() => {
    let subscribe = filters.find(filter => filter.key === "subscribe");

    if (!subscribe) {
      return "";
    }

    return `${subscribe.value}`;
  }, [filters]);

  return (
    <Select
      label={"Subscribe"}
      labelHidden={true}
      value={value}
      options={options}
      onChange={value =>
        onChange({
          key: "subscribe",
          comparator: "is",
          value: value,
        })
      }
    />
  );
};

export default Subscribe;
