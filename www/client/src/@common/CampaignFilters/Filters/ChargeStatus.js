import React, { useMemo } from "react";
import { Select } from "@shopify/polaris";

const ActiveStatus = ({ filters, onChange }) => {
  const options = useMemo(
    () => [
      { label: "Select charge status", value: "" },
      { label: "Charge is accepted", value: "is-accepted" },
      { label: "Charge is not accepted", value: "is_not-accepted" },
      { label: "Charge is declined", value: "is-declined" },
      { label: "Charge is not declined", value: "is_not-declined" },
      { label: "Charge is frozen", value: "is-frozen" },
      { label: "Charge is not frozen", value: "is_not-frozen" },
    ],
    []
  );

  const value = useMemo(() => {
    let charge_status = filters.find(filter => filter.key === "charge_status");

    if (!charge_status) {
      return "";
    }

    return `${charge_status.comparator}-${charge_status.value}`;
  }, [filters]);

  return (
    <Select
      label={"Charge status"}
      labelHidden={true}
      value={value}
      options={options}
      onChange={value =>
        onChange({
          key: "charge_status",
          comparator: value.split("-")[0],
          value: value.split("-")[1],
        })
      }
    />
  );
};

export default ActiveStatus;
