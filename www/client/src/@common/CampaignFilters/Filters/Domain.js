import React, {useCallback, useEffect, useMemo, useState} from "react";
import {Button, FormLayout, Select, Tag, TextContainer, TextField} from "@shopify/polaris";
import {get} from "lodash";
import {mailRegexp} from "../../../@defaults/regexp";

const Domain = ({filters, onChange}) => {
  const [comparator, setComparator] = useState("is");
  const [value, setValue] = useState("");
  const [error, setError] = useState("");

  const domains = useMemo(() => {
    let values = filters.find(filter => filter.key === "domains");
    return get(values, "value", []);
  }, [filters]);

  useEffect(() => {
    if (value.match(/^[\w-]+[.]myshopify[.]com$/g)) {
      setError("")
    } else {
      setError("Domain must match the following pattern: *.myshopify.com")

    }
  }, [value]);

  const handleAddDomain = useCallback(
    domain => {
      onChange({
        key: "domains",
        comparator,
        value: [...domains, domain],
      });
      setValue("");
    },
    [domains, comparator, onChange, setValue]
  );

  const onRemoveDomain = useCallback(
    domain => {
      onChange({
        key: "domains",
        comparator,
        value: domains.filter(e => e !== domain).length ? [...domains.filter(e => e !== domain)] : null,
      });
    },
    [domains, comparator, onChange]
  );

  return (
    <FormLayout>
      <Select
        label={"Comparator"}
        options={[
          {
            label: "Include",
            value: "is",
          },
          {
            label: "Exclude",
            value: "is_not",
          },
        ]}
        value={comparator}
        onChange={setComparator}
      />
      <TextField
        label={"Domain"}
        value={value}
        onChange={setValue}
        error={error}
        connectedRight={<Button disabled={!!error || !value} onClick={() => handleAddDomain(value)}>Add</Button>}
      />
      <TextContainer>
        {domains.map((domain, index) => {
          return (
            <Tag key={index} onRemove={() => onRemoveDomain(domain)}>
              {domain}
            </Tag>
          );
        })}
      </TextContainer>
    </FormLayout>
  );
};

export default Domain;
