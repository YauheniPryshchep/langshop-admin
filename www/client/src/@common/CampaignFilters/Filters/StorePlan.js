import React, { useMemo } from "react";
import { Select } from "@shopify/polaris";

const StorePlan = ({ filters, onChange }) => {
  const options = useMemo(
    () => [
      { label: "Select charge status", value: "" },
      { label: "Store plan is affiliate", value: "is-affiliate" },
      { label: "Store plan is basic", value: "is-basic" },
      { label: "Store plan is business", value: "is-business" },
      { label: "Store plan is cancelled", value: "is-cancelled" },
      { label: "Store plan is custom", value: "is-custom" },
      { label: "Store plan is dormant", value: "is-dormant" },
      { label: "Store plan is fraudulent", value: "is-fraudulent" },
      { label: "Store plan is frozen", value: "is-frozen" },
      { label: "Store plan is npo full", value: "is-npo_full" },
      { label: "Store plan is npo lite", value: "is-npo_lite" },
      { label: "Store plan is open learning", value: "is-open_learning" },
      { label: "Store plan is partner test", value: "is-partner_test" },
      { label: "Store plan is plus partner sandbox", value: "is-plus_partner_sandbox" },
      { label: "Store plan is paused", value: "is-paused" },
      { label: "Store plan is professional", value: "is-professional" },
      { label: "Store plan is shopify plus", value: "is-shopify_plus" },
      { label: "Store plan is staff", value: "is-staff" },
      { label: "Store plan is staff business", value: "is-staff_business" },
      { label: "Store plan is starter", value: "is-starter" },
      { label: "Store plan is trial", value: "is-trial" },
      { label: "Store plan is unlimited", value: "is-unlimited" },
    ],
    []
  );

  const value = useMemo(() => {
    let shopify_plan = filters.find(filter => filter.key === "shopify_plan");

    if (!shopify_plan) {
      return "";
    }

    return `${shopify_plan.comparator}-${shopify_plan.value}`;
  }, [filters]);

  return (
    <Select
      label={"Shopify plan"}
      labelHidden={true}
      value={value}
      options={options}
      onChange={value =>
        onChange({
          key: "shopify_plan",
          comparator: value.split("-")[0],
          value: value.split("-")[1],
        })
      }
    />
  );
};

export default StorePlan;
