import React, { useMemo, useState } from "react";
import { FormLayout, OptionList, Select } from "@shopify/polaris";
import { campaigns } from "@store/campaigns";
import { STATUS_DONE } from "@routes/MainLayout/AppFrame/Campaigns/Campaigns";
import { useSelector } from "react-redux";
import { useParams } from "react-router-dom";

const InCampaign = ({ filters, onChange }) => {
  const [comparator, setComparator] = useState("in");

  const campaignsSelector = useSelector(campaigns);

  const { campaignId } = useParams();

  const options = useMemo(
    () =>
      campaignsSelector
        .filter(campaign => campaign.id !== parseInt(campaignId))
        .map(campaign => ({ value: campaign.id, label: campaign.title })),
    [campaignsSelector]
  );

  const selected = useMemo(() => {
    let in_campaign = filters.find(filter => filter.key === "in_campaign");

    if (!in_campaign) {
      return [];
    }

    return in_campaign.value;
  }, [filters]);

  return (
    <FormLayout>
      <Select
        label={"Comparator"}
        options={[
          {
            label: "Where in campaigns",
            value: "in",
          },
          {
            label: "Where in not campaigns ",
            value: "in_not",
          },
        ]}
        value={comparator}
        onChange={setComparator}
      />
      <OptionList
        title={"Campaigns"}
        onChange={value => onChange({ key: "in_campaign", comparator, value })}
        options={options}
        selected={selected}
        allowMultiple
      />
    </FormLayout>
  );
};

export default InCampaign;
