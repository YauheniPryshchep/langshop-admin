import React, { useCallback } from "react";
import { TextField } from "@shopify/polaris";

const Limit = ({ limit, onChange }) => {
  const handleChange = useCallback(
    value => {
      if (/^\d+$/.test(value)) {
        onChange(!parseInt(value) ? 1 : value);
      } else {
        onChange(limit);
      }
    },
    [limit]
  );

  return (
    <TextField type={"number"} label={"limit"} labelHidden onChange={handleChange} value={limit.toString()} min={1} max={10000} />
  );
};

export default Limit;
