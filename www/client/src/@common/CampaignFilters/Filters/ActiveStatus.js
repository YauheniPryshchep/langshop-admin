import React, { useMemo } from "react";
import { Select } from "@shopify/polaris";

const ActiveStatus = ({ filters, onChange }) => {
  const options = useMemo(
    () => [
      { label: "Select active status", value: "" },
      { label: "Store is installed", value: "is-installed" },
      { label: "Store was never installed", value: "was_never-installed" },
      { label: "Store is not installed", value: "is_not-installed" },
      { label: "Store is uninstalled", value: "is-uninstalled" },
    ],
    []
  );

  const value = useMemo(() => {
    let active_status = filters.find(filter => filter.key === "active_status");

    if (!active_status) {
      return "";
    }

    return `${active_status.comparator}-${active_status.value}`;
  }, [filters]);

  return (
    <Select
      label={"Active status"}
      labelHidden={true}
      value={value}
      options={options}
      onChange={value =>
        onChange({
          key: "active_status",
          comparator: value.split("-")[0],
          value: value.split("-")[1],
        })
      }
    />
  );
};

export default ActiveStatus;
