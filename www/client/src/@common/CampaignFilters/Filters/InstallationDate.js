import React, { useCallback, useMemo, useState } from "react";
import { DatePicker, FormLayout, Select } from "@shopify/polaris";

const InstallationDate = ({ filters, onChange }) => {
  const [{ month, year }, setDate] = useState({ month: new Date().getMonth(), year: new Date().getFullYear() });
  const [comparator, setComparator] = useState("installed_between");

  const handleMonthChange = useCallback((month, year) => setDate({ month, year }), []);

  const selectedDates = useMemo(() => {
    let installationDate = filters.find(filter => filter.key === "installation_date");

    if (!installationDate) {
      return {
        start: new Date(),
        end: new Date(),
      };
    }

    return {
      start: new Date(installationDate.value.start),
      end: new Date(installationDate.value.end),
    };
  }, [filters]);

  return (
    <FormLayout>
      <Select
        label={"Comparator"}
        options={[
          {
            label: "Installed between",
            value: "installed_between",
          },
          {
            label: "Uninstalled between",
            value: "uninstalled_between",
          },
        ]}
        value={comparator}
        onChange={setComparator}
      />

      <DatePicker
        month={month}
        year={year}
        onChange={value =>
          onChange({
            key: "installation_date",
            comparator: comparator,
            value: {
              start: value.start.toDateString(),
              end: value.end.toDateString(),
            },
          })
        }
        onMonthChange={handleMonthChange}
        selected={selectedDates}
        allowRange
      />
    </FormLayout>
  );
};

export default InstallationDate;
