import React, { useCallback, useMemo, useState } from "react";
import { DatePicker, FormLayout, Select } from "@shopify/polaris";

const LantEvent = ({ filters, onChange }) => {
  const [{ month, year }, setDate] = useState({ month: new Date().getMonth(), year: new Date().getFullYear() });
  const [comparator, setComparator] = useState("installed");

  const handleMonthChange = useCallback((month, year) => setDate({ month, year }), []);

  const selectedDates = useMemo(() => {
    let installationDate = filters.find(filter => filter.key === "last_event");

    if (!installationDate) {
      return {
        start: new Date(),
        end: new Date(),
      };
    }

    return {
      start: new Date(installationDate.value.start),
      end: new Date(installationDate.value.end),
    };
  }, [filters]);

  return (
    <FormLayout>
      <Select
        label={"Comparator"}
        options={[
          {
            label: "Installed",
            value: "installed",
          },
          {
            label: "Uninstalled ",
            value: "uninstalled",
          },
        ]}
        value={comparator}
        onChange={
          filters.find(filter => filter.key === "last_event")
            ? value => {
                setComparator(value);
                onChange({
                  key: "last_event",
                  comparator: value,
                  value: {
                    start: selectedDates.start.toDateString(),
                    end: selectedDates.end.toDateString(),
                  },
                });
              }
            : setComparator
        }
      />

      <DatePicker
        month={month}
        year={year}
        onChange={value =>
          onChange({
            key: "last_event",
            comparator: comparator,
            value: {
              start: value.start.toDateString(),
              end: value.end.toDateString(),
            },
          })
        }
        onMonthChange={handleMonthChange}
        selected={selectedDates}
        allowRange
      />
    </FormLayout>
  );
};

export default LantEvent;
