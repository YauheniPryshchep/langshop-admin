import React, { useCallback, useMemo } from "react";
import { Card, List } from "@shopify/polaris";
import { CampaignFiltersAdapter } from "@components/CampaignFiltersAdapter/CampaignFiltersAdapter";
import { get } from "lodash";
import { Field } from "redux-form";

export const CampaignFilters = ({ recipients_filter, isDisabled }) => {
  const limitValue = useMemo(() => get(recipients_filter, "limit", "Unlimited"), [recipients_filter]);

  const getValueToString = useCallback((key, value) => {
    if (["installation_date", "last_event"].includes(key)) {
      return `${value.start} ${value.end}`;
    }

    return value;
  }, []);

  const filtersValue = useMemo(() => {
    const filters = get(recipients_filter, "filters", []);
    if (!filters.length) {
      return <List.Item>All Recipients</List.Item>;
    }

    return filters.map((filter, index) => {
      return (
        <List.Item key={index}>
          {filter.key} {filter.comparator} {getValueToString(filter.key, filter.value)}
        </List.Item>
      );
    });
  }, [recipients_filter]);
  return (
    <Card>
      <Card.Section title={"Limit"}>
        <List type="bullet">
          <List.Item>Limit: {limitValue}</List.Item>
        </List>
      </Card.Section>
      <Card.Section title={"Filters"}>
        <List>{filtersValue}</List>
      </Card.Section>
      {!isDisabled ? (
        <Card.Section title={"Filters"}>
          <Field
            name={"recipients_filter"}
            component={CampaignFiltersAdapter}
            isDisabled={isDisabled}
            recipients_filter={recipients_filter}
          />
        </Card.Section>
      ) : (
        ""
      )}
    </Card>
  );
};
