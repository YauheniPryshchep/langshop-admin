import React, { useEffect, useMemo, useState } from "react";
import { Card, Icon, Modal, ResourceItem, ResourceList, Stack, TextField, TextStyle } from "@shopify/polaris";
import { SearchMinor } from "@shopify/polaris-icons";

import useFetch from "@hooks/useFetch";
import Pagination from "@components/Pagination";
import UserRoles from "@common/UserRoles";
import useCancelToken from "@hooks/useCancelToken";
import { sortOptions, limitOptions } from "@defaults/options";
import { numberFormat } from "../../../@defaults/numberFormat";

export const UnassignedUsersModal = ({
  open,
  onClose,
  unassignedUsers,
  fetchUnassignedUsersAction,
  resetUnassignedUsersAction,
  isLoading,
  total,
  onAddSelected,
}) => {
  const [cancelToken, cancelRequests] = useCancelToken();
  const [selectedUsers, setSelectedUsers] = useState([]);

  const {
    page,
    limit,
    filter,
    sort,
    search,
    pages,
    onPreviousPage,
    onNextPage,
    onLimitChange,
    onSortChange,
    onSearchChange,
    onSearchClear,
  } = useFetch({
    sortOptions,
    limitOptions,
    isLoading,
    total,
  });

  useEffect(() => {
    if (!open) {
      setSelectedUsers([]);
      return;
    }

    const sortOption = sortOptions.find(option => sort === option.value);

    fetchUnassignedUsersAction(
      {
        filter,
        limit,
        page: page,
        sort: {
          field: sortOption.field,
          direction: sortOption.direction,
        },
      },
      cancelToken
    );
  }, [open, page, limit, sort, filter]);

  // On unmount
  useEffect(
    () => () => {
      cancelRequests();
      resetUnassignedUsersAction();
    },
    []
  );

  const pagination = useMemo(() => {
    return {
      hasPrevious: pages > 1 && page > 1,
      hasNext: pages > 1 && page < pages,
      onPrevious: onPreviousPage,
      onNext: onNextPage,
    };
  }, [pages, page, onPreviousPage, onNextPage]);

  const perPage = useMemo(() => {
    return {
      limit: limit,
      options: limitOptions,
      onChange: onLimitChange,
    };
  }, [limit, onLimitChange]);

  const filterControl = (
    <TextField
      prefix={<Icon source={SearchMinor} />}
      label={""}
      value={search}
      placeholder="Search users ..."
      onClearButtonClick={onSearchClear}
      onChange={onSearchChange}
      clearButton
      labelHidden
    />
  );

  const renderItem = item => {
    const { id, name, email, roles } = item;

    return (
      <ResourceItem id={id}>
        <Stack distribution={"equalSpacing"}>
          <Stack.Item>
            <TextStyle variation="strong">{name}</TextStyle> {email}
          </Stack.Item>
          <Stack.Item>
            <UserRoles roles={roles} />
          </Stack.Item>
        </Stack>
      </ResourceItem>
    );
  };

  const resourceListMarkup = (
    <ResourceList
      showHeader
      resourceName={{
        singular: "user",
        plural: "users",
      }}
      sortOptions={sortOptions}
      sortValue={sort}
      onSortChange={onSortChange}
      totalItemsCount={numberFormat(total)}
      loading={isLoading}
      items={unassignedUsers}
      filterControl={filterControl}
      renderItem={renderItem}
      promotedBulkActions={[
        {
          content: "Add to app",
          onAction: () => onAddSelected(selectedUsers),
          disabled: isLoading || !selectedUsers.length,
        },
      ]}
      onSelectionChange={setSelectedUsers}
      selectedItems={selectedUsers}
      idForItem={item => item.id}
    />
  );

  const paginationMarkup =
    unassignedUsers && unassignedUsers.length ? (
      <Card.Section>
        <Pagination pagination={pagination} perPage={perPage} />
      </Card.Section>
    ) : null;

  return (
    <Modal large open={open} onClose={onClose} title="Unassigned Users">
      <Modal.Section>
        {resourceListMarkup}
        {paginationMarkup}
      </Modal.Section>
    </Modal>
  );
};
