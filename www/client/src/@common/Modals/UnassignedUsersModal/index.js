import {
  unassignedUsers,
  fetchUnassignedUsersAction,
  resetUnassignedUsersAction,
  isFetched,
  isLoading,
  total,
} from "@store/unassigned-users";
import { UnassignedUsersModal } from "./UnassignedUsersModal";
import { createStructuredSelector } from "reselect";
import { connect } from "react-redux";

const mapState = createStructuredSelector({
  unassignedUsers,
  isFetched,
  isLoading,
  total,
});

const mapDispatch = {
  fetchUnassignedUsersAction,
  resetUnassignedUsersAction,
};

export default connect(mapState, mapDispatch)(UnassignedUsersModal);
