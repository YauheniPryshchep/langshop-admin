import { initialValues } from "@store/store-discount";
import { CreateStoreDiscountModal } from "./CreateStoreDiscountModal";
import { reduxForm } from "redux-form";

export default reduxForm({
  form: "create-store-discount",
  initialValues: initialValues(),
  enableReinitialize: true,
  destroyOnUnmount: true,
})(CreateStoreDiscountModal);
