import React, {useEffect} from "react";
import {Form, FormLayout, Modal} from "@shopify/polaris";
import TextFieldAdapter from "@components/TextFieldAdapter";
import SelectFieldAdapter from "@components/SelectFieldAdapter";
import {initialValues} from "@store/store-discount";
import {Field} from "redux-form";
import {required, numericality} from "redux-form-validators";

const validate = {
  name: [
    required({
      message: "Store domain is required",
    }),
    value => {
      if (value.match(/^[\w-]+[.]myshopify[.]com$/g)) {
        return;
      }

      return "Domain must match the following pattern: *.myshopify.com";
    },
  ],
  type: [
    required({
      message: "Discount type is required",
    }),
  ],
  value: [
    required({
      message: "Discount value is required",
    }),
    numericality({
      greaterThan: 0,
      message: "Discount value must be greater than 0",
    }),
  ],
};

export const CreateStoreDiscountModal = ({
                                           open,
                                           onClose,
                                           loading,
                                           handleCreate,
                                           handleSubmit,
                                           invalid,
                                           initialValues,
                                           readOnly = [],
                                           ...rest
                                         }) => {

  return (
    <Modal
      open={open}
      onClose={onClose}
      title="New store discount"
      primaryAction={{
        content: "Create",
        loading: loading,
        onAction: handleSubmit(handleCreate),
        disabled: invalid || loading,
      }}
    >
      <Modal.Section>
        <Form onSubmit={handleSubmit(handleCreate)}>
          <FormLayout>
            <Field
              component={TextFieldAdapter}
              name="name"
              label="Store domain"
              type="text"
              placeholder="E.g, example.myshopify.com"
              validate={validate.name}
              normalize={value => value.replace(/\s+/g, "")}
              readOnly={readOnly.includes("name")}
            />
            <Field
              component={SelectFieldAdapter}
              name="type"
              label="Discount type"
              options={[
                {
                  label: "Percent",
                  value: "percent",
                },
                {
                  label: "Fixed",
                  value: "fixed",
                },
              ]}
              validate={validate.type}
            />
            <Field
              component={TextFieldAdapter}
              name="value"
              label="Discount value"
              type="number"
              validate={validate.value}
            />
          </FormLayout>
        </Form>
      </Modal.Section>
    </Modal>
  );
};

CreateStoreDiscountModal.defaultProps = {
  initialValues: initialValues(),
};
