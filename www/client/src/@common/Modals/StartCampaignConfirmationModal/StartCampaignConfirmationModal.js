import React, { useMemo, useState } from "react";
import { FormLayout, Modal, TextContainer, TextField, TextStyle } from "@shopify/polaris";

export const StartCampaignConfirmationModal = ({
  open,
  onClose,
  onSuccess,
  cancel = "Cancel",
  confirm = "Deploy",
  loading = false,
}) => {
  const [value, setValue] = useState(0);

  const first = useMemo(() => {
    return Math.floor(Math.random() * (100 - 1 + 1)) + 1;
  }, []);

  const second = useMemo(() => {
    return Math.floor(Math.random() * (100 - 1 + 1)) + 1;
  }, []);

  const result = useMemo(() => {
    return first + second;
  }, [first, second]);

  return (
    <Modal
      open={open}
      title={"Confirm deploy modal"}
      onClose={onClose}
      secondaryActions={[
        {
          content: cancel,
          onAction: onClose,
        },
      ]}
      primaryAction={{
        content: confirm,
        loading,
        onAction: onSuccess,
        disabled: parseInt(value) !== result,
      }}
    >
      <Modal.Section>
        <TextContainer>
          Are you sure you want to launch a campaign? Solve the equation below to confirm
          <TextStyle variation={"code"}>
            {" "}
            {first} + {second} ={" "}
          </TextStyle>
        </TextContainer>
        <FormLayout>
          <TextField label={"Result"} onChange={setValue} value={value} type={"number"} />
        </FormLayout>
      </Modal.Section>
    </Modal>
  );
};
