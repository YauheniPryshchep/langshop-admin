import React, { useEffect } from "react";
import { Form, FormLayout, Modal } from "@shopify/polaris";
import TextFieldAdapter from "@components/TextFieldAdapter";
import { Field } from "redux-form";
import { required } from "redux-form-validators";

const validate = {
  name: [
    required({
      message: "Store domain is required",
    }),
    value => {
      if (value.match(/^[\w-]+[.]myshopify[.]com$/g)) {
        return;
      }

      return "Domain must match the following pattern: *.myshopify.com";
    },
  ],
};

export const CreateDemoStoreModal = ({ open, onClose, loading, handleCreate, handleSubmit, reset, invalid }) => {
  useEffect(() => {
    reset();
  }, [open]);

  return (
    <Modal
      open={open}
      onClose={onClose}
      title="New demo store"
      primaryAction={{
        content: "Create",
        loading: loading,
        onAction: handleSubmit(handleCreate),
        disabled: invalid || loading,
      }}
    >
      <Modal.Section>
        <Form onSubmit={handleSubmit(handleCreate)}>
          <FormLayout>
            <Field
              component={TextFieldAdapter}
              name="name"
              label="Store domain"
              type="text"
              placeholder="E.g, example.myshopify.com"
              validate={validate.name}
              normalize={value => value.replace(/\s+/g, "")}
            />
          </FormLayout>
        </Form>
      </Modal.Section>
    </Modal>
  );
};
