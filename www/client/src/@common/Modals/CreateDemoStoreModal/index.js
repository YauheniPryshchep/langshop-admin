import { CreateDemoStoreModal } from "./CreateDemoStoreModal";
import { reduxForm } from "redux-form";

export default reduxForm({
  form: "create-demo-store",
})(CreateDemoStoreModal);
