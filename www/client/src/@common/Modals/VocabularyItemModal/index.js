import { createVocabulariesActions } from "@store/vocabularies";
import { VocabularyItemModal } from "./VocabularyItemModal";
import { connect } from "react-redux";
import { reduxForm } from "redux-form";

const mapDispatch = {
  createVocabulariesActions,
};

export default connect(
  null,
  mapDispatch
)(
  reduxForm({
    form: "create-vocabulary-item",
    enableReinitialize: true,
  })(VocabularyItemModal)
);
