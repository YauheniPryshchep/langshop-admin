import React, { useCallback, useEffect, useMemo } from "react";
import { Button, Form, FormLayout, Modal } from "@shopify/polaris";
import { RefreshMajor } from "@shopify/polaris-icons";
import TextFieldAdapter from "@components/TextFieldAdapter";
import { languages } from "../../../@store/config/languages";
import { Field, getFormValues } from "redux-form";
import { required } from "redux-form-validators";
import { get } from "lodash";
import { useSelector } from "react-redux";
import useToggle from "../../../@hooks/useToggle";

const validate = {
  lang: [required()],
  value: [
    required({
      message: "Required",
    }),
  ],
};

export const VocabularyItemModal = ({
  form,
  open,
  onClose,
  loading,
  handleCreate,
  handleSubmit,
  reset,
  invalid,
  change,
  createVocabulariesActions,
  initialValues,
}) => {
  const [isCreating, toogleCreating] = useToggle();


  useEffect(() => {
    reset();
  }, [open]);

  const { original } = useSelector(getFormValues(form));

  const handleSave = useCallback(
    async data => {
      await toogleCreating();
      await createVocabulariesActions(data);
      toogleCreating();
      handleCreate();
    },
    [handleCreate, toogleCreating]
  );

  const fromTitle = useMemo(() => {
    const { from } = initialValues;
    return get(
      languages.find(lang => lang.code === from),
      "title",
      ""
    );
  }, [initialValues]);

  const toTitle = useMemo(() => {
    const { to } = initialValues;

    return get(
      languages.find(lang => lang.code === to),
      "title",
      ""
    );
  }, [initialValues]);

  return (
    <Modal
      open={open}
      onClose={onClose}
      title={`New item to ${fromTitle} -> ${toTitle} memory `}
      large
      primaryAction={{
        content: "Create",
        loading: isCreating,
        onAction: handleSubmit(handleSave),
        disabled: invalid || isCreating,
      }}
    >
      <Modal.Section>
        <Form onSubmit={handleSubmit(handleSave)}>
          <FormLayout>
            <div className={"File-Item-Wrapper"}>
              <div className={"File-Item File-Item--Source"}>
                <Field
                  component={TextFieldAdapter}
                  name="original"
                  label="Original text"
                  placeholder="Original text"
                  type="text"
                  labelHidden
                  multiline={true}
                  validate={validate.value}
                />
              </div>
              <div className={"File-Item File-Item--Button"}>
                <Button icon={RefreshMajor} onClick={() => change(`translated`, original)} />
              </div>
              <div className={"File-Item File-Item--Target"}>
                <Field
                  component={TextFieldAdapter}
                  name="translated"
                  label="Translated text"
                  placeholder="Translated text"
                  type="text"
                  labelHidden
                  multiline={true}
                  validate={validate.value}
                />
              </div>
            </div>
          </FormLayout>
        </Form>
      </Modal.Section>
    </Modal>
  );
};
