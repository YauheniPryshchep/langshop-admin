import React, { useMemo } from "react";
import { FormLayout, Stack, TextStyle } from "@shopify/polaris";

export const PricePreview = ({ amount, shopifyFee }) => {
  const value = useMemo(() => {
    if (!amount) {
      return 0;
    }

    if (!shopifyFee) {
      return amount;
    }
    return parseInt(amount) * (100 / 80);
  }, [amount, shopifyFee]);

  return (
    <FormLayout>
      <Stack>
        <Stack.Item fill>
          <div />
        </Stack.Item>
        <Stack.Item>
          <TextStyle variation={"strong"}>Total Price:</TextStyle>${value.toString()}
        </Stack.Item>
      </Stack>
    </FormLayout>
  );
};
