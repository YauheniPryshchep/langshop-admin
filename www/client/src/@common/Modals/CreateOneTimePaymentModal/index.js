import { initialValues } from "@store/one-time-charge";
import { CreateOneTimePaymentModal } from "./CreateOneTimePaymentModal";
import { reduxForm } from "redux-form";

export default reduxForm({
  form: "create-one-time-charge",
  initialValues: initialValues(),
})(CreateOneTimePaymentModal);
