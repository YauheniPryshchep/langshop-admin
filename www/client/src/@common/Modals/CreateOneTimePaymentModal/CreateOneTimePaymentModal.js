import React, { useEffect } from "react";
import { Form, FormLayout, Modal } from "@shopify/polaris";
import TextFieldAdapter from "@components/TextFieldAdapter";
import { CheckboxFieldAdapter } from "@components/CheckboxFieldAdapter/CheckboxFieldAdapter";
import { PricePreview } from "./PricePreview/PricePreview";
import { required, numericality } from "redux-form-validators";
import { Field, getFormValues } from "redux-form";
import { useSelector } from "react-redux";

const validate = {
  name: [
    required({
      message: "Store domain is required",
    }),
    value => {
      if (value.match(/^[\w-]+[.]myshopify[.]com$/g)) {
        return;
      }

      return "Domain must match the following pattern: *.myshopify.com";
    },
  ],
  amount: [
    numericality({
      greaterThan: 0,
      message: "Amount value must be greater than 0",
    }),
  ],
  description: [required()],
};

export const CreateOneTimePaymentModal = ({
  form,
  open,
  onClose,
  loading,
  handleCreate,
  handleSubmit,
  reset,
  invalid,
  readOnly = [],
}) => {
  useEffect(() => {
    reset();
  }, [open]);

  const { amount, shopifyFee } = useSelector(getFormValues(form));

  return (
    <Modal
      open={open}
      onClose={onClose}
      title="New store charge"
      primaryAction={{
        content: "Create",
        loading: loading,
        onAction: handleSubmit(handleCreate),
        disabled: invalid || loading,
      }}
    >
      <Modal.Section>
        <Form onSubmit={handleSubmit(handleCreate)}>
          <FormLayout>
            <Field
              component={TextFieldAdapter}
              name="name"
              label="Store domain"
              type="text"
              placeholder="E.g, example.myshopify.com"
              validate={validate.name}
              normalize={value => value.replace(/\s+/g, "")}
              readOnly={readOnly.includes("name")}
              disabled={readOnly.includes("name")}
            />
            <Field
              component={TextFieldAdapter}
              name="amount"
              label="Amount"
              type={"number"}
              validate={validate.amount}
            />

            <Field component={CheckboxFieldAdapter} name="shopifyFee" label="Include Shopify fee" />
            <Field
              component={TextFieldAdapter}
              name="description"
              label="Charge description"
              type="text"
              multiline
              validate={validate.description}
            />
            <PricePreview amount={amount} shopifyFee={shopifyFee} />
          </FormLayout>
        </Form>
      </Modal.Section>
    </Modal>
  );
};
