import React, { useCallback, useMemo } from "react";
import { Filters, Layout, Modal, ResourceList, TextStyle } from "@shopify/polaris";
import { usePaginator } from "../../../@hooks/usePaginator";
import { languages } from "../../../@store/config/languages";
import { LanguagesFlagIcon } from "../../../@components/LanguagesFlagIcon/LanguagesFlagIcon";

export const LanguagesModal = ({ open, onClose, onSuccess, exclude, dir }) => {
  const data = useMemo(() => {
    return languages.filter(lang => !exclude.includes(lang.code));
  }, [exclude]);

  const { items, search, onSearchChange, onSearchClear } = usePaginator(data);

  const renderItem = useCallback(
    ({ code, title }, id) => {
      return (
        <ResourceList.Item id={id} onClick={() => onSuccess(dir, code)} media={<LanguagesFlagIcon code={code} />}>
          <Layout>
            <Layout.Section>
              <TextStyle variation="strong">{title}</TextStyle>
            </Layout.Section>
          </Layout>
        </ResourceList.Item>
      );
    },
    [items]
  );

  return (
    <Modal open={open} onClose={onClose} title={`New item to memory`}>
      <Modal.Section>
        <ResourceList
          items={items}
          renderItem={renderItem}
          filterControl={
            <Filters
              queryValue={search}
              queryPlaceholder="Search languages ..."
              filters={[]}
              appliedFilters={[]}
              onQueryChange={onSearchChange}
              onQueryClear={onSearchClear}
              onClearAll={onSearchClear}
            />
          }
        />
      </Modal.Section>
    </Modal>
  );
};
