import React, { useEffect, useMemo } from "react";
import { Form, FormLayout, Modal } from "@shopify/polaris";
import { TextFieldAdapter } from "@components/TextFieldAdapter/TextFieldAdapter";
import { SelectFieldAdapter } from "@components/SelectFieldAdapter/SelectFieldAdapter";
import { FREE_PLAN, PLAN_ENTERPRISE } from "@defaults/constants";
import { isTestPlan } from "../../../@utils/humanShopifyPlan";
import { required, numericality } from "redux-form-validators";
import { Field, getFormValues } from "redux-form";
import { useSelector } from "react-redux";
import get from "lodash/get";

const validate = {
  value: [
    required({
      message: "Commission value is required",
    }),
  ],

  price: (value, values) => {
    let plan = get(values, "planId", "");

    if (plan === FREE_PLAN) {
      return undefined;
    }
    return numericality({
      ">=": 399,
      "<=": 1999,
      message: "Commission value must be greater than 399 and less than 1999",
    })(value);
  },
};

export const ChangePlanModal = ({
  store,
  demoStore,
  form,
  open,
  onClose,
  loading,
  handleCreate,
  handleSubmit,
  reset,
  invalid,
}) => {
  useEffect(() => {
    reset();
  }, [open]);

  const values = useSelector(getFormValues(form));

  const isDemo = useMemo(() => {
    return !!demoStore;
  }, [demoStore]);

  const isEnterpriseDisabled = useMemo(() => {
    const plan = get(store, "data.plan_display_name", "");
    const subscription = get(store, "subscription.plan_id", 0);

    return (isTestPlan(plan) && !isDemo) || subscription === parseInt(PLAN_ENTERPRISE);
  }, [store, isDemo]);

  const isFreeDisabled = useMemo(() => {
    const plan = get(store, "subscription.plan_id", 0);

    return plan === parseInt(FREE_PLAN);
  }, [store]);

  const planId = useMemo(() => {
    return get(values, "planId", "");
  }, [values]);

  return (
    <Modal
      open={open}
      onClose={onClose}
      title="Change store plan"
      primaryAction={{
        content: "Change",
        loading: loading,
        onAction: handleSubmit(handleCreate),
        disabled: invalid || loading,
      }}
    >
      <Modal.Section>
        <Form onSubmit={handleSubmit(handleCreate)}>
          <FormLayout>
            <Field
              component={SelectFieldAdapter}
              name="planId"
              type="number"
              options={[
                { label: "Select plan", value: "" },
                { label: "Free plan", value: FREE_PLAN, disabled: isFreeDisabled },
                { label: "Enterprise plan", value: PLAN_ENTERPRISE, disabled: isEnterpriseDisabled },
              ]}
              validate={[required({ message: "Plan is required" })]}
            />
            {planId === PLAN_ENTERPRISE && (
              <Field
                component={TextFieldAdapter}
                name="price"
                label="Price"
                type="number"
                helpText={"The reason why you change store plan"}
                min={399}
                max={1999}
                validate={[validate.price]}
                disabled={loading}
              />
            )}
            <Field
              component={TextFieldAdapter}
              name="note"
              label="Note"
              type="text"
              helpText={"The reason why you change store plan"}
              multiline={true}
              rows={2}
              validate={[required()]}
              disabled={loading}
            />
          </FormLayout>
        </Form>
      </Modal.Section>
    </Modal>
  );
};
