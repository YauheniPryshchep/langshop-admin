import { ChangePlanModal } from "./ChangePlanModal";
import { reduxForm } from "redux-form";

export default reduxForm({
  form: "change-plan-modal",
})(ChangePlanModal);
