import React, { useEffect } from "react";
import { Form, FormLayout, Modal } from "@shopify/polaris";
import { TextFieldAdapter } from "@components/TextFieldAdapter/TextFieldAdapter";
import { DatePickerFieldAdapter } from "@components/DatePickerFieldAdapter/DatePickerFieldAdapter";
import { required, numericality } from "redux-form-validators";
import { Field } from "redux-form";

const validate = {
  value: [
    required({
      message: "Commission value is required",
    }),
    numericality({
      ">=": 1,
      "<=": 100,
      message: "Commission value must be greater than 0 and less than 100",
    }),
  ],
};

export const CommissionModal = ({
  open,
  onClose,
  loading,
  handleCreate,
  handleSubmit,
  reset,
  invalid,
  disableDatesBefore,
}) => {
  useEffect(() => {
    reset();
  }, [open]);

  return (
    <Modal
      open={open}
      onClose={onClose}
      title="Set new commission for current partner"
      primaryAction={{
        content: "Set Commission",
        loading: loading,
        onAction: handleSubmit(handleCreate),
        disabled: invalid || loading,
      }}
    >
      <Modal.Section>
        <Form onSubmit={handleSubmit(handleCreate)}>
          <FormLayout>
            <Field
              component={TextFieldAdapter}
              name="commission"
              label="Commission value (percent)"
              type="number"
              validate={validate.value}
            />
            <Field
              component={DatePickerFieldAdapter}
              fieldLabel={"Commission from"}
              name="created_at"
              fieldHelpText="The date from which the start new commission for partner"
              validate={[required()]}
              disableDatesBefore={new Date(disableDatesBefore).addDays(1)}
            />
          </FormLayout>
        </Form>
      </Modal.Section>
    </Modal>
  );
};
