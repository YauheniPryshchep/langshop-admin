import { CommissionModal } from "./CommissionModal";
import { reduxForm } from "redux-form";

export default reduxForm({
  form: "user-commission",
})(CommissionModal);
