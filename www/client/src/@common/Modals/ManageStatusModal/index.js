import { ManageStatusModal } from "./ManageStatusModal";
import { reduxForm } from "redux-form";

export default reduxForm({
  form: "manage-payout-status",
  initialValues: {
    status: "2",
  },
})(ManageStatusModal);
