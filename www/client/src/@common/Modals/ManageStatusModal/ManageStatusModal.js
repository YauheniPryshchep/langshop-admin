import React, { useEffect } from "react";
import { Form, FormLayout, Modal, TextContainer } from "@shopify/polaris";
import SelectFieldAdapter from "@components/SelectFieldAdapter";
import { Field } from "redux-form";

export const ManageStatusModal = ({ open, onClose, loading, handleCreate, handleSubmit, reset, invalid }) => {
  useEffect(() => {
    reset();
  }, [open]);

  return (
    <Modal
      open={open}
      onClose={onClose}
      title="Manage payout status"
      primaryAction={{
        content: "Apply",
        loading: loading,
        onAction: handleSubmit(handleCreate),
        disabled: invalid || loading,
      }}
    >
      <Modal.Section>
        <Form onSubmit={handleSubmit(handleCreate)}>
          <FormLayout>
            <Field
              component={SelectFieldAdapter}
              name="status"
              label="Status"
              options={[
                {
                  label: "Paid",
                  value: "2",
                },
                {
                  label: "Declined",
                  value: "3",
                },
              ]}
            />
            <TextContainer>Be careful when changing payout status</TextContainer>
          </FormLayout>
        </Form>
      </Modal.Section>
    </Modal>
  );
};
