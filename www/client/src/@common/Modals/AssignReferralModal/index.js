import { AssignReferralModal } from "./AssignReferralModal";
import { reduxForm } from "redux-form";

export default reduxForm({
  form: "assign-referral",
})(AssignReferralModal);
