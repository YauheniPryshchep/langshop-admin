import React, { useEffect } from "react";
import { Form, FormLayout, Modal } from "@shopify/polaris";
import { AsyncSelectAdapter } from "@components/AsyncSelectAdapter/AsyncSelectAdapter";
import { TextFieldAdapter } from "../../../@components/TextFieldAdapter/TextFieldAdapter";
import { DatePickerFieldAdapter } from "../../../@components/DatePickerFieldAdapter/DatePickerFieldAdapter";
import { required, numericality } from "redux-form-validators";
import { Field } from "redux-form";

const validate = {
  name: [
    required({
      message: "Store domain is required",
    }),
    value => {
      if (value.match(/^[\w-]+[.]myshopify[.]com$/g)) {
        return;
      }

      return "Domain must match the following pattern: *.myshopify.com";
    },
  ],
  amount: [
    numericality({
      greaterThan: 0,
      message: "Amount value must be greater than 0",
    }),
  ],
  description: [required()],
};

export const AssignReferralModal = ({
  open,
  onClose,
  loading,
  handleCreate,
  handleSubmit,
  reset,
  invalid,
  readOnly = [],
}) => {
  useEffect(() => {
    reset();
  }, [open]);

  return (
    <Modal
      open={open}
      onClose={onClose}
      title="Assign store to current partner"
      primaryAction={{
        content: "Assign",
        loading: loading,
        onAction: handleSubmit(handleCreate),
        disabled: invalid || loading,
      }}
    >
      <Modal.Section>
        <Form onSubmit={handleSubmit(handleCreate)}>
          <FormLayout>
            <Field
              component={AsyncSelectAdapter}
              name="shop"
              label="Store domain"
              type="text"
              placeholder="E.g, example.myshopify.com"
              validate={validate.name}
              normalize={value => value.replace(/\s+/g, "")}
              readOnly={readOnly.includes("name")}
              disabled={readOnly.includes("name") || loading}
            />
            <Field component={DatePickerFieldAdapter} name="created_at" validate={[required()]} />
            <Field
              component={TextFieldAdapter}
              name="reason"
              label="Reason"
              type="text"
              helpText={"The reason why you registered this store in our referral program"}
              multiline={true}
              validate={[required()]}
              disabled={loading}
            />
          </FormLayout>
        </Form>
      </Modal.Section>
    </Modal>
  );
};
