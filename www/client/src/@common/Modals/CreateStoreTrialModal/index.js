import { CreateStoreTrialModal } from "./CreateStoreTrialModal";
import { reduxForm } from "redux-form";

export default reduxForm({
  form: "create-store-trial",
})(CreateStoreTrialModal);
