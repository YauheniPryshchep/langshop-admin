import { CreateCompanyModal } from "./CreateCompanyModal";
import { reduxForm } from "redux-form";

export default reduxForm({
  form: "create-company",
})(CreateCompanyModal);
