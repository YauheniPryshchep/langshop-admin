import React, { useEffect } from "react";
import { Form, FormLayout, Modal } from "@shopify/polaris";
import TextFieldAdapter from "@components/TextFieldAdapter";
import DatePicker from "@components/DatePicker";
import { Field } from "redux-form";
import { required } from "redux-form-validators";

const validate = {
  title: [
    required({
      message: "Company title is required",
    }),
  ],
  description: [
    required({
      message: "Company description is required",
    }),
  ],
  subject: [
    required({
      message: "Company subject is required",
    }),
  ],
  start_at: [
    required({
      message: "Company start date is required",
    }),
  ],
};

export const CreateCompanyModal = ({ open, onClose, loading, handleCreate, handleSubmit, reset, invalid }) => {
  useEffect(() => {
    reset();
  }, [open]);

  return (
    <Modal
      open={open}
      onClose={onClose}
      title="New company"
      primaryAction={{
        content: "Create",
        loading: loading,
        onAction: handleSubmit(handleCreate),
        disabled: invalid || loading,
      }}
    >
      <Modal.Section>
        <Form onSubmit={handleSubmit(handleCreate)}>
          <FormLayout>
            <Field
              component={TextFieldAdapter}
              name="title"
              label="Company title"
              type="text"
              placeholder="E.g, some company"
              validate={validate.title}
              normalize={value => value.replace(/\s+/g, "")}
            />
            <Field
              component={TextFieldAdapter}
              name="description"
              label="Company description"
              type="text"
              placeholder="E.g, Company for promotion"
              validate={validate.description}
              normalize={value => value.replace(/\s+/g, "")}
            />
            <Field
              component={TextFieldAdapter}
              name="subject"
              label="Company subject"
              type="text"
              placeholder="E.g, promotion"
              validate={validate.subject}
              normalize={value => value.replace(/\s+/g, "")}
            />
            <Field
              component={DatePicker}
              disableDatesBefore={new Date()}
              name="start_at"
              selected="Date"
              validate={validate.start_at}
            />
          </FormLayout>
        </Form>
      </Modal.Section>
    </Modal>
  );
};
