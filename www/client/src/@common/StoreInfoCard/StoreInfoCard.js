import React, { useEffect, useMemo } from "react";
import { Card, Link, SkeletonBodyText, TextContainer } from "@shopify/polaris";
import useCancelToken from "@hooks/useCancelToken";
import StorePlan from "@common/StorePlan";
import LangShopPlan from "@common/LangShopPlan";
import moment from "moment-timezone";
import ISO6391 from "iso-639-1";

export const StoreInfoCard = ({ domain, isFetched, isLoading, store, fetchStoreAction, resetStoreAction }) => {
  const loading = useMemo(() => isLoading || !isFetched, [isLoading, isFetched]);
  const [cancelToken, cancelRequests] = useCancelToken();

  useEffect(() => {
    fetchStoreAction(domain, cancelToken).catch(() => {});
  }, [domain]);

  // On unmount
  useEffect(
    () => () => {
      cancelRequests();
      resetStoreAction();
    },
    []
  );

  const timeNow = useMemo(() => {
    if (!(store && store.data.iana_timezone)) {
      return null;
    }

    return moment
      .unix(moment().unix())
      .tz(store.data.iana_timezone)
      .format("MMMM Do YYYY, h:mm a");
  }, [store]);

  const primaryLanguage = useMemo(() => {
    if (!(store && store.data.primary_locale)) {
      return null;
    }

    return ISO6391.getName(store.data.primary_locale || "en");
  }, [store]);

  if (loading) {
    return (
      <Card title={"Store"} sectioned>
        <SkeletonBodyText lines={5} />
      </Card>
    );
  }

  const linkSectionMarkup = (
    <Card.Section>
      <Link url={`https://${domain}`} external={true}>
        {domain}
      </Link>
    </Card.Section>
  );

  const appVersionMarkup = store && store.app ? <p>App generation {store.app}.X</p> : null;

  const planSectionMarkup =
    store && store.data.plan_display_name ? (
      <Card.Section title={"Shopify plan"}>
        <StorePlan badge={false} store={store} />
      </Card.Section>
    ) : null;

  const langshopPlan = store ? (
    <Card.Section title={"LangShop plan"}>
      <LangShopPlan badge={false} store={store} />
    </Card.Section>
  ) : null;

  const infoSectionMarkup =
    store && store.data.email ? (
      <Card.Section title={"Emails"}>
        <Link url={`https://support.devit-team.com/#ticket/create`} external={true}>
          {store.data.email}
        </Link>
        {store.data.customer_email && store.data.customer_email !== store.data.email ? (
          <Link url={`https://support.devit-team.com/#ticket/create`} external={true}>
            {store.data.customer_email}
          </Link>
        ) : null}
      </Card.Section>
    ) : null;

  const additionalSectionMarkup =
    primaryLanguage && timeNow ? (
      <Card.Section title={"Additional information"}>
        <TextContainer spacing={"tight"}>
          <p>{primaryLanguage} as a preferred language</p>
          <p>{timeNow} is local time</p>
          {appVersionMarkup}
        </TextContainer>
      </Card.Section>
    ) : null;

  return (
    <Card title={store && store.data.name ? store.data.name : "Store"}>
      {linkSectionMarkup}
      {planSectionMarkup}
      {langshopPlan}
      {infoSectionMarkup}
      {additionalSectionMarkup}
    </Card>
  );
};
