import { store, fetchStoreAction, isFetched, isLoading, resetStoreAction } from "@store/store";
import { StoreInfoCard } from "./StoreInfoCard";
import { createStructuredSelector } from "reselect";
import { connect } from "react-redux";

const mapState = createStructuredSelector({
  store,
  isFetched,
  isLoading,
});

const mapDispatch = {
  fetchStoreAction,
  resetStoreAction,
};

export default connect(mapState, mapDispatch)(StoreInfoCard);
