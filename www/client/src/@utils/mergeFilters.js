import isUndefined from "lodash/isUndefined"
import map from "lodash/map"

export const mergeFilters = (filters, object) => {
  return map(filters, (filter) => {

    if (!filter.hasOwnProperty("key")) {
      return filter;
    }

    const key = filter.key;

    if (object.hasOwnProperty(key) && !isUndefined(object[key])) {
      return {
        ...filter,
        value: filter.hasOwnProperty("replaceFilter") ? filter.replaceFilter(object[key]) : object[key]
      }
    }
    return filter
  })
};
