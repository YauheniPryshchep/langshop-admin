export const storePlan = store => {
  if (!(store && store.data.plan_display_name)) {
    return null;
  }

  return store.data.plan_display_name
    .split("_")
    .map(part => part[0].toUpperCase() + part.slice(1))
    .join(" ");
};
