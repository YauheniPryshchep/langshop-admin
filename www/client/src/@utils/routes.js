export const shopifyPartnerUrl = (shopifyPartnerId, path = "") =>
  `https://partners.shopify.com/${shopifyPartnerId}${path}`;

export const storeName = domain => domain.replace(".myshopify.com", "");

export const storeDomain = name => `${name}.myshopify.com`;
