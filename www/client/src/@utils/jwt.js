export const getToken = () => {
  return window.localStorage.getItem("token");
};

export const setToken = token => {
  return window.localStorage.setItem("token", token);
};

export const removeToken = () => {
  return window.localStorage.removeItem("token");
};

export const getTokenPayload = token => {
  if (!token) {
    return null;
  }

  const parts = token.split(".");
  if (parts.length !== 3) {
    return null;
  }

  let payload;

  try {
    payload = JSON.parse(atob(parts[1]));
  } catch (e) {
    return null;
  }

  if (!payload) {
    return null;
  }

  return typeof payload.exp !== "undefined" ? payload : null;
};

export const getTokenExpiresAt = token => {
  if (!token) {
    return null;
  }

  const payload = getTokenPayload(token);

  return payload ? payload.exp : null;
};
