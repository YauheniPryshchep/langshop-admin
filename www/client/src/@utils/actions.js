export const isSkippedAction = (action, skip) => {
  return action && skip.includes(action.type);
};

export const isFailAction = action => {
  return action && action.type && action.type.match(/_FAIL$/);
};

export const isRequestAction = action => {
  return action && action.type && action.payload && action.payload.request && !action.type.match(/_SUCCESS$|_FAIL$/);
};

export const isRequestFailAction = action => {
  return isFailAction(action) && action.error && action.error.request;
};

export const isClientRequestAction = (action, client = "default") => {
  if (!isRequestAction(action)) {
    return false;
  }

  if (client === "default" && !action.payload.client) {
    return true;
  }

  return action.payload.client === client;
};
