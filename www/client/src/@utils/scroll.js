export const scrollTo = (element, to, duration = 500, onDone) => {
  let start = element.scrollTop,
    change = to - start,
    startTime = performance.now(),
    now,
    elapsed,
    t;

  const animateScroll = () => {
    now = performance.now();
    elapsed = now - startTime;
    t = elapsed / duration;

    element.scrollTop = start + change * easeInOutQuad(t);

    if (t < 1) window.requestAnimationFrame(animateScroll);
    else onDone && onDone();
  };

  animateScroll();
};

const easeInOutQuad = t => (t < 0.5 ? 2 * t * t : -1 + (4 - 2 * t) * t);
