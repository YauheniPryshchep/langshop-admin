export const SHOW_NEWS = "main-news-show";
export const UPDATE_NEWS = "main-news-update";
export const SHOW_USERS = "main-users-show";
export const UPDATE_USERS = "main-users-update";
export const SHOW_USERS_ROLES = "main-roles-show";
export const UPDATE_USERS_ROLES = "main-roles-update";
export const SHOW_STORES_HISTORY = "langshop-storeshistory-show";
export const SHOW_DEMO_STORES = "langshop-stores-demo-show";
export const UPDATE_DEMO_STORES = "langshop-stores-trials-update";
export const SHOW_STORES_TRIALS = "langshop-stores-trials-show";
export const UPDATE_STORES_TRIALS = "langshop-stores-trials-show";
export const SHOW_STORES_DISCOUNTS = "langshop-stores-discounts-show";
export const UPDATE_STORES_DISCOUNTS = "langshop-stores-discounts-update";
export const SHOW_LOCALES = "locales-show";
export const UPDATE_LOCALES = "locales-update";
export const SHOW_CAMPAIGNS = "campaigns-show";
export const UPDATE_CAMPAIGNS = "campaigns-update";
export const SHOW_VOCABULARIES = "vocabularies-show";
export const UPDATE_VOCABULARIES = "vocabularies-update";
export const CREATE_ONE_TIME_PAYMENT_SCOPES = "langshop-stores-one-time-payment-update";
export const SHOW_COUPONS = "coupons-show";
export const UPDATE_COUPONS = "coupons-update";
export const SHOW_CUSTOMERS = "customers-show";
export const SHOW_PARTNERS_USERS = "partners-users-show";
export const SHOW_PARTNERS_STORES = "partners-stores-show";
export const SHOW_PARTNERS_PAYOUTS = "partners-payouts-show";
export const UPDATE_PARTNERS_PAYOUTS = "partners-payouts-update";
export const SHOW_FINANCIAL = "financial-show";
export const SHOW_ANALYTICS = "analytics-show";

export const hasScope = (profileScopes, allowedScope, exact = true) => {
  return profileScopes.find(scope => (exact ? scope === allowedScope : scope.includes(allowedScope)));
};

export const hasEveryScope = (profileScopes, allowedScopes) => {
  return allowedScopes.every(scope => profileScopes.includes(scope));
};

export const hasSomeScope = (profileScopes, allowedScopes) => {
  return allowedScopes.some(scope => profileScopes.includes(scope));
};
