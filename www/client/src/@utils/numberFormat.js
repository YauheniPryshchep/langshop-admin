import isNumber from "lodash/isNumber";

export const numberFormat = number => {
  if (!isNumber(number)) {
    return number;
  }
  return parseFloat(number)
    .toFixed(2)
    .toLocaleString();
};
