export const UNAUTHORIZED = 401;
export const NOT_FOUND = 404;

export const isResponseError = (response, status) => {
  if (!response) {
    return false;
  }

  if (!response.status) {
    return false;
  }

  if (!status) {
    return response.status >= 400;
  }

  return response.status === status;
};
