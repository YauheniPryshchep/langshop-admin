import { numberFormat } from "@utils/numberFormat";

export const moneyFormat = number => {
  return `$${numberFormat(number)}`;
};
