export const sanitizePage = value => {
  if (!value) {
    return 1;
  }

  if (value < 1) {
    return 1;
  }

  return parseInt(value, 10);
};

export const sanitizeLimit = (value, allowed, defaultValue) => {
  if (!value) {
    return defaultValue;
  }

  value = parseInt(value, 10);

  if (!allowed.includes(value)) {
    return defaultValue;
  }

  return value;
};

export const sanitizeSort = (value, allowed, defaultValue) => {
  if (!value) {
    return defaultValue;
  }

  if (allowed.includes(value)) {
    return value;
  }

  return defaultValue;
};

export const sanitizeFilters = (value, defaultValue) => {
  if (!value) {
    return defaultValue;
  }

  return value;
};

export const sanitizeString = value => {
  if (!value) {
    return "";
  }

  return typeof value === "string" ? value : "";
};
