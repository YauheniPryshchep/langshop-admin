export const diffInDaysFromNow = dateString => {
  let compareDate = new Date(dateString);
  let now = new Date();

  let differenceInTime = now.getTime() - compareDate.getTime();

  return differenceInTime / (1000 * 3600 * 24);
};

Date.prototype.addDays = function(days) {
  let date = new Date(this.valueOf());
  date.setDate(date.getDate() + days);
  return date;
};
