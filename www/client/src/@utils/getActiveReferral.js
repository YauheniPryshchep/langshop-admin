import get from "lodash/get";
import last from "lodash/last";

export default store => {
  const referrals = get(store, "referrals", []);
  return last(referrals);
};
