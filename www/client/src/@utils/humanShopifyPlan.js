import includes from "lodash/includes";

export const plans = {
  affiliate: "Affiliate",
  basic: "Basic",
  business: "Business",
  cancelled: "Cancelled",
  custom: "Custom",
  dormant: "Dormant",
  fraudulent: "Fraudulent",
  frozen: "Frozen",
  npo_full: "Npo Full",
  npo_lite: "Npo Lite",
  open_learning: "Open Learning",
  partner_test: "Partner Test",
  paused: "Paused",
  plus_partner_sandbox: "Plus Partner Sandbox",
  professional: "Professional",
  shopify_plus: "Shopify Plus",
  staff: "Staff",
  staff_business: "Staff Business",
  starter: "Starter",
  trial: "Trial",
  unlimited: "Unlimited",
};

export default plan => {
  return plans[plan];
};

export const isTestPlan = plan => {
  return includes(["affiliate", "plus_partner_sandbox", "partner_test"], plan);
};
