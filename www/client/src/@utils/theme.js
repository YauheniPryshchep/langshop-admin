export const theme = {
  colors: {
    topBar: {
      background: "#5c6ac4",
    },
  },
  logo: {
    width: 40,
    topBarSource: "/assets/logo/langshop.svg",
    url: "/",
  },
};
