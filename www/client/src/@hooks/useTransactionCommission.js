import { useCallback } from "react";
import { CONVERSION_RATE } from "@defaults/constants";
import isEmpty from "lodash/isEmpty";
import first from "lodash/first";
import filter from "lodash/filter";
import moment from "moment";

export const useTransactionCommission = commissions => {
  const getTransactionCommission = useCallback(
    transaction => {
      if (isEmpty(commissions)) {
        return transaction["netAmount"]["amount"] * CONVERSION_RATE;
      }

      const filteredCommissions = filter(commissions, commission => {
        return moment(commission["created_at"]).isSameOrBefore(moment(transaction["createdAt"]));
      });

      if (isEmpty(filteredCommissions)) {
        return transaction["netAmount"]["amount"] * CONVERSION_RATE;
      }

      const commission = first(filteredCommissions);

      return transaction["netAmount"]["amount"] * (commission["commission"] / 100);
    },
    [commissions]
  );

  return [getTransactionCommission];
};
