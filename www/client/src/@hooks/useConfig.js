import { config as configSelector } from "@store/config";
import { useSelector } from "react-redux";

export default () => {
  return useSelector(configSelector);
};
