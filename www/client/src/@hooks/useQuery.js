import { useCallback, useMemo } from "react";
import { useHistory, useLocation } from "react-router-dom";
import qs from "qs";

export default () => {
  const { search } = useLocation();
  const history = useHistory();

  const query = useMemo(() => qs.parse(search.substr(1)), [search]);

  const setQuery = useCallback(query => {
    history.replace({
      search: qs.stringify(query),
    });
  }, []);

  return [query, setQuery];
};
