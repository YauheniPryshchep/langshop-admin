import get from "lodash/get";

export default referral => {
  return {
    cancelledAt: get(referral, "cancelled_at", null),
    createdAt: get(referral, "created_at", null),
    id: get(referral, "id", 1),
    ref: get(referral, "ref", null),
    status: get(referral, "status", null),
    storeId: get(referral, "store_id", 0),
    updatedAt: get(referral, "updated_at", null),
  };
};
