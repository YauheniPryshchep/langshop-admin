import {useMemo} from "react";
import humanShopifyPlan from "../@utils/humanShopifyPlan";
import get from "lodash/get";

export default store => {
  const data = useMemo(() => get(store, "data", {}), [store]);

  const myshopifyDomain = useMemo(() => get(store, 'name', ''), [store]);

  return {
    currency: get(data, `currency`, ""),
    customerEmail: get(data, `customer_email`, ""),
    domain: get(data, `domain`, ""),
    email: get(data, `email`, ""),
    timezone: get(data, `iana_timezone`, ""),
    id: get(data, `id`, 0),
    name: get(data, `name`, ""),
    planDisplayName: humanShopifyPlan(get(data, `plan_display_name`, "")),
    myshopifyDomain
  };
};
