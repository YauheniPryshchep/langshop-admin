import {useCallback, useMemo} from "react";
import {useHistory, useLocation} from "react-router-dom";
import qs from "qs";
import keys from 'lodash/keys';
import reduce from 'lodash/reduce';
import isArray from 'lodash/isArray';

export default () => {
  const {search} = useLocation();
  const history = useHistory();

  const query = useMemo(() => qs.parse(search.substr(1)), [search]);

  const handleCheckQuery = useCallback(query => {
    return reduce(keys(query), (acc, q) => {
      if (isArray(query[q]) && !query[q].length) {
        return {
          ...acc,
        }
      }
      if (!query[q]) {
        return {
          ...acc
        }
      }
      return {
        ...acc,
        [q]: query[q]
      }

    }, {});
  }, []);

  const setQuery = useCallback(query => {
    const queryFiltered = handleCheckQuery(query);
    // eslint-disable-next-line
    history.replace({
      search: qs.stringify(queryFiltered, {arrayFormat: 'comma', encode: false, encodeValuesOnly: false}),
    });
  }, [history, handleCheckQuery]);

  const arrayToQuery = useCallback((filters) => {
    return reduce(filters,(acc, item) => {
      if (isArray(item.value) && !item.value.length) {
        return {
          ...acc,
        }
      }

      if(!item.value){
        return {
          ...acc
        }
      }
      return {
        ...acc,
        [item.key]: isArray(item.value) ? item.value.join(',') : item.value
      }
    }, {})
  }, []);

  return [query, setQuery, arrayToQuery];
};
