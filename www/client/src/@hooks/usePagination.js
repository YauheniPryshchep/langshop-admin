import {useCallback, useMemo, useState} from "react";
import {chunk} from "lodash";

export default ({items}) => {
  const [limit, setLimit] = useState(25);
  const [page, setPage] = useState(1);

  const pages = useMemo(() => {
    return Math.ceil(items.length / limit) || 1;
  }, [items, limit]);

  const onPreviousPage = useCallback(() => {
    setPage(state => (state === 0 ? state : state - 1));
  }, [setPage]);

  const onNextPage = useCallback(() => {
    setPage(state => state + 1);
  }, [setPage]);

  const onLimitChange = useCallback(
    async newLimit => {
      await setPage(1);
      await setLimit(parseInt(newLimit));
    },
    [limit, setLimit]
  );

  const values = useMemo(() => {
    return chunk([...items], limit)[page - 1];
  }, [items, limit, page]);

  return {
    items: values,
    limit,
    page,
    pages,
    onPreviousPage,
    onNextPage,
    onLimitChange,
  };
};
