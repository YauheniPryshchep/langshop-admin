import {useCallback, useMemo} from "react";
import {useHistory, useLocation} from "react-router-dom";
import qs from "qs";
import keys from 'lodash/keys';
import reduce from 'lodash/reduce';
import isArray from 'lodash/isArray';

const sortOptions = [
  {
    label: "Last event (Newest)",
    value: "last-event-desc",
    field: "last_event.timestamp",
    direction: "desc",
  },
  {
    label: "Last event (Oldest)",
    value: "last-event-asc",
    field: "last_event.timestamp",
    direction: "asc",
  },
  {
    label: "Alphabetically (A-Z)",
    value: "domain-asc",
    field: "name",
    direction: "asc",
  },
  {
    label: "Alphabetically (Z-A)",
    value: "domain-desc",
    field: "name",
    direction: "desc",
  },
];

export default () => {
  const {search} = useLocation();
  const history = useHistory();

  const query = useMemo(() => qs.parse(search.substr(1)), [search]);

  const handleCheckQuery = useCallback(query => {
    return reduce(keys(query), (acc, q) => {
      if (isArray(query[q]) && !query[q].length) {
        return {
          ...acc,
        }
      }
      if (!query[q]) {
        return {
          ...acc
        }
      }
      return {
        ...acc,
        [q]: query[q]
      }

    }, {});
  }, []);

  const setQuery = useCallback(query => {
    const queryFiltered = handleCheckQuery(query);
    // eslint-disable-next-line
    history.replace({
      search: qs.stringify(queryFiltered, {arrayFormat: 'comma', encode: false, encodeValuesOnly: false}),
    });
  }, [history, handleCheckQuery]);

  const arrayToQuery = useCallback((filters) => {
    return reduce(filters, (acc, item) => {
      if (isArray(item.value) && !item.value.length) {
        return {
          ...acc,
        }
      }

      if (!item.value) {
        return {
          ...acc
        }
      }
      return {
        ...acc,
        [item.key]: isArray(item.value) ? item.value.join(',') : item.value
      }
    }, {})
  }, []);

  return [query, setQuery, arrayToQuery];
};
