import { useCallback, useState } from "react";

export default (activated = false) => {
  const [active, setActive] = useState(activated);

  const activate = useCallback(() => setActive(true), [setActive]);

  const deactivate = useCallback(() => setActive(false), [setActive]);

  return [active, activate, deactivate];
};
