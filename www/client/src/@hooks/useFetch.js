import { useCallback, useMemo, useState } from "react";
import { sanitizeFilters, sanitizeLimit, sanitizePage, sanitizeSort, sanitizeString } from "@utils/query";
import { debounce } from "lodash";

export default ({ query, sortOptions, limitOptions, isLoading, total, sortDefault = "asc" }) => {
  const [search, setSearch] = useState(query ? sanitizeString(query.q) : "");
  const [filters, setFilters] = useState(query ? sanitizeFilters(query.filters, []) : "");

  const [fetch, setFetch] = useState({
    filter: search,
    sort: query
      ? sanitizeSort(
          query.sort,
          sortOptions.map(option => option.value),
          sortDefault
        )
      : sortDefault,
    page: query ? sanitizePage(query.page) : 1,
    limit: query ? sanitizeLimit(query.limit, limitOptions, 25) : 25,
  });

  const pages = useMemo(() => {
    return Math.ceil(total / fetch.limit) || 1;
  }, [total, fetch]);

  const onPreviousPage = useCallback(() => {
    if (isLoading) {
      return;
    }
    setFetch(state => {
      let page = state.page - 1;

      if (page > pages) {
        page = pages;
      }

      return {
        ...state,
        page: page,
      };
    });
  }, [isLoading, setFetch, pages]);

  const onNextPage = useCallback(() => {
    if (isLoading) {
      return;
    }

    setFetch(state => ({
      ...state,
      page: state.page + 1,
    }));
  }, [isLoading, setFetch]);

  const onLimitChange = useCallback(
    limit => {
      setFetch(state => ({
        ...state,
        page: 1,
        limit: parseInt(limit, 10),
      }));
    },
    [setFetch]
  );

  const onSortChange = useCallback(
    sort => {
      setFetch(state => ({
        ...state,
        sort,
      }));
    },
    [setFetch]
  );

  const debounceSearchFilter = useCallback(
    debounce(
      filter =>
        setFetch({
          ...fetch,
          page: 1,
          filter,
        }),
      500
    ),
    [fetch, setFetch]
  );

  const onSearchChange = useCallback(
    value => {
      setSearch(value.length > 255 ? search : value);
      debounceSearchFilter(value.length > 255 ? search : value);
    },
    [search, setSearch, debounceSearchFilter]
  );

  const onSearchClear = useCallback(() => {
    setSearch("");
    setFetch(state => ({
      ...state,
      page: 1,
      filter: "",
    }));
  }, [setSearch, setFetch]);

  const resetPageToDefault = useCallback(() => {
    setFetch(state => ({
      ...state,
      page: 1,
      filter: "",
    }));
  }, [setFetch]);

  const onChangeFilters = useCallback(
    newFilter => {
      setFilters(newFilter);
    },
    [setFilters]
  );

  return {
    ...fetch,
    pages,
    search,
    onPreviousPage,
    onNextPage,
    onLimitChange,
    onSortChange,
    onSearchChange,
    onSearchClear,
    resetPageToDefault,
    filters,
    onChangeFilters,
  };
};
