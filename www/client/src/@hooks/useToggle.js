import { useCallback, useState } from "react";

export default (toggled = false) => {
  const [active, setActive] = useState(toggled);

  const setToggled = useCallback(() => setActive(state => !state), [setActive]);

  return [active, setToggled];
};
