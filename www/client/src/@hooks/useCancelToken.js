import { useMemo } from "react";
import axios from "axios";

export default () => {
  const cancelToken = useMemo(() => axios.CancelToken.source(), []);

  return [cancelToken.token, cancelToken.cancel];
};
