import {useMemo} from "react";

export default (page, pages, handleChange) => {

  const pagination = useMemo(() => {
    return {
      hasNext: page < pages,
      hasPrev: page > 1,
      onNext: () => handleChange({
        key: 'page',
        value: page + 1
      }),
      onPrevious: () => handleChange({
        key: 'page',
        value: page - 1
      })
    }
  }, [page, pages, handleChange]);

  const isHasPagination = useMemo(() => {
    return pages > 1;
  }, [pages]);

  return [pagination, isHasPagination]
}
