import { useCallback } from "react";
import { createToast } from "@store/partners-notifications";
import { useDispatch } from "react-redux";

export const useNotification = () => {
  const dispatch = useDispatch();

  const handleNotify = useCallback((message, action = null) => {
    dispatch(createToast({ message, action }));
  }, []);

  const handleErrorNotify = useCallback((message, action = null) => {
    dispatch(createToast({ message, action, error: true }));
  }, []);

  return {
    handleNotify,
    handleErrorNotify,
  };
};
