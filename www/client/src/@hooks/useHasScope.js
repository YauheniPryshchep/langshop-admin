import { useCallback } from "react";
import { profileScopes } from "@store/profile";
import { hasScope } from "@utils/scopes";
import { useSelector } from "react-redux";

export default () => {
  const scopes = useSelector(profileScopes);

  return useCallback(scope => hasScope(scopes, scope), [scopes]);
};
