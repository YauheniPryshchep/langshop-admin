import React, {useCallback, useMemo} from "react";

import {useDispatch} from 'react-redux';
import {reset} from "redux-form";
import ContextSaveBar from "@components/ContextSaveBar";

export const useReduxContextualSaveBar = ({form, isLoading, onSuccess, dirty, invalid}) => {
  const dispatch = useDispatch();

  const handleDiscard = useCallback(() => {
    dispatch(reset(form))
  }, [dispatch, form]);

  const contextualSaveBar = useMemo(() => {
    return dirty ?
      <ContextSaveBar onSuccess={onSuccess}
                      onDiscard={handleDiscard}
                      loading={isLoading}
                      disabled={invalid}
      />
      :
      null
  }, [dirty, invalid, handleDiscard, isLoading, onSuccess]);

  return [contextualSaveBar, dirty, invalid]
};
