import {useMemo} from "react";

export default (page, pages, handleChange, limit, limitOptions) => {
  const pagination = useMemo(() => {
    return {
      hasNext: page < pages,
      hasPrevious: page > 1,
      onNext: () => handleChange({
        key: 'page',
        value: page + 1
      }),
      onPrevious: () => handleChange({
        key: 'page',
        value: page - 1
      })
    }
  }, [page, pages, handleChange]);

  const perPage = useMemo(() => {
    return {
      limit: limit,
      options: limitOptions,
      onChange: (value) => {
        handleChange({
          key: 'limit',
          value: value
        });
      },
    };
  }, [limit, handleChange]);

  const isHasPagination = useMemo(() => {
    return pages > 1;
  }, [pages]);

  return [pagination, isHasPagination, perPage]
}
