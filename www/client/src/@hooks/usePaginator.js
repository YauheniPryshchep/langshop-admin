import { useCallback, useEffect, useState } from "react";
import { sanitizeLimit, sanitizeSort, sanitizeString } from "../@utils/query";
import { chunk } from "lodash";

export const usePaginator = (data, query, sortBy = "", limitOptions = [], sortOptions = []) => {
  const [search, setSearch] = useState(query ? sanitizeString(query.q) : "");
  const [limit, onChangeLimit] = useState(query ? sanitizeLimit(query.limit, limitOptions, 25) : 10);
  const [sort, onChangeSort] = useState(
    query
      ? sanitizeSort(
          query.sort,
          sortOptions.map(option => option.value),
          "asc"
        )
      : "asc"
  );
  const [page, setPage] = useState(1);
  const [pages, setPages] = useState(Math.ceil(data.length / limit));
  const [items, setItems] = useState(data);
  const [requestData, setRequestData] = useState({
    page: 1,
    limit,
    sortBy,
    sort,
    search,
  });

  useEffect(() => {
    setRequestData(state => ({
      ...state,
      sortBy,
      sort,
      limit,
      search,
      page: 1,
    }));
  }, [sort, sortBy, limit, search]);

  useEffect(() => {
    setRequestData(state => ({
      ...state,
      page,
    }));
  }, [page]);

  useEffect(() => {
    const { page, search, limit, sort, sortBy } = requestData;
    let items = onSort(
      data.filter(i => i.title.toLowerCase().includes(search.toLowerCase())),
      sort,
      sortBy
    );
    setPages(Math.ceil(items.length / limit));
    setItems(() => chunk([...items], limit)[page - 1] || []);
  }, [requestData, data]);

  const onLimitChange = useCallback(
    limit => {
      onChangeLimit(parseInt(limit));
    },
    [onChangeLimit]
  );

  const onSortChange = useCallback(
    sort => {
      onChangeSort(sort);
    },
    [onChangeSort]
  );

  const onSearchChange = useCallback(
    value => {
      setSearch(value);
    },
    [setSearch]
  );

  const onSearchClear = useCallback(() => {
    setSearch("");
  }, [setSearch]);

  const onSort = useCallback((items, sort, sortBy) => {
    if (!sortBy) {
      return items;
    }

    if (sort === "asc") {
      return items.sort((a, b) => {
        if (a[sortBy] < b[sortBy]) {
          return -1;
        }
        if (a[sortBy] > b[sortBy]) {
          return 1;
        }
        return 0;
      });
    }
    return items.sort((b, a) => {
      if (a[sortBy] < b[sortBy]) {
        return -1;
      }
      if (a[sortBy] > b[sortBy]) {
        return 1;
      }
      return 0;
    });
  }, []);

  const handleChangePage = useCallback(
    page => {
      if (page > pages) {
        page = pages;
      }
      if (page < 1) {
        page = 1;
      }
      setPage(page);
    },
    [setPage, pages]
  );

  return {
    ...requestData,
    onSearchChange,
    onSearchClear,
    onSortChange,
    onLimitChange,
    pages,
    items,
    onChangePage: handleChangePage,
  };
};
