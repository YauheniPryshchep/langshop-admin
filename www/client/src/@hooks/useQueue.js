import { useCallback, useMemo } from "react";
import useActive from "@hooks/useActive";
import Queue from "promise-queue";

export default concurrent => {
  const [isQueueing, startQueueing, stopQueueing] = useActive();

  const queue = useMemo(() => {
    return new Queue(concurrent, Infinity);
  }, [concurrent]);

  const inQueue = useCallback(
    promises => {
      startQueueing();

      return new Promise(resolve => {
        promises.forEach(promise =>
          queue.add(promise).finally(() => {
            if (queue.getQueueLength() || queue.getPendingLength()) {
              return;
            }

            stopQueueing();
            resolve();
          })
        );
      });
    },
    [queue, startQueueing, stopQueueing]
  );

  return [isQueueing, inQueue];
};
