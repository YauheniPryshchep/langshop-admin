import {useCallback, useState} from "react";
import map from "lodash/map";

const useFilters = (initialFilters = [], isLoading = false) => {
  const [appliedFilters, onChange] = useState([...initialFilters]);

  const resetPage = useCallback(filters => {
    return map(filters, filter => (filter.key === "page" ? {...filter, value: 1} : {...filter}));
  }, []);

  const handleChangeFilter = useCallback(
    filter => {
      if (isLoading) {
        return;
      }

      onChange(state => {
        if (filter.key !== "page") {
          state = resetPage(state);
        }
        return map(state, i => {
          if (i.key === filter.key) {
            return {...filter};
          }
          return {...i};
        });
      });
    },
    [onChange, resetPage, isLoading]
  );

  const onClearAll = useCallback(() => {
    if (isLoading) {
      return;
    }
    onChange([...initialFilters.map((filter) => ({
      ...filter,
      value: filter.initialValue
    }))]);
  }, [onChange, isLoading, initialFilters]);

  return [appliedFilters, handleChangeFilter, onClearAll];
};
export default useFilters;
