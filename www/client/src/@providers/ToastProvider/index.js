import { notifications, createToast, removeToast } from "@store/partners-notifications";
import { ToastProvider } from "./ToastProvider";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";

const mapState = createStructuredSelector({
  notifications,
});

const mapDispatch = {
  createToast,
  removeToast,
};

export default connect(mapState, mapDispatch)(ToastProvider);
