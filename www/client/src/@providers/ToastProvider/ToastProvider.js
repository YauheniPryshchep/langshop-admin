import React, { useMemo } from "react";
import { Toast } from "@shopify/polaris";

export const ToastProvider = ({ children, notifications, removeToast }) => {
  const toasts = useMemo(() => {
    return notifications.map(notification => {
      return (
        <Toast
          key={notification.id}
          content={notification.message}
          onDismiss={() => removeToast(notification.id)}
          error={!!notification.hasOwnProperty("error")}
          duration={10000}
        />
      );
    });
  }, [notifications]);

  return (
    <div>
      {children}
      {toasts}
    </div>
  );
};
