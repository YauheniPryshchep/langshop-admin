import React from "react";
import { Redirect } from "react-router-dom";

export const ProtectedAppRoute = ({ children, profileScopes, allowed }) => {
  if (!allowed(profileScopes)) {
    return <Redirect to={"/"} />;
  }

  return children;
};
