import { profileScopes } from "@store/profile";
import { ProtectedAppRoute } from "./ProtectedAppRoute";
import { createStructuredSelector } from "reselect";
import { connect } from "react-redux";

const mapState = createStructuredSelector({
  profileScopes,
});

export default connect(mapState, undefined)(ProtectedAppRoute);
