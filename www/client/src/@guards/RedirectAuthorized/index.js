import { token } from "@store/auth";
import { RedirectAuthorized } from "./RedirectAuthorized";
import { createStructuredSelector } from "reselect";
import { connect } from "react-redux";

const mapState = createStructuredSelector({
  token,
});

export default connect(mapState, undefined)(RedirectAuthorized);
