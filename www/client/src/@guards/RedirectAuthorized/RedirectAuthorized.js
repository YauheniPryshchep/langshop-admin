import React from "react";
import useQuery from "@hooks/useQuery";
import { Redirect } from "react-router-dom";

export const RedirectAuthorized = ({ children, token }) => {
  const [query] = useQuery();

  if (token) {
    const { returnUri } = query;

    return <Redirect to={returnUri ? returnUri : "/"} />;
  }

  return children;
};
