import { token } from "@store/auth";
import { profile, fetchProfileAction } from "@store/profile";
import { RedirectUnauthorized } from "./RedirectUnauthorized";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";

const mapState = createStructuredSelector({
  token,
  profile,
});

const mapDispatch = {
  fetchProfileAction,
};

export default connect(mapState, mapDispatch)(RedirectUnauthorized);
