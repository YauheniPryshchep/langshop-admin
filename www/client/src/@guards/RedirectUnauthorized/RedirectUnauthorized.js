import React, { useEffect } from "react";
import FrameSkeleton from "@components/Skeleton/FrameSkeleton";
import { Redirect, useLocation } from "react-router-dom";
import qs from "qs";

export const RedirectUnauthorized = ({ children, fetchProfileAction, token, profile }) => {
  const { pathname, search } = useLocation();

  if (!token) {
    const query = qs.stringify({
      returnUri: pathname + search,
    });

    return (
      <Redirect
        to={{
          pathname: "/auth/sign-in",
          search: `?${query}`,
        }}
      />
    );
  }

  useEffect(() => {
    if (profile) {
      return;
    }

    fetchProfileAction();
  }, []);

  if (!profile) {
    return <FrameSkeleton />;
  }

  return children;
};
