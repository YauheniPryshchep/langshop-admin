const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const CopyPlugin = require("copy-webpack-plugin");
const TerserPlugin = require("terser-webpack-plugin");
const ScriptExtHtmlWebpackPlugin = require("script-ext-html-webpack-plugin");
const LodashModuleReplacementPlugin = require("lodash-webpack-plugin");
//const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

module.exports = {
  entry: ["babel-polyfill", "./index.js"],
  output: {
    path: path.join(__dirname, "../public/static"),
    filename: "[name].[hash].js",
    chunkFilename: "[name].[chunkhash].bundle.js",
    publicPath: "/",
  },
  optimization: {
    splitChunks: {
      cacheGroups: {
        commons: {
          test: /[\\/]node_modules[\\/]/,
          name: "vendors",
          chunks: "all",
        },
      },
    },
    runtimeChunk: true,
    minimizer: [
      new TerserPlugin({
        terserOptions: {
          output: {
            comments: false,
          },
        },
      }),
    ],
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
          options: {
            plugins: ["lodash"],
            presets: [
              "@babel/preset-env",
              "@babel/preset-react",
              {
                plugins: ["@babel/plugin-proposal-class-properties"],
              },
            ],
          },
        },
      },
      {
        test: /\.s[ac]ss$/i,
        use: ["style-loader", "css-loader", "sass-loader"],
      },
      {
        test: /\.css$/i,
        use: ["style-loader", "css-loader"],
      },
      {
        test: /\.svg$/,
        loader: "svg-inline-loader",
      },
    ],
  },
  devServer: {
    historyApiFallback: true,
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: "./index.html",
    }),
    new ScriptExtHtmlWebpackPlugin({
      defaultAttribute: "defer",
    }),
    new CopyPlugin([
      {
        from: "./.htaccess",
        to: path.join(__dirname, "../public/static"),
      },
      {
        from: "./assets",
        to: path.join(__dirname, "../public/static/assets"),
      },
    ]),
    new LodashModuleReplacementPlugin({
      collections: true,
      paths: true,
    }),
  ],
  resolve: {
    alias: {
      "@common": path.resolve(__dirname, "./src/@common"),
      "@components": path.resolve(__dirname, "./src/@components"),
      "@hooks": path.resolve(__dirname, "./src/@hooks"),
      "@store": path.resolve(__dirname, "./src/@store"),
      "@utils": path.resolve(__dirname, "./src/@utils"),
      "@routes": path.resolve(__dirname, "./src/@routes"),
      "@middlewares": path.resolve(__dirname, "./src/@middlewares"),
      "@guards": path.resolve(__dirname, "./src/@guards"),
      "@svg": path.resolve(__dirname, "./src/@svg"),
      "@defaults": path.resolve(__dirname, "./src/@defaults"),
      "@localization": path.resolve(__dirname, "./src/@localization"),
      "@providers": path.resolve(__dirname, "./src/@providers"),
      "@styles": path.resolve(__dirname, "./src/@styles"),
    },
  },
};
