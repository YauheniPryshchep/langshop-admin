<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;

class AddNewsScopes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('scopes_types')) {
            DB::table('scopes_types')->insert([
                'id' => 9,
                'name' => 'News'
            ]);
        }
        if (Schema::hasTable('scopes')) {
            DB::table('scopes')->insert([
                ['id' => 54, 'name' => 'main-news-show', 'description' => 'News: Show', 'scopes_types_id' => 9],
                ['id' => 55, 'name' => 'main-news-update', 'description' => 'News: Edit', 'scopes_types_id' => 9],
            ]);
        }

        if (Schema::hasTable('roles_scopes')) {
            DB::table('roles_scopes')->insert([
                ['roles_id' => 1, 'scopes_id' => 54],
                ['roles_id' => 1, 'scopes_id' => 55],
                ['roles_id' => 3, 'scopes_id' => 54],
                ['roles_id' => 3, 'scopes_id' => 55],

            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
