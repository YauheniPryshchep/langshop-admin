<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FillUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('users')) {
            // add default admin
            $count = DB::table('users')->where('email', 'admin@devit-team.com')->count();
            if($count == 0) {
                DB::table('users')->insert([
                    'name' => 'Base Admin',
                    'email' => 'admin@devit-team.com',
                    'password' => '$2y$10$xc/KBWIVAy/SsYlEbI7pTeG2uvoSfxK1byiO5bN6rd6/xIpBfRKI2',
                    'created_at' => DB::raw('NOW()'),
                    'updated_at' => DB::raw('NOW()'),
                ]);
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('users')) {

        }
    }
}
