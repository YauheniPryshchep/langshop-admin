<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScopesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('scopes')) {
            Schema::create('scopes', function (Blueprint $table) {
                $table->charset = 'utf8';
                $table->collation = 'utf8_general_ci';
                $table->increments('id');
                $table->string('name', 255);
                $table->string('description', 255);
                $table->unsignedInteger('scopes_types_id')->index();
            });

            DB::table('scopes')->insert([
                ['id' => 3, 'name' => 'main-users-show', 'description' => 'Main: users show', 'scopes_types_id' => 1],
                ['id' => 4, 'name' => 'main-users-update', 'description' => 'Main: users edit', 'scopes_types_id' => 1],
                ['id' => 5, 'name' => 'main-roles-show', 'description' => 'Main: roles show', 'scopes_types_id' => 1],
                ['id' => 6, 'name' => 'main-roles-update', 'description' => 'Main: roles update', 'scopes_types_id' => 1],
                ['id' => 11, 'name' => 'main-email-templates-manage', 'description' => 'Main: Manage email templates', 'scopes_types_id' => 3],
                ['id' => 12, 'name' => 'langshop-stores-demo-show', 'description' => 'LangShop: Show demo stores', 'scopes_types_id' => 4],
                ['id' => 13, 'name' => 'langshop-stores-demo-update', 'description' => 'LangShop: Edit demo stores', 'scopes_types_id' => 4],
                ['id' => 14, 'name' => 'langshop-storeshistory-show', 'description' => 'LangShop: Show stores history', 'scopes_types_id' => 4],
                ['id' => 15, 'name' => 'langshop-storeshistory-update', 'description' => 'LangShop: Update stores history', 'scopes_types_id' => 4],
                ['id' => 16, 'name' => 'buildify-stores-demo-show', 'description' => 'Buildify: Show demo stores', 'scopes_types_id' => 5],
                ['id' => 17, 'name' => 'buildify-stores-demo-update', 'description' => 'Buildify: Update demo stores', 'scopes_types_id' => 5],
                ['id' => 18, 'name' => 'buildify-storeshistory-show', 'description' => 'Buildify: Show stores history', 'scopes_types_id' => 5],
                ['id' => 19, 'name' => 'buildify-storeshistory-update', 'description' => 'Buildify: Update stores history', 'scopes_types_id' => 5],
                ['id' => 24, 'name' => 'langshop-stores-trials-show', 'description' => 'LangShop: Show trial stores', 'scopes_types_id' => 4],
                ['id' => 25, 'name' => 'langshop-stores-trials-update', 'description' => 'LangShop: Edit trial stores', 'scopes_types_id' => 4],
                ['id' => 26, 'name' => 'langshop-stores-discounts-show', 'description' => 'LangShop: Show discount stores', 'scopes_types_id' => 4],
                ['id' => 27, 'name' => 'langshop-stores-discounts-update', 'description' => 'LangShop: Edit discount stores', 'scopes_types_id' => 4],
                ['id' => 28, 'name' => 'buildify-stores-trials-show', 'description' => 'Buildify: Show trial stores', 'scopes_types_id' => 5],
                ['id' => 29, 'name' => 'buildify-stores-trials-update', 'description' => 'Buildify: Edit trial stores', 'scopes_types_id' => 5],
                ['id' => 30, 'name' => 'buildify-stores-discounts-show', 'description' => 'Buildify: Show discount stores', 'scopes_types_id' => 5],
                ['id' => 31, 'name' => 'buildify-stores-discounts-update', 'description' => 'Buildify: Edit discount stores', 'scopes_types_id' => 5],
            ]);

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scopes');
    }
}
