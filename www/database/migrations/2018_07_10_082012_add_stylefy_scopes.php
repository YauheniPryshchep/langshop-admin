<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStylefyScopes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('scopes_types')) {
            DB::table('scopes_types')->insert([
                'id' => 7,
                'name' => 'Stylefy'
            ]);
        }
        if (Schema::hasTable('scopes')) {
            DB::table('scopes')->insert([
                ['id' => 38, 'name' => 'stylefy-stores-demo-show', 'description' => 'Stylefy: Show demo stores', 'scopes_types_id' => 7],
                ['id' => 39, 'name' => 'stylefy-stores-demo-update', 'description' => 'Stylefy: Edit demo stores', 'scopes_types_id' => 7],
                ['id' => 40, 'name' => 'stylefy-stores-trials-show', 'description' => 'Stylefy: Show trial stores', 'scopes_types_id' => 7],
                ['id' => 41, 'name' => 'stylefy-stores-trials-update', 'description' => 'Stylefy: Edit trial stores', 'scopes_types_id' => 7],
                ['id' => 42, 'name' => 'stylefy-stores-discounts-show', 'description' => 'Stylefy: Show discount stores', 'scopes_types_id' => 7],
                ['id' => 43, 'name' => 'stylefy-stores-discounts-update', 'description' => 'Stylefy: Edit discount stores', 'scopes_types_id' => 7],
                ['id' => 44, 'name' => 'stylefy-stores-history-show', 'description' => 'Stylefy: Show stores history', 'scopes_types_id' => 7],
                ['id' => 45, 'name' => 'stylefy-stores-history-update', 'description' => 'Stylefy: Update stores history', 'scopes_types_id' => 7],
            ]);
        }

        if (Schema::hasTable('roles_scopes')) {
            DB::table('roles_scopes')->insert([
                ['roles_id' => 1, 'scopes_id' => 38],
                ['roles_id' => 1, 'scopes_id' => 39],
                ['roles_id' => 1, 'scopes_id' => 40],
                ['roles_id' => 1, 'scopes_id' => 41],
                ['roles_id' => 1, 'scopes_id' => 42],
                ['roles_id' => 1, 'scopes_id' => 43],
                ['roles_id' => 1, 'scopes_id' => 44],
                ['roles_id' => 1, 'scopes_id' => 45],
                ['roles_id' => 3, 'scopes_id' => 38],
                ['roles_id' => 3, 'scopes_id' => 39],
                ['roles_id' => 3, 'scopes_id' => 40],
                ['roles_id' => 3, 'scopes_id' => 41],
                ['roles_id' => 3, 'scopes_id' => 42],
                ['roles_id' => 3, 'scopes_id' => 43],
                ['roles_id' => 3, 'scopes_id' => 44],
                ['roles_id' => 3, 'scopes_id' => 45],

            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
