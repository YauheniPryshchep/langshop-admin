<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoresHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('stores_history')) {
            Schema::create('stores_history', function (Blueprint $table) {
                $table->charset = 'utf8';
                $table->collation = 'utf8_general_ci';
                $table->bigIncrements('id');
                $table->string('app', 255);
                $table->enum('type', ['installed', 'uninstalled', 'undefined'])->default('undefined');
                $table->string('store', 255)->nullable();
                $table->string('email', 255)->nullable();
                $table->timestamp('created_at', 0)->useCurrent();
                $table->timestamp('updated_at', 0)->nullable();
                $table->text('reason')->nullable();
                $table->text('response')->nullable();
                $table->unsignedTinyInteger('processed')->default(0);
                $table->timestamp('response_at', 0)->nullable();
                $table->index(['app', 'type', 'store', 'email', 'created_at', 'updated_at', 'processed'], 'app');
                $table->index(['response_at']);
            });

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stores_history');
    }
}
