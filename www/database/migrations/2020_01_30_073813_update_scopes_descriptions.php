<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class UpdateScopesDescriptions extends Migration
{
    private $scopes = [
        [
            'name'        => 'main-users-show',
            'description' => 'Users : Show'
        ],
        [
            'name'        => 'main-users-update',
            'description' => 'Users : Update'
        ],
        [
            'name'        => 'main-roles-show',
            'description' => 'Roles : Show'
        ],
        [
            'name'        => 'main-roles-update',
            'description' => 'Roles : Update'
        ],
        [
            'name'        => 'langshop-stores-demo-show',
            'description' => 'LangShop : Rebate : Demo : Show'
        ],
        [
            'name'        => 'langshop-stores-demo-update',
            'description' => 'LangShop : Rebate : Demo : Update'
        ],
        [
            'name'        => 'langshop-storeshistory-show',
            'description' => 'LangShop : History : Show'
        ],
        [
            'name'        => 'langshop-storeshistory-update',
            'description' => 'LangShop : History : Update'
        ],
        [
            'name'        => 'buildify-stores-demo-show',
            'description' => 'Buildify : Rebate : Demo : Show'
        ],
        [
            'name'        => 'buildify-stores-demo-update',
            'description' => 'Buildify : Rebate : Demo : Update'
        ],
        [
            'name'        => 'buildify-storeshistory-show',
            'description' => 'Buildify : History : Show'
        ],
        [
            'name'        => 'buildify-storeshistory-update',
            'description' => 'Buildify : History : Update'
        ],
        [
            'name'        => 'langshop-stores-trials-show',
            'description' => 'LangShop : Rebate : Trials : Show'
        ],
        [
            'name'        => 'langshop-stores-trials-update',
            'description' => 'LangShop : Rebate : Trials : Update'
        ],
        [
            'name'        => 'langshop-stores-discounts-show',
            'description' => 'LangShop : Rebate : Discounts : Show'
        ],
        [
            'name'        => 'langshop-stores-discounts-update',
            'description' => 'LangShop : Rebate : Discounts : Update'
        ],
        [
            'name'        => 'buildify-stores-trials-show',
            'description' => 'Buildify : Rebate : Trials : Show'
        ],
        [
            'name'        => 'buildify-stores-trials-update',
            'description' => 'Buildify : Rebate : Trials : Update'
        ],
        [
            'name'        => 'buildify-stores-discounts-show',
            'description' => 'Buildify : Rebate : Discounts : Show'
        ],
        [
            'name'        => 'buildify-stores-discounts-update',
            'description' => 'Buildify : Rebate : Discounts : Update'
        ],
        [
            'name'        => 'main-news-show',
            'description' => 'News : Show'
        ],
        [
            'name'        => 'main-news-update',
            'description' => 'News : Update'
        ]
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach ($this->scopes as $scope) {
            DB::table('scopes')
                ->where('name', $scope['name'])
                ->update([
                    'description' => $scope['description']
                ]);
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
