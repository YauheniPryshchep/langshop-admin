<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AddPositionColumnInNews extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('news', function (Blueprint $table) {
            $table->unsignedBigInteger('position')->after('app')->default(0)->index();
        });

        foreach (['langshop', 'langshop-2', 'buildify'] as $appId) {
            DB::table('news')
                ->where('app', $appId)
                ->orderBy('created_at', 'desc')
                ->each(function ($item, $i) {
                    DB::table('news')
                        ->where('id', $item->id)
                        ->update([
                            'position' => $i + 1
                        ]);
                });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('news', function (Blueprint $table) {
            $table->dropColumn('position');
        });
    }
}
