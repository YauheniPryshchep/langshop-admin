<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class ChangeDbCharset extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     * @throws Exception
     */
    public function up()
    {
        $this->convertDb('dashboard', 'utf8mb4', 'utf8mb4_unicode_ci', false);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Exception
     */
    public function down()
    {
        $this->convertDb('dashboard', 'utf8', 'utf8_unicode_ci', false);
    }

    /**
     * @param string $connection
     * @param string $charset
     * @param string $collate
     * @param bool $dryRun
     * @throws Exception
     */
    private function convertDb(string $connection, string $charset, string $collate, bool $dryRun)
    {
        $dbName = config("database.connections.{$connection}.database");

        $varChars = DB::connection($connection)
            ->select(DB::raw("select * from INFORMATION_SCHEMA.COLUMNS where DATA_TYPE = 'VARCHAR' and (CHARACTER_SET_NAME != '{$charset}' or COLLATION_NAME != '{$collate}') AND TABLE_SCHEMA = '{$dbName}'"));

        // Check if shrinking field size will truncate!
        $skip    = [];  // List of table.column that will be handled manually
        $indexed = [];
        if ($charset == 'utf8mb4') {
            $error = false;
            foreach ($varChars as $t) {
                if ($t->CHARACTER_MAXIMUM_LENGTH > 191) {
                    $key = "{$t->TABLE_NAME}.{$t->COLUMN_NAME}";

                    // Check if column is indexed
                    $index         = DB::connection($connection)
                        ->select(DB::raw("SHOW INDEX FROM `{$t->TABLE_NAME}` where column_name = '{$t->COLUMN_NAME}'"));
                    $indexed[$key] = count($index) ? true : false;

                    if (count($index)) {
                        $result = DB::connection($connection)
                            ->select(DB::raw("select count(*) as `count` from `{$t->TABLE_NAME}` where length(`{$t->COLUMN_NAME}`) > 191"));
                        if ($result[0]->count > 0) {
                            if (!in_array($key, $skip)) {
                                $error = true;
                            }
                        }
                    }
                }
            }

            if ($error) {
                throw new Exception('Aborting due to data truncation');
            }
        }

        $query = "SET FOREIGN_KEY_CHECKS = 0";
        $this->dbExec($connection, $query, $dryRun);

        $query = "ALTER SCHEMA {$dbName} DEFAULT CHARACTER SET {$charset} DEFAULT COLLATE {$collate}";
        $this->dbExec($connection, $query, $dryRun);

        $tableChanges = [];
        foreach ($varChars as $t) {
            $key = "{$t->TABLE_NAME}.{$t->COLUMN_NAME}";
            if (!in_array($key, $skip)) {
                if ($charset == 'utf8mb4' && $t->CHARACTER_MAXIMUM_LENGTH > 191 && $indexed["{$t->TABLE_NAME}.{$t->COLUMN_NAME}"]) {
                    $tableChanges["{$t->TABLE_NAME}"][] = "CHANGE `{$t->COLUMN_NAME}` `{$t->COLUMN_NAME}` VARCHAR(191) CHARACTER SET {$charset} COLLATE {$collate}";
                } else {
                    if ($charset == 'utf8' && $t->CHARACTER_MAXIMUM_LENGTH == 191) {
                        $tableChanges["{$t->TABLE_NAME}"][] = "CHANGE `{$t->COLUMN_NAME}` `{$t->COLUMN_NAME}` VARCHAR(255) CHARACTER SET {$charset} COLLATE {$collate}";
                    } else {
                        $tableChanges["{$t->TABLE_NAME}"][] = "CHANGE `{$t->COLUMN_NAME}` `{$t->COLUMN_NAME}` VARCHAR({$t->CHARACTER_MAXIMUM_LENGTH}) CHARACTER SET {$charset} COLLATE {$collate}";
                    }
                }
            }
        }

        $texts = DB::connection($connection)
            ->select(DB::raw("select * from INFORMATION_SCHEMA.COLUMNS where DATA_TYPE like '%TEXT%' and (CHARACTER_SET_NAME != '{$charset}' or COLLATION_NAME != '{$collate}') AND TABLE_SCHEMA = '{$dbName}'"));
        foreach ($texts as $t) {
            $tableChanges["{$t->TABLE_NAME}"][] = "CHANGE `{$t->COLUMN_NAME}` `{$t->COLUMN_NAME}` {$t->DATA_TYPE} CHARACTER SET {$charset} COLLATE {$collate}";
        }

        $tables = DB::connection($connection)
            ->select(DB::raw("select * from INFORMATION_SCHEMA.TABLES where TABLE_COLLATION != '{$collate}' and TABLE_SCHEMA = '{$dbName}';"));
        foreach ($tables as $t) {
            $tableChanges["{$t->TABLE_NAME}"][] = "CONVERT TO CHARACTER SET {$charset} COLLATE {$collate}";
            $tableChanges["{$t->TABLE_NAME}"][] = "DEFAULT CHARACTER SET={$charset} COLLATE={$collate}";
        }

        foreach ($tableChanges as $table => $changes) {
            $query = "ALTER TABLE `{$table}` " . implode(",\n", $changes);
            $this->dbExec($connection, $query, $dryRun);
        }

        $query = "SET FOREIGN_KEY_CHECKS = 1";
        $this->dbExec($connection, $query, $dryRun);
    }

    /**
     * @param string $connection
     * @param string $query
     * @param bool $dryRun
     */
    private function dbExec(string $connection, string $query, bool $dryRun)
    {
        if ($dryRun) {
            echo $query . ';' . PHP_EOL;
        } else {
            DB::connection($connection)->getPdo()->exec($query);
        }
    }
}