<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddReactFlowScopes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('scopes_types')) {
            DB::table('scopes_types')->insert([
                'id' => 8,
                'name' => 'React Flow'
            ]);
        }
        if (Schema::hasTable('scopes')) {
            DB::table('scopes')->insert([
                ['id' => 46, 'name' => 'react-flow-stores-demo-show', 'description' => 'React Flow: Show demo stores', 'scopes_types_id' => 8],
                ['id' => 47, 'name' => 'react-flow-stores-demo-update', 'description' => 'React Flow: Edit demo stores', 'scopes_types_id' => 8],
                ['id' => 48, 'name' => 'react-flow-stores-trials-show', 'description' => 'React Flow: Show trial stores', 'scopes_types_id' => 8],
                ['id' => 49, 'name' => 'react-flow-stores-trials-update', 'description' => 'React Flow: Edit trial stores', 'scopes_types_id' => 8],
                ['id' => 50, 'name' => 'react-flow-stores-discounts-show', 'description' => 'React Flow: Show discount stores', 'scopes_types_id' => 8],
                ['id' => 51, 'name' => 'react-flow-stores-discounts-update', 'description' => 'React Flow: Edit discount stores', 'scopes_types_id' => 8],
                ['id' => 52, 'name' => 'react-flow-stores-history-show', 'description' => 'React Flow: Show stores history', 'scopes_types_id' => 8],
                ['id' => 53, 'name' => 'react-flow-stores-history-update', 'description' => 'React Flow: Update stores history', 'scopes_types_id' => 8],
            ]);
        }

        if (Schema::hasTable('roles_scopes')) {
            DB::table('roles_scopes')->insert([
                ['roles_id' => 1, 'scopes_id' => 46],
                ['roles_id' => 1, 'scopes_id' => 47],
                ['roles_id' => 1, 'scopes_id' => 48],
                ['roles_id' => 1, 'scopes_id' => 49],
                ['roles_id' => 1, 'scopes_id' => 50],
                ['roles_id' => 1, 'scopes_id' => 51],
                ['roles_id' => 1, 'scopes_id' => 52],
                ['roles_id' => 1, 'scopes_id' => 53],
                ['roles_id' => 3, 'scopes_id' => 46],
                ['roles_id' => 3, 'scopes_id' => 47],
                ['roles_id' => 3, 'scopes_id' => 48],
                ['roles_id' => 3, 'scopes_id' => 49],
                ['roles_id' => 3, 'scopes_id' => 50],
                ['roles_id' => 3, 'scopes_id' => 51],
                ['roles_id' => 3, 'scopes_id' => 52],
                ['roles_id' => 3, 'scopes_id' => 53],

            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
