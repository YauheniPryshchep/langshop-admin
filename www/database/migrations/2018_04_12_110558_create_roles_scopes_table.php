<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRolesScopesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('roles_scopes')) {
            Schema::create('roles_scopes', function (Blueprint $table) {
                $table->increments('id');
                $table->unsignedInteger('roles_id');
                $table->unsignedInteger('scopes_id');
                $table->index(['roles_id', 'scopes_id']);
            });

            DB::table('roles_scopes')->insert([
                ['roles_id' => 1, 'scopes_id' => 1],
                ['roles_id' => 1, 'scopes_id' => 2],
                ['roles_id' => 1, 'scopes_id' => 3],
                ['roles_id' => 1, 'scopes_id' => 4],
                ['roles_id' => 1, 'scopes_id' => 5],
                ['roles_id' => 1, 'scopes_id' => 6],
                ['roles_id' => 1, 'scopes_id' => 7],
                ['roles_id' => 1, 'scopes_id' => 8],
                ['roles_id' => 1, 'scopes_id' => 9],
                ['roles_id' => 1, 'scopes_id' => 10],
                ['roles_id' => 1, 'scopes_id' => 11],
                ['roles_id' => 1, 'scopes_id' => 12],
                ['roles_id' => 1, 'scopes_id' => 13],
                ['roles_id' => 1, 'scopes_id' => 14],
                ['roles_id' => 1, 'scopes_id' => 15],
                ['roles_id' => 1, 'scopes_id' => 16],
                ['roles_id' => 1, 'scopes_id' => 17],
                ['roles_id' => 1, 'scopes_id' => 18],
                ['roles_id' => 1, 'scopes_id' => 19],
                ['roles_id' => 1, 'scopes_id' => 20],
                ['roles_id' => 1, 'scopes_id' => 21],
                ['roles_id' => 1, 'scopes_id' => 22],
                ['roles_id' => 1, 'scopes_id' => 23],
                ['roles_id' => 1, 'scopes_id' => 24],
                ['roles_id' => 1, 'scopes_id' => 25],
                ['roles_id' => 1, 'scopes_id' => 26],
                ['roles_id' => 1, 'scopes_id' => 27],
                ['roles_id' => 1, 'scopes_id' => 28],
                ['roles_id' => 1, 'scopes_id' => 29],
                ['roles_id' => 1, 'scopes_id' => 30],
                ['roles_id' => 1, 'scopes_id' => 31],
                ['roles_id' => 3, 'scopes_id' => 1],
                ['roles_id' => 3, 'scopes_id' => 2],
                ['roles_id' => 3, 'scopes_id' => 3],
                ['roles_id' => 3, 'scopes_id' => 4],
                ['roles_id' => 3, 'scopes_id' => 5],
                ['roles_id' => 3, 'scopes_id' => 6],
                ['roles_id' => 3, 'scopes_id' => 7],
                ['roles_id' => 3, 'scopes_id' => 8],
                ['roles_id' => 3, 'scopes_id' => 9],
                ['roles_id' => 3, 'scopes_id' => 10],
                ['roles_id' => 3, 'scopes_id' => 11],
                ['roles_id' => 3, 'scopes_id' => 12],
                ['roles_id' => 3, 'scopes_id' => 13],
                ['roles_id' => 3, 'scopes_id' => 14],
                ['roles_id' => 3, 'scopes_id' => 15],
                ['roles_id' => 3, 'scopes_id' => 16],
                ['roles_id' => 3, 'scopes_id' => 17],
                ['roles_id' => 3, 'scopes_id' => 18],
                ['roles_id' => 3, 'scopes_id' => 19],
                ['roles_id' => 3, 'scopes_id' => 20],
                ['roles_id' => 3, 'scopes_id' => 21],
                ['roles_id' => 3, 'scopes_id' => 22],
                ['roles_id' => 3, 'scopes_id' => 23],
                ['roles_id' => 3, 'scopes_id' => 24],
                ['roles_id' => 3, 'scopes_id' => 25],
                ['roles_id' => 3, 'scopes_id' => 26],
                ['roles_id' => 3, 'scopes_id' => 27],
                ['roles_id' => 3, 'scopes_id' => 28],
                ['roles_id' => 3, 'scopes_id' => 29],
                ['roles_id' => 3, 'scopes_id' => 30],
                ['roles_id' => 3, 'scopes_id' => 31],

            ]);

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roles_scopes');
    }
}
