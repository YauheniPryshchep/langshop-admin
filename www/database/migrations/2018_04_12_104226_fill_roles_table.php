<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FillRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('roles')) {
            // sadmin role
            $count = DB::table('roles')->where('name', 'sadmin')->count();
            if($count == 0) {
                DB::table('roles')->insert([
                    'name' => 'sadmin',
                    'description' => 'Super Administrator. God of this lands.'
                ]);
            }
            // nobody role
            $count = DB::table('roles')->where('name', 'nobody')->count();
            if($count == 0) {
                DB::table('roles')->insert([
                    'name' => 'nobody',
                    'description' => 'No name stranger'
                ]);
            }
            // admin role
            $count = DB::table('roles')->where('name', 'admin')->count();
            if($count == 0) {
                DB::table('roles')->insert([
                    'name' => 'admin',
                    'description' => 'System administrator'
                ]);
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('roles', function (Blueprint $table) {
            //
        });
    }
}
