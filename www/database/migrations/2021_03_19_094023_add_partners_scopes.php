<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPartnersScopes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('scopes_types')) {
            DB::table('scopes_types')->insert([
                'id' => 14,
                'name' => 'Partners'
            ]);
        }
        if (Schema::hasTable('scopes')) {
            DB::table('scopes')->insert([
                ['id' => 67, 'name' => 'partners-users-show', 'description' => 'Partners : Users : Show users', 'scopes_types_id' => 14],
                ['id' => 68, 'name' => 'partners-stores-show', 'description' => 'Partners : Stores : Show stores', 'scopes_types_id' => 14],
                ['id' => 69, 'name' => 'partners-payouts-show', 'description' => 'Partners: Payouts: Show payouts', 'scopes_types_id' => 14],
                ['id' => 70, 'name' => 'partners-payouts-update', 'description' => 'Partners: Payouts: Update payouts', 'scopes_types_id' => 14],
            ]);
        }

        if (Schema::hasTable('roles_scopes')) {
            DB::table('roles_scopes')->insert([
                ['roles_id' => 1, 'scopes_id' => 67],
                ['roles_id' => 1, 'scopes_id' => 68],
                ['roles_id' => 1, 'scopes_id' => 69],
                ['roles_id' => 1, 'scopes_id' => 70],
                ['roles_id' => 3, 'scopes_id' => 67],
                ['roles_id' => 3, 'scopes_id' => 68],
                ['roles_id' => 3, 'scopes_id' => 69],
                ['roles_id' => 3, 'scopes_id' => 70],

            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
