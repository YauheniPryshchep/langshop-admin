<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStoresChargeScopes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('scopes')) {
            DB::table('scopes')->insert([
                ['id' => 63, 'name' => 'langshop-stores-charges-show', 'description' => 'LangShop : Charges : Show', 'scopes_types_id' => 4],
            ]);
        }

        if (Schema::hasTable('roles_scopes')) {
            DB::table('roles_scopes')->insert([
                ['roles_id' => 1, 'scopes_id' => 63],
                ['roles_id' => 3, 'scopes_id' => 63]
            ]);
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
