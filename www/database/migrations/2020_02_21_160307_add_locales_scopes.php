<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AddLocalesScopes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('scopes_types')) {
            DB::table('scopes_types')->insert([
                'id'   => 10,
                'name' => 'Locales'
            ]);
        }
        if (Schema::hasTable('scopes')) {
            DB::table('scopes')->insert([
                ['id' => 56, 'name' => 'locales-show', 'description' => 'Locales: Show', 'scopes_types_id' => 10],
                ['id' => 57, 'name' => 'locales-update', 'description' => 'Locales: Edit', 'scopes_types_id' => 10],
            ]);
        }

        if (Schema::hasTable('roles_scopes')) {
            DB::table('roles_scopes')->insert([
                ['roles_id' => 1, 'scopes_id' => 56],
                ['roles_id' => 1, 'scopes_id' => 57],
                ['roles_id' => 3, 'scopes_id' => 56],
                ['roles_id' => 3, 'scopes_id' => 57],
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
