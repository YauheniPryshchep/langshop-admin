<?php

use App\Models\CampaignProcess;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampaignsProcessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaigns_processes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('campaign_id');
            $table->unsignedBigInteger('parent_id')->nullable();
            $table->unsignedSmallInteger('processor');
            $table->json('data')->nullable();
            $table->json('result')->nullable();
            $table->unsignedSmallInteger('status')->default(CampaignProcess::STATUS_PENDING);
            $table->timestamps();

            $table->index('campaign_id');
            $table->index('parent_id');
            $table->index('processor');
            $table->index('status');
            $table->index('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campaigns_processes');
    }
}
