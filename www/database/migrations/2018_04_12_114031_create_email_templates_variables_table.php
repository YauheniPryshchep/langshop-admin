<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailTemplatesVariablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('email_templates_variables')) {
            Schema::create('email_templates_variables', function (Blueprint $table) {
                $table->charset = 'utf8';
                $table->collation = 'utf8_general_ci';
                $table->increments('id');
                $table->unsignedInteger('email_templates_id')->index();
                $table->string('name', 255);
                $table->string('label', 255);
                $table->enum('type', ['dynamic', 'static'])->default('static');
                $table->unsignedTinyInteger('required')->default(0);
                $table->unsignedTinyInteger('system')->default(1);
                $table->text('value')->nullable();
            });

            DB::table('email_templates_variables')->insert([
                ['email_templates_id' => 1, 'name' => 'title', 'label' => 'title', 'type' => 'dynamic', 'required' => 1, 'system' => 1, 'value' => null],
                ['email_templates_id' => 1, 'name' => 'message', 'label' => 'message', 'type' => 'dynamic', 'required' => 1, 'system' => 0, 'value' => null],
                ['email_templates_id' => 2, 'name' => 'title', 'label' => 'title', 'type' => 'dynamic', 'required' => 0, 'system' => 1, 'value' => null],
                ['email_templates_id' => 3, 'name' => 'title', 'label' => 'title', 'type' => 'dynamic', 'required' => 0, 'system' => 1, 'value' => null],
                ['email_templates_id' => 3, 'name' => 'stage', 'label' => 'stage', 'type' => 'dynamic', 'required' => 0, 'system' => 1, 'value' => null],
                ['email_templates_id' => 3, 'name' => 'name', 'label' => 'name', 'type' => 'dynamic', 'required' => 0, 'system' => 1, 'value' => null],
                ['email_templates_id' => 3, 'name' => 'email', 'label' => 'email', 'type' => 'dynamic', 'required' => 0, 'system' => 1, 'value' => null],
                ['email_templates_id' => 3, 'name' => 'id', 'label' => 'id', 'type' => 'dynamic', 'required' => 0, 'system' => 1, 'value' => null],
                ['email_templates_id' => 3, 'name' => 'type', 'label' => 'type', 'type' => 'dynamic', 'required' => 0, 'system' => 1, 'value' => null],
                ['email_templates_id' => 3, 'name' => 'addedTimestamp', 'label' => 'addedTimestamp', 'type' => 'dynamic', 'required' => 0, 'system' => 1, 'value' => null],
                ['email_templates_id' => 3, 'name' => 'finishedTimestamp', 'label' => 'finishedTimestamp', 'type' => 'dynamic', 'required' => 0, 'system' => 1, 'value' => null],
                ['email_templates_id' => 3, 'name' => 'url', 'label' => 'url', 'type' => 'dynamic', 'required' => 0, 'system' => 1, 'value' => null],
                ['email_templates_id' => 4, 'name' => 'title', 'label' => 'title', 'type' => 'dynamic', 'required' => 0, 'system' => 1, 'value' => null],
                ['email_templates_id' => 4, 'name' => 'url', 'label' => 'url', 'type' => 'dynamic', 'required' => 1, 'system' => 1, 'value' => null],
                ['email_templates_id' => 5, 'name' => 'title', 'label' => 'title', 'type' => 'dynamic', 'required' => 0, 'system' => 1, 'value' => null],
                ['email_templates_id' => 5, 'name' => 'message', 'label' => 'message', 'type' => 'dynamic', 'required' => 1, 'system' => 1, 'value' => null],
            ]);

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('email_templates_variables');
    }
}
