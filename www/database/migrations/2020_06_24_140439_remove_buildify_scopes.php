<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class RemoveBuildifyScopes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $types = DB::table('scopes_types')
            ->whereIn('name', [
                'Buildify'
            ])
            ->pluck('id');

        $scopes = DB::table('scopes')
            ->whereIn('scopes_types_id', $types)
            ->pluck('id');

        DB::table('roles_scopes')->whereIn('scopes_id', $scopes)->delete();
        DB::table('scopes_types')->whereIn('id', $types)->delete();
        DB::table('scopes')->whereIn('id', $scopes)->delete();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
