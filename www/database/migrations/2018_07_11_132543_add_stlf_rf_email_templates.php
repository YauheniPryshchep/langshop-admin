<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class AddStlfRfEmailTemplates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $count = DB::table('email_templates')->where('slug', 'historyLangShopInstalledMessage')->count();
        if ($count == 0) {
            $id = DB::table('email_templates')->insertGetId([
                'slug' => 'historyLangShopInstalledMessage',
                'name' => 'History LangShop Installed Message',
                'type' => 'history',
                'data' => '<table  width="100%" height="150" class="c1363 c406 c1174" style="box-sizing: border-box; height: 150px; margin: 0 auto 10px auto; padding: 5px 5px 5px 5px; width: 100%; border-collapse: collapse;"><tbody id="i125" style="box-sizing: border-box;"><tr id="ii2j" style="box-sizing: border-box;"><td id="iqmg4" valign="top" bgcolor="#f6f6f6" style="box-sizing: border-box; font-size: 12px; font-weight: 300; vertical-align: top; color: #f6f9fb; margin: 0; padding: 0; background-color: #f6f6f6;"><table width="560" height="327" bgcolor="#FFF" id="isna" class="c1066 c401 c402 c406" style="box-sizing: border-box; height: 327px; padding: 20px 20px 20px 20px; border-collapse: collapse; width: 560px; background-color: #FFF; border-radius: 5px 5px 5px 5px; margin: 40px auto 40px auto; border: 0 solid rgba(255,255,255,0);"><tbody id="i037" style="box-sizing: border-box;"><tr id="iula" style="box-sizing: border-box;"><td id="iajw" valign="top" width="100%" style="box-sizing: border-box; font-size: 12px; font-weight: 300; vertical-align: top; color: rgb(111, 119, 125); margin: 0; padding: 0; width: 100%;"><img src="https://devit-general-media.s3.amazonaws.com/whBNMQ3CLDJIo5odn58t4IItxT0b2a3p0SP30gkl.png" id="i6s3" class="c1292" style="box-sizing: border-box; color: black;"><div class="c1837" style="box-sizing: border-box; padding: 10px 10px 17px 75px; margin: -62px 0 0 0; color: #ffffff; font-size: 30px; font-family: Helvetica, serif;">{{title}}</div><table width="100%" height="100%" id="iffa" class="c1753" style="box-sizing: border-box; height: auto; margin: 0 auto 1px auto; padding: 5px 5px 5px 5px; width: 100%; border-collapse: collapse;"><tbody id="i3k4" style="box-sizing: border-box;"><tr id="iw24" style="box-sizing: border-box;"><td id="iz4z4" valign="top" style="box-sizing: border-box; font-size: 12px; font-weight: 300; vertical-align: top; color: rgb(111, 119, 125); margin: 0; padding: 20px 20px 20px 20px; max-width: auto; min-height: auto; height: auto;"><div id="i99pf" class="c1417" style="box-sizing: border-box; padding: 10px; font-size: 14px; font-family: Helvetica, serif; color: #222;">{{message}}</div><div id="i2pqf" class="c2262" style="box-sizing: border-box; padding: 10px; font-size: 14px; font-family: Helvetica, serif; color: #222;"><div id="i014c" style="box-sizing: border-box;">Best regards,\n                          </div><div id="id3a2" style="box-sizing: border-box;"><br id="i65k3" style="box-sizing: border-box;"></div><div id="ib1yi" style="box-sizing: border-box;">DevIT Team\n                          </div></div></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table>',
            ]);
            if (Schema::hasTable('email_templates_variables')) {
                DB::table('email_templates_variables')->insert([
                    ['email_templates_id' => $id, 'name' => 'title', 'label' => 'title', 'type' => 'dynamic', 'required' => 1, 'system' => 1, 'value' => null],
                    ['email_templates_id' => $id, 'name' => 'message', 'label' => 'message', 'type' => 'dynamic', 'required' => 1, 'system' => 0, 'value' => null],
                ]);
            }
            unset($id);
        }

        $count = DB::table('email_templates')->where('slug', 'historyBuildifyInstalledMessage')->count();
        if ($count == 0) {
            $id = DB::table('email_templates')->insertGetId([
                'slug' => 'historyBuildifyInstalledMessage',
                'name' => 'History Buildify Installed Message',
                'type' => 'history',
                'data' => '<table  width="100%" height="150" class="c1363 c406 c1174" style="box-sizing: border-box; height: 150px; margin: 0 auto 10px auto; padding: 5px 5px 5px 5px; width: 100%; border-collapse: collapse;"><tbody id="i125" style="box-sizing: border-box;"><tr id="ii2j" style="box-sizing: border-box;"><td id="iqmg4" valign="top" bgcolor="#f6f6f6" style="box-sizing: border-box; font-size: 12px; font-weight: 300; vertical-align: top; color: #f6f9fb; margin: 0; padding: 0; background-color: #f6f6f6;"><table width="560" height="327" bgcolor="#FFF" id="isna" class="c1066 c401 c402 c406" style="box-sizing: border-box; height: 327px; padding: 20px 20px 20px 20px; border-collapse: collapse; width: 560px; background-color: #FFF; border-radius: 5px 5px 5px 5px; margin: 40px auto 40px auto; border: 0 solid rgba(255,255,255,0);"><tbody id="i037" style="box-sizing: border-box;"><tr id="iula" style="box-sizing: border-box;"><td id="iajw" valign="top" width="100%" style="box-sizing: border-box; font-size: 12px; font-weight: 300; vertical-align: top; color: rgb(111, 119, 125); margin: 0; padding: 0; width: 100%;"><img src="https://devit-general-media.s3.amazonaws.com/whBNMQ3CLDJIo5odn58t4IItxT0b2a3p0SP30gkl.png" id="i6s3" class="c1292" style="box-sizing: border-box; color: black;"><div class="c1837" style="box-sizing: border-box; padding: 10px 10px 17px 75px; margin: -62px 0 0 0; color: #ffffff; font-size: 30px; font-family: Helvetica, serif;">{{title}}</div><table width="100%" height="100%" id="iffa" class="c1753" style="box-sizing: border-box; height: auto; margin: 0 auto 1px auto; padding: 5px 5px 5px 5px; width: 100%; border-collapse: collapse;"><tbody id="i3k4" style="box-sizing: border-box;"><tr id="iw24" style="box-sizing: border-box;"><td id="iz4z4" valign="top" style="box-sizing: border-box; font-size: 12px; font-weight: 300; vertical-align: top; color: rgb(111, 119, 125); margin: 0; padding: 20px 20px 20px 20px; max-width: auto; min-height: auto; height: auto;"><div id="i99pf" class="c1417" style="box-sizing: border-box; padding: 10px; font-size: 14px; font-family: Helvetica, serif; color: #222;">{{message}}</div><div id="i2pqf" class="c2262" style="box-sizing: border-box; padding: 10px; font-size: 14px; font-family: Helvetica, serif; color: #222;"><div id="i014c" style="box-sizing: border-box;">Best regards,\n                          </div><div id="id3a2" style="box-sizing: border-box;"><br id="i65k3" style="box-sizing: border-box;"></div><div id="ib1yi" style="box-sizing: border-box;">DevIT Team\n                          </div></div></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table>',
            ]);
            if (Schema::hasTable('email_templates_variables')) {
                DB::table('email_templates_variables')->insert([
                    ['email_templates_id' => $id, 'name' => 'title', 'label' => 'title', 'type' => 'dynamic', 'required' => 1, 'system' => 1, 'value' => null],
                    ['email_templates_id' => $id, 'name' => 'message', 'label' => 'message', 'type' => 'dynamic', 'required' => 1, 'system' => 0, 'value' => null],
                ]);
            }
            unset($id);
        }

        $count = DB::table('email_templates')->where('slug', 'historyInstalledMessage')->count();
        if ($count == 0) {
            $id = DB::table('email_templates')->insertGetId([
                'slug' => 'historyInstalledMessage',
                'name' => 'History Installed Message',
                'type' => 'history',
                'data' => '<table  width="100%" height="150" class="c1363 c406 c1174" style="box-sizing: border-box; height: 150px; margin: 0 auto 10px auto; padding: 5px 5px 5px 5px; width: 100%; border-collapse: collapse;"><tbody id="i125" style="box-sizing: border-box;"><tr id="ii2j" style="box-sizing: border-box;"><td id="iqmg4" valign="top" bgcolor="#f6f6f6" style="box-sizing: border-box; font-size: 12px; font-weight: 300; vertical-align: top; color: #f6f9fb; margin: 0; padding: 0; background-color: #f6f6f6;"><table width="560" height="327" bgcolor="#FFF" id="isna" class="c1066 c401 c402 c406" style="box-sizing: border-box; height: 327px; padding: 20px 20px 20px 20px; border-collapse: collapse; width: 560px; background-color: #FFF; border-radius: 5px 5px 5px 5px; margin: 40px auto 40px auto; border: 0 solid rgba(255,255,255,0);"><tbody id="i037" style="box-sizing: border-box;"><tr id="iula" style="box-sizing: border-box;"><td id="iajw" valign="top" width="100%" style="box-sizing: border-box; font-size: 12px; font-weight: 300; vertical-align: top; color: rgb(111, 119, 125); margin: 0; padding: 0; width: 100%;"><img src="https://devit-general-media.s3.amazonaws.com/whBNMQ3CLDJIo5odn58t4IItxT0b2a3p0SP30gkl.png" id="i6s3" class="c1292" style="box-sizing: border-box; color: black;"><div class="c1837" style="box-sizing: border-box; padding: 10px 10px 17px 75px; margin: -62px 0 0 0; color: #ffffff; font-size: 30px; font-family: Helvetica, serif;">{{title}}</div><table width="100%" height="100%" id="iffa" class="c1753" style="box-sizing: border-box; height: auto; margin: 0 auto 1px auto; padding: 5px 5px 5px 5px; width: 100%; border-collapse: collapse;"><tbody id="i3k4" style="box-sizing: border-box;"><tr id="iw24" style="box-sizing: border-box;"><td id="iz4z4" valign="top" style="box-sizing: border-box; font-size: 12px; font-weight: 300; vertical-align: top; color: rgb(111, 119, 125); margin: 0; padding: 20px 20px 20px 20px; max-width: auto; min-height: auto; height: auto;"><div id="i99pf" class="c1417" style="box-sizing: border-box; padding: 10px; font-size: 14px; font-family: Helvetica, serif; color: #222;">{{message}}</div><div id="i2pqf" class="c2262" style="box-sizing: border-box; padding: 10px; font-size: 14px; font-family: Helvetica, serif; color: #222;"><div id="i014c" style="box-sizing: border-box;">Best regards,\n                          </div><div id="id3a2" style="box-sizing: border-box;"><br id="i65k3" style="box-sizing: border-box;"></div><div id="ib1yi" style="box-sizing: border-box;">DevIT Team\n                          </div></div></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table>',
            ]);
            if (Schema::hasTable('email_templates_variables')) {
                DB::table('email_templates_variables')->insert([
                    ['email_templates_id' => $id, 'name' => 'title', 'label' => 'title', 'type' => 'dynamic', 'required' => 1, 'system' => 1, 'value' => null],
                    ['email_templates_id' => $id, 'name' => 'message', 'label' => 'message', 'type' => 'dynamic', 'required' => 1, 'system' => 0, 'value' => null],
                ]);
            }
            unset($id);
        }
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
