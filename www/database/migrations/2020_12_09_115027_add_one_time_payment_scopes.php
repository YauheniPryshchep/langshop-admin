<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOneTimePaymentScopes extends Migration

{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('scopes')) {
            DB::table('scopes')->insert([
                ['id' => 62, 'name' => 'langshop-stores-one-time-payment-update', 'description' => 'LangShop : Rebate : One time payment : Create one time payment', 'scopes_types_id' => 4],
            ]);
        }

        if (Schema::hasTable('roles_scopes')) {
            DB::table('roles_scopes')->insert([
                ['roles_id' => 1, 'scopes_id' => 62],
                ['roles_id' => 3, 'scopes_id' => 62]
            ]);
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
