<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {

            $table->charset = 'utf8';
            $table->collation = 'utf8_general_ci';

            $table->bigIncrements('id');
            $table->string('app', 255);
            $table->enum('type', ['article', 'video'])->default('article');
            $table->string('src', 255)->nullable();
            $table->string('video', 255)->nullable();
            $table->enum('layout', ['top', 'left', 'right', 'bottom'])->default('top');
            $table->string('title', 255);
            $table->text('description')->nullable();
            $table->string('primaryLink', 255)->nullable();
            $table->string('primaryText', 255)->nullable();
            $table->string('secondaryLink', 255)->nullable();
            $table->string('secondaryText', 255)->nullable();
            $table->timestamps();

            $table->index(['app', 'type'], 'app');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news');
    }
}
