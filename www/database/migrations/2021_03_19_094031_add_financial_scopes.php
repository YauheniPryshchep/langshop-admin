<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFinancialScopes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('scopes_types')) {
            DB::table('scopes_types')->insert([
                'id'   => 15,
                'name' => 'Financial'
            ]);
        }
        if (Schema::hasTable('scopes')) {
            DB::table('scopes')->insert([
                ['id' => 71, 'name' => 'financial-show', 'description' => 'Financial : Show', 'scopes_types_id' => 15],
            ]);
        }

        if (Schema::hasTable('roles_scopes')) {
            DB::table('roles_scopes')->insert([
                ['roles_id' => 1, 'scopes_id' => 71],
                ['roles_id' => 3, 'scopes_id' => 71],

            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
