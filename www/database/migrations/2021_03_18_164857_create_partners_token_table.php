<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePartnersTokenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partners_token', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text("token");
            $table->text("timestamp");
            $table->unsignedInteger("user_id");
            $table->boolean("locked");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('partners_token');
    }
}
