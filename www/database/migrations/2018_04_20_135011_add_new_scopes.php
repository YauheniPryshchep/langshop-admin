<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewScopes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('scopes_types')) {
            DB::table('scopes_types')->insert([
                'id' => 6,
                'name' => 'LangShopApi'
            ]);
        }
        if (Schema::hasTable('scopes')) {
            DB::table('scopes')->insert([
                ['id' => 32, 'name' => 'langshopapi-stores-demo-show', 'description' => 'LangShop Api: Show demo stores', 'scopes_types_id' => 6],
                ['id' => 33, 'name' => 'langshopapi-stores-demo-update', 'description' => 'LangShop Api: Edit demo stores', 'scopes_types_id' => 6],
                ['id' => 34, 'name' => 'langshopapi-stores-trials-show', 'description' => 'LangShop Api: Show trial stores', 'scopes_types_id' => 6],
                ['id' => 35, 'name' => 'langshopapi-stores-trials-update', 'description' => 'LangShop Api: Edit trial stores', 'scopes_types_id' => 6],
                ['id' => 36, 'name' => 'langshopapi-stores-discounts-show', 'description' => 'LangShop Api: Show discount stores', 'scopes_types_id' => 6],
                ['id' => 37, 'name' => 'langshopapi-stores-discounts-update', 'description' => 'LangShop Api: Edit discount stores', 'scopes_types_id' => 6]
            ]);
        }

        if (Schema::hasTable('roles_scopes')) {
            DB::table('roles_scopes')->insert([
                ['roles_id' => 1, 'scopes_id' => 32],
                ['roles_id' => 1, 'scopes_id' => 33],
                ['roles_id' => 1, 'scopes_id' => 34],
                ['roles_id' => 1, 'scopes_id' => 35],
                ['roles_id' => 1, 'scopes_id' => 36],
                ['roles_id' => 1, 'scopes_id' => 37],
                ['roles_id' => 3, 'scopes_id' => 32],
                ['roles_id' => 3, 'scopes_id' => 33],
                ['roles_id' => 3, 'scopes_id' => 34],
                ['roles_id' => 3, 'scopes_id' => 35],
                ['roles_id' => 3, 'scopes_id' => 36],
                ['roles_id' => 3, 'scopes_id' => 37],

            ]);
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
