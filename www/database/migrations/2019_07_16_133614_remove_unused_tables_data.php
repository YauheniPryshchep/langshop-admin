<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class RemoveUnusedTablesData extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        $types = DB::table('scopes_types')
            ->whereIn('name', [
                'React Flow',
                'Stylefy',
                'LangShopApi',
                'iMobile',
                'Settings'
            ])
            ->pluck('id');

        $scopes = DB::table('scopes')
            ->whereIn('scopes_types_id', $types)
            ->pluck('id');

        DB::table('roles_scopes')->whereIn('scopes_id', $scopes)->delete();
        DB::table('scopes_types')->whereIn('id', $types)->delete();
        DB::table('scopes')->whereIn('id', $scopes)->delete();

        Schema::dropIfExists('email_templates_variables');
        Schema::dropIfExists('email_templates');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        //
    }
}
