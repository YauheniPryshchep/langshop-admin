<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TriggerEventLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trigger_event_logs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('workflow_id')->nullable();
            $table->string('event_slug');
            $table->json('event_data');
            $table->integer('status');
            $table->timestamp('check_at')->nullable();
            $table->timestamps();

            $table->index(['workflow_id']);
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
