<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;

class AddCampaignsScopes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('scopes_types')) {
            DB::table('scopes_types')->insert([
                'id'   => 11,
                'name' => 'Campaigns'
            ]);
        }
        if (Schema::hasTable('scopes')) {
            DB::table('scopes')->insert([
                ['id' => 58, 'name' => 'campaigns-show', 'description' => 'Campaigns: Show', 'scopes_types_id' => 11],
                ['id' => 59, 'name' => 'campaigns-update', 'description' => 'Campaigns: Edit', 'scopes_types_id' => 11],
            ]);
        }

        if (Schema::hasTable('roles_scopes')) {
            DB::table('roles_scopes')->insert([
                ['roles_id' => 1, 'scopes_id' => 58],
                ['roles_id' => 1, 'scopes_id' => 59],
                ['roles_id' => 3, 'scopes_id' => 58],
                ['roles_id' => 3, 'scopes_id' => 59],
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
