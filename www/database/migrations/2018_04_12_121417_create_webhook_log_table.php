<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWebhookLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('webhook_log')) {
            Schema::create('webhook_log', function (Blueprint $table) {
                $table->charset = 'utf8';
                $table->collation = 'utf8_general_ci';
                $table->bigIncrements('id');
                $table->string('store', 255)->index();
                $table->string('app', 255);
                $table->string('type', 255);
                $table->timestamp('created_at', 0)->useCurrent();
                $table->timestamp('updated_at', 0)->nullable();
                $table->longText('data')->nullable();
            });

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('webhook_log');
    }
}
