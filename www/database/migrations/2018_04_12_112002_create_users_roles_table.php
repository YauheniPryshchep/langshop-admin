<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('users_roles')) {
            Schema::create('users_roles', function (Blueprint $table) {
                $table->increments('id');
                $table->unsignedInteger('users_id');
                $table->unsignedInteger('roles_id');
                $table->unique(['users_id', 'roles_id']);
            });

            DB::table('users_roles')->insert([
                ['users_id' => 1, 'roles_id' => 1],
            ]);

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_roles');
    }
}
