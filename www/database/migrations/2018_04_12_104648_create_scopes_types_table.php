<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScopesTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('scopes_types')) {
            Schema::create('scopes_types', function (Blueprint $table) {
                $table->charset = 'utf8';
                $table->collation = 'utf8_general_ci';
                $table->increments('id');
                $table->string('name', 255);
            });

            DB::table('scopes_types')->insert([
                'id' => 1,
                'name' => 'Users'
            ]);
            DB::table('scopes_types')->insert([
                'id' => 3,
                'name' => 'Settings'
            ]);
            DB::table('scopes_types')->insert([
                'id' => 4,
                'name' => 'LangShop'
            ]);
            DB::table('scopes_types')->insert([
                'id' => 5,
                'name' => 'Buildify'
            ]);
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scopes_types');
    }
}
