<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVocabulariesScopes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('scopes_types')) {
            DB::table('scopes_types')->insert([
                'id'   => 12,
                'name' => 'Vocabularies'
            ]);
        }
        if (Schema::hasTable('scopes')) {
            DB::table('scopes')->insert([
                ['id' => 60, 'name' => 'vocabularies-show', 'description' => 'Vocabularies: Show', 'scopes_types_id' => 12],
                ['id' => 61, 'name' => 'vocabularies-update', 'description' => 'Vocabularies: Edit', 'scopes_types_id' => 12],
            ]);
        }

        if (Schema::hasTable('roles_scopes')) {
            DB::table('roles_scopes')->insert([
                ['roles_id' => 1, 'scopes_id' => 60],
                ['roles_id' => 1, 'scopes_id' => 61],
                ['roles_id' => 3, 'scopes_id' => 60],
                ['roles_id' => 3, 'scopes_id' => 61],
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
