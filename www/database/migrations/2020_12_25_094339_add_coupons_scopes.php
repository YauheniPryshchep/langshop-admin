<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCouponsScopes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('scopes_types')) {
            DB::table('scopes_types')->insert([
                'id'   => 13,
                'name' => 'Coupons'
            ]);
        }
        if (Schema::hasTable('scopes')) {
            DB::table('scopes')->insert([
                ['id' => 64, 'name' => 'coupons-show', 'description' => 'Coupons: Show', 'scopes_types_id' => 13],
                ['id' => 65, 'name' => 'coupons-update', 'description' => 'Coupons: Edit', 'scopes_types_id' => 13],
            ]);
        }

        if (Schema::hasTable('roles_scopes')) {
            DB::table('roles_scopes')->insert([
                ['roles_id' => 1, 'scopes_id' => 64],
                ['roles_id' => 1, 'scopes_id' => 65],
                ['roles_id' => 3, 'scopes_id' => 64],
                ['roles_id' => 3, 'scopes_id' => 65],
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
