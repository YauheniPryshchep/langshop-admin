<p align="center">
  <img src="https://cloud.devit-team.com/s/sPZ8nnkWRoDKKH2/preview" alt="" style="width: 100%" />
</p>

---

This is a dockerized Admin Panel for our shopify's apps, integrated with GitLab CI _(sources testing and images building)_.

## System requirements

For local application starting (for development) make sure that you have locally installed next applications:
- `docker >= 18.09.4` 
	_(install: `curl -fsSL get.docker.com | sudo sh`)_
- `sudo usermod -aG docker $USER` (for Linux)
- `newgrp docker` (for Linux)
- `docker-compose >= 1.24` _([installing manual][install_compose])_
- `make >= 4.2.1` _(install: `apt-get install make`)_

## Used services

This application uses next services:

- MariaDB 10.1
- PHP 7.3 FPM
- HTTPD 2.4 (Apache)
- NodeJS 8 (For Development only)

Declaration of all services can be found into `./docker-compose.yml` file.

## Work with application

Most used commands declared in `./Makefile` file. For more information execute in your terminal `make help`.

Here are just a few of them:

Command signature | Description
----------------- | -----------
`make php73-pull` | php73 - pull latest Docker image (from remote registry)
`make php73` | php73 - build Docker image locally
`make php73-push` | php73 - tag and push Docker image into remote registry
`make httpd-pull` | httpd - pull latest Docker image (from remote registry)
`make httpd` | httpd - build Docker image locally
`make httpd-push` | httpd - tag and push Docker image into remote registry
`make pull` | Pull all Docker images (from remote registry)
`make build` | Build all Docker images
`make push` | Tag and push all Docker images into remote registry
`make login` | Make login into remote Docker registry <sup>1</sup>
`make clean` | Remove images from local registry
`make up` | Start all containers (in background)
`make down` | Stop all started for development containers
`make restart` | Restart all started for development containers
`make shell-php73` | Start shell php73 application container
`make shell-apache` | Start shell apache application container
`make install` | Install application dependencies into php73 container
`make init` | Make full application initialization (install, seed, build assets, etc)
`make watch` | Start watching assets for changes (node)
`make npm-install` | NPM Install application dependencies
`docker-compose down -v` | Stop all application containers and **remove all application data** (database, etc)

> **<sup>1</sup>** required for Docker images pulling/pushing. If you use Two-Factor Authentication (2FA) you should use auth token instead your password. Generate your token [here][personal_access_tokens].

After application starting you can open [127.0.0.1:8134](http://127.0.0.1:8134/) in your browser.

### Fast application starting

Just execute into your terminal next commands:

```bash
$ cp .env.example .env
$ cp www/.env.example www/.env
```
Enter variables: **CURRENT_USER** _(how to get: `echo ${USER}`)_ & **CURRENT_USER_ID** _(how to get: `id -u ${USER}`)_

At first you need to authorize with GitLab registry.  
```bash
$ make login
```

Create "dev" network for local environment
```bash
$ docker network create --attachable --gateway 172.30.0.1 --subnet 172.30.0.0/16 dev
```

Install all requirements
```bash
$ make up
$ make init
```

### To continue develop
```bash
$ make npm-install
```

```bash
$ make npm-watch
```

```bash
$ make npm-build
```

In development mode, you can open your browser with a following URL [http://localhost:8134](http://localhost:8134)

[install_compose]:https://docs.docker.com/compose/install/#install-compose
[personal_access_tokens]:https://gitlab.com/profile/personal_access_tokens
