#!/usr/bin/make

include .env

SHELL= /bin/sh

docker_bin= $(shell command -v docker 2> /dev/null)
docker_compose_bin= $(shell command -v docker-compose 2> /dev/null)

.DEFAULT_GOAL := help

help: ## Show this help
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z0-9_-]+:.*?## / {printf "  \033[92m%-15s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)
	@echo "\n  Allowed for overriding next properties:\n\n\
	    PULL_TAG - Tag for pulling images before building own\n\
	              ('latest' by default)\n\
	    PUBLISH_TAGS - Tags list for building and pushing into remote registry\n\
	                   (delimiter - single space, 'latest' by default)\n\n\
	  Usage example:\n\
	    make PULL_TAG='v1.2.3' PUBLISH_TAGS='latest v1.2.3 test-tag' php56-push"
#Registry -------------------------------------------------
	
# --- [ PHP ] -------------------------------------------
---------------: ## ------[ PHP ]---------
php73-pull: ## php - pull latest Docker image (from remote registry)
	-$(docker_bin) pull "$(PHP_IMAGE):$(PHP_IMAGE_TAG)"

php73: ## php - build Docker image locally
	$(docker_bin) build \
	  --tag "$(PHP_IMAGE):$(PHP_IMAGE_TAG)" \
	  -f $(PHP_IMAGE_DOCKERFILE) $(PHP_IMAGE_CONTEXT)

php73-push: ## php - tag and push Docker image into remote registry
	$(docker_bin) build \
	  --tag "$(PHP_IMAGE):$(PHP_IMAGE_TAG)" \
	  -f $(PHP_IMAGE_DOCKERFILE) $(PHP_IMAGE_CONTEXT);
	$(docker_bin) push "$(PHP_IMAGE):$(PHP_IMAGE_TAG)"

# --- [ HTTPD ] -------------------------------------------
---------------: ## ------[ HTTPD ]---------
httpd-pull: ## httpd - pull latest Docker image (from remote registry)
	-$(docker_bin) pull "$(HTTPD_IMAGE):$(HTTPD_IMAGE_TAG)"

httpd: ## httpd - build Docker image locally
	$(docker_bin) build \
	  --tag "$(HTTPD_IMAGE):$(HTTPD_IMAGE_TAG)" \
	  -f $(HTTPD_IMAGE_DOCKERFILE) $(HTTPD_IMAGE_CONTEXT)

httpd-push: ## httpd - tag and push Docker image into remote registry
	$(docker_bin) build \
	  --tag "$(HTTPD_IMAGE):$(HTTPD_IMAGE_TAG)" \
	  -f $(HTTPD_IMAGE_DOCKERFILE) $(HTTPD_IMAGE_CONTEXT);
	$(docker_bin) push "$(HTTPD_IMAGE):$(HTTPD_IMAGE_TAG)"

# --- [ REDIS ] -------------------------------------------
---------------: ## ------[ REDIS ]---------
redis-pull: ## redis - pull latest Docker image (from remote registry)
	-$(docker_bin) pull "$(REDIS_IMAGE):$(REDIS_IMAGE_TAG)"

redis: ## redis - build Docker image locally
	$(docker_bin) build \
	  --tag "$(REDIS_IMAGE):$(REDIS_IMAGE_TAG)" \
	  -f $(REDIS_IMAGE_DOCKERFILE) $(REDIS_IMAGE_CONTEXT)

redis-push: redis-pull ## redis - tag and push Docker image into remote registry
	$(docker_bin) build \
	  --tag "$(REDIS_IMAGE):$(REDIS_IMAGE_TAG)" \
	  -f $(REDIS_IMAGE_DOCKERFILE) $(REDIS_IMAGE_CONTEXT)
	$(docker_bin) push "$(REDIS_IMAGE):$(REDIS_IMAGE_TAG)"

# --- [ NODE ] -------------------------------------------
---------------: ## ------[ NODE ]---------
node-pull: ## node - pull latest Docker image (from remote registry)
	-$(docker_bin) pull "$(NODE_IMAGE):$(NODE_IMAGE_TAG)"

node: ## node - build Docker image locally
	$(docker_bin) build \
	  --tag "$(NODE_IMAGE):$(NODE_IMAGE_TAG)" \
	  -f $(NODE_IMAGE_DOCKERFILE) $(NODE_IMAGE_CONTEXT)

node-push: node-pull ## node - tag and push Docker image into remote registry
	$(docker_bin) build \
	  --tag "$(NODE_IMAGE):$(NODE_IMAGE_TAG)" \
	  -f $(NODE_IMAGE_DOCKERFILE) $(NODE_IMAGE_CONTEXT)
	$(docker_bin) push "$(NODE_IMAGE):$(NODE_IMAGE_TAG)"

---------------: ## ------[ ALL ]---------
#--- [ ALL ] -------------------------------------------

pull: php73-pull httpd-pull redis-pull node-pull## Pull all Docker images (from remote registry)

build: php73 httpd redis node## Build all Docker images

push: php73-push httpd-push redis-push node-push## Tag and push all Docker images into remote registry

login: ## Log in to a remote Docker registry
	@echo $(docker_login_hint)
	$(docker_bin) login -u $(DOCKER_LOGIN) -p $(DOCKER_TOKEN)

logout: ## Log out a remote Docker registry
	$(docker_bin) logout $(REGISTRY_HOST)

clean: ## Remove images from local registry
	-$(docker_compose_bin) down -v
	$(foreach image,$(all_images),$(docker_bin) rmi -f $(image);)
---------------: ## ------[ ACTIONS ]---------
#Actions --------------------------------------------------
up: ## Start all containers (in background)
	$(docker_compose_bin) -f docker-compose.yml -f docker/docker-compose.development.yml up --no-recreate -d
up-staging:	
	$(docker_compose_bin) -f docker-compose.yml -f docker/docker-compose.staging.yml up --no-recreate -d 
up-prod:	
	$(docker_compose_bin) -f docker-compose.yml -f docker/docker-compose.production.yml up --no-recreate -d 
down: ## Stop all started for development containers
	$(docker_compose_bin) -f docker-compose.yml down
restart: up ## Restart all started for development containers
	$(docker_compose_bin) -f docker-compose.yml restart
shell-php73: up ## Start shell php73 application container
	$(docker_compose_bin) -f docker-compose.yml exec -T apps-php73-fpm bash
shell-apache: up ## Start -f docker-compose.yml shell apache application container
	$(docker_compose_bin) exec -T "$(APACHE_CONTAINER)" bash
install: ## Install application dependencies into php73 container
	$(docker_compose_bin) -f docker-compose.yml exec --user $(CURRENT_USER_ID) -T apps-php73-fpm composer install
init: install ## Make full application initialization (install, seed, build assets, etc)
	$(docker_bin) run --rm --network dev mysql mysql -h$(DB_HOST) -P$(DB_PORT) -uroot -p$(MYSQL_ROOT_PASSWORD) -e "GRANT ALL PRIVILEGES ON *.* TO '$(DB_USERNAME)'@'%' WITH GRANT OPTION; flush privileges; CREATE DATABASE IF NOT EXISTS $(DB_DATABASE);"
	$(docker_compose_bin) -f docker-compose.yml exec --user $(CURRENT_USER_ID) -T apps-php73-fpm php artisan migrate --force
staging: up-staging install ## Make full application initialization for STAGING server
production: up-prod install ## Make full application initialization for PRODUCTION server
after-deploy: ## Commands that run after deploy
	$(docker_compose_bin) -f docker-compose.yml exec --user $(CURRENT_USER_ID) -T apps-php73-fpm composer install
	$(docker_compose_bin) -f docker-compose.yml exec --user $(CURRENT_USER_ID) -T apps-php73-fpm php artisan migrate --force
npm-watch: up ## Start watching assets for changes (node)
	$(docker_compose_bin) -f docker-compose.yml run --rm apps-node npm run watch
npm-install: up ## NPM Install application dependencies
	$(docker_compose_bin) -f docker-compose.yml run --rm apps-node npm install
npm-build: up ## Build assets
	$(docker_compose_bin) -f docker-compose.yml run --rm apps-node npm run prod
